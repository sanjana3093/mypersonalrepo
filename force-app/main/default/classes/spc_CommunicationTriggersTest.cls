/**
* @author Deloitte
* @description This is the test class for spc_CommunicationTriggers
*/
@isTest
public class spc_CommunicationTriggersTest {

    public static Case objCase;
    public static Account acc;
    public static List<Case> caselist;
    public static Account manAcc;
    public static PatientConnect__PC_Engagement_Program__c engPrgm;

    private static spc_CommunicationTriggers commTrigger = new spc_CommunicationTriggers();

    static void setup() {
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

        acc = spc_Test_Setup.createPatient('New Patient');
        acc.spc_HIPAA_Consent_Received__c = 'Yes';
        acc.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        acc.spc_Patient_Services_Consent_Received__c = 'Yes';
        acc.spc_Text_Consent__c = 'Yes';
        acc.spc_REMS_Text_Consent__c = 'Yes';
        acc.spc_REMS_Text_Consent_Date__c = Date.valueOf('2014-06-07');
        acc.spc_Patient_Marketing_Consent_Date__c = Date.valueOf('2014-06-07');
        acc.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf('2014-06-07');
        acc.spc_Patient_Text_Consent_Date__c = Date.valueOf('2014-06-07');
        acc.spc_Patient_Service_Consent_Date__c = Date.valueOf('2014-06-07');
        acc.spc_REMS_ID__c = '1234';
        acc.PatientConnect__PC_Gender__c = 'Male';
        acc.spc_REMS_Enrollment_Status__c = 'Enrolled';
        acc.spc_REMS_Enrollment_Status_Date__c = Date.valueOf('2014-06-07');
        spc_Database.ins(acc);

        manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        spc_Database.ins(manAcc);

        engPrgm = spc_Test_Setup.createEngagementProgram('Engagement Program SageRx', manAcc.Id);
        engPrgm.PatientConnect__PC_Program_Code__c = 'SAGE';
        insert engPrgm;
    }
    @isTest
    static void test_AfterUpdateAccountTrigger() {
        setup();

        objCase = spc_Test_Setup.createCase('PC_Program', spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType),
                                            engPrgm.Id, acc.Id);
        objCase.spc_WC__c = spc_ApexConstants.WC_COMPLETE;
        objCase.spc_REMS_Authorization_Status__c = 'Not Authorized';
        objCase.spc_REMS_Enrollment_Status__c = 'Enrolled';
        objCase.Status = 'Enrolled';
        insert objCase;

        test.startTest();
        acc.Name = 'Test';
        acc.spc_REMS_Text_Consent_Date__c = Date.today();
        update acc;
        test.stopTest();
    }

    @isTest
    static void test_AfterInsertCaseTriggers1() {
        setup();
        Account acc1 = spc_Test_Setup.createPatient('Patients');
        acc1.spc_HIPAA_Consent_Received__c = 'No';
        acc1.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        acc1.spc_Patient_Services_Consent_Received__c = 'Yes';
        acc1.spc_REMS_Text_Consent_Date__c = Date.today();
        acc1.spc_Text_Consent__c = 'Yes';
        acc1.spc_REMS_Text_Consent__c = 'Yes';
        acc1.spc_Patient_Marketing_Consent_Date__c = Date.valueOf('2014-06-07');
        acc1.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf('2014-06-07');
        acc1.spc_Patient_Text_Consent_Date__c = Date.valueOf('2014-06-07');
        acc1.spc_Patient_Service_Consent_Date__c = Date.valueOf('2014-06-07');
        acc1.spc_REMS_ID__c = '1234';
        acc1.PatientConnect__PC_Gender__c = 'Male';
        acc1.spc_REMS_Enrollment_Status__c = 'Enrolled';
        acc1.spc_REMS_Enrollment_Status_Date__c = Date.valueOf('2014-06-07');
        spc_Database.ins(acc1);

        objCase = spc_Test_Setup.createCase('PC_Program', spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType),
                                            engPrgm.Id, acc1.Id);
        objCase.spc_WC__c = 'In Progress';
        objCase.spc_REMS_Authorization_Status__c = 'Not Authorized';
        objCase.spc_REMS_Enrollment_Status__c = 'Enrolled';
        objCase.Status = 'Enrolled';
        objCase.PatientConnect__PC_Status_Indicator_3__c = 'In Progress';

        spc_Database.ins(objCase) ;

        Account phyAcc2 = spc_Test_Setup.createTestAccount('PC_Physician');
        phyAcc2.Name = 'Acc2_Name';
        phyAcc2.spc_REMS_Enrollment_Status__c = 'Enrolled';
        spc_Database.ins(phyAcc2);

        Account hcoAcc4 = new Account();
        hcoAcc4.PatientConnect__PC_Date_of_Birth__c = System.today();
        hcoAcc4 .Phone = '1235567888';
        hcoAcc4 .spc_Patient_Services_Consent_Received__c = 'Yes';
        hcoAcc4 .spc_Patient_Service_Consent_Date__c = System.Today();
        hcoAcc4 .spc_REMS_ID__c = '12345234';
        hcoAcc4 .PatientConnect__PC_Gender__c = 'Male';
        //spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
        hcoAcc4.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        hcoAcc4.spc_REMS_Certification_Status__c = 'Certified';
        hcoAcc4.spc_REMS_Certification_Status_Date__c = System.Today();
        hcoAcc4.Name = 'ACCOUNT2_TEST_NAME';
        hcoAcc4.Fax = '123452278';
        hcoAcc4.PatientConnect__PC_Email__c = 'test3@test2.com';
        insert hcoAcc4;

        PatientConnect__PC_Address__c addr2 = spc_Test_Setup.createAddress(hcoAcc4.Id);
        addr2.spc_Fax__c = '123645';
        insert addr2;
        PatientConnect__PC_Association__c socAssoc2 = new PatientConnect__PC_Association__c(PatientConnect__PC_Account__c = hcoAcc4.Id, PatientConnect__PC_Program__c = objCase.Id, PatientConnect__PC_Role__c = 'HCO', PatientConnect__PC_EndDate__c = System.today() + 10, spc_Address__c = addr2.Id);
        insert socAssoc2;

        PatientConnect__PC_Association__c oAsso = new PatientConnect__PC_Association__c();
        oAsso.PatientConnect__PC_Role__c = 'Treating Physician';
        oAsso.PatientConnect__PC_Program__c = objCase.id;
        oAsso.PatientConnect__PC_Account__c = phyAcc2.id;
        insert oAsso;

        List<PatientConnect__PC_Program_Coverage__c> pgrmCoverageList = new List<PatientConnect__PC_Program_Coverage__c>();
        PatientConnect__PC_Program_Coverage__c progCoverageRec1 = spc_Test_Setup.newProgCoverage(objCase.Id, 'PC_PAP', 'Uninsured');
        progCoverageRec1.PatientConnect__PC_Coverage_Type__c = 'PAP Coverage';
        progCoverageRec1.PatientConnect__PC_Coverage_Status__c = 'Active';
        progCoverageRec1.spc_Not_Eligible__c = true;
        insert progCoverageRec1;

        PatientConnect__PC_Program_Coverage__c progCoverageRec3 = spc_Test_Setup.newProgCoverage(objCase.Id, 'PC_Copay_Coverage', '');
        progCoverageRec3.PatientConnect__PC_Coverage_Type__c = spc_ApexConstants.PROGRAM_COVERAGE_TYPE_DRUGCOPAY;
        progCoverageRec3.PatientConnect__PC_Coverage_Status__c = 'Active';
        progCoverageRec3.spc_Not_Eligible__c = true;
        insert progCoverageRec3;

        acc1.spc_HIPAA_Consent_Received__c = 'Yes';
        update acc1;

        objCase.spc_REMS_Authorization_Status__c = 'Authorized';
        objCase.spc_WC__c  = 'Complete';
        objCase.PatientConnect__PC_Status_Indicator_3__c = 'Complete';

        update objCase;

        objCase.status = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_STATUS_PRETREATMENT);
        update objCase;

    }

    @isTest
    static void test_AfterInsertInteractionTriggers() {
        setup();
        objCase = spc_Test_Setup.createCase('PC_Program', spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType),
                                            engPrgm.Id, acc.Id);
        objCase.spc_WC__c = spc_ApexConstants.WC_COMPLETE;

        spc_Database.ins(objCase) ;

        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
        spc_Database.ins(hcoAcc) ;

        PatientConnect__PC_Association__c oAsso = new PatientConnect__PC_Association__c();
        oAsso.PatientConnect__PC_Role__c = 'HCO';
        oAsso.PatientConnect__PC_Program__c = objCase.id;
        oAsso.PatientConnect__PC_Account__c = hcoAcc.id;
        oAsso.PatientConnect__PC_AssociationStatus__c = 'active';
        spc_Database.ins(oAsso);

        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(objCase.id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        interaction.PatientConnect__PC_Status__c = 'Complete';
        interaction.spc_SOC_Type__c = 'Home';
        interaction.spc_Has_Active_SOC__c = true;
        interaction.spc_HIP__c = 'Other';
        interaction.spc_Scheduled_Date__c = system.today();
        spc_Database.ins(interaction);


        interaction.spc_Scheduled_Date__c = system.today() + 2;
        spc_Database.upd(interaction);

    }

    @isTest
    static void test_AfterInsertAssociationTriggers() {
        setup();
        objCase = spc_Test_Setup.createCase('PC_Program',
                                            spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType),
                                            engPrgm.Id, acc.Id);
        objCase.spc_WC__c = spc_ApexConstants.WC_COMPLETE;
        spc_Database.ins(objCase);

        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
        spc_Database.ins(hcoAcc);

        PatientConnect__PC_Association__c oAsso = new PatientConnect__PC_Association__c();
        oAsso.PatientConnect__PC_Role__c = 'HCO';
        oAsso.PatientConnect__PC_Program__c = objCase.id;
        oAsso.PatientConnect__PC_Account__c = hcoAcc.id;
        oAsso.PatientConnect__PC_AssociationStatus__c = 'active';
        spc_Database.ins(oAsso);

        oAsso.PatientConnect__PC_AssociationStatus__c = 'Inactive';
        spc_Database.upd(oAsso);

        oAsso.PatientConnect__PC_AssociationStatus__c = 'active';
        spc_Database.upd(oAsso);
    }

    @isTest
    static void test_AfterInsertAssociationTriggers1() {
        setup();
        objCase = spc_Test_Setup.createCase('PC_Program',
                                            spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType),
                                            engPrgm.Id, acc.Id);
        objCase.spc_WC__c = spc_ApexConstants.WC_COMPLETE;
        Test.startTest();
        spc_Database.ins(objCase) ;

        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Physician').getRecordTypeId());
        spc_Database.ins(hcoAcc) ;

        PatientConnect__PC_Association__c oAsso = new PatientConnect__PC_Association__c();
        oAsso.PatientConnect__PC_Role__c = 'Referring Physician';
        oAsso.PatientConnect__PC_Program__c = objCase.id;
        oAsso.PatientConnect__PC_Account__c = hcoAcc.id;
        oAsso.PatientConnect__PC_AssociationStatus__c = 'active';
        spc_Database.ins(oAsso);

        oAsso.PatientConnect__PC_AssociationStatus__c = 'Inactive';
        spc_Database.upd(oAsso);

        objCase.Status =   'Never Start';
        spc_Database.upd(objCase);

        oAsso.PatientConnect__PC_AssociationStatus__c = 'active';
        spc_Database.upd(oAsso);

        objCase.Status =   'Never Start';
        spc_Database.upd(objCase);

        objCase.Status =   'Enrolled';
        spc_Database.upd(objCase);
        Test.stopTest();
    }

    @isTest
    static void test_AfterInsertAssociationTriggers2() {
        setup();
        objCase = spc_Test_Setup.createCase('PC_Program', spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType),
                                            engPrgm.Id, acc.Id);
        objCase.spc_WC__c = spc_ApexConstants.WC_COMPLETE;
        spc_Database.ins(objCase) ;

        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Physician').getRecordTypeId());
        spc_Database.ins(hcoAcc) ;

        PatientConnect__PC_Association__c oAsso = new PatientConnect__PC_Association__c();
        oAsso.PatientConnect__PC_Role__c = 'Referring Physician';
        oAsso.PatientConnect__PC_Program__c = objCase.id;
        oAsso.PatientConnect__PC_Account__c = hcoAcc.id;
        oAsso.PatientConnect__PC_AssociationStatus__c = 'active';
        spc_Database.ins(oAsso);

        oAsso.PatientConnect__PC_AssociationStatus__c = 'Inactive';
        spc_Database.upd(oAsso);

        objCase.Status = 'Enrolled';
        spc_Database.upd(objCase);

        oAsso.PatientConnect__PC_AssociationStatus__c = 'active';
        spc_Database.upd(oAsso);
    }

    @isTest
    static void test_AfterInsertAssociationTriggers3() {
        setup();
        objCase = spc_Test_Setup.createCase('PC_Program', spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType),
                                            engPrgm.Id, acc.Id);
        objCase.spc_WC__c = spc_ApexConstants.WC_COMPLETE;
        spc_Database.ins(objCase) ;

        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Physician').getRecordTypeId());
        spc_Database.ins(hcoAcc) ;

        PatientConnect__PC_Association__c oAsso = new PatientConnect__PC_Association__c();
        oAsso.PatientConnect__PC_Role__c = 'Referring Physician';
        oAsso.PatientConnect__PC_Program__c = objCase.id;
        oAsso.PatientConnect__PC_Account__c = hcoAcc.id;
        oAsso.PatientConnect__PC_AssociationStatus__c = 'active';
        spc_Database.ins(oAsso);

        oAsso.PatientConnect__PC_AssociationStatus__c = 'Inactive';
        spc_Database.upd(oAsso);

        oAsso.PatientConnect__PC_AssociationStatus__c = 'active';
        spc_Database.upd(oAsso);
    }


    @isTest
    static void test_AfterInsertPRogramCoverageTriggers() {
        setup();
 Test.startTest();  
        objCase = spc_Test_Setup.createCase('PC_Program', spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType),
                                            engPrgm.Id, acc.Id);
        objCase.spc_WC__c = spc_ApexConstants.WC_COMPLETE;
        objCase.spc_REMS_Enrollment_Status__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_ENROLLMENT_STATUS_ENROLLED);
        objCase.spc_REMS_Authorization_Status__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_ENROLLMENT_STATUS_AUTHORIZED);

        spc_Database.ins(objCase) ;


        PatientConnect__PC_Program_Coverage__c progCoverageRec2 = spc_Test_Setup.newProgCoverage(objCase.Id, 'PC_Copay_Coverage', '');
        progCoverageRec2.PatientConnect__PC_Coverage_Type__c = spc_ApexConstants.PROGRAM_COVERAGE_TYPE_ADMINCOPAY;
        progCoverageRec2.PatientConnect__PC_Coverage_Status__c = 'Active';
        progCoverageRec2.spc_Not_Eligible__c = true;
        insert progCoverageRec2;

        PatientConnect__PC_Program_Coverage__c progCoverageRec1 = spc_Test_Setup.newProgCoverage(objCase.Id, 'PC_PAP', 'Uninsured');
        progCoverageRec1.PatientConnect__PC_Coverage_Type__c = 'PAP Coverage';
        progCoverageRec1.PatientConnect__PC_Coverage_Status__c = 'Active';
        progCoverageRec1.spc_Not_Eligible__c = true;
        insert progCoverageRec1;

        PatientConnect__PC_Program_Coverage__c progCoverageRec = spc_Test_Setup.newProgCoverage(objCase.Id, 'PC_Copay_Coverage', '');
        progCoverageRec.PatientConnect__PC_Coverage_Type__c = spc_ApexConstants.PROGRAM_COVERAGE_TYPE_DRUGCOPAY;
        progCoverageRec.PatientConnect__PC_Coverage_Status__c = 'Active';
        progCoverageRec.spc_Not_Eligible__c = true;
        insert progCoverageRec;

        progCoverageRec2.PatientConnect__PC_Coverage_Type__c = 'Drug Copay';
        update progCoverageRec2;

        progCoverageRec2.PatientConnect__PC_Coverage_Type__c = spc_ApexConstants.PROGRAM_COVERAGE_TYPE_ADMINCOPAY;
        update progCoverageRec2;
 Test.stopTest();  

    }

    @isTest
    static void test_AfterUpdatePRogramCoverageTriggers() {
        setup();

        objCase = spc_Test_Setup.createCase('PC_Program', spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType),
                                            engPrgm.Id, acc.Id);
        objCase.spc_WC__c = spc_ApexConstants.WC_COMPLETE;
        objCase.spc_REMS_Authorization_Status__c = 'Authorized';
        objCase.spc_REMS_Enrollment_Status__c = 'Enrolled';

        spc_Database.ins(objCase) ;

        List<PatientConnect__PC_Program_Coverage__c> pgrmCoverageList = new List<PatientConnect__PC_Program_Coverage__c>();
        PatientConnect__PC_Program_Coverage__c progCoverageRec1 = spc_Test_Setup.newProgCoverage(objCase.Id, 'PC_PAP', 'Uninsured');
        progCoverageRec1.PatientConnect__PC_Coverage_Type__c = 'PAP Coverage';
        progCoverageRec1.PatientConnect__PC_Coverage_Status__c = 'Inactive';
        progCoverageRec1.spc_Not_Eligible__c = true;
        insert progCoverageRec1;

        progCoverageRec1.PatientConnect__PC_Coverage_Status__c = 'Active';
        update progCoverageRec1;
    }
}
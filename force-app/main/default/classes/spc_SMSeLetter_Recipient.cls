/*********************************************************************************************************
* This is The PC class cloned by Sage for Reference only 
************************************************************************************************************/
public class spc_SMSeLetter_Recipient {
    // Used on Send eLetter page to show recipients and collect fax and email selections
    @AuraEnabled public Account recipient {get;set;}
    @AuraEnabled public String recipientType {get;set;}  
    @AuraEnabled public String communicationLanguage {get;set;}
    @AuraEnabled public String recipientName{get;set;}
    @AuraEnabled public String recipientSMS{get;set;}
    @AuraEnabled public Boolean sendSMS {get; set;}
    @AuraEnabled public Boolean sendToMe {get;set;}
    @AuraEnabled public Boolean bDisableSendToMe {get;set;}
    @AuraEnabled public Boolean bDisableSMS {get;set;}
    
    public spc_SMSeLetter_Recipient(Account recipient, String recipientType, Set<String> availableLanguages,Set<String> Channels){
        this.recipient = recipient;
        this.recipientSMS = recipient.Phone;
        this.recipientName = recipient.Name;
        this.recipientType = recipientType;
        
        // Communication Language is defaulted to recipent's communiction language or the language set at eLetter record level. 
        this.communicationLanguage = availableLanguages.contains(recipient.PatientConnect__PC_Communication_Language__c) ? recipient.PatientConnect__PC_Communication_Language__c : '';
        // Phone will be selected by default (if a Phone number exist for the participant)
        this.sendSMS =  (recipient.phone!=null) ? (Channels!=null)?Channels.contains(Label.spc_SMS_Template)? true: false:false:false;  
        this.sendToMe = false;
    }
}
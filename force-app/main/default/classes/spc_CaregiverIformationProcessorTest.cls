/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for CaregiverIformationProcessor
    *  @date            07/18/2018
    *  @version         1.0
*********************************************************************************************************/
@isTest
public class spc_CaregiverIformationProcessorTest {

    @testSetup static void setup() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
    }

    @isTest static void testProcessEnrollment() {
        test.startTest();
        Id ID_PATIENT_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)).getRecordTypeId();
        //Create Patient Account
        Account account = spc_Test_Setup.createAccount(ID_PATIENT_RECORDTYPE);
        account.spc_HIPAA_Consent_Received__c = 'Yes';
        account.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf(Label.spc_test_birth_date);
        if (null != account) {
            insert account;
        }
        PatientConnect__PC_Address__c addr = spc_Test_Setup.createTestAddress(account);

        insert addr;
        //Create Enrollment Case
        Case enrollmentCase = spc_Test_Setup.newCase(account.id, 'PC_Enrollment');
        Account   manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        spc_Database.ins(manAcc);
        PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('Engagement Program SageRx', manAcc.Id);
        engPrgm.PatientConnect__PC_Program_Code__c = 'SAGE';
        insert engPrgm;
        //Create Program Case
        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');
        programCase.PatientConnect__PC_Engagement_Program__c = engPrgm.Id;
        if (null != programCase) {
            insert programCase;
            enrollmentCase.PatientConnect__PC_Program__c = programCase.id;
            if (null != enrollmentCase) {
                insert enrollmentCase;
            }
        }
        Map<String, Object> pageState = new Map<String, Object>();
        Map<String, Object> persistedAccountMap = new Map<String, Object>();
        Map<String, Object> persistedAddressMap = new Map<String, Object>();

        persistedAccountMap.put('PatientConnect__PC_Caregiver_First_Name__c', 'TestProcessorFirstName');
        persistedAccountMap.put('PatientConnect__PC_Caregiver_Last_Name__c', 'TestProcessorLastName');
        persistedAccountMap.put('PatientConnect__PC_Email__c', 'TestProcessorCGFirstName@test.com');
        persistedAccountMap.put('address1', 'TestProcessorAddress1');
        persistedAccountMap.put('spc_Caregiver_Date_of_Birth__c', String.valueOf(System.today()));
        persistedAccountMap.put('spc_Caregiver_Gender__c', 'Male');
        persistedAccountMap.put('Phone', '8704693');
        persistedAccountMap.put('spc_Home_Phone__c', '8704693898');
        persistedAccountMap.put('spc_Office_Phone__c', '18876543256');
        persistedAccountMap.put('PatientConnect__PC_Communication_Language__c', 'English');
        persistedAccountMap.put('spc_Caregiver_Preferred_Time_to_Contact__c', 'Morning');
        persistedAccountMap.put('PatientConnect__PC_Caregiver_Relationship_to_Patient', 'Other');
        persistedAccountMap.put('spc_Permission_to_Call__c', 'Yes');
        persistedAccountMap.put('spc_Permission_to_Email__c', 'Yes');
        persistedAccountMap.put('spc_Permission_from_Patient_to_Share_PHI__c', 'Yes');
        persistedAccountMap.put('spc_Date_Permission_to_Share_PHI__c', String.valueOf(System.today()));
        persistedAccountMap.put('caregiverLookUpId', account.id);
        persistedAccountMap.put('notes', 'NA');
        persistedAccountMap.put('PatientConnect__PC_Primary_Address__c', addr.id);

        persistedAddressMap.put('PatientConnect__PC_Address_1__c', 'Address1');
        persistedAddressMap.put('PatientConnect__PC_Address_2__c', 'address 2');
        persistedAddressMap.put('PatientConnect__PC_Address_3__c', 'address 4');
        persistedAddressMap.put('addressType', 'Home');
        persistedAddressMap.put('PatientConnect__PC_City__c', 'City');
        persistedAddressMap.put('PatientConnect__PC_Country__c', 'Couuntry');
        persistedAddressMap.put('PatientConnect__PC_State__c', 'State');
        persistedAddressMap.put('status', 'Active');
        persistedAddressMap.put('PatientConnect__PC_Zip_Code__c', '123987');
        persistedAddressMap.put('PatientConnect__PC_State__c', 'State');
        persistedAddressMap.put('PatientConnect__PC_State__c', 'State');


        pageState.put('Account', persistedAccountMap);
        pageState.put('caregiverAccount', persistedAccountMap);
        pageState.put('Address', persistedAddressMap);

        spc_CaregiverInformationProcessor.processEnrollment(enrollmentCase, pageState);
        System.assert(persistedAccountMap.size() > 0);
        System.assert(persistedAddressMap.size() > 0);
        test.stopTest();
    }

    @isTest static void testProcessEnrollment1() {
        test.startTest();
        Id ID_PATIENT_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)).getRecordTypeId();
        //Create Patient Account
        Account account = spc_Test_Setup.createAccount(ID_PATIENT_RECORDTYPE);
        account.spc_HIPAA_Consent_Received__c = 'Yes';
        account.PatientConnect__PC_First_Name__c = 'test';
        account.PatientConnect__PC_Last_Name__c = 'test last';
        account.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf(Label.spc_test_birth_date);
        if (null != account) {
            insert account;
        }
        PatientConnect__PC_Address__c addr = spc_Test_Setup.createTestAddress(account);

        insert addr;
        //Create Enrollment Case
        Case enrollmentCase = spc_Test_Setup.newCase(account.id, 'PC_Enrollment');
        Account   manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        spc_Database.ins(manAcc);
        PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('Engagement Program SageRx', manAcc.Id);
        engPrgm.PatientConnect__PC_Program_Code__c = 'SAGE';
        insert engPrgm;
        //Create Program Case
        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');
        programCase.PatientConnect__PC_Engagement_Program__c = engPrgm.Id;
        if (null != programCase) {
            insert programCase;
            enrollmentCase.PatientConnect__PC_Program__c = programCase.id;
            if (null != enrollmentCase) {
                insert enrollmentCase;
            }
        }
        Map<String, Object> pageState = new Map<String, Object>();
        Map<String, Object> persistedAccountMap = new Map<String, Object>();
        Map<String, Object> persistedAddressMap = new Map<String, Object>();

        persistedAccountMap.put('PatientConnect__PC_Caregiver_First_Name__c', 'TestProcessorFirstName');
        persistedAccountMap.put('PatientConnect__PC_Caregiver_Last_Name__c', 'TestProcessorLastName');
        persistedAccountMap.put('PatientConnect__PC_Email__c', 'TestProcessorCGFirstName@test.com');
        persistedAccountMap.put('address1', 'TestProcessorAddress1');
        persistedAccountMap.put('spc_Caregiver_Date_of_Birth__c', String.valueOf(System.today()));
        persistedAccountMap.put('spc_Caregiver_Gender__c', 'Male');
        persistedAccountMap.put('Phone', '8704693');
        persistedAccountMap.put('spc_Home_Phone__c', '8704693898');
        persistedAccountMap.put('spc_Office_Phone__c', '18876543256');
        persistedAccountMap.put('PatientConnect__PC_Communication_Language__c', 'English');
        persistedAccountMap.put('spc_Caregiver_Preferred_Time_to_Contact__c', 'Morning');
        persistedAccountMap.put('PatientConnect__PC_Caregiver_Relationship_to_Patient', 'Other');
        persistedAccountMap.put('spc_Permission_to_Call__c', 'Yes');
        persistedAccountMap.put('spc_Permission_to_Email__c', 'Yes');
        persistedAccountMap.put('spc_Permission_from_Patient_to_Share_PHI__c', 'Yes');
        persistedAccountMap.put('spc_Date_Permission_to_Share_PHI__c', String.valueOf(System.today()));
        //persistedAccountMap.put('caregiverLookUpId',account.id);
        persistedAccountMap.put('notes', 'NA');
        persistedAccountMap.put('PatientConnect__PC_Primary_Address__c', addr.id);

        persistedAddressMap.put('PatientConnect__PC_Address_1__c', 'Address1');
        persistedAddressMap.put('PatientConnect__PC_Address_2__c', 'address 2');
        persistedAddressMap.put('PatientConnect__PC_Address_3__c', 'address 4');
        persistedAddressMap.put('addressType', 'Home');
        persistedAddressMap.put('PatientConnect__PC_City__c', 'City');
        persistedAddressMap.put('PatientConnect__PC_Country__c', 'Couuntry');
        persistedAddressMap.put('PatientConnect__PC_State__c', 'State');
        persistedAddressMap.put('status', 'Active');
        persistedAddressMap.put('PatientConnect__PC_Zip_Code__c', '123987');
        persistedAddressMap.put('PatientConnect__PC_State__c', 'State');
        persistedAddressMap.put('PatientConnect__PC_State__c', 'State');


        pageState.put('Account', persistedAccountMap);
        pageState.put('caregiverAccount', persistedAccountMap);
        pageState.put('Address', persistedAddressMap);

        spc_CaregiverInformationProcessor.processEnrollment(enrollmentCase, pageState);
        System.assert(persistedAccountMap.size() > 0);
        System.assert(persistedAddressMap.size() > 0);
        test.stopTest();
    }
}
/********************************************************************************************************
*  @author          Deloitte
*  @description     Test class for CCL_Therapy_Assoc_To_Site_Assoc class.
*  @param           
*  @date            Jan 27, 2021
*********************************************************************************************************/
@isTest
public class CCL_Therapy_Assoc_To_Site_Assoc_Test {
    
    @testSetup
    public static void DataCreation() {
        
        Account siteRecord = CCL_TestDataFactory.createTestParentAccount();
        siteRecord.CCL_External_ID__c='123456';
        insert siteRecord;
        
        CCL_Therapy__c TherapyRecord = CCL_TestDataFactory.createTestTherapy();
        insert TherapyRecord;
        
        CCL_Therapy_Association__c therapyAssocRec = CCL_TestDataFactory.createTestSiteTherapyAssoc(siteRecord.Id,TherapyRecord.Id);
        insert therapyAssocRec;
        
        CCL_Therapy_Association__c therapyAssocRecQueried = [select id,CCL_Unique_Site_Therapy__c from CCL_Therapy_Association__c Limit 1];
        string externalId=therapyAssocRecQueried.CCL_Unique_Site_Therapy__c;
        
        CCL_Site_Association__c siteAssocRec= new CCL_Site_Association__c();
        siteAssocRec.Name='Test Site Association Record';
        siteAssocRec.CCL_Parent_Site__c=siteRecord.Id;
        siteAssocRec.CCL_Child_Site__c=siteRecord.Id;
        siteAssocRec.CCL_Association_Type__c='Infusion Site';
        siteAssocRec.CCL_Site_Qualification__c='Qualified';
        siteAssocRec.CCL_External_ID__c='12341234';
        siteAssocRec.CCL_SAP_Code__c='5678';
        insert siteAssocRec;
        
        CCL_Therapy_To_Site_Association__c therapyToSiteAssociation;
        therapyToSiteAssociation = new CCL_Therapy_To_Site_Association__c();
        therapyToSiteAssociation.CCL_Site_Assocation__c = siteAssocRec.Id;
        therapyToSiteAssociation.CCL_Therapy_Association__c = therapyAssocRec.Id;
        therapyToSiteAssociation.CCL_Site__c = siteAssocRec.CCL_Parent_Site__c;
        therapyToSiteAssociation.CCL_Active__c = true;
        therapyToSiteAssociation.CCL_External_ID__c = externalId + siteRecord.CCL_External_ID__c + siteAssocRec.CCL_External_ID__c;
        system.debug('External Id In Test: '+therapyToSiteAssociation.CCL_External_ID__c);
        insert therapyToSiteAssociation;
        
  }
    
    static testmethod void createSiteAssocToTherapyAssocTest() {
        
        List<Id> therapyAssocIdList = new List<Id>();
        CCL_Therapy_Assoc_To_Site_Assoc CCL_Therapy_Assoc_To_Site_Assoc_Object = new CCL_Therapy_Assoc_To_Site_Assoc();
        
        CCL_Therapy_Association__c therapyAssocRec = [select id,CCL_Unique_Site_Therapy__c from CCL_Therapy_Association__c Limit 1];
        system.debug('Therapy Assoc Record in Test: '+therapyAssocRec);
        
        Account siteRecord= [Select Id,CCL_External_ID__c from Account where CCL_External_ID__c='123456'];
        system.debug('siteRecord In Test: '+siteRecord);
        
        CCL_Site_Association__c siteAssociationRecord = [select CCL_External_ID__c from CCL_Site_Association__c where CCL_External_ID__c='12341234'];
        system.debug('siteAssociationRecord In Test: '+siteAssociationRecord);
        
        string formedExternalId=therapyAssocRec.CCL_Unique_Site_Therapy__c+siteRecord.CCL_External_ID__c+siteAssociationRecord.CCL_External_ID__c;
        system.debug('ExternalId formed in test for System Assert: '+formedExternalId);
        
        CCL_Therapy_To_Site_Association__c therapyToSiteAssocRec = [select Id,CCL_External_ID__c from CCL_Therapy_To_Site_Association__c where CCL_External_ID__c=:formedExternalId];
        system.debug('therapyToSiteAssocRec In Test: '+therapyToSiteAssocRec);
        
        system.assertEquals(formedExternalId, therapyToSiteAssocRec.CCL_External_ID__c);
        
        therapyAssocIdList.add(therapyAssocRec.Id);
        CCL_Therapy_Assoc_To_Site_Assoc.createSiteAssocToTherapyAssoc(therapyAssocIdList);
        
        
    }
    
    

}
@isTest public class OutboundSMSCalloutTest {

    @isTest
    static void testOutboundSMSCallout(){

       	//get the custom settings for the constants
        User thisUser = [ Select Id from User where Id = :UserInfo.getUserId() ];
       	Outbound_SMS_Settings__c smsSettings = Outbound_SMS_Settings__c.getOrgDefaults();
        
        System.runAs( thisUser ){
             
            if(smsSettings == null) {
       		 	System.debug('SMS settings is null');
            	smsSettings = new Outbound_SMS_Settings__c();
            }
            smsSettings.Channel__c = 'SMS';
            smsSettings.Direction__c = 'Outbound';
            smsSettings.Webservice_URL__c = 'https://api.us.via.aspect-cloud.net/via/v2/organizations/sagerx/campaign/strategies/5/records';
            smsSettings.ApiKey__c = 'aspect-api-001';
            insert smsSettings;
        }

        
        System.debug('Channel value = '+smsSettings.Channel__c);
        
        Contact newContact = new Contact(LastName='Smith');
        insert newContact;
        
		List<Contact> programCases = [select id from Contact];
        System.debug('Number of Contact returned = '+programCases.size());
        
    	//create new task
        Task newTask = new Task();
        newTask.WhoId = programCases.get(0).Id;
        //newTask.WhatId = programCases.get(0).Id;
        newTask.Subject = smsSettings.Task_Subject__c;
        newTask.Status = 'Completed';
        newTask.PatientConnect__PC_Channel__c = smsSettings.Channel__c;
        newTask.PatientConnect__PC_Direction__c = smsSettings.Direction__c;
        newTask.Priority = 'Normal';
        newTask.Type = 'Other';
        newTask.ActivityDate = Date.today();     

        Test.startTest();

        try{
            insert newTask;                          
            System.debug(Logginglevel.DEBUG, 'Successfully created task with id ['+newTask.Id+']');
        }catch(DMLException dmle){
            System.debug(Logginglevel.ERROR, 'Caught exception inserting task ==> '+dmle);
        }
        
        //create and populate OutboundSMSDataObject
        OutboundSMSDataObject smsObject = new OutboundSMSDataObject();
        smsObject.phoneNumber = '12012529100';
        smsObject.taskID = '12345';
        smsObject.firstName = 'Mike';
        smsObject.smsTextTemplate = 'Test Template';
        smsObject.zipCode = '07077';
        smsObject.templateName = 'Name of Template';     
        

        System.debug(Logginglevel.DEBUG, 'About to make callout');
        OutboundSMSCallout.makeCallout(smsObject.toJSONString());
        
        Test.stopTest();
                    
    }
}
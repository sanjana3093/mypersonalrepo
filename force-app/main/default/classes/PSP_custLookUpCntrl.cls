/*********************************************************************************************************
class Name      : PSP_custLookUpCntrl 
Description		: Controller for custom lookup
@author		    : Deloitte
@date       	: July 31, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Deloitte            July 31, 2019          Initial Version
****************************************************************************************************************/ 
public with sharing class PSP_custLookUpCntrl {
    /* Constant created for CareProgramEnrolleeProduct Object name */
    final Static String CARE_PROG_OBJ = 'CareProgramEnrolleeProduct';
    /* care plan template */
    final Static String CRE_PLN_TMPLT='HealthCloudGA__CarePlanTemplate__c';
    /*Card and Coupons*/
    final Static String CARD_COUPON='PSP_Card_and_Coupon__c';  
     /*status*/
    final Static String AVAILABLE_STAUS=PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.PICKLIST_VAL_AVAILABLE);
    /*cardtype*/
    final Static String CARD_TYPE=PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.PICKLIST_VAL_PHYSICAL);
	/* Private constructor to prevent instantiation 
	* */
    private PSP_custLookUpCntrl() {
        // Empty constructor as all methods in class are static
    }
    
    /********************************************************************************************************
	*  @author          Deloitte
	*  @description     method for fetching lookup values
	*  @param1          recordType
	*  @param2          searchKeyword
	*  @param3          objectName
	*  @date            Aug 12, 2019
	*********************************************************************************************************/
    @AuraEnabled
    public static List <sObject> fetchLookupValues(String recordType,String searchKeyword,String objectName) {
        List < sObject > searchRecords = new List < sObject > ();
        try {
            if (String.isNotBlank(recordType) || recordType!=PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.PICKLIST_VAL_NONE) ) {
                final String[] recordTypes = recordType.split('#');
                String recordTypeName='';
                integer counter = 0;
                for (String rt : recordTypes) {
                    if (counter == recordTypes.size() - 1) {
                        recordTypeName = rt;
                    } else {
                        recordTypeName = rt +',';
                    }
                }
                final String nameFilter = '%' + searchKeyword + '%';
                if (String.isNotBlank(recordType) && objectName.equals(CARE_PROG_OBJ)) {
                    searchRecords = Database.query ('Select Id, Name, Product.RecordType.Name,Product.Name from CareProgramProduct  where Product.RecordType.Name = \'' + String.escapeSingleQuotes(recordTypeName) + '\' and Name LIKE \'' + String.escapeSingleQuotes(nameFilter)+ '\' and PSP_Active__c = true WITH SECURITY_ENFORCED');
                } else if (String.isNotBlank(recordType) && !objectName.equals(CARD_COUPON)) {
                    searchRecords = Database.query ('select id, Name from ' + String.escapeSingleQuotes(objectName) + ' where Name LIKE \'' + String.escapeSingleQuotes(nameFilter) + '\' and RecordType.Name = \'' + String.escapeSingleQuotes(recordTypeName) + '\'  WITH SECURITY_ENFORCED order by CreatedDate DESC limit 5');
                } else if(objectName.equals(CRE_PLN_TMPLT)) {
                      searchRecords = Database.query ('select id, Name from ' + String.escapeSingleQuotes(objectName) + ' where Name LIKE \'' + String.escapeSingleQuotes(nameFilter) + '\' AND HealthCloudGA__Active__c=true  WITH SECURITY_ENFORCED order by CreatedDate DESC limit 5');
                } else if(objectName.equals(CARD_COUPON) && recordType=='Card') {
                      searchRecords = Database.query ('select id, Name,PSP_Card_Type__c from ' + String.escapeSingleQuotes(objectName) + ' where Name LIKE \'' + String.escapeSingleQuotes(nameFilter) +'\' and RecordType.Name = \'' + String.escapeSingleQuotes(recordTypeName) + '\' AND PSP_Status__c=\''+String.escapeSingleQuotes(AVAILABLE_STAUS)+'\' AND PSP_Card_Type__c=\''+String.escapeSingleQuotes(CARD_TYPE)+'\'  WITH SECURITY_ENFORCED order by CreatedDate DESC limit 5');
                } 
                else if(objectName.equals(CARD_COUPON) && recordType=='Coupon') {
                      searchRecords = Database.query ('select id, Name,PSP_Card_Type__c from ' + String.escapeSingleQuotes(objectName) + ' where Name LIKE \'' + String.escapeSingleQuotes(nameFilter) +'\' and RecordType.Name = \'' + String.escapeSingleQuotes(recordTypeName) + '\' AND PSP_Status__c=\''+String.escapeSingleQuotes(AVAILABLE_STAUS)+'\'  WITH SECURITY_ENFORCED order by CreatedDate DESC limit 5');
                }   
                else {
                    searchRecords = Database.query ('select id, Name from ' + String.escapeSingleQuotes(objectName) + ' where Name LIKE \'' + String.escapeSingleQuotes(nameFilter) + '\'  WITH SECURITY_ENFORCED order by CreatedDate DESC limit 5');
                }
            }
            
        } catch(AuraHandledException ex) {
            System.debug(ex.getMessage());
            throw ex;
        }
        return searchRecords;
    }
    
    
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     get product List method 
    *  @param           record Id
    *  @date            Aug 12, 2019
    *********************************************************************************************************/   
    @AuraEnabled(cacheable = true)
    public static List < CareProgramEnrollee > getEnrolledPrograms(Id recordId) {
        final List < CareProgramEnrollee > enrolledPrograms = new List < CareProgramEnrollee > ();
        try {
            List<CareProgramEnrollee> cProgEnrollees = new List<CareProgramEnrollee>();
            if (CareProgramEnrollee.sObjectType.getDescribe().isAccessible()) {
                cProgEnrollees = [Select Id, Name,PSP_Program_Enrollee__c,PSP_Card_Number__c,PSP_Card_Number__r.PSP_Deactivation_Date__c,PSP_Card_Number__r.Name,CareProgramEnrollee.Account.Name,CareProgramId from CareProgramEnrollee where AccountId =: recordId 
                                 OR PSP_Enrolled_Physician__c=:recordId WITH SECURITY_ENFORCED order by createddate desc];
            }    
            enrolledPrograms.addAll(cProgEnrollees);
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
        return enrolledPrograms;
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     get access of records 
    *  @param           record Id
    *  @date            Aug 12, 2019
    *********************************************************************************************************/
    @AuraEnabled(cacheable = true)
    public static List < UserRecordAccess > getAccessAccount(Id recordId) {
        system.debug('the rcord id'+recordId);
        List<UserRecordAccess> userRecords= new List<UserRecordAccess>();
        Boolean isAllowedToCrAcc=false;
        if(Profile.sObjectType.getDescribe().isAccessible()) {
            final List<Profile> profile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
            try {
                if (PermissionSetAssignment.sObjectType.getDescribe().isAccessible()) {  
                    for (PermissionSetAssignment permSet: [SELECT Id, PermissionSet.Name,AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :Userinfo.getUserId() ]) {
                       if(String.isNotBlank(permSet.PermissionSet.Name) && (permSet.PermissionSet.Name.equals(PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.OP_SUPP_FOR_HEALTH_CLOUD_PER_SET_NAM))
                          || permSet.PermissionSet.Name.equals(PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.PATIENT_ENG_PARTNR_PERM_SET)))) {
                            isAllowedToCrAcc=true;
                        }
                    }
                }
                if (UserRecordAccess.sObjectType.getDescribe().isAccessible()) {
                    userRecords=[SELECT RecordId FROM UserRecordAccess WHERE UserId =: UserInfo.getUserId() AND RecordId = :recordId    AND HasEditAccess = true ];
                }
            } catch(AuraHandledException e) {
                System.debug(e.getMessage());
                throw e;
            }
            if(!isAllowedToCrAcc && !profile[0].Name.equals(PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.PROFILE_NAME_ADMIN))) {
               userRecords= new List<UserRecordAccess>();
            }
        }
        return userRecords; 
        
    }
    
    
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     get prescription List method 
    *  @param           record Id
    *  @param           enrolee Id
    *  @param			 recordType
    *  @date            Oct 12, 2019
    *********************************************************************************************************/  
    @AuraEnabled
    public static List < CareProgramEnrolleeProduct  > getEnrolledPrescriptions(Id recordId,Id enroleeId,String recordType) {
        //enrolled prescriptions
        final List < CareProgramEnrolleeProduct  > enrlldPrscrptns = new List < CareProgramEnrolleeProduct  > ();
        try {
            List<CareProgramEnrolleeProduct> cPrgEnrolleePrd = new List<CareProgramEnrolleeProduct>();
            if (CareProgramEnrolleeProduct.sObjectType.getDescribe().isAccessible()) {
                cPrgEnrolleePrd = [select Id, Name,PSP_Maximum_Dosage__c,PSP_Maximum_Dosage_Frequency__c,PSP_Prescriber__r.Name,PSP_Card_Number__c,PSP_Prescription_Quantity__c,PSP_Units__c,PSP_Prescription_Frequency__c,CareProgramEnrollee.Name,CareProgramProduct.Name,CareProgramProvider.Name,PSP_Indication__c from CareProgramEnrolleeProduct where  CareProgramEnrolleeId=:enroleeId and PSP_Patient__c=:recordId 
                                        and Status=:PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.PICKLIST_VAL_ASSIGNED)and CareProgramProduct.Product.Recordtype.Name=:recordType WITH SECURITY_ENFORCED order by createddate desc];
            }    
            enrlldPrscrptns.addAll(cPrgEnrolleePrd);
            
            return enrlldPrscrptns;
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
    }
    
     /********************************************************************************************************
    *  @author          Deloitte
    *  @description     get careprogramProducts List method 
    *  @param          careprogramId
    *  @param			 recordType
    *  @date            Nov 05, 2019
    *********************************************************************************************************/  
    @AuraEnabled
    public static List < CareProgramProduct> getCareProgramProducts(Id careprogramId,String recordType) {
        final List < CareProgramProduct  > productService = new List < CareProgramProduct  > ();
        try {
            List<CareProgramProduct> cProService = new List<CareProgramProduct>();
            if (CareProgramProduct.sObjectType.getDescribe().isAccessible()) {
                cProService = [select Id, Name,Product.Name from CareProgramProduct  where  CareProgramId=:careprogramId  
                                        and  PSP_Active__c = true and Product.Recordtype.Name=:recordType WITH SECURITY_ENFORCED order by name asc];
            }    
            productService.addAll(cProService);
            
            return productService;
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
    }
    
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     get careprogram List method 
    *  @param           program Type
    *  @date            Dec 6, 2019
    *********************************************************************************************************/  
    @AuraEnabled
    public static List < CareProgram> getCarePrograms(String programType,String programType1) {
        final List<String> programTypes = new List<String>();
        programTypes.add(programType);
        if(String.isNotBlank(programType1)){
            programTypes.add(programType1);
        }
        final List < CareProgram  > cProducts = new List < CareProgram  > ();
        try {
            List<CareProgram> cPro = new List<CareProgram>();
            if (CareProgram.sObjectType.getDescribe().isAccessible()) {
                cPro = [select Id, Name from CareProgram  where  Status= 'Active' 
                                        and  PSP_Program_Type__c IN: programTypes WITH SECURITY_ENFORCED order by name asc];
            }    
            
            cProducts.addAll(cPro);
            return cProducts;
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
    }
    
    
     /********************************************************************************************************
    *  @author          Deloitte
    *  @description     get careprogramProviders List method 
    *  @param          careprogramId
    *  @param			 recordType
    *  @date            Nov 05, 2019
    *********************************************************************************************************/  
    @AuraEnabled
    public static List < CareProgramProvider> getCareProgramProviders(Id proServiceId) {
        final List < CareProgramProvider  > providerslst = new List < CareProgramProvider  > ();
        try {
            List<CareProgramProvider> cProvider = new List<CareProgramProvider>();
            if (CareProgramProvider.sObjectType.getDescribe().isAccessible()) {
                cProvider = [select Id, Name,Role,Account.Name from CareProgramProvider  where  CareProgramProductId=:proServiceId  
                                        WITH SECURITY_ENFORCED order by createddate desc];
            }    
            providerslst.addAll(cProvider);
            
            return providerslst;
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     get deactivatedCard List method 
    *  @param          cardandcouponId
    *  @date            Nov 26, 2019
    *********************************************************************************************************/  
    @AuraEnabled
    public static List <PSP_Card_and_Coupon__c> getDeactivatedCardList(Id assignedToID) {
        final List <PSP_Card_and_Coupon__c> cardList = new List <PSP_Card_and_Coupon__c> ();
        try {
            List<PSP_Card_and_Coupon__c> cList = new List<PSP_Card_and_Coupon__c>();
            if (PSP_Card_and_Coupon__c.sObjectType.getDescribe().isAccessible()) {
                cList = [select Id, PSP_Status__c, Name,PSP_Assigned_to__c from PSP_Card_and_Coupon__c  where PSP_RecordTyeName__c='Card' AND   
                                        PSP_Assigned_to__c=:assignedToID AND PSP_Status__c='Deactivated' WITH SECURITY_ENFORCED order by createddate desc];
            }    
            cardList.addAll(cList);
            
           return cardList;
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
         
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     get deactivatedCard List method 
    *  @param          cardandcouponId
    *  @date            Nov 26, 2019
    *********************************************************************************************************/  
    @AuraEnabled
    public static String getHandedOutByName(String handedOutById) {
        //final List <Contact> conList = new List <Contact> ();
        String conName='';
        try {
            List<Contact> conList = new List<Contact>();
            if (Contact.sObjectType.getDescribe().isAccessible()) {
                conList = [select Id, Name from Contact  where Id=:handedOutById];
            }
            for(Contact c : conList) {
                conName = c.Name;
            }   
            return conName;
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
    }
}
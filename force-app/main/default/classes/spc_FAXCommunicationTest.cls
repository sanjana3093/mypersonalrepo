/**
* @author Deloitte
* @date 06/18/2018
*
* @description This is the Test Class for spc_FAXCommunication
*/

@isTest
public class spc_FAXCommunicationTest {
    @testSetup static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        Account manf = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId());
        insert manf;
        PatientConnect__PC_Engagement_Program__c eg = spc_Test_Setup.createEngagementProgram('test EP', manf.id);
        insert eg;
        Account pharmacy = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Pharmacy').getRecordTypeId());
        pharmacy.PatientConnect__PC_Email__c = 'testpharmacy@email.com';
        pharmacy.fax = '1234567890';
        insert pharmacy;
        Account patientAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        patientAcc1.PatientConnect__PC_Email__c = 'test@email.com';
        patientAcc1.spc_HIPAA_Consent_Received__c = 'yes';
        patientAcc1.spc_Patient_HIPAA_Consent_Date__c = system.today();
        patientAcc1.spc_Patient_Services_Consent_Received__c = 'yes';
        patientAcc1.spc_Patient_Service_Consent_Date__c = system.today();
        patientAcc1.spc_Text_Consent__c = 'yes';
        patientAcc1.spc_Patient_Text_Consent_Date__c = system.today();
        patientAcc1.spc_Patient_Mrkt_and_Srvc_consent__c = 'yes';
        patientAcc1.spc_Patient_Marketing_Consent_Date__c = system.today();
        insert patientAcc1;
        Case programCaseRec2 = spc_Test_Setup.createCases(new List<Account> {patientAcc1}, 1, 'PC_Program').get(0);
        if (null != programCaseRec2) {
            programCaseRec2.Type = 'Program';
            programCaseRec2.PatientConnect__PC_Engagement_Program__c = eg.id;
            insert programCaseRec2;
            PatientConnect__PC_eLetter__c eLetter = new PatientConnect__PC_eLetter__c();
            eLetter.name = 'Patient Enrollment Information to SOC/SPP FAX';
            eLetter.PatientConnect__PC_Template_Name__c = '2010720184708224700_Form';
            eLetter.PatientConnect__PC_Target_Recipients__c = 'HCO;Specialty Pharmacy';
            eLetter.PatientConnect__PC_Source_Object__c = 'Case';
            eLetter.PatientConnect__PC_Communication_Language__c = 'english';
            eletter.spc_eLetter_Data_Migration_Id__c = '12345678' ;
            insert eLetter;
            PatientConnect__PC_Engagement_Program_Eletter__c engELetter = new PatientConnect__PC_Engagement_Program_Eletter__c();
            engELetter.spc_Channel__c = 'fax';
            engELetter.spc_PMRC_Code__c = 'PMRCF05';
            engELetter.PatientConnect__PC_Engagement_Program__c = eg.id;
            engELetter.PatientConnect__PC_eLetter__c = eLetter.id;
            insert engELetter;

            //eletter 2
            PatientConnect__PC_eLetter__c eLetter1 = new PatientConnect__PC_eLetter__c();
            eLetter1.name = 'Receipt To HCP -- Fax';
            eLetter1.PatientConnect__PC_Template_Name__c = '2010720184708224700_Form';
            eLetter1.PatientConnect__PC_Target_Recipients__c = 'Treating Physician';
            eLetter1.PatientConnect__PC_Source_Object__c = 'Case';
            eLetter1.PatientConnect__PC_Communication_Language__c = 'english';
            eLetter1.spc_eLetter_Data_Migration_Id__c = '12345679' ;
            insert eLetter1;
            PatientConnect__PC_Engagement_Program_Eletter__c engELetter1 = new PatientConnect__PC_Engagement_Program_Eletter__c();
            engELetter1.spc_Channel__c = 'fax';
            engELetter1.spc_PMRC_Code__c = 'PMRCF08';
            engELetter1.PatientConnect__PC_Engagement_Program__c = eg.id;
            engELetter1.PatientConnect__PC_eLetter__c = eLetter1.id;
            insert engELetter1;

            spc_Communication_framework_Parameters__c emailParams = new spc_Communication_framework_Parameters__c();
            emailParams.Lash_Fax_Number__c = '1234567890';
            emailParams.Org_Wide_Email_Address_Name__c = 'Jane Doe';
            insert emailParams;

            PatientConnect__PC_Document__c oDoc = spc_Test_Setup.createDocument('Fax - Inbound', 'Review Needed', eg.id);
            insert oDoc;
            system.assertNotEquals(oDoc, null);

            Attachment attach = new Attachment();
            attach.Name = 'xyz';
            attach.ParentId = oDoc.id;
            attach.body = blob.valueOf('testAttachment');
            insert attach;
        }


    }
    public static testmethod void testGetPicklistEntryMap() {
        Map<String, PatientConnect__PC_Engagement_Program_Eletter__c> mapeLetter = new Map<String, PatientConnect__PC_Engagement_Program_Eletter__c>();
        PatientConnect__PC_Engagement_Program_Eletter__c eLetter1 = [select id, spc_PMRC_Code__c, PatientConnect__PC_eLetter__r.Name, PatientConnect__PC_eLetter__c, PatientConnect__PC_eLetter__r.PatientConnect__PC_Template_Name__c from PatientConnect__PC_Engagement_Program_Eletter__c  where spc_PMRC_Code__c = 'PMRCF05' LIMIT 1];
        PatientConnect__PC_Engagement_Program_Eletter__c eLetter2 = [select id, spc_PMRC_Code__c, PatientConnect__PC_eLetter__r.Name, PatientConnect__PC_eLetter__c, PatientConnect__PC_eLetter__r.PatientConnect__PC_Template_Name__c from PatientConnect__PC_Engagement_Program_Eletter__c where spc_PMRC_Code__c = 'PMRCF08' LIMIT 1];

        mapeLetter.put('Patient Enrollment Information to SOC/SPP FAX|FAX', eLetter1);
        mapeLetter.put('spc_Send_Receipt_to_HCP|FAX', eLetter2);
        Case oCase = [select id, status, PatientConnect__PC_Status_Indicator_3__c from Case];
        oCase.status = 'enrolled';
        oCase.PatientConnect__PC_Status_Indicator_3__c = 'complete';
        update oCase;
        Account acc = [select id, fax, recordtypeid, recordtype.developername from Account where recordtypeid = :Schema.SObjectType.Account.getRecordTypeInfosByName().get('Pharmacy').getRecordTypeId() LIMIT 1];
        List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();
        spc_CommunicationTriggers triggerImplementation = new spc_CommunicationTriggers();
        envelops.add(triggerImplementation.createEnvelop(oCase.Id
                     , null
                     , 'Patient Enrollment Information to SOC/SPP FAX'
                     , new Set<String> {spc_ApexConstants.ASSOCIATION_ROLE_TREATING_PHY, spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO), 'Lash', spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_SPHARMACY)}
                     , new Set<Id> {acc.Id}
                     , spc_CommunicationMgr.Channel.Fax));
        envelops[0].outboundDoc = [SELECT Id FROM PatientConnect__PC_Document__c limit 1];
        Attachment att = [select id from Attachment LIMIT 1];
        set<String> setAttach = new set<string>();
        setAttach.add(att.id);
        envelops[0].attachmentIds = setAttach;
        Map<String, Account> mapRecipients1 = new Map<String, Account>();
        Map<String, Account> mapRecipients2 = new Map<String, Account>();
        mapRecipients1.put(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_SPHARMACY), acc);
        mapRecipients2.put(spc_ApexConstants.LASH, acc);
        envelops[0].additionalAccountMap = mapRecipients1;

        Map<String, Map<String, Account>> mapRecipients = new Map<String, Map<String, Account>>();
        mapRecipients.put(oCase.id, mapRecipients1);

        Contact con = new Contact();

        con.accountid = acc.id;
        con.lastName = 'LastCon';
        insert con;
        Map<Id, Contact> mapAccountCon = new Map<Id, Contact>();
        Map<Id, List<sObject>> mapIdObject=new Map<Id, List<sObject>>();
        mapAccountCon.put(acc.id,con);
        PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c();
        doc.PatientConnect__PC_From_Email_Address__c = Label.spc_testemail_from_address;
        insert doc;
        List<Attachment> attList=new List<Attachment>();
        Attachment attachment = new Attachment();
        attachment.Name = Label.spc_attachment_body;
        attachment.Body = Blob.valueOf(Label.spc_attachment_body);
        attachment.ParentId = doc.Id;
        attachment.ContentType = 'text/plain';
        insert attachment;
        attList.add(attachment);
        mapIdObject.put(eLetter1.id,attList);
        Test.startTest();
        spc_FAXCommunication spcFax=new spc_FAXCommunication();
        spcFax.send(envelops,mapeLetter,mapRecipients,mapAccountCon,mapIdObject);
        mapRecipients.put(oCase.id,mapRecipients2);
        spcFax.send(envelops,mapeLetter,mapRecipients,mapAccountCon,mapIdObject);
        Test.stopTest();

    }
}
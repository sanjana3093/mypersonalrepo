/**

    *  @author          Deloitte
    *  @description     SOC details from enrollment case
    *  @date            4-Jan-2017
    *  @version         1.0

 */
public with sharing class spc_SOCEWPController {
    private static List<spc_PicklistFieldManager.PicklistEntryWrapper> accTypeLabels = spc_PicklistFieldManager.getPicklistEntryWrappers(Account.Type);
    private static List<spc_PicklistFieldManager.PicklistEntryWrapper> accSubTypeLabels = spc_PicklistFieldManager.getPicklistEntryWrappers(Account.PatientConnect__PC_Sub_Type__c);
    private static String accTypeLabel;
    private static String accSubTypeLabel;

    private static string HCOType_Clinic = 'Clinic';
    private static string HCOType_PrivatePractice = 'Private Practice';
    private static string HCOType_Hospital = 'Hospital';
    private static string HCOType_LongTermCare = 'Long Term Care Facility';

    /********************************************************************************************************
    *  @author           Deloitte
    *  @date             29/06/2018
    *  @description      Implemented method to get fields from Account
    *  @return           HCO Record
    *********************************************************************************************************/
    @AuraEnabled
    public static HCORecord getFields() {
        //Method to get labels of all the fields
        HCORecord displayFields = new HCORecord();

        displayFields.type = Account.Type.getDescribe().getLabel();
        displayFields.recordTypeId = Account.RecordTypeId.getDescribe().getLabel();
        displayFields.name = Account.Name.getDescribe().getLabel();
        displayFields.recordTypeName = Account.RecordType.Name.getDescribe().getLabel();
        displayFields.subType = Account.PatientConnect__PC_Sub_Type__c.getDescribe().getLabel();

        displayFields.addressId = Account.PatientConnect__PC_Primary_Address__c.getDescribe().getLabel();
        displayFields.primaryAddress1 = Account.PatientConnect__PC_Primary_Address_1__c.getDescribe().getLabel();
        displayFields.primaryAddress2 = Account.PatientConnect__PC_Primary_Address_2__c.getDescribe().getLabel();
        displayFields.primaryAddress3 = Account.PatientConnect__PC_Primary_Address_3__c.getDescribe().getLabel();
        displayFields.primaryAddress = Account.PatientConnect__PC_Primary_Address__c.getDescribe().getLabel();

        displayFields.primaryCity = Account.PatientConnect__PC_Primary_City__c.getDescribe().getLabel();
        displayFields.primaryState = Account.PatientConnect__PC_Primary_State__c.getDescribe().getLabel();
        displayFields.primaryZipCode = Account.PatientConnect__PC_Primary_Zip_Code__c.getDescribe().getLabel();

        displayFields.email = Account.PatientConnect__PC_Email__c.getDescribe().getLabel();
        displayFields.phone = Account.Phone.getDescribe().getLabel();
        displayFields.fax = Account.Fax.getDescribe().getLabel();
        displayFields.remsID = Account.spc_REMS_ID__c.getDescribe().getLabel();
        displayFields.status = Account.PatientConnect__PC_Status__c.getDescribe().getLabel();
        displayFields.lastModifiedDate = Account.LastModifiedDate.getDescribe().getLabel();

        return displayFields;
    }

    /********************************************************************************************************
    *  @author           Deloitte
    *  @date             29/06/2018
    *  @description      Implemented method to get HCO Types
    *  @return           Account
    *********************************************************************************************************/
    @AuraEnabled
    public static Account getHCOTypes2() {
        Account acc = new Account();
        Id devRecordTypeId = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        acc.RecordTypeId = devRecordTypeId;
        acc.Type = acc.Type;
        return acc;
    }
    /********************************************************************************************************
    *  @author           Deloitte
    *  @date             29/06/2018
    *  @description      method to search records of SOC based on search string and filter criteria recieved from EW
    *  @return           List of HCO Records
    *********************************************************************************************************/
    @AuraEnabled
    public static List<HCORecord> searchRecords(String searchString, String searchFilter) {
        try {
            List<HCORecord> records = new List<HCORecord>();
            String recordType = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_HCO);
            List<Account> accts = getAccounts(recordType, searchString, searchFilter);
            HCORecord record;

            for (Account a : accts) {
                for (spc_PicklistFieldManager.PicklistEntryWrapper acctl : accTypeLabels) {
                    if (acctl.value == a.Type) {
                        a.Type = acctl.label;
                    }
                }
                for (spc_PicklistFieldManager.PicklistEntryWrapper accstl : accSubTypeLabels) {
                    if (accstl.value == a.PatientConnect__PC_Sub_Type__c) {
                        a.PatientConnect__PC_Sub_Type__c = accstl.label;
                    }
                }
                record = new HCORecord(a, 'false');
                records.add(record);
            }
            return records;
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
            return null;
        }
    }

    /********************************************************************************************************
    *  @author           Deloitte
    *  @date             29/06/2018
    *  @description      get all the Account details based on search string
    *  @return           List of Accounts
    *********************************************************************************************************/
    public static List<Account> getAccounts(String recordType, String newSearchString, String searchFilter) {
        String soslSearchString = '*' + String.escapeSingleQuotes(newSearchString) + '*';
        /*
        * FLS Check for Account
        */
        List<String> fields = new List<String> {'Name', 'Phone', 'Fax', 'Type', 'PatientConnect__PC_Primary_Address__c',
                                                'PatientConnect__PC_Primary_Address_1__c', 'PatientConnect__PC_Primary_Address_2__c',
                                                'PatientConnect__PC_Primary_Address_3__c', 'PatientConnect__PC_Primary_Address__c',
                                                'PatientConnect__PC_Primary_City__c', 'PatientConnect__PC_Primary_State__c',
                                                'PatientConnect__PC_Primary_Zip_Code__c', 'PatientConnect__PC_Status__c',
                                                'PatientConnect__PC_Email__c', 'PatientConnect__PC_Communication_Language__c',
                                                'PatientConnect__PC_Preferred__c', 'spc_REMS_ID__c', 'PatientConnect__PC_Sub_Type__c'
                                               };
        spc_Database.assertAccess('Account', spc_Database.Operation.Reading, fields);

        if (recordType != null && recordType != '') {
            if (newSearchString != null && newSearchString != '') {
                if (searchFilter != null && searchFilter != '') {
                    List<List<SObject>> soslQueryResults = [FIND :soslSearchString IN ALL FIELDS
                                                            RETURNING Account (Id,  Name, Type, RecordTypeId, RecordType.Name,
                                                                    PatientConnect__PC_Primary_Address_1__c,
                                                                    PatientConnect__PC_Primary_Address_2__c, PatientConnect__PC_Primary_Address_3__c, PatientConnect__PC_Primary_Address__c,
                                                                    PatientConnect__PC_Primary_City__c, PatientConnect__PC_Primary_State__c, PatientConnect__PC_Primary_Zip_Code__c,
                                                                    PatientConnect__PC_Status__c, PatientConnect__PC_Email__c, Phone, Fax, PatientConnect__PC_Communication_Language__c,
                                                                    PatientConnect__PC_Preferred__c, PatientConnect__PC_Sub_Type__c, spc_REMS_ID__c, LastModifiedDate
                                                                    WHERE Account.RecordType.Name = :recordType
                                                                            AND Type = :searchFilter)];
                    return (List<Account>) soslQueryResults[0];
                } else {
                    List<List<SObject>> soslQueryResults = [FIND :soslSearchString IN ALL FIELDS
                                                            RETURNING Account (Id,  Name, Type, RecordTypeId, RecordType.Name,
                                                                    PatientConnect__PC_Primary_Address_1__c, PatientConnect__PC_Primary_Address__c,
                                                                    PatientConnect__PC_Primary_Address_2__c, PatientConnect__PC_Primary_Address_3__c,
                                                                    PatientConnect__PC_Primary_City__c, PatientConnect__PC_Primary_State__c, PatientConnect__PC_Primary_Zip_Code__c,
                                                                    PatientConnect__PC_Status__c,
                                                                    PatientConnect__PC_Email__c, Phone, Fax,
                                                                    PatientConnect__PC_Communication_Language__c, spc_REMS_ID__c, PatientConnect__PC_Preferred__c,
                                                                    PatientConnect__PC_Sub_Type__c,
                                                                    LastModifiedDate
                                                                    WHERE Account.RecordType.Name = :recordType)];
                    return (List<Account>) soslQueryResults[0];
                }
            }
        }
        return null;
    }


    /********************************************************************************************************
    *  @author           Deloitte
    *  @date             29/06/2018
    *  @description      To search for HCO records using Account Ids
    *  @return           List of HCO Records
    *********************************************************************************************************/

    @AuraEnabled
    public static List<HCORecord> searchRecordsByIds(List<String> accountIds) {
        try {
            List<HCORecord> records = new List<HCORecord>();

            /*
            * FLS Check for Account
            */
            List<String> fields = new List<String> {'Name', 'Phone', 'Fax', 'Type',
                                                    spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Primary_Address_1__c'), spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Primary_Address_2__c'),
                                                    spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Primary_Address_3__c'), spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Primary_Address__c'),
                                                    spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Primary_City__c'), spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Primary_State__c'),
                                                    spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Primary_Zip_Code__c'), spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Status__c'),
                                                    spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Email__c'), spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Communication_Language__c'),
                                                    spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Preferred__c'), spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Sub_Type__c')
                                                   };
            spc_Database.assertAccess('Account', spc_Database.Operation.Reading, fields);

            //Todo change to SOSL.
            List<Account> accts = [SELECT
                                   Id,  Name, toLabel(Type), RecordTypeId, RecordType.Name,
                                   PatientConnect__PC_Primary_Address_1__c,
                                   PatientConnect__PC_Primary_Address_2__c, PatientConnect__PC_Primary_Address_3__c, PatientConnect__PC_Primary_Address__c,
                                   PatientConnect__PC_Primary_City__c, PatientConnect__PC_Primary_State__c, PatientConnect__PC_Primary_Zip_Code__c,
                                   PatientConnect__PC_Status__c, spc_REMS_ID__c,
                                   PatientConnect__PC_Email__c, Phone, Fax,
                                   PatientConnect__PC_Communication_Language__c, PatientConnect__PC_Preferred__c,
                                   toLabel(PatientConnect__PC_Sub_Type__c),
                                   LastModifiedDate
                                   FROM Account
                                   WHERE ID IN :accountIds
                                  ];
            HCORecord record;

            for (Account a : accts) {
                record = new HCORecord(a, 'true');
                records.add(record);
            }
            return records;
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
            return null;
        }
    }

    /********************************************************************************************************
     *  @author           Deloitte
     *  @date             29/06/2018
     *  @description      To get HCO Types Picklist wrapper
     *  @return           Map of Picklist values
     *********************************************************************************************************/
    @AuraEnabled
    public static Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> getHCOTypes () {
        /*Changes for PC-1885*/
        Set<String> types = new Set<String>();
        Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> picklistEntryMap =
            new Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>>();

        types.add(HCOType_Clinic);
        types.add(HCOType_PrivatePractice);
        types.add(HCOType_Hospital);
        types.add(HCOType_LongTermCare);

        List<spc_PicklistFieldManager.PicklistEntryWrapper> applicableTypeWrapper = new List<spc_PicklistFieldManager.PicklistEntryWrapper>();

        for (Schema.PicklistEntry picklistEntry : Account.Type.getDescribe().getPicklistValues()) {
            if (types.contains(picklistEntry.getValue())) {
                applicableTypeWrapper.add(new spc_PicklistFieldManager.PicklistEntryWrapper(picklistEntry.getValue(), picklistEntry.getLabel()));
            }
        }

        picklistEntryMap.put('Type', applicableTypeWrapper);
        return picklistEntryMap;
    }
    /********************************************************************************************************
    *  @author           Deloitte
    *  @date             06/07/2018
    *  @description      This method is used for getting picklist values
    *  @param            None
    *  @return           Map<String, List<PicklistEntryWrapper>>
    *********************************************************************************************************/
    @AuraEnabled
    public static Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> getPicklistEntryMap() {
        Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> picklistEntryMap =
            new Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>>();

        picklistEntryMap.put('socType', spc_PicklistFieldManager.getPicklistEntryWrappers(PatientConnect__PC_Interaction__c.spc_SOC_Type__c));
        picklistEntryMap.put('hip', spc_PicklistFieldManager.getPicklistEntryWrappers(PatientConnect__PC_Interaction__c.spc_HIP__c));
        return picklistEntryMap;
    }

    /*********************************************************************************
     *  @author           Deloitte
     *  @date             06/07/2018
     *  @description      Returns the applicant's HCO associated with the enrollment case for the active applicant ID provided.
     *  @param            The Id of the enrollment case for the applicant to retrieve, Active applicant ID
     *  @return           HCORecord
    *********************************************************************************/
    @AuraEnabled
    public static List<HCORecord> getApplicantHCO(Id enrollmentCaseId, Id activeApplicantId) {
        List<HCORecord> records = new List<HCORecord>();

        try {
            List<String> fields = new List<String> {'PatientConnect__PC_HCO_Name__c', 'PatientConnect__PC_HCO_Type__c', 'PatientConnect__PC_HCO_Phone__c',
                                                    'PatientConnect__PC_HCO_Fax__c', 'PatientConnect__PC_HCO_Street_Address1__c', 'PatientConnect__PC_HCO_City__c',
                                                    'PatientConnect__PC_HCO_State__c',
                                                    'PatientConnect__PC_HCO1_Name__c', 'PatientConnect__PC_HCO1_Type__c', 'PatientConnect__PC_HCO1_Phone__c',
                                                    'PatientConnect__PC_HCO1_Fax__c', 'PatientConnect__PC_HCO1_Street_Address1__c', 'PatientConnect__PC_HCO1_City__c',
                                                    'PatientConnect__PC_HCO1_State__c',
                                                    'PatientConnect__PC_HCO2_Name__c', 'PatientConnect__PC_HCO2_Type__c', 'PatientConnect__PC_HCO2_Phone__c',
                                                    'PatientConnect__PC_HCO2_Fax__c', 'PatientConnect__PC_HCO2_Street_Address1__c', 'PatientConnect__PC_HCO2_City__c',
                                                    'PatientConnect__PC_HCO2_State__c'
                                                   };

            //FLS check access before SOQL
            spc_Database.assertAccess(spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Applicant__c'), spc_Database.Operation.Reading, fields);

            //get a list of HCOs from online applicants for the enrollment case
            List<PatientConnect__PC_Applicant__c> lstapplicant = [SELECT Id, RecordType.Name,
                                                  PatientConnect__PC_HCO_Name__c, PatientConnect__PC_HCO_Phone__c, PatientConnect__PC_HCO_Fax__c, PatientConnect__PC_HCO_Street_Address1__c, PatientConnect__PC_HCO_City__c, PatientConnect__PC_HCO_State__c, toLabel(PatientConnect__PC_HCO_Type__c),
                                                  PatientConnect__PC_HCO1_Name__c, toLabel(PatientConnect__PC_HCO1_Type__c), PatientConnect__PC_HCO1_Phone__c, PatientConnect__PC_HCO1_Fax__c, PatientConnect__PC_HCO1_Street_Address1__c, PatientConnect__PC_HCO1_City__c, PatientConnect__PC_HCO1_State__c,
                                                  PatientConnect__PC_HCO2_Name__c, toLabel(PatientConnect__PC_HCO2_Type__c), PatientConnect__PC_HCO2_Phone__c, PatientConnect__PC_HCO2_Fax__c, PatientConnect__PC_HCO2_Street_Address1__c, PatientConnect__PC_HCO2_City__c, PatientConnect__PC_HCO2_State__c
                                                  FROM PatientConnect__PC_Applicant__c
                                                  WHERE PatientConnect__PC_Applicant__c.RecordType.Name = :spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.APPLICANT_ONLINE_RECORD_TYPE_NAME)
                                                          and
                                                          Id = : activeApplicantId LIMIT 1];
            HCORecord applicantHCO;
            if (lstapplicant != null && !lstapplicant.isEmpty()) {
                String fieldPrefix;
                for (integer i = 0; i <= 2; i++) {
                    applicantHCO = new HCORecord();
                    fieldPrefix = (i == 0) ? 'PatientConnect__PC_HCO' : 'PatientConnect__PC_HCO' + i;
                    applicantHCO.name = (String)lstapplicant[0].get(fieldPrefix + '_Name__c');
                    applicantHCO.phone = (String)lstapplicant[0].get(fieldPrefix + '_Phone__c');
                    applicantHCO.fax = (String)lstapplicant[0].get(fieldPrefix + '_Fax__c');
                    applicantHCO.primaryAddress1 = (String)lstapplicant[0].get(fieldPrefix + '_Street_Address1__c');
                    applicantHCO.primaryCity = (String)lstapplicant[0].get(fieldPrefix + '_City__c');
                    applicantHCO.primaryState = (String)lstapplicant[0].get(fieldPrefix + '_State__c');
                    applicantHCO.type = (String)lstapplicant[0].get(fieldPrefix + '_Type__c');
                    if (applicantHCO.name != null || applicantHCO.phone != null || applicantHCO.fax != null
                            || applicantHCO.primaryAddress1 != null || applicantHCO.primaryCity != null || applicantHCO.primaryState != null
                            || applicantHCO.type != null) {
                        records.add(applicantHCO);
                    }
                }
            }
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }
        return records;
    }

    /**

    *  @author          Deloitte
    *  @description     HCO Details from Account
    *  @date            4-Jan-2017
    *  @version         1.0

    */
    public class HCORecord {
        @AuraEnabled public String Id { get; set; }
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public String type { get; set; }
        @AuraEnabled public String subType { get; set; }
        @AuraEnabled public String recordTypeId { get; set; }
        @AuraEnabled public String recordTypeName { get; set; }
        @AuraEnabled public String addressId { get; set; }
        @AuraEnabled public String primaryAddress1 { get; set; }
        @AuraEnabled public String primaryAddress2 { get; set; }
        @AuraEnabled public String primaryAddress3 { get; set; }
        @AuraEnabled public String primaryAddress { get; set; }
        @AuraEnabled public String primaryCity { get; set; }
        @AuraEnabled public String primaryState { get; set; }
        @AuraEnabled public String primaryZipCode { get; set; }
        @AuraEnabled public String assnPhone { get; set; }
        @AuraEnabled public String assnFax { get; set; }
        @AuraEnabled public String remsID { get; set; }
        @AuraEnabled public String email { get; set; }
        @AuraEnabled public String phone { get; set; }
        @AuraEnabled public String fax { get; set; }

        @AuraEnabled public String isSelected { get; set; }
        @AuraEnabled public String status { get; set; }
        @AuraEnabled public String lastModifiedDate { get; set; }
        @AuraEnabled public Datetime lastModifiedDateInUserTZ { get; set; }

        @AuraEnabled public String cssClassName { get; set; } // used in front end.

        public HCORecord() {

        }

        public HCORecord(Account a, String isSelect) {
            Id = a.Id;
            type = a.Type;
            recordTypeId = a.RecordTypeId;
            name = a.Name;
            recordTypeName = a.RecordType.Name;
            subType = a.PatientConnect__PC_Sub_Type__c;
            addressId = a.PatientConnect__PC_Primary_Address__c;
            primaryAddress1 = a.PatientConnect__PC_Primary_Address_1__c;
            primaryAddress2 = a.PatientConnect__PC_Primary_Address_2__c;
            primaryAddress3 = a.PatientConnect__PC_Primary_Address_3__c;
            primaryAddress = a.PatientConnect__PC_Primary_Address__c;
            primaryCity = a.PatientConnect__PC_Primary_City__c;
            primaryState = a.PatientConnect__PC_Primary_State__c;
            primaryZipCode = a.PatientConnect__PC_Primary_Zip_Code__c;
            assnPhone = '';
            assnFax = '';
            email = a.PatientConnect__PC_Email__c;
            phone = a.Phone;
            fax = a.Fax;
            remsID = a.spc_REMS_ID__c;
            isSelected = isSelect;
            status = a.PatientConnect__PC_Status__c;
            lastModifiedDateInUserTZ = a.LastModifiedDate;

            cssClassName = '';
        }

    }

    /**

    *  @author          Deloitte
    *  @description     To filter HCO Search
    *  @date            4-Jan-2017
    *  @version         1.0

    */
    public class HCOSearchFilter {
        @AuraEnabled public String Id { get; set; }
        public HCOSearchFilter() {

        }
    }
    /********************************************************************************************************
    *  @author           Deloitte
    *  @date             11/06/2018
    *  @description      This method is used for getting addreses
    *  @param            physicianId Id
    *  @return           List<PatientConnect__PC_Address__c>
    *********************************************************************************************************/
    @AuraEnabled
    public static List<PatientConnect__PC_Address__c> getSocAddresses(Id hcoid) {
        List<PatientConnect__PC_Address__c> addresses;
        if (String.isNotBlank(hcoid)) {
            addresses = new List<PatientConnect__PC_Address__c>([Select Id, PatientConnect__PC_Address_1__c, PatientConnect__PC_Address_2__c, PatientConnect__PC_Address_3__c,
                    PatientConnect__PC_City__c, PatientConnect__PC_State__c, spc_Phone__c, spc_Phone_2__c, spc_Fax__c, spc_Fax_2__c, PatientConnect__PC_Country__c, PatientConnect__PC_Address_Description__c,
                    PatientConnect__PC_Zip_Code__c, PatientConnect__PC_Primary_Address__c from PatientConnect__PC_Address__c where PatientConnect__PC_Account__c = :hcoid]);
        }
        return addresses;
    }
}
/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Processor class for Physician Information Enrollment Wizard Page Processor
    *  @date            19-June-2018
    *  @version         1.0
    *
    *********************************************************************************************************/
global with sharing class spc_PhysicianEWPProcessor implements PatientConnect.PC_EnrollmentWizard.PageProcessor {

    /********************************************************************************************************
     *  @author           Deloitte
     *  @date             29/06/2018
     *  @description      Implemented method from PC_EnrollmentWizard.PageProcessor
     *  @param            enrollmentCase - Case Object
     *  @param            pageState - Map with field values from page
     *  @return           None
     *********************************************************************************************************/
    public static void processEnrollment(Case enrollmentCase, Map<String, Object> pageState) {

        if (pageState != null ) {
            for (String key : pageState.keySet()) {
                if (key == 'selectedResult') {

                    ID programId = enrollmentCase.PatientConnect__PC_Program__c;
                    if (programId != null) {

                        Map<String, Object> selectedPhysician = (Map<String, Object>) pageState.get(key);

                        if (selectedPhysician != null && selectedPhysician.get('Id') != null) {
                            Case caseProgram = new Case(Id = programId, PatientConnect__PC_Physician__c = (Id) selectedPhysician.get('Id') );
                            spc_ApexConstants.IsEWPRunning = true;
                            spc_Database.upd(caseProgram);
                        }

                    } else {
                        throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException('Physician Processor: Program Id is not found');
                    }

                }
            }
        } else {
            throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException('Missing page state');
        }
    }
}
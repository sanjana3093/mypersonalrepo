/*********************************************************************************************************
    * This is The PC class cloned by Sage for Reference only
    ************************************************************************************************************/
/*********************************************************************************
Class Name      : PatientConnect__PC_eLetterAuraController
Description     : Methods for eLetter service
Created By      : Roxana Ivan
Created Date    : 16-June-16
Modification Log:
----------------------------------------------------------------------------------
Developer                   Date                   Description
-----------------------------------------------------------------------------------
Roxana Ivan               16-June-16              Initial Version
Shabda Bhujad               Dec 08, 2016            Added FLS check to loadEletter,getParticipants methods
*********************************************************************************/
public with sharing class spc_SMSeLetterService {

    private Set<Id> patientAccountIds = new Set<Id>();
    private Map<Id, Id> patientToObjectId = new Map<Id, Id>();
    private String engProgramId ;
    private String pmrcCode ;


    /*********************************************************************************
    Method Name    : getELetter
    Description    : Based on the eLetterId get a ELetter
    Return Type    : PatientConnect__PC_eLetter__c
    Parameter      : 1. eLetterId : Id of the ELetter
    *********************************************************************************/
    public static PatientConnect__PC_eLetter__c getELetter(Id eLetterId) {
        List<String> fields = new List<String> {
            'Name',
            'PatientConnect__PC_Target_Recipients__c',
            'PatientConnect__PC_Communication_Language__c',
            'PatientConnect__PC_Allow_Send_to_Patient__c',
            'PatientConnect__PC_Send_to_User__c',
            'PatientConnect__PC_Template_Name__c'
        };
        spc_database.assertAccess('PatientConnect__PC_eLetter__c', spc_database.Operation.Reading, fields);

        List<PatientConnect__PC_eLetter__c> eLetters = [
                    SELECT Id, Name, PatientConnect__PC_Target_Recipients__c, PatientConnect__PC_Communication_Language__c, PatientConnect__PC_Template_Name__c,
                    PatientConnect__PC_Object_Record_Types__c, PatientConnect__PC_Allow_Send_to_Patient__c, PatientConnect__PC_Send_to_User__c FROM PatientConnect__PC_eLetter__c
                    WHERE Id = :eLetterId LIMIT 1
                ];
        if (!eLetters.isEmpty()) {
            return eLetters[0];
        }
        return null;
    }

    /*********************************************************************************
    Method Name    : getParticipants
    Description    : Based on the eLetterName get the relevant Participants for patient(s) for a single patient
    Return Type    : void
    Parameter      : 1. caseId : Id of the Patient
    2. eLetter: eLetter record details
    *********************************************************************************/
    public static List<spc_SMSeLetter_Recipient> getParticipants(Id caseId, PatientConnect__PC_eLetter__c eLetter) {

        Map<Id, List<spc_SMSeLetter_Recipient>> recipients = getParticipants(new Set<Id> { caseId }, eLetter);
        if (recipients == null || recipients.size() == 0) {
            return null;
        }
        return recipients.values().get(0);
    }

    /*********************************************************************************
    Method Name    : getParticipants
    Description    : Based on the eLetterName get the relevant Participants for patient(s)
    Return Type    : void
    Parameter      : 1. caseIds : Number of Patients for whom the related recipients are required
    2. eLetter: eLetter record details
    *********************************************************************************/
    public static Map<Id, List<spc_SMSeLetter_Recipient>> getParticipants(Set<Id> caseIds, PatientConnect__PC_eLetter__c eLetter) {

        // Get available languages from the eLetter Template and dynamically display the recipient language:
        Set<String> languages = new Set<String> ();
        Set<String> cchannels = new Set<String> ();
        languages.addAll(eLetter.PatientConnect__PC_Communication_Language__c.split(';'));

        Set<Id> programIds = new Set<Id>();
        Set<Id> engProgramIds = new Set<Id>();

        try {
            //add either to program or profram case
            List<String> fields = new List<String> {'PatientConnect__PC_Program__c', 'PatientConnect__PC_Is_Program_Case__c'};
            spc_database.assertAccess('Case', spc_database.Operation.Reading, fields);
            for (Case objCase : [SELECT Id, PatientConnect__PC_Program__c, PatientConnect__PC_Engagement_Program__c, PatientConnect__PC_Is_Program_Case__c FROM Case where Id in :caseIds]) {
                Id programID = objCase.PatientConnect__PC_Is_Program_Case__c ? objCase.Id : objCase.PatientConnect__PC_Program__c;
                if (!programIds.contains(programID)) {
                    programIds.add(programID);
                    engProgramIds.add(objCase.PatientConnect__PC_Engagement_Program__c);
                }
            }

            if (!engProgramIds.isEmpty()) {
                Map<ID, Set<String>> allMapLinks = spc_SMSeLetterService.getMapBetweenEletterAndCchannels(engProgramIds);
                if (!allMapLinks.isEmpty()) {
                    cchannels = allMapLinks.get(eLetter.ID);
                }
            }

            fields = new List<String> {'PatientConnect__PC_Account__c', 'PatientConnect__PC_Program__c'};
            spc_database.assertAccess('PatientConnect__PC_Association__c', spc_database.Operation.Reading, fields);
            // Fetch all the Participants related to the patient (ACTIVE ones only) and Physician location details (as for a physician the location is explicitly selected)
            Map<Id, List<spc_SMSeLetter_Recipient>> recipients = new Map<Id, List<spc_SMSeLetter_Recipient>> ();
            if (eLetter.PatientConnect__PC_Target_Recipients__c != null) {
                List<PatientConnect__PC_Association__c> patientParticipants =
                    [SELECT Id, PatientConnect__PC_Account__c, PatientConnect__PC_Account__r.PatientConnect__PC_Communication_Language__c, PatientConnect__PC_Account__r.Type, PatientConnect__PC_Account__r.Name, PatientConnect__PC_Account__r.Phone,
                     PatientConnect__PC_Program__c, PatientConnect__PC_Role__c
                     FROM PatientConnect__PC_Association__c WHERE PatientConnect__PC_Program__c in :programIds AND PatientConnect__PC_Role__c IN :eLetter.PatientConnect__PC_Target_Recipients__c.split(';')];

                for (PatientConnect__PC_Association__c pp : patientParticipants) {
                    Account recipientAccount = new Account(
                        Id = pp.PatientConnect__PC_Account__c,
                        PatientConnect__PC_Communication_Language__c = pp.PatientConnect__PC_Account__r.PatientConnect__PC_Communication_Language__c,
                        Phone = pp.PatientConnect__PC_Account__r.Phone,
                        Name =  pp.PatientConnect__PC_Account__r.Name,
                        Type = pp.PatientConnect__PC_Role__c);
                    // Create the new recipient record with given parameters
                    spc_SMSeLetter_Recipient recipient = new spc_SMSeLetter_Recipient(recipientAccount, pp.PatientConnect__PC_Role__c, languages, cchannels);

                    if (!recipients.containsKey(pp.PatientConnect__PC_Program__c)) {
                        recipients.put(pp.PatientConnect__PC_Program__c, new List<spc_SMSeLetter_Recipient>());
                    }

                    // Add the recipient wrapper to the list of recipients
                    recipients.get(pp.PatientConnect__PC_Program__c).add(recipient);
                }
            }
            if (eLetter.PatientConnect__PC_Allow_Send_to_Patient__c) {
                fields = new List<String> {'Type', 'PatientConnect__PC_Communication_Language__c', 'PatientConnect__PC_Email__c', 'Name'};
                spc_database.assertAccess('Account', spc_database.Operation.Reading, fields);
                for (Case objCase : [SELECT Id, AccountId, Account.PatientConnect__PC_Communication_Language__c, Account.Type, Account.spc_Mobile_Phone__c, Account.Name FROM Case WHERE Id IN :programIds]) {
                    Account recipientAccount = new Account(
                        Id = objCase.AccountId,
                        PatientConnect__PC_Communication_Language__c = objCase.Account.PatientConnect__PC_Communication_Language__c,
                        Type = objCase.Account.Type,
                        Phone = objCase.Account.spc_Mobile_Phone__c,
                        Name = objCase.Account.Name);
                    // Create the new recipient record with given parameters
                    spc_SMSeLetter_Recipient recipient = new spc_SMSeLetter_Recipient(recipientAccount, recipientAccount.Type, languages, cchannels);

                    if (!recipients.containsKey(objCase.Id)) {
                        recipients.put(objCase.Id, new List<spc_SMSeLetter_Recipient>());
                    }
                    // Add the recipient wrapper to the list of recipients
                    recipients.get(objCase.Id).add(recipient);
                }
            }
            return recipients;
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
            return null;
        }
    }

    /*********************************************************************************
    Method Name    : getRelatedParticipants
    Description    : Based on the eLetterName get the relevant Participants for patient(s)
    Return Type    : void
    Parameter      : 1. caseIds : Number of Patients for whom the related recipients are required
    2. eLetter: eLetter record details
    3. loadedParticipants : Has some value only when invoked from eLetter page where recipients and their language are dyanmically selected at page level. So it won't be applicable for all related recipients
    *********************************************************************************/
    public Map<Id, List<Account>> getRelatedRecipients(List<Account> recipients) {
        // Fetch the eLetter details: The types of Participants to which the eLetters can be sent

        Map<Id, List<Account>> patientToRecipients = new Map<Id, List<Account>> ();
        //if loadedParticipants is not null ; i.e the list has been overwritten, and can be added directly for the Patient (single patient from eLetter page)
        if (recipients != null) {
            for (Id patient : patientAccountIds) {
                patientToRecipients.put(patient, recipients);
            }
        }

        return patientToRecipients;
    }


    /**
    * Method to be used at eLetterManagement page, where recipient's language can differ based on the selection
    * at page level and the communication method can be fax/email
    *
    * @param objectId      Id for which the eLetter has to be sent - Program Case Id
    * @param eLetter       eLetter record
    * @param communicationMethod   Fax or Email
    * @param recipients    Has some value only when invoked from eLetter page where recipients and their language
    *                      are dyanmically selected at page level. So it won't be applicable for all related recipients.
    * @param sendToUser    True, if the eLetter should be send to the current user.
    */
    public static void sendLetter(Id objectId, PatientConnect__PC_eLetter__c eLetter, String communicationMethod, List<Account> recipients, boolean sendToUser) {
        spc_SMSeLetterService service = new spc_SMSeLetterService();
        service.sendLetter(objectId, eLetter, recipients, spc_ApexConstants.ID_EMAIL_OUTBOUND_RECORDTYPE, sendToUser);
    }

    private void sendLetter(Id objectId, PatientConnect__PC_eLetter__c eLetter, List<Account> recipients, Id docRecordTypeId, boolean sendToUser) {
        loadTargetInformation(objectId);
        //Added as a part of new recordtype
        //added 1/30
        loadPMRCAndEProId(eLetter.Id);
        //Use "engProgramId" and "pmrcCode" for Engagment Program Id and PMRC code. added 1/30
        Map<Id, List<Account>> patientToRecipients = getRelatedRecipients(recipients);
        // Create document records for the patients' recipients & get the respective document ids (for attaching the merged templates to these documents)
        // As Aspect will be creating doc, doc log, task Deloitte commenting this method call US-326547
        //List<spc_eLetter.DocumentWrapper> documents = createDocuments(patientToRecipients, eLetter, docRecordTypeId,objectId);
        /*'ASPECT Integration will Take data from here'*/


        //get the case object with the ID
        Case theCase = [SELECT Id, ContactId, Account.Name, Account.PatientConnect__PC_First_Name__c, Account.PatientConnect__PC_Primary_Zip_Code__c, Account.Phone FROM Case WHERE Id = :objectId];
        //Creating Outbound Data Object
        OutboundSMSDataObject smsObj = new OutboundSMSDataObject();
        Outbound_SMS_Settings__c smsSettings = Outbound_SMS_Settings__c.getOrgDefaults();
        String searchTemplates = smsSettings.Appointment_Templates__c;
        if (String.isEmpty(searchTemplates)) searchTemplates = 'sample';
        //Logic check if Appointment reminder was the selected templated to populate the infusion date
        try {

            if (searchTemplates.contains(eLetter.PatientConnect__PC_Template_Name__c)) {
                PatientConnect__PC_Association__c careTeam = [SELECT Id, PatientConnect__PC_Role__c, Name, PatientConnect__PC_AssociationStatus__c, PatientConnect__PC_Program__c, PatientConnect__PC_Account__c from PatientConnect__PC_Association__c where PatientConnect__PC_AssociationStatus__c = 'Active' AND PatientConnect__PC_Role__c = 'HCO' AND PatientConnect__PC_Program__c = :objectId];
                if (careTeam != null) {
                    PatientConnect__PC_Interaction__c interactionObj = [Select id, spc_Scheduled_Date__c from PatientConnect__PC_Interaction__c where PatientConnect__PC_Participant__c = : careTeam.PatientConnect__PC_Account__c AND PatientConnect__PC_Patient_Program__c = :objectId AND spc_Scheduled_Date__c > Today ORDER BY spc_Scheduled_Date__c];
                    if (interactionObj != null)  {smsObj.infusionDate = interactionObj.spc_Scheduled_Date__c;}
                    else {
                        System.debug(Logginglevel.ERROR, 'Cannot send SMS, no schedule infusion date recorded');
                    }
                } //end care team if

            } // end if
        } catch (QueryException e) {
            System.assert(String.isNotBlank(e.getMessage()), 'Cannot send SMS, no schedule infusion date recorded or all are in the past');
            System.debug(Logginglevel.ERROR, e.getMessage());
            return;
        }

        //create task with helper method
        //Task newTask = OutboundSMSForCase.createTaskwithRecipient(theCase, eLetter.PatientConnect__PC_Template_Name__c, rcpt.Name);

        //Loop logic to go through list of receipents to send SMS
        for (Account rcpt : recipients ) {
            //create and populate OutboundSMSDataObject
            //OutboundSMSDataObject smsObj = new OutboundSMSDataObject();
            //smsObj.phoneNumber = theCase.Account.Phone;
            Task newTask = OutboundSMSForCase.createTaskwithRecipient(theCase, eLetter.Name, rcpt.Name);
            smsObj.phoneNumber = rcpt.Phone.replaceAll('\\D', '');
            smsObj.zipCode = rcpt.PatientConnect__PC_Primary_Zip_Code__c;
            smsObj.firstName = rcpt.Name.substringAfter(',').trim(); //PatientConnect__PC_First_Name__c;
            smsObj.taskID = newTask.Id;
            smsObj.templateName = eLetter.PatientConnect__PC_Template_Name__c;

            if (smsObj.isValidRequest()) {
                OutboundSMSCallout.makeCallout(smsObj.toJSONString());
            } else {
                System.debug(Logginglevel.ERROR, 'Will not make SMS callout because required field is missing');
            }
        }
        if (sendToUser) {
            User currUser = spc_SMSeLetterService.sendToMe();
        }
    }


    /**
    * Method to collect the patient ids from the recipients
    *
    * @param objectId Id for which the eLetter has to be sent
    */
    private void loadTargetInformation(Id objectId) {
        SObjectType sObjectType = objectId.getSobjectType();

        if (sObjectType == Case.sObjectType) {
            Case programCase = [SELECT Id, AccountId FROM Case WHERE Id = :objectId];
            patientAccountIds.add(programCase.AccountId);
            patientToObjectId.put(programCase.AccountId, programCase.Id);
        }
    }

    // Added on 1/30 to set Engagment Program Id and PMRC code for Eletter
    private void loadPMRCAndEProId(Id eletterID) {
        List<PatientConnect__PC_Engagement_Program_Eletter__c> eletterEngPrograms = [ SELECT ID, PatientConnect__PC_eLetter__c, spc_PMRC_Code__c, PatientConnect__PC_Engagement_Program__c
                FROM PatientConnect__PC_Engagement_Program_Eletter__c
                WHERE PatientConnect__PC_eLetter__c = : EletterID AND spc_Channel__c = 'SMS' Limit 1];
        if (!eletterEngPrograms.isEmpty()) {
            engProgramId = eletterEngPrograms[0].PatientConnect__PC_Engagement_Program__c  ;
            pmrcCode =eletterEngPrograms[0].spc_PMRC_Code__c;
        }
    }
    /*********************************************************************************
    Method Name    : getMapBetweenEletterAndCchannels
    Developer      : Pratik Raj
    Description    : create Document Logs fro Docs
    Return Type    :  List<PatientConnect__PC_Document_Log__c>
    *********************************************************************************/
    public static Map<ID, Set<String>>  getMapBetweenEletterAndCchannels(Set<ID> engProId) {

        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(spc_ApexConstants.ENG_ELETTER_FORM_FIELD_SET);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        Map<ID, Set<String>> cChannels = new Map<ID, Set<String>>();
        for (PatientConnect__PC_Engagement_Program_Eletter__c eletter : [ SELECT ID, PatientConnect__PC_eLetter__c, PatientConnect__PC_Engagement_Program__c
                FROM PatientConnect__PC_Engagement_Program_Eletter__c
                WHERE PatientConnect__PC_Engagement_Program__c IN : engProId AND spc_Channel__c = 'SMS']) {

            if (cChannels.get(eletter.PatientConnect__PC_eLetter__c) == null) {
                Set<String> cchannelList = new Set<String>();
                cchannelList.add(Label.spc_SMS_Template);
                cChannels.put(eletter.PatientConnect__PC_eLetter__c, cchannelList);
            }
        }
        return cChannels;
    }
    /*********************************************************************************
    Method Name    : verifyValidAndActiveTemplate
    Developer      : Pratik Raj
    Description    : verify Valid And Active Template
    Return Type    :  boolean
    *********************************************************************************/
    public static boolean verifyValidAndActiveTemplate(String templateName) {

        List<EmailTemplate> emailTemplate = [SELECT Id, IsActive FROM EmailTemplate WHERE DeveloperName = : templateName and IsActive = true];
        if (!emailTemplate.isEmpty()) {
            return true;
        }
        return false;
    }
    /*********************************************************************************
    Method Name    : verifyValidAndActiveTemplate
    Developer      : Pratik Raj
    Description    : verify Valid And Active Template
    Return Type    :  boolean
    *********************************************************************************/
    public static user sendToMe() {

        List<User> users = [SELECT id, Email, Fax, Phone FROM User WHERE Id = :UserInfo.getUserId()] ;
        if (!users.isEmpty()) {
            return Users[0];
        }
        return null;
    }
}
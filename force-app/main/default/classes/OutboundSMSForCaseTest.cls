@isTest public class OutboundSMSForCaseTest {

    //@testSetup 
    static void setup() {
        
        //need to do this for the triggers to work
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        
    } 
    
    @isTest
    static void testOutboundSMSForCase(){

       	setup();

        //get the custom settings for the constants
        User thisUser = [ Select Id from User where Id = :UserInfo.getUserId() ];
        Outbound_SMS_Settings__c smsSettings = Outbound_SMS_Settings__c.getOrgDefaults();
        
        System.runAs( thisUser ){
            
            if(smsSettings == null) {
                System.debug('SMS settings is null');
                smsSettings = new Outbound_SMS_Settings__c();
            }
            smsSettings.Channel__c = 'SMS';
            smsSettings.Direction__c = 'Outbound';
            smsSettings.Webservice_URL__c = 'https://api.us.via.aspect-cloud.net/via/v2/organizations/sagerx/campaign/strategies/5/records';
            smsSettings.ApiKey__c = 'aspect-api-001';
            insert smsSettings;
        }
        Test.startTest();
        
        //Create account record
        Account acc=spc_Test_Setup.createTestAccount('PC_Patient');
        acc.PatientConnect__PC_Email__c=Label.spc_testemail_from_address;
        acc.PatientConnect__PC_Date_of_Birth__c=Date.valueOf(Label.spc_test_birth_date);
        acc.spc_Text_Consent__c = 'Yes';
        acc.spc_Patient_Text_Consent_Date__c = Date.today();       
        acc.Phone = '18006567800';
        //acc.PatientConnect__PC_Primary_Zip_Code__c = '16910';
        acc.PatientConnect__PC_First_Name__c = 'Mike';      

        insert acc;
        
        //Create Access
        String devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId();
        Case caseProgrm= new Case(AccountId=acc.id, RecordTypeId=devRecordTypeId);
        insert caseProgrm;
  
        List<Case> caseList = new List<Case>();
		caseList.add(caseProgrm);

        Task newTask = OutboundSMSForCase.createTask(caseProgrm, OutboundSMSForCase.TEMPLATE_NAME);
        System.debug(Logginglevel.DEBUG, 'Added new task with Id ['+newTask.Id+']');
        
        OutboundSMSForCase.sendSMSforCase(caseList);

        Test.stopTest();
                    
    }
}
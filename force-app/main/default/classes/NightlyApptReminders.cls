global class NightlyApptReminders implements Schedulable{ 
/*
**   Author: Anthony Williams
**   Purpose: This class is a scheduled job that searches for interactions that have a schedule date between 48 and 24 hours from schedule 
**            start date.  Once the Interactions are found, a Task is created for the Program Case and an SMS with the Appointment Reminder
**            template is sent to Via  
*******************************************************************************************************************************************/

        global void execute(SchedulableContext SC){
            
          try{
             //String templateName = 'Appointment Reminder';
             Outbound_SMS_Settings__c smsSettings = Outbound_SMS_Settings__c.getOrgDefaults();
             String templateName = smsSettings.Appointment_Reminder_Template__c;
             
             List <PatientConnect__PC_Interaction__c> interactionsList = [Select Id, PatientConnect__PC_Patient_Program__c, spc_Scheduled_Date__c, PatientConnect__PC_Patient_Program__r.PatientConnect__PC_Phone__c from PatientConnect__PC_Interaction__c WHERE spc_Scheduled_Date__c > TOMORROW and spc_Scheduled_Date__c <= NEXT_N_DAYS:3 and (PatientConnect__PC_Patient_Program__r.Account.spc_Text_Consent__c = 'Yes' OR PatientConnect__PC_Patient_Program__r.Account.spc_Text_Consent__c = 'Verbal Consent Received')];

             for (PatientConnect__PC_Interaction__c interaction : interactionsList){
                Case theCase = [SELECT Id, Account.Name, Account.PatientConnect__PC_First_Name__c, Account.PatientConnect__PC_Primary_Zip_Code__c, Account.Phone FROM Case WHERE Id = :interaction.PatientConnect__PC_Patient_Program__c];
                if(theCase != null){
                    //create task with helper method
                    Task newTask = OutboundSMSForCase.createTask(theCase, templateName);
                     
                    //create and populate OutboundSMSDataObject
                    OutboundSMSDataObject smsObj = new OutboundSMSDataObject();
                    smsObj.phoneNumber = interaction.PatientConnect__PC_Patient_Program__r.PatientConnect__PC_Phone__c.replaceAll('\\D','');
                    smsObj.firstName = theCase.Account.PatientConnect__PC_First_Name__c;         
                    smsObj.taskID = newTask.Id;
                    smsObj.templateName = templateName;
                    smsObj.infusionDate = interaction.spc_Scheduled_Date__c;
                    System.debug(Logginglevel.DEBUG, smsObj.toString());
                     
                    if(smsObj.isValidRequest()){
                        OutboundSMSCallout.makeCallout(smsObj.toJSONString());                       
                    }else{
                        System.debug(Logginglevel.ERROR, 'Will not make SMS callout because required field is missing');                         
                    }
                }
            }
            
            }//end try
             catch (QueryException e) {
               System.assert(String.isNotBlank(e.getMessage()),'Cannot send SMS, no schedule infusion date recorded or all are in the past');
               System.debug(Logginglevel.ERROR, e.getMessage());
               return;
             }  
        }
 }
/*************************************************************
* @author       Deloitte
* @date         June 09, 2018
* @description  This is the Test class for inbound Emails
*/
@isTest
public class spc_InboundEmailServiceTest {

    @testSetup static void setup() {
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
    }

    @isTest
    static void createDocument() {
        setup();
        test.startTest();

        //Create account record
        Account acc = spc_Test_Setup.createTestAccount('PC_Patient');
        acc.PatientConnect__PC_Email__c = Label.spc_testemail_from_address;
        acc.PatientConnect__PC_Date_of_Birth__c = Date.valueOf(Label.spc_test_birth_date);
        insert acc;

        Case caseInqry = spc_Test_Setup.createInquiryCase(acc.id, null);
        caseInqry.spc_Send_Consideration_Brochure__c = false;
        insert caseInqry;

        caseInqry = [select id, casenumber, RecordTypeId from case where id = : caseInqry.id];
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEmail email2 = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // setup the data for the email
        email.subject = Label.spc_email_subject_with_attachment;
        email.fromAddress = Label.spc_testemail_from_address;
        email.plainTextBody = 'testTestTest';
        email2.subject = caseInqry.CaseNumber;
        email2.fromAddress = Label.spc_testemail_from_address;
        String[] toAddTest = new String[] {};
        toAddTest.add('test@test.ca');
        email.toAddresses = toAddTest ;
        email2.plainTextBody = 'Hello World';
        email2.toAddresses = toAddTest ;
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body =  Label.spc_attachment_body;
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'textfiletwo3.txt';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };

        //Create document record
        PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c();
        doc.PatientConnect__PC_From_Email_Address__c = Label.spc_testemail_from_address;
        insert doc;
        PatientConnect__PC_Document__c doc2 = new PatientConnect__PC_Document__c();
        doc2.PatientConnect__PC_From_Email_Address__c = Label.spc_testemail_from_address;
        insert doc2;

        //Create attachment record
        Attachment attachment = new Attachment();
        attachment.Name = Label.spc_attachment_body;
        attachment.Body = Blob.valueOf(Label.spc_attachment_body);
        attachment.ParentId = doc.Id;
        attachment.ContentType = 'text/plain';
        insert attachment;

        Attachment attachment2 = new Attachment();
        attachment2.Name = Label.spc_attachment_body;
        attachment2.Body = Blob.valueOf(Label.spc_attachment_body);
        attachment2.ParentId = doc2.Id;
        attachment2.ContentType = 'text/plain';
        insert attachment2;


        // call the email service class and test it with the data in the testMethod
        spc_InboundEmailService testInbound = new spc_InboundEmailService();
        testInbound.handleInboundEmail(email, env);
        // call the email service class and test it with the data in the testMethod
        spc_InboundEmailService testInbound2 = new spc_InboundEmailService();
        testInbound2.handleInboundEmail(email2, env);
        //Create document Record
        PatientConnect__PC_Document_Log__c documentLog = new PatientConnect__PC_Document_Log__c();
        documentLog.spc_Inquiry__c = caseInqry.ID;
        documentLog.PatientConnect__PC_Document__c = doc.Id;
        documentLog.PatientConnect__PC_Account__c = acc.Id;
        insert documentLog;
        test.stopTest();

    }

    @isTest
    static void createDocumentwithPDF() {
        setup();
        test.startTest();
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEmail email2 = new Messaging.InboundEmail() ;
        Messaging.InboundEmail email3 = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // setup the data for the emaill
        email.subject = Label.spc_email_subject_with_attachment;
        email.fromAddress = Label.spc_testemail_from_address;
        email.htmlBody = 'test123';


        email2.subject = Label.spc_email_subject_without_attachment;
        email2.fromAddress = Label.spc_testemail_from_address;
        email2.plainTextBody = 'Hello World';
        Messaging.InboundEmail.BinaryAttachment attachmentpdf = new Messaging.InboundEmail.BinaryAttachment();
        attachmentpdf.body = blob.valueOf('my attachment text');
        attachmentpdf.fileName = 'textfileone.txt';
        attachmentpdf.mimeTypeSubType = 'text/plain';

        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachmentpdf };

        String[] toAddTest = new String[] {};
        toAddTest.add('test@test.ca');
        email3.subject = 'test Subject';
        email3.htmlBody = 'test';
        email3.toAddresses = toAddTest;
        email3.fromAddress = 'testSend@test.com';

        email.toAddresses = toAddTest;


        //Create document record
        PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c();
        doc.PatientConnect__PC_From_Email_Address__c = Label.spc_testemail_from_address;
        insert doc;
        PatientConnect__PC_Document__c doc2 = new PatientConnect__PC_Document__c();
        doc2.PatientConnect__PC_From_Email_Address__c = Label.spc_testemail_from_address;
        insert doc2;

        //Create attachment record
        Attachment attachment2 = new Attachment();
        attachment2.ContentType = 'application/pdf';
        attachment2.Name = Label.spc_attachment_body;
        attachment2.Body = Blob.valueOf(Label.spc_attachment_body);
        attachment2.ParentId =  doc.Id;
        insert attachment2;

        //Create account record
        Account acc = spc_Test_Setup.createTestAccount('PC_Patient');
        acc.PatientConnect__PC_Email__c = Label.spc_testemail_from_address;
        acc.PatientConnect__PC_Date_of_Birth__c = Date.valueOf(Label.spc_test_birth_date);
        insert acc;
        //Create Access
        Case caseProgrm = new Case();
        caseProgrm.AccountId = acc.id;
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId();
        caseProgrm.RecordTypeId = devRecordTypeId;
        insert caseProgrm;
        Case caseInqry = new Case();

        caseInqry.AccountId = acc.id;
        Id devRecordTypeId2 = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        caseInqry.RecordTypeId = devRecordTypeId2;
        insert caseInqry;
        // call the email service class and test it with the data in the testMethod
        spc_InboundEmailService testInbound = new spc_InboundEmailService();
        testInbound.handleInboundEmail(email3, env);
        testInbound.handleInboundEmail(email, env);

        PatientConnect__PC_Document_Log__c documentLog = new PatientConnect__PC_Document_Log__c();
        if (caseInqry.recordType.name == 'Inquiry') {
            documentLog.spc_Inquiry__c = caseInqry.ID;
        }
        if (caseProgrm.recordType.name == 'Program') {
            documentLog.PatientConnect__PC_Program__c = caseProgrm.id;
        }
        documentLog.PatientConnect__PC_Document__c = doc.Id;
        documentLog.PatientConnect__PC_Account__c = acc.Id;
        insert documentLog;
        test.stopTest();

    }

    public static testmethod void testExecution1() {
        setup();
        Account acc = spc_Test_Setup.createTestAccount('PC_Patient');
        acc.PatientConnect__PC_Email__c = Label.spc_testemail_from_address;
        acc.PatientConnect__PC_Date_of_Birth__c = Date.valueOf(Label.spc_test_birth_date);
        insert acc;
        PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c();
        doc.PatientConnect__PC_From_Email_Address__c = Label.spc_testemail_from_address;
        insert doc;
        Case caseInqry = new Case();
        caseInqry.AccountId = acc.id;
        Id devRecordTypeId2 = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        caseInqry.RecordTypeId = devRecordTypeId2;
        spc_Database.ins(caseInqry);
        List <Case> lstCase = new List<Case>();
        lstCase.add(caseInqry);
        spc_InboundEmailService spcI = new spc_InboundEmailService();

        test.startTest();
        spcI.createDocLogAndTask(lstCase, doc, 'test Subject');
        spcI.createOrphanDocLogAndTask(doc, 'test Subject');

        test.stoptest();
    }
}
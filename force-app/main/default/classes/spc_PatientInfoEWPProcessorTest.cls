/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for spc_PatientInfoEWPProcessor
    *  @date            06/18/2018
    *  @version         1.0
    *
*********************************************************************************************************/
@isTest
public class spc_PatientInfoEWPProcessorTest {

    @testSetup static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

    }

    /*********************************************************************************
    @description    : Test process enrollment
    @return    : void
    @param     : None
    *********************************************************************************/
    @isTest
    public static void testProcessEnrollment() {
        test.startTest();
        Account account = spc_Test_Setup.createTestAccount('PC_Patient');
        account.PatientConnect__PC_Email__c = 'sr@sr.com';
        account.Phone = '712-123-1413';
        account.spc_Mobile_Phone__c = '712-123-1413';
        insert account;
        Case enrollmentCase = spc_Test_Setup.newCase(account.id, 'PC_Enrollment');
        insert enrollmentCase;
        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');
        insert programCase;
        enrollmentCase.PatientConnect__PC_Program__c = programCase.id;

        Map<String, Object> pageState = new Map<String, Object>();
        Map<String, Object> persistedAccountMap = new Map<String, Object>();

        persistedAccountMap.put('PatientConnect__PC_First_Name__c', 'TestProcessorFirstName');
        persistedAccountMap.put('PatientConnect__PC_Last_Name__c', 'TestProcessorLastName');
        persistedAccountMap.put('email', 'TestProcessorCGFirstName@test.com');
        persistedAccountMap.put('mobilePhone', '712-123-1413');
        persistedAccountMap.put('address1', 'TestProcessorAddress1');
        persistedAccountMap.put('middleName', 'Ron');
        persistedAccountMap.put('homePhone', '712-123-1413');
        persistedAccountMap.put('officePhone', '1712-123-1413');
        persistedAccountMap.put('weight', 68);
        persistedAccountMap.put('patientServicesConsent', 'Yes');
        persistedAccountMap.put('patientServicesConsentDate', String.valueOf(Date.today()));
        persistedAccountMap.put('patientTextConsent', 'Yes');
        persistedAccountMap.put('patientTextConsentDate', String.valueOf(Date.today()));
        persistedAccountMap.put('patientHIPAConsent', 'Yes');
        persistedAccountMap.put('patientHIPAConsentDate', String.valueOf(Date.today()));
        persistedAccountMap.put('remsId', '12345');
        persistedAccountMap.put('remsEnrollmentId', '12345');
        persistedAccountMap.put('lastName', 'John');
        persistedAccountMap.put('firstName', 'Patrik');
        persistedAccountMap.put('childName', 'Jono');
        persistedAccountMap.put('childDOB', String.valueOf(Date.today().addYears(-10)));
        persistedAccountMap.put('dob', String.valueOf(Date.today()));

        List<object> address = new List<object>();
        Map<String, Object> addressInfo = new Map<String, Object>();

        addressInfo.put('address1', 'TestProcessorAddress1');
        addressInfo.put('city', 'TestProcessorCity');
        addressInfo.put('primary', true);
        address.add(addressInfo);
        pageState.put('persistentAccount', persistedAccountMap);
        pageState.put('persistentAddress', address);
        spc_PatientInfoEWPProcessor.processEnrollment(enrollmentCase, pageState);
        System.assert(persistedAccountMap.size() > 0);
        test.stopTest();

    }
}
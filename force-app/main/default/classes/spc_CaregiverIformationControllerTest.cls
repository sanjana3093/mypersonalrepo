/********************************************************************************************************
    *  @author          Deloitte
    *  @description   This is the test class for CaregiverIformationProcessor
    *  @date            06/18/2018
    *  @version         1.0
*********************************************************************************************************/
@isTest
public class spc_CaregiverIformationControllerTest {

  static void setup() {
    List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
  }
  /*********************************************************************************
  Method Name    : testGetPicklistEntryMap
  Description    : Test pocklist entry map
  Return Type    : void
  Parameter      : None
  *********************************************************************************/
  @isTest
  public static void testGetPicklistEntryMap() {
    setup();
    Test.startTest();
    Map<String, List<spc_CaregiverInformationController.PicklistEntryWrapper>> pickLists = spc_CaregiverInformationController.getPicklistEntryMap();
    System.assertEquals(True, pickLists.size() > 0);
    Test.stopTest();

  }
  /*********************************************************************************
  Method Name    : testProcessEnrollment
  Description    : Test process enrollment
  Return Type    : void
  Parameter      : None
  *********************************************************************************/
  @isTest
  public static void testGetCaregiverAccount() {
    setup();
    test.startTest();

    Account account = spc_Test_Setup.createTestAccount('spc_Caregiver');
    account.PatientConnect__PC_Email__c = 'sr@sr.com';
    account.Phone = '1323243242';
    insert account;

    PatientConnect__PC_Address__c address = new PatientConnect__PC_Address__c();
    address.PatientConnect__PC_Address_1__c = '12';
    address.PatientConnect__PC_Address_2__c = 'Brown street';
    address.PatientConnect__PC_City__c = 'New Jersey';
    address.PatientConnect__PC_Account__c = account.id;
    insert address;
    account.PatientConnect__PC_Primary_Address__c = address.id;
    update account;
    spc_CaregiverInformationController.getCaregiverAccountDetails(account.id);
    System.assertEquals('1323243242', account.Phone);
    test.stopTest();

  }

  @isTest
  public static void testGetDependentOptions() {
    setup();
    Test.startTest();
    Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> pickLists = spc_CaregiverInformationController.getDependentOptions();
    System.assertEquals(True, pickLists.size() > 0);
    Test.stopTest();

  }
}
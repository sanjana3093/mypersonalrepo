/*********************************************************************************************************
class Name      : PSP_PurchaseTransaction_Trigger_Impl
Description     : Trigger Implementation Class for Purchase Transaction Object
@author         : Saurabh Tripathi
@date           : September 17, 2019
Modification Log:
--------------------------------------------------------------------------------------------------------------
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------
Saurabh Tripathi            September 17, 2019          Initial Version
****************************************************************************************************************/
public with sharing class PSP_PurchaseTransaction_Trigger_Impl {
    /*Map to define relation between purchase transaction and Product*/
    final Map<String, String> productMap = new Map<String, String>();
    /*Map for price list*/
    final Map<String, String> priceListMap = new Map<String, String>();
    /*Map to define relation between purchase transaction and Product*/
    final Map<String, String> cardMap = new Map<String, String>();
    /*Map to define relation between purchase transaction and Account of account
* type Pharmacy*/
    final Map<String, String> accountMapPharma = new Map<String, String>();
    /*Map to define relation between purchase transaction and Patient-Person
* Account*/
    final Map<String, String> accountMapPatient = new Map<String, String>();
    /*Map to define relation between purchase transaction and HCP(Contact)*/
    final Map<String, String> contactHCPMap = new Map<String, String>();  
    /*List to get the list of Enrolled Prescription and Service */
    
    Map<Id,Product2> products = new Map<Id,Product2>();
    
    List<CareProgramEnrolleeProduct> epsList =
        new List<CareProgramEnrolleeProduct>();
    /*List to get the list of Purchase Transaction */
    List<PSP_Purchase_Transaction__c> ptList =
        new List<PSP_Purchase_Transaction__c>();
    /*List to get the history of Purchase Transaction for a patient */
    
    /*Map to get the data from Purchase Transaction History object */
    Map<String, Datetime> ptHistoryMap = new Map<String, Datetime>();
    /*Map to get the data of Purchase Transaction from Created Date */
    Map<String, PSP_Purchase_Transaction__c> ptCreatedDateMap = new Map<String, PSP_Purchase_Transaction__c>();
    /*List to get the list of Purchase Transactions which are in Purchased status*/
    List<PSP_Purchase_Transaction__c> ptPurchasedList = new List<PSP_Purchase_Transaction__c>();
    /*List to get the list of Purchase Transactions which are in Cancelled or
* Returned status */
    List<PSP_Purchase_Transaction__c> ptReturnedList = new List<PSP_Purchase_Transaction__c>();
    /*List to get the list of Products */
    List<Product2> productList = new List<Product2>();
    /*Set to get the list of unique patient ID */
    Set<String> patiendId = new Set<String>();
    /*Set to get the list of unique Care Program ID */
    Set<String> careProgramProductId = new Set<String>();
    /*Map to get the data from Purchase Transaction object */
    Map<Id, List<PSP_Purchase_Transaction__c>> ptQtyMap = new Map<Id, List<PSP_Purchase_Transaction__c>>();
    /*Map to get the data from Product object */
    Map<Id, Product2> productQtyMap = new Map<Id, Product2>();
    /*Map to get the data from Product and Care Program Product objects */
    Map<Id, List<String>> productCPMap = new Map<Id, List<String>>();
    /*Maps to get the data from Enrolled Prescription and Service object */
    Map<Id, String> epMap = new Map<Id, String>();
    /*Maps to store the data from CareProgramEnrolleeProduct object */
    Map<Id, CareProgramEnrolleeProduct> epsMapData = new Map<Id, CareProgramEnrolleeProduct>();
    /*List to be used for updating Enrolled Prescription and Service*/
    List<CareProgramEnrolleeProduct> epsListUpdate = new List<CareProgramEnrolleeProduct>();
    /*Map to get the data of Prescription Frequency from Enrolled Prescription and
* Service object */
    Map<String, Integer> frequencyMap = new Map<String, Integer>();
    /* string for record type name Product */
    final string PRODUCT_REC_TYP_NAME = PSP_ApexConstants.getValue(
        PSP_ApexConstants.PicklistValue.PICKLIST_VAL_PRODUCT);
    /* string for Daily Frequency */
    final string FREQ_DAILY = PSP_ApexConstants.getValue(
        PSP_ApexConstants.PicklistValue.PICKLIST_VAL_DAILY);
    /* string for Monthly Frequency */
    final string FREQ_MONTHLY = PSP_ApexConstants.getValue(
        PSP_ApexConstants.PicklistValue.PICKLIST_VAL_MONTHLY);
    /* string for Weekly Frequency */
    final string FREQ_WEEKLY = PSP_ApexConstants.getValue(
        PSP_ApexConstants.PicklistValue.PICKLIST_VAL_WEEKLY);
    /* string for Annual Frequency */
    final string FREQ_ANNUAL = PSP_ApexConstants.getValue(
        PSP_ApexConstants.PicklistValue.PICKLIST_VAL_ANNUAL);
    /* string for Assigned status */
    final string STATUS_ASSIGNED_EPS = PSP_ApexConstants.getValue(
        PSP_ApexConstants.PicklistValue.PICKLIST_VAL_ASSIGNED);
    /* string for Purchased status */
    final string STATUS_PURCHASED_PT = PSP_ApexConstants.getValue(
        PSP_ApexConstants.PicklistValue.PICKLIST_VAL_PURCHASED);
    
    /**************************************************************************************
* @author      : Saurabh Tripathi
* @date        : 09/18/2019
* @Description : Method to query and fectch all parent objects data for a
*purchase transaction
* @Param       : Null
* @Return      : Void
***************************************************************************************/
    public void findRelatedList(List<PSP_Purchase_Transaction__c> purchseTrnLst) {
        /*List of EAN */
        final List<Long> eanList = new List<Long>();
        /*List of card number*/
        final List<String> cardNumList = new List<String>();
        /*List of extId*/
        final Set<String> pharmaExtIdList = new Set<String>();
        /*List of PatientRefExtId*/
        final Set<String> patientRefIdList = new Set<String>();
        /*List of PatientRefExtId*/
        final List<String> hcpRefIdList = new List<String>();
        for (PSP_Purchase_Transaction__c purchasTrn : purchseTrnLst) {
            if (String.isNotBlank(purchasTrn.PSP_EAN__c)) {
                eanList.add(Long.valueOf(purchasTrn.PSP_EAN__c));
            }
            if (String.isNotBlank(purchasTrn.PSP_Card_Number__c)) {
                cardNumList.add(purchasTrn.PSP_Card_Number__c);
            }
            if (String.isNotBlank(purchasTrn.PSP_Pharmacy_External_ID__c)) {
                pharmaExtIdList.add(purchasTrn.PSP_Pharmacy_External_ID__c);
            }
            if (String.isNotBlank(purchasTrn.PSP_HCP_Reference_Id__c)) {
                hcpRefIdList.add(purchasTrn.PSP_HCP_Reference_Id__c);
            }
            if (String.isNotBlank(purchasTrn.PSP_Patient_Reference_ID__c)) {
                patientRefIdList.add(purchasTrn.PSP_Patient_Reference_ID__c);
            }
        }
        
        /****************************************************
		Logic to fetch all related data and put into map
	****************************************************/
        if(!eanList.isEmpty()) {
            getPriceList(eanList); 
        }
        if(!cardNumList.isEmpty()) {
        getCardDetails(cardNumList);
        }
        if(!pharmaExtIdList.isEmpty() || !patientRefIdList.isEmpty()) {
        getAccountDetails(pharmaExtIdList, patientRefIdList);
        }
        if(!hcpRefIdList.isEmpty()) {
        getContactDetails(hcpRefIdList);
        }
        
        /**********************************************************************
    Populating all the related data into new purchase transactions
    * ********************************************************************/
        updateNewTransactions(purchseTrnLst);
    }
    
    /**************************************************************************************
* @author      : Saurabh Tripathi
* @date        : 09/18/2019
* @Description : Method to map purchase transaction with matching HCP data
*before insert
* @Param       : Null
* @Return      : Void
***************************************************************************************/
    public void getContactDetails(List<String> hcpRefIdList) {
        if (Contact.sObjectType.getDescribe().isAccessible()) {
            for (Contact con :
                 [ Select Id, Name,
                  PSP_HCP_External_Reference_ID__c from Contact where
                  PSP_HCP_External_Reference_ID__c in:hcpRefIdList ]) {
                      contactHCPMap.put(con.PSP_HCP_External_Reference_ID__c, con.Id);
                  }
        }
    }
    
    /**************************************************************************************
* @author      : Saurabh Tripathi
* @date        : 09/18/2019
* @Description : Method to map purchase transaction with matching Patient and
*Pharmacy data before insert
* @Param       : Null
* @Return      : Void
***************************************************************************************/
    //or PSP_Patient_External_Reference_ID__c in:patientRefIdList
    public void getAccountDetails(Set<String> pharmaExtIdList, Set<String> patientRefIdList) {
        if (Account.sObjectType.getDescribe().isAccessible()) {
            for (Account acc : [
                Select Id, PSP_External_ID__c,
                PSP_Patient_External_Reference_ID__c from Account where
                PSP_External_ID__c
                in:pharmaExtIdList 
            ]) {
                if (String.isNotBlank(acc.PSP_External_ID__c)) {
                    accountMapPharma.put(acc.PSP_External_ID__c, acc.Id);
                }
                if (String.isNotBlank(acc.PSP_Patient_External_Reference_ID__c)) {
                    accountMapPatient.put(acc.PSP_Patient_External_Reference_ID__c, acc.Id);
                }
            }
        }
    }
    
    /**************************************************************************************
* @author      : Saurabh Tripathi
* @date        : 09/18/2019
* @Description : Method to map purchase transaction with matching Card data
*before insert
* @Param       : Null
* @Return      : Void
***************************************************************************************/
    public void getCardDetails(List<String> cardNumList) {
        
        if (PSP_Card_and_Coupon__c.sObjectType.getDescribe().isAccessible()) {
            for (PSP_Card_and_Coupon__c card :
                 [ Select id, Name from PSP_Card_and_Coupon__c where Name
                  in:cardNumList ]) {
                      cardMap.put(card.Name, card.id);
                  }
        }
    }
    
    /**************************************************************************************
* @author      : Saurabh Tripathi
* @date        : 09/18/2019
* @Description : Method to map purchase transaction with matching Price list
*data before insert
* @Param       : Null
* @Return      : Void
***************************************************************************************/
    public void getPriceList(List<Long> eanList) {
        if (PSP_Price_List__c.sObjectType.getDescribe().isAccessible()) {
            for (PSP_Price_List__c prList :
                 [ Select Id, PSP_Product_Name__c,
                  PSP_EAN__c from PSP_Price_List__c where PSP_EAN__c in:eanList ]) {
                      productMap.put(String.ValueOf(prList.PSP_EAN__c),
                                     prList.PSP_Product_Name__c);
                      priceListMap.put(String.ValueOf(prList.PSP_EAN__c), prList.Id);
                  }
        }
    }
    
    /**************************************************************************************
* @author      : Saurabh Tripathi
* @date        : 09/18/2019
* @Description : Method to populate all retrived parent data
* @Param       : Null
* @Return      : Void
***************************************************************************************/
    public void updateNewTransactions(List<PSP_Purchase_Transaction__c> purchseTrnLst) {
        for (PSP_Purchase_Transaction__c purchaseTrn : purchseTrnLst) {
            if(priceListMap.containsKey(purchaseTrn.PSP_EAN__c)) {
            purchaseTrn.PSP_PriceList_EAN__c =
                priceListMap.get(purchaseTrn.PSP_EAN__c);
            }
            if(productMap.containsKey(purchaseTrn.PSP_EAN__c)) {
            purchaseTrn.PSP_Products_and_Services__c =
                productMap.get(purchaseTrn.PSP_EAN__c); 
            }
            if( accountMapPharma.containsKey(purchaseTrn.PSP_Pharmacy_External_ID__c)) {
            purchaseTrn.PSP_Pharmacy_Account__c =
                accountMapPharma.get(purchaseTrn.PSP_Pharmacy_External_ID__c);
            }
            if(accountMapPatient.containsKey(purchaseTrn.PSP_Patient_Reference_ID__c)) {
            purchaseTrn.PSP_Person_Account__c =
                accountMapPatient.get(purchaseTrn.PSP_Patient_Reference_ID__c);
            }
            if (cardMap.containsKey(purchaseTrn.PSP_Card_Number__c)) {
            purchaseTrn.PSP_Card_and_Coupon__c =
                cardMap.get(purchaseTrn.PSP_Card_Number__c); 
            }
            if(contactHCPMap.containsKey(purchaseTrn.PSP_HCP_Reference_Id1__c)) {
            purchaseTrn.PSP_HCP__c =
                contactHCPMap.get(purchaseTrn.PSP_HCP_Reference_Id1__c);
            }
        }
    }
    
    /**************************************************************************************
* @author      : Sulata Sadhu
* @date        : 12/11/2019
* @Description : Method to fetch the Purchase Transaction records
* @Param       : Null
* @Return      : Void
***************************************************************************************/
    public void updateRefillDate(List<PSP_Purchase_Transaction__c> purchseTrnLst) {        
        frequencyMap.put(FREQ_DAILY, 1);
        frequencyMap.put(FREQ_WEEKLY, 7);
        frequencyMap.put(FREQ_MONTHLY, 30);
        frequencyMap.put(FREQ_ANNUAL, 365);
        
        for (PSP_Purchase_Transaction__c pt : purchseTrnLst) {
            if (pt.PSP_Status__c == STATUS_PURCHASED_PT) {
                ptPurchasedList.add(pt);
            } else {
                ptReturnedList.add(pt);
            }
        }
        
        if (!ptPurchasedList.isEmpty()) {
            fetchEPSRecords(ptPurchasedList);
            if (!epsList.isEmpty()) {
                calculateNextRefillDate(ptList, true);
                for (PSP_Purchase_Transaction__c pt : purchseTrnLst) {
                    pt.PSP_Expected_next_refill_not_updated__c = false;
                }
            } else {
                for (PSP_Purchase_Transaction__c pt : purchseTrnLst) {
                    pt.PSP_Expected_next_refill_not_updated__c = true;
                }
            }
        }
        if (!ptReturnedList.isEmpty()) {
            fetchEPSRecords(ptReturnedList);
            
            if (!epsList.isEmpty()) {
                checkPTHistory(purchseTrnLst);
                calculateNextRefillDate(ptList, false);
                for (PSP_Purchase_Transaction__c pt : purchseTrnLst) {
                    pt.PSP_Expected_next_refill_not_updated__c = false;
                }
            } else {
                for (PSP_Purchase_Transaction__c pt : purchseTrnLst) {
                    if (pt.PSP_Status__c != null) {
                        pt.PSP_Expected_next_refill_not_updated__c = true;
                    }
                }
            }
        }
    }
    
    /**************************************************************************************
    * @author      : Sulata Sadhu
    * @date        : 12/11/2019
    * @Description : Method to calculate Refill Days Due and Expected Next Refill
    * Date on Enrolled Prescription and Service
    * @Param       : Null
    * @Return      : Void
    ***************************************************************************************/
    public void calculateNextRefillDate(List<PSP_Purchase_Transaction__c> purchseTrnLst, Boolean isPurchased) {
        if(CareProgramEnrolleeProduct.sObjectType.getDescribe().isAccessible() && CareProgramEnrolleeProduct.sObjectType.getDescribe().isUpdateable()) {
        for (CareProgramEnrolleeProduct eps : epsList) {
            careProgramProductId.add(eps.CareProgramProductId);
            if (!epMap.containsKey(eps.CareProgramProductId)) {
                epMap.put(eps.CareProgramProductId, eps.Id);
            } else {
                epMap.put(eps.CareProgramProductId,epMap.get(eps.CareProgramProductId) + ';' + eps.Id);
            }
            epsMapData.put(eps.Id, eps);
        }
        if (Product2.sObjectType.getDescribe().isAccessible() && CareProgramProduct.sObjectType.getDescribe().isAccessible()) {
            productList = [ SELECT id, PSP_Quantity_per_SKU__c, PSP_Sub_Brand__c, (SELECT id, ProductId FROM ProductCareProgramProducts)from Product2
                           WHERE id IN ( SELECT ProductId FROM CareProgramProduct WHERE id IN : careProgramProductId)];
            final List<String> cpIdList = new List<String>();
            for (Product2 prd : productList) {
                if (ptQtyMap.containsKey(prd.Id)) {
                    productQtyMap.put(prd.Id, prd);
                    for (CareProgramProduct prodCP : prd.ProductCareProgramProducts) {
                        cpIdList.add(prodCP.Id);
                    }
                    productCPMap.put(prd.Id, cpIdList);
                }
            }
        }
        for (Id productId : ptQtyMap.keySet()) {
            if (ptQtyMap.containsKey(productId)){
                for(PSP_Purchase_Transaction__c pt : ptQtyMap.get(productId)){
                    if(products.get(pt.PSP_Products_and_Services__c).PSP_Sub_Brand__c ==
                       productQtyMap.get(productId).PSP_Sub_Brand__c) {
                           if (pt.PSP_Quantity__c != null && productQtyMap.get(productId).PSP_Quantity_per_SKU__c != null) {
                               CareProgramEnrolleeProduct eps = null;
                               Decimal purchaseTransQty = pt.PSP_Quantity__c;
                               Decimal productQty = productQtyMap.get(productId).PSP_Quantity_per_SKU__c;
                               
                               Date purchaseDate = pt.CreatedDate == Null ? System.today(): pt.CreatedDate.date() ;
                               List<String> careProgram = productCPMap.get(productId);
                               
                               for (Id careProg : careProgram) {
                                   if (epMap.containsKey(careProg)) {
                                       List<String> epsData = epMap.get(careProg).split(';');
                                       
                                       for (String epsId : epsData) {
                                           eps = epsMapData.get(epsId);
                                           if (isPurchased == true || (isPurchased == false && ptHistoryMap.containsKey(productId))) {
                                               if (eps.PSP_Prescription_Quantity__c != null && frequencyMap.get(eps.PSP_Prescription_Frequency__c) != null) {
                                                   
                                                   Decimal epsPrescriptionQty = eps.PSP_Prescription_Quantity__c;
                                                   Decimal epsPrescriptionFreq = frequencyMap.get(eps.PSP_Prescription_Frequency__c);
                                                   Decimal calculatedVal = (purchaseTransQty * productQty * epsPrescriptionFreq) / epsPrescriptionQty;
                                                   if (!isPurchased) { 
                                                       purchaseTransQty = ptCreatedDateMap.get(productId).PSP_Quantity__c;
                                                       purchaseDate = ptCreatedDateMap.get(productId).CreatedDate.date();
                                                   }
                                                   eps.PSP_Expected_Next_Refill_Date__c = purchaseDate.addDays(calculatedVal.round().intValue());
                                                   epsListUpdate.add(eps);
                                               }
                                           } else {
                                               eps.PSP_Expected_Next_Refill_Date__c = null;
                                               epsListUpdate.add(eps);
                                           }
                                       }
                                   }
                               }
                           }
                       }
                }
            }
        }
         Database.update(epsListUpdate);
        }
    }
    /**************************************************************************************
* @author      : Sulata Sadhu
* @date        : 12/11/2019
* @Description : Method to check the Purchase Transaction History object
* @Param       : Null
* @Return      : Void
***************************************************************************************/
    public void checkPTHistory(List<PSP_Purchase_Transaction__c> purchseTrnLst) {
        if (PSP_Purchase_Transaction__c.sObjectType.getDescribe().isAccessible()) {
            List<PSP_Purchase_Transaction__c> ptHistoryList = new List<PSP_Purchase_Transaction__c>();
            ptHistoryList = [
                SELECT id, PSP_Quantity__c, name, PSP_Person_Account__c, PSP_Products_and_Services__c, PSP_Expected_next_refill_not_updated__c,
                PSP_Products_and_Services__r.PSP_Sub_Brand__c, PSP_Status__c,CreatedDate FROM PSP_Purchase_Transaction__c WHERE
                PSP_Person_Account__c != null AND PSP_Products_and_Services__c != null AND PSP_Person_Account__c
                IN:patiendId AND PSP_Status__c =:STATUS_PURCHASED_PT AND id NOT IN:purchseTrnLst order BY CreatedDate DESC
            ];
            for (PSP_Purchase_Transaction__c ptHist : ptHistoryList) {
                for(PSP_Purchase_Transaction__c pt : ptQtyMap.get(ptHist.PSP_Products_and_Services__c)){
                    if (pt.PSP_Products_and_Services__r.PSP_Sub_Brand__c == ptHist.PSP_Products_and_Services__r.PSP_Sub_Brand__c &&
                        !ptHistoryMap.containsKey(ptHist.PSP_Products_and_Services__r.Id)) {
                            ptHistoryMap.put(ptHist.PSP_Products_and_Services__r.Id, ptHist.CreatedDate);
                        }}
            }
            List<PSP_Purchase_Transaction__c> ptData = new List<PSP_Purchase_Transaction__c>();
            ptData = [ SELECT id, PSP_Quantity__c, name, PSP_Person_Account__c,PSP_Products_and_Services__c,PSP_Expected_next_refill_not_updated__c,
                      PSP_Products_and_Services__r.PSP_Sub_Brand__c, PSP_Status__c,CreatedDate FROM PSP_Purchase_Transaction__c WHERE CreatedDate
                      IN:ptHistoryMap.values() order by name desc ];
            for (PSP_Purchase_Transaction__c pt : ptData) {
                ptCreatedDateMap.put(pt.PSP_Products_and_Services__r.Id, pt);
            }
        }
    }
    
    
    /**************************************************************************************
* @author      : Sulata Sadhu
* @date        : 12/11/2019
* @Description : Method to fetch Enrolled Prescription and Service records
* @Param       : Null
* @Return      : Void
***************************************************************************************/
    
    public void fetchEPSRecords(List<PSP_Purchase_Transaction__c> purchseTrnLst) {
        
        if (Product2.sObjectType.getDescribe().isAccessible()) {        
            Set<Id> productList = new Set<Id>();
            ptList.clear();
            ptQtyMap.clear();
            patiendId.clear();
            epsList.clear();
            for(PSP_Purchase_Transaction__c purchase : purchseTrnLst ){
                if(purchase.PSP_Person_Account__c != null && purchase.PSP_Products_and_Services__c != null ){
                    ptList.add(purchase);
                    productList.add(purchase.PSP_Products_and_Services__c);
                }
            }
            products =  new Map<Id,Product2> ([select id,RecordType.name,PSP_Sub_Brand__c from Product2 where ID IN: productList ]);
            for (PSP_Purchase_Transaction__c pt : ptList) {
                patiendId.add(pt.PSP_Person_Account__c);
                if (products.get(pt.PSP_Products_and_Services__c).RecordType.name == PRODUCT_REC_TYP_NAME) {
                    List<PSP_Purchase_Transaction__c> fetchedPT = new List<PSP_Purchase_Transaction__c>();
                    if(ptQtyMap.containsKey(pt.PSP_Products_and_Services__c)){
                        fetchedPT = ptQtyMap.get(pt.PSP_Products_and_Services__c);
                    }    
                    fetchedPT.add(pt);
                    ptQtyMap.put(pt.PSP_Products_and_Services__c, fetchedPT);
                }
            }
        }
        
        if (CareProgramEnrolleeProduct.sObjectType.getDescribe().isAccessible()) {
            epsList = [
                SELECT id, PSP_Prescription_Quantity__c, PSP_Prescription_Frequency__c,
                PSP_Patient__c,CareProgramProductId FROM CareProgramEnrolleeProduct WHERE Status =:STATUS_ASSIGNED_EPS and PSP_Patient__c IN:patiendId
            ];
        }
    }
}
/********************************************************************************************************
*  @author          Deloitte
*  @description     Test class for CCL_Privacy_Disclaimer_Login Page.
*  @param           
*  @date            June 19, 2020
*********************************************************************************************************/

@isTest
private with sharing class CCL_Privacy_Disclaimer_Controller_Test {

/********************************************************************************************************
*  @author          Deloitte
*  @description     Method to create setup data require to test functionality for CCL_Privacy_Disclaimer_Login 
                    Page.
*  @param           
*  @return          Pagereference
*  @date            June 1, 2020
*********************************************************************************************************/
    @TestSetup
    static void setupTestData(){
        List<User> userList = new List<User>();
        UserRole customerPortalUserRole = [Select Id, PortalType, PortalAccountId From UserRole where PortalType ='None' LIMIT 1];
        String profileName = 'System Administrator';
        User testUser = CCL_TestDataFactory.createTestUser(profileName);
        testUser.UserRoleId = customerPortalUserRole.Id;
        insert testUser;
        
        System.runAs(testUser){
            Account externalUserAcc = CCL_TestDataFactory.createTestParentAccount();
            externalUserAcc.Name = 'External User Account';
            insert externalUserAcc;
            
            Contact portalUserCon = new Contact( AccountID = externalUserAcc.id, FirstName = 'External', LastName = 'Portal',
            email = 'testuser@xyz.com' );
            insert portalUserCon;
            
            //String externalProfileName = 'External Base Profile';
            //User portalTestUser = CCL_TestDataFactory.createTestUser(externalProfileName);
            //portalTestUser.username = 'externalPortalUserName@novartis.com';
            //portalTestUser.email = 'externalPortalUser@novartis.com';
            //portalTestUser.ContactId = portalUserCon.Id;
            //insert portalTestUser;
            
            final String currentTime= String.valueOf(System.now().millisecond());
    		final String randomNum= String.valueOf(Math.abs(Crypto.getRandomInteger()));
        
            
            final User externalBaseProfileUser = new User();
            final Profile externalProfile = [SELECT Id FROM Profile WHERE Name =:CCL_StaticConstants_MRC.EXTERNAL_BASE_PROFILE_TYPE];
            externalBaseProfileUser.ProfileId= externalProfile.Id;
            externalBaseProfileUser.LastName= 'test LastName';
            externalBaseProfileUser.FirstName= 'test FirstName';
            externalBaseProfileUser.Alias='testUser';
            externalBaseProfileUser.Email='testUser122221@novartis.com';
            externalBaseProfileUser.emailencodingkey = 'UTF-8';
            externalBaseProfileUser.languagelocalekey = 'en_US';
            externalBaseProfileUser.localesidkey = 'en_US';
            externalBaseProfileUser.country = 'United States';
            externalBaseProfileUser.timezonesidkey = 'America/Los_Angeles';
            externalBaseProfileUser.username = 'testUser' + randomNum + currentTime +'@novartis.com';
            externalBaseProfileUser.ContactId = portalUserCon.Id;
            insert externalBaseProfileUser;
        }
    }

/********************************************************************************************************
*  @author          Deloitte
*  @description     Method to validate logic when a user accept's privacy policy and redirect it to home page. 
*  @param           
*  @return          Pagereference
*  @date            June 1, 2020
*********************************************************************************************************/
    private static testMethod void updateUserDisclaimerInfoTest(){
        User usrRec = [SELECT Id, ProfileId, alias FROM User WHERE Profile.Name = 'External Base Profile' AND alias = 'testUser' LIMIT 1];
        System.runAs(usrRec){
            test.StartTest();
            CCL_Privacy_Disclaimer_Controller privacyDisclaimerCntrlObj = new CCL_Privacy_Disclaimer_Controller();
            PageReference redirectHomePage = privacyDisclaimerCntrlObj.redirectToHomePage();
            System.assertNotEquals(redirectHomePage,null);
            test.StopTest();
        }
    }

/********************************************************************************************************
*  @author          Deloitte
*  @description     Method to validate logic when user already agree on the Privacy Policy.
*  @param           
*  @return          void
*  @date            June 1, 2020
*********************************************************************************************************/
    private static testMethod void userDisclaimerInfoCheckedTest(){
        User usrRec = [SELECT Id, ProfileId, alias FROM User WHERE Profile.Name = 'External Base Profile' AND alias = 'testUser' LIMIT 1];
        
        Contact portalUserContactRec = [SELECT Id, CCL_Has_User_Accepted_Privacy_Disclaimer__c FROM Contact LIMIT 1];
        portalUserContactRec.CCL_Has_User_Accepted_Privacy_Disclaimer__c = true;
        update portalUserContactRec;
        
        System.runAs(usrRec){
            test.StartTest();
            CCL_Privacy_Disclaimer_Controller privacyDisclaimerCntrlObj = new CCL_Privacy_Disclaimer_Controller();
            PageReference redirectHomePage = privacyDisclaimerCntrlObj.redirectToHomePage();
            System.assertNotEquals(redirectHomePage,null);
            test.StopTest();
        }
    }
}
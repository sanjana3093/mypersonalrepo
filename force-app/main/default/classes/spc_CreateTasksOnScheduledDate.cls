/*********************************************************************************************************
* @author Deloitte
* @description This class runs daily and is used to create tasks for interactions scheduled for today.
* @date January 3, 2019
****************************************************************************************************************/
global class spc_CreateTasksOnScheduledDate implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id,PatientConnect__PC_Patient_Program__c,PatientConnect__PC_Patient_Program__r.OwnerId FROM PatientConnect__PC_Interaction__c WHERE spc_Scheduled_Date__c = TODAY';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        //interactions with scheduled date as today
        List<PatientConnect__PC_Interaction__c> lstAllInteractions = new List<PatientConnect__PC_Interaction__c>();
        for (SObject obj : scope) {
            PatientConnect__PC_Interaction__c interaction = (PatientConnect__PC_Interaction__c)obj;
            lstAllInteractions.add(interaction);
        }

        //interactions with open soc task
        Set<Id> lstInteractionsWithOpenTasks = new Set<Id>();
        String subject = System.Label.spc_Call_SOC_to_Confirm_Infusion_Start;
        List<String> lstStatus = new List<String> {'Completed', 'Not Needed'};
        List<Task> openTasks = [SELECT spc_Interaction__c
                                FROM Task
                                WHERE spc_Interaction__c IN :lstAllInteractions AND
                                Subject = :subject AND
                                          Status NOT IN :lstStatus];
        for (Task t : openTasks) {
            lstInteractionsWithOpenTasks.add(t.spc_Interaction__c);
        }

        List<Task> lstTasksToBeCreated = new List<Task>();
        for (PatientConnect__PC_Interaction__c interaction : lstAllInteractions) {
            if (! lstInteractionsWithOpenTasks.contains(interaction.Id)) {
                Task newTask = new Task();
                newTask.Subject = System.Label.spc_Call_SOC_to_Confirm_Infusion_Start;
                newTask.PatientConnect__PC_Category__c = spc_ApexConstantsSetting__c.getInstance('TASK_CAT_SITE_OF_CARE').spc_Value__c;
                newTask.PatientConnect__PC_Sub_Category__c = spc_ApexConstantsSetting__c.getInstance('TASK_CAT_CONFIRM_INFUSION').spc_Value__c;
                newTask.Priority = spc_ApexConstantsSetting__c.getInstance('TASK_PRIORITY_NORMAL').spc_Value__c;
                newTask.OwnerId = interaction.PatientConnect__PC_Patient_Program__r.OwnerId;
                newTask.WhatId = interaction.PatientConnect__PC_Patient_Program__c;
                newTask.ActivityDate = System.today();
                newTask.Status = spc_ApexConstantsSetting__c.getInstance('TASK_STATUS_NOT_STARTED').spc_Value__c;
                newTask.PatientConnect__PC_Direction__c = spc_ApexConstantsSetting__c.getInstance('TASK_DIR_OUTBOUND').spc_Value__c;
                newTask.PatientConnect__PC_Channel__c = spc_ApexConstantsSetting__c.getInstance('CHANNEL_PHONE').spc_Value__c;
                newTask.PatientConnect__PC_Program__c = interaction.PatientConnect__PC_Patient_Program__c;
                newTask.spc_Interaction__c = interaction.Id;
                newTask.PatientConnect__PC_Assigned_To__c = interaction.PatientConnect__PC_Patient_Program__r.OwnerId;
                lstTasksToBeCreated.add(newTask);
            }
        }

        if (!lstTasksToBeCreated.isEmpty()) {
            Database.insert(lstTasksToBeCreated, false);
        }

    }

    global void finish(Database.BatchableContext bc) {

    }
}
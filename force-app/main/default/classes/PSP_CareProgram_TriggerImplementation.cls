/*********************************************************************************************************
class Name      : PSP_CareProgram_TriggerImplementation 
Description		: Trigger Implementation Class for Care Program
@author		    : Shourya Solipuram 
@date       	: July 10, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Shourya Solipuram           July 10, 2019          Initial Version
****************************************************************************************************************/
public class PSP_CareProgram_TriggerImplementation {
  /**************************************************************************************
  * @author      : Deloitte
  * @date        : 07/10/2019
  * @Description : Method to check if the Name is unique.
  * @Param       : List of Care Program
  * @Return      : Void
  ***************************************************************************************/
    public void uniquecheck (List<CareProgram> newPrograms) {
        //List of names
		final List<String> names = new List<String>();
        //List of care programs to be checked
    	final List<CareProgram> prgToChk = new List<CareProgram> ();
        for (CareProgram prg : newPrograms) { 
            if (String.isNotBlank(prg.Name)) {
                names.add (prg.Name);
                prgToChk.add (prg);
            } 
        }
        //PSP_Utility.checkUnique (Label.PSP_CareProgram,Label.PSP_Field_Name,Label.PSP_Unique_Care_Program_Name,names,prgToChk,null);
		 PSP_Utility.checkUnique (PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.OBJ_NAME_CAREPROGRAM),PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.CAREPROGRAM_FLD_NAME),Label.PSP_Unique_Care_Program_Name,names,prgToChk,null);
         //PSP_Utility.checkUnique ('CareProgram','Name',Label.PSP_Unique_Care_Program_Name,names,prgToChk,null);
    }
}
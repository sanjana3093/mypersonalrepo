/**
* @author Deloitte
* @date Sept 21 2018
*
* @description This is the Implementation class for Health Plan Trigger
*/


public class spc_HealthPlanTriggerImplementation {

    /*******************************************************************************************************
    * @description This method is used to ensure that there is only one active health plan of a particular type for a patient.
    * @return void
    */

    public void validateActiveHealthPlans(List<PatientConnect__PC_Health_Plan__c> lstActiveHealthPlans) {
        Set<Id> setPatientAccountIds = new Set<Id>();
        Map<Id, List<PatientConnect__PC_Health_Plan__c>> mapAccountToHealthPlans = new Map<Id, List<PatientConnect__PC_Health_Plan__c>>();
        String activeStatus = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.HEALTHPLAN_STATUS_ACTIVE);

        for (PatientConnect__PC_Health_Plan__c hp : lstActiveHealthPlans) {
            setPatientAccountIds.add(hp.PatientConnect__Patient__c);
        }

        for (PatientConnect__PC_Health_Plan__c hp : [SELECT Id, PatientConnect__Patient__c, PatientConnect__PC_Plan_Type__c, PatientConnect__PC_Plan_Status__c
                FROM PatientConnect__PC_Health_Plan__c
                WHERE PatientConnect__PC_Plan_Status__c = :activeStatus AND PatientConnect__Patient__c IN :setPatientAccountIds]) {
            List<PatientConnect__PC_Health_Plan__c> healthPlans;
            if (mapAccountToHealthPlans.containsKey(hp.PatientConnect__Patient__c)) {
                healthPlans = mapAccountToHealthPlans.get(hp.PatientConnect__Patient__c);
            } else {
                healthPlans = new List<PatientConnect__PC_Health_Plan__c>();
            }
            healthPlans.add(hp);
            mapAccountToHealthPlans.put(hp.PatientConnect__Patient__c, healthPlans);
        }

        for (PatientConnect__PC_Health_Plan__c healthPlan : lstActiveHealthPlans) {
            if (mapAccountToHealthPlans.get(healthPlan.PatientConnect__Patient__c) != null) {
                for (PatientConnect__PC_Health_Plan__c otherHP : mapAccountToHealthPlans.get(healthPlan.PatientConnect__Patient__c)) {
                    if (otherHP.PatientConnect__PC_Plan_Type__c == healthPlan.PatientConnect__PC_Plan_Type__c) {
                        healthPlan.addError(Label.spc_ActiveHealthPlanErrror.substringBefore(activeStatus) + activeStatus + ' ' + healthPlan.PatientConnect__PC_Plan_Type__c + Label.spc_ActiveHealthPlanErrror.substringAfter(activeStatus));
                    }
                }
            }
        }
    }
}
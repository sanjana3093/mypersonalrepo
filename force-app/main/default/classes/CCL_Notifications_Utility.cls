/********************************************************************************************************
*  @author          Deloitte
*  @description     This class will hold the utility methods to support the notification engine 
*  @date            August 04 2020 
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Deloitte                  August 04 2020         Initial Version
****************************************************************************************************************/
public with sharing class CCL_Notifications_Utility {
    
    /*
    *   @author           Deloitte
    *   @description      Fetch the field values, sort through the data, return merge field values
    *   @para             String (text having merge fields), SObject (CurrentRecord information) , Pattern
    *   @return           String
    *   @date             4 August 2020
    */
    public Static String resolveMergeFields(String source, SObject currentRecord, Pattern regexPattern) {
        String SourceMatcher = source;
        final Matcher regexMatcher = regexPattern.matcher(SourceMatcher);
        if(regexMatcher.find()) {
            final String valueToiiBereplaced = SourceMatcher.substring(regexMatcher.start(), regexMatcher.end());
            System.debug('valueToiiBereplaced+++'+valueToiiBereplaced);
            final String matchedValue= SourceMatcher.substring(regexMatcher.start() + 2, regexMatcher.end() - 1);
            system.debug('Matched value is'+ matchedValue);
            final String recordName = (String.valueOf(CCL_RelationshipQueryUtil.getFieldValue(currentRecord,matchedValue)) == null) ? '' : String.valueOf(CCL_RelationshipQueryUtil.getFieldValue(currentRecord,matchedValue));
            system.debug('Record Name is'+ recordName);
            SourceMatcher = source.replace(valueToiiBereplaced,recordName);
            system.debug('Final String is'+SourceMatcher);
        }
        return SourceMatcher;
    }

   /*
    *  @author          Deloitte
    *  @description     General method to take Object name and fieldset name 
    *                   and convert it into a Set of Strings
    *  @param           String, String
    *  @return          Set<String>
    *  @date            July 30, 2020
    */ 
    public static Set<String> convertFieldSetToStringSet(String ObjectName, String fieldSetName ) {
        final Set<String> fields = new set<String>();
        for(Schema.FieldSetMember m:readFieldSet(fieldsetname, objectname)) {
            fields.add(m.getFieldPath().toLowerCase());
        }
        return fields;
    }
    
    /*
    *  @author          Deloitte
    *  @description     General method to take Object name and fieldset name 
    *                   which reads field set and returns described fields
    *  @param           String, String
    *  @return          List<Schema.FieldSetMember>
    *  @date            July 30, 2020
    */ 
    private static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String objectName) {
        final Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        final Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);
        final Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        final Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
    return fieldSetObj.getFields(); 
    }

    /*
    *  @author          Deloitte
    *  @description     General method to take Map of fieldNamesPerRecordId & map of recordsPerRecordId
    *                   With the help of above mentioned maps we will get the values of the field passed in the Map of fieldNamesPerRecordId
    *  @param           Map<String,Set<String>>, Map<String,sObject>
    *  @return          Map<String,Set<Id>>
    *  @date            July 30, 2020
    */
    public Static Map<String,Set<Id>> generateIdsFromString(Map<String,Set<String>> fieldNamesPerRecordId, Map<String,sObject> recordsPerRecordId) {
        final Map<String,Set<Id>> mapToReturn = new Map<String,Set<Id>>();
        final Set<Id> setToStoreGeneratedIds= new Set<Id>();
        if(!fieldNamesPerRecordId.isEmpty() && !recordsPerRecordId.isEmpty()) {
            for(String Key: fieldNamesPerRecordId.keySet()) {
                for(String fieldName: fieldNamesPerRecordId.get(Key)) {
                    final sObject currentRecord=recordsPerRecordId.get(Key);
                    final Id relatedRecordId= (ID)CCL_RelationshipQueryUtil.getFieldValue(currentRecord,fieldName);
                    setToStoreGeneratedIds.add(relatedRecordId);
                }
                mapToReturn.put(Key,setToStoreGeneratedIds);
            }
        }
    return mapToReturn;
    }
}
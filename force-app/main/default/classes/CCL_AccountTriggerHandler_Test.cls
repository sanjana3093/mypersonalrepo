/********************************************************************************************************
*  @author          Deloitte
*  @description     Account Trigger Handler Test Class.
*  @date            August 31, 2020
*  @version         1.0
*********************************************************************************************************/
@isTest
private class CCL_AccountTriggerHandler_Test {
    /********************************************************************************************************
     *  @author          Deloitte
     *  @description     Test class to execute updatePotentialDuplicateFlag
     *  @param
     *  @date            August 31, 2020
     *********************************************************************************************************/
    private static testMethod void testUpdatePotentialDuplicateFlag() {
        Test.startTest();
        
        Account patientdetail = new Account();
        patientdetail.RecordTypeId= CCL_StaticConstants_MRC.ACCOUNT_PERSONACCOUNT_PATIENTDETAIL;
        patientdetail.FirstName = 'Test';
        patientdetail.LastName = 'Acc Duplicate';
        patientdetail.CCL_Date_of_DOB__pc='1';
        patientdetail.CCL_Month_of_DOB__pc='Jan';
        patientdetail.CCL_Year_of_DOB__pc='1992';
        insert patientdetail;
            
        Account newAccount = new Account();
        newAccount.RecordTypeId= CCL_StaticConstants_MRC.ACCOUNT_PERSONACCOUNT_PATIENTDETAIL;
        newAccount.FirstName = 'Test';
        newAccount.LastName = 'Accd Duplicate';
        newAccount.CCL_Date_of_DOB__pc='1';
        newAccount.CCL_Month_of_DOB__pc='Jan';
        newAccount.CCL_Year_of_DOB__pc='1992';
        insert(newAccount);

        Test.stopTest();
        
        Account updateAcc = [SELECT Id, CCL_Potential_Duplicate_Patient__c FROM Account WHERE Id = :newAccount.Id];

        System.assertEquals(updateAcc.CCL_Potential_Duplicate_Patient__c, false, 'The duplicate flag should be set to false.');
    }
}
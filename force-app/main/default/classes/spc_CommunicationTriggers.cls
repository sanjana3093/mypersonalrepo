/*********************************************************************************************************
 * @author        Deloitte
 * @date         January 3, 2019
 * @description  spc_CommunicationTriggers
**/
public class spc_CommunicationTriggers {

  private static spc_CommunicationTriggers triggerImplementation = new spc_CommunicationTriggers();

  /*********************************************************************************
  * @author      Deloitte
  * @description  afterUpdateCaseTriggers Send all communication for on Update Case Conditions
  * @return     Void
  * @date         January 3, 2019
  */
  public void afterUpdateCaseTriggers(List<Case> newCases) {

    List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();
    Set<Id> programCaseMICompleted = new Set<Id>();
    Set<Id> programCaseCoverageId = new Set<Id>();

    //////////////////////////////////////////     T-059 and T-063    /////////////////////

    for (Case newCase : newCases) {

      String caseRcrdType = Schema.SObjectType.Case.getRecordTypeInfosById().get(newCase.recordtypeid).getname();
      Case oldCaseRec = (Case)Trigger.oldMap.get(newCase.Id) ;

      // Patient Enrollment Information to SOC & ECP Program Case Status template - when status changed to Enrolled and indicator MI to Complete
      if (newCase.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType)
          && ((newCase.Status != oldCaseRec.Status) || (newCase.PatientConnect__PC_Status_Indicator_3__c != oldCaseRec.PatientConnect__PC_Status_Indicator_3__c))
          && newCase.Status.equalsIgnoreCase(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_STATUS_ENROLLED))
          && String.isNotBlank(newCase.PatientConnect__PC_Status_Indicator_3__c)
          && newCase.PatientConnect__PC_Status_Indicator_3__c.equalsIgnoreCase(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.PATIENT_STATUS_INDICATOR_COMPLETE))
          && newCase.spc_HIPAA_Consent_Received__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES)) {
        envelops.add(triggerImplementation.createEnvelop(newCase.id
                     , null
                     , Label.spc_Patient_Enrollment_Information_to_SOC_SPP_FAX
                     , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO),
                                        spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_SPECIALTY_PHARMACY),
                                        spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_EXT_COMP_PHARMACY)
                                       }
                     , null
                     , spc_CommunicationMgr.Channel.Fax));
        if (newCase.spc_HIPAA_Consent_Received__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES)) {
          programCaseMICompleted.add(newCase.Id);
        }
      }
      //PAP Order Form
      if (newCase.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType)
          && Label.spc_Yes.equalsIgnoreCase(newCase.spc_HIPAA_Consent_Received__c)
          && String.isNotBlank(newCase.spc_REMS_Authorization_Status__c)
          && String.isNotBlank(newCase.spc_REMS_Enrollment_Status__c)
          && ((newCase.spc_REMS_Authorization_Status__c != oldCaseRec.spc_REMS_Authorization_Status__c) || (newCase.spc_REMS_Enrollment_Status__c != oldCaseRec.spc_REMS_Enrollment_Status__c))
          && newCase.spc_REMS_Authorization_Status__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_ENROLLMENT_STATUS_AUTHORIZED))
          && newCase.spc_REMS_Enrollment_Status__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_ENROLLMENT_STATUS_ENROLLED))) {
        programCaseCoverageId.add(newCase.Id);
        if (! programCaseCoverageId.isEmpty()) {
          envelops.addAll(sendPAPForm(programCaseCoverageId, true));
        }
      }

      // Advocacy Support template condition - When Case Status moved from Enrolled to Pretreatment
      if (newCase.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType)
          && oldCaseRec.Status.equalsIgnoreCase(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_STATUS_ENROLLED))
          && newCase.Status.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_STATUS_PRETREATMENT))
          && newCase.spc_HIPAA_Consent_Received__c == Label.spc_Yes
          && oldCaseRec.Status != newCase.Status) {

        envelops.add(triggerImplementation.createEnvelop(newCase.Id
                     , null
                     , label.spc_AdvocacySupport
                     , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)}
                     , null
                     , spc_CommunicationMgr.Channel.Email));

      }

    }
    if (! programCaseMICompleted.isEmpty()) {
      envelops.addAll(triggerImplementation.automateFaxToLash(programCaseMICompleted));
    }
    if (! envelops.isEmpty()) {
      triggerImplementation.sendCommunication(envelops);
    }
  }
  /*********************************************************************************
  * @author      Deloitte
  * @description   afterUpdateAccount, Send all communication for on Update Account Conditions
  * @return    void
  * @date         January 3, 2019
  *********************************************************************************/
  public void afterUpdateAccount(List<Account> accountList) {
    Set<Id> accountIds = new Set<Id>();
    Set<Id> programCaseCoverageId = new Set<Id>();
    Set<Id> programCaseCoverageFaxId = new Set<Id>();
    Set<Id> programCaseMICompleted = new Set<Id>();
    Set<Id> remsAccountIds = new Set<Id>();
    Set<Id> caseIdList = new Set<Id>();
    List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();
    for (Account newAcc : accountList) {
      Account oldAcc = (Account) Trigger.oldMap.get(newAcc.Id);
      if (newAcc.spc_HIPAA_Consent_Received__c != oldAcc.spc_HIPAA_Consent_Received__c
          && newAcc.spc_HIPAA_Consent_Received__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES)) {
        accountIds.add(newAcc.Id);
      }
      if (newAcc.spc_REMS_Text_Consent__c != oldAcc.spc_REMS_Text_Consent__c
          && spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES).equalsIgnoreCase(newAcc.spc_REMS_Text_Consent__c)) {
        remsAccountIds.add(newAcc.Id);
      }
    }
    //null check of account checking MI complete complete and patient auth status
    if (!accountList.isEmpty()) {
      for (Case oCase : [SELECT Id, AccountId, spc_REMS_Authorization_Status__c, spc_REMS_Enrollment_Status__c, spc_Patient_HIPAA_Services_Consent__c, spc_WC__c, Status, PatientConnect__PC_Status_Indicator_3__c
                         from Case where AccountId IN: accountList AND Type = : spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE_NAME
                             AND IsClosed != true]) {
        if (!accountIds.isEmpty() && accountIds.contains(oCase.AccountId)) {
          if (spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_ENROLLMENT_STATUS_AUTHORIZED).equalsIgnoreCase(oCase.spc_REMS_Authorization_Status__c )
              && spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_ENROLLMENT_STATUS_ENROLLED).equalsIgnoreCase(oCase.spc_REMS_Enrollment_Status__c )) {
            programCaseCoverageId.add(oCase.Id);
          }
          programCaseCoverageFaxId.add(oCase.Id);
          if (spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_STATUS_ENROLLED).equalsIgnoreCase(oCase.Status)
              && String.isNotBlank(oCase.PatientConnect__PC_Status_Indicator_3__c)
              && spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.PATIENT_STATUS_INDICATOR_COMPLETE).equalsIgnoreCase(oCase.PatientConnect__PC_Status_Indicator_3__c)) {
            programCaseMICompleted.add(oCase.Id);
          }
          if (!remsAccountIds.isEmpty() && remsAccountIds.contains(oCase.AccountId)) {
            caseIdList.add(oCase.Id);
          }
        }
      }
      if (!caseIdList.isEmpty()) {
        for (PatientConnect__PC_Interaction__c intr : [SELECT Id, Name, spc_Scheduled_Date__c, spc_Has_Active_SOC__c, PatientConnect__PC_Patient_Program__c
             from PatientConnect__PC_Interaction__c where
             PatientConnect__PC_Patient_Program__r.spc_HIPAA_Consent_Received__c = :System.Label.spc_Yes AND PatientConnect__PC_Patient_Program__c IN: caseIdList]) {
          if (null != intr.spc_Scheduled_Date__c && intr.spc_Scheduled_Date__c >=  Date.TODAY() + 2) {
            envelops.add(triggerImplementation.createEnvelop(intr.PatientConnect__PC_Patient_Program__c
                         , null
                         , label.spc_Appointment_Reminder
                         , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)}
                         , new Set<Id> {intr.Id}
                         , spc_CommunicationMgr.Channel.Email));
          }
        }
      }
    }

    if (! programCaseMICompleted.isEmpty()) {
      envelops.addAll(automateFaxToLash(programCaseMICompleted));
    }

    if (! programCaseCoverageId.isEmpty()) {
      envelops.addAll(sendPAPForm(programCaseCoverageId, true));
    }
    if (! programCaseCoverageFaxId.isEmpty()) {
      envelops.addAll(sendPAPForm(programCaseCoverageId, false));
    }
    if (! envelops.isEmpty()) {
      triggerImplementation.sendCommunication(envelops);
    }
  }
  /*********************************************************************************
  * @author       Deloitte
  * @escription   afterInsertInteractionTriggers  Send all communication for on Insert Interaction Conditions
  * @return      Void
  *********************************************************************************/
  public void afterInsertInteractionTriggers(List<PatientConnect__PC_Interaction__c> lstNewInteractions) {

    List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();

    // Scheduled Infusion Date Pre-Tx Phase - Scheduled Infused Date is populated in the Interaction Record
    Set<Id> setProgramCaseIds = new Set<Id>();
    Set<Id> socSet = new Set<Id>();
    List<PatientConnect__PC_Interaction__c> lstAllInteractions = new List<PatientConnect__PC_Interaction__c>();
    //getting all interactions based on scheduled date
    for (PatientConnect__PC_Interaction__c interaction : lstNewInteractions) {
      if (interaction.spc_Scheduled_Date__c != null ) {
        setProgramCaseIds.add(interaction.PatientConnect__PC_Patient_Program__c);
        socSet.add(interaction.PatientConnect__PC_Participant__c);
      }
    }

    lstAllInteractions = [SELECT Id, Name, spc_Scheduled_Date__c, spc_Has_Active_SOC__c, PatientConnect__PC_Patient_Program__c,
                          PatientConnect__PC_Participant__c,
                          PatientConnect__PC_Patient_Program__r.spc_HIPAA_Consent_Received__c,
                          PatientConnect__PC_Patient_Program__r.spc_Patient_Services_Consent_Received__c
                          FROM PatientConnect__PC_Interaction__c
                          WHERE spc_Has_Active_SOC__c = true AND PatientConnect__PC_Patient_Program__c IN :setProgramCaseIds];
    Map<Id, PatientConnect__PC_Association__c> socMap = new Map<Id, PatientConnect__PC_Association__c>();
    for (PatientConnect__PC_Association__c socAssociation : [SELECT Id, PatientConnect__PC_Account__c FROM PatientConnect__PC_Association__c
         WHERE PatientConnect__PC_Program__c = :setProgramCaseIds AND PatientConnect__PC_Account__c = :socSet
             AND PatientConnect__PC_AssociationStatus__c = :spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)]) {
      socMap.put(socAssociation.PatientConnect__PC_Account__c, socAssociation);
    }
    if (!lstAllInteractions.isEmpty()) {
      for (PatientConnect__PC_Interaction__c inter : lstAllInteractions ) {
        if (inter.PatientConnect__PC_Patient_Program__r.spc_HIPAA_Consent_Received__c == Label.spc_Yes
            && inter.PatientConnect__PC_Patient_Program__r.spc_Patient_Services_Consent_Received__c == Label.spc_Yes) {
          if (socMap.containsKey(inter.PatientConnect__PC_Participant__c)) {
            envelops.add(triggerImplementation.createEnvelop(inter.PatientConnect__PC_Patient_Program__c
                         , null
                         , Label.spc_ScheduledInfusionDatePreTxPhase
                         , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)}
                         , new Set<Id> {inter.Id}
                         , spc_CommunicationMgr.Channel.Email));
          }
        }
      }

    }
    if (! envelops.isEmpty()) {
      triggerImplementation.sendCommunication(envelops);
    }
  }

  /*********************************************************************************
  * @author       Deloitte
  * @description     afterUpdateInteractionTriggers Send all communication for on Update Interaction Conditions
  * @return Void return type
  *********************************************************************************/
  public void afterUpdateInteractionTriggers(List<PatientConnect__PC_Interaction__c> lstNewInteractions) {

    List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();

    // Scheduled Infusion Date Pre-Tx Phase - Scheduled Infused Date is updated on the Interaction Record
    Set<Id> setProgramCaseIds = new Set<Id>();
    Set<Id> socSet = new Set<Id>();
    List<PatientConnect__PC_Interaction__c> lstAllInteractions = new List<PatientConnect__PC_Interaction__c>();

    for (PatientConnect__PC_Interaction__c interaction : lstNewInteractions) {
      PatientConnect__PC_Interaction__c oldrec = (PatientConnect__PC_Interaction__c)trigger.oldmap.get(interaction.id);
      if (oldrec.spc_Scheduled_Date__c != interaction.spc_Scheduled_Date__c && interaction.spc_Scheduled_Date__c != null) {
        setProgramCaseIds.add(interaction.PatientConnect__PC_Patient_Program__c);
        socSet.add(interaction.PatientConnect__PC_Participant__c);
      }
    }

    lstAllInteractions = [SELECT Id, Name, spc_Scheduled_Date__c, spc_Has_Active_SOC__c, PatientConnect__PC_Patient_Program__c,
                          PatientConnect__PC_Participant__c, PatientConnect__PC_Patient_Program__r.spc_HIPAA_Consent_Received__c,
                          PatientConnect__PC_Patient_Program__r.spc_Patient_Services_Consent_Received__c
                          FROM PatientConnect__PC_Interaction__c
                          WHERE spc_Has_Active_SOC__c = true AND PatientConnect__PC_Patient_Program__c IN :setProgramCaseIds];
    Map<Id, PatientConnect__PC_Association__c> socMap = new Map<Id, PatientConnect__PC_Association__c>();
    for (PatientConnect__PC_Association__c socAssociation : [SELECT Id, PatientConnect__PC_Account__c FROM PatientConnect__PC_Association__c
         WHERE PatientConnect__PC_Program__c = :setProgramCaseIds AND PatientConnect__PC_Account__c = :socSet
             AND PatientConnect__PC_AssociationStatus__c = :spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)]) {
      socMap.put(socAssociation.PatientConnect__PC_Account__c, socAssociation);
    }
    if (!lstAllInteractions.isEmpty()) {
      for (PatientConnect__PC_Interaction__c inter : lstAllInteractions ) {
        if (inter.PatientConnect__PC_Patient_Program__r.spc_HIPAA_Consent_Received__c == Label.spc_Yes
            && inter.PatientConnect__PC_Patient_Program__r.spc_Patient_Services_Consent_Received__c == Label.spc_Yes) {
          if (socMap.containsKey(inter.PatientConnect__PC_Participant__c)) {
            envelops.add(triggerImplementation.createEnvelop(inter.PatientConnect__PC_Patient_Program__c
                         , null
                         , Label.spc_ScheduledInfusionDatePreTxPhase
                         , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)}
                         , new Set<Id> {inter.Id}
                         , spc_CommunicationMgr.Channel.Email));
          }
        }
      }

    }


    if (! envelops.isEmpty()) {
      triggerImplementation.sendCommunication(envelops);
    }
  }


  /*********************************************************************************

  * @author      Deloitte
  * @description   Send all communication for on Insert Proagram coverage Conditions
  * @return      Void
  **/
  public void afterInsertProgramCoverageTriggers(List<PatientConnect__PC_Program_Coverage__c> pCoverages) {

    List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();
    List<PatientConnect__PC_Program_Coverage__c> coveredList = new List<PatientConnect__PC_Program_Coverage__c>();
    List<Id> lstCaseIds = new List<Id>();

    Map<Id, Id> coverageTypeMap = new Map<Id, Id>();

    for (PatientConnect__PC_Program_Coverage__c cov : (List<PatientConnect__PC_Program_Coverage__c>)Trigger.New) {
      lstCaseIds.add(cov.PatientConnect__PC_Program__c);
    }
    Map<Id, Case> mapCases = new Map<Id, Case>([SELECT Id, spc_HIPAA_Consent_Received__c, spc_REMS_Authorization_Status__c, spc_REMS_Enrollment_Status__c  FROM Case WHERE Id IN :lstCaseIds]);

    for (PatientConnect__PC_Program_Coverage__c coverageObj : (List<PatientConnect__PC_Program_Coverage__c>)Trigger.New) {

      if (coverageObj.PatientConnect__PC_Program__c != null) {
        //checking if coverage is of type Copay,status is active and service consent yes
        if (coverageObj.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.PROGRAMCOVERAGE_RT_COPAY, PatientConnect__PC_Program_Coverage__c.SobjectType)) {

          if (coverageObj.PatientConnect__PC_Coverage_Status__c == spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE &&
              mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_HIPAA_Consent_Received__c == Label.spc_Yes) {

            if (coverageObj.PatientConnect__PC_Coverage_Type__c == spc_ApexConstants.PROGRAM_COVERAGE_TYPE_ADMINCOPAY) {
              //sending Enrollmen of Admin Copay to SOC/HIP
              envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                           , null
                           , Label.spc_Enrollment_of_Admin_Co_Pay_to_SOC_HIP
                           , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO)}
                           , new Set<Id> {coverageObj.Id}
                           , spc_CommunicationMgr.Channel.Fax));
            }

            if (coverageObj.PatientConnect__PC_Coverage_Type__c == spc_ApexConstants.PROGRAM_COVERAGE_TYPE_DRUGCOPAY) {
              envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                           , null
                           , Label.spc_Enrollment_of_Drug_Co_Pay_to_SOC_HIP
                           , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO)}
                           , new Set<Id> {coverageObj.Id}
                           , spc_CommunicationMgr.Channel.Fax));
            }
          }
        }

        if (coverageObj.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.PROGRAMCOVERAGE_RT_PAP, PatientConnect__PC_Program_Coverage__c.SobjectType)) {

          if (coverageObj.PatientConnect__PC_Coverage_Status__c == spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE &&
              mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_HIPAA_Consent_Received__c == Label.spc_Yes) {
            envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                         , null
                         , Label.spc_Enrollment_Approval_of_PAP_to_SOC
                         , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO)}
                         ,  new Set<Id> {coverageObj.Id}
                         , spc_CommunicationMgr.Channel.Fax));
            envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                         , null
                         , Label.spc_Enrollment_in_Assistance_Zulresso_Payer_Fax
                         , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_PAYER)}
                         ,  new Set<Id> {coverageObj.Id}
                         , spc_CommunicationMgr.Channel.Fax));

            envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                         , null
                         , Label.spc_EnrollmentApprovalPAPToPatient
                         , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)}
                         , new Set<Id> {coverageObj.Id}
                         , spc_CommunicationMgr.Channel.Mail));
            envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                         , null
                         , Label.spc_EnrollmentApprovalPAPToPatient
                         , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)}
                         ,  new Set<Id> {coverageObj.Id}
                         , spc_CommunicationMgr.Channel.Email));
            if (String.isNotBlank(mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_REMS_Authorization_Status__c)  && mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_REMS_Authorization_Status__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_ENROLLMENT_STATUS_AUTHORIZED))
                && String.isNotBlank(mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_REMS_Enrollment_Status__c) && mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_REMS_Enrollment_Status__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_ENROLLMENT_STATUS_ENROLLED))
               ) {
              //PAP Order Form
              envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                           , null
                           , Label.spc_Free_Drug_Order_Form
                           , new Set<String> {spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.FREE_DRUG_VENDOR)}
                           ,  new Set<Id> {coverageObj.Id}
                           , spc_CommunicationMgr.Channel.Email));
            }
          }
        }
      }
    }
    if (! envelops.isEmpty()) {
      triggerImplementation.sendCommunication(envelops);
    }
  }

  /*********************************************************************************

  * @author       Deloitte
  * @description     Send all communication for on Update Program Coverage Conditions
  * @return     Void
  *********************************************************************************/
  public void afterUpdateProgramCoverageTriggers(List<PatientConnect__PC_Program_Coverage__c> pCoverages) {

    List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();

    Map<ID, ID> parentToChildMap = new Map<ID, ID>();
    List<PatientConnect__PC_Program_Coverage__c> coveredList = new List<PatientConnect__PC_Program_Coverage__c>();
    PatientConnect__PC_Program_Coverage__c oldCoverage;
    Map<Id, Id> coverageTypeMap = new Map<Id, Id>();
    List<Id> lstCaseIds = new List<Id>();
    for (PatientConnect__PC_Program_Coverage__c cov : (List<PatientConnect__PC_Program_Coverage__c>)Trigger.New) {
      lstCaseIds.add(cov.PatientConnect__PC_Program__c);
    }
    Map<Id, Case> mapCases = new Map<Id, Case>([SELECT Id, spc_HIPAA_Consent_Received__c, spc_REMS_Authorization_Status__c, spc_REMS_Enrollment_Status__c FROM Case WHERE Id IN :lstCaseIds]);

    for (PatientConnect__PC_Program_Coverage__c coverageObj : (List<PatientConnect__PC_Program_Coverage__c>)Trigger.New) {
      String coverageRcrdType = Schema.SObjectType.PatientConnect__PC_Program_Coverage__c.getRecordTypeInfosById().get(coverageObj.recordtypeid).getname();
      oldCoverage = Trigger.oldMap != NULL ? (PatientConnect__PC_Program_Coverage__c)Trigger.oldMap.get(coverageObj.Id) : NULL;

      if (coverageObj.PatientConnect__PC_Program__c != null) {
        parentToChildMap.put(coverageObj.PatientConnect__PC_Program__c, coverageObj.Id);

        if (coverageObj.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.PROGRAMCOVERAGE_RT_COPAY, PatientConnect__PC_Program_Coverage__c.SobjectType)) {

          if (coverageObj.PatientConnect__PC_Coverage_Status__c == spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE
              && ( oldCoverage.PatientConnect__PC_Coverage_Status__c != coverageObj.PatientConnect__PC_Coverage_Status__c ||  oldCoverage.PatientConnect__PC_Coverage_Type__c != coverageObj.PatientConnect__PC_Coverage_Type__c)
              && mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_HIPAA_Consent_Received__c == Label.spc_Yes) {

            if (coverageObj.PatientConnect__PC_Coverage_Type__c == spc_ApexConstants.PROGRAM_COVERAGE_TYPE_ADMINCOPAY) {
              envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                           , null
                           , Label.spc_Enrollment_of_Admin_Co_Pay_to_SOC_HIP
                           , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO)}
                           , new Set<Id> {coverageObj.Id}
                           , spc_CommunicationMgr.Channel.Fax));
            }
            if (coverageObj.PatientConnect__PC_Coverage_Type__c == spc_ApexConstants.PROGRAM_COVERAGE_TYPE_DRUGCOPAY) {
              envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                           , null
                           , Label.spc_Enrollment_of_Drug_Co_Pay_to_SOC_HIP
                           , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO)}
                           , new Set<Id> {coverageObj.Id}
                           , spc_CommunicationMgr.Channel.Fax));
            }
          }
        }
        if (coverageObj.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.PROGRAMCOVERAGE_RT_PAP, PatientConnect__PC_Program_Coverage__c.SobjectType)) {

          if (coverageObj.PatientConnect__PC_Coverage_Status__c == spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE
              && oldCoverage.PatientConnect__PC_Coverage_Status__c != coverageObj.PatientConnect__PC_Coverage_Status__c || oldCoverage.PatientConnect__PC_Coverage_Type__c != coverageObj.PatientConnect__PC_Coverage_Type__c
              && mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_HIPAA_Consent_Received__c == Label.spc_Yes) {
            envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                         , null
                         , Label.spc_Enrollment_Approval_of_PAP_to_SOC
                         , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO)}
                         ,  new Set<Id> {coverageObj.Id}
                         , spc_CommunicationMgr.Channel.Fax));
            envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                         , null
                         , Label.spc_EnrollmentApprovalPAPToPatient
                         , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)}
                         , new Set<Id> {coverageObj.Id}
                         , spc_CommunicationMgr.Channel.Mail));
            envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                         , null
                         , Label.spc_EnrollmentApprovalPAPToPatient
                         , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)}
                         ,  new Set<Id> {coverageObj.Id}
                         , spc_CommunicationMgr.Channel.Email));
            if (String.isNotBlank(mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_REMS_Authorization_Status__c)
                && mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_REMS_Authorization_Status__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_ENROLLMENT_STATUS_AUTHORIZED))
                && String.isNotBlank(mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_REMS_Enrollment_Status__c)
                && mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_REMS_Enrollment_Status__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_ENROLLMENT_STATUS_ENROLLED))) {
              //PAP Order Form
              envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                           , null
                           , Label.spc_Free_Drug_Order_Form
                           , new Set<String> {spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.FREE_DRUG_VENDOR)}
                           ,  new Set<Id> {coverageObj.Id}
                           , spc_CommunicationMgr.Channel.Email));
            }
          }
          if (coverageObj.PatientConnect__PC_Coverage_Status__c == spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE
              && oldCoverage.PatientConnect__PC_Coverage_Status__c != coverageObj.PatientConnect__PC_Coverage_Status__c
              && mapCases.get(coverageObj.PatientConnect__PC_Program__c).spc_HIPAA_Consent_Received__c == Label.spc_Yes) {
            envelops.add(triggerImplementation.createEnvelop(coverageObj.PatientConnect__PC_Program__c
                         , null
                         , Label.spc_Enrollment_in_Assistance_Zulresso_Payer_Fax
                         , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_PAYER)}
                         ,  new Set<Id> {coverageObj.Id}
                         , spc_CommunicationMgr.Channel.Fax));

          }
        }
      }
    }

    if (! envelops.isEmpty()) {
      triggerImplementation.sendCommunication(envelops);
    }
  }
  /*********************************************************************************
  * @author       Deloitte
  * @description     create envelop for Lash Fax
  * @return     List<spc_CommunicationMgr.Envelop>

  *********************************************************************************/
  public List<spc_CommunicationMgr.Envelop> automateFaxToLash(Set<Id> programCaseIds) {
    List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();
    Map<Id, Set<String>> mapCaseIdsToAttachmentIds = new Map<Id, Set<String>>();
    //sending automated fax to lash
    for ( PatientConnect__PC_Document_Log__c log : [SELECT
          PatientConnect__PC_Document__r.PatientConnect__PC_Attachment_Id__c,
          PatientConnect__PC_Program__c, PatientConnect__PC_Document__r.zPaper_Fax_Unique_Id__c
          FROM
          PatientConnect__PC_Document_Log__c
          where
          PatientConnect__PC_Document__r.PatientConnect__PC_Document_Category__c includes ('Services Start Form', 'Insurance Information', 'Prescription', 'Patient Consent')
          AND PatientConnect__PC_Program__c in: programCaseIds]) {

      string attachmentId = log.PatientConnect__PC_Document__r.PatientConnect__PC_Attachment_Id__c;
      if (String.isNotBlank(attachmentId)) {
        if (mapCaseIdsToAttachmentIds.containsKey(log.PatientConnect__PC_Program__c)) {
          mapCaseIdsToAttachmentIds.get(log.PatientConnect__PC_Program__c).add(attachmentId);
        } else {
          mapCaseIdsToAttachmentIds.put(log.PatientConnect__PC_Program__c, new Set<String> {attachmentId});
        }
      }
    }
    for (Id caseId : programCaseIds) {
      //D-264109: Recepient Updated. HCO removed from list
      spc_CommunicationMgr.Envelop env = this.createEnvelop(caseId
                                         , null
                                         , Label.spc_Patient_Enrollment_Information_to_Lash
                                         , new Set<String> {spc_ApexConstants.LASH}
                                         , null
                                         , spc_CommunicationMgr.Channel.FAX);
      env.attachmentIds = mapCaseIdsToAttachmentIds.get(caseId);
      envelops.add(env);
    }

    return envelops;
  }
  /*********************************************************************************
  * @author       Deloitte
  * @description     create communication envelop
  * @return     spc_CommunicationMgr.Envelop

  *********************************************************************************/

  public spc_CommunicationMgr.Envelop createEnvelop(string  programCaseId, String sourceId
      , String flowName, Set<String> recipietTypes
      , Set<Id> additionalIds
      , spc_CommunicationMgr.Channel channel) {
    spc_CommunicationMgr.Envelop env = new spc_CommunicationMgr.Envelop();
    env.programCaseId = programCaseId;
    env.sourceId = sourceId;
    env.flowName = flowName;
    env.recipientTypes = recipietTypes;
    env.addtionalObjectIds = additionalIds;
    env.channel = channel;
    return env;
  }
  /*********************************************************************************
  * @author       Deloitte
  * @description     Call spc_CommunicationMgr class to send communication
  * @return     Void

  *********************************************************************************/

  public void sendCommunication(List<spc_CommunicationMgr.Envelop> envelops) {
    spc_CommunicationMgr obj = new spc_CommunicationMgr();
    if (! spc_ApexConstants.IsEWPRunning) {
      obj.send(envelops);
    } else {
      sendCommunicationEWP(Json.serialize(envelops));
    }
  }

  @future
  private static void sendCommunicationEWP(String jsonStr) {
    List<spc_CommunicationMgr.Envelop> envelops = (List<spc_CommunicationMgr.Envelop>)JSON.deserialize(jsonStr,  list<spc_CommunicationMgr.Envelop>.class);
    spc_CommunicationMgr obj = new spc_CommunicationMgr();
    obj.send(envelops);
  }
  /*********************************************************************************
  * @author       Deloitte
  * @description     Call sendPAPForm class to check Coverage Status
  * @return     Void

  *********************************************************************************/

  public List<spc_CommunicationMgr.Envelop> sendPAPForm(Set<Id> programCaseCoverageId, boolean FreeDrugOrder) {
    List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();
    if (! programCaseCoverageId.isEmpty()) {
      for (PatientConnect__PC_Program_Coverage__c pCov : [Select id, PatientConnect__PC_Program__c, RecordTypeId,
           PatientConnect__PC_Coverage_Status__c FROM
           PatientConnect__PC_Program_Coverage__c WHERE
           RecordTypeId = :spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.PROGRAMCOVERAGE_RT_PAP, PatientConnect__PC_Program_Coverage__c.SobjectType)
                          AND PatientConnect__PC_Coverage_Status__c = : spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE
                              AND PatientConnect__PC_Program__c IN:programCaseCoverageId]) {
        if (FreeDrugOrder) {
          envelops.add(triggerImplementation.createEnvelop(pCov.PatientConnect__PC_Program__c
                       , null
                       , Label.spc_Free_Drug_Order_Form
                       , new Set<String> {spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.FREE_DRUG_VENDOR)}
                       , new Set<Id> {pCov.Id}
                       , spc_CommunicationMgr.Channel.Email));
        } else {
          envelops.add(triggerImplementation.createEnvelop(pCov.PatientConnect__PC_Program__c
                       , null
                       , Label.spc_Enrollment_in_Assistance_Zulresso_Payer_Fax
                       , new Set<String> {spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_PAYER)}
                       ,  new Set<Id> {pCov.Id}
                       , spc_CommunicationMgr.Channel.Fax));
        }
      }


    }
    return envelops;
  }

}
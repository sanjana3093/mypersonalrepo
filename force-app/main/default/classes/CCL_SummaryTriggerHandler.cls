/*
*  @author          Deloitte
*  @description     This class is the trigger handler for the Summary trigger
*  @date            Aug 13, 2020
*  @version         1.0
Modification Log:
--------------------------------------------------------------------------------------------------------------
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------
Deloitte                  Aug 13 2020         Initial Version
*/
public without sharing class CCL_SummaryTriggerHandler {


  /*
   *   @author           Deloitte
   *   @description      Retieve the Date Value and Timezone and output the NVS format in the text field
   *   @date             Aug 13, 2020
   */
    public void updateDateAndTimeFields(List<CCL_Summary__c> summaryRecords) {

    for(CCL_Summary__c summaryRecord:summaryRecords) {
       if(summaryRecord.RecordTypeId == CCL_StaticConstants_MRC.SUMMARY_RECORDTYPE_MANUFACTURING) {
            if(summaryRecord.CCL_Manufacturing_End_Date_Time__c!=null && summaryRecord.CCL_Time_Zone__c!=null) {
                    summaryRecord.CCL_Manufacturing_End_Date_Time_Text__c= CCL_Utility.getdisplayDateTimeValues(summaryRecord.CCL_Manufacturing_End_Date_Time__c,summaryRecord.CCL_Time_Zone_Text__c);
                }
                if(summaryRecord.CCL_Manufacturing_Start_Date_Time__c!=null && summaryRecord.CCL_Time_Zone__c!=null) {
                    summaryRecord.CCL_Manufacturing_Start_Date_Time_Text__c= CCL_Utility.getdisplayDateTimeValues(summaryRecord.CCL_Manufacturing_Start_Date_Time__c,summaryRecord.CCL_Time_Zone_Text__c);
                }
            }
        }
    }

  /*
   *   @author           Deloitte
   *   @description      CGTU#2121, update Manufacturing Start Date on Order with related Summary MAX Manufacturing Start Date
   *   @date             Nov 10, 2020
   */
    public static void updateStartDateonOrder(List<CCL_Summary__c> sumList){

        if(!CCL_StaticConstants_MRC.sumryNotFrstRun) {
            /*Set static variable to true so that it does not allow re entry*/
            CCL_StaticConstants_MRC.sumryNotFrstRun = true;
        Set<Id> orderIds = new Set<Id>();
        List<CCL_Order__c> orderToUpdate = new List<CCL_Order__c>();

        for(CCL_Summary__c sum :sumList){
            orderIds.add(sum.CCL_Order__c);
        }
        orderIds.remove(null);
        String RecordTypeId= Schema.Sobjecttype.CCL_Summary__c.getRecordTypeInfosByName().get('Manufacturing').getRecordTypeId();

        for(AggregateResult ar :[SELECT CCL_Order__c,MAX(CCL_Order__r.CCL_Estimated_Manufacturing_Start_Date__c) maxOrder, MAX(CCL_Manufacturing_Start_Date_Time__c) maxDate
                                 FROM CCL_Summary__c
                                 WHERE CCL_Order__c IN :orderIds AND RecordTypeId =:RecordTypeId
                                 GROUP BY CCL_Order__c]){

                                     CCL_Order__c ord = new CCL_Order__c();
                                     ord.Id= (Id)ar.get('CCL_Order__c');
                                     ord.CCL_Estimated_Manufacturing_Start_Date__c= (Date)ar.get('maxOrder');

                                     Date mxDate = (DateTime)ar.get('maxDate') != null ? ((DateTime)ar.get('maxDate')).date() : null;

                                     if(ord.CCL_Estimated_Manufacturing_Start_Date__c > mxDate){
                                         ord.CCL_Manufacturing_Start_Date_Time__c = ord.CCL_Estimated_Manufacturing_Start_Date__c;
                                     }else{
                                         ord.CCL_Manufacturing_Start_Date_Time__c = (DateTime)ar.get('maxDate');
                                     }

                                     orderToUpdate.add(ord);
                                 }
        if(Schema.sObjectType.CCL_Order__c.isUpdateable()){
                update orderToUpdate;
            }
    }
    }
}
public without sharing class CCL_Invocable_Integration {
    
    /*invokeIntegrationProcess Method get's data from Process Builder and Invokes the 
       Utility class method processOutboundIntegration*/
    
    @InvocableMethod(label='Invoke OutBound Integration Process' description='Method to invoke the Integration Process')
    public static void invokeIntegrationProcess(List<CCL_Integration_Invocable_Wrapper> integrationWrprList)
    {
        ID jobID = System.enqueueJob(new CCL_Async_IntegrationCallout(integrationWrprList));
    }
	
	public static void methodToBeCalledFromAsyncIntegrationProcess(List<CCL_Integration_Invocable_Wrapper> integrationWrprList)
    {
        if(integrationWrprList!= null && integrationWrprList.size()>0)
        {
            List<CCL_Integration_Dto> integrationDTOList = new List<CCL_Integration_Dto>();
            integrationDTOList=CCL_Invocable_Integration.populateDTO(integrationWrprList);

            if(integrationDTOList!=null && integrationDTOList.size()>0){
                CCL_Integration_Utility.processOutboundIntegration(integrationDTOList);
            }
        }
    }
    
    /*Prepares the DTO List to be sent to Utility Method for processing.*/
    
    public static List<CCL_Integration_Dto> populateDTO (List<CCL_Integration_Invocable_Wrapper> wrapperValuesList)
    {
        List<CCL_Integration_Dto> listOfRecordDTO = new List<CCL_Integration_Dto>();
        
        CCL_Integration_Message_Setting__mdt metadataRec=null;
        
        Map<String,CCL_Integration_Message_Setting__mdt> metadataMap = 
            CCL_Integration_Accessor.accessMetadataRecords();
        
        for(CCL_Integration_Invocable_Wrapper integrationWraprVal : wrapperValuesList )
        {
           	CCL_Integration_Dto intWrapperdto = new  CCL_Integration_Dto();
            
            metadataRec=metadataMap.get(integrationWraprVal.eventType);
            
            intWrapperdto.eventType = integrationWraprVal.eventType;
            intWrapperdto.recordId = integrationWraprVal.recordId;
            intWrapperdto.metadataRecord=metadataRec;
            
            listOfRecordDTO.add(intWrapperdto);
            
        }
        return listOfRecordDTO;
        
    }
    
    public class CCL_Integration_Invocable_Wrapper {
    
        @invocableVariable(label='Event Type')
        public String  eventType;
    
        @invocableVariable(label='Record Id')
        public Id recordId;
       
    }
    

}
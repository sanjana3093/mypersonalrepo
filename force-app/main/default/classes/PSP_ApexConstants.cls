/*********************************************************************************************************
class Name      : PSP_ApexConstants 
Description		: Apex Constant Class
@author		    : Saurabh Tripathi
@date       	: July 10, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi            July 10, 2019          Initial Version
Sulata Sadhu                Dec, 2019          Initial Version
****************************************************************************************************************/ 
public virtual class PSP_ApexConstants {
    /* Constant for LOCALE setting for US */
    /* Constant for LOCALE setting access */
    Public Static final String ACCESS_ALL ='All';
    /* Constant for LOCALE US */
    Public Static final String LOCALE_US = 'US';
    /* Constant for permission set program and catalogu */
    Public Static final String PROGRAM_CATALOGUE ='PSP_Program_and_Catalogue_Set_up';
    /* Constant for Adverse Events */
    public static final String ADVERSE_EVENT = 'Adverse Event';
    /* Constant for New status */
    public static final String STATUS_NEW = 'New';
    /* Constant for Active status */
    public static final String STATUS_ACTIVE = 'Active';
    /* Variable for map of Strings */
    private static    Map < String, String > mapLiterals = new Map<String, String>();
    /* Variable for map of String and Id */
    private static   Map < String, String > mapRTLiterals  = new Map<String, Id>();
    
    /*Constructor method to initialize variables  */ 
    private  PSP_ApexConstants() {
        //put constructor code
        
    }
    /**************************************************************************************
* @author      : Deloitte
* @date        : 09/25/2019
* @Description : 
* @Param       : record type and sObjecttype
* @Return      : Id
***************************************************************************************/
    public static Id getRTId(RecordTypeName eVal, SObjectType objectType) {
        
        final String val =  String.valueOf(eVal);  
        if (mapLiterals.isEmpty() || ! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, PSP_ApexConstantsSetting__c.getInstance(val).PSP_Value__c);
        }
        final string rtName =  mapLiterals.get(val);
        if (! mapRTLiterals.containsKey(objectType + ':' + rtName)) {
            //system.debug(objectType.getDescribe().getRecordTypeInfosByDeveloperName().get(rtName).getRecordTypeId());
            mapRTLiterals.put(objectType + ':' + rtName
                              , objectType.getDescribe().getRecordTypeInfosByDeveloperName().get(rtName).getRecordTypeId());
        }
        //system.debug(mapRTLiterals.get(objectType + ':' + rtName) + 'fjgj');
        return  mapRTLiterals.get(objectType + ':' + rtName);
    }
    /**************************************************************************************
* @author      :Deloitte
* @date        : 09/25/2019
* @Description : Restricts mexico user to insert or update distributor account records
* @Param       : enum value
* @Return      : String
* @Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Divya Eduvulapati          October 25, 2019        Added enum for  Card recordtype of Card and Coupon object
***************************************************************************************/
    public static string getValue(PicklistValue eVal) {
        //System.debug('Test Value:'+eVal);
        final String val = String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, PSP_ApexConstantsSetting__c.getInstance(val).PSP_Value__c);
        }
        return mapLiterals.get(val);
    }
    
    /***********Enum to decalre record types*************/
    public enum RecordTypeName {
        PRODUCT_RT_PRODUCT,
            CASE_RT_ADVERSE_EVENTS,
            ACCOUNT_RT_BUSINESS,
            ACCOUNT_RT_PATIENT,
            CARDANDCOUPON_RT_CARD,
            CAREPROGRAM_RT_FAMILY,
           	ACCOUNT_RT_CAREGIVER,
            CONTACT_RT_HCP,
            CASE_RT_CAREPLAN,
            TASK_RT_CAREPLAN
            }
    
    /***********Enum to decalre variables for account validation*************/
    public enum PicklistValue {
        ACCOUNT_TYPE_CLINIC,
            ACCOUNT_TYPE_HOSPITAL,
            ACCOUNT_TYPE_PRO_MNFCR,
            ACCOUNT_TYPE_ACCESS_ALL,
            PROG_CAT_PERMSN_SET_NAME,
            OP_SUPP_FOR_HEALTH_CLOUD_PER_SET_NAM,
            PATIENT_ENG_PARTNR_PERM_SET,
            ACC_TYP_PRODMANUF,
            ACC_TYP_HOSPTAL,
            ACC_TYP_CLINIC,
            ACC_TYP_PATNT,
            ACC_TYP_PHRM,
            ACC_TYP_OTHR,
            ACC_TYP_HOUSHLD,
            ACC_TYP_PHYSOFCE,
            ACC_TYP_TESTLAB,
            ACC_TYP_DSTRIBTR,
            ACC_ALLACCSS,
            ACC_NOACCSS,
            ACC_CUSTMACCSS,
            ACC_SOURCE,
            LOCALE_MX,
            PICKLIST_VAL_NONE,
            OBJECT_NAME_CPEP,
            PICKLIST_VAL_ASSIGNED,
            PROFILE_NAME_ADMIN,
            PICKLIST_VAL_SIGNED,
            OBJ_NAME_ACCOUNT,
            OBJ_NAME_CONTACT,
            OBJ_NAME_CPE,
            OBJ_NAME_INDIVIDUAL,
            OBJ_NAME_USER,
			CASE_PV_IN_PROGRESS,          
            CASE_PV_OTHER,
            CASE_PV_NORMAL,
            PICKLIST_VAL_PRODUCT,
            PICKLIST_VAL_DAILY,
            PICKLIST_VAL_MONTHLY,
            PICKLIST_VAL_WEEKLY,
            PICKLIST_VAL_ANNUAL,
            PICKLIST_VAL_PURCHASED,
            PICKLIST_VAL_AVAILABLE,
            PICKLIST_VAL_PHYSICAL,
            OBJ_NAME_CAREPROGRAM,
            CAREPROGRAM_FLD_NAME,
            PICKLIST_VAL_STATUS			
            }
    
}
/**
* @author Deloitte
* @date 08/08/2018
*
* @description This is the Test Class for SPC_eLetterAuraController
*/
@isTest
public class spc_eLetterAuraControllerTest {
    @testSetup static void setup() {

        // create a record for the custom setting
        PatientConnect__PC_System_Settings__c cSetting = new PatientConnect__PC_System_Settings__c();
        cSetting.PatientConnect__PC_eLetter_Sender_Class__c = MOCK_SENDER;
        spc_Database.ins(cSetting);

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        spc_ApexConstantsSetting__c cs = spc_ApexConstantsSetting__c.getInstance('ELETTER_SENDER_CLASS');
        cs.spc_Value__c = 'spc_MockELEtterSenderTest';
        update cs;

        insert new spc_Communication_framework_Parameters__c(SetupOwnerId = UserInfo.getOrganizationId(), Lash_Fax_Number__c = 'test');

    }
    public static final String MOCK_SENDER = 'spc_Test_MockELetterSender';



    @isTest
    static void getLetters() {

        // Create a Manufacturer account


        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);

        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Email Template');
        insert eletter;
        // Create a Engagement Eletter Program

        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Email', program.ID);
        insert peletter;

        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.Name = 'Physician Name';
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.PatientConnect__PC_Email__c = 'test@test.com';
        account.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        account.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        account.spc_Text_Consent__c = 'Yes';
        account.spc_Services_Text_Consent__c = 'Yes';
        account.spc_REMS_Text_Consent__c = 'Yes';
        account.spc_HIPAA_Consent_Received__c = 'Yes';
        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        insert programCase;

        PatientConnect__PC_Association__c association = spc_eLetterAuraControllerTest.createAssociation(account, programCase, 'Treating Physician', Date.today().addDays(1));

        List<spc_eLetter_Recipient> getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, programCase.Id, false);

        System.assertEquals(account.PatientConnect__PC_Email__c, getRecepients[0].recipientEmail);
        String recepients = '[{"bDisableEmail":false,"bDisableFax":false,"bDisableSendToMe":true,"bDisableSMS":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PatientConnect__PC_Email__c":"English","PatientConnect__PC_Email__c":"test@a.com","Type":"Specialist","Fax":"12345","Name":"Name."},"recipientType":"Specialist","recipientEmail":"aiii@biii.com","sendEmail":true,"sendFax":false,"sendToMe":false,"sendSMS":false}]';

        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);

        System.assertNotEquals(returnMessage, null);

        peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Fax', program.ID);
        insert peletter;
        account = spc_Test_Setup.createTestAccount('spc_Caregiver');
        account.Name = 'Test Caregiver';
        account.spc_REMS_Text_Consent__c = 'Yes';
        account.spc_HIPAA_Consent_Received__c = 'Yes';
        account.spc_Patient_Services_Consent_Received__c = 'Yes';
        insert account;
        programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        insert programCase;
        association = spc_eLetterAuraControllerTest.createAssociation(account, programCase, 'Caregiver', Date.today().addDays(1));
        association.PatientConnect__PC_AssociationStatus__c = 'Active';
        spc_eLetterAuraController.getRecepients(eletter.Id, programCase.Id, true);
    }

    private static PatientConnect__PC_Association__c createAssociation(Account account, Case caseObj, String roleName, Date endDate) {
        PatientConnect__PC_Association__c association = new PatientConnect__PC_Association__c (PatientConnect__PC_Account__c = account.Id, PatientConnect__PC_Program__c = caseObj.Id, PatientConnect__PC_Role__c = roleName, PatientConnect__PC_EndDate__c = endDate);
        return (PatientConnect__PC_Association__c)spc_Database.ins(association);
    }

    @isTest
    static void testSendToMe() {

        // Create a Manufacturer account

        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');

        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);

        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Fax Template');
        insert eletter;
        // Create a Engagement Eletter Program

        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Fax', program.ID);
        insert peletter;
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.Name = 'Physician Name';
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.Fax = '12345';
        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        insert programCase;
        PatientConnect__PC_Association__c association = spc_eLetterAuraControllerTest.createAssociation(account, programCase, 'Treating Physician', Date.today().addDays(1));

        List<spc_eLetter_Recipient> getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, programCase.Id, false);
        System.assertEquals(account.PatientConnect__PC_Email__c, getRecepients[0].recipientEmail);
        String recepients = '[{"bDisableEmail":true,"bDisableFax":true,"bDisableSendToMe":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PatientConnect__PC_Communication_Language__c":"English","Type":"Specialist","PatientConnect__PC_Email__c":"test@a.com","Name":"Name."},"recipientEmail":"aiii@biii.com","recipientType":"Specialist","sendEmail":false,"sendFax":false,"sendToMe":true}]';
        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);

        System.assertNotEquals(returnMessage, null);
    }


    @isTest
    static void testSendFax() {

        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');

        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Fax Template');
        insert eletter;
        // Create a Engagement Eletter Program

        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Fax', program.ID);
        insert peletter;
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.Name = 'Physician Name';
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.PatientConnect__PC_Email__c = 'test@test.com';
        account.spc_HIPAA_Consent_Received__c = 'Yes';
        account.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf('2018-06-07');
        account.spc_Patient_Services_Consent_Received__c = 'Yes';
        account.spc_Patient_Service_Consent_Date__c = Date.valueOf('2018-06-07');
        account.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        account.spc_Text_Consent__c = 'Yes';
        account.spc_Services_Text_Consent__c = 'Yes';
        account.spc_REMS_Text_Consent__c = 'Yes';
        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        insert programCase;
        spc_eLetterAuraController.getLetters(programCase.Id, 'actioncenter', false);
        spc_eLetterAuraController.getLetters(programCase.Id, System.Label.spc_inquiry, false);
        PatientConnect__PC_Association__c association = spc_eLetterAuraControllerTest.createAssociation(account, programCase, 'Treating Physician', Date.today().addDays(1));

        List<spc_eLetter_Recipient> getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, programCase.Id, false);
        System.assertEquals(account.PatientConnect__PC_Email__c, getRecepients[0].recipientEmail);
        String recepients = '[{"bDisableEmail":false,"bDisableFax":false,"bDisableSendToMe":false,"bDisableSMS":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PC_Communication_Language__c":"English","Type":"Specialist","PC_Email__c":"test@a.com","Name":"Name."},"recipientType":"Specialist","sendEmail":true,"sendFax":false,"sendToMe":true,"sendSMS":false}]';

        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);

        System.assertNotEquals(returnMessage, null);
    }
    @isTest
    static void testNoRecepients() {

        // Create a Manufacturer account
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Email Template');
        eletter.PatientConnect__PC_Allow_Send_to_Patient__c = true;
        insert eletter;
        // Create a Engagement Eletter Program

        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Email', program.ID);
        insert peletter;
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.Name = 'Physician Name';
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.PatientConnect__PC_Email__c = 'test@test.com';
        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        spc_Database.ins(programCase);
        String recepients = '[{"bDisableEmail":false,"bDisableFax":false,"bDisableSendToMe":false,"bDisableSMS":false,"communicationLanguage":"English","recipient":{},"recipientType":"Specialist","sendEmail":false,"sendFax":true,"sendToMe":false,"sendSMS":false}]';

        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);
        System.assertNotEquals(returnMessage, null);
    }

    @isTest
    static void enquiryEletter() {

        Id accountRTypeId = spc_System.recordTypeId(spc_Test_Setup.ACCOUNT_OBJ, 'PC_Patient');
        Account manAcc = spc_Test_Setup.createAccount(accountRTypeId);
        manAcc.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        manAcc.spc_Patient_Marketing_Consent_Date__c = Date.valueOf('2014-06-07');
        manAcc.spc_HIPAA_Consent_Received__c = 'Yes';
        manAcc.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf('2018-06-07');
        manAcc.spc_Patient_Services_Consent_Received__c = 'Yes';
        manAcc.spc_Patient_Service_Consent_Date__c = Date.valueOf('2018-06-07');
        manAcc.spc_Services_Text_Consent__c = Label.spc_Verbal_Consent_Received;
        spc_Database.ins(manAcc);

        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Inquiry', 'Email Template');
        insert eletter;

        // Create a Engagement Eletter Program

        Case inquiryCase  = new Case();
        inquiryCase.AccountId = manAcc.Id;
        inquiryCase.RecordTypeId = spc_System.recordTypeId('Case', 'spc_Inquiry');
        inquiryCase.Status = 'Open';
        insert inquiryCase;

        spc_eLetterAuraController.getLetters(inquiryCase.Id, 'inquiry', false);

        List<spc_eLetter_Recipient> getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, inquiryCase.Id, false);
        String recepients = '[{"bDisableEmail":false,"bDisableFax":false,"bDisableSendToMe":false,"bDisableSMS":false,"communicationLanguage":"English","recipient":{},"recipientType":"Patient","sendEmail":true,"sendFax":true,"sendToMe":false,"sendSMS":false}]';

        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, inquiryCase.Id);
        System.assertNotEquals(returnMessage, null);
    }

    @isTest
    static void testSendSMS() {
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Fax Template');
        eletter.PatientConnect__PC_Send_to_User__c = true;
        insert eletter;
        // Create a Engagement Eletter Program

        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Fax', program.ID);
        insert peletter;
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.Name = 'Physician Name';
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.PatientConnect__PC_Email__c = 'test@test.com';
        account.Phone = '1234567898';
        insert account;

        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        insert programCase;
        spc_eLetterAuraController.getLetters(programCase.Id, 'actioncenter', false);
        PatientConnect__PC_Association__c association = spc_eLetterAuraControllerTest.createAssociation(account, programCase, 'Treating Physician', Date.today().addDays(1));

        List<spc_eLetter_Recipient> getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, programCase.Id, false);
        System.assertEquals(account.PatientConnect__PC_Email__c, getRecepients[0].recipientEmail);
        String recepients = '[{"bDisableEmail":false,"bDisableFax":false,"bDisableSendToMe":true,"bDisableSMS":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PatientConnect__PC_Email__c":"English","PatientConnect__PC_Email__c":"test@a.com","Type":"Specialist","Fax":"12345","Name":"Name."},"recipientType":"Specialist","sendEmail":true,"sendFax":false,"sendToMe":false,"sendSMS":false}]';


        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);

        System.assertNotEquals(returnMessage, null);
    }
    public static testmethod void  testExecution1() {

        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');

        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);

        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Email Template');

        insert eletter;
        // Create a Engagement Eletter Program

        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Email', program.ID);

        insert peletter;


        Account account = spc_Test_Setup.createTestAccount('PC_Patient');

        account.Name = 'Patient Name';

        account.PatientConnect__PC_Email__c = 'test@test.com';
        account.spc_Patient_Mrkt_and_Srvc_consent__c = System.Label.spc_No;
        account.spc_Patient_Marketing_Consent_Date__c = System.today();
        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;

        insert programCase;

        Case preTreamentCase = new Case();
        preTreamentCase.RecordTypeId = spc_System.recordTypeId('Case', 'spc_Post_Treatment');
        preTreamentCase.Status = 'Open';
        preTreamentCase.PatientConnect__PC_Program__c = programCase.id;
        test.startTest();
        insert preTreamentCase;

        spc_eLetterAuraController.getLetters(preTreamentCase.Id, 'Inquiry', true);
        test.stopTest();

    }

    public static testmethod void  testExecution3() {

        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');

        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);

        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Email Template');

        insert eletter;
        // Create a Engagement Eletter Program

        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Email', program.ID);

        insert peletter;


        Account account = spc_Test_Setup.createTestAccount('PC_Patient');

        account.Name = 'Patient Name';

        account.PatientConnect__PC_Email__c = 'test@test.com';
        account.spc_Patient_Mrkt_and_Srvc_consent__c = System.Label.spc_No;
        account.spc_Patient_Marketing_Consent_Date__c = System.today();
        insert account;

        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;

        insert programCase;
        
        spc_Additional_eLetter__c addeLetter = new spc_Additional_eLetter__c();
        addeLetter.spc_eLetter__c = eletter.Id;
        addeLetter.spc_Source_Object__c = 'Case';
        addeLetter.spc_Object_Record_Types__c = 'Inquiry';
        addeLetter.spc_Consent_Eletter_Needed__c = 'Service Consent;REMS Consent';
        addeLetter.spc_All_Consents_Mandatory__c = true;
        insert addeLetter;
        
        Case inquiryCase  = new Case();
        inquiryCase.AccountId = account.Id;
        inquiryCase.RecordTypeId = spc_System.recordTypeId('Case', 'spc_Inquiry');
        inquiryCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        inquiryCase.Status = 'Open';
        insert inquiryCase;
        system.debug('inquirycase'+inquiryCase);
        spc_eLetterAuraController.getLetters(inquiryCase.Id,'inquiry',false);
        
        Case preTreamentCase=new Case();
        preTreamentCase.RecordTypeId = spc_System.recordTypeId('Case', 'spc_Post_Treatment');
        preTreamentCase.Status = 'Open';
        preTreamentCase.PatientConnect__PC_Program__c = programCase.id;
        test.startTest();
        insert preTreamentCase;

        spc_eLetterAuraController.getLetters(programCase.Id, 'Inquiry', false);
        test.stopTest();

    }
    public static testmethod void  testExecution2() {

        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');

        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('BREX', manAcc.ID);

        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Email Template');

        insert eletter;
        // Create a Engagement Eletter Program

        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Email', program.ID);

        insert peletter;


        Account account = spc_Test_Setup.createTestAccount('spc_Caregiver');

        account.Name = 'Caregiver Name';

        account.PatientConnect__PC_Email__c = 'test@test.com';
        account.spc_Patient_Mrkt_and_Srvc_consent__c = System.Label.spc_No;

        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;

        test.startTest();
        insert programCase;
        spc_eLetterAuraController.getLetters(programCase.Id, 'Inquiry', true);
        String recepients = '[{"bDisableEmail":true,"bDisableFax":true,"bDisableSendToMe":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PatientConnect__PC_Communication_Language__c":"English","Type":"Designated Caregiver","PatientConnect__PC_Email__c":"test@a.com","Name":"Name."},"recipientEmail":"aiii@biii.com","recipientType":"Designated Caregiver","sendEmail":true,"sendFax":false,"sendToMe":true}]';
        spc_eLetterAuraController.sendOutboundDocuments('[{"id":""}]', 'None', null);
        test.stopTest();

    }
    public static testmethod void testExecution() {
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;

        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);

        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Email Template');

        insert eletter;
        // Create a Engagement Eletter Program

        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Email', program.ID);

        insert peletter;


        Account account = spc_Test_Setup.createTestAccount('spc_Caregiver');

        account.Name = 'Caregiver Name';

        account.PatientConnect__PC_Email__c = 'test@test.com';
        account.spc_Patient_Mrkt_and_Srvc_consent__c = System.Label.spc_No;

        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;

        insert programCase;
        PatientConnect__PC_Association__c association = spc_eLetterAuraControllerTest.createAssociation(account, programCase, 'Designated Caregiver', Date.today().addDays(1));

        List<spc_eLetter_Recipient> getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, programCase.Id, false);

        String recepients = '[{"bDisableEmail":true,"bDisableFax":true,"bDisableSendToMe":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PatientConnect__PC_Communication_Language__c":"English","Type":"Designated Caregiver","PatientConnect__PC_Email__c":"test@a.com","Name":"Name."},"recipientEmail":"aiii@biii.com","recipientType":"Designated Caregiver","sendEmail":true,"sendFax":false,"sendToMe":true}]';
        test.startTest();

        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);
        test.stopTest();
    }

    @isTest
    static void getLetters2() {

        // Create a Manufacturer account


        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Email Template');

        insert eletter;
        // Create a Engagement Eletter Program

        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Email', program.ID);
        //spc_Database.ins(peletter);
        insert peletter;

        // Id accountRecordTypeId = ID_PHYSICIAN_RECORDTYPE;
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        // Account account = new Account();
        account.Name = 'Physician Name';
        // account.RecordTypeId = accountRecordTypeId;
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.PatientConnect__PC_Email__c = 'test@test.com';
        account.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        account.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        account.spc_Text_Consent__c = 'Yes';
        account.spc_Services_Text_Consent__c = 'Yes';
        account.spc_REMS_Text_Consent__c = 'Yes';
        account.spc_HIPAA_Consent_Received__c = 'Yes';
        // spc_Database.ins(account);
        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        //spc_Database.ins(programCase);
        insert programCase;

        PatientConnect__PC_Association__c association = spc_eLetterAuraControllerTest.createAssociation(account, programCase, 'Treating Physician', Date.today().addDays(1));

        List<spc_eLetter_Recipient> getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, programCase.Id, false);
        //getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, preTrtCase.Id, false);

        System.assertEquals(account.PatientConnect__PC_Email__c, getRecepients[0].recipientEmail);
        String recepients = '[{"bDisableEmail":false,"bDisableFax":false,"bDisableSendToMe":true,"bDisableSMS":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PatientConnect__PC_Email__c":"English","PatientConnect__PC_Email__c":"test@a.com","Type":"Specialist","Fax":"12345","Name":"Name."},"recipientType":"Specialist","recipientEmail":"aiii@biii.com","sendEmail":true,"recipientFax":"9848022338","sendFax":true,"sendToMe":false,"sendSMS":false}]';

        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);

        System.assertNotEquals(returnMessage, null);

        peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Fax', program.ID);
        insert peletter;
        account = spc_Test_Setup.createTestAccount('spc_Caregiver');
        account.Name = 'Test Caregiver';
        insert account;
        programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        insert programCase;
        association = spc_eLetterAuraControllerTest.createAssociation(account, programCase, 'Caregiver', Date.today().addDays(1));
        association.PatientConnect__PC_AssociationStatus__c = 'Active';
        spc_eLetterAuraController.getRecepients(eletter.Id, programCase.Id, true);
    }


}
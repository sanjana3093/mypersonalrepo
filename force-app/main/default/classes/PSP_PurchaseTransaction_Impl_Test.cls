/*********************************************************************************************************
class Name      : PSP_PurchaseTransaction_Impl_Test 
Description		: Test class for purchase transaction trigger related classes
@author		    : Saurabh Tripathi
@date       	: September 18, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi           September 18, 2019          Initial Version
****************************************************************************************************************/
@isTest
public class PSP_PurchaseTransaction_Impl_Test {
    
     /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 09/18/2019
  	* @Description : Test Method for before insert for PSP_PurchaseTransaction_Trigger_Impl.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/
    private static testmethod void insertWithConsistency() {
        Database.insert(PSP_Test_Setup.createCard());
        Database.insert(PSP_Test_Setup.createContact());
            try {
            Database.insert(PSP_Test_Setup.createPurchasetransaction());  
            } catch (DmlException e) {
			System.assert ( e.getMessage().contains('Insert failed.'),e.getMessage() );
		} 
    }
    
    
     /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 09/18/2019
  	* @Description : Test Method for before insert for PSP_PurchaseTransaction_Trigger_Impl.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/
    private static testmethod void insertWithInConsistency() {
        final List<PSP_Purchase_Transaction__c> purchTrns = new List<PSP_Purchase_Transaction__c> {new PSP_Purchase_Transaction__c(Name='Test')};
            try {
            Database.insert(purchTrns);  
            } catch (DmlException e) {
			System.assert ( e.getMessage().contains('Insert failed.'),e.getMessage() );
		} 
    }
}
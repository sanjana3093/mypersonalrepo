/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the test class for CaseTriggerHandler
*  @date            09/18/2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                        Description
--------------------------------------------------------------------------------------------------------------            
Sanjana Tripathy            September 18, 2019          Initial Version
****************************************************************************************************************/
@IsTest
public class PSP_CaseTriggerHandlerTest {
    /*Constant for test */
    static final String TEST = 'Test';
    /* Static list for initializing apex constants */
    static final List<PSP_ApexConstantsSetting__c>  APEX_CONSTANTS =  PSP_Test_Setup.setDataforApexConstants();
	/*************************************************************************************
    * @author      : Deloitte
    * @date        : 09/17/2019
    * @Description : Testing insert scenarios
    * @Param       : Void
    * @Return      : Void
    ***************************************************************************************/
    @IsTest 
    static void testInsertScenarios () {
        
        /*Test setup method being instantiated */
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram (TEST,null);
        Database.insert(cP1);
        /*Test setup method being instantiated */
        final CareProgramEnrollee cPE2 = PSP_Test_Setup.createTestCareProgramEnrollment (TEST,CP1.Id);
        Database.insert(cPE2);
        /*Test setup method being instantiated */
        final Product2 pro = PSP_Test_Setup.createProduct (TEST);
        Database.insert (pro);
        /*Test setup method being instantiated */
        final CareProgramProduct cpp = PSP_Test_Setup.createTestCareProgramProduct (cP1,pro);
        Database.insert (cpp);
        final CareProgramEnrolleeProduct cPEP = PSP_Test_Setup.createTestCareProgramEnrolleeProduct ('TEST',cpp.Id,cPE2.Id);
        Database.insert (cPEP);  
        final Case newCase=PSP_Test_Setup.createTestAdverseEventCase(TEST,cPEP.Id);
        try {
            Database.insert (newCase);  
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains ('Insert failed.'),e.getMessage () );
        }
    }
}
/**
* @author Deloitte
* @date 12-June-18
*
* @description Wrapper classes for Eletter
*/

public class spc_eLetter {
  public interface Sender {
    void sendLetter(Id objectId, List<DocumentWrapper> documentRecipients,
                    PatientConnect__PC_eLetter__c eLetter, Boolean sendToCurrentUser);
  }

  /*******************************************************************************************************
    * @description  Wrapper class for Document
    *
    */

  public class DocumentWrapper {
    public Id patientAccountId;
    public Account recipient;
    public PatientConnect__PC_Document__c doc;

    public DocumentWrapper(Id patientAccountId, Account recipient, PatientConnect__PC_Document__c doc) {
      this.patientAccountId = patientAccountId;
      this.recipient = recipient;
      this.doc = doc;
    }
  }
}
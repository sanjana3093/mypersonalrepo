/**************************************************************************
@description    : Test class for spc_eLetterAuraController
@author         : Deloitte
@date           : July 30, 2018
*/
@isTest
public class spc_SMSeLetterAuraControllerTest {
    public static final String MOCK_SENDER = 'spc_Test_MockELetterSender';
    @testSetup static void setup() {
        // create a record for the custom setting
        PatientConnect__PC_System_Settings__c cSetting = new PatientConnect__PC_System_Settings__c();
        cSetting.PatientConnect__PC_eLetter_Sender_Class__c = MOCK_SENDER;
        spc_Database.ins(cSetting);
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
    }

    /*********************************************************************************
    @description    : Test create new user functionality
    @return         : void
    @param          : None
    */
    @isTest
    static void getLetters() {
        Id ID_MANUFACTURER_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_MANUFACTUR)).getRecordTypeId();
        Id ID_PHYSICIAN_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PHYSICIAN)).getRecordTypeId();
        // Create a Manufacturer account
        Id accountRTypeId = ID_MANUFACTURER_RECORDTYPE;
        Account manAcc = spc_Test_Setup.createAccount(accountRTypeId);
        spc_Database.ins(manAcc);

        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        spc_Database.ins(program);

        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'SMS Template');
        spc_Database.ins(eletter);

        // Create a Engagement Eletter Program

        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'SMS', program.ID);
        spc_Database.ins(peletter);

        Id accountRecordTypeId = ID_PHYSICIAN_RECORDTYPE;
        Account account = new Account();
        account.Name = 'Physician Name';
        account.RecordTypeId = accountRecordTypeId;
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.Phone = '1234567890';
        spc_Database.ins(account);
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        spc_Database.ins(programCase);
        PatientConnect__PC_Association__c association = spc_SMSeLetterAuraControllerTest.createAssociation(account, programCase, 'Treating Physician', Date.today().addDays(1));
        List<spc_SMSeLetter_Recipient> getRecepients =  spc_SMSeLetterAuraController.getRecepients(eletter.Id, programCase.Id);
        System.assertEquals(account.Phone, getRecepients[0].recipientSMS);
        String recepients = '[{"bDisableSendToMe":true,"bDisableSMS":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PatientConnect__PC_Email__c":"English","Phone":"1234567890","PatientConnect__PC_Email__c":"test@a.com","Type":"Specialist","Fax":"1234567890","Name":"Name."},"recipientType":"Specialist","recipientSMS":"1234567890","sendToMe":true,"sendSMS":true}]';
        String returnMessage = spc_SMSeLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);
        System.assertNotEquals(returnMessage, null);
    }

    /*********************************************************************************
    @description    : create Association
    @return         : void
    @param          : None
    */
    private static PatientConnect__PC_Association__c createAssociation(Account account, Case caseObj, String roleName, Date endDate) {
        PatientConnect__PC_Association__c association = new PatientConnect__PC_Association__c (PatientConnect__PC_Account__c = account.Id, PatientConnect__PC_Program__c = caseObj.Id, PatientConnect__PC_Role__c = roleName, PatientConnect__PC_EndDate__c = endDate);
        return (PatientConnect__PC_Association__c)spc_Database.ins(association);
    }

    /*********************************************************************************
    @description    : Test send eLetter to user
    @return    : void
    @param      : None
    */
    //@isTest
    static void testNoRecepients() {
        Id ID_MANUFACTURER_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_MANUFACTUR)).getRecordTypeId();
        Id ID_PHYSICIAN_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PHYSICIAN)).getRecordTypeId();
        // Create a Manufacturer account
        Id accountRTypeId = ID_MANUFACTURER_RECORDTYPE;
        Account manAcc = spc_Test_Setup.createAccount(accountRTypeId);
        spc_Database.ins(manAcc);
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        spc_Database.ins(program);
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'SMS Template');
        eletter.PatientConnect__PC_Allow_Send_to_Patient__c = true;
        spc_Database.ins(eletter);
        // Create a Engagement Eletter Program
        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'SMS', program.ID);
        spc_Database.ins(peletter);
        Id accountRecordTypeId = ID_PHYSICIAN_RECORDTYPE;
        Account account = new Account();
        account.Name = 'Physician Name';
        account.RecordTypeId = accountRecordTypeId;
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.Phone = '1234567890';
        spc_Database.ins(account);
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        spc_Database.ins(programCase);
        String recepients = '[{"bDisableSendToMe":false,"bDisableSMS":false,"communicationLanguage":"English","recipient":{},"recipientSMS":"1234567890","recipientType":"Specialist","sendEmail":false,"sendFax":true,"sendToMe":false,"sendSMS":true}]';
        String returnMessage = spc_SMSeLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);
        System.assertNotEquals(returnMessage, null);
    }

    /*********************************************************************************
    @description    : Test send eLetter to user from Enquiry Case
    @return         : void
    @param          : None
    */
    @isTest
    static void enquiryEletter() {
        Id accountRTypeId = spc_System.recordTypeId(spc_Test_Setup.ACCOUNT_OBJ, 'PC_Patient');
        Account manAcc = spc_Test_Setup.createAccount(accountRTypeId);
        manAcc.spc_HIPAA_Consent_Received__c = System.Label.spc_Yes;
        manAcc.spc_Patient_HIPAA_Consent_Date__c = System.today() - 20;
        manAcc.spc_Text_Consent__c = System.Label.spc_Yes;
        manAcc.spc_REMS_Text_Consent_Date__c = System.today() - 20;
        manAcc.spc_Patient_Text_Consent_Date__c = System.today() - 20;
        manAcc.spc_Services_Text_Consent__c = System.Label.spc_Yes;
        manAcc.spc_Services_Text_Consent_Date__c = System.today() - 20;
        manAcc.spc_REMS_Text_Consent__c = System.Label.spc_Yes;

        manAcc.spc_Patient_Services_Consent_Received__c = System.Label.spc_Yes;
        manAcc.spc_Patient_Mrkt_and_Srvc_consent__c = System.Label.spc_Yes;
        manAcc.spc_Marketing_Consent_Date__c  = System.today() - 20;
        manAcc.spc_Patient_Marketing_Consent_Date__c = System.today() - 20;
        manAcc.spc_Patient_Service_Consent_Date__c = System.today() - 20;
        spc_Database.ins(manAcc);
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Inquiry', 'Email Template');
        spc_Database.ins(eletter);
        // Create a Engagement Eletter Program
        Case inquiryCase  = new Case();
        inquiryCase.AccountId = manAcc.Id;
        inquiryCase.RecordTypeId = spc_System.recordTypeId('Case', 'spc_Inquiry');
        inquiryCase.Status = 'Open';
        spc_Database.ins(inquiryCase);
        spc_SMSeLetterAuraController.getLetters(inquiryCase.Id);
        List<spc_SMSeLetter_Recipient> getRecepients =  spc_SMSeLetterAuraController.getRecepients(eletter.Id, inquiryCase.Id);
        String recepients = '[{"bDisableSendToMe":false,"bDisableSMS":false,"communicationLanguage":"English","recipient":{},"recipientType":"Patient","sendEmail":true,"sendFax":true,"sendToMe":false,"sendSMS":false}]';
        String returnMessage = spc_SMSeLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, inquiryCase.Id);
        System.assertNotEquals(returnMessage, null);
    }

    /*********************************************************************************
    @description      : Test send SMS to user
    @return           : void
    @param            : None
    */
    @isTest
    static void testSendSMS() {
        Id ID_MANUFACTURER_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_MANUFACTUR)).getRecordTypeId();
        Id ID_PHYSICIAN_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PHYSICIAN)).getRecordTypeId();
        Id accountRTypeId = ID_MANUFACTURER_RECORDTYPE;
        Account manAcc = spc_Test_Setup.createAccount(accountRTypeId);
        spc_Database.ins(manAcc);
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        spc_Database.ins(program);
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Fax Template');
        eletter.PatientConnect__PC_Send_to_User__c = true;
        spc_Database.ins(eletter);
        // Create a Engagement Eletter Program
        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'SMS', program.ID);
        spc_Database.ins(peletter);
        Id accountRecordTypeId = ID_PHYSICIAN_RECORDTYPE;
        Account account = new Account();
        account.Name = 'Physician Name';
        account.RecordTypeId = accountRecordTypeId;
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.Phone = '1234567890';
        account.Phone = '123456789067898';
        account.spc_HIPAA_Consent_Received__c = 'Yes';
        account.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf('2018-06-07');
        account.spc_Patient_Services_Consent_Received__c = 'Yes';
        account.spc_Patient_Service_Consent_Date__c = Date.valueOf('2018-06-07');
        spc_Database.ins(account);
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        spc_Database.ins(programCase);
        spc_SMSeLetterAuraController.getLetters(programCase.Id);
        PatientConnect__PC_Association__c association = spc_SMSeLetterAuraControllerTest.createAssociation(account, programCase, 'Treating Physician', Date.today().addDays(1));
        List<spc_SMSeLetter_Recipient> getRecepients =  spc_SMSeLetterAuraController.getRecepients(eletter.Id, programCase.Id);
        System.assertEquals(account.Phone, getRecepients[0].recipientSMS);
        String recepients = '[{"bDisableSendToMe":true,"bDisableSMS":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PatientConnect__PC_Email__c":"English","PatientConnect__PC_Email__c":"test@a.com","Type":"Specialist","Fax":"1234567890","Name":"Name."},"recipientType":"Specialist","sendToMe":true,"sendSMS":true}]';
        String returnMessage = spc_SMSeLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);
        System.assertNotEquals(returnMessage, null);
    }

    @isTest
    static void testSendSMS1() {
        Id ID_MANUFACTURER_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_MANUFACTUR)).getRecordTypeId();
        Id ID_PHYSICIAN_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PHYSICIAN)).getRecordTypeId();
        Id accountRTypeId = ID_MANUFACTURER_RECORDTYPE;
        Account manAcc = spc_Test_Setup.createAccount(accountRTypeId);
        spc_Database.ins(manAcc);
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        spc_Database.ins(program);
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Fax Template');
        eletter.PatientConnect__PC_Send_to_User__c = true;
        eletter.spc_All_Consents_Mandatory__c = false;
        spc_Database.ins(eletter);
        // Create a Engagement Eletter Program
        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'SMS', program.ID);
        spc_Database.ins(peletter);
        //Patient Account
        Account account = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        account.PatientConnect__PC_Email__c = 'test@email.com';
        account.spc_HIPAA_Consent_Received__c = 'Yes';
        account.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf('2018-06-07');
        account.spc_Patient_Services_Consent_Received__c = 'Yes';
        account.spc_Patient_Service_Consent_Date__c = Date.valueOf('2018-06-07');
        account.spc_Services_Text_Consent__c = 'Verbal Consent Received';
        account.spc_Services_Text_Consent_Date__c = System.today() - 20;
        account.spc_Patient_Mrkt_and_Srvc_consent__c = 'Verbal Consent Received';
        account.spc_Patient_Marketing_Consent_Date__c = System.today() - 20;
        account.Phone = '123456789067890';
        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        spc_Database.ins(programCase);
        spc_SMSeLetterAuraController.getLetters(programCase.Id);
        account.spc_HIPAA_Consent_Received__c = '';
        account.spc_Patient_Services_Consent_Received__c = '';
        account.spc_Services_Text_Consent__c = '';
        account.spc_Patient_Mrkt_and_Srvc_consent__c = '';
        update account;
        spc_SMSeLetterAuraController.getLetters(programCase.Id);
        String recepients = '[{"bDisableSendToMe":true,"bDisableSMS":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PatientConnect__PC_Email__c":"English","PatientConnect__PC_Email__c":"test@a.com","Type":"Specialist","Fax":"1234567890","Phone":"123456789067890","Name":"Name."},"recipientType":"Specialist","sendToMe":true,"sendSMS":true}]';
        spc_SMSeLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);
    }

}
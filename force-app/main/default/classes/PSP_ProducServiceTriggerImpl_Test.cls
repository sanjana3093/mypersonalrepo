/*********************************************************************************************************
class Name      : PSP_Prod_And_Service_Triggr_Implement 
Description		: Trigger Implementation Class for PSP_Prod_And_Service_Triggr_Implement
@author		    : Saurabh Tripathi
@date       	: July 10, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi           July 10, 2019          Initial Version
Divya Eduvulapati			November 18,2019	   version1
****************************************************************************************************************/
@isTest
private class PSP_ProducServiceTriggerImpl_Test {
    
     /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 07/12/2019
  	* @Description : Test Method for before insert for PSP_Prod_And_Service_Triggr_Implement negative scenario.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/

	private static testmethod void checkDuplicateNameInsert() {
        //product instantiation
       	final Product2 prodInstance1 = new Product2 (Name='Prod_1', PSP_SKU__c='Test',PSP_Quantity_per_SKU__c=1,PSP_ID__c='Unique_ID1');
         //product instantiation
       	final Product2 prodInstance2 = new Product2 (Name='Prod_1', PSP_SKU__c='Test123',PSP_Quantity_per_SKU__c=5,PSP_ID__C='Uniquet_ID2');         
		Database.insert (prodInstance1);
    	try {
			Database.insert (prodInstance2);
		} catch ( DmlException e) {
			System.assert ( e.getMessage().contains('Insert failed.'),e.getMessage() );
		} 
	}
    
     /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 07/12/2019
  	* @Description : Test Method for before insert for PSP_Prod_And_Service_Triggr_Implement negative scenario.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/

	private static testmethod void checkDuplicaeIdInsert() {
        //product instantiation
       	final Product2 prodInstance1 = new Product2 (Name='Test_14', PSP_SKU__c='Test1',PSP_Quantity_per_SKU__c=1,PSP_ID__c='Same_ID');
         //product instantiation
       	final Product2 prodInstance2 = new Product2 (Name='Test_15', PSP_SKU__c='Test123',PSP_Quantity_per_SKU__c=5,PSP_ID__C='Same_ID');         
		Database.insert (prodInstance1);
    	try {
			Database.insert (prodInstance2);
		} catch ( DmlException e) {
			System.assert ( e.getMessage().contains('Insert failed.'),e.getMessage() );
		} 
	}
         /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 07/12/2019
  	* @Description : Test Method for before insert for PSP_Prod_And_Service_Triggr_Implement positive scenario.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/

	private static testmethod void allowInsert() {
        //product instantiation
       	final Product2 prodInstance1 = new Product2 (Name='Test_4', PSP_SKU__c='Test2',PSP_Quantity_per_SKU__c=1,PSP_ID__c='ID1',StockKeepingUnit='Test45');
         //product instantiation
       	final Product2 prodInstance2 = new Product2 (Name='Test_5', PSP_SKU__c='Test123',PSP_Quantity_per_SKU__c=5,PSP_ID__C='ID2',StockKeepingUnit='Test45');         
		Database.insert (prodInstance1);
    	try {
			Database.insert (prodInstance2);
		} catch ( DmlException e) {
			System.assert ( e.getMessage().contains('Insert failed.'),e.getMessage() );
		} 
	}
    
     /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 07/12/2019
  	* @Description : Test Method for before update for PSP_Prod_And_Service_Triggr_Implement.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/
    
    private static testmethod void checkDuplicateNameOnUpdate () {
         //product instantiation
        final Product2 prodInstance =new Product2 (Name='Test2', PSP_SKU__c='TestSKU3',PSP_Quantity_per_SKU__c=1,PSP_SKU_Units__c='Ampoules',PSP_ID__c='TestId',StockKeepingUnit='TestSKU');
   		Database.insert (prodInstance); 
		try {
            //product instantiation
      		final Product2 productToUpdate = [Select Id,Name,PSP_SKU__c,PSP_SKU_Units__c,StockKeepingUnit,PSP_ID__c from product2 where Name='Test2' limit 1][0];
    		productToUpdate.PSP_SKU__c='NewValue';
            productToUpdate.StockKeepingUnit='NewValueSKU';
    		Database.update (productToUpdate);
		} catch (DmlException e) {
			System.assert ( e.getMessage().contains('Update failed.'),e.getMessage() );
		} 
	} 
    
      /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 07/12/2019
  	* @Description : Test Method for before update for PSP_Prod_And_Service_Triggr_Implement.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/
    
    private static testmethod void checkDuplicateIdOnUpdate () {
         //product instantiation
        final Product2 prodInstance =new Product2 (Name='TestUnique', PSP_SKU__c='TestSKU2',PSP_Quantity_per_SKU__c=1,PSP_ID__c='TestId');
   		Database.insert (prodInstance); 
		try {
            //product instantiation
      		final Product2 productToUpdate = [Select Id,Name,PSP_SKU__c,PSP_SKU_Units__c,PSP_ID__c from product2 where Name='TestUnique' limit 1][0];
    		productToUpdate.Name='TestUnique2';
    		Database.update (productToUpdate);
		} catch (DmlException e) {
			System.assert ( e.getMessage().contains('Update failed.'),e.getMessage() );
		} 
	} 

       /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 07/12/2019
  	* @Description : Test Method for before update for PSP_Prod_And_Service_Triggr_Implement.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/
    
    private static testmethod void allowUpdate() {
         //product instantiation
        final Product2 prodInstance =new Product2 (Name='TestUnique', PSP_SKU__c='TestSKU1',PSP_Quantity_per_SKU__c=1,PSP_ID__c='TestId');
   		Database.insert (prodInstance); 
		try {
            //product instantiation
      		final Product2 productToUpdate = [Select Id,Name,PSP_SKU__c,PSP_SKU_Units__c,PSP_ID__c from product2 where Name='TestUnique' limit 1][0];
    		productToUpdate.Name='TestUnique2';
            productToUpdate.PSP_ID__c='TestIdUn';
    		Database.update (productToUpdate);
		} catch (DmlException e) {
			System.assert ( e.getMessage().contains('Update failed.'),e.getMessage() );
		} 
	}
}
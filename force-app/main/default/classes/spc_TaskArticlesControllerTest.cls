/********************************************************************************************************
*  @author          Deloitte
*  @date            30/07/2018
*  @description     This is the test class for spc_TaskArticlesController
*  @version         1.0
*********************************************************************************************************/
@IsTest
public class spc_TaskArticlesControllerTest {

    @testSetup static void setupTestdata() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
    }

    @IsTest static void testGetArticlesByTaskId() {
        Test.startTest();
        Task taskObj = new Task();
        taskObj.PatientConnect__PC_Category__c = 'BI/BV';
        insert taskObj;
        spc_DataCategories__c dataCategoryCustSett = new spc_DataCategories__c();
        dataCategoryCustSett.Name = 'BI/BV';
        dataCategoryCustSett.spc_DataCategoryAPIName__c = 'BI_BV__c';
        insert dataCategoryCustSett;
        spc_TaskArticlesController.getArticlesByTaskId(taskObj.Id);
        Test.stopTest();
    }

}

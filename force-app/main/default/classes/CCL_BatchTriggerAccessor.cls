public with sharing class CCL_BatchTriggerAccessor {
  public static List<CCL_Finished_Product__c> fetchfinishedProductRecords(set<Id> finishedProductIds) {
       List<CCL_Finished_Product__c> fpList= new List<CCL_Finished_Product__c>();
      try{
        if (!finishedProductIds.isEmpty()  && Schema.sObjectType.CCL_Finished_Product__c.isAccessible()){
         fpList=[select Id, Name, CCL_Shipment__c from CCL_Finished_Product__c where Id IN:finishedProductIds WITH SECURITY_ENFORCED];
        }
      }
      catch (System.QueryException qe) {
      System.debug('An exception occurred with fetchfinishedProductRecords: ' + qe.getMessage());
      }
         
        return fpList;
            
     }
     
  public static List<AggregateResult> aggregateResults(set<Id> shipmentIds) {
        List<AggregateResult> aggregateResultsList = new List<AggregateResult>();
     try{
        if (!shipmentIds.isEmpty()  && Schema.sObjectType.CCL_Batch__c.isAccessible()){
         aggregateResultsList =[select  CCL_Finished_Product__r.CCL_Shipment__c, count(Id)batch from CCL_Batch__c where CCL_Finished_Product__r.CCL_Shipment__c IN:shipmentIds GROUP BY CCL_Finished_Product__r.CCL_Shipment__c];
        }
      }
      catch (System.QueryException qe) {
      System.debug('An exception occurred with aggregateResultsRecords: ' + qe.getMessage());
      }
         
        return aggregateResultsList ;
     }
     
  public static void updateShipmentRecords(List<CCL_Shipment__c> shipmentRecordListToBeUpdated)
    {
         if(Schema.sObjectType.CCL_Shipment__c.isUpdateable()) {
			update shipmentRecordListToBeUpdated;
      }
    }
     

}
/********************************************************************************************************
*  @author          Deloitte
*  @description     Data transfer object created to support the notification.
*  @date            July 16, 2020
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Deloitte                  July 16 2020         Initial Version
****************************************************************************************************************/
public with sharing class CCL_Notification_DTO {
   
  /*
   *   @author           Deloitte
   *   @description      Created to support the invocable variable "NotificationType" for the notification engine. 
   *   @date             21 July 2020
   */
    public String NotificationType;

  /*
   *   @author           Deloitte
   *   @description      Created to support the invocable variable "recordId" for the notification engine.  
   *   @date             21 July 2020
   */
    public Id recordId;

  /*
   *   @author           Deloitte
   *   @description      Created to support the invocable variable "AccountId" for the notification engine. 
   *   @date             21 July 2020
   */
    public Id AccountId;

  /*
   *   @author           Deloitte
   *   @description      Created to support the invocable variable "TherapyId" for the notification engine.  
   *   @date             21 July 2020
   */
    public Id TherapyId;

  /*
   *   @author           Deloitte
   *   @description      Created to support flexibility for the notification engine.  
   *   @date             21 July 2020
   */
  public SObject genericSObject;

  /*
   *   @author           Deloitte
   *   @description      Created to support the creation get unique key for Notifications.  
   *   @date             28 July 2020
   */
  public string Category;

  /*
   *   @author           Deloitte
   *   @description      Created to get Notification setting metadata record based on unique key.  
   *   @date             28 July 2020
   */
  public CCL_Notification_Settings__mdt notificationSettingMetadataRecord;

  /*
   *   @author           Deloitte
   *   @description      Created to get Notification Content metadata record based on unique key.  
   *   @date             28 July 2020
   */
  public CCL_Notification_Content__mdt notificationContentMetadataRecord;


  /*
   *   @author           Deloitte
   *   @description      This will have unique values per DTO instance. 
   *                     relatedSiteAccountIdSet
   *   @date             28 July 2020
   */
  public Set<Id> relatedSiteAccountIdSet;

  /*
   *   @author           Deloitte
   *   @description      This will have unique values per DTO instance. 
   *                     It will be a combination of Record Id +'~'+Notification Setting Metadata developer Name
   *   @date             28 July 2020
   */
  public string uniqueKeyForDTOInstance;

   /*
   *   @author           Deloitte
   *   @description      This will have sObject API Name to which the current record Id belongs.
   *   @date             28 July 2020
   */
  public string objectAPIName;


  /*
   *   @author           Deloitte
   *   @description      This will be a sObject list which will hold a list of SObject which will have User Preference.
   *   @date             28 July 2020
   */
  public List<SObject>  userPreferenceObjectList= new List<SObject>();

 /*
   *   @author           Deloitte
   *   @description      This will be a uniqueKey made up of Record Id +'~'+Notification Setting Metadata developer Name
   *                      for Each Email Template. 
   *   @date             28 July 2020
   */
  public EmailTemplate EmailTemplateRecord;
  
 /*
   *   @author           Deloitte
   *   @description      This will hold the email content of the resolved merged fields for the email body.
   *   @date             4 August 2020
   */
  public String emailBody;

  /*
  *   @author           Deloitte
  *   @description      This will hold the email content of the resolved merged fields for the subject.
  *   @date             4 August 2020
  */
  public String emailSubject;


}
/**
* @author Deloitte
* @date May 31, 2018
*
* @description This is the test class for spc_CreateDocumentController
*/
@isTest
public class spc_CreateDocumentControllerTest {
           
    static void setupTestdata() {        
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);    
        Account oPat = spc_Test_Setup.createPatient('TestPatient');
        oPat.PatientConnect__PC_Date_of_Birth__c=system.today()-200;
        insert oPat;
        list<account> lstPatient=new list<account>();
        lstPatient.add(oPat);
        Account lstManf = spc_Test_Setup.createTestAccount('Manufacturer'); 
        insert lstManf;
        PatientConnect__PC_Engagement_Program__c  lstEngPrgm  = spc_Test_Setup.createEngagementProgram('TestEp',lstManf.id);
        insert lstEngPrgm;
        
        List<case> lstCasesPrgm = spc_Test_Setup.createCases(lstPatient, 1,'PC_Program');
        insert lstCasesPrgm;                           
    }
    
    @IsTest public static void testCreateDocuments(){
        
        setupTestdata();
        test.startTest();        
        Case[] testCase = [select Id,type from Case limit 1];
        List<String> categories = spc_CreateDocumentController.getDocumentTypes();
        List<String> programs = spc_CreateDocumentController.getEngagementPrograms();
        system.assertequals(false,testCase.isEmpty());
        spc_CreateDocumentController.RemoteResponse responsse 
                    = spc_CreateDocumentController.createDocument(
                            testCase[0].Id
                            , 'Test'
                            , EncodingUtil.base64Encode(blob.valueOf('test'))
                            ,'text'
                            , categories[1],'test'
                            , 'TestEp'
                            , '2017-11-15T23:00:00.000Z'
                            , 'Patient'                                                                              
                            ,'');
       test.stopTest(); 
        
    }
    
    @IsTest public static void testCreateDocumentsFromAccount(){
        
        setupTestdata();
        test.startTest();
        Account[] testAcc = [select Id, Name, Type FROM Account limit 5];
        List<String> programs = spc_CreateDocumentController.getEngagementPrograms();
        List<String> categories = spc_CreateDocumentController.getDocumentTypes();
        system.assertequals(false,testAcc.isEmpty());
        spc_CreateDocumentController.RemoteResponse responsse 
                    = spc_CreateDocumentController.createDocument(
                            testAcc[0].Id
                            , 'Test'
                            , EncodingUtil.base64Encode(blob.valueOf('test'))
                            ,'text'
                            , categories[1]
                            ,'test'
                            , 'TestEp'
                            , '2017-11-15T23:00:00.000Z'
                            , 'HCP'
                            ,''
                                              );
       test.stopTest(); 
        
    }
}
/*********************************************************************************************************
class Name      : PSP_PurchaseTransaction_Impl_Test 
Description		: Test class for account trigger related classes
@author		    : Saurabh Tripathi
@date       	: September 18, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi           September 18, 2019          Initial Version
****************************************************************************************************************/
@isTest
public class PSP_AccountTrggrImpl_Test {
    /* Getting person account patient record type id */
    private static Id recTypPatient = [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'].Id;
    /* Static list for initializing apex constants */
    static final List<PSP_ApexConstantsSetting__c>  APEX_CONSTANTS =  PSP_Test_Setup.setDataforApexConstants();
  /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 09/18/2019
  	* @Description : Test Method for before insert for PSP_PurchaseTransaction_Trigger_Impl.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/
    private static testmethod void triggerTestMethodBeforeInsert() {
        	Test.startTest(); 
        
        
       		final List<Account> accList = new List<Account> {new Account(Name='TestSK1Hos',PSP_Account_Type__c='Hospital'),
               new Account(Name='TestSK1Clinic',PSP_Account_Type__c='Clinic'),
               new Account(Name='TestSK1Othr',PSP_Account_Type__c='Other'),
               new Account(Name='TestSK1Insur',PSP_Account_Type__c='Insurance Company'),
               new Account(Name='TestSK1Pharm',PSP_Account_Type__c='Pharmacy'),
               new Account(Name='TestSK1House',PSP_Account_Type__c='Household'),
               new Account(Name='TestSK1PO',PSP_Account_Type__c='Physician Office'),
               new Account(Name='TestSK1TL',PSP_Account_Type__c='Testing Laboratory'),
               new Account(Name='TestSK1Dist',PSP_Account_Type__c='Distributor'),
               new Account(Name='TestSK1Dist',PSP_Account_Type__c='Product Manufacturer'),
               new Account(Name='TestSK1patient',RecordTypeId=recTypPatient)};
               try {
                   Database.insert(accList); 
               } catch(DmlException e) {
                   System.debug('skms error:- '+e);
               }
        	Test.stopTest();
        	System.assertEquals(0, [Select count() from Account where PSP_Account_Type__c='TestSK1Clinic' ],'checking number of DML Statements');
       
    }
    /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 09/18/2019
  	* @Description : Test Method for before update for PSP_PurchaseTransaction_Trigger_Impl.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/
    private static testmethod void triggerTestMethodBeforeUpdate() {
        Test.startTest();
        final Account acco = PSP_Test_Setup.getPersonAccount ('Test');
        Database.insert(acco);
        final Account acc= [Select Id from Account where recordtypeid= :recTypPatient limit 1];
        acc.phone='123456';
        try {
            Database.update(acc); 
        } catch(DmlException e) {
            System.debug('skms error:- '+e);
        }
        Test.stopTest();
        system.assertEquals( Limits.getDmlStatements(), 2, 'checking number of DML Statements');
    }
}
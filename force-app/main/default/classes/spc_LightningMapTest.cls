/*****************************************************************************
@description     : Test class for spc_LightningMap
@author   Deloitte
@date     August 06, 2018
*********************************************************************************/
@isTest
public class spc_LightningMapTest {

	@isTest
	static void testFunctionalityMethod() {
		test.startTest();
		spc_LightningMap lightningInst = new spc_LightningMap('testName', 'testValue');
		System.assert(lightningInst != NULL);
		spc_LightningMap lightningInst1 = new spc_LightningMap('testName', 'testValue', true, false);
		System.assert(lightningInst1 != NULL);
		test.stopTest();
	}

}
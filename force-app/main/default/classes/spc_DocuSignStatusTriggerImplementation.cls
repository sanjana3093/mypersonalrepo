/**
* @author Deloitte
* @date July 03, 2018
*
* @description Implementation class for spc_DocuSignStatusTrigger
*/

public class spc_DocuSignStatusTriggerImplementation  {

	/*******************************************************************************************************
	* @description  This function process docuSign status record and create Email Inbound/Outbound document record.
							if docusign status is Sent/Created then email outbound, status is Compelted then email inbound
	* @param Attachment
	*/
	public void processStatus(Map<String, dsfs__DocuSign_Status__c> mapDocuSignStatus
	                          , spc_ApexConstants.RecordTypeName docRT, spc_ApexConstants.PicklistValue docStatus) {
		try {
			String documentIdPrefix = PatientConnect__PC_Document__c.sObjectType.getDescribe().getKeyPrefix();
			Map<String, String> mapDocuSignEnvelope = new Map<String, String>();
			//Added by Pratik for creating dcoc log
			// Added to get Document vs Program Map
			Map<Id, Id> docToCaseMap = new Map<Id, Id>();
			Map<Id, String> mapEnvelopDocuments = new Map<Id, String>();
			//Find Envelope record for related docuSign status
			for (dsfs__DocuSign_Envelope__c dEnvlope : [SELECT
			        ID, dsfs__Source_Object__c, dsfs__DocuSign_Envelope_ID__c
			        , (SELECT ID, dsfs__Attachment_ID__c FROM dsfs__DocuSign_Documents__r)
			        FROM
			        dsfs__DocuSign_Envelope__c
			        Where
			        dsfs__DocuSign_Envelope_ID__c in :mapDocuSignStatus.keySet()
			        AND dsfs__Source_Object__c like : ( documentIdPrefix + '%')]) {
				mapDocuSignEnvelope.put(dEnvlope.dsfs__Source_Object__c, dEnvlope.dsfs__DocuSign_Envelope_ID__c.toLowerCase());
				for (dsfs__DocuSign_Envelope_Document__c eDoc : dEnvlope.dsfs__DocuSign_Documents__r) {
					mapEnvelopDocuments.put(eDoc.dsfs__Attachment_ID__c, dEnvlope.dsfs__Source_Object__c);
				}
			}
			//Added by Pratik for creating doc log
			// Added map for document and Program Case
			for (PatientConnect__PC_Document_Log__c docLog : [Select Id, PatientConnect__PC_Document__c, PatientConnect__PC_Program__c from PatientConnect__PC_Document_Log__c
			        where PatientConnect__PC_Document__c IN : mapDocuSignEnvelope.keySet()]) {
				docToCaseMap.put(docLog.PatientConnect__PC_Document__c, docLog.PatientConnect__PC_Program__c);
			}
			//if envelop exist for PC_Document__c
			if (! mapDocuSignEnvelope.isEmpty()) {
				//Create PC_EmailOutbound Document with status = Send with
				Map<String, PatientConnect__PC_Document__c> emailOutboundDocuments = new Map<String, PatientConnect__PC_Document__c>();
				//Added by Pratik for creating doc log
				List<PatientConnect__PC_Document_Log__c> outboundDocLogs = new List<PatientConnect__PC_Document_Log__c>();
				//Find PC_Document__C (original document)
				for (PatientConnect__PC_Document__c doc : [SELECT
				        ID, spc_Program_Case_Id__c, PatientConnect__PC_Description__c, PatientConnect__PC_Document_Category__c, PatientConnect__PC_Engagement_Program__c
				        FROM
				        PatientConnect__PC_Document__c
				        WHERE
				        ID in :mapDocuSignEnvelope.keySet()]) {

					PatientConnect__PC_Document__c emailOutbound = doc.clone(false, false, false, false);
					emailOutbound.RecordTypeId = spc_ApexConstants.getRecordTypeId(docRT, PatientConnect__PC_Document__c.sObjectType);
					emailOutbound.PatientConnect__PC_Parent_Document__c = doc.Id;
					if (!docToCaseMap.isEmpty()) {
						if (docToCaseMap.get(doc.Id) != null) {
							emailOutbound.spc_Program_Case_Id__c = docToCaseMap.get(doc.Id);
							emailOutbound.PatientConnect__PC_Description__c = doc.PatientConnect__PC_Description__c;
						}
					}
					emailOutbound.PatientConnect__PC_Document_Status__c = spc_ApexConstants.getValue(docStatus);//'Sent By DocuSign';
					emailOutboundDocuments.put(doc.Id, emailOutbound);
				}
				// added by pratik to create Doc Log
				insert emailOutboundDocuments.values();

				for (string docId : emailOutboundDocuments.keySet()) {
					// added by pratik to create Doc Log
					if (!docToCaseMap.isEmpty()) {
						if (docToCaseMap.get(docId) != null) {
							PatientConnect__PC_Document_Log__c docLog = new PatientConnect__PC_Document_Log__c();
							docLog.PatientConnect__PC_Program__c = docToCaseMap.get(docId);
							docLog.PatientConnect__PC_Document__c = emailOutboundDocuments.get(docId).Id;
							outboundDocLogs.add(docLog);
						}
					}
					if (mapDocuSignEnvelope.containsKey(docId)) {
						string envelopId = mapDocuSignEnvelope.get(docId);
						if (mapDocuSignStatus.containsKey(envelopId)) {
							dsfs__DocuSign_Status__c dStatus = mapDocuSignStatus.get(envelopId);
							if (docRT == spc_ApexConstants.RecordTypeName.DOC_RT_EMAIL_OUTBOUND) {
								dStatus.spc_Outbound_Document__c = emailOutboundDocuments.get(docId).Id;
							} else {
								dStatus.spc_Inbound_Document__c = emailOutboundDocuments.get(docId).Id;
							}


						}
					}
				}
				if (!outboundDocLogs.isEmpty()) {
					insert outboundDocLogs;
				}
				if (docRT == spc_ApexConstants.RecordTypeName.DOC_RT_EMAIL_OUTBOUND) {
					this.createOutboundAttachment(emailOutboundDocuments, mapEnvelopDocuments);
				}
			}
		} catch (Exception ex) {
			spc_Utility.createExceptionLog(ex);
		}
	}

	/*******************************************************************************************************
	* @description  Find related attachments and create for Email outbound
	* @param Map<String, PatientConnect__PC_Document__c>
	* @param Map<Id, String>
	* @return void
	*/
	@testVisible
	private void createOutboundAttachment(Map<String, PatientConnect__PC_Document__c> emailOutboundDocuments , Map<Id, String> mapEnvelopDocuments) {
		List<Attachment> attachmentsToInsert  = new List<Attachment>();
		for (Attachment attach : [SELECT ID, Body, ContentType, Name, ParentId, isPrivate FROM Attachment where Id in : mapEnvelopDocuments.keySet()]) {
			string sourceId = mapEnvelopDocuments.get(attach.Id);
			if (emailOutboundDocuments.containsKey(sourceId)) {
				Attachment clonned = attach.clone(false, false, false);
				clonned.ParentId = emailOutboundDocuments.get(sourceId).Id;
				attachmentsToInsert.add(clonned);
			}
		}
		insert attachmentsToInsert;

	}
}
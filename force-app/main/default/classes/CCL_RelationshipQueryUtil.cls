public with sharing class CCL_RelationshipQueryUtil {

    /**
     * @description: Allows you to fetch the Parent Relationship Record Values while using the .get method on SObject
     * @param : SObject target, String fieldName
     * @return : Object
     **/

    public static Object getFieldValue(SObject target, String fieldName) {
        Object fieldValue;
        try {
            //1. Check if Field Name Contains .dot. This means that it is a relationship Field
            if (fieldName != null && fieldName.contains('.')) {
                List < String > relationshipAPINames = new List < String > ();
                relationshipAPINames = fieldName.split('\\.');
                System.debug('relationship API Name##'+relationshipAPINames);
                //2. If there are any relationship fields
                if (!relationshipAPINames.isEmpty()) {
                    SObject nextParentSObject = target.getSObject(relationshipAPINames[0]);
                    System.debug('next Parent Sobject##'+nextParentSObject);
                    //Because the Last element in the List will be the actual field Name to extract  Query Result
                    Integer iterateTill = relationshipAPINames.size() - 2;
                    // Used the  while Construct , because the Loop has a conditional exit
                    Integer i = 1;
                    //Keep moving till you either reach a null relationship or you have reach the end of iterations which is including the last but second element.
                    while (nextParentSObject != NULL && i <= iterateTill) {
                        nextParentSObject = nextParentSObject.getSObject(relationshipAPINames[i]);
                        i++;
                    }
                    //After successful iteration, next ParentSObject points to the final relationship in fieldName parameter
                    if (nextParentSObject != NULL) {
                        String actualFieldToExtract = relationshipAPINames[relationshipAPINames.size() - 1];
                        fieldValue = nextParentSObject.get(actualFieldToExtract);
                    }
                }
            }
            //2. Implies it's a normal field
            else {
                fieldValue = target.get(fieldName);
                System.debug('Else_fieldValue##'+ fieldValue);
            }
        } catch (Exception ex) {
            throw ex;
        }
        return fieldValue;
    }
}
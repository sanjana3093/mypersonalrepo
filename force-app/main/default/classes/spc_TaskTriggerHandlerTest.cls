/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the test class for Task Trigger Handler
*  @date            06/07/2018
*  @version         1.0
*********************************************************************************************************/


@IsTest
public class spc_TaskTriggerHandlerTest {

    public static Id ID_PATIENT_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
    public static Id ID_MANUFACTURER_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId();
    public static Id ID_PROGRAMCASE_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId();
    static string STRING_EMAIL = 'Email';

    /**************************************************************************************
    *  @author          Deloitte
    * @Description : Used to Create test data for Accounts, Enagagement Programs and Case and Task
    * @Param       : None
    * @Return      : None
    ***************************************************************************************/
    @testSetup static void setupTestdata() {

        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
    }

    @IsTest static void testUpdateScenarios() {
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User objUser = new User(Alias = 'test', email = 'TestSysAdmin@test123.com', emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', country = 'United States', profileid = p.Id,
                                timezonesidkey = 'America/Los_Angeles', username = System.currentTimeMillis() + 'TestSysAdmin@test123.com');

        spc_Database.ins(objUser);

        System.runAs(objUser) {

            //creation of Patient Account
            Account objAccount = new  Account();
            objAccount.PatientConnect__PC_Date_of_Birth__c = Date.today();
            objAccount.PatientConnect__PC_Gender__c = 'Male';
            objAccount.PatientConnect__PC_First_Name__c = 'TestClass';
            objAccount.PatientConnect__PC_Last_Name__c = 'TestClass';
            objAccount.Name = 'Test Program' + DateTime.now();
            objAccount.spc_HIPAA_Consent_Received__c = 'No';
            objAccount.spc_Patient_HIPAA_Consent_Date__c = System.today().addDays(-1);
            objAccount.RecordTypeId = ID_PATIENT_RECORDTYPE;
            insert objAccount;

            //Create a HCO account
            Account hcoAcc = new Account();
            hcoAcc = spc_Test_Setup.createTestAccount('HCO');
            insert hcoAcc;

            Account objManAccount = new  Account();
            objManAccount.Name = 'Manufacturer SageRx';
            objManAccount.PatientConnect__PC_Email__c = 'test@test.com';
            objManAccount.RecordTypeId = ID_MANUFACTURER_RECORDTYPE;
            insert objManAccount;

            //Creation of Engagement Program
            PatientConnect__PC_Engagement_Program__c objEngagementProgram = new PatientConnect__PC_Engagement_Program__c();
            objEngagementProgram.Name = 'Engagement Program SageRx';
            objEngagementProgram.PatientConnect__PC_Manufacturer__c = objManAccount.Id;
            objEngagementProgram.PatientConnect__PC_Program_Code__c = '1234567890';
            insert objEngagementProgram;

            //Creation of Case__c
            Case objCase = new  Case();
            objCase.RecordTypeId = ID_PROGRAMCASE_RECORDTYPE;
            objCase.Status = 'New';
            objCase.PatientConnect__PC_Engagement_Program__c = objEngagementProgram.Id;
            objCase.Origin = STRING_EMAIL;
            insert objCase;

            //Creation of Case__c
            Case objCase1 = new  Case();
            objCase1.RecordTypeId = ID_PROGRAMCASE_RECORDTYPE;
            objCase1.Status = 'New';
            objCase1.PatientConnect__PC_Engagement_Program__c = objEngagementProgram.Id;
            objCase1.Origin = STRING_EMAIL;
            insert objCase1;

            PatientConnect__PC_Interaction__c intRec = spc_Test_Setup.createInteraction(objCase.Id);
            intRec.spc_Welcome_Call_complete__c = 'No';
            insert intRec;

            //Creation of Task
            Task task1 = new Task();
            task1.ActivityDate = Date.today().addDays(7);
            task1.Subject = 'Follow up with patient regarding post-treatment steps';
            task1.WhatId = objCase.Id;
            task1.OwnerId = UserInfo.getUserId();
            task1.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
            task1.Status = 'In Progress';
            insert task1;

            task1.Status = 'Completed';
            task1.PatientConnect__PC_Call_Outcome__c = 'Left Message';
            task1.WhatId = objCase1.Id;
            update task1;

            Task task2 = new Task();
            task2.ActivityDate = Date.today();
            task2.Subject = 'Notify HCP of Patient Discontinued Treatment';
            task2.WhatId = objCase.Id;
            task2.OwnerId = UserInfo.getUserId();
            task2.PatientConnect__PC_Category__c = 'Welcome Call Lite';

            task2.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
            task2.Status = 'In Progress';
            insert task2;

            task2.Status = 'Completed';
            task2.PatientConnect__PC_Call_Outcome__c = 'Left Message';
            update task2;

            Task task3 = new Task();
            task3.ActivityDate = Date.today();
            task3.Subject = 'Perform Welcome Call';
            task3.WhatId = objCase.Id;
            task3.OwnerId = UserInfo.getUserId();
            task3.PatientConnect__PC_Category__c = 'Welcome Call';
            task3.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
            task3.Status = 'In Progress';
            insert task3;

            task3.PatientConnect__PC_Call_Outcome__c = 'Reached';
            task3.Status = 'Completed';
            update task3;

            Task task4 = new Task();
            task4.ActivityDate = Date.today()  ;
            task4.Subject = 'Perform Welcome Call (Attempt 2)';
            task4.WhatId = objCase.Id;
            task4.OwnerId = UserInfo.getUserId();
            task4.PatientConnect__PC_Category__c = 'Welcome Call';
            task4.PatientConnect__PC_Call_Outcome__c = 'Reached';
            task4.Status = 'In Progress';
            insert task4;

            task4.Status = 'Completed';
            task4.PatientConnect__PC_Category__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_MISSING_INFORMATION);
            task4.PatientConnect__PC_Call_Outcome__c = 'Left Message';
            update task4;

            Task task5 = new Task();
            task5.ActivityDate = Date.today();
            task5.Subject = 'Perform Welcome Call (Attempt 3)';
            task5.WhatId = objCase.Id;
            task5.OwnerId = UserInfo.getUserId();
            task5.PatientConnect__PC_Category__c = 'Welcome Call';
            task5.PatientConnect__PC_Call_Outcome__c = 'Reached';
            task5.Status = 'In Progress';
            task5.spc_ReAttempt_Count__c = 3;
            insert task5;

            task5.Status = 'Completed';
            task5.PatientConnect__PC_Call_Outcome__c = 'Left Message';
            update task5;


            Task task7 = new Task();
            task7.ActivityDate = Date.today();
            task7.Subject = 'Notify HCP of Patient Discontinued Treatment';
            task7.WhatId = objCase.Id;
            task7.OwnerId = UserInfo.getUserId();
            task7.PatientConnect__PC_Category__c = 'Welcome Call Lite';

            task7.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
            task7.Status = 'In Progress';
            insert task7;

            task7.Status = 'Completed';
            task7.PatientConnect__PC_Call_Outcome__c = 'Reached';
            task7.PatientConnect__PC_Sub_Category__c = 'First Attempt';
            update task7;


            Task task9 = new Task();
            task9.ActivityDate = Date.today();
            task9.Subject = 'Notify HCP of Patient Discontinued Treatment';
            task9.WhatId = objCase.Id;
            task9.OwnerId = UserInfo.getUserId();
            task9.PatientConnect__PC_Category__c = 'Welcome Call Lite';
            task9.PatientConnect__PC_Call_Outcome__c = 'Left Message';
            task9.Status = 'In Progress';
            insert task9;

            task9.Status = 'Completed';
            task9.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
            task9.PatientConnect__PC_Sub_Category__c = 'Second Attempt';
            update task9;

            Task task10 = new Task() ;
            task10.ActivityDate = Date.today() + 1;
            task10.Subject = 'SOC Logistics Call';
            task10.WhatId = objCase.Id;
            task10.OwnerId = UserInfo.getUserId();
            task10.PatientConnect__PC_Category__c = 'SOC Logistics Call';
            task10.PatientConnect__PC_Call_Outcome__c = 'Left Message';
            task10.Status = 'In Progress';
            insert task10;
            task10.Status = 'Completed';
            task10.PatientConnect__PC_Call_Outcome__c = 'Left Message';
            task10.PatientConnect__PC_Sub_Category__c = 'First Attempt';
            update task10;

            Task task11 = new Task();
            task11.ActivityDate = Date.today();
            task11.Subject = 'Notify HCP of Patient Discontinued Treatment';
            task11.WhatId = objCase.Id;
            task11.OwnerId = UserInfo.getUserId();
            task11.PatientConnect__PC_Category__c = 'Welcome Call Lite';

            task11.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
            task11.Status = 'In Progress';
            insert task11;

            task11.Status = 'Completed';
            task11.PatientConnect__PC_Call_Outcome__c = 'Reached';
            update task11;


        }

    }

    @IsTest static void testInsertScenarios() {
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User objUser = new User(Alias = 'test', email = 'TestSysAdmin@test123.com', emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', country = 'United States', profileid = p.Id,
                                timezonesidkey = 'America/Los_Angeles', username = System.currentTimeMillis() + 'TestSysAdmin@test123.com');

        spc_Database.ins(objUser);
        Account objManAccount = new  Account();
        objManAccount.Name = 'Manufacturer SageRx';
        objManAccount.PatientConnect__PC_Email__c = 'test@test.com';
        objManAccount.RecordTypeId = ID_MANUFACTURER_RECORDTYPE;
        insert objManAccount;

        //Creation of Engagement Program
        PatientConnect__PC_Engagement_Program__c objEngagementProgram = new PatientConnect__PC_Engagement_Program__c();
        objEngagementProgram.Name = 'Engagement Program SageRx';
        objEngagementProgram.PatientConnect__PC_Manufacturer__c = objManAccount.Id;
        objEngagementProgram.PatientConnect__PC_Program_Code__c = '1234567890';
        insert objEngagementProgram;

        //Creation of Case__c
        Case objCase = new  Case();
        objCase.RecordTypeId = ID_PROGRAMCASE_RECORDTYPE;
        objCase.Status = 'New';
        objCase.PatientConnect__PC_Engagement_Program__c = objEngagementProgram.Id;
        objCase.Origin = STRING_EMAIL;
        insert objCase;

        Task task1 = new Task();
        task1.ActivityDate = Date.today();
        task1.WhatId = objCase.Id;
        task1.OwnerId = UserInfo.getUserId();
        task1.PatientConnect__PC_Category__c = 'Welcome Call';
        task1.PatientConnect__PC_Call_Outcome__c = 'Reached';
        task1.Status = 'Completed';
        task1.PatientConnect__PC_Assigned_To__c = objUser.id;
        insert task1;
        Profile p2 = [SELECT Id FROM Profile WHERE Name = 'SAGE Patient Services User'];
        User objUser2 = new User(Alias = 'test', email = 'TestSysAdmin@test123.com', emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                                 localesidkey = 'en_US', country = 'United States', profileid = p2.Id,
                                 timezonesidkey = 'America/Los_Angeles', username = System.currentTimeMillis() + 'TestSysAdmin@test123.com');

        spc_Database.ins(objUser2);
        Task task2 = new Task();
        task2.ActivityDate = Date.today();
        task2.PatientConnect__PC_Program__c = objCase.id;
        task2.WhatId = objCase.Id;
        task2.OwnerId = objUser2.id;
        task2.PatientConnect__PC_Category__c = 'Welcome Call';
        task2.PatientConnect__PC_Call_Outcome__c = 'Reached';
        task2.Status = 'Completed';
        insert task2;

        delete task2;



        List<Account> accList = new List<Account>();

        Account pharmacyAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PC_Pharmacy').getRecordTypeId());
        accList.add(pharmacyAcc);

        Account pateintRec = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        pateintRec.spc_HIPAA_Consent_Received__c = 'No';
        pateintRec.PatientConnect__PC_Date_of_Birth__c = system.today();
        pateintRec.spc_Patient_HIPAA_Consent_Date__c = System.today().addDays(-1);
        accList.add(pateintRec);

        if (accList != NULL) {
            spc_Database.ins(accList);
        }
        Contact pharmacyCon = new Contact();
        pharmacyCon.AccountId = accList[0].Id;
        pharmacyCon.Email = 'test@test.com';
        pharmacyCon.LastName = 'TestPharmacyContact';
        spc_Database.ins(pharmacyCon);

        Case programCaseRec = spc_Test_Setup.createCases(new List<Account> {pateintRec}, 1, 'PC_Program').get(0);
        if (programCaseRec != NULL) {
            spc_Database.ins(programCaseRec);

        }
        PatientConnect__PC_Association__c association = spc_Test_Setup.createAssociation(pharmacyAcc, programCaseRec, 'Specialty Pharmacy', Date.today().addDays(1));

        List<PatientConnect__PC_Program_Coverage__c> pgrmCoverageList = new List<PatientConnect__PC_Program_Coverage__c>();
        PatientConnect__PC_Program_Coverage__c progCoverageRec1 = spc_Test_Setup.newProgCoverage(programCaseRec.Id, 'PC_PAP', 'Uninsured');
        progCoverageRec1.PatientConnect__PC_Coverage_Status__c = 'Active';
        progCoverageRec1.spc_Not_Eligible__c = true;
        pgrmCoverageList.add(progCoverageRec1);


        spc_Database.ins(pgrmCoverageList);

        Task task3 = new Task();
        task3.ActivityDate = Date.today();
        task3.PatientConnect__PC_Program__c = programCaseRec.Id;
        task3.WhatId = progCoverageRec1.Id;
        task3.OwnerId = UserInfo.getUserId();
        task3.PatientConnect__PC_Category__c = 'Welcome Call';
        task3.PatientConnect__PC_Call_Outcome__c = 'Reached';
        task3.Status = 'Completed';
        insert task3;

        //Create a HCO account
        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
        insert hcoAcc;


        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(objCase.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        interaction.spc_SOC_Type__c = 'Home';
        interaction.spc_HIP__c = 'Other';
        insert interaction;
        interaction.PatientConnect__PC_Status__c = 'In Progress';
        update interaction;

        Task task4 = new Task();
        task4.ActivityDate = Date.today();
        task4.PatientConnect__PC_Program__c = objCase.id;
        task4.WhatId = interaction.Id;
        task4.OwnerId = UserInfo.getUserId();
        task4.PatientConnect__PC_Category__c = 'Welcome Call';
        task4.PatientConnect__PC_Call_Outcome__c = 'Reached';
        task4.Status = 'Completed';
        insert task4;


    }
    /**************Logistic Task Creation  US 302027 **********************/
    @IsTest static void testLogisticTaskCreation() {
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User objUser = new User(Alias = 'test', email = 'TestSysAdmin@test123.com', emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', country = 'United States', profileid = p.Id,
                                timezonesidkey = 'America/Los_Angeles', username = System.currentTimeMillis() + 'TestSysAdmin@test123.com');

        spc_Database.ins(objUser);
        List<Account> accList = new List<Account>();

        Account HCOAcc = spc_Test_Setup.createTestAccount('HCO');
        HCOAcc.spc_REMS_Certification_Status_Date__c = Date.Today();
        HCOAcc.spc_REMS_Certification_Status__c = 'Certified';
        insert HCOAcc;


        Account pateintRec = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        pateintRec.spc_HIPAA_Consent_Received__c = 'No';
        pateintRec.PatientConnect__PC_Date_of_Birth__c = system.today();
        pateintRec.spc_Patient_HIPAA_Consent_Date__c = System.today().addDays(-1);
        spc_Database.ins(pateintRec);
        /****  accList.add(pateintRec); ***/

        if (accList != NULL) {
            spc_Database.ins(accList);
        }

        Case programCaseRec = spc_Test_Setup.createCases(new List<Account> {pateintRec}, 1, 'PC_Program').get(0);
        if (programCaseRec != NULL) {
            spc_Database.ins(programCaseRec);

        }
        /*********Association Creation***********/
        PatientConnect__PC_Association__c association = spc_Test_Setup.createAssociation(HCOAcc, programCaseRec, 'HCO', Date.today().addDays(1));

        /************Task Creation**********/
        Task task = spc_Test_Setup.createTask('Completed', programCaseRec.Id, 'Normal', 'Welcome Call Lite', 'Phone', Date.Today());
        task.PatientConnect__PC_Call_Outcome__c = 'Reached';
        insert task;
        Set<Id> caseset = new Set<Id>();
        if (programCaseRec != NULL) {
            caseset.add(programCaseRec.id);
        }

    }
    @istest static void testmethod12() {
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User objUser = new User(Alias = 'test', email = 'TestSysAdmin@test123.com', emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', country = 'United States', profileid = p.Id,
                                timezonesidkey = 'America/Los_Angeles', username = System.currentTimeMillis() + 'TestSysAdmin@test123.com');

        spc_Database.ins(objUser);

        System.runAs(objUser) {

            //creation of Patient Account
            Account objAccount = new  Account();
            objAccount.PatientConnect__PC_Date_of_Birth__c = Date.today();
            objAccount.PatientConnect__PC_Gender__c = 'Male';
            objAccount.PatientConnect__PC_First_Name__c = 'TestClass';
            objAccount.PatientConnect__PC_Last_Name__c = 'TestClass';
            objAccount.Name = 'Test Program' + DateTime.now();
            objAccount.spc_HIPAA_Consent_Received__c = 'Yes';
            objAccount.spc_Patient_HIPAA_Consent_Date__c = System.today().addDays(-1);
            objAccount.RecordTypeId = ID_PATIENT_RECORDTYPE;
            insert objAccount;

            //Create a HCO account
            Account hcoAcc = new Account();
            hcoAcc = spc_Test_Setup.createTestAccount('HCO');
            insert hcoAcc;

            Account objManAccount = new  Account();
            objManAccount.Name = 'Manufacturer SageRx';
            objManAccount.PatientConnect__PC_Email__c = 'test@test.com';
            objManAccount.RecordTypeId = ID_MANUFACTURER_RECORDTYPE;
            insert objManAccount;

            //Creation of Engagement Program
            PatientConnect__PC_Engagement_Program__c objEngagementProgram = new PatientConnect__PC_Engagement_Program__c();
            objEngagementProgram.Name = 'Engagement Program SageRx';
            objEngagementProgram.PatientConnect__PC_Manufacturer__c = objManAccount.Id;
            objEngagementProgram.PatientConnect__PC_Program_Code__c = '1234567890';
            insert objEngagementProgram;

            //Creation of Case__c
            Case objCase = new  Case();
            objCase.RecordTypeId = ID_PROGRAMCASE_RECORDTYPE;
            objCase.Status = 'New';
            objCase.PatientConnect__PC_Engagement_Program__c = objEngagementProgram.Id;
            objCase.AccountId = objAccount.id;
            objCase.Origin = STRING_EMAIL;
            insert objCase;

            PatientConnect__PC_Interaction__c intRec = spc_Test_Setup.createInteraction(objCase.Id);
            intRec.spc_Welcome_Call_complete__c = 'No';
            insert intRec;

            Task task1 = new Task();
            task1.ActivityDate = Date.today();
            task1.Subject = 'Call HCP for Missing Information';
            task1.WhatId = objCase.Id;
            task1.OwnerId = UserInfo.getUserId();
            task1.PatientConnect__PC_Category__c='Missing Information';

            task1.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
            task1.Status = 'In Progress';
            task1.spc_ReAttempt_Count__c = 1;
            insert task1;

            task1.Status = 'Completed';
            task1.PatientConnect__PC_Call_Outcome__c = 'Left Message';
            update task1;

            Task task2 = new Task();
            task2.ActivityDate = Date.today();
            task2.Subject = 'Call HCP for Missing Information (2nd Attempt)';
            task2.WhatId = objCase.Id;
            task2.OwnerId = UserInfo.getUserId();
            task2.PatientConnect__PC_Category__c='Missing Information';
            task2.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
            task2.Status = 'In Progress';
            task2.spc_ReAttempt_Count__c = 1;
            insert task2;

            task2.Status = 'Completed';
            task2.PatientConnect__PC_Call_Outcome__c = 'Left Message';
            task2.PatientConnect__PC_Sub_Category__c = 'First Attempt';
            update task2;

            Task task3 = new Task();
            task3.ActivityDate = Date.today();
            task3.Subject = '';
            task3.WhatId = objCase.Id;
            task3.OwnerId = UserInfo.getUserId();
            task3.PatientConnect__PC_Category__c='Missing Information';
            task3.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
            task3.Status = 'In Progress';
            task3.spc_ReAttempt_Count__c = 2;
            insert task3;

            task3.Status = 'Completed';
            task3.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
            task3.PatientConnect__PC_Sub_Category__c = 'Second Attempt';
            update task3;

            Task task4 = new Task() ;
            task4.ActivityDate = Date.today() + 1;
            task4.Subject = 'SOC Logistics Call';
            task4.WhatId = objCase.Id;
            task4.OwnerId = UserInfo.getUserId();
            task4.PatientConnect__PC_Category__c = 'SOC Logistics Call';
            task4.PatientConnect__PC_Call_Outcome__c = 'Left Message';
            task4.Status = 'In Progress';
            insert task4;
            task4.Status = 'Completed';
            task4.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
            task4.PatientConnect__PC_Sub_Category__c = 'Second Attempt';
            update task4;

            Task task5 = new Task() ;
            task5.ActivityDate = Date.today() + 1;
            task5.Subject = 'SOC Logistics Call';
            task5.WhatId = objCase.Id;
            task5.OwnerId = UserInfo.getUserId();
            task5.PatientConnect__PC_Category__c = 'SOC Logistics Call';
            task5.PatientConnect__PC_Call_Outcome__c = 'Left Message';
            task5.Status = 'In Progress';
            insert task5;
            task5.Status = 'Completed';
            task5.PatientConnect__PC_Call_Outcome__c = 'Reached';
            task5.PatientConnect__PC_Sub_Category__c = 'Second Attempt';
            update task5;



        }
    }
    @isTest static void testWelcomeCall() {
        Account patientAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        patientAcc.PatientConnect__PC_Date_of_Birth__c = system.today();
        patientAcc.spc_HIPAA_Consent_Received__c = 'No';
        patientAcc.spc_Patient_HIPAA_Consent_Date__c = System.Today();
        patientAcc.spc_Patient_Services_Consent_Received__c = 'No';
        patientAcc.spc_Patient_Service_Consent_Date__c = System.Today();
        patientAcc.spc_Text_Consent__c = 'No';
        patientAcc.spc_Patient_Text_Consent_Date__c = System.Today();
        patientAcc.spc_Patient_Mrkt_and_Srvc_consent__c = 'No';
        patientAcc.spc_Patient_Marketing_Consent_Date__c = System.Today();

        insert patientAcc;

        Case programCaseRec = spc_Test_Setup.createCases(new List<Account> {patientAcc}, 1, 'PC_Program').get(0);
        if (null != programCaseRec) {
            programCaseRec.Type = 'Program';
            insert programCaseRec;
        }
        programCaseRec.spc_AC__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_PIC_STATUS_ONHOLD);
        update programCaseRec;
        PatientConnect__PC_Interaction__c intr = new PatientConnect__PC_Interaction__c();
        intr.PatientConnect__PC_Patient_Program__c = programCaseRec.id;
        intr.spc_Welcome_Call_complete__c = 'No';
        insert intr;

        Set<Id> caseIdsForWelcome = new Set<Id>();
        caseIdsForWelcome.add(programCaseRec.id);
        spc_TaskTriggerImplementation  tti = new spc_TaskTriggerImplementation();
        tti.setWelcomeCallFinished(caseIdsForWelcome);
        tti.createFollowUpTasks(caseIdsForWelcome, Date.Today() + 1, 'SOC Logistics Call', 'First Attempt', 'FAX', 'Outbound', 'High', 'Not Started', 'Test');
        tti.createFollowUpTasks(caseIdsForWelcome, Date.Today() + 1, 'Site of Care', 'Confirm Infusion', 'FAX', 'Outbound', 'High', 'Not Started', 'Test');
        Task tt = new Task();
        tt.PatientConnect__PC_Call_Outcome__c = 'Reached';
        tt.Subject = 'Test';
        tt.PatientConnect__PC_Channel__c = 'FAX';
        tt.PatientConnect__PC_Direction__c = 'Outbound';
        tt.PatientConnect__PC_Category__c = 'SOC Logistics Call';
        tt.PatientConnect__PC_Program__c = programCaseRec.id;
        insert tt;
        tt.PatientConnect__PC_Sub_Category__c = 'First Attempt';
        upsert tt;
        Task task20 = new Task();
        task20.ActivityDate = Date.today();
        task20.Subject = '';
        task20.WhatId = programCaseRec.Id;
        task20.OwnerId = UserInfo.getUserId();
        task20.PatientConnect__PC_Category__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_OVERDUE_PIC);
        task20.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
        task20.Status = 'In Progress';
        task20.spc_ReAttempt_Count__c = 2;
        insert task20;

        task20.Status = 'Completed';
        task20.PatientConnect__PC_Call_Outcome__c = 'Patient Not Reached';
        task20.PatientConnect__PC_Sub_Category__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_CAT_FIRST_ATTEMPT);
        update task20;
    }
     @isTest static void testcreateFollowupTasks1()
    {
        spc_TaskTriggerImplementation triggerImplementation = new spc_TaskTriggerImplementation(); 
        Account patientAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId()); 
        patientAcc.PatientConnect__PC_Date_of_Birth__c=system.today();
        patientAcc.spc_HIPAA_Consent_Received__c='Yes';
        patientAcc.spc_Patient_HIPAA_Consent_Date__c=System.Today();
        patientAcc.spc_Patient_Services_Consent_Received__c='No';
        patientAcc.spc_Patient_Service_Consent_Date__c=System.Today();
        patientAcc.spc_Text_Consent__c='No';
        patientAcc.spc_Patient_Text_Consent_Date__c=System.Today();
        patientAcc.spc_Patient_Mrkt_and_Srvc_consent__c='No';
        patientAcc.spc_Patient_Marketing_Consent_Date__c=System.Today();
        
        insert patientAcc;
 
        Case programCaseRec = spc_Test_Setup.createCases(new List<Account>{patientAcc},1,'PC_Program').get(0);
        if(null!=programCaseRec)
        {
            programCaseRec.Type = 'Program';
            insert programCaseRec;
        }

        Task tt=new Task();
        tt.PatientConnect__PC_Call_Outcome__c='Reached';
        tt.Subject='Test';
        tt.PatientConnect__PC_Channel__c='FAX';
        tt.PatientConnect__PC_Direction__c='Outbound';
        tt.PatientConnect__PC_Category__c='SOC Logistics Call';
        tt.PatientConnect__PC_Program__c=programCaseRec.id;
        tt.WhatId = programCaseRec.id;
        insert tt;

        Map<Id,spc_ApexConstants.PicklistValue> mapFollowUpTasks = new Map<Id,spc_ApexConstants.PicklistValue>();
        Set<Id> caseIdsFollowupTasks = new Set<Id>();

        mapFollowUpTasks.put(tt.Id,  spc_ApexConstants.PicklistValue.TASK_SUB_PATIENT_DISCONTINUATION);
        caseIdsFollowupTasks.add(tt.WhatId);
        Map<Id, Task> taskMap = new Map<Id, Task>();
        taskMap.put(tt.id,tt);
        triggerImplementation.createFollowupTasks(mapFollowUpTasks, caseIdsFollowupTasks, taskMap);


    }

    @isTest static void testcreateFollowupTasks2()
    {
        spc_TaskTriggerImplementation triggerImplementation = new spc_TaskTriggerImplementation(); 
        Account patientAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId()); 
        patientAcc.PatientConnect__PC_Date_of_Birth__c=system.today();
        patientAcc.spc_HIPAA_Consent_Received__c='Yes';
        patientAcc.spc_Patient_HIPAA_Consent_Date__c=System.Today();
        patientAcc.spc_Patient_Services_Consent_Received__c='No';
        patientAcc.spc_Patient_Service_Consent_Date__c=System.Today();
        patientAcc.spc_Text_Consent__c='No';
        patientAcc.spc_Patient_Text_Consent_Date__c=System.Today();
        patientAcc.spc_Patient_Mrkt_and_Srvc_consent__c='No';
        patientAcc.spc_Patient_Marketing_Consent_Date__c=System.Today();
        
        insert patientAcc;
 
        Case programCaseRec = spc_Test_Setup.createCases(new List<Account>{patientAcc},1,'PC_Program').get(0);
        if(null!=programCaseRec)
        {
            programCaseRec.Type = 'Program';
            insert programCaseRec;
        }

        Task tt=new Task();
        tt.PatientConnect__PC_Call_Outcome__c='Reached';
        tt.Subject='Test';
        tt.PatientConnect__PC_Channel__c='FAX';
        tt.PatientConnect__PC_Direction__c='Outbound';
        tt.PatientConnect__PC_Category__c='SOC Logistics Call';
        tt.PatientConnect__PC_Program__c=programCaseRec.id;
        tt.WhatId = programCaseRec.id;
        insert tt;

        Map<Id,spc_ApexConstants.PicklistValue> mapFollowUpTasks = new Map<Id,spc_ApexConstants.PicklistValue>();
        Set<Id> caseIdsFollowupTasks = new Set<Id>();

        mapFollowUpTasks.put(tt.Id,  spc_ApexConstants.PicklistValue.TASK_SUB_PERFORM_WELCOME_CALL);
        caseIdsFollowupTasks.add(tt.WhatId);
        Map<Id, Task> taskMap = new Map<Id, Task>();
        taskMap.put(tt.id,tt);
        triggerImplementation.createFollowupTasks(mapFollowUpTasks, caseIdsFollowupTasks, taskMap);


    }
    
}
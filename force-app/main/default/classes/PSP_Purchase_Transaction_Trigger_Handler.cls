/*********************************************************************************************************
class Name      : PSP_Purchase_Transaction_Trigger_Handler 
Description		: Trigger Handler Class for Purchase Transaction Object
@author     	: Saurabh Tripathi 
@date       	: September 17, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi            September 17, 2019          Initial Version
****************************************************************************************************************/ 
public class PSP_Purchase_Transaction_Trigger_Handler extends PSP_trigger_Handler {

    /*Instantiation of Trigger impelementation class   */ 
	PSP_PurchaseTransaction_Trigger_Impl triggerImpl = new PSP_PurchaseTransaction_Trigger_Impl ();   
	 /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 09/17/2019
  	* @Description : Before Insert Method being overriden here
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/    
	public override void beforeInsert () {
    	triggerImpl.findRelatedList (Trigger.New);
		triggerImpl.updateRefillDate(Trigger.New);        
    }
    /**************************************************************************************
  	* @author      : Sulata Sadhu
  	* @date        : 12/11/2019
  	* @Description : Before Update Method being overriden here
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/
    public override void beforeUpdate () {
    	triggerImpl.updateRefillDate(Trigger.New);        
    }
    
}
/*********************************************************************************************************
* @author Deloitte
* @description This is the schedular class for spc_CreateTasksOnScheduledDate.
* @date January 3, 2019
****************************************************************************************************************/
global class spc_CreateTasksOnScheduledDateSchedular implements Schedulable {

	global void execute(SchedulableContext sc) {
		Database.executebatch(new spc_CreateTasksOnScheduledDate());
	}

}
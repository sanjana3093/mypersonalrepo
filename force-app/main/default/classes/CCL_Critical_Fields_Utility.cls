/********************************************************************************************************
*  @author          Deloitte
*  @description     Utility class to validate critical fields change
*  @date            August 19, 2020
*  @version         1.0
*********************************************************************************************************/
public with sharing class CCL_Critical_Fields_Utility {
    /*
    *   @author           Deloitte
    *   @description      Validate crtical fields whether they are changed based on passed new and old record, return true if critical fiels changed
    *   @para             String (text for event Type), sObject (CurrentRecord information) , sObject (Old version of CurrentRecord)
    *   @return           Boolean
    *   @date             19 August 2020
    */
    public static Boolean isCriticalFieldsChanged(String eventType, sObject oldObjectRecord, sObject newObjectRecord) {
        Boolean isFieldsChanged = false;
        CCL_Fields_Validation__mdt fieldValidation = fetchMetadataRecord(eventType);
        if(fieldValidation != null) {
            List<String> criticalFields = fieldValidation.CCL_Field__c.split(',');
            for(String criticalField : criticalFields) {
                if(oldObjectRecord.get(criticalField.trim()) != newObjectRecord.get(criticalField.trim())) {
                    isFieldsChanged = true;
                    break;
                }
            }
        }
        return isFieldsChanged;
    }

    private static CCL_Fields_Validation__mdt  fetchMetadataRecord(String eventType) {
         CCL_Fields_Validation__mdt metaData=new CCL_Fields_Validation__mdt();
        if(CCL_Fields_Validation__mdt.sObjectType.getDescribe().isAccessible()){
            metaData=[SELECT CCL_Field__c,CCL_Validation_Type__c FROM CCL_Fields_Validation__mdt WHERE CCL_Event_type__c = :eventType LIMIT 1];
        }
        return metaData;    }
}
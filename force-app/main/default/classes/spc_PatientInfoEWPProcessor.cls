/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Processor class for Patient Information Enrollment Wizard Page Processor
    *  @date            19-June-2018
    *  @version         1.0
    *
    *********************************************************************************************************/
global with sharing class spc_PatientInfoEWPProcessor implements PatientConnect.PC_EnrollmentWizard.PageProcessor {
    private static String FIELDSET_KEY = 'fsFields';
    /********************************************************************************************************
     *  @author           Deloitte
     *  @date             29/06/2018
     *  @description      Implemented method from PC_EnrollmentWizard.PageProcessor
     *  @param            enrollmentCase - Case Object
     *  @param            pageState - Map with field values from page
     *  @return           None
     *********************************************************************************************************/
    public static void processEnrollment(Case enrollmentCase, Map<String, Object> pageState) {

        if (pageState == null) {
            throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException(Label.Missing_Page_State);
        }
        if (pageState.get('persistentAccount') == null) {
            throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException(Label.spc_Missing_Account_Information);
        }

        Object persistentAccount = (Object) pageState.get('persistentAccount');
        Map<String, Object> patientInfoAccount = (Map<String, Object>) persistentAccount;
        Account acc = new Account();

        //Creating Patient Account
        if (enrollmentCase.AccountId != null) {
            acc.Id = enrollmentCase.AccountId;
        }
        //Update the RecordType to Patinet Record Type

        acc.RecordTypeId    = spc_ApexConstants.ID_PATIENT_RECORDTYPE;
        acc.PatientConnect__PC_Status__c = spc_ApexConstants.ACCOUNT_STATUS_ACTIVE;
        acc.PatientConnect__PC_First_Name__c = (String) (patientInfoAccount.get('firstName'));
        acc.PatientConnect__PC_Last_Name__c = (String) (patientInfoAccount.get('lastName'));
        if (String.isNotBlank((String) patientInfoAccount.get('middleName'))) {
            acc.spc_Middle_Name__c = (String) (patientInfoAccount.get('middleName'));
        }
        if (String.isNotBlank((String) patientInfoAccount.get('preferredName'))) {
            acc.spc_Preferred_Name__c = (String) (patientInfoAccount.get('preferredName'));
        }

        //Add conditions to check blank values before assigning to picklists
        if ((String) (patientInfoAccount.get('salutation')) != spc_ApexConstants.PICKLIST_NONE) {
            acc.PatientConnect__PC_Salutation__c = (String) (patientInfoAccount.get('salutation'));
        }
        acc.PatientConnect__PC_Email__c = (String) (patientInfoAccount.get('email'));
        if (String.isNotBlank((String) patientInfoAccount.get('homePhone'))) {
            acc.spc_Home_Phone__c = FormatPhone((String) (patientInfoAccount.get('homePhone')));
        }
        if (String.isNotBlank((String) patientInfoAccount.get('mobilePhone'))) {
            acc.spc_Mobile_Phone__c = FormatPhone((String) (patientInfoAccount.get('mobilePhone')));
        }
        if (String.isNotBlank((String) patientInfoAccount.get('officePhone'))) {
            acc.spc_Office_Phone__c = FormatPhone((String) (patientInfoAccount.get('officePhone')));
        }
        if ((String) (patientInfoAccount.get('patientHIPAConsent')) != spc_ApexConstants.PICKLIST_NONE) {
            String hipaaConsent = (String) (patientInfoAccount.get('patientHIPAConsent'));
            acc.spc_HIPAA_Consent_Received__c = hipaaConsent;
            acc.spc_Services_Text_Consent__c = hipaaConsent;
            acc.spc_Patient_Mrkt_and_Srvc_consent__c = hipaaConsent;
            enrollmentCase.Is_No_Services_Consent__c = (hipaaConsent.equalsIgnoreCase(Label.spc_No) ? true : false);
        }
        if ((String) (patientInfoAccount.get('patientHIPAConsent')) == spc_ApexConstants.PICKLIST_NONE) {
            enrollmentCase.spc_Is_None_Service_Consent__c  = true;
        }
        if ((String) (patientInfoAccount.get('patientTextConsent')) != spc_ApexConstants.PICKLIST_NONE) {
            acc.spc_Text_Consent__c = (String) (patientInfoAccount.get('patientTextConsent'));
        }
        if ((String) (patientInfoAccount.get('patientPHIConsent')) != spc_ApexConstants.PICKLIST_NONE) {
            acc.spc_Patient_PHI_Consent_Confidentiality__c = (String) (patientInfoAccount.get('patientPHIConsent'));
        }
        if (patientInfoAccount.get('weight') != null && patientInfoAccount.get('weight') != '') {
            acc.spc_Weight_lbs__c = (Integer) patientInfoAccount.get('weight');
        }
        //Creating Name Field from First Name and Last Name
        acc.Name = (String) (patientInfoAccount.get('lastName')) + ' , ' + (String) (patientInfoAccount.get('firstName'));

        if ((String) (patientInfoAccount.get('preferredCommunicationLanguage')) != spc_ApexConstants.PICKLIST_NONE) {
            acc.PatientConnect__PC_Communication_Language__c = (String) (patientInfoAccount.get('preferredCommunicationLanguage'));
        }
        if ((String) (patientInfoAccount.get('preferredCommunicationChannel')) != spc_ApexConstants.PICKLIST_NONE) {
            acc.PatientConnect__PC_Preferred__c = (String) (patientInfoAccount.get('preferredCommunicationChannel'));
        }
        if ((String) (patientInfoAccount.get('preferredTimeToContact')) != spc_ApexConstants.PICKLIST_NONE) {
            acc.spc_Preferred_Time_to_Contact__c = (String) (patientInfoAccount.get('preferredTimeToContact'));
        }
        if ((String) (patientInfoAccount.get('okToleaveMessage')) != spc_ApexConstants.PICKLIST_NONE) {
            acc.spc_OK_to_leave_message__c = (String) (patientInfoAccount.get('okToleaveMessage'));
        }

        if (String.isNotBlank((String) patientInfoAccount.get('childName'))) {
            acc.spc_Child_Name__c = (String) (patientInfoAccount.get('childName'));
        }
        if (String.isNotBlank((String) patientInfoAccount.get('childDOB'))) {
            acc.spc_Child_Date_of_Birth__c = (Date.valueOf(String.valueOf(patientInfoAccount.get('childDOB'))));
        }
        if ((String) (patientInfoAccount.get('childGender')) != spc_ApexConstants.PICKLIST_NONE) {
            acc.spc_Child_Gender__c = (String) (patientInfoAccount.get('childGender'));
        }
        //Add date with check for null and using date of string
        if (String.isNotBlank((String) patientInfoAccount.get('dob'))) {
            acc.PatientConnect__PC_Date_of_Birth__c = (Date.valueOf(String.valueOf(patientInfoAccount.get('dob'))));
        }
        if (String.isNotBlank((String) patientInfoAccount.get('patientHIPAConsentDate'))) {
            Date hipaaDate = (Date.valueOf(String.valueOf(patientInfoAccount.get('patientHIPAConsentDate'))));
            acc.spc_Patient_HIPAA_Consent_Date__c = hipaaDate;
            acc.spc_Services_Text_Consent_Date__c = hipaaDate;
            acc.spc_Patient_Marketing_Consent_Date__c = hipaaDate;
        }
        if (String.isNotBlank((String) patientInfoAccount.get('patientTextConsentDate'))) {
            acc.spc_Patient_Text_Consent_Date__c = (Date.valueOf(String.valueOf(patientInfoAccount.get('patientTextConsentDate'))));
        }
        if (String.isNotBlank((String) patientInfoAccount.get('patientServicesConsentDate'))) {
            acc.spc_Patient_Service_Consent_Date__c = (Date.valueOf(String.valueOf(patientInfoAccount.get('patientServicesConsentDate'))));
            acc.spc_REMS_Text_Consent_Date__c = (Date.valueOf(String.valueOf(patientInfoAccount.get('patientServicesConsentDate'))));
        }
        if ((String) (patientInfoAccount.get('gender')) != spc_ApexConstants.PICKLIST_NONE) {
            acc.PatientConnect__PC_Gender__c = (String) (patientInfoAccount.get('gender'));
        }

        if (patientInfoAccount.containsKey(FIELDSET_KEY)) {
            Map<String, Object> fieldSetMap = (Map<String, Object>) patientInfoAccount.get(FIELDSET_KEY);
            spc_FieldSetController.copyFieldSetValues(fieldSetMap, acc);
        }
        if (patientInfoAccount.get('remsId') != null && patientInfoAccount.get('remsId') != '') {
            acc.spc_REMS_ID__c = (String) patientInfoAccount.get('remsId');
        }

        acc.ParentId    = enrollmentCase.PatientConnect__PC_Engagement_Program__r.PatientConnect__PC_Manufacturer__c;
        spc_Database.ups(acc);
        enrollmentCase.AccountId = acc.Id;//Link the account to Enrollment Case

        //Creating Address for patient account
        List<Object> persistentAddress = (List<Object>) pageState.get('persistentAddress');
        String addrID;
        List<PatientConnect__PC_Address__c> addresses = new List<PatientConnect__PC_Address__c>();
        PatientConnect__PC_Address__c addr;
        for (Object o : persistentAddress) {
            addr = new PatientConnect__PC_Address__c();
            Map<String, Object> patientInfoAddress = (Map<String, Object>) o;
            addrID = (String) (patientInfoAddress.get('Id'));
            if (!String.isEmpty(addrID)) {
                addr.Id = addrID;
            }
            addr.PatientConnect__PC_Account__c = acc.Id;
            addr.PatientConnect__PC_Address_1__c = (String) (patientInfoAddress.get('address1'));
            addr.PatientConnect__PC_Address_2__c = (String) (patientInfoAddress.get('address2'));
            addr.PatientConnect__PC_Address_3__c = (String) (patientInfoAddress.get('address3'));
            //Add conditions to check blank values before assigning to picklists
            if ((String) (patientInfoAddress.get('addressType')) != spc_ApexConstants.PICKLIST_NONE) {
                addr.PatientConnect__PC_Address_Description__c = (String) (patientInfoAddress.get('addressType'));
            }
            addr.PatientConnect__PC_City__c = (String) (patientInfoAddress.get('city'));
            addr.PatientConnect__PC_Country__c = (String) (patientInfoAddress.get('country'));
            addr.PatientConnect__PC_State__c = (String) (patientInfoAddress.get('state'));
            addr.PatientConnect__PC_Status__c = 'Active';
            addr.PatientConnect__PC_Zip_Code__c = (String) (patientInfoAddress.get('zipCode'));
            addr.PatientConnect__PC_Primary_Address__c = ((boolean) (patientInfoAddress.get('primary')));
            addresses.add(addr);
        }
        if (!addresses.isEmpty()) {
            spc_Database.ups(addresses);//FLS/CRUD Check enforced in the PC_database method
        }
        List<Case> enrollmentList = [Select Origin From Case Where ID = :enrollmentCase.Id];
        //Create a new Program Case with patient information
        Case prgCase = new Case();
        prgCase.Status = spc_ApexConstants.STATUS_NEW;
        prgCase.AccountId = acc.Id;
        prgCase.PatientConnect__PC_Engagement_Program__c = enrollmentCase.PatientConnect__PC_Engagement_Program__c;
        prgCase.Type = spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE_NAME;
        prgCase.PatientConnect__PC_Enrollment_Date__c = date.today();
        if (patientInfoAccount.get('remsEnrollmentId') != null && patientInfoAccount.get('remsEnrollmentId') != '') {
            prgCase.spc_REMS_Enrollment_ID__c = (String) patientInfoAccount.get('remsEnrollmentId');
        }
        if (!enrollmentList.isEmpty()) {
            prgCase.Origin = enrollmentList[0].Origin;
        }
        spc_Template_Data__c zpData = new spc_Template_Data__c();
        insert zpData;
        prgCase.spc_Template_Data__c = zpData.Id;
        spc_Database.ins(prgCase);//FLS/CRUD Check enforced in the PC_database method
        //Link the newly created program to the Enrollment Case
        enrollmentCase.PatientConnect__PC_Program__c = prgCase.Id;
    }
    private static String FormatPhone(String Phone) {
        string nondigits = '[^0-9]';
        string PhoneDigits;

        // remove all non numeric
        PhoneDigits = Phone.replaceAll(nondigits, '');

        // 10 digit: reformat with dashes
        if (PhoneDigits.length() == 10)
            return '(' + PhoneDigits.substring(0, 3) + ')' + '-' +
                   PhoneDigits.substring(3, 6) + '-' +
                   PhoneDigits.substring(6, 10);
        // 11 digit: if starts with 1, format as 10 digit
        if (PhoneDigits.length() == 11) {
            if (PhoneDigits.substring(0, 1) == '1') {
                return '(' + PhoneDigits.substring(1, 4) + ')' + '-' +
                       PhoneDigits.substring(4, 7) + '-' +
                       PhoneDigits.substring(7, 11);

            }
        }

        // if it isn't a 10 or 11 digit number, return the original because
        // it may contain an extension or special information
        return ( Phone );
    }
}

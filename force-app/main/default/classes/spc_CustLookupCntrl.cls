/**
* @author Deloitte
* @date 06/08/2018
*
* @description This is The PC class cloned by Sage for Reference only
* Class for fetching lookup values.
*/
public class spc_CustLookupCntrl {

    /*******************************************************************************************************
    * @description This method returns the Record based on the SearchKey
    * @param searchKeyWord Search Word entered
    * @param ObjectName The Object in which the record needs to be searched
    * @param filterObjectId String Additional Criteria
    * @param recordTypeName String
    * @return List<sObject> which are fetched with the matching criteria
    */

    @AuraEnabled
    public static List<sObject> fetchLookUpValues(String searchKeyWord, String ObjectName, String filterObjectId, String recordTypeName) {

        String sQuery;
        String sQueryApp;
        String Status = 'Provisioned';
        List < sObject > returnList = new List < sObject > ();
        Map<Id, sObject> appMap = new Map <Id, sObject> ();
        if (String.isNotBlank(searchKeyWord)) {
            String searchKey = '\'%' + searchKeyWord + '%\''  ;
            sQuery = 'select id, Name from ' + ObjectName + ' where Name LIKE ' + searchKey + ' order by CreatedDate DESC limit 5';
            if (String.isNotBlank(recordTypeName)) {
                String[] recordTypes = recordTypeName.split('#');
                String recordType = '';
                integer i = 0;
                for (String rt : recordTypes) {
                    if (i == recordTypes.size() - 1) {
                        recordType = '\'' + rt + '\'';
                    } else {
                        recordType = '\'' + rt + '\',';
                    }
                }
                sQuery = 'select id, Name from ' + ObjectName + ' where Name LIKE ' + searchKey + ' And RecordType.DeveloperName in ( ' + recordType + ')order by CreatedDate DESC limit 5';
            }
            List < sObject > lstOfRecords = Database.query(sQuery);


            return lstOfRecords;

        } else {
            return null;
        }
    }
}
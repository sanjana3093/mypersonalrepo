@isTest public class OutboundSMSDataObjectTest {

    @isTest
    static void testOutboundSMSDataObject(){
        
        OutboundSMSDataObject smsObject = new OutboundSMSDataObject();
        smsObject.phoneNumber = '12012529100';
        smsObject.taskID = '12345';
        smsObject.firstName = 'Mike';
        smsObject.smsTextTemplate = 'Test Template';
        smsObject.zipCode = '07077';
        smsObject.templateName = 'Name of Template';
        
        Test.startTest();
        String loggedData = smsObject.toString();
        System.debug('OutboundSMSDataObject string = '+loggedData);
        String jsonString = smsObject.toJSONString();
        DateTime today = DateTime.now();
        String todaysDate = today.format('MM/dd/yyyy');
        System.debug('JSON String = '+jsonString);
        
        System.assertEquals(jsonString.contains('InfusionDate'), true);
        System.assertEquals(jsonString.contains(todaysDate), true);
        System.assertEquals(loggedData.contains('Infusion Date'), true);
        //System.assertEquals(loggedData.contains(todaysDate), true);
        Test.stopTest();
    }
}
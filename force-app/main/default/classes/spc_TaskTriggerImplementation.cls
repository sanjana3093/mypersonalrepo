/********************************************************************************************************
*  @author          Deloitte
*  @date            05/29/2018
*  @description     This is the Implementation class for Task Trigger
*  @version         1.0
*********************************************************************************************************/
public class spc_TaskTriggerImplementation {

    /*************************************************************************************************
    * @author      Deloitte
    * @date        06/20/2018
    * @Description US-161068 When Welcome Call status is Complete and Call Outcome is Reached,
                     'Welcome Call Completed' picklist on Interaction record should be flipped to 'Yes'(On update)
    * @Param       Set of caseIdsForWelcome
    * @Return      Void
    **************************************************************************************************/
    public void setWelcomeCallFinished(Set<Id> caseIdsForWelcome) {
        List<PatientConnect__PC_Interaction__c> interactions = new List<PatientConnect__PC_Interaction__c>();
        //Get the Welcome Call Completed field on Interaction having Program case equal to the above case Ids
        for (PatientConnect__PC_Interaction__c interation : [SELECT
                Id, spc_Welcome_Call_complete__c, PatientConnect__PC_Patient_Program__c
                FROM
                PatientConnect__PC_Interaction__c
                WHERE
                PatientConnect__PC_Patient_Program__c IN : caseIdsForWelcome
                AND spc_Welcome_Call_complete__c != 'Yes']) {

            //For these 'Welcome Call' tasks, the 'Welcome Call Completed' picklist on Interaction record should be flipped to 'Yes'
            interation.spc_Welcome_Call_complete__c = 'Yes';
            interactions.add(interation);
        }
        //Update the interactions
        if (! interactions.isEmpty()) {
            spc_Database.upd(interactions);
        }
    }


    /**************************************************************************************
    * @author      Deloitte
    * @date        05/29/2018
    * @Description Generic method to get details of current Task and then create respective child tasks
    * @Param       mapFollowUpTasks
    * @Param       Set of caseIdsFollowupTasks
    * @Param       mapTasks
    * @Return      Void
    ***************************************************************************************/
    public void createFollowupTasks(Map<Id, spc_ApexConstants.PicklistValue> mapFollowUpTasks, Set<Id> caseIdsFollowupTasks, Map<Id, Task> mapTasks) {
        //Find caseOwnerIds
        Map<Id, Case> mapCase = new Map<Id, Case> ([SELECT Id, OwnerId, CreatedById, spc_HIPAA_Consent_Received__c from Case Where id in :caseIdsFollowupTasks]);
        List<spc_TaskProcessManager.TaskWrapper> tasksToCreate = new List<spc_TaskProcessManager.TaskWrapper>();
        for (Id taskId : mapFollowUpTasks.keySet()) {
            Task taskObj = mapTasks.get(taskId);
            Case caseObj = mapCase.get(taskObj.WhatId);
            //Create followupTask
            spc_TaskProcessManager.TaskWrapper followUpTask = new spc_TaskProcessManager.TaskWrapper();
            followUpTask.relatedTo = caseObj.Id;
            followUpTask.pcProgram = taskObj.PatientConnect__PC_Program__c;
            followUpTask.channel = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CHANNEL_CALL);
            followUpTask.priority = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_PRIORITY_NORMAL);
            followUpTask.status = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_NOT_STARTED);
            followUpTask.dueDate = System.today() + 1;
            followUpTask.direction = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_DIR_OUTBOUND);
            followUpTask.ownerId = caseObj.OwnerId;
            //Assign category, subCategory and subject based on the task
            if (mapFollowUpTasks.get(taskId) == spc_ApexConstants.PicklistValue.TASK_SUB_PATIENT_DISCONTINUATION) {
                followUpTask.category = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CAT_DISCONTINUATION);
                followUpTask.subCategory = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUBCATEGORY_SECONDATTEMPT);
                followUpTask.Subject = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_PATIENT_DISCONTINUATION_2);
            } else if (mapFollowUpTasks.get(taskId) == spc_ApexConstants.PicklistValue.TASK_SUB_PERFORM_WELCOME_CALL) {
                if (null != caseObj && String.isBlank(caseObj.spc_HIPAA_Consent_Received__c)
                        || spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO) == caseObj.spc_HIPAA_Consent_Received__c) {
                    continue;
                }
                followUpTask.category = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CAT_WELCOMECALL);
                followUpTask.reAttemptCount = 1 + (taskObj.spc_ReAttempt_Count__c == null ? 1 : taskObj.spc_ReAttempt_Count__c);
                if (followUpTask.reAttemptCount == 2) {
                    followUpTask.subCategory = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUBCATEGORY_SECONDATTEMPT);
                } else if (followUpTask.reAttemptCount == 3) {
                    followUpTask.subCategory = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUBCATEGORY_THIRDATTEMPT);
                } else {
                    followUpTask.subCategory = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUBCATEGORY_ADDITIONALATTEMPT);
                }
                followUpTask.subject = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_PERFORM_WELCOME_CALL) + ' (' + 'Attempt ' +   Integer.valueOf(followUpTask.reAttemptCount) + ')';
            }
            tasksToCreate.add(followUpTask);
        }

        //Pass the tasks details to processManager to created if needed
        if (!tasksToCreate.isEmpty()) {
            spc_TaskProcessManager.checkExistingTaskAndCreateNew(tasksToCreate);
        }

    }

    /**************************************************************************************
    * @author      Deloitte
    * @date        08/23/2018
    * @Description Generic method update program case field in Task
    * @Param       List of Tasks
    * @Param       Set of related Object ids.
    * @Return      Void
    ***************************************************************************************/
    public void procesTaskForProgramCases(List<Task> tasks, Set<Id> relatedObjectsList) {
        List<String> caseRecordTypes = new List<String>();
        caseRecordTypes.add(spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        caseRecordTypes.add(spc_ApexConstants.CASE_PRETREATMENT_RECORD_TYPE);
        caseRecordTypes.add(spc_ApexConstants.CASE_POSTTREATMENT_RECORD_TYPE);
        caseRecordTypes.add(spc_ApexConstants.CASE_INQUIRY_RECORD_TYPE);
        caseRecordTypes.add(spc_ApexConstants.CASE_APPEAL_RECORD_TYPE);
        caseRecordTypes.add(spc_ApexConstants.CASE_BI_BV_RECORD_TYPE);
        caseRecordTypes.add(spc_ApexConstants.CASE_PA_RECORD_TYPE);
        Map<Id, Case> mapCases = new Map<Id, Case>([Select Id, PatientConnect__PC_Is_Program_Case__c, PatientConnect__PC_Program__c
                FROM Case WHERE Id in: relatedObjectsList AND recordType.developerName in: caseRecordTypes ]);
        for (Task task : tasks) {
            if (mapCases.containsKey(task.WhatId)) {
                task.PatientConnect__PC_Program__c = task.WhatId ;
            }
        }
    }


    /**************************************************************************************
    * @author      Deloitte
    * @date        10/15/2018
    * @Description Update Whatid for Manual Created Tasks
    * @Param       List of ObjectToTaksListMap
    * @Param       List of whatIds
    * @Param       ObjectName
    * @Return      Void
    ***************************************************************************************/
    public void processTasksForCases(List<Task> ObjectToTaksListMap, List<ID> whatIds, String ObjectName) {
        Map<ID, ID> caseToObjectMap = new Map<ID, ID>();
        if (ObjectName == spc_ApexConstants.PC_CASE) {
            for (Case currCase : [Select ID, PatientConnect__PC_Program__c, PatientConnect__PC_Is_Program_Case__c, spc_Is_Enquiry_Case__c from Case where ID IN : whatIds]) {
                if (!currCase.PatientConnect__PC_Is_Program_Case__c && !currCase.spc_Is_Enquiry_Case__c && currCase.PatientConnect__PC_Program__c != null) {
                    caseToObjectMap.put(currCase.Id, currCase.PatientConnect__PC_Program__c);
                }
            }
        }
        if (ObjectName == spc_ApexConstants.OBJ_PROGRAM_COV) {
            for (PatientConnect__PC_Program_Coverage__c currCov : [Select ID, PatientConnect__PC_Program__c from PatientConnect__PC_Program_Coverage__c where ID IN : whatIds]) {
                if (currCov.PatientConnect__PC_Program__c != null) {
                    caseToObjectMap.put(currCov.Id, currCov.PatientConnect__PC_Program__c);
                }
            }
        }
        if (ObjectName == spc_ApexConstants.OBJ_INTERACTION) {
            for (PatientConnect__PC_Interaction__c interaction : [Select ID, PatientConnect__PC_Patient_Program__c from PatientConnect__PC_Interaction__c where ID IN : whatIds]) {
                if (interaction.PatientConnect__PC_Patient_Program__c != null) {
                    caseToObjectMap.put(interaction.Id, interaction.PatientConnect__PC_Patient_Program__c);
                }
            }
        }
        if (!caseToObjectMap.isEmpty()) {
            for (Task newTask : ObjectToTaksListMap) {
                ID whatid = caseToObjectMap.get(newTask.WhatId);
                if (newTask.WhatId.getSObjectType() == Case.sObjectType) {
                    newTask.PatientConnect__PC_Program__c = newTask.WhatId;
                } else if (newTask.WhatId.getSObjectType() == PatientConnect__PC_Program_Coverage__c.sObjectType) {
                    newTask.spc_Program_Coverage__c = newTask.WhatId;
                } else if (newTask.WhatId.getSObjectType() == PatientConnect__PC_Interaction__c.sObjectType) {
                    newTask.spc_Interaction__c = newTask.WhatId;
                }
                newTask.WhatId = whatid;
            }
        }
    }

    /**************************************************************************************
    * @author      Deloitte
    * @date        08/23/2018
    * @Description Method checks and restrict deletion of tasks for all except System Admins.
    * @Param       taskAndOwnerMap
    * @Return      Void
    ***************************************************************************************/
    public void restrictDeletionOfTask(Map<Id, Task> taskAndOwnerMap) {
        for (User user : [SELECT Id, userRole.name, name, profile.name from User where id = :UserInfo.getUserId() LIMIT 1] ) {
            if (!spc_ApexConstants.SYSTEM_ADMINISTRATOR.equalsIgnoreCase(user.profile.name)) {
                Task task = taskAndOwnerMap.get(user.id);
                if ( null != task) {
                    //Add error to the users task
                    task.addError(Label.spc_Restrict_Task_Deletion);
                }
            }
        }

    }

    /**************************************************************************************
    * @author      Deloitte
    * @date        12/17/2018
    * @Description Create task wrappers and call method of spc_TasKProcessManager to create tasks.
    * @Param       Set of Case ids and parameters to populate task fields
    * @Param       DueDate
    * @Param       Category
    * @Param       Channel
    * @Param       Direction
    * @Param       Priority
    * @Param       Status
    * @Param       Subject
    * @Return      Void
    ***************************************************************************************/
    public void createFollowUpTasks(Set<Id> programCaseIds, Date d, String cat, String subcat, String channel, String dir, String priority, String status, String subject) {
        List<spc_TaskProcessManager.TaskWrapper> taskWrappers = new List<spc_TaskProcessManager.TaskWrapper>();
        List<Case> programCases = [SELECT Id, OwnerId, spc_Interaction_Id__c FROM Case WHERE Id IN :programCaseIds];
        //Create taskwrapper object for each tasks
        for (Case prgmCase : programCases) {
            spc_TaskProcessManager.TaskWrapper tw = new spc_TaskProcessManager.TaskWrapper();

            tw.dueDate = d;
            tw.OwnerId = prgmCase.OwnerId;
            tw.category = cat;
            tw.subCategory = subcat;
            tw.channel = channel;
            tw.direction = dir;
            tw.priority = priority;
            tw.status = status;
            tw.subject = subject;
            tw.relatedTo = prgmCase.Id;
            if (cat == spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_OVERDUE_PIC)) {
                tw.interactionId = prgmCase.spc_Interaction_Id__c;
            }
            if ((cat == spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_SOC_LOGISTIC_CALL) &&
                    subcat == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_CAT_FIRST_ATTEMPT)) ||
                    (cat == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CAT_SITE_OF_CARE))) {
                tw.pcProgram = prgmCase.Id;
            }
            //Call SOC to Confirm Infusion Start - Followup task
            if (cat == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CAT_SITE_OF_CARE)
                    && subcat == spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_CONFIRM_INFUSION)) {
                tw.pcProgram = prgmCase.Id;
                tw.interactionId = prgmCase.spc_Interaction_Id__c;
            }
            taskWrappers.add(tw);
        }

        //Send the list of taskWrappers to ProcessManager to create tasks
        if (!taskWrappers.isEmpty()) {
            spc_TaskProcessManager.checkExistingTaskAndCreateNew(taskWrappers);
        }
    }

}

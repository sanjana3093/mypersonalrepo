/*********************************************************************************************************
class Name      : PSP_Card_and_Coupon_Trigger_Impl 
Description     : Trigger Implementation Class for Card and Coupon  Object
@author         : Divya Eduvulapati
@date           : October 24, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Divya Eduvulapati            October 24, 2019          Initial Version
****************************************************************************************************************/ 
public with sharing class PSP_Card_and_Coupon_Trigger_Impl {  
    /*Boolean flag to check if card has been updated once*/
    private static boolean isExecuted = true;     
    /* Map of account id and contact id */
    final Map<ID, ID> contAccMap = new Map<Id, ID>();
        
    /**************************************************************************************
    * @author      : Divya Eduvulapati
    * @date        : 10/24/2019
    * @Description : Method to update references of the Last Active Card 
                     when the "replacement" check box goes from checked to unchecked
    * @Param       : Null
    * @Return      : Void
    ***************************************************************************************/
    public void updateLastActiveCardReferences(Map<Id, PSP_Card_and_Coupon__c> oldCardMap, Map<Id,PSP_Card_and_Coupon__c> newCardMap, List<PSP_Card_and_Coupon__c> newCardList) {       
        /*Map of Last Active card Name and current card Patient ID*/
        final Map<String,Id> patientMap = new Map<String,Id>();
        /*Map of Last Active card Name and current card HCP ID*/
        final Map<String,Id> hcpMap = new Map<String,Id>();
        /*Map of Last Active card Name and current card  ID*/
        final Map<String,Id> cardMap = new Map<String,Id>();
        /*List of  cards that has to be updated*/
        final List<PSP_Card_and_Coupon__c> cardsToUpdateList = new List<PSP_Card_and_Coupon__c>(); 
        /*List of  cards that has to be updated*/
        final List<PSP_Card_and_Coupon__c> cardsList = new List<PSP_Card_and_Coupon__c>(); 
        /*Set of LastActivecards*/
        final Set<String> lstActvCardSet = new Set<String>();
        
       for (PSP_Card_and_Coupon__c card :newCardList) {
             if(oldCardMap != null && newCardMap != null && oldCardMap.containskey(card.id) && newCardMap.containskey(card.id) &&
                oldCardMap.get(card.id).PSP_Replacement__c != newCardMap.get(card.id).PSP_Replacement__c && card.PSP_Replacement__c == false &&
                card.PSP_Last_Active_Card1__c !=null && card.RecordTypeId == PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CARDANDCOUPON_RT_CARD, PSP_Card_and_Coupon__c.getSObjectType()) &&
                card.Name!=card.PSP_Last_Active_Card1__c &&  card.PSP_Assigned_to__c!= null) {
                   patientMap.put(card.PSP_Last_Active_Card1__c, card.PSP_Assigned_to__c);
                   hcpMap.put(card.PSP_Last_Active_Card1__c, card.PSP_Assigned_to__c);
                   CardMap.put(card.PSP_Last_Active_Card1__c, card.Id);
                   cardsToUpdateList.add(card);
                } else if(oldCardMap!=null && newCardMap!=null && oldCardMap.containskey(card.id) && newCardMap.containskey(card.id) &&
                          oldCardMap.get(card.id).PSP_Replacement__c != newCardMap.get(card.id).PSP_Replacement__c &&
                          card.PSP_Replacement__c == false && card.PSP_Last_Active_Card1__c != null &&
                          card.RecordTypeId == PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CARDANDCOUPON_RT_CARD, PSP_Card_and_Coupon__c.getSObjectType())
                          && card.Name != card.PSP_Last_Active_Card1__c) {       
                            cardsToUpdateList.add(card);                      
            }
        }

        for(Contact cnt : [Select Id, AccountId from Contact where Id in :patientMap.values()]) {
            contAccMap.put(cnt.id, cnt.AccountId);
        }
        //Combining the keys of two maps into a single set 
        lstActvCardSet.addAll(patientMap.keySet());
        lstActvCardSet.addAll(hcpMap.keySet()); 
        //Updating care program enrollee records
        updCareProgEnrolleeRec(patientMap, hcpMap, cardMap, lstActvCardSet );
        //updating cards
        updCards (cardsToUpdateList, cardsList );           
    }
    /**************************************************************************************
    * @author      : Divya Eduvulapati
    * @date        : 10/24/2019
    * @Description : Method to update care program enrollee records
    * @Param1      : Map<String,Id> patientMap
    * @Param2      : Map<String,Id> hcpMap
    * @Param3      : Map<String,Id> cardMap
    * @Param4      : Set<String> lstActvCardSet
    * @Return      : Void
    ***************************************************************************************/
    private void updCareProgEnrolleeRec(Map<String,Id> patientMap, Map<String,Id> hcpMap, Map<String,Id> cardMap, Set<String> lstActvCardSet ) {
        /*List of CareProgramEnrollee records*/
        final List<CareProgramEnrollee> cPrgEnrlleeRec = new List<CareProgramEnrollee> ();  
         //Fetching only those  CPE records that matches with atleast one of the Patient or Physcian fields on the current card
        if (CareProgramEnrollee.sObjectType.getDescribe().isAccessible()) {
            
        for (CareProgramEnrollee cPE: [select id,PSP_Card_Number__c,PSP_Card_Number__r.Name,AccountId,PSP_Enrolled_Physician__c FROM CareProgramEnrollee where PSP_Card_Number__r.Name in : lstActvCardSet 
                                       AND (AccountId in:contAccMap.values() OR (PSP_Enrolled_Physician__c in :hcpMap.values()))]) {
                //condition to correctly update card number of  the CPE record with the current card number   
                for(Id contId : contAccMap.keySet()) {                        
                    if(( patientMap.get(cPE.PSP_Card_Number__r.Name) != null && cPE.AccountId == contAccMap.get(contId))
                        || (cPE.PSP_Enrolled_Physician__c == hcpMap.get(CPE.PSP_Card_Number__r.Name) && hcpMap.get(cPE.PSP_Card_Number__r.Name) != null)) {
                        cPE.PSP_Card_Number__c = cardMap.get(CPE.PSP_Card_Number__r.Name);
                        cPrgEnrlleeRec.add(cPE);
                     }     
                }
            } 
        }
        Database.update(cPrgEnrlleeRec);
    }
    /**************************************************************************************
    * @author      : Divya Eduvulapati
    * @date        : 10/24/2019
    * @Description : Method to update cards
    * @Param1      : List<PSP_Card_and_Coupon__c> cardsToUpdateList
    * @Param2      : List<PSP_Card_and_Coupon__c> cardsList
    * @Return      : Void
    ***************************************************************************************/
    private void updCards (List<PSP_Card_and_Coupon__c> cardsToUpdateList, List<PSP_Card_and_Coupon__c> cardsList) {
        for (PSP_Card_and_Coupon__c card: cardsToUpdateList) {
            final PSP_Card_and_Coupon__c cardObjectRec = new PSP_Card_and_Coupon__c ( Id=card.Id );
            //To update the last active card field on cards with the current card number
            cardObjectRec.PSP_Last_Active_Card1__c= card.Name;    
            cardsList.add(cardObjectRec);
        } 
        //To run the process only once
        if(isExecuted) { 
            isExecuted=false;
            Database.update(cardsList);            
        } 
    }
 }
/**
* @author Deloitte
* @date 23 Mar 2018
*
* @description This class is used for Home Page Task and Alerts
*/

global with sharing class spc_HomeProgramActivities_CPV_Controller {
//Use @AuraEnabled to enable client- and server-side access to the method
    public static final Integer QUERY_OVERDUE_ACTIVITIES = 1;
    public static final Integer QUERY_MY_ACTIVITIES = 2;
    public static final Integer QUERY_ACTIVITIES_DUE_TODAY = 3;
    public static final Integer QUERY_ACTIVITIES_DUE_TOMORROW = 4;
    public static final Integer QUERY_ACTIVITIES_STARRED = 5;
    public static final Integer QUERY_ACTIVITIES_DUE_THIS_WEEK = 6;
    public static final String ACTIVITY_STATUS_COMPLETED = 'Completed';

    /*******************************************************************************************************
    * @description Method to retrieve Activities for the week
    * @param fromDate,DateTime used if a specific time is given
    * @param toDate,DateTime used if a specific time is given
    * @return List<activityWrapper >
    */
    @AuraEnabled
    public static List<activityWrapper > getThisWeekActivities(Datetime fromDate, Datetime toDate) {
        List<activityWrapper > lstActivities = getActivities(QUERY_ACTIVITIES_DUE_THIS_WEEK, fromDate, toDate, null);
        List<alertWrapper> lstAlertWrappers = getActiveUserAlerts();
        List<activityWrapper> lstAlerts = convertAlertsAsTasks(lstAlertWrappers);


        List<activityWrapper> lstActivityAndAlerts = new List<activityWrapper>();
        lstActivityAndAlerts.addAll(lstActivities);
        lstActivityAndAlerts.addAll(lstAlerts);
        return lstActivityAndAlerts ;
    }

    /*******************************************************************************************************
    * @description Method to retrieve Overdue Activities
    * @param fromDate,DateTime used if a specific time is given
    * @param toDate,DateTime used if a specific time is given
    * @return List<activityWrapper >
    */

    @AuraEnabled
    public static List<activityWrapper > getOverDueActivities(Datetime fromDate, Datetime toDate, String filter) {
        List<activityWrapper > lstActivities = getActivities(QUERY_OVERDUE_ACTIVITIES, fromDate, toDate, filter);
        return lstActivities ;
    }

    /*******************************************************************************************************
    * @description Method to retrieve Activities Due Today
    * @param filter String
    * @return List<activityWrapper >
    */
    @AuraEnabled
    public static List<activityWrapper > getActivitiesDueToday(String filter) {
        List<activityWrapper > lstActivities = getActivities(QUERY_ACTIVITIES_DUE_TODAY, null, null, filter);
        return lstActivities ;
    }

    /*******************************************************************************************************
    * @description Method to retrieve Activities Due Tomorrow
    * @param filter String
    * @return List<activityWrapper >
    */
    @AuraEnabled
    public static List<activityWrapper > getActivitiesDueTomorrow(String filter) {
        List<activityWrapper > lstActivities = getActivities(QUERY_ACTIVITIES_DUE_TOMORROW , null, null, filter);
        return lstActivities ;
    }

    /*******************************************************************************************************
    * @description Method to retrieve Activities that have been starred by the user
    * @param fromDate,DateTime used if a specific time is given
    * @param toDate,DateTime used if a specific time is given
    * @param filter String
    * @return List<activityWrapper >
    */
    @AuraEnabled
    public static List<activityWrapper > getStarredActivities(Datetime fromDate, Datetime toDate, String filter) {
        List<activityWrapper > lstActivities = getActivities(QUERY_ACTIVITIES_STARRED , fromDate, toDate, filter);
        return lstActivities ;
    }

    /*******************************************************************************************************
    * @description Method to Update the Starred Field on a task
    * @param taskId String
    * @return activityWrapper
    */
    @AuraEnabled
    public static activityWrapper updateTask(String taskID) {
        activityWrapper aw = null;

        try {
            //Main query
            String query = getSearchQueryString();
            query += ' WHERE Id = \'' + taskID + '\' LIMIT 1';

            //Query without Access check since access has been asserted in the getSearchQueryString method
            List<Task> tasks = spc_Database.queryWithAccess(query , false);

            if (tasks.size() > 0) {
                Task t = tasks[0];
                t.PatientConnect__PC_Starred__c = !t.PatientConnect__PC_Starred__c;
                spc_Database.upd(t);//FLS/CRUD Check enforced in the database method

                aw = new activityWrapper(t);
            }
        } catch (exception e) {
            spc_Utility.logAndThrowException(e);
        }
        return aw;
    }

    /*******************************************************************************************************
    * @description Method to Update the Priority
    * @param taskId String
    * @return activityWrapper
    */

    @AuraEnabled
    public static activityWrapper updatePriority(String taskID) {
        activityWrapper aw = null;

        try {
            //Main query
            String query = getSearchQueryString();
            query += ' WHERE Id = \'' + taskID + '\' LIMIT 1';

            //Query without Access check since access has been asserted in the getSearchQueryString method
            List<Task> tasks = spc_Database.queryWithAccess(query , false);

            if (tasks.size() > 0) {
                Task t = tasks[0];
                t.Priority = 'High';
                spc_Database.upd(t);//FLS/CRUD Check enforced in the database method

                aw = new activityWrapper(t);
                List<activityWrapper> new_aws = new List<activityWrapper>();
                new_aws.add(aw);
                List<activityWrapper> updated_aw = setAdditionalInfoForActivities(new_aws);
                aw = updated_aw[0];
            }
        } catch (exception e) {
            spc_Utility.logAndThrowException(e);
        }
        return aw;
    }

    /*******************************************************************************************************
    * @description Method to Update the Status to Close the Task
    * @param taskId String
    * @return activityWrapper
    */
    @AuraEnabled
    public static activityWrapper closeTask(String taskID) {
        activityWrapper aw = null;

        try {
            List<String> fields = new List<String> {'Subject', 'Status', 'WhatID', 'Priority', 'ActivityDate', 'PatientConnect__PC_Starred__c', 'PatientConnect__PC_Category__c', 'PatientConnect__PC_Program__c'};
            spc_Database.assertAccess('Task', spc_Database.Operation.Reading, fields);
            fields = new List<String> {'Name', 'IsActive'};
            spc_Database.assertAccess('User', spc_Database.Operation.Reading, fields);
            fields = new List<String> {'Name'};
            spc_Database.assertAccess('Account', spc_Database.Operation.Reading, fields);
            //Main query
            String query = getSearchQueryString();
            query += ' WHERE Id = \'' + taskID + '\' LIMIT 1';

            //Query without Access check since access has been asserted in the getSearchQueryString method
            List<Task> tasks = spc_Database.queryWithAccess(query , false);

            if (tasks.size() > 0) {
                /*
                * FLS Check Added
                */
                fields = new List<String> {'PatientConnect__PC_Starred__c'};
                spc_Database.assertAccess('Task', spc_Database.Operation.Updating, fields);
                Task t = tasks[0];
                t.Status = ACTIVITY_STATUS_COMPLETED;
                spc_Database.upd(t);

                aw = new activityWrapper(t);
                List<activityWrapper> new_aws = new List<activityWrapper>();
                new_aws.add(aw);
                List<activityWrapper> updated_aw = setAdditionalInfoForActivities(new_aws);

                aw = updated_aw[0];
            }
        } catch (exception e) {
            spc_Utility.logAndThrowException(e);
        }
        return aw;
    }

    /*******************************************************************************************************
    * @description Method to Return standard query search string
    * @return Query String
    */
    private static string getSearchQueryString() {
        List<String> fields = new List<String> {'Subject', 'Status', 'WhatID', 'WhoID', 'Priority', 'ActivityDate', 'PatientConnect__PC_Starred__c', 'PatientConnect__PC_Category__c', 'PatientConnect__PC_Program__c'};
        spc_Database.assertAccess('Task', spc_Database.Operation.Reading, fields);
        fields = new List<String> {'Name', 'IsActive'};
        spc_Database.assertAccess('User', spc_Database.Operation.Reading, fields);
        fields = new List<String> {'Name'};
        spc_Database.assertAccess('Account', spc_Database.Operation.Reading, fields);
        return 'SELECT Id, toLabel(Subject), Status, WhatID, WhoID, toLabel(Priority), OwnerId, ActivityDate, Owner.Name, Owner.IsActive, PatientConnect__PC_Starred__c, PatientConnect__PC_Category__c, PatientConnect__PC_Program__c, PatientConnect__PC_Program__r.account.name, PatientConnect__PC_Program__r.account.id, PatientConnect__PC_Channel__c, What.RecordTypeId, What.Type, What.Name, toLabel(PatientConnect__PC_Program__r.RecordType.Name) FROM Task ';
    }

    /*******************************************************************************************************
    * @description Method to set Additional Information For activities
    * @param lstActivities List<activityWrapper>
    * @return List<activityWrapper>
    */
    private static List<activityWrapper> setAdditionalInfoForActivities(List<activityWrapper> lstActivities) {
        List<activityWrapper> ret_lstActivities = new List<activityWrapper>();

        List<String> taskRelatedIds = new List<String>();
        for (activityWrapper aw : lstActivities) {
            taskRelatedIds.add(aw.relatedToRecordID);
        }

        List<Case> cases = [SELECT ID, AccountId, Account.Name, toLabel(RecordType.Name) FROM Case where ID IN :taskRelatedIds];

        Map<String, Case> caseAccountMap = new Map<String, Case>();
        for (Case c : cases) {
            caseAccountMap.put(c.Id, c);
        }

        Case relatedCase;
        activityWrapper updatedActivityWrapper = new activityWrapper();
        for (activityWrapper aw : lstActivities) {
            if (caseAccountMap.containsKey(aw.relatedToRecordID)) {
                relatedCase = caseAccountMap.get(aw.relatedToRecordID);
                updatedActivityWrapper = aw;
                updatedActivityWrapper.patientName = relatedCase.Account.Name;
                updatedActivityWrapper.patientID = relatedCase.Account.Id;
                updatedActivityWrapper.patientURL = '../' + relatedCase.Id;
                updatedActivityWrapper.relatedToRecordTypeName = relatedCase.RecordType.Name;
                ret_lstActivities.add(updatedActivityWrapper);
            } else {
                ret_lstActivities.add(aw);
            }
        }
        return  ret_lstActivities;

    }

    /*******************************************************************************************************
    * @description Utility Retrieve activity method
    * @param queryType, Integer to represent the type of query being made
    * @param fromDate, used if a specific time is given
    * @param toDate, used if a specific time is giv
    * @return List<activityWrapper>
    */
    private static List<activityWrapper> getActivities(Integer queryType, dateTime fromDate, dateTime toDate, String filter) {
        List<activityWrapper > lstActivities = new List<activityWrapper>();
        List<activityWrapper > lstActivitiesWithAdditionalInfo = new List<activityWrapper>();

        try {
            //Main query
            String query = getSearchQueryString(); //'Select Id, Subject, Priority, Status, OwnerId, ActivityDate, Owner.Name, Owner.IsActive, PatientConnect__PC_Starred__c, PatientConnect__PC_Category__c, PatientConnect__PC_Program__c, PatientConnect__PC_Program__r.account.name, PatientConnect__PC_Program__r.account.id from Task ';

            //Where clause that will change depending on type of query
            String whereQuery = ' where OwnerId = \'' + userinfo.getUserID() + '\'';

            //Limit or Order
            String orderQuery = '';

            //Specific logic for each type of query
            if (queryType == QUERY_OVERDUE_ACTIVITIES) {
                whereQuery += ' AND ActivityDate < TODAY';
                orderQuery += ' ORDER BY ActivityDate ';
            } else if ( queryType == QUERY_ACTIVITIES_DUE_TODAY ) {
                whereQuery += ' AND ActivityDate= TODAY';
            } else if ( queryType == QUERY_ACTIVITIES_DUE_TOMORROW ) {
                whereQuery += ' AND ActivityDate= TOMORROW';
            } else if ( queryType == QUERY_ACTIVITIES_STARRED ) {
                whereQuery += ' AND PatientConnect__PC_Starred__c = true';
            } else if (queryType == QUERY_ACTIVITIES_DUE_THIS_WEEK) {
                Date today = Date.today();
                Date newDate = today.addDays(7);
                whereQuery += ' AND ActivityDate <= ' + String.valueOf(newDate);
                orderQuery += ' ORDER BY ActivityDate ';
            }

            //Add from/to dates if provided
            if (fromDate != null) {
                whereQuery += ' AND ActivityDate >= ' + fromDate;
            }
            if (toDate != null) {
                whereQuery += ' AND ActivityDate <= ' + toDate;
            }

            //Query for only non-completed activities
            whereQuery += ' AND status != \'' + ACTIVITY_STATUS_COMPLETED + '\'';

            if (filter != null) {
                whereQuery += ' AND (' + filter + ')';
            }

            query += whereQuery;
            query += orderQuery;


            //Query without Access check since access has been asserted in the getSearchQueryString method
            List<Task> lstTasks = spc_Database.queryWithAccess(query , false);

            //If activities found, add them to list.
            if (lstTasks.size() > 0) {
                for (Task t : lstTasks) {
                    lstActivities.add(new activityWrapper(t));
                }
            }

            lstActivitiesWithAdditionalInfo = setAdditionalInfoForActivities(lstActivities);
        } catch (exception e) {
            spc_Utility.logAndThrowException(e);
        }
        return lstActivitiesWithAdditionalInfo ;
    }

    /*******************************************************************************************************
    * @description Method to convert Alerts as Tasks
    * @param lstAlertWrappers List<alertWrapper>
    * @return List<activityWrapper>
    */
    public static List<activityWrapper> convertAlertsAsTasks(List<alertWrapper> lstAlertWrappers) {
        List<activityWrapper> lstAlertsAsTasks = new List<activityWrapper>();
        activityWrapper aw = new activityWrapper();
        for (alertWrapper alertWrapper : lstAlertWrappers) {
            aw = new activityWrapper(alertWrapper);
            lstAlertsAsTasks.add(aw);
        }
        return lstAlertsAsTasks;
    }

    /*******************************************************************************************************
    * @description Wrapper Class for Activity
    *
    */
    public class activityWrapper {
        @AuraEnabled public Object objTask {get; set;}
        @AuraEnabled public String taskID {get; set;}
        @AuraEnabled public String taskURL {get; set;}
        @AuraEnabled public String subject {get; set;}
        @AuraEnabled public String status {get; set;}
        @AuraEnabled public String priority {get; set;}
        @AuraEnabled public Integer priorityIndex {get; set;}
        @AuraEnabled public Integer index_priorityPlusDueDate {get; set;}
        @AuraEnabled public Integer index_dueDatePlusPriority {get; set;}
        @AuraEnabled public boolean isStarred {get; set;}
        @AuraEnabled public String category {get; set;}
        @AuraEnabled public String programID {get; set;}
        @AuraEnabled public String programURL {get; set;}
        @AuraEnabled public String patientID {get; set;}
        @AuraEnabled public String patientName {get; set;}
        @AuraEnabled public String patientURL {get; set;}
        @AuraEnabled public String navigationText {get; set;}
        @AuraEnabled public String dueDate {get; set;}
        @AuraEnabled public String channel {get; set;}
        @AuraEnabled public Boolean isOverdue {get; set;}
        @AuraEnabled public Boolean isDueToday {get; set;}
        @AuraEnabled public Boolean isDueTomorrow {get; set;}
        @AuraEnabled public Boolean isAlert {get; set;}

        @AuraEnabled public String relatedToRecordID {get; set;}
        @AuraEnabled public String relatedToRecordURL {get; set;}
        @AuraEnabled public String relatedToRecordName {get; set;}
        @AuraEnabled public String relatedToObjectName {get; set;}
        @AuraEnabled public String relatedToRecordTypeName {get; set;}
        @AuraEnabled public String relatedToRecordTypeID {get; set;}
        @AuraEnabled public String svgIconName {get; set;}


        public activityWrapper() {

        }
        public activityWrapper(Task t) {
            this.objTask = t;
            this.taskID = t.ID;
            this.taskURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + t.ID;
            this.subject = t.Subject;
            this.status = t.Status;
            this.priority = t.Priority;
            this.priorityIndex = this.priority == 'High' ? 2 : (this.priority == 'Normal' ? 3 : 4);
            this.category = t.PatientConnect__PC_Category__c;
            this.programID = t.PatientConnect__PC_Program__c;
            this.programURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + t.PatientConnect__PC_Program__c;
            if (t.ActivityDate != null) {
                this.dueDate = t.ActivityDate.format();
            }
            this.isStarred = t.PatientConnect__PC_Starred__c;
            this.relatedToRecordID = t.WhatID;
            this.relatedToRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + t.WhatID;
            this.relatedToRecordTypeID = t.What.RecordTypeId;
            this.relatedToObjectName = t.What.Type;
            this.relatedToRecordName = t.What.Name;
            this.svgIconName = String.isBlank(t.PatientConnect__PC_Channel__c) ? 'task' : t.PatientConnect__PC_Channel__c.toLowerCase() ;
            this.isOverdue = t.ActivityDate < Date.today();
            this.isDueToday = t.ActivityDate == Date.today();
            this.isDueTomorrow = t.ActivityDate == Date.today().addDays(1);
            this.isAlert = false;
            this.channel = t.PatientConnect__PC_Channel__c;

            if (t.PatientConnect__PC_Program__c != null && t.PatientConnect__PC_Program__r.account != null) {
                this.patientID = t.PatientConnect__PC_Program__r.account.id;
                this.patientName = t.PatientConnect__PC_Program__r.account.name;
                this.patientURL = '../' + this.patientID;
                this.relatedToRecordTypeName = t.PatientConnect__PC_Program__r.RecordType.Name;
                this.navigationText = t.PatientConnect__PC_Program__r.account.name;
            } else {
                this.patientID = '';
                this.patientName = '';
                this.patientURL = '';
                //PC-2504 For non case related tasks add Object Name as Task Title
                this.navigationText = t.What.Name;
            }
            if (t.ActivityDate != null) {
                String dtFormat = getFormattedDate(t.ActivityDate);
                String x = dtFormat + this.priorityIndex;
                String y = this.priorityIndex + dtFormat;
                this.index_dueDatePlusPriority = Integer.valueOf(x);
                this.index_priorityPlusDueDate = Integer.valueOf(y);
            } else {
                this.index_dueDatePlusPriority = this.priorityIndex;
                this.index_priorityPlusDueDate = this.priorityIndex;
            }
        }
        public activityWrapper(alertWrapper a) {
            this.isAlert = true;
            this.objTask = a;
            this.patientName = a.patientName;

            this.taskID = a.alertID;
            if (a.alertDate != null) {
                this.dueDate = String.valueOf(a.alertDate.date().format());
                this.isOverdue = a.alertDate.date() < Date.today();
                this.isDueToday = a.alertDate.date() == Date.today();
                this.isDueTomorrow = a.alertDate.date() == Date.today().addDays(1);
            } else {
                this.isOverdue = false;
                this.isDueToday = false;
                this.isDueTomorrow = false;
            }

            this.subject = a.alertReason;

            this.category = a.alertType;
            this.priority = 'High';

            this.priorityIndex = this.priority == 'High' ? 2 : (this.priority == 'Normal' ? 3 : 4);
            this.patientName = a.patientName;
            this.patientID = a.patientId;


            this.relatedToRecordID = a.caseId;
            this.relatedToRecordName = a.caseNumber;
            this.relatedToRecordTypeName = a.caseType;

            this.programURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + a.caseId;
            this.taskURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + a.alertID;
            this.patientURL = '../' + a.patientId;



            this.svgIconName = 'warning';

            if (a.alertDate != null) {

                String dtFormat = getFormattedDate(a.alertDate.date());
                String x = dtFormat + this.priorityIndex;
                String y = this.priorityIndex + dtFormat;
                this.index_dueDatePlusPriority = Integer.valueOf(x);
                this.index_priorityPlusDueDate = Integer.valueOf(y);
            } else {
                this.index_dueDatePlusPriority = this.priorityIndex;
                this.index_priorityPlusDueDate = this.priorityIndex;
            }
        }
        public String getFormattedDate(Date dt) {
            String y = String.valueOf(dt.year());
            String m = String.valueOf(dt.month());
            String d = String.valueOf(dt.day());
            m = m.length() > 1 ? m : '0' + m;
            d = d.length() > 1 ? d : '0' + d;
            String dtFormat = y + m + d;
            return dtFormat;
        }


    }

    /*******************************************************************************************************
    * @description retrieve Users Alerts based on the Programs they are assigned to
    * @return List<alertWrapper>
    */
    @AuraEnabled
    public static List<alertWrapper> getActiveUserAlerts() {

        List<alertWrapper> lstAlertWrapper = new List<alertWrapper>();
        try {
            DateTime newDate = DateTime.Now().AddDays(7);
            List<String> fields = new List<String> {'Name', 'PatientConnect__PC_Program__c', 'PatientConnect__PC_Patient__c', 'PatientConnect__PC_Patient_ID__c', 'PatientConnect__PC_Reason_for_Alert__c', 'PatientConnect__PC_Type__c', 'PatientConnect__PC_Date_of_Alert__c', 'PatientConnect__PC_Alert_Status__c'};
            spc_Database.assertAccess('PatientConnect__PC_Alert__c', spc_Database.Operation.Reading, fields);

            fields = new List<String> {'ParentID', 'MemberID'};
            spc_Database.assertAccess('CaseTeamMember', spc_Database.Operation.Reading, fields);

            List<PatientConnect__PC_Alert__c> lstAlerts = [SELECT Id, Name, PatientConnect__PC_Program__c, PatientConnect__PC_Program__r.CaseNumber, toLabel(PatientConnect__PC_Program__r.RecordType.Name), PatientConnect__PC_Patient__c, PatientConnect__PC_Patient_ID__c, PatientConnect__PC_Reason_for_Alert__c, PatientConnect__PC_Type__c, PatientConnect__PC_Date_of_Alert__c, PatientConnect__PC_Alert_Status__c FROM PatientConnect__PC_Alert__c WHERE PatientConnect__PC_Alert_Status__c = 'New' AND PatientConnect__PC_Date_of_Alert__c <= :newDate AND PatientConnect__PC_Program__c IN (SELECT ParentID FROM CaseTeamMember WHERE MemberID = : userinfo.getUserID()) ORDER BY PatientConnect__PC_Date_of_Alert__c ASC];

            for (PatientConnect__PC_Alert__c a : lstAlerts) {
                lstAlertWrapper.add(new alertWrapper(a));
            }
        } catch (exception e) {
            spc_Utility.logAndThrowException(e);
        }

        return lstAlertWrapper;
    }

    /*******************************************************************************************************
    * @description retrieve List View for Alerts. This is a standard Salesforce Method found Here:
                     https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/ref_force_navigateToList.htm
    * @return List View
    */
    @AuraEnabled
    public static ListView getListViews() {
        String alertObj = 'PatientConnect__PC_Alert__c';

        try {
            List<String> fields = new List<String> {'Name', 'SobjectType'};
            spc_Database.assertAccess('ListView', spc_Database.Operation.Reading, fields);

            List<ListView> listviews = [SELECT Id, Name FROM ListView WHERE SobjectType = : alertObj and Name = 'New Alerts'];

            if (listviews.size() <= 0) {
                listviews = [SELECT Id, Name FROM ListView WHERE SobjectType = : alertObj  limit 1];
            }

            ListView listview = null;
            if (listviews.size() > 0) {
                listview = listviews[0];
            }
            return listview;
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
            return null;
        }
    }

    /*******************************************************************************************************
    * @description This method is used for overridden an alert
    * @param objAlertId String
    */

    @AuraEnabled
    public static void overrideAlert(String objAlertId) {
        if (String.isBlank(objAlertId)) {
            return;
        }
        try {
            //CRUD/FLS check for PatientConnect__PC_Alert__c
            List<String> fields = new List<String> {'Id'};
            spc_Database.assertAccess('PatientConnect__PC_Alert__c', spc_Database.Operation.Reading, fields);
            List<PatientConnect__PC_Alert__c> lstAlerts = [SELECT Id FROM PatientConnect__PC_Alert__c WHERE Id = : objAlertId];
            PatientConnect__PC_Alert__c oAlert;
            if (lstAlerts.size() > 0) {
                oAlert = lstAlerts[0];
            }
            String defaultVal = getDefaultOverriddenReasonValue();
            oAlert.PatientConnect__PC_Alert_Status__c = 'Overridden';
            if (!String.isBlank(defaultVal)) {
                oAlert.PatientConnect__PC_Reason_for_Override__c = defaultVal;
            }
            spc_Database.upd(oAlert);//CRUD/FLS enforced in the database upd method
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }
    }

    /*******************************************************************************************************
    * @description This method is used for getting the default overridden reason
    * @return defaultVal String
    */
    private static String getDefaultOverriddenReasonValue() {
        String defaultVal;
        Schema.DescribeFieldResult result = PatientConnect__PC_Alert__c.PatientConnect__PC_Reason_for_Override__c.getDescribe();
        List<Schema.PicklistEntry> pickVals = result.getPicklistValues();
        for (Schema.PicklistEntry pv : pickVals) {
            if (pv.isDefaultValue()) {
                defaultVal = pv.getValue();
            }
        }
        return defaultVal;
    }

    /*******************************************************************************************************
    * @description This method is used to get the return url
    * @return String URL
    */
    @AuraEnabled
    public static String getReturnUrl() {
        String key = PatientConnect__PC_Alert__c.getSobjectType().getDescribe().getKeyPrefix();
        return '/' + key;
    }

    /*******************************************************************************************************
    * @description Wrapper Class For alert
    *
    */
    public class alertWrapper {
        @AuraEnabled public String patientName {get; set;}
        @AuraEnabled public String patientId {get; set;}
        @AuraEnabled public String alertID {get; set;}
        @AuraEnabled public String alertReason {get; set;}
        @AuraEnabled public String alertType {get; set;}
        @AuraEnabled public DateTime alertDate {get; set;}
        @AuraEnabled public String caseId {get; set;}
        @AuraEnabled public String caseNumber {get; set;}
        @AuraEnabled public String caseType {get; set;}
        public alertWrapper(PatientConnect__PC_Alert__c a) {
            this.patientName = a.PatientConnect__PC_Patient__c;
            this.alertID = a.ID;
            this.alertReason = a.PatientConnect__PC_Reason_for_Alert__c;
            this.alertType = a.PatientConnect__PC_Type__c;
            this.alertDate = a.PatientConnect__PC_Date_of_Alert__c;
            this.caseId = a.PatientConnect__PC_Program__c;
            this.caseType = a.PatientConnect__PC_Program__r.RecordType.Name;
            this.patientId = a.PatientConnect__PC_Patient_ID__c;
            this.caseNumber = a.PatientConnect__PC_Program__r.CaseNumber;
        }
    }
}
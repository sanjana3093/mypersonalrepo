/*********************************************************************************
@description     : Test class for spc_PatientConsentDetails class
@author      : Deloitte
@date    : July 18, 2018
*********************************************************************************/
@isTest
public class spc_PatientConsentDetailsTest {
    Static user adminUserRec;

    static void testDataSetup() {
        adminUserRec = spc_Test_Setup.getProfileID();
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

    }

    @isTest
    static void getInstanceTest() {
        testDataSetup();
        System.runAs(adminUserRec) {
            test.startTest();
			
			spc_PatientConsentDetails newclass = new spc_PatientConsentDetails();

            Account patientRec = spc_Test_Setup.createTestAccount('PC_Patient');
            patientRec.PatientConnect__PC_Email__c = 'test@deloitte.com';
            insert patientRec;

            Case programCaseRec = spc_Test_Setup.newCase(patientRec.id, 'PC_Program');
            insert programCaseRec;
            spc_PatientConsentDetails consent = spc_PatientConsentDetails.getInstance(programCaseRec.Id);
            system.assert(consent != NULL);
            test.stopTest();
        }
    }

}
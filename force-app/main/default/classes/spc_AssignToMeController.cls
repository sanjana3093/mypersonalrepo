/**
* @author Deloitte
* @date Jan 15, 2019
*
* @description Controller class for spc_AssignToMe aura to assign a task
* based on the logged in user
*/

public without sharing class spc_AssignToMeController {
    /*******************************************************************************************************
    * @description Changes Ownership to current logged in user if user is case manager or above.
    * @param recordId the ID of the task to look up
    * @return String - Task updated status message.
    */
    @Auraenabled
    public static String updateOwnership(string recordId) {
        String updateStatus;
        List<Task> tasksToBeUpdated = new List<Task>();
        for (User loggedInUser : [SELECT Id, userRole.name, name, profile.name from User where id = :UserInfo.getUserId() LIMIT 1]) {
            if (loggedInUser.userRole.name == spc_ApexConstants.getUSerRole(spc_ApexConstants.USERRoles.USER_ROLE_SUPERVISOR)
                    || loggedInUser.userRole.name == spc_ApexConstants.getUSerRole(spc_ApexConstants.USERRoles.USER_ROLE_CASEMANAGER)) {
                Task tsk = new Task(Id = recordId);
                tsk.OwnerId = loggedInUser.id;
                tasksToBeUpdated.add(tsk);
            } else {
                updateStatus = label.spc_Not_A_Case_Manager;
            }
        }
        if (!tasksToBeUpdated.isEmpty()) {
            update tasksToBeUpdated;
            updateStatus = Label.spc_Task_Updated;
        }
        return updateStatus;
    }
}
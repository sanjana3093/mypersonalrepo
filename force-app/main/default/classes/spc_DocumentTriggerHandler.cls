/**
* @author Deloitte
* @date 07/13/2018
*
* @description This is the Trigger Handler class for Document Trigger
*/

public class spc_DocumentTriggerHandler extends spc_TriggerHandler {
    spc_DocumentTriggerImplementation docTriggerImplementation = new spc_DocumentTriggerImplementation();

    public override void beforeInsert() {
        //populate engagment program details
        engUpdate();

        //Populate info from parent doc
        childEngUpdate();


    }

    public override void beforeUpdate() {

        //populate engagment program details
        engUpdate();

    }
    public override void afterUpdate() {

        //change parent docuemnt status
        changeParentDocumentStatus();

    }

    /*******************************************************************************************************
    * @description  changing parent document status after doc split
    * @return void
    */
    public void changeParentDocumentStatus() {

        Set<Id> ParentDocIds = new Set<Id>();
        Map<Id, PatientConnect__PC_Document__c> oldMap = (Map<Id, PatientConnect__PC_Document__c>)Trigger.oldMap;
        for (PatientConnect__PC_Document__c newDoc : (List<PatientConnect__PC_Document__c>)Trigger.New) {
            if (newDoc.PatientConnect__PC_Parent_Document__c != null
                    && newDoc.PatientConnect__PC_Document_Status__c == 'Split Complete'
                    && oldMap.get(newDoc.Id).PatientConnect__PC_Document_Status__c != newDoc.PatientConnect__PC_Document_Status__c) {

                ParentDocIds.add(newDoc.PatientConnect__PC_Parent_Document__c);

            }
        }

        if (!ParentDocIds.isEmpty()) {
            List<PatientConnect__PC_Document__c> parentDocToUpdates = new List<PatientConnect__PC_Document__c>();
            for (PatientConnect__PC_Document__c parentDoc : [SELECT ID
                    , (SELECT ID,  PatientConnect__PC_Document_Status__c
                       from PatientConnect__Documents__r
                       where PatientConnect__PC_Document_Status__c
                       in ('Split in Progress', 'Awaiting Attachment')
                      )
                    from PatientConnect__PC_Document__c
                    where id in :ParentDocIds
                    and PatientConnect__PC_Document_Status__c = 'Split in Progress']) {
                if (parentDoc.PatientConnect__Documents__r.isEmpty()) {
                    parentDoc.PatientConnect__PC_Document_Status__c = 'Split Complete';
                    parentDocToUpdates.add(parentDoc);
                }
            }

            update parentDocToUpdates;
        }

    }
    /*******************************************************************************************************
    * @description  populate engagment program details
    * @return void
    */

    public void engUpdate() {
        //Added for US-192013 START
        //If Document is created with a "To Email Address" = Org wide Email #1 OR Org Wide Email#2
        //Populate Engagement Program field with 'Brexanalone Engagement Program'
        Map<String, List<PatientConnect__PC_Document__c>> mapEmailFaxDoc = new Map<String, List<PatientConnect__PC_Document__c>>();
        for (PatientConnect__PC_Document__c newDoc : (List<PatientConnect__PC_Document__c>)Trigger.New) {
            //Only if Program code is not set on the Document - This is required for sharing
            if (String.isBlank(newDoc.spc_Program_Code__c)) {
                //If this is email document
                if (newDoc.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.DOC_RT_EMAIL_INBOUND, PatientConnect__PC_Document__c.SobjectType) && null != newDoc.spc_to_Email_address__c) {
                    if (!mapEmailFaxDoc.isEmpty() && mapEmailFaxDoc.containsKey(newDoc.spc_to_Email_address__c)) {
                        mapEmailFaxDoc.get(newDoc.spc_to_Email_address__c).add(newDoc);
                    } else {
                        mapEmailFaxDoc.put(newDoc.spc_to_Email_address__c, new List<PatientConnect__PC_Document__c> {newDoc});
                    }
                }
                //If this is for FAX document
                else if (String.isNotBlank(newDoc.PatientConnect__PC_To_Fax_Number__c) ||  String.isNotBlank(newDoc.PatientConnect__PC_From_Fax_Number__c)) {
                    if (!mapEmailFaxDoc.isEmpty() && mapEmailFaxDoc.containsKey(newDoc.PatientConnect__PC_To_Fax_Number__c)) {
                        mapEmailFaxDoc.get( newDoc.PatientConnect__PC_To_Fax_Number__c).add(newDoc);
                    } else {
                        mapEmailFaxDoc.put( newDoc.PatientConnect__PC_To_Fax_Number__c, new List<PatientConnect__PC_Document__c> {newDoc});
                    }

                    if (!mapEmailFaxDoc.isEmpty() && mapEmailFaxDoc.containsKey(newDoc.PatientConnect__PC_From_Fax_Number__c)) {
                        mapEmailFaxDoc.get( newDoc.PatientConnect__PC_From_Fax_Number__c).add(newDoc);
                    } else {
                        mapEmailFaxDoc.put( newDoc.PatientConnect__PC_From_Fax_Number__c, new List<PatientConnect__PC_Document__c> {newDoc});
                    }
                }
            }
        }
        if (! mapEmailFaxDoc.isEmpty()) {
            docTriggerImplementation.populateEPForDocument(mapEmailFaxDoc);
        }
        //Added for US-192013 END

    }

    /*******************************************************************************************************
    * @description  Update child document information based on parent
    * @return void
    */
    public void childEngUpdate() {
        //Set of parent Doc Ids
        Set<Id> ParentDocIds = new Set<Id>();


        for (PatientConnect__PC_Document__c newDoc : (List<PatientConnect__PC_Document__c>)Trigger.New) {
            if (newDoc.PatientConnect__PC_Parent_Document__c != null) {
                ParentDocIds.add(newDoc.PatientConnect__PC_Parent_Document__c);
            }
        }

        Map<Id, PatientConnect__PC_Document__c> ParentDocs = new Map<Id, PatientConnect__PC_Document__c>();

        //Fetch Info from Parent Documents
        if (!ParentDocIds.isEmpty()) {
            ParentDocs = new Map<Id, PatientConnect__PC_Document__c>([Select Id, PatientConnect__PC_Engagement_Program__c , spc_Program_Code__c From PatientConnect__PC_Document__c Where Id IN : ParentDocIds ]);
        }


        //Update the Document with the information fetched from parent document
        for (PatientConnect__PC_Document__c newDoc : (List<PatientConnect__PC_Document__c>)Trigger.New) {
            if (newDoc.PatientConnect__PC_Parent_Document__c != null && ParentDocs.containsKey(newDoc.PatientConnect__PC_Parent_Document__c)) {
                if (ParentDocs.get(newDoc.PatientConnect__PC_Parent_Document__c).PatientConnect__PC_Engagement_Program__c != null)
                    newDoc.PatientConnect__PC_Engagement_Program__c = ParentDocs.get(newDoc.PatientConnect__PC_Parent_Document__c).PatientConnect__PC_Engagement_Program__c ;

                if (ParentDocs.get(newDoc.PatientConnect__PC_Parent_Document__c).spc_Program_Code__c != null)
                    newDoc.spc_Program_Code__c = ParentDocs.get(newDoc.PatientConnect__PC_Parent_Document__c).spc_Program_Code__c;
            }
        }


    }
}
/********************************************************************************************************
*  @author          Deloitte
*  @description     test class for Trigger handler class for shipment trigger
*  @param           
*  @date            aug 11, 2020
*********************************************************************************************************/  
@isTest
public with sharing class CCL_ShipmentTriggerHandler_Test {

    /*************************************************************************************
	* @author      : Deloitte
	* @date        : 16/Aug/2020
	* @Description : Data Setup to test the date update fields for the finished product records
	* @Param       : Void
	* @Return      : Void
	***************************************************************************************/
    @TestSetup
    static void shipmentRecordCreation() {



        Account shipmentAccount = CCL_TestDataFactory.createTestParentAccount();
        insert shipmentAccount;

        String timezoneKey ='Europe/Berlin';

        CCL_Time_Zone__c plantTimezone = CCL_TestDataFactory.createTimezoneTest(timezoneKey);

        insert plantTimezone;
        Account plantAccount = CCL_TestDataFactory.createPlantAccount(plantTimezone.Id);
        insert plantAccount;

        CCL_Therapy__c commercialTherapy = CCL_TestDataFactory.createTestTherapy();
        insert commercialTherapy;

        CCL_Therapy_Association__c siteTherapyAsso = CCL_TestDataFactory.createTestSiteTherapyAssoc(shipmentAccount.Id, commercialTherapy.Id);
        siteTherapyAsso.CCL_Infusion_Data_Collection__c = CCL_StaticConstants_MRC.VALUE_YES;
        insert siteTherapyAsso;

        CCL_Order__c testOrder =  CCL_Test_SetUp.createTestOrder();
        testOrder.CCL_Delivery_Chevron__c='Estimated Delivery';

        CCL_Shipment__c shipmentRec = CCL_TestDataFactory.createTestFinishedProductShipment(testOrder.Id, commercialTherapy.Id);
         shipmentRec.CCL_Plant__c = plantAccount.Id;
         shipmentRec.CCL_Infusion_Center__c = shipmentAccount.Id;
         shipmentRec.CCL_Estimated_FP_Delivery_Date_Time__c  = datetime.newInstance(1995, 7, 24, 10, 30, 0);
         shipmentRec.CCL_Estimated_FP_Shipment_Date_Time__c   = datetime.newInstance(2020, 10, 3, 10, 30, 0);
         shipmentRec.CCL_Actual_Shipment_Date_Time__c   = datetime.newInstance(1995, 7, 24, 10, 30, 0);
         shipmentRec.CCL_Expiry_Date_of_Shipped_Bags__c    = date.newInstance(1995, 7, 24);
       	 shipmentRec.CCL_Novartis_Batch_Id__c='12345';
         shipmentRec.CCL_Planned_Apheresis_Pickup_Date__c=datetime.newInstance(2020, 10, 3, 10, 30, 0);
       	 shipmentRec.CCL_Actual_Aph_Pick_up_Date_Time__c=datetime.newInstance(2020, 10, 3, 10, 30, 0);
       	 shipmentRec.CCL_Planned_Dewar_Arrival_Date_Time__c=datetime.newInstance(2020, 10, 3, 10, 30, 0);
       	 shipmentRec.CCL_Actual_Goods_Receipt_Date_Time__c=datetime.newInstance(2020, 10, 3, 10, 30, 0);
         //shipmentRec.CCL_TEXT_Planned_Dewar_Arrival_Date_Time__c=datetime.newInstance(2020, 10, 3, 10, 30, 0);
         shipmentRec.CCL_Indication_Clinical_Trial__c=commercialTherapy.Id;
         insert shipmentRec;

         final User testGMTUser = CCL_TestDataFactory.createTestGMTUser(CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE);        
          insert testGMTUser;
    }
    
    /*************************************************************************************
	* @author      : Deloitte
	* @date        : 27/Aug/2020
	* @Description : Testing CCL_Expiry_Date_of_Shipped_Bags_Text__c Date/time field format is correct. 
	* @Param       : Void
	* @Return      : Void
	***************************************************************************************/
    static testmethod void testDateTimeFieldUpdates() {
    final CCL_Shipment__c shipRec = [SELECT ID, Name, 
                                    CCL_Estimated_FP_Delivery_Date_Time__c, CCL_Estimated_FP_Delivery_Date_Time_Text__c,
                                    CCL_Estimated_FP_Shipment_Date_Time__c, CCL_Estimated_FP_Shipment_Date_Time_Text__c,
                                    CCL_Actual_Shipment_Date_Time__c, CCL_Actual_Shipment_Date_Time_Text__c,
                                    CCL_Expiry_Date_of_Shipped_Bags__c, CCL_Expiry_Date_of_Shipped_Bags_Text__c,
                                    CCL_Novartis_Batch_Id__c,CCL_Planned_Apheresis_Pickup_Date__c,
                                     CCL_Actual_Aph_Pick_up_Date_Time__c,CCL_Planned_Dewar_Arrival_Date_Time__c,
                                     CCL_Actual_Goods_Receipt_Date_Time__c,CCL_Indication_Clinical_Trial__c,
                                     CCL_Infusion_Center__c,CCL_TEXT_Planned_Dewar_Arrival_Date_Time__c
                                    FROM CCL_Shipment__c limit 1 ];
        
        // This asserts that that the date field is outputted in the appropriate DD MMM YYYY format
        system.assertEquals( '24 Jul 1995' , shipRec.CCL_Expiry_Date_of_Shipped_Bags_Text__c, 'Date fields do not match' );
       

    }

  /*************************************************************************************
	* @author      : Deloitte
	* @date        : 27/Aug/2020
  * @Description : Testing CCL_Estimated_FP_Shipment_Date_Time_Text__c Date/time field format is correct 
  *                 where the user is in a GMT time zone and the Plant Account is not. 
	* @Param       : Void
	* @Return      : Void
	***************************************************************************************/

    static testmethod void estimatedFPShipmentForGMTTimezonePlantNegativeTest() {
        final User sysUser=[SELECT Id,timezonesidkey,isActive 
                            FROM User 
                            WHERE timezonesidkey='Asia/Kolkata' AND isActive= true limit 1];

        Test.startTest();
        System.runAs(sysUser) {
        final CCL_Shipment__c shipRec = [SELECT ID, Name,
                                    CCL_Estimated_FP_Delivery_Date_Time__c, CCL_Estimated_FP_Delivery_Date_Time_Text__c,
                                    CCL_Estimated_FP_Shipment_Date_Time__c, CCL_Estimated_FP_Shipment_Date_Time_Text__c,
                                    CCL_Actual_Shipment_Date_Time__c, CCL_Actual_Shipment_Date_Time_Text__c,
                                    CCL_Expiry_Date_of_Shipped_Bags__c, CCL_Expiry_Date_of_Shipped_Bags_Text__c,
                                    RecordTypeId,CCL_Novartis_Batch_Id__c,CCL_Planned_Apheresis_Pickup_Date__c,
                                    CCL_Actual_Aph_Pick_up_Date_Time__c,CCL_Planned_Dewar_Arrival_Date_Time__c,
                                    CCL_Actual_Goods_Receipt_Date_Time__c,CCL_Indication_Clinical_Trial__c,
                                    CCL_Infusion_Center__c,CCL_TEXT_Planned_Dewar_Arrival_Date_Time__c
                                    FROM CCL_Shipment__c limit 1 ];
       
        // This asserts that that the date/time field is outputted in the appropriate DD MMM YYYY HH:MM format
        System.assertNotEquals( '03 Oct 2020 05:00' , shipRec.CCL_Estimated_FP_Shipment_Date_Time_Text__c ,'Excepted and actual values do not match' );
       } 
         Test.stopTest();
    }
	
	/*************************************************************************************
	* @author      : Deloitte
	* @date        : 27/Aug/2020
	* @Description : Testing CriticalFieldsLocked Flag update scenarios
	* @Param       : Void
	* @Return      : Void
	***************************************************************************************/

	public static testMethod void testUpdateCriticalFieldsLockedFlagForValidation() {
		Test.startTest();


        List<CCL_Shipment__c> shipment = [SELECT ID, Name, 
                                            CCL_Estimated_FP_Delivery_Date_Time__c, CCL_Estimated_FP_Delivery_Date_Time_Text__c,
                                            CCL_Estimated_FP_Shipment_Date_Time__c, CCL_Estimated_FP_Shipment_Date_Time_Text__c,
                                            CCL_Actual_Shipment_Date_Time__c, CCL_Actual_Shipment_Date_Time_Text__c,
                                            CCL_Expiry_Date_of_Shipped_Bags__c, CCL_Expiry_Date_of_Shipped_Bags_Text__c,
                                            CCL_Hospital_Purchase_Order_Number__c, CCL_TECH_Critical_Fields_Changed__c,
                                            RecordTypeId, CCL_Ship_to_Location__c, CCL_Shipment__c.CCL_Order__r.Id,
                                          	CCL_Novartis_Batch_Id__c,CCL_Planned_Apheresis_Pickup_Date__c,
                                            CCL_Actual_Aph_Pick_up_Date_Time__c,CCL_Planned_Dewar_Arrival_Date_Time__c,
                                          	CCL_Actual_Goods_Receipt_Date_Time__c,CCL_Indication_Clinical_Trial__c,
                                          	CCL_Infusion_Center__c,CCL_TEXT_Planned_Dewar_Arrival_Date_Time__c
                                            FROM CCL_Shipment__c 
                                            WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FINSIHEDPRODUCT
                                            LIMIT 1 ];

		shipment[0].CCL_Hospital_Purchase_Order_Number__c = '1233';
        Update shipment;
        Test.stopTest();
        
        CCL_Order__c updatedOrder = [SELECT Id, CCL_Critical_Fields_Locked__c 
                                    FROM CCL_Order__c 
                                    WHERE Id = :shipment[0].CCL_Order__r.Id];
		System.assertEquals(true, updatedOrder.CCL_Critical_Fields_Locked__c, 'Critical Fields Locked flag should be set as true');
	}
    
    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute updateInfusionDataCollectionField Positive scenario
	 *  @param
	 *  @date            December 14, 2020
	 *********************************************************************************************************/
        static testmethod void updateInfusionDataCollectionFieldTest() {
        List<CCL_Shipment__c> shipRec = [SELECT ID, Name,  CCL_Infusion_Data_Collection_Allowed__c
                                            FROM CCL_Shipment__c limit 1];

        System.assert(shipRec[0].CCL_Infusion_Data_Collection_Allowed__c); 
    }
    
    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute updateCriticalFieldsChangedFlag Negative scenario
	 *  @param
	 *  @date            December 14, 2020
	 *********************************************************************************************************/
    private static testMethod void testUpdateCriticalFieldsChangedFlagWithoutAnyChange() {
   

        List<CCL_Shipment__c> shipRec = [SELECT ID, Name, 
                                            CCL_Estimated_FP_Delivery_Date_Time__c, CCL_Estimated_FP_Delivery_Date_Time_Text__c,
                                            CCL_Estimated_FP_Shipment_Date_Time__c, CCL_Estimated_FP_Shipment_Date_Time_Text__c,
                                            CCL_Actual_Shipment_Date_Time__c, CCL_Actual_Shipment_Date_Time_Text__c,
                                            CCL_Expiry_Date_of_Shipped_Bags__c, CCL_Expiry_Date_of_Shipped_Bags_Text__c,
                                            CCL_Hospital_Purchase_Order_Number__c, CCL_TECH_Critical_Fields_Changed__c,
                                            RecordTypeId, CCL_Ship_to_Location__c,CCL_Novartis_Batch_Id__c,CCL_Planned_Apheresis_Pickup_Date__c,
                                            CCL_Actual_Aph_Pick_up_Date_Time__c,CCL_Planned_Dewar_Arrival_Date_Time__c,
                                          	CCL_Actual_Goods_Receipt_Date_Time__c,CCL_Indication_Clinical_Trial__c,
                                          	CCL_Infusion_Center__c,CCL_TEXT_Planned_Dewar_Arrival_Date_Time__c
                                            FROM CCL_Shipment__c 
                                            WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FINSIHEDPRODUCT
                                            LIMIT 1 ];

        Test.startTest();

            shipRec[0].CCL_Estimated_FP_Delivery_Date_Time__c  = datetime.newInstance(1996, 7, 24, 10, 30, 0);

            update shipRec[0];

            List<CCL_Shipment__c> shipUpdateRec = [SELECT ID, Name, 
                                                CCL_Estimated_FP_Delivery_Date_Time__c, CCL_Estimated_FP_Delivery_Date_Time_Text__c,
                                                CCL_Estimated_FP_Shipment_Date_Time__c, CCL_Estimated_FP_Shipment_Date_Time_Text__c,
                                                CCL_Actual_Shipment_Date_Time__c, CCL_Actual_Shipment_Date_Time_Text__c,
                                                CCL_Expiry_Date_of_Shipped_Bags__c, CCL_Expiry_Date_of_Shipped_Bags_Text__c,
                                                CCL_Hospital_Purchase_Order_Number__c, CCL_TECH_Critical_Fields_Changed__c,
                                                RecordTypeId, CCL_Ship_to_Location__c,
                                                CCL_Novartis_Batch_Id__c,CCL_Planned_Apheresis_Pickup_Date__c,
                                                CCL_Actual_Aph_Pick_up_Date_Time__c,CCL_Planned_Dewar_Arrival_Date_Time__c,
                                                CCL_Actual_Goods_Receipt_Date_Time__c,CCL_Indication_Clinical_Trial__c,
                                                CCL_Infusion_Center__c,CCL_TEXT_Planned_Dewar_Arrival_Date_Time__c
                                                FROM CCL_Shipment__c 
                                                WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FINSIHEDPRODUCT
                                                LIMIT 1 ];

            System.assertEquals(false, shipUpdateRec[0].CCL_TECH_Critical_Fields_Changed__c, 'Critical Fields Changed flag should be set as false');
            Test.stopTest();
    }
    
	/********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute updateCriticalFieldsChangedFlag
	 *  @param
	 *  @date            December 14, 2020
	 *********************************************************************************************************/
    private static testMethod void testUpdateCriticalFieldsChangedFlag() {
        
        List<CCL_Shipment__c> shipRec = [SELECT ID, Name, 
                                            CCL_Estimated_FP_Delivery_Date_Time__c, CCL_Estimated_FP_Delivery_Date_Time_Text__c,
                                            CCL_Estimated_FP_Shipment_Date_Time__c, CCL_Estimated_FP_Shipment_Date_Time_Text__c,
                                            CCL_Actual_Shipment_Date_Time__c, CCL_Actual_Shipment_Date_Time_Text__c,
                                            CCL_Expiry_Date_of_Shipped_Bags__c, CCL_Expiry_Date_of_Shipped_Bags_Text__c,
                                            CCL_Hospital_Purchase_Order_Number__c, CCL_TECH_Critical_Fields_Changed__c, 
                                         	CCL_Novartis_Batch_Id__c,CCL_Planned_Apheresis_Pickup_Date__c,
                                            CCL_Actual_Aph_Pick_up_Date_Time__c,CCL_Planned_Dewar_Arrival_Date_Time__c,
                                          	CCL_Actual_Goods_Receipt_Date_Time__c,CCL_Indication_Clinical_Trial__c,
                                          	CCL_Infusion_Center__c,CCL_TEXT_Planned_Dewar_Arrival_Date_Time__c,
                                            RecordTypeId
                                            FROM CCL_Shipment__c 
                                            WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FINSIHEDPRODUCT
                                            LIMIT 1 ];


       
		shipRec[0].CCL_Hospital_Purchase_Order_Number__c = '1233';

        Update shipRec;
        Test.startTest();
        List<CCL_Shipment__c> shipAfterUpdateRec = [SELECT ID, Name, 
                                                    CCL_Estimated_FP_Delivery_Date_Time__c, CCL_Estimated_FP_Delivery_Date_Time_Text__c,
                                                    CCL_Estimated_FP_Shipment_Date_Time__c, CCL_Estimated_FP_Shipment_Date_Time_Text__c,
                                                    CCL_Actual_Shipment_Date_Time__c, CCL_Actual_Shipment_Date_Time_Text__c,
                                                    CCL_Expiry_Date_of_Shipped_Bags__c, CCL_Expiry_Date_of_Shipped_Bags_Text__c,
                                                    CCL_Hospital_Purchase_Order_Number__c, CCL_TECH_Critical_Fields_Changed__c, 
                                                    CCL_Novartis_Batch_Id__c,CCL_Planned_Apheresis_Pickup_Date__c,
                                           			CCL_Actual_Aph_Pick_up_Date_Time__c,CCL_Planned_Dewar_Arrival_Date_Time__c,
                                          			CCL_Actual_Goods_Receipt_Date_Time__c,
                                                    RecordTypeId
                                                    FROM CCL_Shipment__c 
                                                    WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FINSIHEDPRODUCT
                                                    LIMIT 1 ];

        System.debug(LoggingLevel.FINEST, '::shipAfterUpdateRec[0] ' + JSON.serializePretty(shipAfterUpdateRec[0]));
        
        System.assertEquals(true, shipAfterUpdateRec[0].CCL_TECH_Critical_Fields_Changed__c, 'Critical Fields Changed flag should be set as true');
        Test.stopTest();
    }
    
    //

}
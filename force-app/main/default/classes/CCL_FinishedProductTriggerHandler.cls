/**
 * @description       : Trigger Handler for the CCL_Finished_Product__c Object Trigger
 * @author            : Deloitte
 * @group             : Deloitte
 * @last modified on  : 15-11-2020
 * @last modified by  : Deloitte
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   15-11-2020   Deloitte   Initial Version
**/
public class CCL_FinishedProductTriggerHandler {
    /*
    *  @author          Deloitte
    *  @description     Method to format the date fields on Finished Products
    *  @param           List<Trigger.New)
    *  @return          N/A
    *  @date            July 15, 2020
    */
    public void updateDateFieldsOnFP(List<CCL_Finished_Product__c> finishedProducts) {
        for(CCL_Finished_Product__c finishProduct : finishedProducts) {
            if(finishProduct.CCL_Manufacturing_Date__c!=null) {
                finishProduct.CCL_Manufacturing_Date_Text__c = CCL_Utility.getFormattedDate(finishProduct.CCL_Manufacturing_Date__c);
            }
            if(finishProduct.CCL_Actual_Batch_Release_Date__c!= null) {
                finishProduct.CCL_Actual_Batch_Release_Date_Text__c = CCL_Utility.getFormattedDate(finishProduct.CCL_Actual_Batch_Release_Date__c);
            }
            if(finishProduct.CCL_Expiry_Date__c!= null){
                finishProduct.CCL_Expiration_Date_Text__c= CCL_Utility.getFormattedDate(finishProduct.CCL_Expiry_Date__c);
            }
            if(finishProduct.CCL_Follow_Up_Infusion_Date__c!= null) {
                finishProduct.CCL_Follow_Up_Infusion_Date_Text__c= CCL_Utility.getFormattedDate(finishProduct.CCL_Follow_Up_Infusion_Date__c);
            }
            if(finishProduct.CCL_Actual_Infusion_Date_Time__c!= null) {
                finishProduct.CCL_Actual_Infusion_Date_Time_Text__c= CCL_Utility.getFormattedDate(finishProduct.CCL_Actual_Infusion_Date_Time__c.date());
                finishProduct.CCL_OBA_Invoice_Date_Time__c= finishProduct.CCL_Actual_Infusion_Date_Time__c.date()+60;
                finishProduct.CCL_OBA_invoice__c= CCL_Utility.getFormattedDate(finishProduct.CCL_OBA_Invoice_Date_Time__c.date());
            }
            if(finishProduct.CCL_OBA_Milestone_1_Date__c!= null) {
                finishProduct.CCL_OBA_Milestone_1__c= CCL_Utility.getFormattedDate(finishProduct.CCL_OBA_Milestone_1_Date__c);
            }
            if(finishProduct.CCL_OBA_Milestone_2_Date__c!= null) {
                finishProduct.CCL_OBA_Milestone_2__c= CCL_Utility.getFormattedDate(finishProduct.CCL_OBA_Milestone_2_Date__c);
            }
            if(finishProduct.CCL_OBA_Milestone_3_Date__c!= null) {
                finishProduct.CCL_OBA_Milestone_3__c= CCL_Utility.getFormattedDate(finishProduct.CCL_OBA_Milestone_3_Date__c);
            }
            if(finishProduct.CCL_OBA_Milestone_4_Date__c!= null) {
                finishProduct.CCL_OBA_Milestone_4__c= CCL_Utility.getFormattedDate(finishProduct.CCL_OBA_Milestone_4_Date__c);
             }
            if(finishProduct.CCL_OBA_Milestone_5_Date__c!= null) {
                finishProduct.CCL_OBA_Milestone_5__c= CCL_Utility.getFormattedDate(finishProduct.CCL_OBA_Milestone_5_Date__c);
            }
        }
    }
    /*
    *  @author          Deloitte
    *  @description     Method to update the count of Finished Products on Shipment
    *  @param           List<Trigger.New)
    *  @return          N/A
    *  @date            July 15, 2020
    */
    public void updateNumberOfDosesOnShipment(List<CCL_Finished_Product__c> finishedProducts) {
       set<Id> shipmentIds = new Set<Id>();
        for(CCL_Finished_Product__c fp:finishedProducts) {
            shipmentIds.add(fp.CCL_Shipment__c);
            System.debug('Shipment value' + fp.CCL_Shipment__c);
        }
        List<CCL_Shipment__c> shipmentLstToUpdate = new List<CCL_Shipment__c>();
        List<CCL_Shipment__c> shipmentRec =CCL_FinishedProductTriggerAccessor.fetchShipmentRecordsToUpdateNoOfDoses(shipmentIds);
        for(CCL_Shipment__c ship:shipmentRec){
            ship.CCL_Number_of_Doses_Shipped__c = ship.Finished_Products__r.size();
            System.debug('CCL_Number_of_Doses_Shipped__c' +ship.CCL_Number_of_Doses_Shipped__c);
            shipmentLstToUpdate.add(ship);
        }
        if(shipmentLstToUpdate.size()>0) {
            CCL_FinishedProductTriggerAccessor.updateShipmentRecords(shipmentLstToUpdate);
        }
    }
    /*
    *  @author          Deloitte
    *  @description     Method to update the Expiry date of Finished Products on Shipment 
    *  @param           List<Trigger.New)
    *  @return          N/A
    *  @date            July 15, 2020
    */
    public void updateFPExpiryDateOnShipment(List<CCL_Finished_Product__c> finishedProducts) {
        System.debug('IN EXPIRY DATE METHOD!!!! '+finishedProducts);
        set<Id> shipmentIdList = new set<Id>();
        List<CCL_Shipment__c> shipmentRecordList= new List<CCL_Shipment__c>();
        List<CCL_Shipment__c> shipmentRecordListToBeUpdated= new List<CCL_Shipment__c>();
        
        // Map<CCL_Shipment__c,Date> latestExpiryDateBasedOnShipmentRecordMap=new Map<CCL_Shipment__c,Date>();
        if(finishedProducts!=null && !finishedProducts.isEmpty() ) {
            for(CCL_Finished_Product__c fpRecord :finishedProducts ) {
                shipmentIdList.add(fpRecord.CCL_Shipment__c);
                System.debug('shipmentIdList: '+shipmentIdList);
            }
            if(shipmentIdList!=null && shipmentIdList.size()>0) {
                try {
                    shipmentRecordList=CCL_FinishedProductTriggerAccessor.fetchShipmentRecordsToUpdateExpiryDate(shipmentIdList);
                    System.debug('shipmentRecordList: '+shipmentRecordList);
                } catch(Exception e) {
                    System.debug('Some exception occurred while querying Shipment Records: '+e.getMessage());
                }  
            }
            if(shipmentRecordList!=null && shipmentRecordList.size()>0) {
                for(CCL_Shipment__c shipmentRecord:shipmentRecordList ) {
                    if(shipmentRecord.Finished_Products__r != null && shipmentRecord.Finished_Products__r.size()>0) {
                        shipmentRecord.CCL_Expiry_Date_of_Shipped_Bags__c=shipmentRecord.Finished_Products__r[0].CCL_Expiry_Date__c;
                    }
                    shipmentRecordListToBeUpdated.add(shipmentRecord);
                }
            }
            if(shipmentRecordListToBeUpdated.size()>0) {
                try {
                    CCL_FinishedProductTriggerAccessor.updateShipmentRecords(shipmentRecordListToBeUpdated);
                } catch(Exception e) {
                    System.debug('Some exception occurred while querying Updating Shipment Records: '+e.getMessage());
                } 
            }
        }      
    }
    /*
    *  @author          Deloitte
    *  @description     Method to update the Expiry date of Finished Products on old Shipment 
    *  @param           List<Trigger.Old>
    *  @return          N/A
    *  @date            July 15, 2020
    */
    public void updateFPExpiryDateOnShipmentAfterUpdateOldValues(List<CCL_Finished_Product__c> finishedProductsOld) {
        System.debug('IN AFTER UPDATE OLD VALUES EXPIRY DATE METHOD!!!! '+finishedProductsOld);
        set<Id> shipmentIdList = new set<Id>();
        List<CCL_Shipment__c> shipmentRecordList= new List<CCL_Shipment__c>();
        List<CCL_Shipment__c> shipmentRecordListToBeUpdated= new List<CCL_Shipment__c>();
        
        // Map<CCL_Shipment__c,Date> latestExpiryDateBasedOnShipmentRecordMap=new Map<CCL_Shipment__c,Date>();
        if(finishedProductsOld!=null && finishedProductsOld.size()>0 ) {
            
            for(CCL_Finished_Product__c fpRecord :finishedProductsOld ) {
                shipmentIdList.add(fpRecord.CCL_Shipment__c);
                System.debug('shipmentIdList: '+shipmentIdList);
            }
            
            if(shipmentIdList!=null && shipmentIdList.size()>0) {
                try {
                    shipmentRecordList=CCL_FinishedProductTriggerAccessor.fetchShipmentRecordsToUpdateExpiryDate(shipmentIdList);
                    System.debug('shipmentRecordList: '+shipmentRecordList);
                } catch(Exception e) {
                    System.debug('Some exception occurred while querying Shipment Records: '+e.getMessage());
                }
            }
            if(shipmentRecordList!=null && shipmentRecordList.size()>0) {
                for(CCL_Shipment__c shipmentRecord:shipmentRecordList ) {
                    if(shipmentRecord.Finished_Products__r != null && shipmentRecord.Finished_Products__r.size()>0) {
                        shipmentRecord.CCL_Expiry_Date_of_Shipped_Bags__c=shipmentRecord.Finished_Products__r[0].CCL_Expiry_Date__c;
                    } else {
                            shipmentRecord.CCL_Expiry_Date_of_Shipped_Bags__c=null;
                    }
                    shipmentRecordListToBeUpdated.add(shipmentRecord);
                }
            }
            if(!shipmentRecordListToBeUpdated.isEmpty()) {
                try {
                    CCL_FinishedProductTriggerAccessor.updateShipmentRecords(shipmentRecordListToBeUpdated);
                } catch(Exception e) {
                    System.debug('Some exception occurred while querying Updating Shipment Records: '+e.getMessage());
                }
            }
        }
    }
}

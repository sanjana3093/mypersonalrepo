/********************************************************************************************************
*  @author          Deloitte
*  @date            17/07/2018
*  @description     This is the Trigger Handler class for Program Coverage Trigger
*  @version         1.0
*********************************************************************************************************/
public class spc_ProgramCoverageTriggerHandler extends spc_TriggerHandler {

    spc_CommunicationTriggers CommtriggerImplementation = new spc_CommunicationTriggers();

    /*************************************************************************************************
    * @author       Deloitte
    * @date         05/29/2018
    * @Description  BeforeInsert trigger for Program coverage object
    * @Return       Void
    **************************************************************************************************/
    public override void beforeInsert() {
        List<PatientConnect__PC_Program_Coverage__c> programCoverageListToActivate = new List<PatientConnect__PC_Program_Coverage__c>();
        //Update Program Coverage Active if the start date is populated for PAP and Copay Type
        for (PatientConnect__PC_Program_Coverage__c coverageObj : (List<PatientConnect__PC_Program_Coverage__c>)Trigger.New) {
            if ((coverageObj.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.PROGRAMCOVERAGE_RT_PAP, PatientConnect__PC_Program_Coverage__c.SobjectType)
                    || coverageObj.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.PROGRAMCOVERAGE_RT_COPAY, PatientConnect__PC_Program_Coverage__c.SobjectType))
                    && coverageObj.spc_Enrollment_Start_Date__c != NULL) {
                programCoverageListToActivate.add(coverageObj);
            }
            //Updating system created Private Coverage to Medical Coverage as Medical Coverage (Private) is not in system.
            if (coverageObj.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.PROGRAMCOVERAGE_RT_MEDICALCOVERAGEPRIV, PatientConnect__PC_Program_Coverage__c.SobjectType)) {
                coverageObj.RecordTypeId = spc_ApexConstants.getRTId(spc_ApexConstants.RecordTypeName.PROGRAMCOVERAGE_RT_MEDICALCOVERAGE, PatientConnect__PC_Program_Coverage__c.SobjectType);
            }
        }
        if (!programCoverageListToActivate.isEmpty()) {
            for (PatientConnect__PC_Program_Coverage__c rec : programCoverageListToActivate) {
                rec.PatientConnect__PC_Coverage_Status__c = spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE;
            }
        }
    }

    /*************************************************************************************************
    * @author       Deloitte
    * @date         05/29/2018
    * @Description  BeforeUpdate trigger for Program coverage object
    * @Return       Void
    **************************************************************************************************/
    public override void beforeUpdate() {
        List<PatientConnect__PC_Program_Coverage__c> programCoverageListToActivate = new List<PatientConnect__PC_Program_Coverage__c>();
        PatientConnect__PC_Program_Coverage__c oldCoverage;
        //Update Program Coverage Active if the start date is populated for PAP and Copay Type
        for (PatientConnect__PC_Program_Coverage__c coverageObj : (List<PatientConnect__PC_Program_Coverage__c>)Trigger.New) {
            oldCoverage = Trigger.oldMap != NULL ? (PatientConnect__PC_Program_Coverage__c)Trigger.oldMap.get(coverageObj.Id) : NULL;
            if ((coverageObj.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.PROGRAMCOVERAGE_RT_PAP, PatientConnect__PC_Program_Coverage__c.SobjectType)
                    || coverageObj.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.PROGRAMCOVERAGE_RT_COPAY, PatientConnect__PC_Program_Coverage__c.SobjectType))
                    && oldCoverage.spc_Enrollment_Start_Date__c == NULL && coverageObj.spc_Enrollment_Start_Date__c != NULL) {
                programCoverageListToActivate.add(coverageObj);
            }
        }
        if (!programCoverageListToActivate.isEmpty()) {
            for (PatientConnect__PC_Program_Coverage__c rec : programCoverageListToActivate) {
                rec.PatientConnect__PC_Coverage_Status__c = spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE;
            }
        }
    }

    /*************************************************************************************************
    * @author       Deloitte
    * @date         05/29/2018
    * @Description  AfterInsert trigger for Program coverage object
    * @Return       Void
    **************************************************************************************************/
    public override void afterInsert() {
        CommtriggerImplementation.afterInsertProgramCoverageTriggers((List<PatientConnect__PC_Program_Coverage__c>)Trigger.New);
    }

    /*************************************************************************************************
    * @author       Deloitte
    * @date         05/29/2018
    * @Description  AfterUpdate trigger for Program coverage object
    * @Return       Void
    **************************************************************************************************/
    public override void afterUpdate() {
        CommtriggerImplementation.afterUpdateProgramCoverageTriggers((List<PatientConnect__PC_Program_Coverage__c>)Trigger.New);
    }
}
/*********************************************************************************

    *  @author          Deloitte
    *  @description     Processor class for the health plan enrollment wizard page.
    *  @date            12-Jan-17
    *  @version         1.0
*********************************************************************************/
global with sharing class spc_HealthPlanEWPProcessor implements PatientConnect.PC_EnrollmentWizard.PageProcessor {

  /********************************************************************************************************
   *  @author           Deloitte
   *  @date             19/06/2018
   *  @description      Implemented method from PC_EnrollmentWizard.PageProcessor
   *  @param            enrollmentCase - Case Object
   *  @param            pageState - Map with field values from page
   *  @return           None
   *********************************************************************************************************/
  Private Static String FIELDSET_KEY = 'fsFields';
  public static void processEnrollment(Case enrollmentCase, Map<String, Object> pageState) {
    if (pageState == null) {
      throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException('Missing page state');
    }
    if (enrollmentCase.AccountId == null) {
      throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException('Patient Account not created');
    }

    Account selfAccount;
    List<Object> lstHealthPlan = (List<Object>) pageState.get('lstHealthPlan');
    PatientConnect__PC_Health_Plan__c hp;
    List<PatientConnect__PC_Health_Plan__c> hps = new List<PatientConnect__PC_Health_Plan__c>();
    //[PC-1368]Resolving the Blocker issue raised by the force reviewer scanner
    String pObjectName = spc_ApexConstants.spc_HEALTH_PLAN;
    List<RecordType> lstHpRecordTypes = new List<RecordType>();
    if (!String.isBlank(pObjectName)) {
      lstHpRecordTypes = [Select Id, Name From RecordType Where SobjectType = :pObjectName];
    }

    for (Object o : lstHealthPlan) {
      Map<String, Object> healthPlan = (Map<String, Object>) o;
      String pDeveloperName =  String.valueOf(healthPlan.get('recordName'));
      Id pRecordTypeId;
      if (!lstHpRecordTypes.isEmpty() && !String.isBlank(pDeveloperName)) {
        for (RecordType lstHpRt : lstHpRecordTypes) {
          if (lstHpRt.Name == pDeveloperName) {
            pRecordTypeId = lstHpRt.Id;
          }
        }
      }

      hp = new PatientConnect__PC_Health_Plan__c();
      if (!String.isBlank(String.valueOf(healthPlan.get('healthPlanId')))) {
        hp.id = String.valueOf(healthPlan.get('healthPlanId'));
      }
      hp.PatientConnect__Group_No__c = String.valueOf(healthPlan.get('groupNo'));
      hp.PatientConnect__ID_Policy_No__c = String.valueOf(healthPlan.get('policyNo'));
      hp.PatientConnect__PC_Cardholder_Employer__c = String.valueOf(healthPlan.get('cardholderEmployer'));
      if (String.isNotBlank((String) healthPlan.get('policyholderPhone'))) {
        hp.spc_Policy_Holder_Phone__c = FormatPhone(String.valueOf(healthPlan.get('policyholderPhone')));
      }
      String cardholderRelationshipToPatient = (String) healthPlan.get('cardholderRelationshipToPatient');
      if (String.isNotBlank(cardholderRelationshipToPatient) && cardholderRelationshipToPatient != spc_ApexConstants.PICKLIST_NONE) {
        if (cardholderRelationshipToPatient.capitalize() == 'SELF') {
          if (selfAccount == NULL) {
            /* Get Account Info for "SELf" relationship*/
            selfAccount = [Select Name, PatientConnect__PC_Date_of_Birth__c from Account where Id = :enrollmentCase.AccountId];
          }

          hp.PatientConnect__PC_Cardholder_Relationship_to_Patient__c = cardholderRelationshipToPatient;
          hp.PatientConnect__PC_Card_Holder_Name__c = selfAccount.Name;
          if (selfAccount.PatientConnect__PC_Date_of_Birth__c != NULL) {
            hp.PatientConnect__PC_Cardholder_s_Date_of_Birth__c = selfAccount.PatientConnect__PC_Date_of_Birth__c;
          }
        } else {
          hp.PatientConnect__PC_Cardholder_Relationship_to_Patient__c = cardholderRelationshipToPatient;

          hp.PatientConnect__PC_Card_Holder_Name__c = String.valueOf(healthPlan.get('cardholderName'));

          String cardholderDOB = (String) healthPlan.get('cardholdersBirthDate');
          if (String.isNotBlank(cardholderDOB)) {
            hp.PatientConnect__PC_Cardholder_s_Date_of_Birth__c = Date.valueOf(cardholderDOB);
          }
        }
      }
      String hpEffectiveDate = (String) healthPlan.get('healthPlanEffectiveDate');
      if (String.isNotBlank(hpEffectiveDate)) {
        hp.PatientConnect__PC_Effective_Date__c = Date.valueOf(hpEffectiveDate);
      }
      String hpExpirationDate = (String) healthPlan.get('healthPlanExpirationDate');
      if (String.isNotBlank(hpExpirationDate)) {
        hp.PatientConnect__PC_Expiration_Date__c = Date.valueOf(hpExpirationDate);
      }
      hp.PatientConnect__PC_Payer__c = String.valueOf(healthPlan.get('payerRef'));
      hp.PatientConnect__PC_Plan_Claim_Card_No__c = String.valueOf(healthPlan.get('healthPlanClaimCardNo'));

      String healthPlanStatus = (String) healthPlan.get('healthPlanStatus');
      if (String.isNotBlank(healthPlanStatus) && healthPlanStatus != spc_ApexConstants.PICKLIST_NONE) {
        hp.PatientConnect__PC_Plan_Status__c = healthPlanStatus;
      }

      String healthPlanType = (String) healthPlan.get('healthPlanType');
      if (String.isNotBlank(healthPlanType) && healthPlanType != spc_ApexConstants.PICKLIST_NONE) {
        hp.PatientConnect__PC_Plan_Type__c = healthPlanType;
      }
      hp.PatientConnect__PC_Rx_Bin__c = String.valueOf(healthPlan.get('planRxBin'));
      hp.PatientConnect__PC_Rx_Grp__c = String.valueOf(healthPlan.get('planRxGrp'));
      hp.PatientConnect__PC_Rx_PCN__c = String.valueOf(healthPlan.get('planRxPCN'));
      hp.RecordTypeId = pRecordTypeId;
      if (String.isBlank(String.valueOf(healthPlan.get('healthPlanId')))) {
        hp.PatientConnect__Patient__c = enrollmentCase.AccountId;
      }

      if (healthPlan.containsKey(FIELDSET_KEY)) {
        Map<String, Object> fieldSetMap = (Map<String, Object>) healthPlan.get(FIELDSET_KEY);
        spc_FieldSetController.copyFieldSetValues(fieldSetMap, hp);
      }
      hps.add(hp);
    }
    if (!hps.isEmpty()) {
      spc_Database.ups(hps);//FLS/CRUD Check enforced in the PC_database method
    }

  }
  private static String FormatPhone(String Phone) {
    string nondigits = '[^0-9]';
    string PhoneDigits;

    // remove all non numeric
    PhoneDigits = Phone.replaceAll(nondigits, '');

    // 10 digit: reformat with dashes
    if (PhoneDigits.length() == 10)
      return '(' + PhoneDigits.substring(0, 3) + ')' + '-' +
             PhoneDigits.substring(3, 6) + '-' +
             PhoneDigits.substring(6, 10);
    // 11 digit: if starts with 1, format as 10 digit
    if (PhoneDigits.length() == 11) {
      if (PhoneDigits.substring(0, 1) == '1') {
        return '(' + PhoneDigits.substring(1, 4) + ')' + '-' +
               PhoneDigits.substring(4, 7) + '-' +
               PhoneDigits.substring(7, 11);

      }
    }

    // if it isn't a 10 or 11 digit number, return the original because
    // it may contain an extension or special information
    return ( Phone );
  }
}
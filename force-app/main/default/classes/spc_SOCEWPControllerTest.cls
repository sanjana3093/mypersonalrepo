/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for spc_SOCEWPController
    *  @date            06/18/2018
    *  @version         1.0
    *
    *  Modification Log:
    * -------------------------------------------------------------------------------------------------------
    *  Developer                Date                                    Description
    * -------------------------------------------------------------------------------------------------------
    *  Samiksha Pradhan            07/20/2018                              Initial version
*********************************************************************************************************/
@isTest
public class spc_SOCEWPControllerTest {
    public static Id ID_CAREGIVER_ACCOUNT_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
    public static Id ID_Applicant_RECORDTYPE = Schema.SObjectType.PatientConnect__PC_Applicant__c.getRecordTypeInfosByName().get('Online').getRecordTypeId();
    @testSetup static void setup() {
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        Account acc = spc_Test_Setup.createTestAccount('PC_Patient');
        acc.name = 'patient';
        acc.PatientConnect__PC_Email__c = 'test@email.com';
        insert acc;
        system.assertNotEquals(acc, NULL);

    Case enrollmentCase = new case ();
        enrollmentCase.AccountId = acc.id;
        enrollmentCase.type = 'inquiry';
        enrollmentCase.Status = 'new';
        enrollmentCase.recordtypeid = ID_CAREGIVER_ACCOUNT_RECORDTYPE;
        insert enrollmentCase;
        system.assertNotEquals(enrollmentCase, NULL);

        PatientConnect__PC_Applicant__c app = new PatientConnect__PC_Applicant__c();
        app.recordtypeid = ID_Applicant_RECORDTYPE;
        insert app;
        system.assertNotEquals(app, NULL);
    }

    public static testmethod void testExecution1() {
        test.startTest();
        Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> pickLists = spc_SOCEWPController.getHCOTypes();
        System.assertEquals(True, pickLists.size() > 0);
        test.StopTest();
    }
    public static testmethod void testExecution2() {
        test.starttest();
    case ocase=[select id from Case where recordtypeid=:ID_CAREGIVER_ACCOUNT_RECORDTYPE LIMIT 1];
        system.assertNotEquals(ocase, NULL);
        PatientConnect__PC_Applicant__c app = [select id from PatientConnect__PC_Applicant__c LIMIT 1];
        system.assertNotEquals(app, NULL);
        List<spc_SOCEWPController.HCORecord> hcorecord = spc_SOCEWPController.getApplicantHCO(ocase.id, app.id);
        test.stopTest();
 }
 public static testmethod void testExecution3(){
     test.startTest();
     Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> getPicklst=spc_SOCEWPController.getPicklistEntryMap();
     test.stopTest();
     
 }
 public static testmethod void testExecution4(){
     test.startTest();
     spc_SOCEWPController.HCORecord hc=spc_SOCEWPController.getFields();
     test.stopTest();
 }
 public static testmethod void testExecution5(){
     test.startTest();
     Account acc =spc_SOCEWPController.getHCOTypes2();
     test.stoptest();
 }
 public static testmethod void testEexcution6(){
     test.startTest();
     List<spc_SOCEWPController.HCORecord> hcoRecord=spc_SOCEWPController.searchRecords('patient','patient');
     test.stoptest();
 }
 
 public static testmethod void testEexcution7(){
     test.startTest();
     List<Account> lstAcc=[select id from Account];
     system.assertNotEquals(lstAcc.size(),0);
     List<string> lstAccount=new List<String>();
     lstAccount.add(lstAcc[0].id);
     List<spc_SOCEWPController.HCORecord> lstHCORECORDS=spc_SOCEWPController.searchRecordsByIds(lstAccount);
     spc_SOCEWPController.HCOSearchFilter hcp=new spc_SOCEWPController.HCOSearchFilter();
     test.stoptest();
 }
 
 public static testmethod void testEexcution8(){
        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
        hcoAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        hcoAcc.Name = 'Test HCO Account';
        hcoAcc.Type = 'Clinic';
        insert hcoAcc;
        system.debug('test acc:' +[select Id, Name, Type, RecordTypeId, RecordType.Name, PatientConnect__PC_Primary_Address_1__c, PatientConnect__PC_Primary_Address_2__c, PatientConnect__PC_Primary_Address_3__c, PatientConnect__PC_Primary_Address__c, PatientConnect__PC_Primary_City__c, PatientConnect__PC_Primary_State__c, PatientConnect__PC_Primary_Zip_Code__c, PatientConnect__PC_Status__c, PatientConnect__PC_Email__c, Phone, Fax, PatientConnect__PC_Communication_Language__c, PatientConnect__PC_Preferred__c, PatientConnect__PC_Sub_Type__c, spc_REMS_ID__c, LastModifiedDate from account]);
        PatientConnect__PC_Address__c hcoAddress = spc_Test_Setup.createTestAddress(hcoAcc);
        test.startTest();
        List<spc_SOCEWPController.HCORecord> hcoRecord = spc_SOCEWPController.searchRecords('Test HCO Account','Clinic');
        List<spc_SOCEWPController.HCORecord> hcoRecord1 = spc_SOCEWPController.searchRecords('HCO','');
        List<PatientConnect__PC_Address__c> socAddresses = spc_SOCEWPController.getSocAddresses(hcoAcc.Id);
        test.stoptest();
    }
    
    
 }
/********************************************************************************************************
*  @author          Deloitte
*  @description     Accessor class to handle DML and SOQL related request for User Object.
*  @param           
*  @date            June 19, 2020
*********************************************************************************************************/
public with sharing class CCL_UserAccessor {
    
    /********************************************************************************************************
*  @author          Deloitte
*  @description     Method to get the  current logged in user info.
*  @param           Id
*  @return          user
*  @date            June 1, 2020
*********************************************************************************************************/

    public static user getLoggedInUserInfo (Id userRecId) {
        User loggedInUser = new User();
        if (User.sObjectType.getDescribe().isAccessible()) {
            loggedInUser = [SELECT Id, ContactId, Contact.CCL_Has_User_Accepted_Privacy_Disclaimer__c 
                            FROM User 
                            WHERE Id = :userRecId];
        }
        return loggedInUser;
    }
}
public
with sharing class PSP_AccountController {
public
  PSP_AccountController() {}
  /* @AuraEnabled(Cacheable = true) public static List<Account> getAllPatients()
   { return [ SELECT Name, RecordTypeId, PSP_Patient_ID__c, CreatedDate,
              ShippingCity, ShippingState,
              HealthCloudGA__Active__c FROM Account where RecordType.Name
              =:Label.PSP_Patient_Record_Type ];
   }*/
  @AuraEnabled(
      Cacheable = true) public static List<AccountRecord> getAllPatients() {
    final List<AccountRecord> accounts = new List<AccountRecord>();
    try {
      for (Account account :
           [ SELECT Name, RecordType.Name, PSP_Patient_ID__c, CreatedDate,
             ShippingCity, ShippingState,
             HealthCloudGA__Active__c FROM Account where RecordType.Name
             =:Label.PSP_Patient_Record_Type WITH SECURITY_ENFORCED ]) {
        final AccountRecord accountWrapper = new AccountRecord(account);
        System.debug(accountWrapper);
        accounts.add(accountWrapper);
      }
      return accounts;
    } catch (AuraHandledException e) {
      System.debug(e.getMessage());
      throw e;
    }
  }

public
  class AccountRecord {
    /* getter class to get Id*/
    @AuraEnabled public Id acctId {
      get;
      set;
    }
    /* getter class to get Name*/
    @AuraEnabled public String name {
      get;
      set;
    }
    /* getter class to get nameurl in wrapper class*/
    @AuraEnabled public String nameURL {
      get;
      set;
    }
    /* getter class to get isactive in wrapper class*/
    @AuraEnabled public String recordType {
      get;
      set;
    }
    /* getter class to get isactive in wrapper class*/
    @AuraEnabled public String patientId {
      get;
      set;
    }
    /* getter class to get isactive in wrapper class*/
    @AuraEnabled public String createdDate {
      get;
      set;
    }
    /* getter class to get isactive in wrapper class*/
    @AuraEnabled public String shippigCity {
      get;
      set;
    }
    /* getter class to get isactive in wrapper class*/
    @AuraEnabled public String state {
      get;
      set;
    }
    /* getter class to get isactive in wrapper class*/
    @AuraEnabled public String active {
      get;
      set;
    }
  public
    AccountRecord(Account account) {
      acctId = account.Id;
      name = account.Name;
      nameURL = '/' + account.Id;
      recordType = account.RecordType.Name;
      patientId = account.PSP_Patient_ID__c;
      final String dateOutput =
          account.CreatedDate.format('MM/dd/yyyy HH:mm a');
      createdDate = dateOutput;
      shippigCity = account.ShippingCity;
      state = account.ShippingState;
      active = account.HealthCloudGA__Active__c;
    }
  }
}
/**
* @author Deloitte
* @date 07/18/2018
*
* @description This is the Test Class for PC_eLetterAuraController
*/

@isTest
public class spc_eLetterTest {
    @testSetup static void setup() {
        // create a record for the custom setting
        PatientConnect__PC_System_Settings__c cSetting = new PatientConnect__PC_System_Settings__c();
        cSetting.PatientConnect__PC_eLetter_Sender_Class__c = MOCK_SENDER;
        spc_Database.ins(cSetting);
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        spc_ApexConstantsSetting__c cs = spc_ApexConstantsSetting__c.getInstance('ELETTER_SENDER_CLASS');
        update cs;
    }
    public static final String MOCK_SENDER = 'spc_Test_MockELetterSender';

    @isTest
    static void getLetters() {
        // Create a Manufacturer account
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Email Template');
        insert eletter;
        // Create a Engagement Eletter Program
        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Email', program.ID);
        insert peletter;
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.Name = 'Physician Name';
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.PatientConnect__PC_Email__c = 'test@test.com';
        account.spc_Patient_Mrkt_and_Srvc_consent__c = System.Label.spc_No;
        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        insert programCase;
        PatientConnect__PC_Association__c association = createAssociation(account, programCase, 'Treating Physician', Date.today().addDays(1));
        List<spc_eLetter_Recipient> getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, programCase.Id, false);
        System.assertEquals(account.PatientConnect__PC_Email__c, getRecepients[0].recipientEmail);
        String recepients = '[{"bDisableEmail":false,"bDisableFax":false,"bDisableSendToMe":true,"bDisableSMS":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PatientConnect__PC_Email__c":"English","PatientConnect__PC_Email__c":"test@a.com","Type":"Specialist","Fax":"12345","Name":"Name."},"recipientType":"Specialist","recipientEmail":"aiii@biii.com","sendEmail":true,"sendFax":false,"sendToMe":false,"sendSMS":false}]';
        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);
        System.assertNotEquals(returnMessage, null);
    }

    private static PatientConnect__PC_Association__c createAssociation(Account account, Case caseObj, String roleName, Date endDate) {
        PatientConnect__PC_Association__c association = new PatientConnect__PC_Association__c (PatientConnect__PC_Account__c = account.Id, PatientConnect__PC_Program__c = caseObj.Id, PatientConnect__PC_Role__c = roleName, PatientConnect__PC_EndDate__c = endDate);
        return (PatientConnect__PC_Association__c)spc_Database.ins(association);
    }

    @isTest
    static void testSendToMe() {
        // Create a Manufacturer account
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Fax Template');
        insert eletter;
        // Create a Engagement Eletter Program
        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Fax', program.ID);
        insert peletter;
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.Name = 'Physician Name';
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.Fax = '12345';
        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        insert programCase;
        PatientConnect__PC_Association__c association = createAssociation(account, programCase, 'Treating Physician', Date.today().addDays(1));
        List<spc_eLetter_Recipient> getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, programCase.Id, false);
        System.assertEquals(account.PatientConnect__PC_Email__c, getRecepients[0].recipientEmail);
        String recepients = '[{"bDisableEmail":true,"bDisableFax":true,"bDisableSendToMe":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PatientConnect__PC_Communication_Language__c":"English","Type":"Specialist","PatientConnect__PC_Email__c":"test@a.com","Name":"Name."},"recipientEmail":"aiii@biii.com","recipientType":"Specialist","sendEmail":false,"sendFax":false,"sendToMe":true}]';
        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);
        System.assertNotEquals(returnMessage, null);
    }


    @isTest
    static void testSendFax() {
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Fax Template');
        insert eletter;
        // Create a Engagement Eletter Program
        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Fax', program.ID);
        insert peletter;
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.Name = 'Physician Name';
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.PatientConnect__PC_Email__c = 'test@test.com';
        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        insert programCase;
        spc_eLetterAuraController.getLetters(programCase.Id, 'actioncenter', false);
        spc_eLetterAuraController.getLetters(programCase.Id, System.Label.spc_inquiry, false);
        PatientConnect__PC_Association__c association = createAssociation(account, programCase, 'Treating Physician', Date.today().addDays(1));
        List<spc_eLetter_Recipient> getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, programCase.Id, false);
        System.assertEquals(account.PatientConnect__PC_Email__c, getRecepients[0].recipientEmail);
        String recepients = '[{"bDisableEmail":false,"bDisableFax":false,"bDisableSendToMe":false,"bDisableSMS":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PC_Communication_Language__c":"English","Type":"Specialist","PC_Email__c":"test@a.com","Name":"Name."},"recipientType":"Specialist","sendEmail":true,"sendFax":false,"sendToMe":true,"sendSMS":false}]';
        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);
        System.assertNotEquals(returnMessage, null);
    }

    @isTest
    static void testNoRecepients() {
        // Create a Manufacturer account
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Email Template');
        eletter.PatientConnect__PC_Allow_Send_to_Patient__c = true;
        insert eletter;
        // Create a Engagement Eletter Program
        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Email', program.ID);
        insert peletter;
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.Name = 'Physician Name';
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.PatientConnect__PC_Email__c = 'test@test.com';
        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        spc_Database.ins(programCase);
        String recepients = '[{"bDisableEmail":false,"bDisableFax":false,"bDisableSendToMe":false,"bDisableSMS":false,"communicationLanguage":"English","recipient":{},"recipientType":"Specialist","sendEmail":false,"sendFax":true,"sendToMe":false,"sendSMS":false}]';
        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);
        System.assertNotEquals(returnMessage, null);
    }

    @isTest
    static void enquiryEletter() {
        Id accountRTypeId = spc_System.recordTypeId(spc_Test_Setup.ACCOUNT_OBJ, 'PC_Patient');
        Account manAcc = spc_Test_Setup.createAccount(accountRTypeId);
        manAcc.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        manAcc.spc_Patient_Marketing_Consent_Date__c = Date.valueOf('2014-06-07');
        spc_Database.ins(manAcc);
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Inquiry', 'Email Template');
        insert eletter;
        Case inquiryCase  = new Case();
        inquiryCase.AccountId = manAcc.Id;
        inquiryCase.RecordTypeId = spc_System.recordTypeId('Case', 'spc_Inquiry');
        inquiryCase.Status = 'Open';
        insert inquiryCase;
        spc_eLetterAuraController.getLetters(inquiryCase.Id, 'inquiry', false);
        List<spc_eLetter_Recipient> getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, inquiryCase.Id, false);
        String recepients = '[{"bDisableEmail":false,"bDisableFax":false,"bDisableSendToMe":false,"bDisableSMS":false,"communicationLanguage":"English","recipient":{},"recipientType":"Patient","sendEmail":true,"sendFax":true,"sendToMe":false,"sendSMS":false}]';
        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, inquiryCase.Id);
        System.assertNotEquals(returnMessage, null);
    }

    @isTest
    static void testSendSMS() {
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        insert program;
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Fax Template');
        eletter.PatientConnect__PC_Send_to_User__c = true;
        insert eletter;
        // Create a Engagement Eletter Program
        PatientConnect__PC_Engagement_Program_Eletter__c peletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Fax', program.ID);
        insert peletter;
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.Name = 'Physician Name';
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.PatientConnect__PC_Email__c = 'test@test.com';
        account.Phone = '1234567898';
        insert account;
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        insert programCase;
        spc_eLetterAuraController.getLetters(programCase.Id, 'actioncenter', false);
        PatientConnect__PC_Association__c association = createAssociation(account, programCase, 'Treating Physician', Date.today().addDays(1));
        List<spc_eLetter_Recipient> getRecepients =  spc_eLetterAuraController.getRecepients(eletter.Id, programCase.Id, false);
        System.assertEquals(account.PatientConnect__PC_Email__c, getRecepients[0].recipientEmail);
        String recepients = '[{"bDisableEmail":false,"bDisableFax":false,"bDisableSendToMe":true,"bDisableSMS":false,"communicationLanguage":"English","recipient":{"Id":"' + account.Id + '","PatientConnect__PC_Email__c":"English","PatientConnect__PC_Email__c":"test@a.com","Type":"Specialist","Fax":"12345","Name":"Name."},"recipientType":"Specialist","sendEmail":true,"sendFax":false,"sendToMe":false,"sendSMS":false}]';
        String returnMessage = spc_eLetterAuraController.sendOutboundDocuments(recepients, eletter.Id, programCase.Id);
        System.assertNotEquals(returnMessage, null);
    }
}
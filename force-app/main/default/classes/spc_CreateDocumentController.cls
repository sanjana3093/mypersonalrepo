/**
* @author Deloitte
* @date 31-May-2018
*
* @description This class is the controller of spc_CreateManualDocument lightning component
*/
global with sharing class spc_CreateDocumentController {
    /*******************************************************************************************************
    * @description This method returns document categories
    * @return List<String> of the Categories
    */
    @AuraEnabled
    public static List<String> getDocumentTypes() {
        List<String> results = new List<String>();
        results.add(spc_ApexConstants.NONE_PICKLIST_VALUE);
        List<Schema.PicklistEntry> picklistEntries;
        for (Schema.PicklistEntry pe : PatientConnect__PC_Document__c.PatientConnect__PC_Document_Category__c.getDescribe().getPicklistValues()) {
            results.add(pe.getValue());
        }
        return results;
    }
    /*******************************************************************************************************
    * @description This method returns all the Engagement Programs
    * @return List<String> Engagement Program names
    */
    @AuraEnabled
    public static List<String> getEngagementPrograms() {
        List<String> results = new List<String>();
        results.add('');
        List<PatientConnect__PC_Engagement_Program__c> lstEngProg = [Select name from PatientConnect__PC_Engagement_Program__c where PatientConnect__PC_Active__c = true ];
        for (PatientConnect__PC_Engagement_Program__c pe : lstEngProg ) {
            results.add(pe.Name);
        }
        return results;
    }


    /*******************************************************************************************************
    * @description This method returns all the Engagement Program from Program Case
    * @param programId Program Case Id
    * @return String Engagement Program name
    */
    @AuraEnabled
    public static string getEPFromProgram(Id programId) {
        Case oCase = [select id, PatientConnect__PC_Engagement_Program__c, PatientConnect__PC_Engagement_Program__r.name from Case where id = : programId LIMIT 1];
        return oCase.PatientConnect__PC_Engagement_Program__r.name;
    }

    /*******************************************************************************************************
    * @description This method fetches the name of the sObject which is Parent ID
    * @param parentId SObject Id
    * @return sObject Name
    */

    @AuraEnabled
    public static string getSOBjectName(Id parentId) {
        Schema.SObjectType sobjectType = parentId.getSObjectType();
        String sobjectName = sobjectType.getDescribe().getName();
        return sobjectName;
    }

    /*******************************************************************************************************
    * @description This method is used to create Document with the Data entered
    * @param parentId SObject Id
    * @param fileName String for the file Name
    * @param base64Data String
    * @param contentType String for the content type
    * @param category String
    * @param notes String
    * @param Engprog Engagement Program ID
    * @param dateSent String
    * @param sentBy Id of the User
    * @param fileId Id of the File
    * @return RemoteResponse
    */
    @AuraEnabled
    public static RemoteResponse createDocument(Id parentId, string fileName, String base64Data, String contentType, String category, String notes, String Engprog, String dateSent, String sentBy, String fileId) {
        RemoteResponse response = new RemoteResponse();
        Id docId;
        String sobjectName;
        response.isSuccess = false;
        if (dateSent != null) {
            String[] strDate = dateSent.split('-');
            String[] myDate = strDate[2].split(':');
            String[] dateVal = myDate[0].split('T');
            String[] sec = mydate[2].split('\\.');
            Integer myIntMonth = integer.valueOf(strDate[1]);
            Integer myIntYear = integer.valueOf(strDate[0]);
            DateTime datesent2 = DateTime.newInstanceGMT(myIntYear, myIntMonth, integer.valueof(dateVal[0]), integer.valueof(dateVal[1]), integer.valueof(myDate[1]), integer.valueof(sec[0]));
            PatientConnect__PC_Engagement_Program__c engProgFinal = [select id from PatientConnect__PC_Engagement_Program__c where name = :Engprog LIMIT 1];
            if (fileId == '') {
                if (parentId != NULL) {
                    Schema.SObjectType sobjectType = parentId.getSObjectType();
                    sobjectName = sobjectType.getDescribe().getName();
                }
                if (sobjectName == 'Case') {
                    PatientConnect__PC_Document__c objDoc = new PatientConnect__PC_Document__c();
                    objDoc.RecordTypeId = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_MANUAL_UPLOAD)).getRecordTypeId();
                    objDoc.PatientConnect__PC_Document_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_STATUS_REVIEW_NEEDED);
                    objDoc.PatientConnect__PC_Document_Category__c = category;
                    objDoc.spc_Sent_By__c = sentBy;
                    objDoc.PatientConnect__PC_Fax_Sent_Date_Time__c = datesent2;
                    objDoc.PatientConnect__PC_Engagement_Program__c = engProgFinal.id;
                    objDoc.PatientConnect__PC_Description__c = notes;
                    spc_Database.ins(objDoc);
                    if (objDoc.Id != null) {
                        fileId = saveAttachment(objDoc.Id, fileName, base64Data, contentType);
                        PatientConnect__PC_Document_Log__c objDocLog = new PatientConnect__PC_Document_Log__c();
                        objDocLog.PatientConnect__PC_Document__c = objDoc.Id;
                        objDocLog.PatientConnect__PC_Program__c = parentId;
                        spc_Database.ins(objDocLog);
                    }
                    response.isSuccess = true;
                    response.documentId = objDoc.Id;
                    List<PatientConnect__PC_Document__c> documentNames = [SELECT ID, Name FROM PatientConnect__PC_Document__c WHERE id = :objDoc.Id limit 1];
                    response.documentName = documentNames[0].name;
                } else {
                    PatientConnect__PC_Document__c objDoc = new PatientConnect__PC_Document__c();
                    objDoc.RecordTypeId = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_MANUAL_UPLOAD)).getRecordTypeId();
                    objDoc.PatientConnect__PC_Document_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_STATUS_REVIEW_NEEDED);
                    objDoc.PatientConnect__PC_Document_Category__c = category;
                    objDoc.spc_Sent_By__c = sentBy;
                    objDoc.PatientConnect__PC_Fax_Sent_Date_Time__c = datesent2;
                    objDoc.PatientConnect__PC_Engagement_Program__c = engProgFinal.id;
                    objDoc.PatientConnect__PC_Description__c = fileName + ' ' + notes;
                    spc_Database.ins(objDoc);
                    if (objDoc.Id != null) {
                        fileId = saveAttachment(objDoc.Id, fileName, base64Data, contentType);
                    }
                    response.isSuccess = true;
                    response.documentId = objDoc.Id;
                    List<PatientConnect__PC_Document__c> documentNames = [SELECT ID, Name FROM PatientConnect__PC_Document__c WHERE id = :objDoc.Id limit 1];
                    response.documentName = documentNames[0].name;
                }
            }
            appendToFile(fileId, base64Data);
            response.isSuccess = true;
            response.fileId = Id.valueOf(fileId);
        }
        return  response;
    }

    /*******************************************************************************************************
    * @description To fetch the Attachment based on File ID and update the attachment with the base64Data
    * @param fileId the ID of the Attachment
    * @param base64Data String
    */
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Attachment a = [
                           SELECT Id, Body
                           FROM Attachment
                           WHERE Id = :fileId
                       ];
        String existingBody = EncodingUtil.base64Encode(a.Body);
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data);
        update a;
    }

    /*******************************************************************************************************
    * @description Method used to save a file
    * @param Id parentId
    * @param String FileName
    * @param String base64Data
    * @param String contentType
    * @return Id of Attachment
    */
    public static Id saveAttachment(Id parentId, String fileName, String base64Data, String contentType) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Attachment a = new Attachment();
        a.parentId = parentId;
        a.Body = EncodingUtil.base64Decode(base64Data);
        a.Name = fileName;
        a.ContentType = contentType;
        insert a;

        return a.Id;
    }

    /*******************************************************************************************************
    * @description Wrapper class used for the Response
    *
    */
    public class RemoteResponse {
        @AuraEnabled public Id documentId { get; set; }
        @AuraEnabled public Id fileId { get; set; }
        @AuraEnabled public boolean isSuccess { get; set; }
        @AuraEnabled public String  documentName { get; set; }
        @AuraEnabled public String errorMsg {get; set;}
    }
}
/********************************************************************************************************
*  @author          Deloitte
*  @description     Utility class  which holds all the static variables which control the execution of various triggers
*  @param           
*  @date            July 10, 2020
*********************************************************************************************************/


public class CCL_TriggerExecutionUtility{

//Trigger Execution Variable Declaration for User Therapy Association Object
private static Boolean processUserTherapyAssociationTrigger = true;
//Trigger Execution Variable Declaration for Language Therapy Association Object
private static Boolean processLanguageTherapyAssociationTrigger = true;
//Trigger Execution  Variable for Therapy Association object
private static Boolean processTherapyAssociationTrigger=true;
    //Trigger Execution  Variable for ADF  object
private static Boolean processprocessADFTrigger=true;
     //Trigger Execution  Variable for Shipment  object
private static Boolean processShipmentTrigger=true;

//Trigger Execution Variable Declaration for Order Object
private static Boolean processOrderTrigger=true;

//Trigger Execution Variable Declaration for Account Object
private static Boolean processAccountTrigger=true;

//Trigger Execution Variable for Batch object
private static Boolean ProcessBatchTrigger=true;

//Trigger Execution Variable for Finished Product object
private static Boolean ProcessFinishedProductTrigger=true;

//Trigger Execution Variable for Summary object
private static Boolean ProcessSummaryTrigger=true;

//getter for ProcessUserTherapyAssociationTrigger
public static Boolean getProcessUserTherapyAssociationTrigger(){
        return processUserTherapyAssociationTrigger;
    }
    
//getter for ProcessLanguageTherapyAssociationTrigger
public static Boolean getProcessLanguageTherapyAssociationTrigger(){
        return processLanguageTherapyAssociationTrigger;
    }
    
//getter for processTherapyAssociationTrigger
public static Boolean getProcessTherapyAssociationTrigger(){
         return processTherapyAssociationTrigger;
    }
	 //getter for processShipmentTrigger
public static Boolean getprocessShipmentTrigger(){
        return processShipmentTrigger;
    }
    //getter for processADFTrigger
public static Boolean getProcessprocessADFTrigger(){
         return processprocessADFTrigger;
    }
    
     //getter for processBatchTrigger
     public static Boolean getProcessBatchTrigger(){
          return ProcessBatchTrigger;
    }
    
     //getter for ProcessFinishedProductTrigger
     public static Boolean getProcessFinishedProductTrigger(){
          return ProcessFinishedProductTrigger;
    }
    
    //getter for ProcessSummaryTrigger
     public static Boolean getProcessSummaryTrigger(){
          return ProcessSummaryTrigger;
    }
	
	//getter for ProcessOrderTrigger
    public static Boolean getProcessOrderTrigger(){
        return processOrderTrigger;
    }
	
	//getter for ProcessAccountTrigger
    public static Boolean getProcessAccountTrigger(){
        return processAccountTrigger;
    }

//setter for processUserTherapyAssociationTrigger
public static void setprocessUserTherapyAssociationTrigger(Boolean val){
         processUserTherapyAssociationTrigger = val;
    }
//setter for processLanguageTherapyAssociationTrigger
public static void setprocessLanguageTherapyAssociationTrigger(Boolean val){
         processLanguageTherapyAssociationTrigger = val;
    }
    //setter for processADFTrigger
public static void setProcessADFTrigger(Boolean val){
         processprocessADFTrigger = val;
    }
    
//setter for processTherapyAssociationTrigger
public static void setProcessTherapyAssociationTrigger(Boolean val){
         processTherapyAssociationTrigger = val;
    }
	  //setter for processShipmentTrigger
public static void setprocessShipmentTrigger(Boolean val){
         processShipmentTrigger = val;
    }

//setter for processBatchTrigger
    public static Boolean setProcessBatchTrigger(Boolean val){
         return ProcessBatchTrigger=val;
    }
    
     //setter for ProcessFinishedProductTrigger
    public static Boolean setProcessFinishedProductTrigger(Boolean val){
         return ProcessFinishedProductTrigger=val;
    }
    
    //setter for ProcessSummaryTrigger
    public static Boolean setProcessSummaryTrigger(Boolean val){
         return ProcessSummaryTrigger=val;
    }
	
	//setter for processOrderTrigger
    public static void setProcessOrderTrigger(Boolean val){
        processOrderTrigger = val;
    }
	
	//setter for processAccountTrigger
    public static void setProcessAccountTrigger(Boolean val){
        processAccountTrigger = val;
    }
}
/*********************************************************************************************************
 * @author Deloitte
 * @date   January 3, 2019
 * @description  spc_FAXCommunication for sending out faxes

*/

//Do not commit this class
public class spc_FAXCommunication  implements spc_CommunicationMgr.ICommunication {
    string OutboundFAXTemplate = Label.spc_FaxTemplateParserString;
    spc_CommunicationMgr.RecipientHandler rHandler = new spc_CommunicationMgr.RecipientHandler();
    static spc_Communication_framework_Parameters__c emailParam;

    static {
        emailParam = spc_Communication_framework_Parameters__c.getOrgDefaults();
        if (emailParam == null) {
            emailParam = new spc_Communication_framework_Parameters__c();
        }

    }
    /*****************************************************************************
    * @author       Deloitte
    * @description    Send all fax communication and creates document
    * @return     Void
    */
    public void send(List<spc_CommunicationMgr.Envelop> envelops
                     , Map<String, PatientConnect__PC_Engagement_Program_Eletter__c> mapEmailTemplates
                     , Map<String, Map<String, Account>> mapRecipients, Map<Id, Contact> mapAccountContacts,Map<Id,List<sObject>> additionalObjects) {
        for (spc_CommunicationMgr.Envelop env : envelops) {

            PatientConnect__PC_Engagement_Program_Eletter__c eLetter = mapEmailTemplates.get(env.flowName + '|' + String.valueOf(env.channel));
            if (eLetter != null) {
                if (!String.isBlank(eLetter.PatientConnect__PC_eLetter__r.PatientConnect__PC_Template_Name__c)) {
                    env.outboundDoc = new PatientConnect__PC_Document__c(id = env.outboundDoc.Id);
                    env.outboundDoc.spc_PMRC_Code_New__c = spc_Utility.getActualPMRCCode(eLetter.spc_PMRC_Code__c);
                    env.outboundDoc.PatientConnect__PC_Description__c = eLetter.PatientConnect__PC_eLetter__r.Name;
                    string outboundParam = OutboundFAXTemplate.replaceAll('CASERECORDID', env.programCaseId);
                    outboundParam = outboundParam.replaceAll('DOCUMENTID', env.outboundDoc.Id);
                    outboundParam = outboundParam.replaceAll('ELETTERTEMPALTENAME', eLetter.PatientConnect__PC_eLetter__r.PatientConnect__PC_Template_Name__c);
					Set<String> attachmentIds = new Set<String>();
                    String attachmentString = '';
					List<Attachment> attachmentList=new List<Attachment>();
                    if(additionalObjects.get(eLetter.PatientConnect__PC_eLetter__c)!=null
                        && !additionalObjects.get(eLetter.PatientConnect__PC_eLetter__c).isEmpty()
                               && additionalObjects.get(eLetter.PatientConnect__PC_eLetter__c).getSobjectType()==Schema.Attachment.sObjectType){
                                
                                 attachmentList.addAll((List<Attachment>)additionalObjects.get(eLetter.PatientConnect__PC_eLetter__c));
                       }
                   
					//Functionality for Bundle FAX
                    if(env.attachmentIds!=null && !env.attachmentIds.isEmpty()){
                        attachmentIds.addAll(env.attachmentIds);
                    }
                   if(attachmentList!=null && ! attachmentList.isEmpty()){
                    for(Attachment attachment : attachmentList){
						attachmentIds.add(attachment.Id);
                    }
                   }
                    
					if(attachmentIds!=null && !attachmentIds.isEmpty() ){
                        Integer attachmentIndex = 0;
                        for (String attachmentId : env.attachmentIds) {
                            string attachStr  = 'attachId' + (attachmentIndex > 0 ? String.valueOf(attachmentIndex) : '');
                            attachmentString += '<' + attachStr + '>' + attachmentId + '</' + attachStr + '>';
                            attachmentIndex++;
                        }
                    }
                    outboundParam = outboundParam.replaceAll('ATTACHID', attachmentString);

                    Map<String, Account> mapCaseAccounts = mapRecipients.get(env.programCaseId);
                    List<String> toAddresses = new List<String>();
                    List<String> sentToAddress = new List<String>();
                    for (String recipientType : env.recipientTypes) {
                        if (recipientType == 'Lash' &&  String.isNotBlank(emailParam.Lash_Fax_Number__c)) {
                            sentToAddress.add(emailParam.Lash_Fax_Number__c);
                            toAddresses.add(rHandler.getFAXNumber(emailParam.Lash_Fax_Number__c));
                        } else if (mapCaseAccounts.containsKey(recipientType)) {
                            string faxNumber = mapCaseAccounts.get(recipientType).Fax;
                            if (!String.isBlank(faxNumber)) {
                                toAddresses.add(rHandler.getFAXNumber(faxNumber));
                                sentToAddress.add(faxNumber);
                            }

                        } else if (env.additionalAccountMap != null && env.additionalAccountMap.containsKey(recipientType)) {
                            String faxNumber = '';
                            if(env.additionalAccountMap.get(recipientType).recordtype.developername == spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.ACCOUNT_RT_HCO)){
                                 faxNumber = mapCaseAccounts.get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_HCO)).Fax;
                            } else{
                                 faxNumber = env.additionalAccountMap.get(recipientType).Fax;
                            }
                            if (!String.isBlank(faxNumber)) {
                                toAddresses.add(rHandler.getFAXNumber(faxNumber));
                                sentToAddress.add(faxNumber);
                            }
                        }
                    }
                    outboundParam = outboundParam.replaceAll('FAXNUMBER', string.join(toAddresses, ','));
                    env.outboundDoc.PatientConnect__PC_To_Fax_Number__c = string.join(sentToAddress, ',');
                    if (emailParam != null && emailParam.Org_Wide_Email_Address_Name__c != null) {
                        env.outboundDoc.PatientConnect__PC_From_Name__c = emailParam.Org_Wide_Email_Address_Name__c;
                    }
                    if (String.isBlank(env.outboundDoc.PatientConnect__PC_To_Fax_Number__c)) {
                        env.outboundDoc.spc_Error_Log__c = label.spc_NoRecipentErrorForFax;
                        env.outboundDoc.PatientConnect__PC_Document_Status__c = spc_ApexConstants.DOC_STATUS_FAILED;
                    } else {
                        env.outboundDoc.spc_OutboundFAX_Template__c = outboundParam;
                    }
                }
            }
        }
    }
}
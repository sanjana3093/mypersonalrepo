/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the test class for CaseTriggerHandler
*  @date            09/18/2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                        Description
--------------------------------------------------------------------------------------------------------------            
Sanjana Tripathy            September 18, 2019          Initial Version
****************************************************************************************************************/
@IsTest
public class PSP_AccountTriggerTest {
    /*Constant for test */
    static final String TEST = 'Test';
    /* Static list for initializing apex constants */
    static final List<PSP_ApexConstantsSetting__c>  apexConstants =  PSP_Test_Setup.setDataforApexConstants();
    /*************************************************************************************
* @author      : Deloitte
* @date        : 09/17/2019
* @Description : Testing insert scenarios
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    
    public static testMethod void testRunAs () {
        
        List<Account> accountLst=new List<Account>();
        Account accnt=PSP_Test_Setup.createTestBusinessAccount('testaccount',PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACCOUNT_TYPE_HOSPITAL));
        Database.insert (accnt);  
        accnt.PSP_Account_Type__c=PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACCOUNT_TYPE_HOSPITAL);
        Database.update (accnt); 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='es_MX', 
                          LocaleSidKey='es_MX', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123@testorg.com');
        Account accnt2=PSP_Test_Setup.createTestBusinessAccount('testing',PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACCOUNT_TYPE_PRO_MNFCR));
        Account accnt4=PSP_Test_Setup.createTestBusinessAccount('testing2345',PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACCOUNT_TYPE_CLINIC));
        Account accnt3=PSP_Test_Setup.createTestBusinessAccount('testing234',PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACCOUNT_TYPE_HOSPITAL));
        Account accnt5=PSP_Test_Setup.createTestBusinessAccount('testing23456',PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_PHRM));
        Account accnt6=PSP_Test_Setup.createTestBusinessAccount('phyoffc',PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_PHYSOFCE));
        Account accnt7=PSP_Test_Setup.createTestBusinessAccount('testing234578',PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_DSTRIBTR));
        Account accnt8=PSP_Test_Setup.createTestBusinessAccount('testing2345789',PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_TESTLAB));
        Account accnt9=PSP_Test_Setup.createTestBusinessAccount('testingzz45789',PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_OTHR));
        Account accnt10=PSP_Test_Setup.createTestBusinessAccount('testingzzz5789',PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_HOUSHLD));
        Account accnt11=PSP_Test_Setup.createTestBusinessAccount('testinzzzz5789',PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACCOUNT_TYPE_CLINIC));
        accountLst.add(accnt);
        accountLst.add(accnt2);
        accountLst.add(accnt3);
        accountLst.add(accnt4);
        accountLst.add(accnt5);
        accountLst.add(accnt6);
        accountLst.add(accnt7);
        accountLst.add(accnt8);
        accountLst.add(accnt9);
        accountLst.add(accnt10);
        accountLst.add(accnt11);
        //Database.insert(accnt5);
        try {
          
            System.runAs(u) {
                Database.insert (accountLst); 
                
            }
             accnt4.PSP_Account_Type__c=PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_PHRM);
            Database.update(accnt4);
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains ('Insert failed.'),e.getMessage () );
        }
        
        
    }
    
}
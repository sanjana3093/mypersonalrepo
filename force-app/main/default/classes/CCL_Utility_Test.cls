/********************************************************************************************************
*  @author          Deloitte
*  @description     This is a test class created for CCL_Utility. 
                    To Test the common methods in the CCL_Utility class
*  @param           
*  @date            June 19, 2020
*********************************************************************************************************/    
@isTest
private with sharing class CCL_Utility_Test {

    @TestSetUp
    private static void utilityDataSetup(){
        CCL_GeneralSettings__c generalSettingsObj = CCL_Test_SetUp.createTestSettings();
    }


/********************************************************************************************************
*  @author          Deloitte
*  @description     This is to test getFormattedDate method. 
*  @param           
*  @date            June 19, 2020
*********************************************************************************************************/
    private static testMethod void getFormattedDateTest(){
        test.startTest();
            //Testing with inputing correct date
            Date inputDate = Date.newInstance(2020,1,12);
            String formattedDateStr = CCL_Utility.getFormattedDate(inputDate);
            System.assertEquals('12 Jan 2020',formattedDateStr);

            //Testing with a blank.
            //Date invalidInputDate = null;
            //String invalidFormattedDateStr = CCL_Utility.getFormattedDate(invalidInputDate);
           // System.assertNotEquals('12-Jan-2020',invalidFormattedDateStr);
        test.stopTest();
    }
    

/********************************************************************************************************
*  @author          Deloitte
*  @description     This is to create sample Therapy and Site Records for the Therapy Association Public Group Creation. 
*  @param           
*  @date            June 25, 2020
*********************************************************************************************************/
    
    
    public static CCL_Therapy__c testTherapyRecord(){
        
            CCL_Therapy__c thera = new CCL_Therapy__c();
                thera.Name  = 'Test Therapy Id';
                thera.CCL_Therapy_Description__c  = 'Test Therapy Name';
                thera.CCL_Type__c = 'Clinical';
                thera.CCL_Status__c = 'Active';
            
                return thera;      
    }
    
    public static Account testSiteRecord(){
        
            String siteAccountRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CCL3_Site_Account').getRecordTypeId();
        
            Account site = new Account();
                site.RecordTypeId = siteAccountRT;
                site.Name  = 'Test Site Name';
                site.CCL_Active__c = True;
                site.CCL_Label_Compliant__c   = 'SEC';
                site.CCL_Type__c = 'Customer';
                site.CCL_External_ID__c = 'Test ID';
                site.CCL_Capability__c = 'Ordering Site';
                site.ShippingCountry = 'Ireland';
                site.ShippingCountryCode = 'IRE';
                  
            return site;      
    }
    
/********************************************************************************************************
*  @author          Deloitte
*  @description     Method to test site time zones 
*  @param           
*  @date            Aug 20, 2020
*********************************************************************************************************/  
        public static testMethod void testgetSiteTimeZone() {
        final String ACCOUNT_NAME = 'Test Account';
        Test.startTest();
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestApheresisDataForm(order);
        CCL_Utility.getSiteTimeZone(ACCOUNT_NAME,adf.id);
        final String testDateTime = String.valueOfGmt(datetime.newInstance(2014, 9, 15, 12, 30, 0));
        final DateTime timezonevalue = CCL_Utility.getDateTimeValue(testDateTime,'test1');
        System.assertEquals(datetime.newInstance(2014, 9, 15, 12, 30, 0),timezonevalue,'Time Zone Check');
        final DateTime inputDateTime = system.today();
        final Time gettime = inputDateTime.Time();
        CCL_Utility.getdisplayDateTimeValues(inputDateTime,'test');
        CCL_Utility.getFormattedTime(gettime);
        Test.stopTest();
         
     }
    /********************************************************************************************************
*  @author          Deloitte
*  @description     Method to test site Therapy Associations 
*  @param           
*  @date            Nov 17, 2020
*********************************************************************************************************/  
    public static testMethod void testFetchSiteTherapyAssociations() {
        
        final Account site = new Account(
            Name  = 'Test Site Name',
            CCL_Active__c = True,
            CCL_Label_Compliant__c   = 'SEC',
            CCL_Type__c = 'Customer',
            CCL_External_ID__c = 'Test ID',
            CCL_Capability__c = 'Ordering Site',
            ShippingCountryCode = 'US'
        );
        insert site; 

        final CCL_Therapy__c therapy = new CCL_Therapy__c(
            Name  = 'Test Therapy Id',
            CCL_Therapy_Description__c  = 'Test Therapy Name',
            CCL_Type__c = 'Commercial',
            CCL_Clinical_Patient_Eligibility_Text__c = 'Test',
            CCL_Status__c = 'Active'
        );
        insert therapy;
        
        final CCL_Therapy_Association__c siteTherapyAssociation = CCL_TestDataFactory.createTestSiteTherapyAssoc(site.Id, therapy.Id);
        insert siteTherapyAssociation;
        
        final List<Id> accountIds = new List<Id>{site.Id};
        final List<Id> therapyIds = new List<Id>{therapy.Id};

        Test.startTest();
        final List<CCL_Therapy_Association__c> siteTherapiesFetched = CCL_Utility.fetchSiteTherapyAssociations(therapyIds, accountIds);
        Test.stopTest();

        System.assertEquals(1, siteTherapiesFetched.size(), 'The returned list was not of expected length.');
    }
	 /********************************************************************************************************
*  @author          Deloitte
*  @description     Method to test site Therapy Associations 
*  @param           
*  @date            Nov 17, 2020
*********************************************************************************************************/  
    public static testMethod void testfetchAddress() {
        final CCL_Time_Zone__c timezone =CCL_Test_SetUp.createTestTimezone('test'); 
        Account myAccount=CCL_Test_SetUp.createTestAccount('test',timezone.Id);
        CCL_Address__c add=CCL_TestDataFactory.createTestAddress(myAccount.Id,'en_US');
        User testUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE);
        testUser.languagelocalekey='fr';
        Test.startTest(); 
        insert testUser;
        List<Id> accId=new list<Id>();
        accId.add(myAccount.Id);
        insert add;
        System.runAs(testUser) {
            final list< CCL_Address__c> addressList=CCL_Utility.fetchAddress(accId);
            
            final list< CCL_Address__c> actualaddressList= [select CCL_Complete_Address__c,CCL_Site__r.Name,CCL_Site__c,CCL_Site_Name__c from CCL_Address__c where CCL_Site__c in: accId and  CCL_Language__c='en_US'  ];
            test.stopTest();
            System.assertEquals(addressList.size(),actualaddressList.size(),'The size');
        }
    }
	/********************************************************************************************************
*  @author          Deloitte
*  @description     Method to test Content Version Updates 
*  @param           
*  @date            Nov 17, 2020
*********************************************************************************************************/  
    public static testMethod void updateContentVersionTest() {
        
        CCL_Test_SetUp.createTestOrder();
        
        final String nullDocId;
        
        final String strDocId = CCL_Utility.createOrderDocument(nullDocId);
        final String strUpdatedDocId = CCL_Utility.createOrderDocument(strDocId);
        
        final Id orderId = Id.valueOf(strUpdatedDocId);
         final List<CCL_Document__c > docList = CCL_ADFController_Utility.getDocId(orderId);
        final id docId;
        for (CCL_Document__c doc : doclist){
            docId = doc.Id; 
        }
        final List<Id> contentDocIds = new List<Id>{docId};
        
        Test.startTest(); 
        final Boolean isSuccess = CCL_Utility.updateContentVersion(contentDocIds);
        final List<ContentDocumentLink> finalList = CCL_Utility.getConsentFiles(docId);
        Test.stopTest();

        System.assert(isSuccess, 'Records were not inserted successfully');
    }
}
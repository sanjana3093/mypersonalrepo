/********************************************************************************************************
*  @author          Deloitte
*  @description     This method is being used to retrieve the ADF steps.
It retrieves the ADF Steps from the Custom metadata Object.
*  @param           
*  @date            June 14, 2020
*********************************************************************************************************/  
public with sharing class CCL_ADF_Controller {  
    /********************************************************************************************************
*  @author          Deloitte
*  @description    to fetch ADF wizard data
*  @param           
*  @date            june 14, 2020
*********************************************************************************************************/
    @AuraEnabled(cacheable=true)
    public static List<CCL_ADF_Progress_Steps__mdt> fetchADFSteps() {
        List<CCL_ADF_Progress_Steps__mdt> myADFSteps;
        try {
            if (CCL_ADF_Progress_Steps__mdt.sObjectType.getDescribe().isAccessible()) {
                myADFSteps=[select Masterlabel,CCL_Order_of_Step__c from CCL_ADF_Progress_Steps__mdt order by CCL_Order_of_Step__c limit 10];
            }
            return myADFSteps;
        } catch(QueryException ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        
    }
      
    /********************************************************************************************************
*  @author          Deloitte
*  @description    to fetch details of account
*  @param           
*  @date            june 14, 2020
*********************************************************************************************************/
    @AuraEnabled 
    public static List<CCL_Apheresis_Data_Form__c> getADFHeadeInfo(Id recordId) {
        try { 
            List<CCL_Apheresis_Data_Form__c> adfList=new List<CCL_Apheresis_Data_Form__c>();
            if (CCL_Apheresis_Data_Form__c.sObjectType.getDescribe().isAccessible()) {
                adfList=[select id,name,CCL_Patient_Name__c,CCL_Date_Of_Birth__c,CCL_Date_Of_Birth_Text__c,CCL_Treatment_Protocol_SubID_HospitalID__c,CCL_Therapy_Formula__c,CCL_Novartis_Batch_Id__c,CCL_Patient_Weight__c,toLabel(CCL_Status__c),CCL_Order__c,CCL_Therapy__r.CCl_Type__c,CCL_Hospital_Pat_Id_Opt_In__c,CCL_Order__r.CCL_Patient_Initials__c,CCL_Order__r.CCL_Initials_COI__c,CCL_Order__r.CCL_Month_of_Birth_COI__c,CCL_Order__r.CCL_Day_of_Birth_COI__c,CCL_Order__r.CCL_Year_of_Birth_COI__c,CCL_Order__r.CCL_Date_of_DOB__c,CCL_Order__r.CCL_Month_of_DOB__c,CCL_Order__r.CCL_Year_of_DOB__c,CCL_Order__r.CCL_Patient_Id__c,CCL_Patient_Id__c,CCL_Patient_Initials__c from CCL_Apheresis_Data_Form__c where id=:recordId WITH SECURITY_ENFORCED ];
            }
            return adfList;
        } catch(AuraHandledException ex) {
            System.debug(ex.getMessage());
            throw ex;
        }
    }
	  
    /********************************************************************************************************
*  @author          Deloitte
*  @description    to fetch PRF data
*  @param           
*  @date            june 14, 2020
*********************************************************************************************************/
    @AuraEnabled(cacheable=true)
    public static List<CCL_PRF_Summary__mdt > getPRF(string therapyName) {
        final List<CCL_PRF_Summary__mdt> myFinalList = new List<CCL_PRF_Summary__mdt>();
        try {
            //therapyName='%'+therapyName+'%';
			final String mytherapyName='%'+therapyName+'%';
            if (CCL_PRF_Summary__mdt.sObjectType.getDescribe().isAccessible()) {
                
                final List<CCL_PRF_Summary__mdt> myLst=[select CCL_Field_Api_Name__c,Masterlabel,CCL_Field_Type__c,CCL_Help_Text__c,CCL_HasAddress__c, CCL_Therapy_Name_all__c ,CCL_Order_of_Field__c,
                CCL_Order_of_Section__c,CCL_Object_API_Name__c,CCL_Section_Name__c from CCL_PRF_Summary__mdt   order by CCL_Order_of_Section__c];
                myFinalList.addAll(myLst);   
            }
            if(!myFinalList.isEmpty()) {
                final String obj=myFinalList[0].CCL_Object_API_Name__c;
                final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
                final Schema.SObjectType mySchema = schemaMap.get(obj);
                final Map <String, Schema.SObjectField> fieldMap = mySchema.getDescribe().fields.getMap();
                for(CCL_PRF_Summary__mdt cs:myFinalList) {
                    cs.Masterlabel=fieldMap.get(cs.CCL_Field_Api_Name__c).getDescribe().getLabel();
                }
                
            }
        } catch(AuraHandledException ex) {
            System.debug(ex.getMessage());
            throw  ex;
        }
        return myFinalList;
    }
    /********************************************************************************************************
*  @author          Deloitte
*  @description    to fetch field names and values from metadata
*  @param           
*  @date            june 14, 2020
*********************************************************************************************************/
    @AuraEnabled
    public static list<sobject> getRecords(string sobj,string cols,Id recordId) {
        
        try {
            //final string query = 'select '+cols+' from '+sobj+' where id='+'\'' + recordId + '\'' ;
			final string query = 'select '+String.escapeSingleQuotes(cols)+' from '+String.escapeSingleQuotes(sobj)+' where id='+'\'' + String.escapeSingleQuotes(recordId) + '\'' ;
            return Database.query(query);
        } catch(AuraHandledException ex) {
            System.debug(ex.getMessage());
            throw ex;
        }
        
    }
	   /********************************************************************************************************
*  @author          Deloitte
*  @description     to save the JSON data in field on click of SAVE FOR LATER button
*  @param           
*  @date            june 17, 2020
*********************************************************************************************************/
  @AuraEnabled
public static List<CCL_Apheresis_Data_Form__c> saveForLater(string myJSON,Id recordId) {
    try {
        final List<CCL_Apheresis_Data_Form__c> adf=[SELECT CCL_Patient_Name__c,CCL_SaveJSON__c FROM CCL_Apheresis_Data_Form__c WHERE Id=:recordId WITH SECURITY_ENFORCED];
        adf[0].CCL_SaveJSON__c=myJSON;
		adf[0].CCL_Land_on_Patient_Verification__c=false;
        //update adf;
		Database.Update(adf);
        return adf;
    } catch(DmlException error) {
        //final System.AuraHandledException error = new AuraHandledException(e.getMessage());
         throw new AuraHandledException(error.getMessage());}
}
/********************************************************************************************************
*  @author          Deloitte
*  @description     to save the ADF data from the Flow
*  @param           
*  @date            june 24, 2020
*********************************************************************************************************/
    @AuraEnabled
    public static void saveAdfData(Map<String,String> adfData) {
        
       if(!adfData.isEmpty()) {
            
           CCL_Apheresis_Data_Form__c adf = new CCL_Apheresis_Data_Form__c();
            if(adfData.containsKey('Id')) {
                adf.id = adfData.get('Id');
            }
            if(adfData.containsKey('CCL_Other__c')) {
                adf.CCL_Other__c=adfData.get('CCL_Other__c');
            }
            if(adfData.containsKey('CCL_Collection_Cryopreservation_Comments__c')) {
                adf.CCL_Collection_Cryopreservation_Comments__c=adfData.get('CCL_Collection_Cryopreservation_Comments__c');
            }
            if(adfData.containsKey('CCL_Apheresis_System_Used__c')) {
                adf.CCL_Apheresis_System_Used__c=adfData.get('CCL_Apheresis_System_Used__c');
            }
            try {
                Database.update(adf) ;
            } catch(AuraHandledException ex) {
                System.debug(ex);
                throw ex;
            }
       }
        }
   /********************************************************************************************************
*  @author          Deloitte
*  @description     to create Collections using the JSON data from flow 
*  @param           
*  @date            june 24, 2020
*********************************************************************************************************/
 @AuraEnabled
    public static string saveCollections(List<CCL_summary__c> collList) {
        Id integrationUserId;
        if (User.sObjectType.getDescribe().isAccessible()) {
            integrationUserId = [Select Id From User WHERE Profile.Name =: CCL_StaticConstants_MRC.CUSTOM_INTEGRATION_USER].Id;
        }
       if(collList!=null ) {
    Schema.SObjectField externalID = CCL_summary__c.Fields.CCL_External_Identification__c;
    final Id recordTypeId = Schema.SObjectType.CCL_summary__c.getRecordTypeInfosByName().get('Collection').getRecordTypeId();
    for (CCL_summary__c summ:collList) {
        summ.recordtypeid=recordTypeId;
        summ.OwnerId = integrationUserId;
    }
   try {
 
           database.UpsertResult[] results = Database.upsert(collList,externalID,false);
     system.debug('results' + results);
       
            if (results != null)
            {
                for (database.upsertResult result : results)
                {
                     if (!result.isSuccess())
                    {
                        Database.Error[] errs = result.getErrors();
                        for(Database.Error err : errs){
                            
                        System.debug(err.getStatusCode() + ' - ' + err.getMessage());
                            throw new AuraHandledException(err.getMessage());
                        }
                           
     
                    }
                }
            }    
 return Json.serialize(collList);
            }
               catch (AuraHandledException errs)
            {
               throw  errs;
            }
}
        return null;

    }
   /********************************************************************************************************
*  @author          Deloitte
*  @description     to get Recommended Collection meta data
*  @param           
*  @date            june 17, 2020
*********************************************************************************************************/
    @AuraEnabled(cacheable=true)
public static List<CCL_Recommended_Collection_Details__mdt > getRecommendedCollectionDetails() {
     List<CCL_Recommended_Collection_Details__mdt> metaDataList=new List<CCL_Recommended_Collection_Details__mdt>();
    try {
        
        if (CCL_Recommended_Collection_Details__mdt.sObjectType.getDescribe().isAccessible()) {
           metaDataList=[select CCL_Field_Api_Name__c,Masterlabel,CCL_Field_Type__c,CCL_Help_Text__c, CCL_Order_of_Field__c,
            CCL_Order_of_Section__c,CCL_Object_API_Name__c,CCL_Section_Name__c,CCL_Place_Holder__c,CCL_Show_Step__c,CCL_Max_Allowed__c,CCL_When_Range_overflow__c,CCL_When_Step_Mismatch__c,CCL_Min_Value__c ,CCL_When_Range_Underflow__c  from CCL_Recommended_Collection_Details__mdt  order by CCL_Order_of_Section__c limit 11]; 
        }
        if(!metaDataList.isEmpty()) {
            final String obj=metaDataList[0].CCL_Object_API_Name__c;
            final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            final Schema.SObjectType mySchema = schemaMap.get(obj);
            final Map <String, Schema.SObjectField> fieldMap = mySchema.getDescribe().fields.getMap();
            for(CCL_Recommended_Collection_Details__mdt cs:metaDataList) {
                cs.Masterlabel=fieldMap.get(cs.CCL_Field_Api_Name__c).getDescribe().getLabel();
            }
            
        }
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw  ex;
    }
    return metaDataList;

}

/********************************************************************************************************
*  @author          Deloitte
*  @description     This Method is being used to fetch the Details of the Logged in User.
*                   Added as part of CGTU-266
*  @param           void
*  @date            June 25, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static User getUserName() {
    User userDetails=new User();
    try {
        userDetails=[SELECT id,firstname, lastname,timezonesidkey,localesidkey FROM User WHERE id = :UserInfo.getUserId()];
        return userDetails;
    } catch(QueryException ex) {
        System.debug('Exception in CCL_ADF_Controller getUserName'+ex.getMessage());
        throw  ex;
    }
}
 /********************************************************************************************************
*  @author          Deloitte
*  @description    to fetch PRF data
*  @param           
*  @date            june 14, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<CCL_Required_Cryopreservation_Details__mdt > getRequiredCryo() {
    final List<CCL_Required_Cryopreservation_Details__mdt> myCryoFinalList = new List<CCL_Required_Cryopreservation_Details__mdt>();
    try {
        
        if (CCL_Required_Cryopreservation_Details__mdt.sObjectType.getDescribe().isAccessible()) {
            
            final List<CCL_Required_Cryopreservation_Details__mdt> myCryoLst=[select CCL_Field_Api_Name__c,CCL_IsEditable__c,CCL_Object_API_Name__c,CCL_Field_Length__c,Masterlabel,CCL_Field_Type__c,CCL_Help_Text__c ,CCL_Order_of_Field__c
             from CCL_Required_Cryopreservation_Details__mdt order by CCL_Order_of_Field__c limit 50];
            myCryoFinalList.addAll(myCryoLst);   
        }
        if(!myCryoFinalList.isEmpty()) {
            final String obj=myCryoFinalList[0].CCL_Object_API_Name__c;
            final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            final Schema.SObjectType mySchema = schemaMap.get(obj);
            final Map <String, Schema.SObjectField> fieldMap = mySchema.getDescribe().fields.getMap();
            for(CCL_Required_Cryopreservation_Details__mdt cs:myCryoFinalList) {
                cs.Masterlabel=fieldMap.get(cs.CCL_Field_Api_Name__c).getDescribe().getLabel();
            }
            
        }
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw  ex;
    }
    return myCryoFinalList;
}      
 /********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetch the crypreservation details of the collectiions
*  @param           object name,colums, and collection ids
*  @date            June 27, 2020
*********************************************************************************************************/
     @AuraEnabled(cacheable=true)
    public static list<sobject> getRequiredCryoDetails(string sobj,string cols,List<Id> idList) {
        try {
            //final string query = 'select '+cols+' from '+sobj+' where id in: idList';
			final string query = 'select '+String.escapeSingleQuotes(cols)+' from '+String.escapeSingleQuotes(sobj)+' where id in: idList';
            return Database.query(query);
        } catch(AuraHandledException ex) {
            System.debug(ex.getMessage());
            throw ex;
        }
        
    }
    

/********************************************************************************************************
*  @author          Deloitte
*  @description     This Method is being used to fetch the cuttent time of the Logged in user,
*                   based on their time Zone. Added as part of CGTU-266
*  @param           void
*  @date            June 25, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static DateTime getUserTimeZoneDetails() {
    try {
        final Timezone userTimeZone = UserInfo.getTimeZone();
        final Integer offset = userTimeZone.getOffset(system.now());
        return  System.now().addSeconds(offset/1000);
    } catch(AuraHandledException ex) {
        System.debug('Exception in CCL_ADF_Controller getUserTimeZoneDetails'+ex.getMessage());
        throw ex;
    }
}

/********************************************************************************************************
*  @author          Deloitte
*  @description     This Method is being used to fetch the cuttent time of the Logged in user,
*                   based on their time Zone. Added as part of CGTU-266
*  @param           void
*  @date            June 25, 2020
*********************************************************************************************************/
@AuraEnabled
public static DateTime getUserTimeZoneDetails(String timeZoneVal) {
    try {
    	DateTime adjustedDateTime;
        final Timezone userTimeZone = TimeZone.getTimeZone(timeZoneVal);
        final String currentTime=String.valueOf(Datetime.now());
        DateTime inputDateTime = DateTime.valueofGmt(currentTime);
        final TimeZone timeZone = TimeZone.getTimeZone(userTimeZone.getID());
        final Integer offset = timeZone.getOffset(inputDateTime);
        adjustedDateTime = Datetime.now().addMinutes( offset / (1000 * 60));
       return adjustedDateTime;
    } catch(AuraHandledException ex) {
        System.debug('Exception in CCL_ADF_Controller getUserTimeZoneDetails'+ex.getMessage());
        throw ex;
    }
}

/********************************************************************************************************
*  @author          Deloitte
*  @description     This Method is being used to fetch the cuttent time of the Logged in user,
*                   based on their time Zone in a specified format. Added as part of CGTU-266
*  @param           void
*  @date            June 25, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static String getUserTimeInFormat(DateTime dateTimeVal, String format){
try{
      String formatedDate='';
        if(dateTimeVal!=null) {
            formatedDate=dateTimeVal.formatGMT(format);
        }
        return formatedDate;
    } catch(AuraHandledException ex) {
        System.debug('Exception in CCL_ADF_Controller getUserTimeInFormat'+ex.getMessage());
        throw ex;
    }

}

/********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetch the cuttent time of the Logged in user,
*                   based on their time Zone. Added as part of CGTU-266
*  @param           userName and completionDate
*  @date            June 25, 2020
*********************************************************************************************************/
@AuraEnabled
public static void saveTransferToCellLabDetails(User userName,Datetime completionDate,Id recordId,String textVal) {
    try {   
        CCL_Apheresis_Data_Form__c adfRecords;
        if (CCL_Apheresis_Data_Form__c.sObjectType.getDescribe().isAccessible()) {
            adfRecords=[SELECT id,CCL_Collection_Details_Completed_By__c,CCL_Collection_Details_Completed_Date__c,CCL_TEXT_Collection_Details_Date_Time__c FROM CCL_Apheresis_Data_Form__c WHERE Id=:recordId WITH SECURITY_ENFORCED];                                     
            }           
            if(adfRecords!=null) {
            adfRecords.CCL_Collection_Details_Completed_By__c=userName.Id;
            adfRecords.CCL_Collection_Details_Completed_Date__c=completionDate;
            adfRecords.CCL_TEXT_Collection_Details_Date_Time__c=textVal;
            if (CCL_Apheresis_Data_Form__c.sObjectType.getDescribe().isUpdateable()) {
            update adfRecords;
            }
            }
    } catch(QueryException ex) {
        System.debug('Exception in CCL_ADF_Controller saveTransferToCellLabDetails'+ex.getMessage());
        throw  ex;
    }
}
 /********************************************************************************************************
*   @author          Deloitte
*  @description     thiis method saves summary and batches creates
*  @param           summary list and batch list
*  @date            June 27, 2020
*********************************************************************************************************/
@AuraEnabled
public static string saveCryo(List<CCL_Batch__c>bags,List<CCL_Summary__c>summaryLst,String comments,Id adfId) {
     try {  
         Schema.SObjectField cryoBagId = CCL_Batch__c.Fields.CCL_Cryobag_External_ID__c;
         Id integrationUserId;
         if (User.sObjectType.getDescribe().isAccessible()) {
            integrationUserId = [Select Id From User WHERE Profile.Name =: CCL_StaticConstants_MRC.CUSTOM_INTEGRATION_USER].Id;
         }
if(!bags.isEmpty()) {
      final Id recordTypeId = Schema.SObjectType.CCL_Batch__c.getRecordTypeInfosByName().get('Cryobag').getRecordTypeId();
       
     for (CCL_Batch__c bag:bags) {
        bag.recordtypeid=recordTypeId;
        bag.OwnerId = integrationUserId;
    }
    
   
     database.UpsertResult[] results = Database.upsert(bags, cryoBagId, false);
	  
            if (results != null)
            {
                for (database.upsertResult result : results)
                {
                     if (!result.isSuccess())
                    {
                        Database.Error[] errs = result.getErrors();
                        for(Database.Error err : errs){
                            
                        throw new AuraHandledException(err.getMessage());
                        }
                           
     
                    }
                }
            }
 return Json.serialize(bags);    
    }
 if(!summaryLst.isEmpty()) {
        Database.update(summaryLst);
    }
         if(comments!=null){
             CCL_Apheresis_Data_Form__c adf=new CCL_Apheresis_Data_Form__c(Id=adfId);
             adf.CCL_Collection_Cryopreservation_Comments__c=comments;
              Database.update(adf);
         }
} catch(AuraHandledException ex) {
    System.debug(ex);
    throw ex;
}
   return null;
}    

     /********************************************************************************************************
*  @author          Deloitte
*  @description     to get Recommended Cryopreservation meta data
*  @param           
*  @date            july 06, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<CCL_Recommended_Cryopreservation_Details__mdt> getRecommendedCryoDetails() {
     List<CCL_Recommended_Cryopreservation_Details__mdt> metaDataList=new List<CCL_Recommended_Cryopreservation_Details__mdt>();
    try {
        
        if (CCL_Recommended_Cryopreservation_Details__mdt.sObjectType.getDescribe().isAccessible()) {
           metaDataList=[select CCL_Field_Api_Name__c,Masterlabel,CCL_Field_Type__c,CCL_Help_Text__c, CCL_Order_of_Field__c,
            CCL_Order_of_Section__c,CCL_Object_API_Name__c,CCL_Section_Name__c,CCL_Place_Holder__c,CCL_Show_Step__c,CCL_Max_Allowed__c,CCL_When_Range_overflow__c,CCL_When_Step_Mismatch__c,CCL_Min_Value__c ,CCL_When_Range_Underflow__c  from CCL_Recommended_Cryopreservation_Details__mdt  order by CCL_Order_of_Section__c limit 14]; 
        }
        if(!metaDataList.isEmpty()) {
            final String obj=metaDataList[0].CCL_Object_API_Name__c;
            final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            final Schema.SObjectType mySchema = schemaMap.get(obj);
            final Map <String, Schema.SObjectField> fieldMap = mySchema.getDescribe().fields.getMap();
            for(CCL_Recommended_Cryopreservation_Details__mdt cs:metaDataList) {
                cs.Masterlabel=fieldMap.get(cs.CCL_Field_Api_Name__c).getDescribe().getLabel();
            }
            
        }
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw  ex;
    }
    return metaDataList;

}  
 
   

}

/*********************************************************************************************************
class Name      : PSP_UtilityTest
Description     : Test class for Controller for custom lookup
@author         : Deloitte
@date           : November 11, 2019
Modification Log:
--------------------------------------------------------------------------------------------------------------
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------
Deloitte            November 11, 2019          Initial Version
****************************************************************************************************************/
@IsTest public class PSP_UtilityTest {
  /* Apex constants */
  static final List<PSP_ApexConstantsSetting__c> APEX_CONSTANTS =
  PSP_Test_Setup.setDataforApexConstants();
  /*Care Program Object*/
    final Static String OBJ_NAME_CAREPROGRAM=PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.OBJ_NAME_CAREPROGRAM);
  /*name field*/
    final Static String CAREPROGRAM_FLD_NAME=PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.CAREPROGRAM_FLD_NAME);
  /* User name */
  final Static String TEST_USER = 'Test';
  /*************************************************************************************
   * @author      : Deloitte
   * @date        : 11/11/2019
   * @Description : Testing Careprogram insert scenarios
   * @Param       : Void
   * @Return      : Void
   ***************************************************************************************/
  public
  static testMethod void testUniqueRecords() {

    try {
      Test.startTest();
      final List<CareProgram> careProgramLst = new List<CareProgram>();
      final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram(TEST_USER, null);
      Database.insert(cP1);
      final CareProgram cP2 = PSP_Test_Setup.createTestCareProgramWithRecordType(TEST_USER, null);
      final CareProgram cP3 = PSP_Test_Setup.createTestCareProgramWithRecordType(TEST_USER, null);
      careProgramLst.add(cP2);
      careProgramLst.add(cP3);

      final List<String> names = new List<String>();
      // List of care programs to be checked
      final List<CareProgram> prgToChk = new List<CareProgram>();
      for (CareProgram prg : careProgramLst) {
        if (String.isNotBlank(prg.Name)) {
          names.add(prg.Name);
          prgToChk.add(prg);
        }
      }
      PSP_Utility.checkUnique(OBJ_NAME_CAREPROGRAM,
          CAREPROGRAM_FLD_NAME,
          Label.PSP_Unique_Care_Program_Name,
          names,
          prgToChk,
          careProgramLst[0].RecordTypeId);
      PSP_Utility.checkUnique(OBJ_NAME_CAREPROGRAM,
          CAREPROGRAM_FLD_NAME,
          Label.PSP_Unique_Care_Program_Name,
          names,
          prgToChk,
          '');  
      Test.stopTest();
    } catch (DmlException e) {
      System.assert(e.getMessage().contains('Insert failed.'), e.getMessage());
    }
  }
}
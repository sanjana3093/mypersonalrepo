/********************************************************************************************************
*  @author          Deloitte
*  @description     This is a test class created for CCL ADF Controller Utility.
To Test the common methods in the ADF Controller class
*  @param
*  @date            July 10, 2020
*********************************************************************************************************/
@isTest
public class CCL_ADFController_Utility_Test {
    /* Therapy name */
    final static String THERAPY_NAME = 'KYMRIAH - Pediatric ALL US';
    /*Account Name */
    final static String ACCOUNT_NAME = 'Test Account';
    /*************************************************************************************
* @author      : Deloitte
* @date        : July 10 2020
* @Description : Testing CheckUserPermission(), to check the Current Logged in user permission.
*                Added as part of Change CGTU-318.
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void testCheckUserPermission() {
        final String permissionName=CCL_StaticConstants_MRC.PERMISSION_TYPE_SUBMITTER;
        Test.startTest();
        final Boolean checkPermission=CCL_ADFController_Utility.checkUserPermission(permissionName);
        System.assert(!checkPermission,'The user does not have permission');
        Test.stopTest();
    }

    /*************************************************************************************
* @author      : Deloitte
* @date        : December 11 2020
* @Description : Testing getAllPermissionSets(), to check the Current Logged in user permission.
*                Added as part of Change CGTU-318.
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    static testMethod void getAllPermissionSetsTest() {
        final String permissionName=CCL_StaticConstants_MRC.PERMISSION_TYPE_SUBMITTER;
        Profile p = [SELECT Id FROM Profile WHERE Name='External Base Profile'];
        User u = [Select Id from user where ProfileId = :p.Id AND isActive = true limit 1];
        Test.startTest();
        System.runAs(u) {
        final List<PermissionSetAssignment> permSetList=CCL_ADFController_Utility.getAllPermissionSets();
            boolean hasPermissions = permSetList.size()>0?true:false;
       System.assertEquals(hasPermissions, true);
        Test.stopTest();
        }
    }
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : July 10 2020
* @Description : Testing getADFSummaryPageInfo(), to fetch the ADF Summary page details.
*                Added as part of Change CGTU-318.
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void testGetADFSummaryPageInfo() {
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        final CCL_Apheresis_Data_Form__c adfObj=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
        Test.startTest();
        final List<CCL_Apheresis_Data_Form__c> adfList= CCL_ADFController_Utility.getADFSummaryPageInfo(adfObj.Id);
        Test.stopTest();
        System.assertEquals(adfList[0].CCL_Status__c,'ADF Pending Approval','The Status Matches');

    }

    /*************************************************************************************
* @author      : Deloitte
* @date        : July 10 2020
* @Description : Testing getUploadedFileDetails(), to fetch the file details for ADF Summary page details.
*                Added as part of Change CGTU-318.
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void testGetUploadedFileDetails() {
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        final CCL_Apheresis_Data_Form__c adfObj=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
        Test.startTest();
         final list<ContentDocument> cdList= CCL_TestDataFactory.createTestContentDocumenet();
         final ContentDocumentLink contentDocLink=CCL_Test_SetUp.createTestContentDocumentLink(cdList[0].Id,adfObj.Id);
        final List<ContentDocumentLink> fileList= CCL_ADFController_Utility.getUploadedFileDetails(adfObj.Id);
        Test.stopTest();
        System.assertEquals(fileList.size(),1,'No Files uploaded');

    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing infectious disease fetch scenario
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void getInfectDiseases() {
        Test.startTest();
        final List<CCL_Infectious_Diseases_Testing__mdt> testInfDData=CCL_ADFController_Utility.getInfectousDiseases(THERAPY_NAME);
        Test.stopTest();
        final List<CCL_Infectious_Diseases_Testing__mdt> actualInfDisData=[select CCL_Field_Api_Name__c,Masterlabel ,CCL_Display_Label__c,CCL_Order_of_Field__c
                                                                           from CCL_Infectious_Diseases_Testing__mdt where CCL_Therapy_Name_all__c like: '%'+THERAPY_NAME+'%'  order by CCL_Order_of_Field__c limit 50];
        System.assertEquals(testInfDData.size(),actualInfDisData.size(),'testDataSize');
    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/24/2020
* @Description : Testing deleteCollections function
* @Param       : String
* @Return      : Void
***************************************************************************************/
   static testMethod void deleteCollectionsTest () {
        User testUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE);
        
        insert testUser;
        System.runAs(testUser) {
            final List<CCL_Summary__c> summList = new List<CCL_Summary__c>();
            final CCL_Summary__c summObj= new CCL_Summary__c();
            summObj.CCL_Apheresis_ID_DIN_DEC__c='test1';
            summObj.CCL_Volume_of_blood_processed__c=1.2;
			summObj.CCL_External_Identification__c='1234567';
            summList.add(summObj);
            database.insert(summList);
            Test.startTest();
            CCL_ADF_Controller.saveCollections(summList);
            CCL_ADFController_Utility.deleteCollections(summList);
            
            Test.stopTest();
            final CCL_Summary__c summ = [Select Id, IsDeleted from CCL_Summary__c WHERE Id = :summObj.Id ALL ROWS];
            System.assertEquals(0,[Select Id From CCL_Summary__c where Id = : summObj.Id].size(),'Success');
        }
    } 
    /*************************************************************************************
* @author      : Deloitte
* @date        : July 28 2020
* @Description : Testing getSummaryObjectsInfo(), to fetch the summary manufacturing
*                details for ADF Summary page details.
*                Added as part of Change CGTU-844.
* @Param       : Void
* @Return      : Void
***************************************************************************************/
   static testMethod void testGetSummaryObjectsInfo () {
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        final CCL_Apheresis_Data_Form__c adfObj=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
        List<CCL_Summary__c> actSummaryObjList = new List<CCL_Summary__c>();
        final Id recordTypeId = Schema.SObjectType.CCL_summary__c.getRecordTypeInfosByName().get('Manufacturing').getRecordTypeId();
       final CCL_Summary__c summaryObj=CCL_Test_SetUp.createTestSummaryByRecordType(recordTypeId,adfObj.Id,order.Id);
        Test.startTest();
        actSummaryObjList=CCL_ADFController_Utility.getSummaryObjectsInfo(order.Id);
        Test.stopTest();
        final CCL_Summary__c returnList = [select id,CCL_Manufacturing_Start_Date_Time__c from CCL_summary__c where CCL_Order__r.id=:order.id AND RecordTypeId=:recordTypeId WITH SECURITY_ENFORCED];
        System.assertEquals(returnList.CCL_Manufacturing_Start_Date_Time__c,actSummaryObjList[0].CCL_Manufacturing_Start_Date_Time__c,'Date value Matches');

    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : July 30 2020
* @Description : Testing getADFFileDetails(), to fetch the file details uploaded by scp
*                Added as part of Change CGTU-842.
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void testGetADFFileDetails() {
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
        Test.startTest();
        final List<ContentDocumentLink> fileList= CCL_ADFController_Utility.getADFFileDetails(adf.Id);
        Test.stopTest();
        System.assertEquals(fileList.size(),0,'No Files uploaded');

    }
/*************************************************************************************
 * @author      : Deloitte
 * @date        : 11/19/2020
 * @Description : Testing order header fetch scenario
 * @Param       : Void
 * @Return      : Void
 ***************************************************************************************/
static testMethod void testShipmentHeaderWithOrderObject() {
    Test.startTest();
    final List<CCL_PRF_Header__mdt> testInfDData=CCL_ADFController_Utility.getShipmentHeader('CCL_Order__c');
    Test.stopTest();
    final List<CCL_PRF_Header__mdt> actualInfDisData=[select CCL_Field_API_Name__c,CCL_Field_Type__c,Masterlabel,CCL_Object_API_Name__c,CCL_Order_of_field__c
                                                           from CCL_PRF_Header__mdt  order by CCL_Order_of_field__c limit 20];
    System.assertEquals(testInfDData.size(),actualInfDisData.size(),'The size');
}

/*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing shipment  header fetch scenario
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void testShipmentHeader() {
        Test.startTest();
        final List<CCL_Shipment_Header__mdt> testInfDData=CCL_ADFController_Utility.getShipmentHeader('CCL_Shipment__c');
        Test.stopTest();
        final List<CCL_Shipment_Header__mdt> actualInfDisData=[select CCL_Field_API_Name__c,CCL_Field_Type__c,Masterlabel,CCL_Object_API_Name__c,CCL_Order_of_field__c
                                                               from CCL_Shipment_Header__mdt  order by CCL_Order_of_field__c limit 20];
        System.assertEquals(testInfDData.size(),actualInfDisData.size(),'The size');
    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing shipment  details fetch scenario
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void testShipmentDetails() {
        Test.startTest();
        final List<CCL_Aph_Shipment_Detail__mdt> testshipmntData=CCL_ADFController_Utility.getShipmentDetails();
        Test.stopTest();
        final List<CCL_Aph_Shipment_Detail__mdt> actualshpmntData=[select CCL_Field_API_Name__c,CCL_Field_Length__c,CCL_HasAddress__c,CCL_isEditable__c,CCL_Field_Type__c,Masterlabel,CCL_Object_API_Name__c,CCL_Order_of_field__c
                                                                   from CCL_Aph_Shipment_Detail__mdt  order by CCL_Order_of_field__c limit 20];
        System.assertEquals(testshipmntData.size(),actualshpmntData.size(),'The size');
    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing cryobag  details fetch scenario
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void testcryoBagfetch() {
        Test.startTest();
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
       final CCL_Batch__c bag=CCL_Test_SetUp.createTestBtch(adf.Id,order.Id);
        final string recordId=adf.Id;
        final List<CCL_Batch__c> testbagData=CCL_ADFController_Utility.getCryoBagDetails(order.Id);
        Test.stopTest();
        final List<CCL_Batch__c> actualbagtData=[select Id,CCL_Cryobag_ID__c,CCL_COI_ID__c,CCL_Label_Compliant__c from CCL_Batch__c where CCL_Apheresis_Data_Form__c=:recordId WITH SECURITY_ENFORCED];
        System.assertEquals(testbagData.size(),actualbagtData.size(),'The size');
    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing timezone  details fetch scenario
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void testtimezone() {
        Test.startTest();
        final CCL_Time_Zone__c timezone =CCL_Test_SetUp.createTestTimezone(ACCOUNT_NAME);
        final Account acc=CCL_Test_SetUp.createTestAccount(ACCOUNT_NAME,timezone.Id);
        final List<Account> accountData=CCL_ADFController_Utility.getTimeZone(acc.Id);
        Test.stopTest();
        final List<Account> actualaccData=[SELECT CCL_Time_Zone__r.Name FROM Account WHERE id=:acc.Id  WITH SECURITY_ENFORCED limit 1];
        System.assertEquals(accountData.size(),actualaccData.size(),'actualaccDataSize');
    }

/*************************************************************************************
 * @author      : Deloitte
 * @date        : 11/19/2020
 * @Description : Testing therapy type details fetch scenario
 * @Param       : Void
 * @Return      : Void
 ***************************************************************************************/
static testMethod void testTherapyTypeWithOrderObject() {
    Test.startTest();
    final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
    final List<CCL_Order__c> accountData=CCL_ADFController_Utility.getTherapyType(order.Id, 'Order');
    Test.stopTest();
    final List<CCL_Order__c> actualaccData=[SELECT Id FROM CCL_Order__c WHERE id=:order.Id  WITH SECURITY_ENFORCED limit 1];
    System.assertEquals(accountData.size(),actualaccData.size(),'actualaccDataSize');
}

/*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing therapy type  details fetch scenario
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void testTherapyType() {
        Test.startTest();
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
        final CCL_Shipment__c shipment =CCL_Test_SetUp.createTestShippmnt(adf.Id,order.Id);
        final List<CCL_Shipment__c> accountData=CCL_ADFController_Utility.getTherapyType(shipment.Id,'Shipment');
        Test.stopTest();
        final List<CCL_Shipment__c> actualaccData=[SELECT CCL_Indication_Clinical_Trial__r.CCL_Type__c,CCL_Ordering_Hospital__r.CCL_Hospital_Patient_Id_Opt_In__c FROM CCL_Shipment__c WHERE id=:shipment.Id  WITH SECURITY_ENFORCED limit 1];
        System.assertEquals(accountData.size(),actualaccData.size(),'actualaccDataSize');
    }
     /*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing therapy type  details fetch scenario
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void testfetchHospital() {
        Test.startTest();
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        final CCL_Therapy__c therapy =CCL_TestDataFactory.createTestTherapy();
        insert therapy;
         final CCL_Time_Zone__c timezone =CCL_Test_SetUp.createTestTimezone(ACCOUNT_NAME);
        final Account acc=CCL_Test_SetUp.createTestAccount(ACCOUNT_NAME,timezone.Id);
        final List<CCL_Therapy_Association__c> therapyAss=CCL_ADFController_Utility.fetchHospitalOptIn(therapy.Id,acc.Id);
        Test.stopTest();
        final List<CCL_Therapy_Association__c> actualTherapyData=[SELECT CCL_Hospital_Patient_ID_Opt_In__c FROM CCL_Therapy_Association__c WHERE CCL_Therapy__c=:therapy.Id and CCL_Site__c=:acc.Id  WITH SECURITY_ENFORCED limit 1];
        System.assertEquals(therapyAss.size(),actualTherapyData.size(),'actualTherapyDataSize');
    }

    /*************************************************************************************
* @author      : Deloitte
* @date        : 08/4/2020
* @Description : Testing DeleteFile method
* @Param       : Void
* @Return      : Void
***************************************************************************************/
   static testMethod void testDeleteFile() {
        User testUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE);
        
        insert testUser;
        System.runAs(testUser) {
            final list<ContentDocument> cdList= CCL_TestDataFactory.createTestContentDocumenet();
            Test.startTest();
            final Integer integ=CCL_ADFController_Utility.deleteFile(cdList[0].Id);
            //CCL_ADFController_Utility.deleteFile('test');
            Test.stopTest();
            System.assertEquals(integ,1,'Deleted Successfully');
        }
    }
        /*************************************************************************************
* @author      : Deloitte
* @date        : 08/105/2020
* @Description : Testing Associated ADF details fetch scenario
* @Param       : Void
* @Return      : Void
***************************************************************************************/
static testMethod void testGetAssociatedADFStatus() {
    Test.startTest();
    final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
    final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
    final CCL_Shipment__c shipment =CCL_Test_SetUp.createTestShippmnt(adf.Id,order.Id);
    final List<CCL_Shipment__c> accountData=CCL_ADFController_Utility.getAssociatedADFStatus(shipment.Id);
    Test.stopTest();
    final List<CCL_Shipment__c> actualaccData=[SELECT CCL_Apheresis_Data_Form__r.CCL_Status__c,CCL_Apheresis_Data_Form__r.Id  FROM CCL_Shipment__c WHERE id=:shipment.Id   WITH SECURITY_ENFORCED limit 1];
    System.assertEquals(accountData.size(),actualaccData.size(),'actualaccDataSize');
}
/*************************************************************************************
* @author      : Deloitte
* @date        : 08/12/2020
* @Description : Testing Associated getDateTimeValues and retriveBatches method
***************************************************************************************/
        static testMethod void testgetDateTimeValues() {
        Test.startTest();
        final DateTime myDateTime = DateTime.newInstance(2002, 12, 3, 3, 3, 3);
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
        CCL_ADFController_Utility.getTimeZoneDetails('ScreenName',adf.Id);
        final List<CCL_Date_Time_Site_Mapping__mdt> DateTimeSite = CCL_ADFController_Utility.getPageTimeZoneDetails('ScreenName');
        final String datevalue = string.valueof(myDateTime);
        final DateTime adfDateTime = CCL_ADFController_Utility.getFormattedDateTimeDetails(datevalue,'time');
        System.assertEquals(myDateTime,adfDateTime,'Date Time Value');
        final Timezone actualTimeZone = UserInfo.getTimeZone();
        //final Integer offset = actualTimeZone.getOffset(system.now());
        final Timezone time1 = UserInfo.getTimeZone();
        //final Integer offset1 = actualTimeZone.getOffset(system.now()-10);
        CCL_ADFController_Utility.getFormattedDateTimeDetails(datevalue,'time1');
        Test.stopTest();

        }


  /*************************************************************************************
* @author      : Deloitte
* @date        : 08/20/2020
* @Description : Testing Associated getShipmentInfo method
***************************************************************************************/
/*
static testMethod void testgetShipmentInfo() {
        Test.startTest();
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        insert order;
        final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
        insert adf;
        final CCL_Shipment__c shipment = CCL_Test_SetUp.createTestShippmnt(adf.id);
        insert shipment;
        final List<CCL_Shipment__c> shipmentdata = CCL_ADFController_Utility.getShipmentInfo(shipment.id);
        final List<CCL_Apheresis_Data_Form__c> adfList = CCL_ADFController_Utility.updateReasonForModificaton(adf.id,'reason');
        final List<CCL_Apheresis_Data_Form__c> listShipment = [Select id,CCL_Reason_For_Modification__c from CCL_Apheresis_Data_Form__c where id =:adf.id];
    system.debug('adfList'+adfList+listShipment);
        system.assertEquals(listShipment,adfList,'test shipment list');
        Test.stopTest();
}
  */
        /*************************************************************************************
* @author      : Deloitte
* @date        : Aug 27 2020
* @Description : Testing getAssociatedOrderHardPeg(), to fetch the Order Hard Peg from Shipment
*                Added as part of Change CGTU-319.
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void getAssociatedOrderHardPegTest () {
         Test.startTest();
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        order.CCL_Hard_Peg__c= true;
        final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
        final CCL_Shipment__c shipment = CCL_Test_SetUp.createTestShippmnt(adf.id,order.Id);
        final List<CCL_Shipment__c> shipmentdata = CCL_ADFController_Utility.getAssociatedOrderHardPeg(shipment.id);
        final List<CCL_Apheresis_Data_Form__c> adfdata = CCL_ADFController_Utility.getADFAssociatedOrderHardPeg(adf.id);

        final List<CCL_Shipment__c> listShipment = [SELECT Id, CCL_Order_Apheresis__r.CCL_Hard_Peg__c, CCL_Order_Apheresis__r.Id FROM CCL_Shipment__c WHERE id=:shipment.Id  WITH SECURITY_ENFORCED limit 1];
        system.assert(!listShipment[0].CCL_Order_Apheresis__r.CCL_Hard_Peg__c,'false');
        Test.stopTest();
    }
	/*************************************************************************************
* @author      : Deloitte
* @date        : 12/17/2020
* @Description : Testing getDocumentDetails method 
***************************************************************************************/ 
static testMethod void testgetDocumentDetails() {
     Test.startTest();
    final list<ContentDocument> testDocument = CCL_TestDataFactory.createTestContentDocumenet();
      final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
    CCL_Document__c doc=CCL_Test_SetUp.createTestDocuemnt(order.Id);

    final ContentDocumentLink testContentDocLink = CCL_Test_SetUp.createTestContentDocumentLink(testDocument[0].Id, doc.Id);
     final list<CCL_Document__c> docList=[select id,name,CCL_Apheresis_Data_Form__c,CCL_Order__c,CCL_Shipment__c from CCL_Document__c where id= :testContentDocLink.LinkedEntityId];
    final list<CCL_Document__c> fetchedDocList=  CCL_ADFController_Utility.getDocumentDetails(testDocument[0].Id); 
 System.assertEquals(docList.size(),fetchedDocList.size(),'actualaccDataSize');    
    Test.stopTest();
}
  /*************************************************************************************
* @author      : Deloitte
* @date        : Jan 28 2021
* @Description : Test method for ADFController_Utility class.
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void getAllOrdersTest () {
         Test.startTest();
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        Id accId = CCL_Test_SetUp.createTestAccount('testAccountXY',null).Id;
        CCL_ADFController_Utility.getAllOrders(0,accId,accId);
        Integer ordrCount = CCL_ADFController_Utility.getAllOrderCount(accId,null);
        CCL_ADFController_Utility.fetchAccountsFrmFilter('test','Account');
        CCL_ADFController_Utility.fetchListViews();
        CCL_ADFController_Utility.getOrderHeader();
        Id shipmntId = CCL_Test_SetUp.createTestShippmnt(null,null).Id;
        CCL_ADFController_Utility.getShipmentInfo(shipmntId);
        CCL_ADFController_Utility.getTimeZone(accId);
        final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF('testAcc',order.Id);
        CCL_ADFController_Utility.getTimeZoneIDDetails('1',adf.id);
        final List<CCL_Apheresis_Data_Form__c> adfdata = CCL_ADFController_Utility.getOrderApprovalInfo(adf.id);
        final List<CCL_Document__c > docList= CCL_ADFController_Utility.getDocId(adf.id);
        system.assertEquals(0, ordrCount);
        Test.stopTest();
    }

}
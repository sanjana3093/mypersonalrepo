/**
* @author Deloitte
* @date 07/13/2018
*
* @description This is the Test class for class for spc_DocumentLogTriggerImplementationTest
*/

@isTest
public class spc_DocumentLogTriggerImplementationTest {

    @testsetup
    static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User objUser = new User(Alias = 'test', email = 'TestSysAdmin@test123.com', emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', country = 'United States', profileid = p.Id,
                                timezonesidkey = 'America/Los_Angeles', username = System.currentTimeMillis() + 'TestSysAdmin@test123.com');

        spc_Database.ins(objUser);

        List<Account> accountList = new List<Account>();

        Account patientAcc = spc_Test_Setup.createPatient('Patient Name');

        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        accountList.add(patientAcc);
        accountList.add(manAcc);
        insert accountList;

        List<Case> caseList = new List<Case>();

        Case prgCase1 = spc_Test_Setup.newCase(patientAcc.Id, 'PC_Program');
        Case inqCase2 = spc_Test_Setup.newCase(patientAcc.Id, 'spc_Inquiry');
        caseList.add(prgCase1);
        caseList.add(inqCase2);
        insert caseList;



        PatientConnect__PC_Engagement_Program__c engProgram = spc_Test_Setup.createEngagementProgram('Test EP', manAcc.Id);

        PatientConnect__PC_Document__c docNew = spc_Test_Setup.createDocument(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_FAX_INBOUND), 'Sent', engProgram.Id);
        insert docNew;

        PatientConnect__PC_Document_Log__c docLog = new PatientConnect__PC_Document_Log__c();
        docLog.PatientConnect__PC_Program__c = prgCase1.Id;
        docLog.PatientConnect__PC_Document__c = docNew.Id;
        docLog.spc_Inquiry__c = inqCase2.Id;

        insert docLog;

        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction.PatientConnect__PC_Patient_Program__c = prgCase1.Id;
        insert interaction;

    }

    public static testMethod void createReviewInboudDocumentTask() {
        Test.startTest();
        Map<Id, Id> caseIdsAndDocIds = new Map<Id, Id>();
        Map<Id, Id>  interactionsAndDocuments = new Map<Id, Id>();

        Case patientCase = [select id from Case LIMIT 1] ;
        PatientConnect__PC_Document__c oDoc = [select id from PatientConnect__PC_Document__c LIMIT 1];
        PatientConnect__PC_Document_Log__c doclog = [select id from PatientConnect__PC_Document_Log__c LIMIT 1];
        caseIdsAndDocIds.put(patientCase.ID, doclog.ID);
        Case progCase = [select id from Case where Type = 'Program'];
        PatientConnect__PC_Interaction__c interaction =  [select id from PatientConnect__PC_Interaction__c LIMIT 1];
        interactionsAndDocuments.put(progCase.Id, interaction.Id);
        system.assertNotEquals(interactionsAndDocuments, NULL);
        system.assertNotEquals(caseIdsAndDocIds, NULL);

        Test.stopTest();

    }

    public static testMethod void mapProgramCaseId() {
        Test.startTest();
        PatientConnect__PC_Interaction__c interaction =  [select id, PatientConnect__PC_Patient_Program__c from PatientConnect__PC_Interaction__c LIMIT 1];
        system.assertNotEquals(interaction.PatientConnect__PC_Patient_Program__c, NULL);
        Test.stopTest();

    }

    public static testMethod void intDocmethd() {
        Test.startTest();
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.spc_HIPAA_Consent_Received__c = 'YES';
        insert account;

        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');
        insert programCase;

        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction.PatientConnect__PC_Patient_Program__c = programCase.Id;
        insert interaction;

        PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c();
        doc.PatientConnect__PC_Document_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_STATUS_READYTOSEND);
        doc.RecordTypeId = Schema.getGlobalDescribe().get(spc_ApexConstants.DOCUMENT_OBJECT_API).getDescribe().getRecordTypeInfosByName().get(spc_ApexConstants.DOCUMENT_TYPE_FAX_OUTBOUND).getRecordTypeId();
        doc.spc_Interaction__c = interaction.id;
        insert doc;

        Map<Id, Id> interactionsAndDocuments = new Map<Id, Id>();
        interactionsAndDocuments.put(interaction.id, doc.id);

        spc_DocumentLogTriggerImplementation dti = new spc_DocumentLogTriggerImplementation();
        Map<Id, Id> caseIdsAndDocIds = new Map<Id, Id>();
        dti.createReviewInboudDocumentTask(caseIdsAndDocIds, interactionsAndDocuments, false);
        dti.createReviewInboudDocumentTask(caseIdsAndDocIds, interactionsAndDocuments, true);


        PatientConnect__PC_Document_Log__c docLog = new PatientConnect__PC_Document_Log__c();
        docLog.PatientConnect__PC_Program__c = programCase.id;
        docLog.PatientConnect__PC_Document__c = doc.Id;
        insert docLog;

        Map<Id, PatientConnect__PC_Document_Log__c> caseIdsAndDocLogs = new Map<Id, PatientConnect__PC_Document_Log__c>();
        caseIdsAndDocLogs.put(programCase.id, docLog);
        dti.mapProgramCaseIdinDocLog(caseIdsAndDocLogs);


        Map<Id, PatientConnect__PC_Document_Log__c> interactionIdsAndDocLogs = new Map<Id, PatientConnect__PC_Document_Log__c>();
        interactionIdsAndDocLogs.put(interaction.id, docLog);
        dti.mapProgramCaseId(interactionIdsAndDocLogs);

        PatientConnect__PC_Program_Coverage__c pcc = new PatientConnect__PC_Program_Coverage__c();
        pcc.PatientConnect__PC_Program__c = programCase.id;
        insert pcc;
        Map<Id, PatientConnect__PC_Document_Log__c> coverageIdsAndDocLogs = new Map<Id, PatientConnect__PC_Document_Log__c>();
        coverageIdsAndDocLogs.put(pcc.id, docLog);
        dti.populateCaseIdinDocLog(coverageIdsAndDocLogs);
        Test.stopTest();

    }
}
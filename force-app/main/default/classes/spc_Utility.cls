/********************************************************************************************************
 *  @author          Deloitte
 *  @description     This class is used for common functionalities across the application.
 *  @date            08/06/2018
 *  @version         1.0
 *********************************************************************************************************/

public class spc_Utility {

    /********************************************************************************************************
     *  @author          Deloitte
     *  @description     Logs the exception in the custom error object and throws exception
     *  @date            08/06/2018
     *  @return          void
     *  @param           1. Exception e : Exception raised in the application
     *  @version         1.0
     *********************************************************************************************************/
    public static void logAndThrowException(Exception e) {
        createExceptionLog(e);
        throw e;
    }
    /********************************************************************************************************
     *  @author          Deloitte
     *  @description     Create Exception Log record
     *  @date            08/06/2018
     *  @return          void
     *  @param           1. Exception e : Exception raised in the application
     *  @version         1.0
     *********************************************************************************************************/
    public static void createExceptionLog(Exception e) {
        PatientConnect__PC_Error_Log__c errLog = new PatientConnect__PC_Error_Log__c(PatientConnect__PC_Error_Datetime__c = DateTime.Now(), PatientConnect__PC_Error_Message__c = e.getMessage(), PatientConnect__PC_Running_User__c = UserInfo.getUserId(), PatientConnect__PC_Stack_Trace__c = e.getStackTraceString());
        createErrorLog(JSON.serialize(errLog));
    }
    /********************************************************************************************************
     *  @author          Deloitte
     *  @description     This is a helper method to create Error Log record
     *  @date            08/06/2018
     *  @return          void
     *  @param           PC_Error_Log__c
     *  @version         1.0
     *********************************************************************************************************/
    @future
    private static void createErrorLog(String errorString) {
        PatientConnect__PC_Error_Log__c error = (PatientConnect__PC_Error_Log__c) JSON.deserialize(errorString, PatientConnect__PC_Error_Log__c.class);
        spc_Database.ins(error, false);
    }

    /********************************************************************************************************
     *  @author          Deloitte
     *  @description     This is a helper method to check type of the user
     *  @date            08/06/2018
     *  @return          void
     *  @param           Id
     *  @version         1.0
     *********************************************************************************************************/
    public static Boolean checkOwnerIsUser(Id userId) {
        Boolean isUser = false;
        if (userId.getSObjectType().getDescribe().getName().equalsIgnoreCase(spc_ApexConstants.OwnerTypeUser)) {
            isUser = true;
        }
        return isUser;
    }
    /*********************************************************************************
    Method Name    : fetchAdditionaleLetters
    Author         : Deloitte
    Description    : This is a helper method to check type of the user
    Return Type    : void
    Parameter      : Id
    *********************************************************************************/
    public static List < spc_LightningMap > fetchAdditionaleLetters(List < string > consentlst, Set < String > consentAllSet, Case currentCase, String objType, Map < ID, Set < String >> cChannels) {
        List < spc_LightningMap > lstAvailableeLetters = new List < spc_LightningMap > ();
        String query = 'SELECT Id,spc_All_Consents_Mandatory__c,spc_Consent_Eletter_Needed__c,spc_eLetter__c,spc_eLetter__r.Name,spc_Source_Object__c, spc_Object_Record_Types__c FROM spc_Additional_eLetter__c';
        query += ' WHERE  spc_Source_Object__c = \'' + objType + '\' AND spc_Object_Record_Types__c = \'' + currentCase.RecordType.Name + '\'';
        if (!consentlst.isEmpty()) {
            query += ' AND  (spc_Consent_Eletter_Needed__c includes' + consentlst;
            query += 'OR spc_Consent_Eletter_Needed__c=\'' + Label.spc_Default + '\') order by Name';
        } else {
            query += 'AND spc_Consent_Eletter_Needed__c=\'' + Label.spc_Default + '\' order by Name';
        }
        List < spc_Additional_eLetter__c > addEletterLst = spc_Database.queryWithAccess(query, false);
        if (addEletterLst != null && addEletterLst.size() > 0) {
            for (spc_Additional_eLetter__c ad: addEletterLst) {
                if (cChannels.get(ad.spc_eLetter__c) != null) {
                    Set < String > filteredList = cChannels.get(ad.spc_eLetter__c);
                    if (!filteredList.isEmpty()) {
                        boolean hideEmailRow = filteredList.contains(System.Label.spc_Email_Template);
                        boolean hideFaxRow = filteredList.contains(System.Label.spc_Fax_Template);
                        // lstAvailableeLetters.add(new spc_LightningMap(el.Name, el.Id,hideFaxRow,hideEmailRow)); 
                        if (ad.spc_All_Consents_Mandatory__c) {
                            Set < String > eletterConsentSet = new Set < String > ();
                            eletterConsentSet.addAll(ad.spc_Consent_Eletter_Needed__c.split(';'));
                            If(consentAllSet.containsAll(eletterConsentSet)) {
                                lstAvailableeLetters.add(new spc_LightningMap(ad.spc_eLetter__r.Name, ad.spc_eLetter__c, hideFaxRow, hideEmailRow));
                            }

                        } else {
                            lstAvailableeLetters.add(new spc_LightningMap(ad.spc_eLetter__r.Name, ad.spc_eLetter__c, hideFaxRow, hideEmailRow));
                        }
                    }
                }
            }
        }
        // }

        return lstAvailableeLetters;
    }
    /*********************************************************************************
    Method Name    : fetchConsentList
    Author         : Deloitte
    Description    : This is a helper method to fetch the consents available to the Patient
    Return Type    : List<String>
    Parameter      : spc_PatientConsentDetails
    *********************************************************************************/
    public static List < String > fetchConsentList(spc_PatientConsentDetails consentDetails) {
        List < String > consentlst = new List < String > ();
        if ((consentDetails.objCase.Account.spc_Patient_Services_Consent_Received__c != null && consentDetails.objCase.Account.spc_Patient_Services_Consent_Received__c != spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.NOCONSENT))) {
            consentlst.add('\'' + spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.REMSCONSENT) + '\'');
        }
        if ((consentDetails.objCase.Account.spc_HIPAA_Consent_Received__c != null && consentDetails.objCase.Account.spc_HIPAA_Consent_Received__c != spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.NOCONSENT))) {
            consentlst.add('\'' + spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.ServiceConsent) + '\'');
        }
        if ((consentDetails.objCase.Account.spc_Text_Consent__c != null && consentDetails.objCase.Account.spc_Text_Consent__c != spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.NOCONSENT))) {
            consentlst.add('\'' + spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.MarketingTextConsent) + '\'');
        }
        if ((consentDetails.objCase.Account.spc_Services_Text_Consent__c != null && consentDetails.objCase.Account.spc_Services_Text_Consent__c != spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.NOCONSENT) &&
                consentDetails.objCase.Account.spc_Services_Text_Consent__c != spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_VERBAL_CONSENT_RECEIVED))) {
            consentlst.add('\'' + spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.ServiceTextConsent) + '\'');
        }
        if (consentDetails.objCase.Account.spc_Services_Text_Consent__c != null &&
            consentDetails.objCase.Account.spc_Services_Text_Consent__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_VERBAL_CONSENT_RECEIVED)) {
            consentlst.add('\'' + spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_VERBAL_CONSENT_RECEIVED) + '\'');
        }
        if ((consentDetails.objCase.Account.spc_REMS_Text_Consent__c != null && consentDetails.objCase.Account.spc_REMS_Text_Consent__c != spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.NOCONSENT))) {
            consentlst.add('\'' + spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.REMSTextConsent) + '\'');
        }
        if ((consentDetails.objCase.Account.spc_Patient_Mrkt_and_Srvc_consent__c != null && consentDetails.objCase.Account.spc_Patient_Mrkt_and_Srvc_consent__c != spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.NOCONSENT) &&
                consentDetails.objCase.Account.spc_Patient_Mrkt_and_Srvc_consent__c != spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_VERBAL_CONSENT_RECEIVED))) {
            consentlst.add('\'' + spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.MarketingConsent) + '\'');
        }
        if (consentDetails.objCase.Account.spc_Patient_Mrkt_and_Srvc_consent__c != null &&
            consentDetails.objCase.Account.spc_Patient_Mrkt_and_Srvc_consent__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_VERBAL_CONSENT_RECEIVED)) {
            consentlst.add('\'' + spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_VERBAL_CONSENT_RECEIVED) + '\'');
        }
        return consentlst;
    }

    /*********************************************************************************
    Method Name    : addToMap
    Author         : Deloitte
    Description    : This is a helper method to put map values
    Return Type    : void
    Parameter      : Id
    *********************************************************************************/
    public static void addToMap(Map < String, Set < Id >> mapObject, String key, Id value, Set < Id > defaultVal) {
        if (!mapObject.containsKey(key)) {
            mapObject.put(key, defaultVal);
        }
        mapObject.get(key).add(value);

    }

    public static void addToMap(Map < String, Set < String >> mapObject, String key, String value, Set < String > defaultVal) {
        if (!mapObject.containsKey(key)) {
            mapObject.put(key, defaultVal);
        }
        mapObject.get(key).add(value);

    }


    public static void addToMap(Map < string, List < Case >> mapObject, string key, Case value, List < Case > defaultVal) {
        if (!mapObject.containsKey(key)) {
            mapObject.put(key, defaultVal);
        }
        mapObject.get(key).add(value);

    }

    public static String getActualPMRCCode(String pmrcCode) {
        if (!String.isBlank(pmrcCode) && pmrcCode.contains('#')) {
            pmrcCode = pmrcCode.substring(4);
        }
        return pmrcCode;
    }

}
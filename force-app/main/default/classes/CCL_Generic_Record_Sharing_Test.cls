/********************************************************************************************************
*  @author          Deloitte
*  @description     Test class for CCL_Generic_Record_Sharing Model.
*  @param           
*  @date            June 19, 2020
*********************************************************************************************************/
@isTest
public with sharing class CCL_Generic_Record_Sharing_Test {
    @testSetup
    public static void createTestData() {
        
        UserRole customerPortalUserRole = [Select Id, PortalType, PortalAccountId From UserRole where PortalType ='None' LIMIT 1];
        String profileName =CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE;
        User testUser = CCL_TestDataFactory.createTestUser(profileName);
        testUser.UserRoleId = customerPortalUserRole.Id;
        insert testUser;
        
        System.runAs(testUser){
            Account externalUserAcc = CCL_TestDataFactory.createTestParentAccount();
            externalUserAcc.Name = 'External User Account';
            insert externalUserAcc;
            
            Contact portalUserCon = CCL_testDataFactory.createCommunityUser(externalUserAcc);
            insert portalUserCon;
            
            String externalProfileName = CCL_StaticConstants_MRC.EXTERNAL_BASE_PROFILE_TYPE;
            User portalTestUser = CCL_TestDataFactory.createTestUser(externalProfileName);
            portalTestUser.username = 'externalPortalUserName@novartis.com';
            portalTestUser.email = 'externalPortalUser@novartis.com';
            portalTestUser.ContactId = portalUserCon.Id;
            portalTestUser.User_Region__c='US';
            insert portalTestUser;
            System.runAs(portalTestUser){
                 
           CCL_Time_Zone__c newTimeZoneObj = new CCL_Time_Zone__c();
           newTimeZoneObj.Name = 'Test NewTime Zone5';
           newTimeZoneObj.CCL_SAP_Time_Zone_Key__c = System.currentTimeMillis()+'347939-test3';
           newTimeZoneObj.CCL_External_ID__c = '66677358';
           newTimeZoneObj.CCL_Saleforce_Time_Zone_Key__c = System.currentTimeMillis()+'847835-test1';
           insert newTimeZoneObj;
            CCL_Time_Zone__c timeZoneId = [SELECT Id FROM CCL_Time_Zone__c LIMIT 1];
            Account newAccountObj = new Account();
            newAccountObj.Name = 'Hospital 6' ;
            newAccountObj.Type = 'Prospect';
            newAccountObj.CCL_Capability__c = 'L1O';
            newAccountObj.CCL_Active__c=True;
            newAccountObj.CCL_Label_Compliant__c = 'SEC';  
            newAccountObj.CCL_Type__c = CCL_StaticConstants_MRC.ACCOUNT_TYPE_VENDOR;

            newAccountObj.CCL_External_ID__c = '1245767'; 
            newAccountObj.CCL_Time_Zone__c=timeZoneId.id;
            newAccountObj.shippingCountryCode='US';
            
            insert newAccountObj;
             // Account newAccountObj=CCL_TestDataFactory.createTestParentAccount(); 
                if(newAccountObj != null) {
                    //insert testParentAccount;
                    Account accountId = [SELECT Id FROM Account LIMIT 1];
                    List<CCL_Therapy__c> therapyList = new List<CCL_Therapy__c>();
                    CCL_Therapy__c testTherapy=CCL_TestDataFactory.createTestTherapy();
                    therapyList.add(testTherapy);
                    CCL_Therapy__c testAllTherapy=CCL_TestDataFactory.createTestTherapy();
                    testAllTherapy.Name = 'All Therapy';
                    therapyList.add(testAllTherapy);

                    if(testTherapy != null) {
                        insert therapyList;
                        Id allTherapyId;
                        List<CCL_Therapy__c> therapyLst = [SELECT Id,name FROM CCL_Therapy__c];
                        List<CCL_Therapy_Association__c> therapyAssociationList = new List<CCL_Therapy_Association__c>();
                        for(CCL_Therapy__c therapyObj : therapyLst){
                            if(therapyObj.Name != 'All Therapy'){
                                CCL_Therapy_Association__c siteTherapyAssociation = CCL_TestDataFactory.createTestSiteTherapyAssoc(accountId.Id,therapyObj.Id);
                                therapyAssociationList.add(siteTherapyAssociation);
                                CCL_Therapy_Association__c countryTherapyAssociation = CCL_TestDataFactory.createTestCountryTherapyAssoc('Test Hospital',therapyObj.Id);
                                therapyAssociationList.add(countryTherapyAssociation);
                            }else{
                                allTherapyId = therapyObj.Id;
                                CCL_Therapy_Association__c allTherapyCountryTherapyAssociation = CCL_TestDataFactory.createTestCountryTherapyAssoc('Test Hospital',therapyObj.Id);
                                therapyAssociationList.add(allTherapyCountryTherapyAssociation);
                            }
                        }
                        insert therapyAssociationList;
                        
                        CCL_Therapy_Association__c therapyAssociationId = [SELECT Id FROM CCL_Therapy_Association__c WHERE id =:therapyAssociationList[0].Id AND RecordTypeId =:CCL_StaticConstants_MRC.THERAPY_ASSOCIATION_RECORDTYPE_SITE LIMIT 1];
                        CCL_User_Therapy_Association__c userTherapyAssociation = new CCL_User_Therapy_Association__c();
                        userTherapyAssociation.CCL_Therapy_Association__c = therapyAssociationId.Id;
                        userTherapyAssociation.CCL_User__c = portalTestUser.Id;
                        Id devRecordTypeId = Schema.SObjectType.CCL_User_Therapy_Association__c.getRecordTypeInfosByName().get('External').getRecordTypeId();
                        userTherapyAssociation.RecordTypeId = devRecordTypeId;
                        userTherapyAssociation.CCL_Record_Accessability__c = 'Edit';
                        userTherapyAssociation.Name = 'test user3';
                        userTherapyAssociation.CCL_Primary_Role__c ='PRF Submitter';
                        insert userTherapyAssociation;
                        CCL_Order__c prfOrder = new CCL_Order__c();
                        prfOrder.Name = 'TestData3';
                        prfOrder.CCL_Hospital_Patient_ID__c = '12345';
                        prfOrder.CCL_Therapy__c = allTherapyId;
                        prfOrder.CCL_Ordering_Hospital__c = accountId.Id;
                        prfOrder.recordtypeId=CCL_StaticConstants_MRC.ORDER_RECORDTYPE_COMMERCIAL;
                        prfOrder.CCL_Novartis_Batch_ID__c='T99887766';
                        insert prfOrder;
                   
                    }
                }
            }
        }
    }
    
        static testMethod void testShareRecords() {
        CCL_Order__c orderId = [SELECT Id FROM CCL_Order__c where CCL_Hospital_Patient_ID__c = '12345' LIMIT 1];
        User usrRec = [SELECT Id, ProfileId FROM User WHERE Profile.Name = 'External Base Profile'AND isActive = TRUE LIMIT 1];
        //System.runAs(usrRec){
        List<CCL_Invocable_Generic_Record_Sharing.CCL_Sharing_Invocable_Wrapper> listOfRecordDTO = new List<CCL_Invocable_Generic_Record_Sharing.CCL_Sharing_Invocable_Wrapper>();
        CCL_Invocable_Generic_Record_Sharing.CCL_Sharing_Invocable_Wrapper objDTO = new CCL_Invocable_Generic_Record_Sharing.CCL_Sharing_Invocable_Wrapper();  
        
        if(orderId!=null){
        objDTO.treatmentLifeCycleStage = 'PRF Created';
        objDTO.recordId = orderId.Id;
        objDTO.removeShareRecord=false;
        listOfRecordDTO.add(objDTO);
        }
        Test.startTest();
            if(listOfRecordDTO!=null){
        CCL_Invocable_Generic_Record_Sharing.shareRecordsPerObject(listOfRecordDTO);
        Set<Id> currentRecIdSet = new Set<Id>();
        List<CCL_Generic_Record_Sharing_DTO> listOfDTO = new List<CCL_Generic_Record_Sharing_DTO>();
        CCL_Generic_Record_Sharing_DTO recSharingdto = new  CCL_Generic_Record_Sharing_DTO();
        recSharingdto.treatmentLifeCycleStage = 'PRF Created';
        recSharingdto.recordId = orderId.Id;
        listOfDTO.add(recSharingdto);
        currentRecIdSet.add(orderId.Id);
         CCL_Generic_Record_Sharing.shareRecords(listOfDTO);
         CCL_Generic_Record_Sharing.shareRecordsForInternalUser(listOfDTO);
         CCL_Generic_Record_Sharing.removeShareRecordFromCurrentRec(currentRecIdSet,'PRF Created');
            //removeShareRecord = shareingWrapr.removeShareRecord;
            }
        System.assertNotEquals(listOfRecordDTO,null);
        Test.stopTest();
     //}   
 }
    
}
/**
* @author Deloitte
* @date 19-June-2018
*
* @description Controller class for Caregiver Information Enrollment Wizard Page
*/
global with sharing class spc_CaregiverInformationController {
    /*******************************************************************************************************
    * @description This method is used for getting picklist values
    * @return Map<String, List<PicklistEntryWrapper>>
    */
    @AuraEnabled
    public static Map<String, List<PicklistEntryWrapper>> getPicklistEntryMap() {
        Map<String, List<PicklistEntryWrapper>> picklistEntryMap =
            new Map<String, List<PicklistEntryWrapper>>();

        picklistEntryMap.put('gender', getPicklistEntryWrappers(Account.PatientConnect__PC_Gender__c));
        picklistEntryMap.put('commLang', getPicklistEntryWrappers(Account.PatientConnect__PC_Communication_Language__c));
        picklistEntryMap.put('permissionToCall', getPicklistEntryWrappers(Account.spc_Permission_to_Call__c));
        picklistEntryMap.put('permissionToEmail', getPicklistEntryWrappers(Account.spc_Permission_to_Email__c));
        picklistEntryMap.put('permissionFromPatientToSharePHI', getPicklistEntryWrappers(Account.spc_Permission_from_Patient_to_Share_PHI__c));
        picklistEntryMap.put('caregiverRelationToPatient', getPicklistEntryWrappers(Account.PatientConnect__PC_Caregiver_Relationship_to_Patient__c));
        picklistEntryMap.put('caregiverPreferredTimeToContact', getPicklistEntryWrappers(Account.spc_Preferred_Time_to_Contact__c));
        picklistEntryMap.put('country', getPicklistEntryWrappers(PatientConnect__PC_Address__c.PatientConnect__PC_Country__c));
        picklistEntryMap.put('state', getPicklistEntryWrappers(PatientConnect__PC_Address__c.PatientConnect__PC_State__c));
        picklistEntryMap.put('addressType', getPicklistEntryWrappers(PatientConnect__PC_Address__c.PatientConnect__PC_Address_Description__c));
        picklistEntryMap.put('status', getPicklistEntryWrappers(PatientConnect__PC_Address__c.PatientConnect__PC_Status__c));
        picklistEntryMap.put('primary', getPicklistEntryWrappers(PatientConnect__PC_Address__c.PatientConnect__PC_Primary_Address__c));
        return picklistEntryMap;
    }
    /*******************************************************************************************************
    * @description Returns a list of PicklistEntryWrappers for the given field
    *
    */

    private static List<PicklistEntryWrapper> getPicklistEntryWrappers(Schema.SObjectField field) {
        return getPicklistEntryWrappersFordescFields(field.getDescribe());
    }
    /*******************************************************************************************************
    * @description Returns a list of PicklistEntryWrappers for the given describe results of field
    *
    */

    private static List<PicklistEntryWrapper> getPicklistEntryWrappersFordescFields(Schema.DescribeFieldResult descField) {
        List<PicklistEntryWrapper> picklistEntryWrappers  = new List<PicklistEntryWrapper>();
        for (Schema.PicklistEntry picklistEntry : descField.getPicklistValues()) {
            picklistEntryWrappers.add(new PicklistEntryWrapper(picklistEntry.getValue(), picklistEntry.getLabel()));
        }

        return picklistEntryWrappers;
    }
    /*******************************************************************************************************
    * @description Wrapper for Picklist values
    *
    */
    public class PicklistEntryWrapper {
        public PicklistEntryWrapper(String value, String label) {
            this.value = value;
            this.label = label;
        }

        // Somehow gets populated when deserializing a serialized Schema.PicklistEntry.
        public String validFor { get; set; }

        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }
    }
    /*******************************************************************************************************
    * @description This method is used for getting dependent picklist values
    * @return Map<String, List<PicklistEntryWrapper>>
    */
    @AuraEnabled
    public static Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> getDependentOptions() {
        return spc_PicklistFieldManager.picklistFieldMap(PatientConnect__PC_Address__c.PatientConnect__PC_Country__c, PatientConnect__PC_Address__c.PatientConnect__PC_State__c);
    }
    /*******************************************************************************************************
    * @description This method is used for getting caregiver Account details
    * @param   caregiverId Caregiver's Account id
    * @return CaregiverWrapper
    */
    @AuraEnabled
    public static CaregiverWrapper getCaregiverAccountDetails(String caregiverId) {
        CaregiverWrapper caregiver;
        String query = getAccountQueryString();
        //Query for selected id
        query += 'where id= \'' + caregiverId + '\'';
        List<Account> listAccounts = spc_Database.queryWithAccess(query , false);
        if (listAccounts.size() > 0) {
            caregiver = new CaregiverWrapper(listAccounts.get(0));
        }
        return caregiver;
    }
    /*******************************************************************************************************
    * @description This method is used for creating the Account Query
    * @return  accountQuery
    */
    private static string getAccountQueryString() {
        String accountQuery = 'SELECT Id, PatientConnect__PC_First_Name__c, PatientConnect__PC_Last_Name__c, PatientConnect__PC_Date_of_Birth__c, ';
        accountQuery += 'PatientConnect__PC_Gender__c, PatientConnect__PC_Email__c, Phone, spc_Home_Phone__c, spc_Office_Phone__c, PatientConnect__PC_Communication_Language__c,  ';
        accountQuery += 'spc_Preferred_Time_to_Contact__c, PatientConnect__PC_Caregiver_Relationship_to_Patient__c, spc_Permission_to_Call__c, spc_Permission_to_Email__c, ';
        accountQuery += 'spc_Permission_from_Patient_to_Share_PHI__c, spc_Notes__c, PatientConnect__PC_Primary_Address__c, spc_Date_Permission_to_Share_PHI__c FROM Account ';
        return accountQuery;
    }

    /*******************************************************************************************************
    * @description This method is used for creating the address Query
    * @return  addressQuery
    */
    private static string getAddressQueryString() {
        String addressQuery = 'SELECT Id, PatientConnect__PC_Address_1__c, PatientConnect__PC_Address_2__c, PatientConnect__PC_Address_3__c, ';
        addressQuery += 'PatientConnect__PC_Address_Description__c, PatientConnect__PC_City__c, PatientConnect__PC_Country__c, ';
        addressQuery += 'PatientConnect__PC_State__c, PatientConnect__PC_Status__c,PatientConnect__PC_Primary_Address__c, PatientConnect__PC_Zip_Code__c From PatientConnect__PC_Address__c  ';
        return addressQuery;
    }
    /*******************************************************************************************************
    * @description This method is used for fetching the Caregiver's Primary Address
    * @param primaryAddress the ID of the Primary Address
    * @return  PatientConnect__PC_Address__c record
    */
    private static PatientConnect__PC_Address__c getCaregiverAddress(String primaryAddress) {

        String query = getAddressQueryString();
        PatientConnect__PC_Address__c address;
        //Query for selected id
        query += 'where id= \'' + primaryAddress + '\'';
        List<PatientConnect__PC_Address__c> listAddress = spc_Database.queryWithAccess(query , false);
        if (listAddress.size() > 0) {
            address = listAddress.get(0);
        }
        return address;
    }
    /*******************************************************************************************************
    * @description Wrapper for Caregiver Account values
    *
    */

    public class CaregiverWrapper {
        public CaregiverWrapper(Account acc) {
            this.firstName = acc.PatientConnect__PC_First_Name__c;
            this.lastName = acc.PatientConnect__PC_Last_Name__c;
            this.dob = String.valueOf(acc.PatientConnect__PC_Date_of_Birth__c);
            this.gender = acc.PatientConnect__PC_Gender__c;
            this.email = acc.PatientConnect__PC_Email__c;
            this.phone = acc.Phone;
            this.homePhone = acc.spc_Home_Phone__c;
            this.officePhone = acc.spc_Office_Phone__c;
            this.communicationLanguage = acc.PatientConnect__PC_Communication_Language__c;
            this.preferredTimeToContact = acc.spc_Preferred_Time_to_Contact__c;
            this.caregiverRelationshipToPatient = acc.PatientConnect__PC_Caregiver_Relationship_to_Patient__c;
            this.permissionToCall = acc.spc_Permission_to_Call__c;
            this.permissionToEmail = acc.spc_Permission_to_Email__c;
            this.permissionToSharePHI = acc.spc_Permission_from_Patient_to_Share_PHI__c;
            if (acc.spc_Date_Permission_to_Share_PHI__c != NULL) {
                this.permissionToSharePHIDate = String.valueOf(acc.spc_Date_Permission_to_Share_PHI__c);
            }
            this.notes = acc.spc_Notes__c;
            this.primaryAddress = acc.PatientConnect__PC_Primary_Address__c;
            if (String.isNotBlank(primaryAddress)) {
                PatientConnect__PC_Address__c address = getCaregiverAddress(primaryAddress);
                if (null != address) {
                    this.address1 = address.PatientConnect__PC_Address_1__c;
                    this.address2 = address.PatientConnect__PC_Address_2__c;
                    this.address3 = address.PatientConnect__PC_Address_3__c;
                    this.city = address.PatientConnect__PC_City__c;
                    this.state = address.PatientConnect__PC_State__c;
                    this.country = address.PatientConnect__PC_Country__c;
                    this.zipCode = address.PatientConnect__PC_Zip_Code__c;
                    this.addressType = address.PatientConnect__PC_Address_Description__c;
                    this.status = address.PatientConnect__PC_Status__c;
                    this.primary = address.PatientConnect__PC_Primary_Address__c;
                }
            }
        }
        @AuraEnabled
        public String firstName { get; set; }
        @AuraEnabled
        public String lastName { get; set; }
        @AuraEnabled
        public String dob { get; set; }
        @AuraEnabled
        public String gender { get; set; }
        @AuraEnabled
        public String email { get; set; }
        @AuraEnabled
        public String phone { get; set; }
        @AuraEnabled
        public String homePhone { get; set; }
        @AuraEnabled
        public String officePhone { get; set; }
        @AuraEnabled
        public String communicationLanguage { get; set; }
        @AuraEnabled
        public String preferredTimeToContact { get; set; }
        @AuraEnabled
        public String caregiverRelationshipToPatient { get; set; }
        @AuraEnabled
        public String permissionToCall { get; set; }
        @AuraEnabled
        public String permissionToEmail { get; set; }
        @AuraEnabled
        public String permissionToSharePHI { get; set; }
        @AuraEnabled
        public String permissionToSharePHIDate { get; set; }
        @AuraEnabled
        public String notes { get; set; }
        @AuraEnabled
        public String primaryAddress { get; set; }
        @AuraEnabled
        public String address1 { get; set; }
        @AuraEnabled
        public String address2 { get; set; }
        @AuraEnabled
        public String address3 { get; set; }
        @AuraEnabled
        public String city { get; set; }
        @AuraEnabled
        public String zipCode { get; set; }
        @AuraEnabled
        public String state { get; set; }
        @AuraEnabled
        public String country { get; set; }
        @AuraEnabled
        public String addressType { get; set; }
        @AuraEnabled
        public String status { get; set; }
        @AuraEnabled
        public Boolean primary { get; set; }
    }
}
public without sharing class CCL_BatchTriggerHandler {
  public void updateDateFields(List<CCL_Batch__c> batchRecords){
   for(CCL_Batch__c  bacthRecord :batchRecords){
   system.debug('$$$$$Batch' + bacthRecord.CCL_Actual_Batch_Release_Date__c);
       if(bacthRecord.CCL_Actual_Batch_Release_Date__c!= null){
       system.debug('$$$$$Batch' + bacthRecord.CCL_Actual_Batch_Release_Date__c);
          bacthRecord.CCL_Actual_Batch_Release_Date_Text__c =CCL_Utility.getFormattedDate(bacthRecord.CCL_Actual_Batch_Release_Date__c);
          system.debug('$$$$$BatchFormatted' + bacthRecord.CCL_Actual_Batch_Release_Date_Text__c);
       }
       if(bacthRecord.CCL_Expiry_Date__c!=null ){
           bacthRecord.CCL_Expiration_Date_Text__c = CCL_Utility.getFormattedDate(bacthRecord.CCL_Expiry_Date__c);
       }
       if(bacthRecord.CCL_Manufacturing_Date__c!=null){
          bacthRecord.CCL_Manufacturing_Date_Text__c = CCL_Utility.getFormattedDate(bacthRecord.CCL_Manufacturing_Date__c);
       }
   }

  }

    public void updateNumberOfBagsOnShipment(List<CCL_Batch__c> numberOfBags) {
		 Set<Id> fpIds = new  Set<Id>();
		 Set<Id> shipmentIds = new  Set<Id>();
		 List<CCL_Shipment__c> shipmentsToUpdate = new List<CCL_Shipment__c>();
        		 for(CCL_Batch__c batch :numberOfBags){
                     if(batch.RecordTypeId == CCL_StaticConstants_MRC.BATCH_RECORDTYPE_MANUFACTURING) {
                         system.debug('RC' +batch.RecordTypeId );
					 fpIds.add(batch.CCL_Finished_Product__c);
                     }
				 }

				 for(CCL_Finished_Product__c fp:CCL_BatchTriggerAccessor.fetchfinishedProductRecords(fpIds)){
					 shipmentIds.add(fp.CCL_Shipment__c);
				 }
				 List<AggregateResult> aggrs = CCL_BatchTriggerAccessor.aggregateResults(shipmentIds);

                 if(aggrs.size() > 0){
                   for(AggregateResult aggr : aggrs){
                   CCL_Shipment__c ship = new CCL_Shipment__c();
                   ship.Id = (id)aggr.get('CCL_Shipment__c');
                   ship.CCL_Number_of_Bags_Shipped__c =(decimal)aggr.get('batch') ;
                   shipmentsToUpdate.add(ship);
                   }
                   system.debug('LIST BATCH' +shipmentsToUpdate );
                  if(shipmentsToUpdate.size()>0){
                     CCL_BatchTriggerAccessor.updateShipmentRecords(shipmentsToUpdate);
                   }
	             }
}

public void updateNumberOfBagsOnFinishedProducts(List<CCL_Batch__c> batchRecList){
  Set<Id> fpRecIdSet = new  Set<Id>();
  List<CCL_Finished_Product__c> finishedProductList = new List<CCL_Finished_Product__c>();

  try{
  for(CCL_Batch__c batch :batchRecList){
    if(batch.RecordTypeId == CCL_StaticConstants_MRC.BATCH_RECORDTYPE_MANUFACTURING) {
      fpRecIdSet.add(batch.CCL_Finished_Product__c);
    }
  }
  if (Schema.sObjectType.CCL_Batch__c.isAccessible()){
      List<AggregateResult> primaryBatchRecCountPerFP =  [SELECT Count(Id)batch, CCL_Finished_Product__c FROM CCL_Batch__c
                                                    WHERE CCL_Finished_Product__c IN :fpRecIdSet
                                                    GROUP BY CCL_Finished_Product__c];
      for(AggregateResult aggreResult : primaryBatchRecCountPerFP){
        CCL_Finished_Product__c finishedProductRec = new CCL_Finished_Product__c();
        finishedProductRec.Id = (id)aggreResult.get('CCL_Finished_Product__c');
        finishedProductRec.CCL_Number_of_BagsShipped__c = (decimal)aggreResult.get('batch');
        finishedProductList.add(finishedProductRec);
      }
  }


    if(Schema.sObjectType.CCL_Finished_Product__c.isUpdateable() && finishedProductList != null && !finishedProductList.isEmpty()){
          update finishedProductList;
      }
    }catch(Exception ex){
      System.debug('Exception ex &&** '+ex);
    }
  }
}
/*********************************************************************************************************
class Name      : PSP_Prod_And_Service_Triggr_Implement 
Description		: Trigger Handler Class for Product And Services Object
@author		    : Saurabh Tripathi
@date       	: July 10, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi            July 10, 2019          Initial Version
Divya Eduvulapati			November 18,2019	   version1
****************************************************************************************************************/ 
public with Sharing class PSP_Prod_And_Service_Triggr_Implement {
     /**************************************************************************************	
      * @author      : Deloitte
      * @date        : 07/10/2019
      * @Description : Method to check duplicate value in records.
      * @Param       : List of Products
      * @Return      : Void
      ***************************************************************************************/
        public void checkDuplicate (List<Product2> prodList,Map<Id,Product2> prodOldMap) {
            /*Set of Product names*/
            final set<String> prdNameSet = new set<String> ();
            /*Set of Product Ids*/
            final set<String> prdIdSet = new set<String> ();
            /*Set of Product StockKeepingUnit*/
            final set<String> prdSkuSet = new set<String> ();
            /*Set of new Prd Names  */
            final Set<String> newPrdNameSet = new Set<String> ();
            /*Set of new Prd Ids*/
            final Set<String> newPrdIdSet = new Set<String> ();
            /*Set of new Prd StockKeepingUnit*/
            final Set<String> newPrdSkuSet = new Set<String> ();
            //Adding the productSKU, Name and Id values to respective newPrdsets only if their values got updated
            for (Product2 product: prodList) {
                 if(prodOldMap==NULL) {
                 		newPrdNameSet.add (product.Name);
                    	newPrdIdSet.add (product.PSP_ID__c);
                 		newPrdSkuSet.add (product.StockKeepingUnit);
                	} else {                                 
                		checkUpdateName(newPrdNameSet,product,prodOldMap);
                		checkUpdateId(newPrdIdSet,product,prodOldMap);
                		checkUpdateSku(newPrdSkuSet,product,prodOldMap);                      
                }
            }
     	if (Product2.sObjectType.getDescribe().isAccessible()) {
            //Fetching all products and populating sets for productSKU, Name and Id.    
        	for (Product2 product: [Select Id, Name, StockKeepingUnit, PSP_Id__C ,RecordTypeId from Product2 where Name in :newPrdNameSet OR PSP_Id__C in :newPrdIdSet OR StockKeepingUnit in :newPrdSkuSet WITH SECURITY_ENFORCED ]) {
            	
                //check if the new list values match with existing record values
            	if (String.isNotBlank (product.name)) {
                prdNameSet.add (product.name.toLowerCase (PSP_ApexConstants.LOCALE_US)+';'+product.RecordTypeId);                                        
            	}
            	if (String.isNotBlank (product.PSP_ID__c)) {  
                prdIdSet.add (product.PSP_ID__c+';'+product.RecordTypeId);     
            	}
            	if (String.isNotBlank (product.StockKeepingUnit)) {
                prdSkuSet.add (product.StockKeepingUnit.toLowerCase (PSP_ApexConstants.LOCALE_US)+';'+product.RecordTypeId);                                                      
            	}
            
        }
     }  
    	addErrorForDuplicate ( prdNameSet, prdIdSet,prdSkuSet, prodList);
        
        }
    
        /**************************************************************************************	
          * @author      : Deloitte
          * @date        : 19/11/2019
          * @Description : Method to productName values to respective newPrdNameSet only if their values got updated
          * @Param       : product, prodOldMap,newPrdNameSet
          * @Return      : Void
          ***************************************************************************************/
        private void checkUpdateName (set<String> newPrdNameSet, Product2 product,Map<Id,Product2> prodOldMap) {
            
                if (String.isNotBlank (product.name) && prodOldMap!=NULL && prodOldMap.containsKey(product.Id) 
                    && !product.Name.equals(prodOldMap.get(product.Id).Name)) {
                 	newPrdNameSet.add (product.Name);                       
                    } 
            
    }  
        /**************************************************************************************	
          * @author      : Deloitte
          * @date        : 19/11/2019
          * @Description : Method to productId values to respective newPrdIdset only if their values got updated
          * @Param       : product, prodOldMap,newPrdIdSet
          * @Return      : Void
          ***************************************************************************************/
        private void checkUpdateId (set<String> newPrdIdSet, Product2 product,Map<Id,Product2> prodOldMap) {
            
                 if (String.isNotBlank (product.PSP_ID__c) && prodOldMap!=NULL && prodOldMap.containsKey(product.Id) 
                    && !product.PSP_ID__c.equals(prodOldMap.get(product.Id).PSP_ID__c)) {
                 	newPrdIdSet.add (product.PSP_ID__c);                       
                    }
            
    }  
        /**************************************************************************************	
          * @author      : Deloitte
          * @date        : 19/11/2019
          * @Description : Method to productSku values to respective newPrdIdset only if their values got updated
          * @Param       : product, prodOldMap,newPrdSkuSet
          * @Return      : Void
          ***************************************************************************************/
        private void checkUpdateSku (set<String> newPrdSkuSet, Product2 product,Map<Id,Product2> prodOldMap) {
            
                 if (String.isNotBlank (product.StockKeepingUnit) && prodOldMap!=NULL && prodOldMap.containsKey(product.Id) 
                    && !product.StockKeepingUnit.equals(prodOldMap.get(product.Id).StockKeepingUnit)) {
                 	newPrdSkuSet.add (product.StockKeepingUnit);                       
                    }
            
    }  
        /**************************************************************************************	
          * @author      : Deloitte
          * @date        : 07/10/2019
          * @Description : Method to add error for duplicate field values.
          * @Param       : productNameSet, productIdSet,prdSkuSet, prodList
          * @Return      : Void
          ***************************************************************************************/
        private void addErrorForDuplicate (set<String> productNameSet, set<String> productIdSet,set<String> prdSkuSet, List<Product2> prodList ) {
            for (Product2 prod : prodList) {
      
                if (String.isNotBlank (prod.Name) && !productNameSet.isEmpty () 
                    && productNameSet.contains (prod.Name.toLowerCase (PSP_ApexConstants.LOCALE_US)+';'+prod.RecordTypeId)) {
                	prod.name.addError(Label.PSP_Duplicate_Name_Error);
            	}
                if (String.isNotBlank (prod.PSP_ID__c) 
                    && !productIdSet.isEmpty () && productIdSet.contains (prod.PSP_Id__c+';'+prod.RecordTypeId)) {
                	prod.PSP_ID__c.addError (Label.PSP_Duplicate_Id_error);
            	}
                if (String.isNotBlank(prod.StockKeepingUnit) 
                    && !prdSkuSet.isEmpty () && prdSkuSet.contains (prod.StockKeepingUnit.toLowerCase (PSP_ApexConstants.LOCALE_US)+';'+prod.RecordTypeId)) {
                	prod.StockKeepingUnit.addError (Label.PSP_Duplicate_SKU_Error);
            	}
           
        }
    }
}
/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the test class for Program Catalogue LWC
*  @date            08/14/2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Shourya Solipuram          August 14, 2019          Initial Version
****************************************************************************************************************/
@IsTest
public class PSP_Search_Component_Controller_Test {
    
    /* Constant for  cptServiceObj*/
    final static String CPT_SERV_OBJ ='[{"cppId":"a09180000063e0EAAQ","productId":"01t18000002NAvRAAW","careProgProdId":"0bd180000008OXtAAM"}]';
    /* Constant for  serviceToggleObj*/
    final static String SERV_TOG_OBJ='[{"defChecked":false,"productId":"01t18000002M8P6AAK","careProgProdId":"0bd180000008OTXAA2"}]';
    /* Constant for  cppToggleObj*/
    final static String CPP_TOGG_OBJ='[{"actChecked":false,"productId":"01t18000002M8P6AAK","careProgProdId":"0bd180000008OTXAA2"}]';
    @testSetup static void setup() {
        
        final List<PSP_ApexConstantsSetting__c> apexConstansts =  PSP_Test_Setup.setDataforApexConstants();
        Database.insert(apexConstansts);
    }
    @IsTest static void testSearchResults () {
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram ('Test1',null);
        Database.insert (cP1);
        
        final Product2 prod = PSP_Test_Setup.createProduct('Test Product');
        Database.insert (prod);
        
        final Product2 prod1 = PSP_Test_Setup.createProduct('Test New');
        Database.insert (prod1);
        
        final CareProgramProduct cPP = PSP_Test_Setup.createTestCareProgramProduct(cP1,prod);
        Database.insert (cPP);
        
        try {
            PSP_Search_Component_Controller.fetchRecords('Product','New',cP1.Id); 
            PSP_Search_Component_Controller.fetchRecords('Product1','New',cP1.Id); 
        } catch(AuraHandledException e) {
            System.assert( e.getMessage ().contains ('Script-thrown exception'),e.getMessage () );
        }
        
        // PSP_Search_Component_Controller.fetchRecords('Product','New',cP1.Id);
    }
    @IsTest static void testSearchedResults () {
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram ('Test1',null);
        Database.insert (cP1);
        
        final Product2 prod = PSP_Test_Setup.createProduct('Test Product');
        Database.insert (prod);
        
        final Product2 prod1 = PSP_Test_Setup.createProduct('Test New');
        Database.insert (prod1);
        
        final CareProgramProduct cPP = PSP_Test_Setup.createTestCareProgramProduct(cP1,prod);
        Database.insert (cPP);
                
        try {
            PSP_Search_Component_Controller.getCarePlanTemplate('Service',cP1.Id);        
        } catch(AuraHandledException e) {
            System.assert( e.getMessage ().contains ('Script-thrown exception'),e.getMessage () );
        }
        
        // PSP_Search_Component_Controller.fetchRecords('Product','New',cP1.Id);
    }
    @IsTest static void testProductScenarios () {
        PSP_Search_Component_Controller controller= new PSP_Search_Component_Controller();
        final List<Map<String,Object>> draftValues = new List<Map<String,Object>>();
        final Map<String,Object> draftMap = new Map<String,Object>();
        final List<Map<String,String>> iDToIDList = new List<Map<String,String>>();
        final Map<String,String> iDToIDMap = new Map<String,String>();
        final List<Map<String,Object>> infoList = new List<Map<String,Object>>();
        final Map<String, Object> cppInfo = new Map<String, Object>();
        
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram ('Test1',null);
        Database.insert (cP1);
        
        final Product2 prod = PSP_Test_Setup.createProduct('Test Product');
        Database.insert (prod);
        
        final CareProgramProduct cPP = PSP_Test_Setup.createTestCareProgramProduct(cP1,prod);
        Database.insert (cPP);
        system.debug('cPPId' +cPP);
        
        iDToIDMap.put('key',prod.Id);
        iDToIDMap.put('value',cPP.Id);
        iDToIDList.add(iDToIDMap);
        system.debug('iDToIDList' +iDToIDList);
        
        draftMap.put('isActive',false);
        draftMap.put('prodId',prod.Id);
        draftValues.add(draftMap);
        final string jsonstring = JSON.serialize(draftValues);
        
        cppInfo.put('prodId',prod.Id);
        cppInfo.put('Name',prod.Name);
        infoList.add(cppInfo);
        final string infostring = JSON.serialize(infoList);
        
        PSP_Search_Component_Controller.getProductList(cP1.Id);
        PSP_Search_Component_Controller.getServiceList(cP1.Id);
        PSP_Search_Component_Controller.getProduct(cP1.Id);
        PSP_Search_Component_Controller.updateRecords(jsonstring,iDToIDList);
        PSP_Search_Component_Controller.createCPPRecords(infostring,cP1);
        PSP_Search_Component_Controller.updateProduct(CPT_SERV_OBJ,SERV_TOG_OBJ,CPP_TOGG_OBJ);
        System.assert(cppInfo.containsKey('prodId'), 'cppInfo contains key prodId'); 
    }
}
/**
* @author Deloitte
* @date 07/18/2018
*
* @description This is the Test Class for spc_EmailCommunication
*/
@isTest
public class spc_EmailCommunicationTest {
    @future
    private static void setupEmailTemplates() {
        EmailTemplate emailtempOne = new EmailTemplate();
        emailtempOne.Name = 'testOne';
        emailtempOne.DeveloperName = 'testApiOne';
        emailtempOne.FolderId = UserInfo.getUserId();
        emailtempOne.TemplateType = 'text';
        emailtempOne.isActive = true;
        insert emailtempOne;
        EmailTemplate emailtempTwo = new EmailTemplate();
        emailtempTwo.Name = 'testTwo';
        emailtempTwo.DeveloperName = 'testApiTwo';
        emailtempTwo.FolderId = UserInfo.getUserId();
        emailtempTwo.TemplateType = 'text';
        emailtempTwo.isActive = true;
        insert emailtempTwo;


    }

    @testSetup static void setup() {
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

        spc_Communication_framework_Parameters__c emailParams = new spc_Communication_framework_Parameters__c();
        emailParams.Lash_Fax_Number__c = '1234567890';
        emailParams.Org_Wide_Email_Address_Email__c = 'test@test.com';
        emailParams.Org_Wide_Email_Address_Name__c = 'orgwide';
        insert emailParams;

        User usr = [ Select id from User LIMIT 1 ];
        System.RunAs(usr) {
            setupEmailTemplates();
        }

        Account manf = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId());
        insert manf;
        PatientConnect__PC_Engagement_Program__c eg = spc_Test_Setup.createEngagementProgram('test EP', manf.id);
        insert eg;
        Account pharmacy = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Pharmacy').getRecordTypeId());
        pharmacy.PatientConnect__PC_Email__c = 'testpharmacy@email.com';
        pharmacy.fax = '1234567890';
        insert pharmacy;
        Account Physician = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Physician').getRecordTypeId());
        Physician.PatientConnect__PC_Email__c = 'testpharmacy@email.com';
        Physician.fax = '1234567890';
        insert Physician;
        Account patientAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        patientAcc1.PatientConnect__PC_Email__c = 'test@email.com';
        patientAcc1.spc_HIPAA_Consent_Received__c = 'yes';
        patientAcc1.spc_Patient_HIPAA_Consent_Date__c = system.today();
        patientAcc1.spc_Patient_Services_Consent_Received__c = 'yes';
        patientAcc1.spc_Patient_Service_Consent_Date__c = system.today();
        patientAcc1.spc_Text_Consent__c = 'yes';
        patientAcc1.spc_Patient_Text_Consent_Date__c = system.today();
        patientAcc1.spc_Patient_Mrkt_and_Srvc_consent__c = 'yes';
        patientAcc1.spc_Patient_Marketing_Consent_Date__c = system.today();
        insert patientAcc1;
        Case programCaseRec2 = spc_Test_Setup.createCases(new List<Account> {patientAcc1}, 1, 'PC_Program').get(0);
        if (null != programCaseRec2) {
            programCaseRec2.Type = 'Program';
            programCaseRec2.PatientConnect__PC_Engagement_Program__c = eg.id;

            insert programCaseRec2;
            PatientConnect__PC_eLetter__c eLetter = new PatientConnect__PC_eLetter__c();
            eLetter.name = 'Patient Enrollment Information to SOC/SPP Email';
            eLetter.PatientConnect__PC_Template_Name__c = 'testApiOne';
            eLetter.PatientConnect__PC_Target_Recipients__c = 'Treating Physician;HCO;Specialty Pharmacy';
            eLetter.PatientConnect__PC_Source_Object__c = 'Case';
            eLetter.PatientConnect__PC_Communication_Language__c = 'english';
            eLetter.spc_eLetter_Data_Migration_Id__c = '123456';

            insert eLetter;
            PatientConnect__PC_Engagement_Program_Eletter__c engELetter = new PatientConnect__PC_Engagement_Program_Eletter__c();

            engELetter.spc_Channel__c = 'Email';
            engELetter.spc_PMRC_Code__c = 'PMRCF05';
            engELetter.PatientConnect__PC_Engagement_Program__c = eg.id;
            engELetter.PatientConnect__PC_eLetter__c = eLetter.id;
            insert engELetter;

            //eletter 2
            PatientConnect__PC_eLetter__c eLetter1 = new PatientConnect__PC_eLetter__c();
            eLetter1.name = 'Receipt To HCP -- Email';
            eLetter1.PatientConnect__PC_Template_Name__c = 'testApiTwo';
            eLetter1.PatientConnect__PC_Target_Recipients__c = 'Treating Physician;HCO;Specialty Pharmacy';
            eLetter1.PatientConnect__PC_Source_Object__c = 'Case';
            eLetter1.PatientConnect__PC_Communication_Language__c = 'english';
            eLetter1.spc_eLetter_Data_Migration_Id__c = '123457';
            insert eLetter1;
            PatientConnect__PC_Engagement_Program_Eletter__c engELetter1 = new PatientConnect__PC_Engagement_Program_Eletter__c();
            engELetter1.spc_Channel__c = 'Email';
            engELetter1.spc_PMRC_Code__c = 'PMRCF08';
            engELetter1.PatientConnect__PC_Engagement_Program__c = eg.id;
            engELetter1.PatientConnect__PC_eLetter__c = eLetter1.id;
            insert engELetter1;

            PatientConnect__PC_Document__c docOne = new PatientConnect__PC_Document__c();
            insert docOne;


        }


    }
    public static testmethod void testGetPicklistEntryMap() {
        Map<String, PatientConnect__PC_Engagement_Program_Eletter__c> mapeLetter = new Map<String, PatientConnect__PC_Engagement_Program_Eletter__c>();
        PatientConnect__PC_Engagement_Program_Eletter__c eLetter1 = [select id, PatientConnect__PC_eLetter__r.Name, spc_PMRC_Code__c, PatientConnect__PC_eLetter__c, PatientConnect__PC_eLetter__r.PatientConnect__PC_Template_Name__c from PatientConnect__PC_Engagement_Program_Eletter__c  where spc_PMRC_Code__c = 'PMRCF05' LIMIT 1];
        PatientConnect__PC_Engagement_Program_Eletter__c eLetter2 = [select id, PatientConnect__PC_eLetter__r.Name, spc_PMRC_Code__c, PatientConnect__PC_eLetter__c, PatientConnect__PC_eLetter__r.PatientConnect__PC_Template_Name__c from PatientConnect__PC_Engagement_Program_Eletter__c where spc_PMRC_Code__c = 'PMRCF08' LIMIT 1];

        mapeLetter.put('Patient Enrollment Information to SOC/SPP Email|Email', eLetter1);
        mapeLetter.put('spc_Send_Receipt_to_HCP|FAX', eLetter2);
        Account acc = [select id, fax, PatientConnect__PC_Email__c from Account where recordtypeid = :Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId() LIMIT 1];
        Case oCase = [select id, status, PatientConnect__PC_Status_Indicator_3__c from Case];
        oCase.status = 'enrolled';
        oCase.AccountId = acc.Id;
        oCase.PatientConnect__PC_Status_Indicator_3__c = 'complete';
        update oCase;
        List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();
        spc_CommunicationTriggers triggerImplementation = new spc_CommunicationTriggers();
        envelops.add(triggerImplementation.createEnvelop(oCase.Id
                     , null
                     , 'Patient Enrollment Information to SOC/SPP Email'
                     , new Set<String> {spc_ApexConstants.ASSOCIATION_ROLE_TREATING_PHY, spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO), spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_SPHARMACY), 'Patient'}
                     , null
                     , spc_CommunicationMgr.Channel.Email));
        envelops[0].outboundDoc = [SELECT Id FROM PatientConnect__PC_Document__c limit 1];
        Map<String, Account> mapRecipients1 = new Map<String, Account>();
        mapRecipients1.put('Patient', acc);
		Map<Id, List<sObject>> mapIdObject=new Map<Id, List<sObject>>();
        Map<String, Map<String, Account>> mapRecipients = new Map<String, Map<String, Account>>();
        mapRecipients.put(oCase.Id, mapRecipients1);
        Contact con = new Contact();

        con.accountid = acc.id;
        con.lastName = 'LastCon';
        insert con;
        Map<Id, Contact> mapAccountCon = new Map<Id, Contact>();
        mapAccountCon.put(acc.id, con);
        PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c();
        doc.PatientConnect__PC_From_Email_Address__c = Label.spc_testemail_from_address;
        insert doc;
        List<Attachment> attList=new List<Attachment>();
        Attachment attachment = new Attachment();
        attachment.Name = Label.spc_attachment_body;
        attachment.Body = Blob.valueOf(Label.spc_attachment_body);
        attachment.ParentId = doc.Id;
        attachment.ContentType = 'text/plain';
        insert attachment;
        attList.add(attachment);
        mapIdObject.put(eLetter1.id,attList);
        Test.startTest();
        spc_EmailCommunication spcEmail=new spc_EmailCommunication();
        spcEmail.send(envelops,mapeLetter,mapRecipients,mapAccountCon,mapIdObject);
        Test.stopTest();

    }
}
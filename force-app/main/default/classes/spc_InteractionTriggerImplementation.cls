/**
* @author Deloitte
* @date 6-6-2018
*
* @description This class is the implementation class for Interaction Trigger
*/


public class spc_InteractionTriggerImplementation {

    /*******************************************************************************************************
    * @description This method is used to Insert Document, Document Log, Attachment
    * @param List<PatientConnect__PC_Interaction__c>
    */

    public void createDocument(List<PatientConnect__PC_Interaction__c> newList) {
        Map<Id, PatientConnect__PC_Document__c> mapInteractiontoDoc = new Map<Id, PatientConnect__PC_Document__c>();
        List<PatientConnect__PC_Document_Log__c> docLogList = new List<PatientConnect__PC_Document_Log__c>();
        Set<Id> programCaseIds = new Set<Id>();
        List<Attachment> newAttments = new List<Attachment>();

        for (PatientConnect__PC_Interaction__c rec : newList) {
            programCaseIds.add(rec.PatientConnect__PC_Patient_Program__c);
            PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c();
            doc.PatientConnect__PC_Document_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_STATUS_READYTOSEND);
            doc.RecordTypeId = Schema.getGlobalDescribe().get(spc_ApexConstants.DOCUMENT_OBJECT_API).getDescribe().getRecordTypeInfosByName().get(spc_ApexConstants.DOCUMENT_TYPE_FAX_OUTBOUND).getRecordTypeId();
            doc.spc_Interaction__c = rec.id;
            mapInteractiontoDoc.put(rec.Id, doc);
        }

        try {
            if (!mapInteractiontoDoc.values().isempty()) {
                spc_Database.ins(mapInteractiontoDoc.values());
            }
            docLogList = createDocumentLog(mapInteractiontoDoc);
            if (!docLogList.isempty()) {
                spc_Database.ins(docLogList);
            }
            newAttments = attachmentsRec(programCaseIds, newList, mapInteractiontoDoc);
            if (!newAttments.isempty()) {
                spc_Database.ins(newAttments);
            }
        } catch (Exception e) {
            spc_Utility.logAndThrowException(e);
        }
    }

    /*******************************************************************************************************
    * @description This method is used to create Document Log records
    * @param Map<Id, PatientConnect__PC_Document__c>
    * @return List<PatientConnect__PC_Document_Log__c>
    */

    public static List<PatientConnect__PC_Document_Log__c> createDocumentLog(Map<Id, PatientConnect__PC_Document__c> mapInteractionWithDocument) {
        PatientConnect__PC_Document_Log__c docLogRec;
        List<PatientConnect__PC_Document_Log__c> newdocLogList = new List<PatientConnect__PC_Document_Log__c>();
        for (Id recrd : mapInteractionWithDocument.keySet()) {
            docLogRec = new PatientConnect__PC_Document_Log__c();
            docLogRec.PatientConnect__PC_Document__c = mapInteractionWithDocument.get(recrd).Id;
            docLogRec.PatientConnect__PC_Interaction__c = recrd;
            newdocLogList.add(docLogRec);
        }
        return newdocLogList;
    }

    /*******************************************************************************************************
    * @description This method is used to create Attachments
    * @param Set<Id>, List<PatientConnect__PC_Interaction__c>, Map<Id, PatientConnect__PC_Document__c>
    * @return List<Attachment>
    */

    Public static List<Attachment> attachmentsRec(Set<Id> caseIds, List<PatientConnect__PC_Interaction__c> intractionRecs, Map<Id, PatientConnect__PC_Document__c> mapInteractionWithDocument) {
        List<Attachment> attToInsert = new List<Attachment>();

        Map<Id, Case> mapcaseIdtoCase = new Map<Id, Case>([SELECT ID, AccountId From Case WHERE ID IN: caseIds]);
        List<Id> accIds = new List<Id>();
        for (Id cIds : mapcaseIdtoCase.Keyset()) {
            accIds.add(mapcaseIdtoCase.get(cIds).AccountId);
        }
        Map<Id, Account> accIdtoAcc = new Map<Id, Account>();
        List<PatientConnect__PC_Address__c> addressRec = new List<PatientConnect__PC_Address__c>();
        if (!accIds.isempty()) {
            accIdtoAcc = new Map<Id, Account>([SELECT ID, Phone, PatientConnect__PC_Email__c, PatientConnect__PC_Last_Name__c, PatientConnect__PC_First_Name__c
                                               FROM ACCOUNT
                                               WHERE ID IN: accIds]);
            addressRec = [SELECT ID, PatientConnect__PC_Address_1__c, PatientConnect__PC_Address_2__c, PatientConnect__PC_Address_3__c,
                          PatientConnect__PC_City__c, PatientConnect__PC_State__c, PatientConnect__PC_Zip_Code__c, PatientConnect__PC_Account__c FROM PatientConnect__PC_Address__c WHERE PatientConnect__PC_Account__c IN: accIds];
        }

        Map<ID, Account> caseIdWithAccount = New Map<Id, Account>();
        for (Account act : accIdtoAcc.values()) {
            for (Id cIds : mapcaseIdtoCase.Keyset()) {
                if (mapcaseIdtoCase.get(cIds).AccountId == act.Id) {
                    caseIdWithAccount.put(cIds, act);
                }
            }
        }

        Map<Id, PatientConnect__PC_Address__c> accountIdWithAddressMap = new Map<Id, PatientConnect__PC_Address__c>();
        for (PatientConnect__PC_Address__c add : addressRec) {
            accountIdWithAddressMap.put(add.PatientConnect__PC_Account__c, add);
        }

        List<Attachment> attc = new List<Attachment>();
        for (PatientConnect__PC_Interaction__c rec : intractionRecs) {
            Account acnt = new Account();
            PatientConnect__PC_Address__c address = new PatientConnect__PC_Address__c();
            if (caseIdWithAccount.containsKey(rec.PatientConnect__PC_Patient_Program__c)) {
                acnt = caseIdWithAccount.get(rec.PatientConnect__PC_Patient_Program__c);
                address = accountIdWithAddressMap.get(acnt.Id);

            }
            String s = Label.spc_BrexanaloneFaxDocBody;
            s = s + acnt.PatientConnect__PC_First_Name__c + ' ' + acnt.PatientConnect__PC_Last_Name__c + '\n';
            if (address != null) {
                s = s + address.PatientConnect__PC_Address_1__c + ',' + address.PatientConnect__PC_Address_2__c + '\n';
                s = s + address.PatientConnect__PC_Address_3__c + '\n';
                s = s + address.PatientConnect__PC_City__c + ',' + address.PatientConnect__PC_State__c + ',' + address.PatientConnect__PC_Zip_Code__c + '\n';
            }
            s = s + acnt.Phone + '\n';
            s = s + acnt.PatientConnect__PC_Email__c;

            Attachment attachment = new Attachment ();
            attachment.ParentId = mapInteractionWithDocument.get(rec.Id).Id;
            attachment.Body = Blob.valueOf(s);
            attachment.Name = Label.spc_BrexanaloneFaxDocName;
            attachment.ContentType = spc_ApexConstants.Doc_ContantType;
            attToInsert.add(attachment);
        }
        return attToInsert;
    }

    /*******************************************************************************************************
    * @description It is used to create Evaluate Coverage tasks for interactions for which Reason for Status is Coverage Denied or PA Denied.
    * @param Set<PatientConnect__PC_Interaction__c>
    * @return void
    */

    public void createEvalCovTask(Set<PatientConnect__PC_Interaction__c> lstTaskSubEvaluateCover) {
        List<PatientConnect__PC_Interaction__c> listInteractions =  [SELECT Id, PatientConnect__PC_Patient_Program__c
                , PatientConnect__PC_Patient_Program__r.OwnerId
                FROM PatientConnect__PC_Interaction__c WHERE Id in :lstTaskSubEvaluateCover];
        List<spc_TaskProcessManager.TaskWrapper> taskWrappers = new List<spc_TaskProcessManager.TaskWrapper>();
        spc_TaskProcessManager.TaskWrapper taskWrapper;
        for (PatientConnect__PC_Interaction__c intr : listInteractions) {
            taskWrapper = new spc_TaskProcessManager.TaskWrapper();
            taskWrapper.ownerId = intr.PatientConnect__PC_Patient_Program__r.OwnerId;
            taskWrapper.relatedTo = intr.PatientConnect__PC_Patient_Program__c;
            taskWrapper.Subject = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_CAT_EVALUATE_COVERAGE);
            taskWrapper.category = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CAT_COVERAGE);
            taskWrapper.subcategory = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_CAT_EVALUATE_COVERAGE);
            taskWrapper.Priority = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_PRIORITY_NORMAL);
            taskWrapper.Status = spc_ApexConstants.getValue(spc_ApexConstants.PickListValue.TASK_STATUS_NOT_STARTED);
            taskWrapper.dueDate = Date.Today();
            taskWrapper.direction = spc_ApexConstants.getValue(spc_ApexConstants.PickListValue.TASK_DIR_OUTBOUND);
            taskWrapper.pcProgram = intr.PatientConnect__PC_Patient_Program__c;
            taskWrapper.interactionId = intr.id;
            taskWrappers.add(taskWrapper);

        }
        spc_TaskProcessManager.checkExistingTaskAndCreateNew(taskWrappers);
    }

    /*******************************************************************************************************
    * @description It is used to create Call SOC/HIP tasks for interactions for which Reason for Status is Medication Not Started or Patient Deceased.
    * @param Set<PatientConnect__PC_Interaction__c>
    * @return void
    */

    public void createCallSOCHCPTask(Set<PatientConnect__PC_Interaction__c> lstTaskMedicationDeceased) {
        List<PatientConnect__PC_Interaction__c> listInteractions =  [SELECT Id, PatientConnect__PC_Patient_Program__c
                , PatientConnect__PC_Patient_Program__r.OwnerId
                FROM PatientConnect__PC_Interaction__c WHERE Id in :lstTaskMedicationDeceased];
        List<spc_TaskProcessManager.TaskWrapper> taskWrappers = new List<spc_TaskProcessManager.TaskWrapper>();
        spc_TaskProcessManager.TaskWrapper taskWrapper;
        for (PatientConnect__PC_Interaction__c intr : listInteractions) {
            taskWrapper = new spc_TaskProcessManager.TaskWrapper();
            taskWrapper.ownerId = intr.PatientConnect__PC_Patient_Program__r.OwnerId;
            taskWrapper.relatedTo = intr.PatientConnect__PC_Patient_Program__c;
            taskWrapper.Subject = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_CALL_HCP_SOC);
            taskWrapper.channel = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CHANNEL_CALL);
            taskWrapper.category = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CAT_SITE_OF_CARE);
            taskWrapper.subcategory = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_CAT_CONTACT_SITE_OF_CARE);
            taskWrapper.Priority = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_PRIORITY_NORMAL);
            taskWrapper.Status = spc_ApexConstants.getValue(spc_ApexConstants.PickListValue.TASK_STATUS_NOT_STARTED);
            taskWrapper.dueDate = Date.Today();
            taskWrapper.direction = spc_ApexConstants.getValue(spc_ApexConstants.PickListValue.TASK_DIR_OUTBOUND);
            taskWrapper.pcProgram = intr.PatientConnect__PC_Patient_Program__c;
            taskWrapper.interactionId = intr.id;
            taskWrappers.add(taskWrapper);

        }
        spc_TaskProcessManager.checkExistingTaskAndCreateNew(taskWrappers);
    }

    /*******************************************************************************************************
    * @description This method is used to show an error if some other interaction on the case has active SOC
    * @param List<PatientConnect__PC_Interaction__c>
    * @return void
    */

    public void checkSOC(List<PatientConnect__PC_Interaction__c> lstNewInteractions) {
        List<Id> lstProgramCaseIds = new List<Id>();
        List<PatientConnect__PC_Association__c> lstSOCAssociations = new List<PatientConnect__PC_Association__c>();
        Map<Id, Id> mapCaseToSOCAssociationAccount = new Map<Id, Id>();
        List<PatientConnect__PC_Interaction__c> lstInteractions = new List<PatientConnect__PC_Interaction__c>();
        Map<Id, List<PatientConnect__PC_Interaction__c>> mapCaseToInteractions = new Map<Id, List<PatientConnect__PC_Interaction__c>>();

        for (PatientConnect__PC_Interaction__c interaction : lstNewInteractions) {
            lstProgramCaseIds.add(interaction.PatientConnect__PC_Patient_Program__c);
        }

        lstSOCAssociations = [SELECT Id, PatientConnect__PC_Program__c, PatientConnect__PC_Account__c
                              FROM PatientConnect__PC_Association__c
                              WHERE PatientConnect__PC_Role__c = : spc_ApexConstants.ASSOCIATION_ROLE_HCO AND PatientConnect__PC_AssociationStatus__c = : spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE) AND PatientConnect__PC_Program__c IN: lstProgramCaseIds];
        if (!lstSOCAssociations.isEmpty()) {
            for (PatientConnect__PC_Association__c association : lstSOCAssociations) {
                mapCaseToSOCAssociationAccount.put(association.PatientConnect__PC_Program__c, association.PatientConnect__PC_Account__c);
            }
        }

        lstInteractions = [SELECT Id, Name, PatientConnect__PC_Patient_Program__c, PatientConnect__PC_Participant__c
                           FROM PatientConnect__PC_Interaction__c
                           WHERE PatientConnect__PC_Patient_Program__c IN :lstProgramCaseIds];
        if (!lstInteractions.isEmpty()) {
            for (PatientConnect__PC_Interaction__c interaction : lstInteractions) {
                List<PatientConnect__PC_Interaction__c> interactions;
                if (mapCaseToInteractions.containsKey(interaction.PatientConnect__PC_Patient_Program__c)) {
                    interactions = mapCaseToInteractions.get(interaction.PatientConnect__PC_Patient_Program__c);
                } else {
                    interactions = new List<PatientConnect__PC_Interaction__c>();
                }
                interactions.add(interaction);
                mapCaseToInteractions.put(interaction.PatientConnect__PC_Patient_Program__c, interactions);
            }
        }

        for (PatientConnect__PC_Interaction__c interaction : lstNewInteractions) {
            Id socAccountId = mapCaseToSOCAssociationAccount.get(interaction.PatientConnect__PC_Patient_Program__c);
            List<PatientConnect__PC_Interaction__c> otherInteractions = mapCaseToInteractions.get(interaction.PatientConnect__PC_Patient_Program__c);

            //error will be shown when the interaction is populated with active care team SOC whereas some other interaction has active care team SOC
            if (otherInteractions != null && !otherInteractions.isEmpty()) {
                for (PatientConnect__PC_Interaction__c otherInt : otherInteractions) {
                    if (interaction.PatientConnect__PC_Participant__c == socAccountId && otherInt.PatientConnect__PC_Participant__c == socAccountId) {
                        interaction.addError(System.Label.spc_PopulatingSOCOnIntercationError + ' ' + otherInt.Name + ' ' + System.Label.spc_PopulatingSOCOnIntercationError1);
                    }
                }
            }

            //The flag will be active only if the interaction is populated with active care team SOC and not for any other SOC
            if (interaction.PatientConnect__PC_Participant__c == socAccountId) {
                interaction.spc_Has_Active_SOC__c = true;
            }

        }
    }

    /*******************************************************************************************************
    * @description This method is used to create Call SOC to Confirm Infusion tasks
    * @param List<PatientConnect__PC_Interaction__c>
    * @return void
    */

    public void createCallSOCToConfirmInfusionTask(List<PatientConnect__PC_Interaction__c> lstScheduledDateInteractions) {

        List<PatientConnect__PC_Interaction__c> lstInteractions = new List<PatientConnect__PC_Interaction__c>();
        Set<Id> lstInteractionsWithOpenTasks = new Set<Id>();

        lstInteractions = [SELECT PatientConnect__PC_Patient_Program__c, PatientConnect__PC_Patient_Program__r.OwnerId FROM PatientConnect__PC_Interaction__c WHERE Id IN :lstScheduledDateInteractions];

        String subject = System.Label.spc_Call_SOC_to_Confirm_Infusion_Start;
        List<String> lstStatus = new List<String> {'Completed', 'Not Needed'};
        List<Task> openTasks = [SELECT spc_Interaction__c
                                FROM Task
                                WHERE spc_Interaction__c IN :lstInteractions AND
                                Subject = :subject AND
                                          Status NOT IN :lstStatus];
        for (Task t : openTasks) {
            lstInteractionsWithOpenTasks.add(t.spc_Interaction__c);
        }

        List<Task> lstTasksToBeCreated = new List<Task>();
        for (PatientConnect__PC_Interaction__c interaction : lstInteractions) {
            if (! lstInteractionsWithOpenTasks.contains(interaction.Id)) {
                Task newTask = new Task();
                newTask.Subject = System.Label.spc_Call_SOC_to_Confirm_Infusion_Start;
                newTask.PatientConnect__PC_Category__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CAT_SITE_OF_CARE);
                newTask.PatientConnect__PC_Sub_Category__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_CONFIRM_INFUSION);
                newTask.Priority = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_PRIORITY_NORMAL);
                newTask.OwnerId = interaction.PatientConnect__PC_Patient_Program__r.OwnerId;
                newTask.WhatId = interaction.PatientConnect__PC_Patient_Program__c;
                newTask.ActivityDate = System.today();
                newTask.Status = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_NOT_STARTED);
                newTask.PatientConnect__PC_Direction__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_DIR_OUTBOUND);
                newTask.PatientConnect__PC_Channel__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CHANNEL_PHONE);
                newTask.PatientConnect__PC_Program__c = interaction.PatientConnect__PC_Patient_Program__c;
                newTask.spc_Interaction__c = interaction.Id;
                newTask.PatientConnect__PC_Assigned_To__c = interaction.PatientConnect__PC_Patient_Program__r.OwnerId;
                lstTasksToBeCreated.add(newTask);
            }
        }

        if (!lstTasksToBeCreated.isEmpty()) {
            spc_Database.ins(lstTasksToBeCreated);
        }
    }

}

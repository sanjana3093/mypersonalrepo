/*********************************************************************************************************
class Name      : PSP_Utility
Description     : Utility Implementation Class 
@author         : Shourya Solipuram 
@date           : July 10, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Shourya Solipuram           July 10, 2019          Initial Version
****************************************************************************************************************/

public with Sharing class PSP_Utility {
    /*private constructor */
    @TestVisible 
    private PSP_Utility () {
        //Private Constructor
    }
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 07/10/2019
    * @Description : Method to check if a field is unique.
    * @Param       : Name of Object
    * @Param       : Name of field
    * @Param       : Error Message
    * @Param       : List of values
    * @Param         : List<sObject> records
    * @Return      : Void
    ***************************************************************************************/
    public static void checkUnique (String sObjectName, String fieldName, String errMsg,List<String> value,List<sObject> records,String recordtype) {
        final List<String> uniqueValues = new List<String>();
      
        if (String.isNotBlank (recordtype) ) {
            
            for (sObject rec : Database.query ('Select '+ String.escapeSingleQuotes(fieldName) +' from '+ String.escapeSingleQuotes (sObjectName) +' where '+ String.escapeSingleQuotes (fieldName) +' IN: value AND recordtypeId =:recordtype WITH SECURITY_ENFORCED')) {
                uniqueValues.add (String.Valueof (rec.get (fieldName)).toLowerCase ());    
            }  
        } else {
             
            for(sObject rec : Database.query ('Select '+ String.escapeSingleQuotes (fieldName) +' from '+ String.escapeSingleQuotes (sObjectName) +' where '+ String.escapeSingleQuotes (fieldName) +' IN: value WITH SECURITY_ENFORCED')) {
                uniqueValues.add (String.Valueof (rec.get(fieldName)).toLowerCase ());    
            } 
        }
        if (!uniqueValues.isEmpty ()) {

            for (sObject rec : records) {
                if (uniqueValues.contains (String.Valueof (rec.get(fieldName)).toLowerCase (PSP_ApexConstants.LOCALE_US))) {
                    rec.addError (errMsg);
                }
            }
        }
    } 
}
/**
 * @description       : Unit Test for CCL_FPShipmentController
 * @author            : Deloitte
 * @group             : 
 * @last modified on  : 05-10-2020
 * @last modified by  : Deloitte
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   08-30-2020   Deloitte                              Initial Version    
 * 1.2   08-Dec-2020  Deloitte                              Update the unit test method around checkUserPermission method.      
**/
@IsTest(SeeAllData=false)
public with sharing class CCL_FPShipmentControllerTest {

    @TestSetup
    static void createTestData() {
        //final String randomNum= String.valueOf(Math.abs(Crypto.getRandomInteger()));
        final String profileName = CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE;
        final User sysAdminUser = CCL_TestDataFactory.createTestUser(profileName);
    
        final UserRole nvsRoleId = [SELECT Id, DeveloperName 
                                        FROM UserRole 
                                        WHERE DeveloperName =:CCL_StaticConstants_MRC.ROLE_NOVARTIS_ADMIN 
                                        LIMIT 1];
    
    
        sysAdminUser.UserRoleId = nvsRoleId.Id;
    
        insert sysAdminUser;
    
        System.runAs(sysAdminUser) {
          final Account testAccountSite = CCL_TestDataFactory.createTestParentAccount();
          testAccountSite.Name = CCL_Notifications_Constants.TESTSITEACCOUNT_VALUE;
          testAccountSite.OwnerId = sysAdminUser.Id;
          insert testAccountSite;
        
        // User / Contact Creation within creating test data ***********

        // FP Receiver User Creation
        Contact portalFPReceiverUserCon = new Contact();
        portalFPReceiverUserCon = CCL_TestDataFactory.createCommunityUser(testAccountSite );
        insert portalFPReceiverUserCon;
        
        final User communityFPReceiverUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.EXTERNAL_BASE_PROFILE_TYPE);
        communityFPReceiverUser.ContactId = portalFPReceiverUserCon.Id;
        insert communityFPReceiverUser;
        final PermissionSetAssignment fpReceiverAssignment = CCL_TestDataFactory.createPermissionSetForAssignment(CCL_StaticConstants_MRC.PERMISSION_SET_FP_RECEIVER, communityFPReceiverUser);
        insert fpReceiverAssignment;

        // AccountManager ( Must confirm About Account User being a valid Contact)
        Contact portalAccountManagerUserCon = new Contact();
        portalAccountManagerUserCon = CCL_TestDataFactory.createCommunityUser( testAccountSite );
        insert portalAccountManagerUserCon;

        final User communityAccountManagerUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.EXTERNAL_BASE_PROFILE_TYPE);
        communityAccountManagerUser.ContactId = portalAccountManagerUserCon.Id;
        insert communityAccountManagerUser;
        final PermissionSetAssignment accountManagerAssignment = CCL_TestDataFactory.createPermissionSetForAssignment(CCL_StaticConstants_MRC.PERMISSION_SET_ACCOUNT_MANAGER, communityAccountManagerUser);
        insert accountManagerAssignment;

        //InfusionReporter
        Contact portalInfusionReporterUserCon = new Contact();
        portalInfusionReporterUserCon = CCL_TestDataFactory.createCommunityUser(testAccountSite );
        insert portalInfusionReporterUserCon;

        final User communityInfusionReporterUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.EXTERNAL_BASE_PROFILE_TYPE);
        communityInfusionReporterUser.ContactId = portalInfusionReporterUserCon.Id;
        insert communityInfusionReporterUser;
        final PermissionSetAssignment infusionReporterAssignment = CCL_TestDataFactory.createPermissionSetForAssignment(CCL_StaticConstants_MRC.PERMISSION_SET_INFUSION_REPORTER, communityInfusionReporterUser);
        insert infusionReporterAssignment;

        // Create Internal Outbound Logistics user
        final User internalOutBoundLogisticUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.INTERNAL_BASE_PROFILE_TYPE);
        insert internalOutBoundLogisticUser;

        final PermissionSetAssignment outBoundLogisticAssignment = CCL_TestDataFactory.createPermissionSetForAssignment(CCL_StaticConstants_MRC.PERMISSION_SET_OUTBOUND_LOGISTICS, internalOutBoundLogisticUser);
        insert outBoundLogisticAssignment;


        // Creation of Internal Outbound Logistics user with PermissionSet

        Contact portalUserWithOutPermissionSet = new Contact();
        portalUserWithOutPermissionSet = CCL_TestDataFactory.createCommunityUser(testAccountSite );
        insert portalUserWithOutPermissionSet;

        final User externalUserWithOutPermissionSet = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.EXTERNAL_BASE_PROFILE_TYPE);
        externalUserWithOutPermissionSet.ContactId = portalUserWithOutPermissionSet.Id;
        externalUserWithOutPermissionSet.LastName = 'UserWithOutPermissionSet';
        insert externalUserWithOutPermissionSet;
        
        // Record Creation Section of DataSetup  ****************


        //ist<CCL_Shipment__c> shipments = new List<CCL_Shipment__c> ();
        // Create Order to Support creation of Shipment 
        final CCL_Order__c testOrder = CCL_TestDataFactory.createTestPRFOrder();
        /* testOrder.CCL_Novartis_Batch_ID__c =  CCL_StaticConstants_MRC.TEST_DATA_TEN_CHARS;
        update testOrder; */
        // Create Commerical Therapy to support Creation Shipment 
        final CCL_Therapy__c testCommercialTherapy =   CCL_TestDataFactory.createTestTherapy();
         insert testCommercialTherapy; 
        
         System.debug('::CCL_FPShipmentController ::createSetupData ::testCommercialTherapy ' + testCommercialTherapy );
         // Initialize Shipment and prepare to perform Insert
         
         
         //CCL_Shipment__c fpTestShipment = CCL_TestDataFactory.createTestFinishedProductShipment(testOrder.Id, testCommercialTherapy.Id );
         //System.debug('::CCL_FPShipmentController ::createSetupData ::fpTestShipment ' + fpTestShipment );
         //fpTestShipment.Name = CCL_StaticConstants_MRC.TEST_DATA_TEN_CHARS;
         //fpTestShipment.CCL_Status__c = 'Product Shipped';
         
         final CCL_Shipment__c fpTestShipment = new CCL_Shipment__c();
        //final String fpRecordType = Schema.SObjectType.CCL_Shipment__c.getRecordTypeInfosByDeveloperName().get('CCL_Finished_Product').getRecordTypeId();
         fpTestShipment.RecordTypeId=CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP;
         fpTestShipment.Name = CCL_StaticConstants_MRC.TEST_DATA_TEN_CHARS;
         fpTestShipment.CCL_Order__c=testOrder.Id;
         fpTestShipment.CCL_Indication_Clinical_Trial__c= testCommercialTherapy.Id; 
         fpTestShipment.CCL_Hospital_Purchase_Order_Number__c= '1239099090';   

            
         
         insert fpTestShipment;
         System.debug('::CCL_FPShipmentController ::createSetupData ::fpTestShipment xx ' + fpTestShipment );

            
            final ContentVersion contentVersion = new ContentVersion(
            Title = 'test',
            PathOnClient = 'test.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
            );
            insert contentVersion;    
            final List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
            final ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = fpTestShipment.id;
            cdl.ContentDocumentId = documents[0].Id;
            cdl.shareType = 'V';
            insert cdl;
        
        }
        
    }

    static testMethod void shipmentToBeUpdatedPositiveTest() {
        
        final List<CCL_Shipment__c> retrieveFPShipments = [SELECT Id, CCL_Hospital_Purchase_Order_Number__c,  RecordTypeId, CCL_Order__r.CCL_Novartis_Batch_ID__c, Name
                                                    From CCL_Shipment__c
                                                    WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP
                                                    AND CCL_Hospital_Purchase_Order_Number__c= '1239099090'
                                                    LIMIT 1 ];

        final List<PermissionSetAssignment> listFpReceivers  = [SELECT Id, PermissionSetId, PermissionSet.Name, PermissionSet.ProfileId, PermissionSet.Profile.Name, 
                                                            AssigneeId, Assignee.Name 
                                                            FROM PermissionSetAssignment 
                                                            WHERE PermissionSet.Name = :CCL_StaticConstants_MRC.PERMISSION_SET_OUTBOUND_LOGISTICS
                                                            ];
        final String assignName = listFpReceivers[0].AssigneeId;

        final User outBoundLogisticUser = [SELECT Id, Name, Profile.Name, IsActive
                                    FROM User 
                                    WHERE Profile.Name = :CCL_StaticConstants_MRC.INTERNAL_BASE_PROFILE_TYPE
                                    AND IsActive = :CCL_StaticConstants_MRC.VALUE_TRUE
                                    LIMIT 1];
        System.debug('::CCL_FPShipmentController ::shipmentToBeUpdatedPositiveTest ::outBoundLogisticUser ' + outBoundLogisticUser );
        //final String strJSON =   '{\"CCL_Status__c\":\"Shipment Cancelled\",\"Id\":\"'+retrieveFPShipments[0].Id+'\",\"CCL_Shipment_Cancellation_Reason__c\":\"Logistics\"}';    
        Map<String,String> jsonTest = new Map<String,String>();
        jsonTest.put('Id', retrieveFPShipments[0].Id);
        jsonTest.put('CCL_Status__c', 'Shipment Cancelled');
        jsonTest.put('CCL_Shipment_Cancellation_Reason__c', 'Logistics');
        Boolean confirmShipmentUpdate;
        Test.startTest();
        //System.runAs(outBoundLogisticUser) {
            confirmShipmentUpdate = CCL_FPShipmentController.shipmentToBeUpdated(jsonTest);
        //}
        Test.stopTest();
        System.assert(CCL_StaticConstants_MRC.VALUE_TRUE, 'Shipment Record was not found ' + confirmShipmentUpdate);
    }

    static testMethod void shipmentToBeUpdatedExceptionHandlingTest() {
                
         final List<CCL_Shipment__c> retrieveFPShipments = [SELECT Id, CCL_Hospital_Purchase_Order_Number__c,  RecordTypeId, CCL_Order__r.CCL_Novartis_Batch_ID__c, Name
                                                    From CCL_Shipment__c
                                                    WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP
                                                    AND CCL_Hospital_Purchase_Order_Number__c= '1239099090'
                                                    LIMIT 1 ];

        final List<PermissionSetAssignment> listFpReceivers  = [SELECT Id, PermissionSetId, PermissionSet.Name, PermissionSet.ProfileId, PermissionSet.Profile.Name, 
                                                    AssigneeId, Assignee.Name 
                                                    FROM PermissionSetAssignment 
                                                    WHERE PermissionSet.Name = :CCL_StaticConstants_MRC.PERMISSION_SET_FP_RECEIVER
                                                    ];
        final String assignName = listFpReceivers[0].AssigneeId;

        final User fpReceiver = [SELECT Id, Name, Profile.Name, IsActive
                                    FROM User 
                                    WHERE Profile.Name = :CCL_StaticConstants_MRC.INTERNAL_BASE_PROFILE_TYPE
                                    AND IsActive = :CCL_StaticConstants_MRC.VALUE_TRUE
                                    LIMIT 1];

        //final String strJSON =   '{\"CCL_Status__c\":\"Shipment Cancelled\",\"Id\":\"'+retrieveFPShipments[0].Id+'\",\"CCL_Shipment_Cancellation_Reason__c\":\"Logistics\"}'; 
        Map<String,String> jsonTest = new Map<String,String>();
        jsonTest.put('Id', retrieveFPShipments[0].Id);
        jsonTest.put('CCL_Status__c', 'Shipment Cancelled');
        jsonTest.put('CCL_Shipment_Cancellation_Reason__c', 'Logistics');
        final Boolean confirmShipmentUpdate;
        Test.startTest();
            System.runAs(fpReceiver) { 
                try {
                    confirmShipmentUpdate = CCL_FPShipmentController.shipmentToBeUpdated(jsonTest);
                } catch (Exception e) {
                    final Boolean expectedExceptionThrown = e.getMessage().contains('DML');
                     System.assertEquals(false,expectedExceptionThrown);
                }
            }
        Test.stopTest();
    }

    static testMethod void getFinishedProductShipmentRecTest() {
        
    final List<CCL_Shipment__c> retrieveFPShipments = [SELECT Id, CCL_Hospital_Purchase_Order_Number__c,  RecordTypeId, CCL_Order__r.CCL_Novartis_Batch_ID__c, Name
                                                    From CCL_Shipment__c
                                                    WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP
                                                    AND CCL_Hospital_Purchase_Order_Number__c= '1239099090'
                                                    LIMIT 1 ];
    final List<PermissionSetAssignment> listFpReceivers  = [SELECT Id, PermissionSetId, PermissionSet.Name, PermissionSet.ProfileId, PermissionSet.Profile.Name, 
                                                            AssigneeId, Assignee.Name 
                                                            FROM PermissionSetAssignment 
                                                            WHERE PermissionSet.Name = :CCL_StaticConstants_MRC.PERMISSION_SET_FP_RECEIVER
                                                            ];
        System.debug('::CCL_FPShipmentController ::getFinishedProductShipmentRecTest ::retrieveFPShipments ' + retrieveFPShipments );
    final String assignName = listFpReceivers[0].AssigneeId;

    final User systemAdminUser = [SELECT Id, Name, Profile.Name, IsActive
                                FROM User 
                                WHERE Profile.Name = :CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE
                                AND IsActive = :CCL_StaticConstants_MRC.VALUE_TRUE
                                LIMIT 1];
        System.debug('::CCL_FPShipmentController ::getFinishedProductShipmentRecTest ::systemAdminUser ' + systemAdminUser );
         
        CCL_Shipment__c shipmentFPRec = new CCL_Shipment__c();
        Test.startTest();
        final String hospitalPurchaseOrderNumber = '1239099090';
        System.runAs(systemAdminUser) {
            shipmentFPRec = CCL_FPShipmentController.getFinishedProductShipmentRec(retrieveFPShipments[0].Id);
        }
        Test.stopTest();
        System.assertEquals(retrieveFPShipments[0].CCL_Hospital_Purchase_Order_Number__c,hospitalPurchaseOrderNumber , 'Shipment Record was not found ' + shipmentFPRec.Id);
        
    }

    static testMethod void getFinishedProductShipmentRecExceptionHandlingTest() {
        
    final List<CCL_Shipment__c> retrieveFPShipments = [SELECT Id, CCL_Hospital_Purchase_Order_Number__c,  RecordTypeId, CCL_Order__r.CCL_Novartis_Batch_ID__c, Name
                                                    From CCL_Shipment__c
                                                    WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP
                                                    AND CCL_Hospital_Purchase_Order_Number__c= '1239099090'
                                                    LIMIT 1 ];

    final User externalUserWithoutPermissionSet = [SELECT Id, Name, Profile.Name, IsActive, LastName
                                                FROM User 
                                                WHERE Profile.Name = :CCL_StaticConstants_MRC.EXTERNAL_BASE_PROFILE_TYPE
                                                AND IsActive = :CCL_StaticConstants_MRC.VALUE_TRUE
                                                AND LastName = 'UserWithOutPermissionSet'
                                                LIMIT 1];
        System.debug('::CCL_FPShipmentController ::getFinishedProductShipmentRecExceptionHandlingTest ::externalUserWithoutPermissionSet ' + externalUserWithoutPermissionSet );
         
        CCL_Shipment__c shipmentFPRec = new CCL_Shipment__c();
        Test.startTest();
        System.runAs(externalUserWithoutPermissionSet) {
            try {
                shipmentFPRec = CCL_FPShipmentController.getFinishedProductShipmentRec(retrieveFPShipments[0].Id);
            } catch (Exception e) {
                System.debug('::CCL_FPShipmentController ::getFinishedProductShipmentRecExceptionHandlingTest ::e.getMessage() ' + e.getMessage() );
                final Boolean expectedExceptionThrown =  e.getMessage().contains('Access');
                System.assert(expectedExceptionThrown, 'Expected Exception was not hit ' + expectedExceptionThrown);
            }
            
            }
        Test.stopTest();
    }



    static testMethod void fetchFPShipmentRecordTest() {

         final List<CCL_Shipment__c> retrieveFPShipments = [SELECT Id, CCL_Hospital_Purchase_Order_Number__c,  RecordTypeId, CCL_Order__r.CCL_Novartis_Batch_ID__c, Name
                                                    From CCL_Shipment__c
                                                    WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP
                                                    AND CCL_Hospital_Purchase_Order_Number__c= '1239099090'
                                                    LIMIT 1 ];
        final List<PermissionSetAssignment> listFpReceivers  = [SELECT Id, PermissionSetId, PermissionSet.Name, PermissionSet.ProfileId, PermissionSet.Profile.Name, 
                                                            AssigneeId, Assignee.Name 
                                                            FROM PermissionSetAssignment 
                                                            WHERE PermissionSet.Name = :CCL_StaticConstants_MRC.PERMISSION_SET_FP_RECEIVER
                                                            ];
        System.debug('::CCL_FPShipmentController ::fetchFPShipmentRecordTest ::retrieveFPShipments ' + retrieveFPShipments );
        final String assignName = listFpReceivers[0].AssigneeId;

        final User systemAdminUser = [SELECT Id, Name, Profile.Name, IsActive
                                FROM User 
                                WHERE Profile.Name = :CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE
                                AND IsActive = :CCL_StaticConstants_MRC.VALUE_TRUE
                                LIMIT 1];
        System.debug('::CCL_FPShipmentController ::fetchFPShipmentRecordTest ::systemAdminUser ' + systemAdminUser );
        
        List<CCL_Shipment__c> fetchFPShipmentRecords = new List<CCL_Shipment__c>();  
        Test.startTest();
        System.runAs(systemAdminUser) {
            fetchFPShipmentRecords = CCL_FPShipmentController.fetchFPShipmentRecord(retrieveFPShipments[0].Id);
        }
        Test.stopTest();
        System.assertEquals(fetchFPShipmentRecords[0].CCL_Order__r.CCL_Novartis_Batch_ID__c, null, 'Shipment Record was not found ' + fetchFPShipmentRecords[0].Id);
        
    }

    static testMethod void checkUserPermissionTestPositiveTest() {
        
        final List<PermissionSetAssignment> listfpReceivers  = [SELECT Id, PermissionSetId, PermissionSet.Name, PermissionSet.ProfileId, PermissionSet.Profile.Name, 
                                                            AssigneeId, Assignee.Name 
                                                            FROM PermissionSetAssignment 
                                                            WHERE PermissionSet.Name = :CCL_StaticConstants_MRC.PERMISSION_SET_FP_RECEIVER
                                                            ];
        System.debug('::CCL_FPShipmentController ::checkUserPermissionTestPositiveTest ::listfpReceivers ' + listfpReceivers );
        final String assignName = listfpReceivers[0].AssigneeId;

        final User fpReceiver = [SELECT Id, Name 
                            FROM User 
                            WHERE Id = :assignName
                            LIMIT 1];

        System.debug('::CCL_FPShipmentController ::checkUserPermissionTestPositiveTest ::fpReceiver ' + fpReceiver );
        final Boolean checkCustomPermission;
        String customFPReceiverPermissions = 'CCL_FP_Receiver';
        Test.startTest();
        System.runAs(fpReceiver) {

            checkCustomPermission = CCL_FPShipmentController.checkUserPermission(customFPReceiverPermissions);
        }
        Test.stopTest(); 
        System.assert(checkCustomPermission, 'Custom Permission was not found ' + checkCustomPermission);
    }

    static testMethod void checkUserPermissionTestNegativeTest() {

        final List<PermissionSetAssignment> listfpReceivers  = [SELECT Id, PermissionSetId, PermissionSet.Name, PermissionSet.ProfileId, PermissionSet.Profile.Name, 
                                                            AssigneeId, Assignee.Name 
                                                            FROM PermissionSetAssignment 
                                                            WHERE PermissionSet.Name = :CCL_StaticConstants_MRC.PERMISSION_SET_INFUSION_REPORTER
                                                            ];
        System.debug('::CCL_FPShipmentController ::checkUserPermissionTestNegativeTest ::listfpReceivers ' + listfpReceivers );

        List<id> assignName = new List<id>();
        for(PermissionSetAssignment ids:listfpReceivers){
						assignName.add(ids.AssigneeId);
        }

        final User fpReceiver = [SELECT Id, Name 
                            FROM User 
                            WHERE Id in :assignName AND IsActive= true
                            LIMIT 1];

        System.debug('::CCL_FPShipmentController ::checkUserPermissionTestNegativeTest ::NonFpReceiver ' + fpReceiver );
        String customFPReceiverPermissions = 'CCL_FP_Receiver';
        Test.startTest();
        System.runAs(fpReceiver) {
        final Boolean expectedErrorMessage = false;
        Boolean checkCustomPermission;
            checkCustomPermission = CCL_FPShipmentController.checkUserPermission(customFPReceiverPermissions);
        System.assertEquals(expectedErrorMessage, checkCustomPermission, 'Custom Permission was not found ' + checkCustomPermission);
        }
        Test.stopTest();
        
    }

        static testMethod void getUploadedFileDetails() {
            
        
        final List<CCL_Shipment__c> retrieveFPShipments = [SELECT Id, CCL_Hospital_Purchase_Order_Number__c,  RecordTypeId, CCL_Order__r.CCL_Novartis_Batch_ID__c, Name
                                                    From CCL_Shipment__c
                                                    WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP
                                                    AND CCL_Hospital_Purchase_Order_Number__c= '1239099090'
                                                    LIMIT 1 ];
        System.debug('retrieveFPShipments xoxo' + retrieveFPShipments);
        List<ContentDocumentLink> fileDetails= new List<ContentDocumentLink>();

        fileDetails = [SELECT Id,ContentDocumentId,ContentDocument.LatestPublishedVersionId, ContentDocument.Title,ContentDocument.FileType,ContentDocument.FileExtension,ContentDocument.CreatedBy.Id  
                                   FROM ContentDocumentLink 
                                   WHERE LinkedEntityId =: retrieveFPShipments[0].Id]; 
            
                    System.debug('fileDetails x' + fileDetails);

            
         Test.startTest();
         CCL_FPShipmentController.getUploadedFileDetails(retrieveFPShipments[0].Id);
                    
            
         System.assertEquals(1, fileDetails.size(), 'The current shipment record does not have any files attached.');

        Test.stopTest();

       
    }

    static testMethod void getShipmentDataTest() {
            
        
        final List<CCL_Shipment__c> retrieveFPShipments = [SELECT Id, CCL_Hospital_Purchase_Order_Number__c,  RecordTypeId, CCL_Order__r.CCL_Novartis_Batch_ID__c, Name
                                                    From CCL_Shipment__c
                                                    WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP
                                                    AND CCL_Hospital_Purchase_Order_Number__c= '1239099090'
                                                    LIMIT 1 ];   
         Test.startTest();
         CCL_Shipment__c fpShipment = CCL_FPShipmentController.getShipmentData(retrieveFPShipments[0].Id);
         Test.stopTest();
        
        final Id shipmentFPRecordTypeId = CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP;
         CCL_Shipment__c       shipmentFPRec = [SELECT Id, RecordTypeId, Name, toLabel(CCL_Status__c), toLabel(CCL_Shipment_Reason__c), CCL_Secondary_Therapy__c, CCL_Infusion_Center__r.ShippingCity, 
                                CCL_Infusion_Center__r.ShippingStreet, CCL_Infusion_Center__r.ShippingCountry, CCL_Infusion_Center__r.ShippingState, 
                                CCL_Infusion_Center__r.ShippingPostalCode, CCL_Ship_to_Location__c,CCL_Hospital_Purchase_Order_Number__c, 
                                CCL_Estimated_FP_Delivery_Date_Time__c, CCL_Waybill_number__c, CCL_Actual_FP_Delivery_Date_Time_Text__c, CCL_Dewar_Tracking_link_1__c, CCL_Dewar_Tracking_link_2__c,
                                CCL_Product_Acceptance_Status__c, CCL_Ship_to_Location__r.ShippingCity, CCL_Ship_to_Location__r.ShippingStreet, CCL_Ship_to_Location__r.ShippingCountry, 
                                CCL_Ship_to_Location__r.ShippingState, CCL_Ship_to_Location__r.ShippingPostalCode, CCL_Manufacturing_Time_Zone__c, CCL_Ship_to_Time_Zone__c, 
                                CCL_Actual_Shipment_Date_Time_Text__c, CCL_Estimated_FP_Shipment_Date_Time_Text__c, CCL_Estimated_FP_Delivery_Date_Time_Text__c, CCL_Acceptance_Rejection_Details__c,
                                CCL_Number_of_Doses_Shipped__c, CCL_Number_of_Bags_Shipped__c, CCL_Expiry_Date_of_Shipped_Bags_Text__c, CCL_Number_of_Bags_Received__c,
                                CCL_Order__r.Id, CCL_Order__r.Name, LastModifiedDate, CCL_Shipment_Cancellation_Reason__c, CCL_Product_Rejection_Reason__c, CCL_Product_Rejection_Notes__c, CCL_Ordering_Hospital__r.ShippingCountryCode, CCL_Indication_Clinical_Trial__c
                                FROM CCL_Shipment__c WHERE Id =:retrieveFPShipments[0].Id 
                                AND RecordTypeId =:shipmentFPRecordTypeId 
                                LIMIT 1];
        System.assertEquals(fpShipment.Name,shipmentFPRec.Name,'ActualData');
       
    }
    
   /*static testMethod void getShipmentFileDetailsTest() {
            
        
       final List<CCL_Shipment__c> retrieveFPShipments = [SELECT Id, CCL_Hospital_Purchase_Order_Number__c,  RecordTypeId, CCL_Order__r.CCL_Novartis_Batch_ID__c, Name
                                                          From CCL_Shipment__c
                                                          WHERE RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP
                                                          AND CCL_Hospital_Purchase_Order_Number__c= '1239099090'
                                                          LIMIT 1 ];
       
       Test.startTest();
      // List<ContentDocumentLink> documentDetails = CCL_FPShipmentController.getShipmentFileDetails(retrieveFPShipments[0].Id);
       List<String> getAphIdDINSECList= CCL_FPShipmentController.getAphIdDINSECList(retrieveFPShipments[0].Id);
       String timezone1 = CCL_FPShipmentController.getsUserTimeZone();
       String rejectshipment = CCL_FPShipmentController.updateShipmentRecordForRejectProcess(retrieveFPShipments[0]);
       
       String PermissionName= CCL_StaticConstants_MRC.PERMISSION_SET_FP_RECEIVER;
       CCL_FPShipmentController.checkUserPermissionOutboundLogistic(PermissionName);
       CCL_FPShipmentController.getFormattedDate(system.today());
       CCL_FPShipmentController.getFormattedDateTime(system.today(), 'GMT');
       Test.stopTest();
       //final List<ContentDocumentLink> fileDetails=[SELECT ContentDocument.title, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId=:retrieveFPShipments[0].Id ];
       
      // System.assertEquals(documentDetails.size(),fileDetails.size(),'ActualData');
       
    }*/
    
    
}
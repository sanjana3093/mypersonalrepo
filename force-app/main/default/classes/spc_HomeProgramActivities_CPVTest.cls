/**
* @author Deloitte
* @date 07/30/2018
*
* @description This is the test class for spc_HomeProgramActivities_CPVTest
*/
@isTest
public class spc_HomeProgramActivities_CPVTest {
    @testSetup static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

    }
    private static PatientConnect__PC_Document__c oDoc;
    private static Attachment oAttachment;
    private static List<Case> programCases;


    private static void createTestData() {

        //Create 1 patient accounts
        Account patient = spc_Test_Setup.createTestAccount('PC_Patient');
        patient.PatientConnect__PC_Email__c = 'sr@sr.com';
        patient.Phone = '7121231413';
        patient.spc_Mobile_Phone__c = '7121231413';
        insert patient;

        //Create 1 program cases
        Case programCases = spc_Test_Setup.newCase(patient.id, 'PC_Program');
        spc_Database.ins(programCases);

        Task t1 = new Task();
        t1.ActivityDate = date.today();
        t1.subject = 'Test Subject - today - In Progress';
        t1.status = 'In Progress';
        t1.WhatId = programCases.id;
        spc_Database.ins(t1);

        Task t2 = new Task();
        t2.ActivityDate = date.today().addDays(1);
        t2.subject = 'Test Subject - tomorrow - Completed - Starred';
        t2.status = 'Completed';
        t2.PatientConnect__PC_Starred__c = true;
        spc_Database.ins(t2);

        Task t3 = new Task();
        t3.ActivityDate = date.today().addDays(-1);
        t3.subject = 'Test Subject - yesterday - In Progress - Starred';
        t3.status = 'In Progress';
        t3.PatientConnect__PC_Starred__c = true;
        spc_Database.ins(t3);

        Task t4 = new Task();
        t4.ActivityDate = date.today();
        t4.subject = 'Test Subject - Today - In Progress - Starred';
        t4.status = 'In Progress';
        t4.PatientConnect__PC_Starred__c = true;
        spc_Database.ins(t4);

        Task t5 = new Task();
        t5.ActivityDate = date.today();
        t5.subject = 'Test Subject - Today - Completed';
        t5.status = 'Completed';
        spc_Database.ins(t5);

        Task t6 = new Task();
        t6.ActivityDate = date.today().addDays(-6);
        t6.subject = 'Test Subject - Overdue';
        t6.status = 'Completed';
        spc_Database.ins(t6);

    }
    public static PatientConnect__PC_Document__c createDocument(String sRecordType, String sStatus, String programName) {
        Test.StartTest();
        Account manufacturer = new Account();
        manufacturer.phone = '7511231234';
        manufacturer.PatientConnect__PC_Email__c = 'test@test.com';
        insert manufacturer;
        Test.stopTest();
        PatientConnect__PC_Engagement_Program__c  engagement = spc_Test_Setup.createEngagementProgram('programA', manufacturer.Id);
        engagement = (PatientConnect__PC_Engagement_Program__c)spc_Database.ins(engagement, false);
        PatientConnect__PC_Document__c objDocument = new PatientConnect__PC_Document__c();
        objDocument.RecordTypeId = recordTypeId('PatientConnect__PC_Document__c', sRecordType);
        objDocument.PatientConnect__PC_Document_Status__c = sStatus;
        objDocument.PatientConnect__PC_Page_Count__c = 5;
        objDocument.PatientConnect__PC_Engagement_Program__c = engagement.Id;
        return objDocument;

    }
    public static Id recordTypeId(String objectName, String developerName) {
        RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = :objectName AND DeveloperName = :developerName];
        return rt == null ? null : rt.Id;
    }
    public static Attachment createAttachment(Id docId) {
        Attachment objAtt = new Attachment();
        String name = 'abc';
        objAtt.body = Blob.valueof(name);
        objAtt.name = 'DemoName.pdf';
        objAtt.ParentId = docId;
        return objAtt;
    }
    static testMethod void testGetOverDueActivities() {
        createTestData();
        Test.StartTest();
        spc_HomeProgramActivities_CPV_Controller.activityWrapper[] results = spc_HomeProgramActivities_CPV_Controller.getOverDueActivities(null, null, null);

        System.assertNotEquals(null, results );


        Test.StopTest();
    }

    static testMethod void testGetActivitiesDueToday() {
        createTestData();
        Test.StartTest();
        spc_HomeProgramActivities_CPV_Controller.activityWrapper[] results = spc_HomeProgramActivities_CPV_Controller.getActivitiesDueToday(null);
        System.assertNotEquals(null, results );


        Test.StopTest();
    }
    static testMethod void testGetActivitiesDueTomorrow() {
        createTestData();
        Test.StartTest();
        spc_HomeProgramActivities_CPV_Controller.activityWrapper[] results = spc_HomeProgramActivities_CPV_Controller.getActivitiesDueTomorrow(null);
        System.assertNotEquals(null, results );

        Test.StopTest();

    }

    static testMethod void testGetStarredActivities() {
        createTestData();
        Test.StartTest();
        spc_HomeProgramActivities_CPV_Controller.activityWrapper[] results = spc_HomeProgramActivities_CPV_Controller.getStarredActivities(null, null, null);
        System.assertNotEquals(null, results );

        Test.StopTest();

    }
    static testMethod void testUpdateTaskStar() {
        createTestData();
        Test.StartTest();
        spc_HomeProgramActivities_CPV_Controller.activityWrapper[] results = spc_HomeProgramActivities_CPV_Controller.getStarredActivities(null, null, null);


        System.assertNotEquals(null, results );

        Integer resultCount = results.size();
        Account patient = spc_Test_Setup.createTestAccount('PC_Patient');
        patient.PatientConnect__PC_Email__c = 's1r@s1r.com';
        patient.Phone = '7141231413';
        patient.spc_Mobile_Phone__c = '7151231413';
        patient.name  = 'joy1';
        insert patient;
        Case programCases = spc_Test_Setup.newCase(patient.id, 'PC_Program');
        spc_Database.ins(programCases);
        Task t1 = new Task();
        t1.ActivityDate = date.today();
        t1.subject = 'Test Subject - today - In Progress';
        t1.status = 'In Progress';
        t1.PatientConnect__PC_Starred__c = false;
        t1.PatientConnect__PC_Program__c = programCases.id;
        spc_Database.ins(t1);

        results = spc_HomeProgramActivities_CPV_Controller.getStarredActivities(null, null, null);
        System.assertNotEquals(null, results );

        spc_HomeProgramActivities_CPV_Controller.updateTask(t1.id);

        results = spc_HomeProgramActivities_CPV_Controller.getStarredActivities(null, null, null);
        System.assertNotEquals(null, results );

        spc_HomeProgramActivities_CPV_Controller.updateTask(t1.id);

        results = spc_HomeProgramActivities_CPV_Controller.getStarredActivities(null, null, null);
        PatientConnect__PC_Alert__c alert = new PatientConnect__PC_Alert__c();
        alert.PatientConnect__PC_Program__c = programCases.id;
        insert alert;
        spc_HomeProgramActivities_CPV_Controller.overrideAlert(alert.id);
        PatientConnect__PC_Alert__c al = new PatientConnect__PC_Alert__c();
        al.PatientConnect__PC_Program__c = programCases.id;
        al.PatientConnect__PC_Date_of_Alert__c = Date.newInstance(2012, 01, 05);
        spc_HomeProgramActivities_CPV_Controller.alertWrapper alertObj = new spc_HomeProgramActivities_CPV_Controller.alertWrapper(al);
        spc_HomeProgramActivities_CPV_Controller.activityWrapper activityWrap = new spc_HomeProgramActivities_CPV_Controller.activityWrapper(alertObj);
        PatientConnect__PC_Alert__c alert2 = new PatientConnect__PC_Alert__c();
        alert2.PatientConnect__PC_Program__c = programCases.id;
        spc_HomeProgramActivities_CPV_Controller.alertWrapper alert2Obj = new spc_HomeProgramActivities_CPV_Controller.alertWrapper(alert2);
        spc_HomeProgramActivities_CPV_Controller.activityWrapper activityWrapper = new spc_HomeProgramActivities_CPV_Controller.activityWrapper(alert2Obj);
        System.assert(al != null, al);
        System.assertNotEquals(null, results );
        System.assertEquals(resultCount, results.size());

        Test.StopTest();
    }
    static testMethod void testgetThisWeekActivities() {
        createTestData();
        Test.StartTest();
        spc_HomeProgramActivities_CPV_Controller.activityWrapper[] results = spc_HomeProgramActivities_CPV_Controller.getThisWeekActivities(null, null);
        System.assertNotEquals(null, results );


        Test.StopTest();
    }

    static testMethod void testupdatePriority() {

        Test.StartTest();

        Task t1 = new Task();
        t1.ActivityDate = date.today();
        t1.subject = 'Test Subject - today - In Progress';
        t1.status = 'In Progress';
        t1.PatientConnect__PC_Starred__c = false;
        spc_Database.ins(t1);

        spc_HomeProgramActivities_CPV_Controller.activityWrapper results = spc_HomeProgramActivities_CPV_Controller.updatePriority(t1.id);


        Test.StopTest();

    }

    static testMethod void testcloseTask() {
        Test.StartTest();

        Task t1 = new Task();
        t1.ActivityDate = date.today();
        t1.subject = 'Test Subject - today - In Progress';
        t1.status = 'In Progress';
        t1.PatientConnect__PC_Starred__c = false;
        t1.PatientConnect__PC_Call_Outcome__c = 'Reached';
        spc_Database.ins(t1);

        spc_HomeProgramActivities_CPV_Controller.activityWrapper results = spc_HomeProgramActivities_CPV_Controller.closeTask(t1.id);



        Test.StopTest();
    }

    private static PatientConnect__PC_Alert__c alert1;
    private static PatientConnect__PC_Alert__c alert2;

    @isTest static void testGetActiveUserAlerts() {
        createTestData();
        Test.StartTest();

        List<spc_HomeProgramActivities_CPV_Controller.alertWrapper> results = spc_HomeProgramActivities_CPV_Controller.getActiveUserAlerts();
        System.assertNotEquals(null, results );

        Test.StopTest();
    }
    static testMethod void testGetListViews() {
        createTestData();
        Test.StartTest();
        ListView results = spc_HomeProgramActivities_CPV_Controller.getListViews();
        System.assertNotEquals(null, results );
        Test.StopTest();
    }

    static testMethod void testgetReturnUrl() {
        createTestData();
        Test.StartTest();
        String url = spc_HomeProgramActivities_CPV_Controller.getReturnUrl();
        system.assertNotEquals(url, null);
        Test.StopTest();
    }
}
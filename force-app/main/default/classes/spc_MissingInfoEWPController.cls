/**
* @author Deloitte
* @date 06-August-18
*
* @description Controller class for Missing Information Enrollment Wizard Page
*/

global with sharing class spc_MissingInfoEWPController {

    /*******************************************************************************************************
    * @description This method is used for getting picklist values
    * @return Map<String, List<PicklistEntryWrapper>>
    */
    @AuraEnabled
    public static Map<String, List<spc_PickListFieldManager.PicklistEntryWrapper>> getPicklistEntryMap() {
        Map<String, List<spc_PickListFieldManager.PicklistEntryWrapper>> picklistEntryMap =
            new Map<String, List<spc_PickListFieldManager.PicklistEntryWrapper>>();

        picklistEntryMap.put('missingInfoStatus', spc_PickListFieldManager.getPicklistEntryWrappers(Case.PatientConnect__PC_Status_Indicator_3__c));
        return picklistEntryMap;
    }

    /*******************************************************************************************************
    * @description This method is used for getting HIPAA Consent values
    * @param enrollmentCaseId String
    * @return String
    */
    @AuraEnabled
    public static String getHIPAAConsent(String enrollmentCaseId) {
        String hipaaConsentMissing = '';
        List<Case> cases = [select PatientConnect__PC_EnrollmentWizardState__c from Case where id = :enrollmentCaseId];
        for (Case enrollmentCase : cases) {
            if (!String.isBlank(enrollmentCase.PatientConnect__PC_EnrollmentWizardState__c)) {
                Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(enrollmentCase.PatientConnect__PC_EnrollmentWizardState__c);
                Map<String, Object> objectNames = (Map<String, Object>)result.get('Patient_Information');
                Map<String, Object> account = (Map<String, Object>)objectNames.get('persistentAccount');
                if (null != account) {
                    if (Label.spc_No.equalsIgnoreCase((String) account.get('patientHIPAConsent'))) {
                        hipaaConsentMissing = Label.spc_No;
                    } else if (Label.spc_Yes.equalsIgnoreCase((String) account.get('patientHIPAConsent'))) {
                        hipaaConsentMissing = Label.spc_Yes;
                    }
                }
            }

        }
        return hipaaConsentMissing;
    }

}
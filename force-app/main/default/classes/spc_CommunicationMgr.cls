/*********************************************************************************************************

 * @author      Deloitte
 * @date       June 13, 2018
 * @description spc_CommunicationMgr communication framework for sending any kind of outbound communications
*/
global without sharing class spc_CommunicationMgr  {
	//Envelop List to process
	protected List<Envelop> envelopes = new List<Envelop>();
	//Envelop Map - specific to the channel
	private Map<Channel, List<Envelop>> mapEnvelops = new Map<Channel, List<Envelop>>();
	//ELetter records based on flowName + Channel
	private Map<String, PatientConnect__PC_Engagement_Program_Eletter__c> mapEmailTemplates = new Map<String, PatientConnect__PC_Engagement_Program_Eletter__c>();
	//Case specific recipiet -> CasId => Role => Account
	private Map<String, Map<String, Account>> mapRecipients = new Map<String, Map<String, Account>>();
	//Contact records based on account - This contact is required if channel is Email
	private Map<Id, Contact> mapAccountContacts = new Map<Id, Contact>();
	// Program Case id To Program to Program Case
	private Map<Id, Case> mapProgramCase = new Map<Id, Case>();
	public Map<Id,List<Attachment>> mapEletterToAttachments=new Map<Id,List<Attachment>>();


	//Default Constructor
	public spc_CommunicationMgr() {
	}

	/**
	* @Name          execute
	* @description   Process envelop to send communication- this method is invocable from process builders
	*/
	@InvocableMethod
	public static void execute(List<String> envelopJson) {
		//Desearialize input json to envelops object list
		List<Envelop> envelops = new List<Envelop>();
		for (string jsonstr : envelopJson) {
			envelops.add((Envelop)JSON.deserialize(jsonstr, Envelop.class));
		}
		//Process envelop for the communication
		spc_CommunicationMgr obj = new spc_CommunicationMgr();
		obj.envelopes = envelops;

		obj.processEnvelopes();
	}

	/**
	* @Name          Private method processEnvelop
	* @description   This method process envelops and send communication
	*/
	private void processEnvelopes() {

		//Sepaerte kind of communictions Channel -> List of Envelop
		this.envelopes = this.splitEnvelopes();
		mapEnvelops = new Map<Channel, List<Envelop>>();
		Set<Id> caseIds = new Set<Id>();
		Set<String> flowNames = new Set<String>();


		for (Envelop env : envelopes) {
			if (mapEnvelops.containsKey(env.channel)) {
				mapEnvelops.get(env.channel).add(env);
			} else {
				mapEnvelops.put(env.channel, new List<Envelop> {env});
			}
			caseIDs.add(env.programCaseId);
			flowNames.add(env.flowName);
			//if sourceId is null then set it to ProgramCaseId
			if (String.isBlank(env.sourceId)) {
				env.sourceId = env.programCaseId;
			}
		}

		//Do the query on the custom metadata and find eLetter junction record based on flow name
		this.getEmailTempaltes(flowNames);
		//Find recipiet email and fax number based on  recipients into envelop
		this.getCaseRecipients(caseIds);
		//Create Outbound document record with Ready to send status
		List<PatientConnect__PC_Document__c> outboundDocuments = new List<PatientConnect__PC_Document__c>();
		for (Envelop env : envelopes) {
			outboundDocuments.add(this.createDocument(env));
		}
		//Insert outbound documents
		insert outboundDocuments;

		//Method to set Template data for FAX
		if (mapChannelImpl.containsKey(Channel.FAX) && ! mapProgramCase.isEmpty()) {
			//call method to set template data
			spc_zPaperData obj = new spc_zPaperData(mapProgramCase.values());
		}

		//Process each channel for the communication
		for (Channel s : mapEnvelops.keySet()) {
			if (mapChannelImpl.containsKey(s)) {
				Type t = Type.forname(mapChannelImpl.get(s));
				spc_CommunicationMgr.ICommunication obj = (spc_CommunicationMgr.ICommunication)t.newInstance();
				obj.send(mapEnvelops.get(s), mapEmailTemplates, mapRecipients, mapAccountContacts,mapEletterToAttachments);
			}
		}

		//Create Document Log and update document status
		this.createDocumentLog(envelopes);
	}

	//Public method to call directly to send communications
	public void send(List<Envelop> envelopDetails) {
		this.envelopes = envelopDetails;
		this.processEnvelopes();

	}

	//private List
	private List<Envelop> splitEnvelopes() {
		List<Envelop> newEnvelops = new List<Envelop>();
		for (Envelop envelop : this.envelopes) {
			if (envelop.recipientTypes != null) {
				for (String recipient : envelop.recipientTypes) {
					Envelop env = envelop.clone();
					env.recipientTypes = new Set<String> {recipient};
					newEnvelops.add(env);
				}
			}
		}
		return newEnvelops;
	}

	//This method find receipient details based on case and roles
	private void getCaseRecipients(Set<Id> caseIds) {
		mapRecipients = new Map<String, Map<String, Account>>();
		mapProgramCase = new Map<Id, Case>();
		Set<String> additionalIds = new Set<String>();
		// If Recipient added by Record Id
		for (Envelop env : envelopes) {
			if (env.recipientTypes != null) {
				for (String s : env.recipientTypes) {
					additionalIds.add(s);
				}
			}
		}
		Map<String, Account> additionalAccountMap = new Map<String, Account>();
		Set<Id> accountIds = new Set<Id>();
		for (Account a : [SELECT ID, FAX, PatientConnect__PC_Email__c, recordtype.developerName FROM Account where Id in :additionalIds]) {
			additionalAccountMap.put(a.Id, a );
			accountIds.add(a.Id);
		}

		for (Envelop env : envelopes) {
			env.additionalAccountMap = additionalAccountMap;
		}

		for (Case caseObj : [SELECT ID , AccountId, Account.fax, ownerId, PatientConnect__PC_Physician__c
		                     , PatientConnect__PC_Engagement_Program__c, Account.PatientConnect__PC_Email__c
		                     , spc_Template_Data__r.ID
		                     from Case where ID IN : caseIds]  ) {
			Map<String, Account> roleToAccMap = new Map<String, Account>();
			if (mapRecipients.get(caseObj.ID) != null) {
				roleToAccMap = mapRecipients.get(caseObj.ID);
			}
			if (String.isNotBlank(caseObj.AccountId)) {
				accountIds.add(caseObj.AccountId);
				roleToAccMap.put(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT), caseObj.Account);
			}
			if (String.isNotBlank(caseObj.PatientConnect__PC_Physician__c)) {
				accountIds.add(caseObj.PatientConnect__PC_Physician__c);
				roleToAccMap.put(spc_ApexConstants.ASSOCIATION_ROLE_PRES_PHY, caseObj.Account);
			}
			mapRecipients.put(caseObj.ID, roleToAccMap);
			mapProgramCase.put(caseObj.Id, caseObj);
		}

		List<PatientConnect__PC_Document__c> outboundDocuments = new List<PatientConnect__PC_Document__c>();

		if (!mapProgramCase.isEmpty()) {
			for (Envelop env : envelopes) {
				env.engagementProgramId = mapProgramCase.get(env.programCaseId).PatientConnect__PC_Engagement_Program__c;
			}
		}
		for ( PatientConnect__PC_Association__c patientAssociation : [SELECT Id, PatientConnect__PC_Account__c, PatientConnect__PC_Account__r.PatientConnect__PC_Communication_Language__c, PatientConnect__PC_Account__r.Type, spc_Fax__c, PatientConnect__PC_Account__r.Name,
		        PatientConnect__PC_Account__r.PatientConnect__PC_Email__c, PatientConnect__PC_Program__c, PatientConnect__PC_Role__c, PatientConnect__PC_Account__r.Fax, PatientConnect__PC_Account__r.PatientConnect__PC_Status__c
		        FROM PatientConnect__PC_Association__c
		        WHERE PatientConnect__PC_Program__c in :caseIDs
		        AND PatientConnect__PC_Account__r.PatientConnect__PC_Status__c  = 'Active'
		                AND PatientConnect__PC_AssociationStatus__c = 'Active'
		                        AND PatientConnect__PC_Account__c != null AND PatientConnect__PC_Role__c != null]) {
			Map<String, Account> roleToAccMap = new Map<String, Account>();
			if (mapRecipients.get(patientAssociation.PatientConnect__PC_Program__c) != null) {
				roleToAccMap = mapRecipients.get(patientAssociation.PatientConnect__PC_Program__c);
			}
			if (patientAssociation.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.ASSOCIATION_ROLE_PRES_PHY)
			        || patientAssociation.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_REFERRING_PHYSICIAN))
			        || patientAssociation.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_HCO))
			        || patientAssociation.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_SPHARMACY))
                    || patientAssociation.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_PAYER))
			        || patientAssociation.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_EXT_COMP_PHARMACY))
			        || patientAssociation.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_DESIGNATED_CAREGIVER))) {
				patientAssociation.PatientConnect__PC_Account__r.Fax = patientAssociation.spc_Fax__c;
			}
			roleToAccMap.put(patientAssociation.PatientConnect__PC_Role__c, patientAssociation.PatientConnect__PC_Account__r);
			mapRecipients.put(patientAssociation.PatientConnect__PC_Program__c, roleToAccMap);
			accountIds.add(patientAssociation.PatientConnect__PC_Account__c);
		}
		mapAccountContacts = new Map<Id, Contact>();
		for (Contact con : [SELECT ID , Email, AccountId from Contact where AccountId in :accountIds ] ) {
			mapAccountContacts.put(con.AccountId, con);
		}
	}

	//This method find eletter junction record based on flow names
	private void getEmailTempaltes(Set<String> flowNames) {
		mapEmailTemplates = new Map<String, PatientConnect__PC_Engagement_Program_Eletter__c>();
		Map<String, String> mapFlows = new Map<String, String>();

		for (spc_Communication_Framework__mdt setting : 	[SELECT  PMRC_Code__c, Fax_PMRC_Code__c, Flow_Name__c, Mail_PMRC_Code_del__c
		        FROM spc_Communication_Framework__mdt
		        WHERE Flow_Name__c in :flowNames]) {

			mapFlows.put(spc_Utility.getActualPMRCCode(setting.PMRC_Code__c), setting.Flow_Name__c + '|' + String.valueOf(Channel.Email));
			mapFlows.put(spc_Utility.getActualPMRCCode(setting.Fax_PMRC_Code__c), setting.Flow_Name__c + '|' + String.valueOf(Channel.FAX));
			mapFlows.put(spc_Utility.getActualPMRCCode(setting.Mail_PMRC_Code_del__c), setting.Flow_Name__c + '|' + String.valueOf(Channel.Mail));
			
		}
		 // create a local set
        Set<Id> eletterIds=new Set<Id>();
        for(PatientConnect__PC_Engagement_Program_Eletter__c eletter : [SELECT 
                                                                        ID, spc_PMRC_Code__c,PatientConnect__PC_eLetter__c
                                                                        , PatientConnect__PC_eLetter__r.PatientConnect__PC_Template_Name__c
                                                                        , PatientConnect__PC_eLetter__r.Name 
                                                                        FROM
                                                                        PatientConnect__PC_Engagement_Program_Eletter__c
                                                                        Where spc_PMRC_Code__c IN:mapFlows.keySet() ] ){//Create unique fields, instead of multiple fields
                                                                            mapEmailTemplates.put(mapFlows.get(spc_Utility.getActualPMRCCode(eletter.spc_PMRC_Code__c)),eletter);
                                                                            eletterIds.add(eletter.PatientConnect__PC_eLetter__c);                                                        // Write in set
                                                                            
                                                                        }
        //getting all the attachments from all eLetter records and putting into a map
        for(Attachment attch:[select id,name,ParentId,Body, Parent.Type FROM Attachment where Parent.Type = 'PatientConnect__PC_eLetter__c'
                              AND ParentId IN: eletterIds]){
                                  if(mapEletterToAttachments.get(attch.ParentId)!= null) {
                                      mapEletterToAttachments.get(attch.ParentId).add(attch);
                                      
                                  } else{
                                      mapEletterToAttachments.put(attch.ParentId, new List<Attachment>{attch});
                                  }                     
                                  
                              }
        
        
    }
	//This method create document record of outbound type
	private PatientConnect__PC_Document__c createDocument(Envelop env) {
		PatientConnect__PC_Document__c outboundDocument = new PatientConnect__PC_Document__c();
		PatientConnect__PC_Engagement_Program_Eletter__c eLetter = mapEmailTemplates.get(env.flowName + '|' + String.valueOf(env.channel));
		if (eLetter != null) {
			outboundDocument.PatientConnect__PC_Description__c = eLetter.PatientConnect__PC_eLetter__r.Name;
			outboundDocument.spc_PMRC_Code_New__c = spc_Utility.getActualPMRCCode(eLetter.spc_PMRC_Code__c);
			outboundDocument.PatientConnect__PC_Document_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_STATUS_READYTOSEND);

		} else {
			outboundDocument.PatientConnect__PC_Description__c = Label.spc_no_template_err_msg;
			outboundDocument.PatientConnect__PC_Document_Status__c = spc_ApexConstants.DOC_STATUS_FAILED;
		}
		if (!env.recipientTypes.isEmpty()) {
			outboundDocument.PatientConnect__PC_To_Fax_Name__c = new List<String>(env.recipientTypes)[0];
		}
		//Channel based record type ID set
		if (env.channel == Channel.Email ) {
			outboundDocument.RecordTypeId = spc_ApexConstants.ID_EMAIL_OUTBOUND_RECORDTYPE;
		} else  if (env.channel == Channel.Mail ) {
			outboundDocument.RecordTypeId = spc_ApexConstants.ID_MAIL_OUTBOUND_RECORDTYPE;
		} else  if (env.channel == Channel.FAX ) {
			outboundDocument.RecordTypeId = spc_ApexConstants.ID_FAX_OUTBOUND_RECORDTYPE;
		} else {
			outboundDocument.RecordTypeId = spc_ApexConstants.ID_EMAIL_OUTBOUND_RECORDTYPE;
		}
		outboundDocument.spc_Program_Case_Id__c = env.programCaseId;
		if (null != env.addtionalObjectIds) {
			for (Id recordId : env.addtionalObjectIds ) {
				if (recordId.getSObjectType() == Case.sobjectType) {
					outboundDocument.spc_InquiryCaseId__c = recordId;
				} else if (recordId.getSObjectType() == PatientConnect__PC_Program_Coverage__c.sobjectType) {
					outboundDocument.spc_Program_Coverage__c = recordId;
				} else  if (recordId.getSObjectType() == PatientConnect__PC_Interaction__c.sobjectType) {
					outboundDocument.spc_Interaction__c = recordId;
				}
				outboundDocument.spc_Source_Object_ID__c = recordId ;
			}
		} else {
			outboundDocument.spc_Source_Object_ID__c = env.programCaseId ;
		}
		outboundDocument.PatientConnect__PC_Engagement_Program__c = env.engagementProgramId;
		if (!mapProgramCase.isEmpty()) {
			outboundDocument.OwnerId = mapProgramCase.get(env.programCaseId).OwnerId;
		}
		env.outboundDoc = outboundDocument;
		return outboundDocument;
	}

	/**
	* @Name          createDocumentLog
	* @description   Creation of Documents within the PatientConnect context
	* @author        Zeeshan S. Hussain
	* @date   2018-06-29
	* @return        void
	*/
	private void createDocumentLog(List<Envelop> envelopDetails) {
		List<PatientConnect__PC_Document_Log__c> newDocLogs = new List<PatientConnect__PC_Document_Log__c>();
		List<PatientConnect__PC_Document__c> documents = new List<PatientConnect__PC_Document__c>();
		for (Envelop env : envelopDetails) {
			PatientConnect__PC_Document_Log__c newDocLog = new PatientConnect__PC_Document_Log__c();
			newDocLog.PatientConnect__PC_Program__c = env.programCaseId;
			newDocLog.PatientConnect__PC_Document_Log__c = env.outboundDoc.PatientConnect__PC_Description__c;
			if (null != env.addtionalObjectIds) {
				for (Id recordId : env.addtionalObjectIds ) {
					if (recordId.getSObjectType() == Case.sobjectType) {
						newDocLog.spc_Inquiry__c = recordId;
					} else if (recordId.getSObjectType() == PatientConnect__PC_Program_Coverage__c.sobjectType) {
						newDocLog.PatientConnect__PC_Coverage__c = recordId;
					} else if (recordId.getSObjectType() == PatientConnect__PC_Interaction__c.sobjectType) {
						newDocLog.PatientConnect__PC_Interaction__c = recordId;
					}
				}
			}
			newDocLog.PatientConnect__PC_Document__c = env.outboundDoc.Id;
			documents.add(env.outboundDoc);
			newDocLogs.add(newDocLog);
		}
		update documents;
		insert newDocLogs;
	}


	private Map<Channel, String> mapChannelImpl
	    = new Map<Channel, String> {Channel.Email => 'spc_EmailCommunication'
	                                , Channel.Fax => 'spc_FAXCommunication'
	                                , Channel.Mail => 'spc_MailCommunication'
	                               };

	public class Envelop {
		public Channel channel;//Required - Channel
		public Set<String> recipientTypes;// Required -
		public String programCaseId;//Reauired
		public String sourceId;//Option- only set if whatId is not programCaseId
		public String flowName;//Required
		public Set<Id> addtionalObjectIds;//Use to set additional lookup on document and document log
		public PatientConnect__PC_Document__c outboundDoc;//do not set
		public string engagementProgramId;//Do not set
		public Set<String> attachmentIds ;// Set this only if Attachment has to be sent along with FAX bundle
		public Map<String, Account> additionalAccountMap ;//DO NOT SET
		public string eLetterName; // set eLetter record Id
		public  Set<String> pmrcs; // set pmrc code
		public List<Attachment> attachmentList;// Set for Email merge functionalities
	}

	//Interface for Communication
	public interface ICommunication {
		//Key will be CaseID+flowName+Channel
		//This method will
		void send(List<Envelop> envelops
		          , Map<String, PatientConnect__PC_Engagement_Program_Eletter__c> mapEmailTemplates
		          , Map<String, Map<String, Account>> mapRecipients
		          , Map<Id, Contact> mapAccountContacts,Map<Id,List<sObject>> additionalObjects);


	}

	public enum Channel {Email, FAX, SMS, Mail}

	public class RecipientHandler {
		spc_Comm_Frmwrk_Testing_Parameters__c safeGaurdSetting;
		Set<String> allowedDomain ;//= new Set<String>();
		boolean isSafeGaurdEnabled = false;
		public RecipientHandler() {
			safeGaurdSetting = spc_Comm_Frmwrk_Testing_Parameters__c.getOrgDefaults();
			allowedDomain = new Set<String>();
			if (safeGaurdSetting != null && safeGaurdSetting.spc_Enable_Parameters__c) {
				isSafeGaurdEnabled = true;
				if (String.isNotBlank(safeGaurdSetting.spc_Email_Domain_Allowed__c)) {
					for (String email : safeGaurdSetting.spc_Email_Domain_Allowed__c.split(';')) {
						allowedDomain.add(email);
					}
				}
			}
		}

		public String getFAXNumber(string faxNumber) {
			if (isSafeGaurdEnabled) {
				faxNumber = safeGaurdSetting.spc_Fax__c;
			}
			return faxNumber;
		}

		public String getContactId(string ContactId) {
			if (isSafeGaurdEnabled) {
				ContactId = safeGaurdSetting.spc_Contact_Id__c;
			}
			return ContactId;
		}

		public String getTargetObjectId(Contact con) {
			String contactId = con.Id;
			String emailDomain = this.getEmailDomain(con.Email);
			if (isSafeGaurdEnabled && String.isNotBlank(emailDomain) && !allowedDomain.contains(emailDomain) &&
                !allowedDomain.contains(con.Email)) {
				contactId =  safeGaurdSetting.spc_Contact_Id__c;
			}
			return contactId;
		}
		@testvisible
		private string getEmailDomain(String emailAddress) {
			string emailDomain = '';
			if (String.isNotBlank(emailAddress)) {
				emailDomain = emailAddress.substringAfterLast('@');
			}
			return emailDomain;
		}

		public string getEmailAddress(string emailAddress) {
			String emailDomain = this.getEmailDomain(emailAddress);
			if (isSafeGaurdEnabled && String.isNotBlank(emailDomain) && !allowedDomain.contains(emailDomain) 
                 && !allowedDomain.contains(emailAddress)) {
				emailAddress = safeGaurdSetting.spc_Email__c;
			}
			return emailAddress;
		}

	}
}
/**
* @author Deloitte
* @date 30 July 2018
* @description This Test class is used for Home Page Task and Alerts ;
*/

@isTest
public class spc_HomeActivitiesControllerTest {
    @testSetup static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
    }

    @isTest
    public static void testGetTasksPagination() {
        test.startTest();
        spc_HomeActivitiesController.pageActivityWrapper results = spc_HomeActivitiesController.getTasksPagination('0', 1, 10, null, null);
        spc_HomeActivitiesController.pageActivityWrapper results2 = spc_HomeActivitiesController.getTasksPagination('1', 1, 10, null, null);
        spc_HomeActivitiesController.pageActivityWrapper results3 = spc_HomeActivitiesController.getTasksPagination('3', 1, 10, null, null);
        spc_HomeActivitiesController.pageActivityWrapper results4 = spc_HomeActivitiesController.getTasksPagination('4', 1, 10, null, null);
        spc_HomeActivitiesController.pageActivityWrapper results5 = spc_HomeActivitiesController.getTasksPagination('7', 1, 10, null, null);
        system.assertEquals(results.pageSize, 10);
        system.assertEquals(results3.pageSize, 10);
        test.stopTest();
    }

    @isTest
    public static void testGetAllAlertsPagination() {
        test.startTest();
        spc_HomeActivitiesController.pageActivityWrapper results = spc_HomeActivitiesController.getAllAlertsPagination(1, 10);
        system.assertEquals(results.pageSize, 10);
        test.stopTest();
    }

    @isTest
    public static void testUpdateTask() {
        test.startTest();
        Account patient = spc_Test_Setup.createTestAccount('PC_Patient');
        patient.PatientConnect__PC_Email__c = 'sr@sr.com';
        patient.Phone = '7121231413';
        patient.spc_Mobile_Phone__c = '7121231413';
        insert patient;
        Case programCases = spc_Test_Setup.newCase(patient.id, 'PC_Program');
        spc_Database.ins(programCases);


        Task t1 = spc_Test_Setup.createTask('In Progress', programCases.id, 'Normal', 'Test Subject - today - In Progress',
+                                       'call', System.date.today());
        t1.PatientConnect__PC_Program__c = programCases.id;
        spc_Database.ins(t1);
        spc_HomeActivitiesController.activityWrapper results = spc_HomeActivitiesController.updateTask(t1.id);
        test.stopTest();
    }

    @isTest
    public static void testUpdatePriority() {
        test.startTest();
        Account patient = spc_Test_Setup.createTestAccount('PC_Patient');
        patient.PatientConnect__PC_Email__c = 'sr@sr.com';
        patient.Phone = '7121231413';
        patient.spc_Mobile_Phone__c = '7121231413';
        insert patient;
        Case programCases = spc_Test_Setup.newCase(patient.id, 'PC_Program');
        spc_Database.ins(programCases);

        Task t1 = spc_Test_Setup.createTask('In Progress', programCases.id, 'Normal', 'Test Subject - today - In Progress',
+                                       'call', System.date.today());
        spc_Database.ins(t1);
        spc_HomeActivitiesController.activityWrapper results = spc_HomeActivitiesController.updatePriority(t1.id);
        test.stopTest();
    }

    @isTest
    public static void testCloseTask() {
        test.startTest();
        Account patient = spc_Test_Setup.createTestAccount('PC_Patient');
        patient.PatientConnect__PC_Email__c = 'sr@sr.com';
        patient.Phone = '7121231413';
        patient.spc_Mobile_Phone__c = '7121231413';
        insert patient;
        Case programCases = spc_Test_Setup.newCase(patient.id, 'PC_Program');
        spc_Database.ins(programCases);

        Task t1 = spc_Test_Setup.createTask('In Progress', programCases.id, 'Normal', 'Test Subject - today - In Progress',
                                       'call', System.date.today());
        t1.PatientConnect__PC_Program__c = programCases.id;
        t1.PatientConnect__PC_Call_Outcome__c = 'Reached';

        spc_Database.ins(t1);
        spc_HomeActivitiesController.activityWrapper results = spc_HomeActivitiesController.closeTask(t1.id);
        System.assertEquals(results.programID, t1.PatientConnect__PC_Program__c);
        test.stopTest();
    }

    @isTest
    public static void testGetAllGroupedByTasks() {
        test.startTest();
        Account patient = spc_Test_Setup.createTestAccount('PC_Patient');
        patient.PatientConnect__PC_Email__c = 'sr@sr.com';
        patient.Phone = '7121231413';
        patient.spc_Mobile_Phone__c = '7121231413';
        patient.name = 'joy, green';
        insert patient;
        Case programCases = spc_Test_Setup.newCase(patient.id, 'PC_Program');
        spc_Database.ins(programCases);
        list<task> taskList = new list<task>();

        Task t1 = spc_Test_Setup.createTask('In Progress', programCases.id, 'Normal', 'Test Subject - today - In Progress1',
+                                       'call', System.date.today());
        taskList.add(t1);
        Task t2 = spc_Test_Setup.createTask('In Progress', programCases.id, 'Normal', 'Test Subject - today - In Progress2',
+                                       'call', System.date.today());
        taskList.add(t2);
        spc_Database.ins(taskList);
        spc_HomeActivitiesController.groupedActivityWrapper results = spc_HomeActivitiesController.getAllGroupedByTasks('Physician', 1, 10, 0);
        spc_HomeActivitiesController.groupedActivityWrapper results2 = spc_HomeActivitiesController.getAllGroupedByTasks('Patient', 1, 10, 0);
        spc_HomeActivitiesController.groupedActivityWrapper results3 = spc_HomeActivitiesController.getAllGroupedByTasks('HIP', 1, 10, 0);
        spc_HomeActivitiesController.groupedActivityWrapper results8 = spc_HomeActivitiesController.getAllGroupedByTasks('SOC', 1, 10, 0);
        spc_HomeActivitiesController.groupedActivityWrapper results4 = spc_HomeActivitiesController.getAllGroupedByTasks('Patient', 1, 10, 3);
        spc_HomeActivitiesController.groupedActivityWrapper results5 = spc_HomeActivitiesController.getAllGroupedByTasks('Patient', 1, 10, 4);
        spc_HomeActivitiesController.groupedActivityWrapper results6 = spc_HomeActivitiesController.getAllGroupedByTasks('Patient', 1, 10, 1);
        spc_HomeActivitiesController.groupedActivityWrapper results7 = spc_HomeActivitiesController.getAllGroupedByTasks('Patient', 1, 10, 7);
        spc_HomeActivitiesController.getFilterByValues();
        System.assertEquals(results2.pageSize, 10);
        System.assertEquals(results5.pageSize, 10);
        test.stopTest();
    }

    @isTest
    public static void testOverrideAlert() {
        test.startTest();
        Account patient = spc_Test_Setup.createTestAccount('PC_Patient');
        patient.PatientConnect__PC_Email__c = 'sr@sr.com';
        patient.Phone = '7121231413';
        patient.spc_Mobile_Phone__c = '7121231413';
        insert patient;
        Case programCases = spc_Test_Setup.newCase(patient.id, 'PC_Program');
        spc_Database.ins(programCases);
        PatientConnect__PC_Alert__c alert = new PatientConnect__PC_Alert__c();
        alert.PatientConnect__PC_Program__c = programCases.id;
        insert alert;
        spc_HomeActivitiesController.overrideAlert(alert.id);
        PatientConnect__PC_Alert__c al = new PatientConnect__PC_Alert__c();
        al.PatientConnect__PC_Program__c = programCases.id;
        al.PatientConnect__PC_Date_of_Alert__c = Date.newInstance(2012, 01, 05);
        spc_HomeActivitiesController.alertWrapper alertObj = new spc_HomeActivitiesController.alertWrapper(al);
        spc_HomeActivitiesController.activityWrapper activityWrap = new spc_HomeActivitiesController.activityWrapper(alertObj);
        PatientConnect__PC_Alert__c alert2 = new PatientConnect__PC_Alert__c();
        alert2.PatientConnect__PC_Program__c = programCases.id;
        spc_HomeActivitiesController.alertWrapper alert2Obj = new spc_HomeActivitiesController.alertWrapper(alert2);
        spc_HomeActivitiesController.activityWrapper activityWrapper = new spc_HomeActivitiesController.activityWrapper(alert2Obj);
        System.assert(al != null, al);
        test.stopTest();
    }
}
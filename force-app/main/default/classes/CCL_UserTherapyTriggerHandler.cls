/********************************************************************************************************
*  @author          Deloitte
*  @description     User Therapy Trigger Handler Class to populate the CCL_Unique_Combination__c Field.
*  @param           
*  @date            July 10, 2020
*********************************************************************************************************/

public class CCL_UserTherapyTriggerHandler {
         
        /* This method updates the unique combination field for all the record types*/
    	public void updateUniqueCombinationforDuplicationRule(List<CCL_User_Therapy_Association__c> userTherapList) {
                    
        for(CCL_User_Therapy_Association__c userTh :userTherapList) {
                           
            if(!String.isBlank(userTh.CCL_User__c)) {
                
            userTh.CCL_Unique_Combination__c  = userTh.CCL_User__c +''+ userTh.CCL_Therapy_Association__c;

            } else if(!String.isBlank(userTh.CCL_Contact__c)) {              
            userTh.CCL_Unique_Combination__c  = userTh.CCL_Contact__c +''+ userTh.CCL_Therapy_Association__c;
            }            
       	}    
    }
}
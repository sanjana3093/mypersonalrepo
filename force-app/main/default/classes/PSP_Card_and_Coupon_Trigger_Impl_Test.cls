/*********************************************************************************************************
class Name      : PSP_Card_and_Coupon_Trigger_Impl_Test 
Description		: Test class for Card and Coupon trigger related classes
@author		    : Divya Eduvulapati
@date       	: October 30, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Divya Eduvulapati          October 30, 2019          Initial Version
****************************************************************************************************************/
@isTest
public class PSP_Card_and_Coupon_Trigger_Impl_Test {
    /*Constant for Test */
    private static final String TEST_USER = 'Test';
    /* Constant for last active */
    private static final String LAST_ACTIVE = 'Last';
    /*Apex Constants */
    static final List<PSP_ApexConstantsSetting__c>  APEX_CONSTANTS =  PSP_Test_Setup.setDataforApexConstants();  
     /**************************************************************************************
  	* @author      : Divya Eduvulapati
  	* @date        : 10/30/2019
  	* @Description : Test Method for after update for PSP_Card_and_Coupon_Trigger_Impl.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/
    private static testmethod void triggerTestMethodforAfterUpdate() {
        Test.startTest();
        /*Test setup method being instantiated */
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram (TEST_USER,null);
        cP1.PSP_Program_Sector__c = 'Public';
        Database.insert(cP1);
        /*Test setup method being instantiated */
        final Account acc=PSP_Test_Setup.getPersonAccount(TEST_USER);
       // Database.insert (acc);
        insert acc;
        system.debug(acc.HealthCloudGA__PrimaryContact__c+'acc');
        final PSP_Card_and_Coupon__c lastActiveCard = new PSP_Card_and_Coupon__c(Name='1234567891234567',PSP_Care_Program__c=cP1.Id,PSP_Card_Type__c='Physical',PSP_Status__c='Activated',
                                                                                                                  RecordTypeId = PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CARDANDCOUPON_RT_CARD, PSP_Card_and_Coupon__c.getSObjectType()));                                                                                                                  
        Database.insert(lastActiveCard); 
        final List<PSP_Card_and_Coupon__c> newCard = new List<PSP_Card_and_Coupon__c> {new PSP_Card_and_Coupon__c(Name='New1234562345612',PSP_Care_Program__c=cP1.Id,PSP_Card_Type__c='Physical',PSP_Status__c='Activated',PSP_Last_Active_Card1__c=lastActiveCard.id,PSP_Replacement__c=true,
                                                                                        						 RecordTypeId = PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CARDANDCOUPON_RT_CARD, PSP_Card_and_Coupon__c.getSObjectType()))};                                                                                                            
        Database.insert(newCard);
        final List<PSP_Card_and_Coupon__c> CrdWOHcpOrPtnt = new List<PSP_Card_and_Coupon__c> {new PSP_Card_and_Coupon__c(Name='Test345612351234',PSP_Care_Program__c=cP1.Id,PSP_Card_Type__c='Physical',PSP_Status__c='Activated',PSP_Last_Active_Card1__c=lastActiveCard.id,PSP_Replacement__c=true,
                                                                                        						  				  RecordTypeId = PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CARDANDCOUPON_RT_CARD, PSP_Card_and_Coupon__c.getSObjectType()))};                                                                                                            
        Database.insert(CrdWOHcpOrPtnt);         
        /*Test setup method being instantiated */
        final CareProgramEnrollee cpe = PSP_Test_Setup.createTestCareProgramEnrollment (TEST_USER,cP1.Id);
        cpe.PSP_Card_Number__c = lastActiveCard.Id;
        cpe.Account = acc;      
        Database.insert (cpe);
        final List<PSP_Card_and_Coupon__c> cardList = [Select Id,PSP_Replacement__c,PSP_Last_Active_Card1__c from PSP_Card_and_Coupon__c where PSP_Last_Active_Card1__c=:lastActiveCard.id ];
        for (PSP_Card_and_Coupon__c card: cardList) {
             card.PSP_Replacement__c=false;   
        }
        try {
            Database.update(cardList); 
        } catch(DmlException e) {
            System.debug(' error:- '+e);
        }
        Test.stopTest();
        System.assertEquals(lastActiveCard.Id, cardList[0].PSP_Last_Active_Card1__c, 'Last Active Number has been updated');
    }
	
		
}
/********************************************************************************************************
*  @author          Deloitte
*  @description     File Deletion Trigger Handler to allow only submitter to delete the files
*  @param
*  @date            July 28, 2020
*********************************************************************************************************/

public with sharing class CCL_ContentDocument_trigger_Handler {
   /*  Adf object Name*/
    final static string ADF_OBJECT='CCL_Apheresis_Data_Form__c';
    /* list*/
    final static List<String> PROFILE_NAMES = new List<String>{'System Administrator', 'External Base Profile', 'Internal Base Profile'};
     /* private constructor*/
     private CCL_ContentDocument_trigger_Handler() {

    }
    /* This method checks the user permission and throws error if doesnot matches*/
    public static void handleBeforeDelete(List<ContentDocument> documentList) {
	        Boolean isAdminInteg = false;
			 Map<Id,Id> mapDocContentlinkId=new Map<Id,Id>();
        Set<Id> documentIds=new Set<Id>();
         set<Id> docLinkId=new Set<Id>();
        Set<Id> adfDocs=new Set<Id>();
        Id profileId= userinfo.getProfileId();
    String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        if(profileName.contains('Admin') || profileName.contains('Integ')){
            isAdminInteg=true;
        }
        Boolean adfSubmitter=false;
        for(PermissionSetAssignment perSet:[SELECT Id, PermissionSet.Name,AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :Userinfo.getUserId()]) {
            if(perSet.PermissionSet.Name!=null && (perSet.PermissionSet.Name.Equals(CCL_StaticConstants_MRC.PERMISSION_TYPE_SUBMITTER))||perSet.PermissionSet.Name.Equals(CCL_StaticConstants_MRC.CUTSOM_PERMISSION_PRF_VIEWER)||perSet.PermissionSet.Name.Equals(CCL_StaticConstants_MRC.CUTSOM_PERMISSION_PRF_APPROVER)||perSet.PermissionSet.Name.Equals(CCL_StaticConstants_MRC.PERMISSION_SET_PRF_SUBMITTER)||perSet.PermissionSet.Name.Equals(CCL_StaticConstants_MRC.PERMISSION_TYPE_ADF_APPROVER)||perSet.PermissionSet.Name.Equals(CCL_StaticConstants_MRC.PERMISSION_TYPE_ADF_APPROVER_USER)||perSet.PermissionSet.Name.Equals(CCL_StaticConstants_MRC.PERMISSION_TYPE_ADF_VIEWER_USER)||perSet.PermissionSet.Name.Equals(CCL_StaticConstants_MRC.CUTSOM_PERMISSION_ADF_VIEWER)) {
                System.debug('@@@@@'+perSet.PermissionSet.Name);
                adfSubmitter=true;
				break;
            }
        }
		for(ContentDocument cdoc : documentList) {
            documentIds.add(cdoc.Id);
        }
         for(ContentDocumentLink doc:[select LinkedEntityId,Id,ContentDocumentId from ContentDocumentLink where ContentDocumentId in:documentIds]) {
            docLinkId.add(doc.LinkedEntityId);
             mapDocContentlinkId.put(doc.LinkedEntityId,doc.ContentDocumentId);

        }
        if(!docLinkId.isEmpty()){
            for(CCL_Document__c document:[select id,name,CCL_Apheresis_Data_Form__c,CCL_Order__c,CCL_Shipment__c from CCL_Document__c where id in: docLinkId and CCL_Apheresis_Data_Form__c!=null]){
                adfDocs.add(mapDocContentlinkId.get(document.Id));
            }
        }
        for(ContentDocument cdoc : documentList) {
            if(adfSubmitter==false && !isAdminInteg && adfDocs.contains(cdoc.Id)) {
                cdoc.addError(System.Label.CCL_FileDeletionError);
            }
        }
		 handleDeletionDocs(documentList);
    }
	     /********************************************************************************************************
*  @author          Deloitte
*  @description     this method puts the related ContentDocumentId, in a set
*  @param          ContenetDocumnt List
*  @date          Aug 19.20202
*********************************************************************************************************/
public static void handleDeletionDocs(List<ContentDocument> documentList) {
    Set<Id> contentDocId=new Set<Id>();
  Map<Id,Id> adfContentDocId=new Map<Id,Id>();
  for(ContentDocument doc:documentList) {
      contentDocId.add(doc.Id);
  }
       List<ContentDocumentLink> contentDocLink=new  List<ContentDocumentLink>();
     for(ContentDocumentLink doc:[select LinkedEntityId,Id,ContentDocumentId from ContentDocumentLink where ContentDocumentId in:contentDocId]) {

         if(ADF_OBJECT.equalsIgnoreCase(doc.LinkedEntityId.getSObjectType().getDescribe().getName())) {
           contentDocLink.add(doc);
       }
     }
     if(!contentDocLink.isEmpty()) {
         for(ContentDocumentLink doc:contentDocLink) {
             adfContentDocId.put(doc.ContentDocumentId,doc.LinkedEntityId);
         }
     }
     if(!adfContentDocId.isEmpty()&& adfContentDocId!=null) {
      getNamesofDocs(contentDocId,adfContentDocId);
  }
 }
 /********************************************************************************************************
*  @author          Deloitte
*  @description     this method puts the related adfIds in a set
*  @param         set an dmap
*  @date          Aug 19.20202
*********************************************************************************************************/

 public static  void getNamesofDocs(Set<Id> contentDocId,  Map<Id,Id>adfContentDocId) {
     Set<Id> adfIds=new Set<Id>();
     Map<Id,String> adfIdTitleMap=new Map<Id,String>();
     List<ContentDocument> contentDocs =new   List<ContentDocument>();

     if (ContentDocument.sObjectType.getDescribe().isAccessible()) {
          contentDocs=[Select Title,parentId from ContentDocument where Id in:contentDocId and CreatedBy.Profile.Name in:PROFILE_NAMES ];
     }
     for(ContentDocument doc: contentDocs) {

             adfIds.add(adfContentDocId.get(doc.Id));
             adfIdTitleMap.put(adfContentDocId.get(doc.Id),doc.Title);

     }
     if(!adfIds.isEmpty()) {
      setNamesinADF(adfIds,adfIdTitleMap);
  }
 }
 /********************************************************************************************************
*  @author          Deloitte
*  @description     this method updates the text field in adf
*  @param         set and map
*  @date          Aug 19.20202
*********************************************************************************************************/

 public static void setNamesinADF(Set<Id> adfIds,Map<Id,String>adfIdTitleMap) {
     list<CCL_Apheresis_Data_Form__c> adfList=new List<CCL_Apheresis_Data_Form__c>();
     for(CCL_Apheresis_Data_Form__c adf:[select
                                         CCL_ADF_Document_Titles__c,
                                         id from CCL_Apheresis_Data_Form__c where Id in:adfIds]) {
         	if(adf.CCL_ADF_Document_Titles__c!=null&&adf.CCL_ADF_Document_Titles__c.contains(adfIdTitleMap.get(adf.Id))) {
            adf=calculateNames(adf,adfIdTitleMap);

         	}

         adfList.add(adf);
     }

         if (CCL_Apheresis_Data_Form__c.sObjectType.getDescribe().isAccessible()) {
             Database.update(adfList);
         }


 }
   /********************************************************************************************************
*  @author          Deloitte
*  @description     calculate sthe new value for ADF documnt title
*  @param        adf object
*  @date          Aug 19.20202
*********************************************************************************************************/

 public static CCL_Apheresis_Data_Form__c calculateNames(CCL_Apheresis_Data_Form__c adf,Map<Id,String>adfIdTitleMap) {
     List<String> nameList = new List<String>();
     for(String str:adf.CCL_ADF_Document_Titles__c.split(',')) {
         if(str!=adfIdTitleMap.get(adf.Id)&&adf.CCL_ADF_Document_Titles__c.split(',').size()>1) {
             nameList.add(str);
         } else if(str==adfIdTitleMap.get(adf.Id)&&adf.CCL_ADF_Document_Titles__c.split(',').size()==1) {
             adf.CCL_ADF_Document_Titles__c='';
         }
     }
     if(!nameList.isEmpty()) {
         adf.CCL_ADF_Document_Titles__c=String.join(nameList, ',');
     }
     return adf;
 }

}

/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the Implementation class for Task Trigger
*  @date            05/29/2018
*  @version         1.0
*********************************************************************************************************/
public class spc_AccountTriggerImplementation {


  /**************************************************************************************
  * @author      : Deloitte
  * @date        : 05/29/2018
  * @Description : Method to populate Patient Consent received on case based on Consent(HIPAA, TEXT, MKT or PS Services)
  * @Param       : Set of Account Id
  * @Return      : Void
  ***************************************************************************************/
  public void setProgramCaseIndicatorStatus(Set<Id> accountList) {

    Map<Id, List<Case>> accountCaseMap = new Map<Id, List<Case>>();
    List <Case> updatedCaselist = new List <Case>();
    Map<String, List<Case>> indicatorCaseMap = new Map<String, List<Case>>();
    for (Case oCase : [SELECT
                       Id, Type, AccountId, PatientConnect__PC_Status_Indicator_1__c, Account.spc_HIPAA_Consent_Received__c
                       , Account.spc_Patient_Mrkt_and_Srvc_consent__c, Account.spc_Patient_Services_Consent_Received__c
                       , Account.spc_Text_Consent__c, Account.spc_REMS_Text_Consent__c, Account.spc_Services_Text_Consent__c
                       from
                       Case
                       where
                       AccountId IN: accountList AND Type = 'Program' AND IsClosed != true]) {
      //If REMS Consent is 'Yes' and if any of the other consents (Services Consent,Marketing Text Consent,Services Text Consent, REMS Text Consent,Marketing Consent) are 'No' or Blank
      //Update Status Indicator 1 to 'In Progress'
      if ((String.isBlank(oCase.Account.spc_HIPAA_Consent_Received__c) || oCase.Account.spc_HIPAA_Consent_Received__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO)
           || String.isBlank(oCase.Account.spc_Text_Consent__c) || oCase.Account.spc_Text_Consent__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO)
           || String.isBlank(oCase.Account.spc_Services_Text_Consent__c ) || oCase.Account.spc_Services_Text_Consent__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO)

           || String.isBlank(oCase.Account.spc_Patient_Mrkt_and_Srvc_consent__c) || oCase.Account.spc_Patient_Mrkt_and_Srvc_consent__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO))) {
        oCase.PatientConnect__PC_Status_Indicator_1__c =  spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_STATUS_INDICATOR_1_IN_PROGRESS);
      }
      //If Services Consent is 'Yes' and REMS Consent is 'Yes' and Marketing Text Consent is 'Yes' and Marketing Consent is either 'Yes' or 'Verbal Consent Received'
      //If REMS Text Consent is 'Yes' and if Services Text Consent is either 'Yes' or 'Verbal Consent Received'
      //Update Status Indicator 1 to 'Complete'
      if ((oCase.Account.spc_HIPAA_Consent_Received__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES)

           && oCase.Account.spc_Text_Consent__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES)
           && (oCase.Account.spc_Patient_Mrkt_and_Srvc_consent__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES)
               || oCase.Account.spc_Patient_Mrkt_and_Srvc_consent__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_VERBAL_CONSENT_RECEIVED))

           && (oCase.Account.spc_Services_Text_Consent__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES)
               || oCase.Account.spc_Services_Text_Consent__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_VERBAL_CONSENT_RECEIVED))
          )) {
        oCase.PatientConnect__PC_Status_Indicator_1__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_STATUS_INDICATOR_1_COMPLETE);
      }
      if (String.isBlank(oCase.Account.spc_HIPAA_Consent_Received__c)) { //Service Consent
        oCase.spc_Is_None_Service_Consent__c = true;
        oCase.Is_No_Services_Consent__c = false;
      } else if (oCase.Account.spc_HIPAA_Consent_Received__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO)) {
        oCase.Is_No_Services_Consent__c = true;
        oCase.spc_Is_None_Service_Consent__c = false;
      } else {
        oCase.Is_No_Services_Consent__c = false;
        oCase.spc_Is_None_Service_Consent__c = false;
      }
      updatedCaselist.add(oCase);
    }

    if (! updatedCaselist.isEmpty()) {
      try {
        spc_Database.upd(updatedCaselist);
      } catch (Exception e) {
        spc_Utility.logAndThrowException(e);
      }
    }
  }
  /**************************************************************************************
  * @author      : Deloitte
  * @date        : 05/29/2018
  * @Description : Method to populate Patient Consent received on case based on Consent in Account.
  * @Param       : List of Account
  * @Return      : Void
  ***************************************************************************************/
  public void setProgramCaseConsent(List<Id> accountList) {
    List <Case> updatedCaselist = new List <Case>();


    for (Case oCase : [SELECT Id, spc_Patient_HIPAA_Services_Consent__c, Account.spc_HIPAA_Consent_Received__c, Account.spc_Patient_Mrkt_and_Srvc_consent__c
                       from Case where AccountId IN: accountList AND Type = 'Program' AND IsClosed != true]) {
      if (spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO).equalsIgnoreCase(oCase.Account.spc_HIPAA_Consent_Received__c) ) {
        oCase.spc_Patient_HIPAA_Services_Consent__c = true;
        oCase.spc_AC__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_PIC_STATUS_ONHOLD);
      } else if (spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES).equalsIgnoreCase(oCase.Account.spc_HIPAA_Consent_Received__c) ) {
        oCase.spc_Patient_HIPAA_Services_Consent__c = false;
      }
      updatedCaselist.add(oCase);
    }
    try {
      if (! updatedCaselist.isEmpty()) {
        spc_Database.upd(updatedCaselist);
      }
    } catch (Exception e) {
      spc_Utility.logAndThrowException(e);
    }


  }
  /**************************************************************************************
  * @author      : Deloitte
  * @date        : 01/25/2019
  * @Description : Method to populate BI/BV on case based on Consent in Account.
  * @Param       : List of Account
  * @Return      : Void
  ***************************************************************************************/
  public void setBIBVStatusIndicator(Set<Id> accountList) {
    List <Case> updatedCaselist = new List <Case>();


    for (Case oCase : [SELECT Id, PatientConnect__PC_Status_Indicator_3__c
                       from Case where AccountId IN: accountList AND Type = : spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM) AND IsClosed != true
                           AND PatientConnect__PC_Status_Indicator_3__c != null]) {
      if (oCase.PatientConnect__PC_Status_Indicator_3__c.equalsIgnoreCase(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.PATIENT_STATUS_INDICATOR_COMPLETE))) {
        oCase.PatientConnect__PC_Status_Indicator_2__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_COVERAGE_ACTION_REQUIRED);
      }
      updatedCaselist.add(oCase);
    }
    try {
      if (! updatedCaselist.isEmpty()) {
        spc_Database.upd(updatedCaselist);
      }
    } catch (Exception e) {
      spc_Utility.logAndThrowException(e);
    }


  }

  /**************************************************************************************
  * @author      : Deloitte
  * @date        : 05/29/2018
  * @Description : Method to populate Parent Account Id as Sage Therapeutics in Account.
  * @Param       : List of Accounts
  * @Return      : Void
  ***************************************************************************************/
  public void setParentAccount(List<Account> nonManufacturerAccounts) {
    List<Account> manufacturerAccount = new List<Account> ([Select Id from Account where recordType.DeveloperName = :spc_ApexConstants.ACCOUNT_MANUFACTURER_DEVELOPER_NAME and name = :spc_ApexConstants.SAGE_THERAPEUTICS_ACCOUNT_NAME]);
    if (!manufacturerAccount.isEmpty()) {
      for (Account acc : nonManufacturerAccounts) {
        acc.ParentId = manufacturerAccount[0].id;
      }
    }
  }
  /**************************************************************************************
  * @author      : Deloitte
  * @date        : 05/29/2018
  * @Description : Method to update Caregiver Association to Designated Caregiver.
  * @Param       : Set of Account Ids
  * @Return      : Void
  ***************************************************************************************/
  public List<PatientConnect__PC_Association__c> updateCaregiverAssociation(Map<Id, Account> caregiverAccounts) {
    List<PatientConnect__PC_Association__c> associationsToBeInsOrUpdated = new List<PatientConnect__PC_Association__c>();

    List<PatientConnect__PC_Association__c > associations = new List<PatientConnect__PC_Association__c > ([Select Id, PatientConnect__PC_Account__c,
        PatientConnect__PC_Program__c, PatientConnect__PC_AssociationStatus__c, PatientConnect__PC_Role__c
        from PatientConnect__PC_Association__c  where PatientConnect__PC_Account__c in:caregiverAccounts.keySet()
        and PatientConnect__PC_Role__c = :spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_CAREGIVER)
                                         and PatientConnect__PC_AssociationStatus__c = :spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)
                                             and PatientConnect__PC_Program__r.isClosed = false]);

    if (!associations.isEmpty()) {
      for (PatientConnect__PC_Association__c association : associations) {
        //Inactive exising Caregiver association
        PatientConnect__PC_Association__c caregiverAssociation = new PatientConnect__PC_Association__c();
        caregiverAssociation.PatientConnect__PC_AssociationStatus__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_STATUS_INACTIVE);
        caregiverAssociation.id = association.id;
        associationsToBeInsOrUpdated.add(caregiverAssociation);

        //Create an active Designated Caregiver association
        PatientConnect__PC_Association__c newCaregiverAssociation = new PatientConnect__PC_Association__c();
        newCaregiverAssociation.PatientConnect__PC_AssociationStatus__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE);
        newCaregiverAssociation.PatientConnect__PC_Role__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_DESIGNATED_CAREGIVER);
        newCaregiverAssociation.PatientConnect__PC_Account__c = association.PatientConnect__PC_Account__c;
        newCaregiverAssociation.PatientConnect__PC_Program__c = association.PatientConnect__PC_Program__c;
        associationsToBeInsOrUpdated.add(newCaregiverAssociation);
      }
    }
    return associationsToBeInsOrUpdated;
  }

  /**************************************************************************************
    * @author      : Deloitte
    * @date        : 05/29/2018
    * @Description : Method to populate Patient Consent received on case based on Consent in Account.
    * @Param       : List of Account
    * @Return      : Void
    ***************************************************************************************/
  public void createHCPLogisticTasks(List<Id> accountList, boolean isAccountTypeIsPatient) {

    Set<Id> programIds  = new Set<Id>();
    String subcategory_firstattempt = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_CAT_FIRST_ATTEMPT);
    String category_soclogisticscall = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_SOC_LOGISTIC_CALL);
    String priority_normal = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_PRIORITY_NORMAL);
    String channel_phone = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CHANNEL_PHONE);
    String direction_outbound = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_DIR_OUTBOUND);
    String status_notstarted = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_NOT_STARTED);
    Id programRTId = spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType);
    String completedStatus = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_STATUS_COMPLETED);
    String neverStartStatus = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_STATUS_NEVER_START);
    String sCertifiedStatus  = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ACCOUNT_REMS_CERTIFIED_STATUS);
    String sCompleteStatus = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.PATIENT_STATUS_INDICATOR_COMPLETE);

    spc_TaskTriggerImplementation triggerImplementation = new spc_TaskTriggerImplementation();
    if (isAccountTypeIsPatient) {
      Set<Id>programCasesForAccount = new Set<Id>();
      for (Case newCase : [SELECT ID FROM CASE WHERE RecordTypeId = :programRTId AND PatientConnect__PC_Status_Indicator_3__c = :sCompleteStatus
                           AND Status != :completedStatus AND Status != :neverStartStatus AND accountId IN:accountList]) {
        programCasesForAccount.add(newCase.Id);
      }
      for (PatientConnect__PC_Association__c aa : [Select Id, PatientConnect__PC_AssociationStatus__c, PatientConnect__PC_Role__c, PatientConnect__PC_Program__c from PatientConnect__PC_Association__c where PatientConnect__PC_AssociationStatus__c = : spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)
           AND PatientConnect__PC_Account__r.spc_REMS_Certification_Status__c = :sCertifiedStatus
               AND  PatientConnect__PC_Role__c = : spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO) AND PatientConnect__PC_Program__c IN: programCasesForAccount]) {
        ProgramIds.add(aa.PatientConnect__PC_Program__c);
      }
    } else {
      for (PatientConnect__PC_Association__c aa : [Select Id, PatientConnect__PC_AssociationStatus__c, PatientConnect__PC_Program__r.spc_HIPAA_Consent_Received__c, PatientConnect__PC_Role__c, PatientConnect__PC_Program__c from PatientConnect__PC_Association__c where PatientConnect__PC_AssociationStatus__c = : spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)
           AND  PatientConnect__PC_Role__c = : spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO) AND PatientConnect__PC_Account__c IN: accountList AND PatientConnect__PC_Program__r.PatientConnect__PC_Status_Indicator_3__c = :sCompleteStatus AND PatientConnect__PC_Program__r.spc_HIPAA_Consent_Received__c IN ('', :spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO))]) {
        ProgramIds.add(aa.PatientConnect__PC_Program__c);
      }
    }

    if (!programIds.isEmpty()) {
      triggerImplementation.createFollowUpTasks(programIds, System.today() + 1, category_soclogisticscall,
          subcategory_firstattempt, channel_phone, direction_outbound, priority_normal, status_notstarted,
          category_soclogisticscall);
    }
  }
  /**************************************************************************************
  * @author      : Deloitte
  * @date        : 05/29/2018
  * @Description : Method to update Designated Caregiver Association to  Caregiver.
  * @Param       : Set of Account Ids
  * @Return      : Void
  ***************************************************************************************/
  public List<PatientConnect__PC_Association__c> updateDesignatedCaregiverAssociation(Set<Id> designatedCaregiverAccounts) {
    List<PatientConnect__PC_Association__c> associationsToBeInsOrUpdated = new List<PatientConnect__PC_Association__c>();
    List<PatientConnect__PC_Association__c > associations = new List<PatientConnect__PC_Association__c > ([Select Id, PatientConnect__PC_Account__c,
        PatientConnect__PC_Program__c, PatientConnect__PC_AssociationStatus__c, PatientConnect__PC_Role__c
        from PatientConnect__PC_Association__c  where PatientConnect__PC_Account__c in:designatedCaregiverAccounts
        and PatientConnect__PC_Role__c = :spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_DESIGNATED_CAREGIVER)
                                         and PatientConnect__PC_AssociationStatus__c = :spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)
                                             and PatientConnect__PC_Program__r.isClosed = false]);
    if (!associations.isEmpty()) {
      for (PatientConnect__PC_Association__c association : associations) {
        PatientConnect__PC_Association__c caregiverAssociation = new PatientConnect__PC_Association__c();
        caregiverAssociation.PatientConnect__PC_AssociationStatus__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_STATUS_INACTIVE);
        caregiverAssociation.id = association.id;
        associationsToBeInsOrUpdated.add(caregiverAssociation);

        PatientConnect__PC_Association__c newCaregiverAssociation = new PatientConnect__PC_Association__c();
        newCaregiverAssociation.PatientConnect__PC_AssociationStatus__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE);
        newCaregiverAssociation.PatientConnect__PC_Role__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_CAREGIVER);
        newCaregiverAssociation.PatientConnect__PC_Account__c = association.PatientConnect__PC_Account__c;
        newCaregiverAssociation.PatientConnect__PC_Program__c = association.PatientConnect__PC_Program__c;
        associationsToBeInsOrUpdated.add(newCaregiverAssociation);

      }
    }
    return associationsToBeInsOrUpdated;
  }
  /**************************************************************************************
   * @author      : Deloitte
   * @date        : 12/11/2018
   * @Description : Method to Update Account Name in Case Subject
   * @Param       : Map of Account Id and Name
   * @Return      : Void
   ***************************************************************************************/
  public void updateAccountNameInCase(Map<Id, String> accountIdAndNameMap) {
    List <Case> updatedCaselist = new List <Case>();
    for (Case newCase : [SELECT AccountId, Subject, PatientConnect__PC_Engagement_Program__c, PatientConnect__PC_Engagement_Program__r.Name, RecordTypeId from Case where AccountId IN: accountIdAndNameMap.keySet() AND IsClosed != true ]) {
      String caseRcrdType = Schema.SObjectType.Case.getRecordTypeInfosById().get(newCase.RecordTypeId).getname();
      newCase.Subject = (String.isBlank(accountIdAndNameMap.get(newCase.AccountId)) ? '' : accountIdAndNameMap.get(newCase.AccountId)) + '-' + (String.isBlank(newCase.PatientConnect__PC_Engagement_Program__r.Name) ? '' : newCase.PatientConnect__PC_Engagement_Program__r.Name) + '-' + caseRcrdType;
      updatedCaselist.add(newCase);
    }
    if (!updatedCaselist.isEmpty()) {
      spc_Database.upd(updatedCaselist);
    }
  }

  /**************************************************************************************
  * @author      : Deloitte
  * @date        : 12/1/2018
  * @Description : Method to create patient authorization tasks
  * @Param       : Set of Id
  * @Return      : Void
  ***************************************************************************************/
  public void createPatientAuthorizationTasks(Set<Id> setNotCertifiedAccountIds) {

    Set<Id> filteredProgramCases = new Set<Id>();
    for (PatientConnect__PC_Association__c association : [SELECT PatientConnect__PC_Program__c, PatientConnect__PC_Program__r.OwnerId, PatientConnect__PC_Program__r.RecordTypeId, PatientConnect__PC_Program__r.Status
         FROM PatientConnect__PC_Association__c
         WHERE PatientConnect__PC_Account__c IN :setNotCertifiedAccountIds
         AND PatientConnect__PC_Program__r.spc_HIPAA_Consent_Received__c = : spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES)
             AND PatientConnect__PC_Program__r.spc_Patient_Services_Consent_Received__c = : spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES)
                 AND PatientConnect__PC_Role__c = :spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_SOC)
                     AND PatientConnect__PC_AssociationStatus__c = :spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)] ) {
      //If the Program related to the Site of Care Association status is anything but 'Completed' and 'Never Start'
      if (association.PatientConnect__PC_Program__r.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType)
          && association.PatientConnect__PC_Program__r.Status != spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_STATUS_COMPLETED)
          && association.PatientConnect__PC_Program__r.Status != spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_STATUS_NEVER_START)) {
        filteredProgramCases.add(association.PatientConnect__PC_Program__c);
      }
    }
    if (!filteredProgramCases.isEmpty()) {
      spc_TaskTriggerImplementation tasktriggerImplementation = new spc_TaskTriggerImplementation();
      tasktriggerImplementation.createFollowUpTasks(filteredProgramCases, System.today(), spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_PATIENT),
          spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_SUB_CAT_AUTHORIZATION),
          '', '', spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_HIGH_PRIORITY), spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_NOT_STARTED),
          spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_SUB_PATIENT_AUTHORIZATION));
    }
  }
}

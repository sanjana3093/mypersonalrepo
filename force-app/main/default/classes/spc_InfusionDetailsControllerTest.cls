/**
* @author Deloitte
* @date 07/18/2018
*
* @description This is the test class for spc_InfusionDetailsController
*/

@IsTest
public class spc_InfusionDetailsControllerTest {

    @testSetup static void setupTestdata() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
        Account patientAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        patientAcc1.PatientConnect__PC_Date_of_Birth__c = system.today();
        patientAcc1.spc_HIPAA_Consent_Received__c = '';
        patientAcc1.spc_Patient_Services_Consent_Received__c = '';
        patientAcc1.spc_Text_Consent__c = '';
        patientAcc1.spc_Patient_Mrkt_and_Srvc_consent__c = '';
        insert patientAcc1;
        Case programCaseRec2 = spc_Test_Setup.createCases(new List<Account> {patientAcc1}, 1, 'PC_Program').get(0);
        if (null != programCaseRec2) {
            programCaseRec2.Type = 'Program';

            insert programCaseRec2;
        }
        system.assertNotEquals(programCaseRec2, NULL);
    }
    public static testmethod void testExecution1() {
        Case oCase = [select id from Case];
        system.assertNotEquals(oCase, NULL);
        Case oCaseFinal = new Case();
        test.startTest();
        oCaseFinal = spc_InfusionDetailsController.checkConsentDetails(oCase.id);
        test.stopTest();
    }
    public static testmethod void testExecution2() {
        Case oCaseFinal = new Case();
        test.startTest();
        oCaseFinal = spc_InfusionDetailsController.checkConsentDetails(NULL);
        test.stopTest();
    }
}
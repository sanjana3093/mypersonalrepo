/**
* @author Deloitte
* @date Sept 21 2018
*
* @description This Test class is used for spc_HealthPlanTriggerImplementation
*/

@isTest
public class spc_HealthPlanTriggerImplementationTest {
    @testSetup static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

    }
    @isTest
    public static void testProcessEnrollment() {
        String primary = spc_ApexConstants.getHelthPlanType(spc_ApexConstants.HealthPlanType.HEALTH_PLANTYPE_PRIMARY);
        String secondary = spc_ApexConstants.getHelthPlanType(spc_ApexConstants.HealthPlanType.HEALTH_PLANTYPE_SECONDARY);
        String tertiary = spc_ApexConstants.getHelthPlanType(spc_ApexConstants.HealthPlanType.HEALTH_PLANTYPE_TERTIARY);
        String active = spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE;
        String inactive = spc_ApexConstants.ASSOCIATION_STATUS_INACTIVE;
        test.startTest();
        Account account = spc_Test_Setup.createTestAccount('PC_Patient');
        account.PatientConnect__PC_Email__c = 'sr@sr.com';
        account.PatientConnect__PC_Date_of_Birth__c = System.today() - 30;
        account.Phone = '712-123-1413';
        insert account;
        PatientConnect__PC_Health_Plan__c hp = spc_Test_Setup.createHealthPlan(account, primary, active);
        spc_Database.ins(hp);
        PatientConnect__PC_Health_Plan__c hp2 = spc_Test_Setup.createHealthPlan(account, secondary, active);
        spc_Database.ins(hp2);
        PatientConnect__PC_Health_Plan__c hp3 = spc_Test_Setup.createHealthPlan(account, tertiary, active);
        spc_Database.ins(hp3);
        Account account2 = spc_Test_Setup.createTestAccount('PC_Patient');
        account2.PatientConnect__PC_Email__c = 'sr12@sr.com';
        account2.Phone = '717-123-1414';
        account2.PatientConnect__PC_Date_of_Birth__c = System.today() - 10;
        insert account2;
        PatientConnect__PC_Health_Plan__c hpNew = spc_Test_Setup.createHealthPlan(account2, primary, inactive);
        spc_Database.ins(hpNew);
        hpNew.PatientConnect__PC_Plan_Status__c = spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE;
        spc_Database.upd(hpNew);
        PatientConnect__PC_Health_Plan__c hpNew2 = spc_Test_Setup.createHealthPlan(account2, secondary, inactive);
        spc_Database.ins(hpNew2);
        hpNew2.PatientConnect__PC_Plan_Status__c = spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE;
        spc_Database.upd(hpNew2);
        PatientConnect__PC_Health_Plan__c hpNew3 = spc_Test_Setup.createHealthPlan(account2, tertiary, inactive);
        spc_Database.ins(hpNew3);
        hpNew3.PatientConnect__PC_Plan_Status__c = spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE;
        spc_Database.upd(hpNew3);
        test.stopTest();

    }
}
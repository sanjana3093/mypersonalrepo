/*********************************************************************************

    *  @author          Deloitte
    *  @description     Associates physian with enrollment case
    *  @date            4-Jan-2017
    *  @version         1.0
*********************************************************************************/
global with sharing class spc_SOCEWPProcessor implements PatientConnect.PC_EnrollmentWizard.PageProcessor {
  /********************************************************************************************************
   *  @author           Deloitte
   *  @date             29/06/2018
   *  @description      Implemented method from PC_EnrollmentWizard.PageProcessor
   *  @param            enrollmentCase - Case Object
   *  @param        pageState - Map with field values from page
   *  @return           None
   *********************************************************************************************************/
  public static void processEnrollment(Case enrollmentCase, Map<String, Object> pageState) {
    ID programId = enrollmentCase.PatientConnect__PC_Program__c;
    if (pageState != null ) {


      String role = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_HCO);
      List<PatientConnect__PC_Association__c> existingHCOs;
      //getting the SOC values populated in EW based on pagestate vale
      for (String key : pageState.keySet()) {
        if (key == 'selectedResults') {
          if (programId != null) {
            List<PatientConnect__PC_Association__c> associations = new List<PatientConnect__PC_Association__c>();
            PatientConnect__PC_Association__c hco;
            Map<String, Object> selectedSOC = (Map<String, Object>) pageState.get(key);

            if (selectedSOC != null && !selectedSOC.isEmpty() && selectedSOC.get('Id') != null) {
              Account hcoAcc = new Account(id = (String)selectedSOC.get('Id'));
              hco = new PatientConnect__PC_Association__c();
              if (String.isNotBlank((String)selectedSOC.get('remsID'))) {

                hcoAcc.spc_REMS_ID__c = (String)selectedSOC.get('remsID');
                spc_Database.upd(hcoAcc);
              }
              hco.PatientConnect__PC_Program__c = programId;
              hco.PatientConnect__PC_Account__c = (String)selectedSOC.get('Id');
              hco.PatientConnect__PC_Role__c = role;
              //populating the address,phone and fax fields in association record
              if (String.isNotBlank((String)selectedSOC.get('addressId'))) {
                String addressId = (String)selectedSOC.get('addressId');
                hco.spc_Address__c = addressId;
              }
              if (String.isNotBlank((String)selectedSOC.get('assnFax'))) {
                String fax = (String)selectedSOC.get('assnFax');
                hco.spc_Fax__c = FormatPhone(fax);
              }
              if (String.isNotBlank((String)selectedSOC.get('assnPhone'))) {
                String phone = (String)selectedSOC.get('assnPhone');
                hco.spc_Phone__c = FormatPhone(phone);
              }
              associations.add(hco);
            }


            if (!associations.isEmpty()) {
              spc_Database.ups(associations);//FLS/CRUD Check enforced in the PC_database method
            }
          } else {
            throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException('HCO Processor: Program ID not found');
          }

        }
      }
      //making an interaction rcord based on the selected SOC values
      if (pageState.get('interaction') != null) {
        for (String key : pageState.keySet()) {
          if (key == 'interaction') {
            PatientConnect__PC_Interaction__c interactionLinkedToCase = null;

            if (programId != null) {
              Object persistentInteraction = (Object) pageState.get('interaction');
              Map<String, Object> persistedInteractionMap = (Map<String, Object>) persistentInteraction;
              PatientConnect__PC_Interaction__c interaction = null;
              if (String.isNotBlank((String) persistedInteractionMap.get('spc_SOC_Type__c'))) {
                if (null == interaction) {
                  interaction = new PatientConnect__PC_Interaction__c();
                }
                interaction.spc_SOC_Type__c = (String) (persistedInteractionMap.get('spc_SOC_Type__c'));
              }
              if (String.isNotBlank((String) persistedInteractionMap.get('spc_SOC_Type_Other__c'))) {
                if (null == interaction) {
                  interaction = new PatientConnect__PC_Interaction__c();
                }
                interaction.spc_SOC_Type_Other__c = (String) (persistedInteractionMap.get('spc_SOC_Type_Other__c'));
              }
              if (String.isNotBlank((String) persistedInteractionMap.get('socNameId'))) {
                if (null == interaction) {
                  interaction = new PatientConnect__PC_Interaction__c();
                }
                interaction.PatientConnect__PC_Participant__c = (String) (persistedInteractionMap.get('socNameId'));
              }
              if (String.isNotBlank((String) persistedInteractionMap.get(Label.spc_fax))) {

                Account accountHco = new Account(Id = (String) persistedInteractionMap.get('socNameId'));
                accountHco.fax = (String) persistedInteractionMap.get(Label.spc_fax);
                spc_Database.upd(accountHco);
              }
              if (null != interaction) {
                interaction.PatientConnect__PC_Patient_Program__c = enrollmentCase.PatientConnect__PC_Program__c;
                interactionLinkedToCase = (PatientConnect__PC_Interaction__c) spc_Database.ins(interaction);
              }
              if (interactionLinkedToCase != null ) {
                Case caseProgram = new Case(Id = programId, spc_Interaction_Id__c =  interactionLinkedToCase.id);
                spc_Database.upd(caseProgram);
              }
            } else {
              throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException(Label.PSC_Program_Id_Not_Found);
            }
          }
        }
      } else {
        throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException(Label.Missing_Interaction_Information);
      }
    } else {
      throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException(Label.Missing_Page_State);
    }
  }
  /********************************************************************************************************
  *  @author         Deloitte
  *  @date             24/10/2018
  *  @description      Returns formatted phone number
  *  @param           Phone number
  *  @return          Phone
  *********************************************************************************************************/

  private static String FormatPhone(String Phone) {
    string nondigits = '[^0-9]';
    string PhoneDigits;

    // remove all non numeric
    PhoneDigits = Phone.replaceAll(nondigits, '');

    // 10 digit: reformat with dashes
    if (PhoneDigits.length() == 10)
      return '(' + PhoneDigits.substring(0, 3) + ')' + '-' +
             PhoneDigits.substring(3, 6) + '-' +
             PhoneDigits.substring(6, 10);
    // 11 digit: if starts with 1, format as 10 digit
    if (PhoneDigits.length() == 11) {
      if (PhoneDigits.substring(0, 1) == '1') {
        return '(' + PhoneDigits.substring(1, 4) + ')' + '-' +
               PhoneDigits.substring(4, 7) + '-' +
               PhoneDigits.substring(7, 11);

      }
    }

    // if it isn't a 10 or 11 digit number, return the original because
    // it may contain an extension or special information
    return ( Phone );
  }

}
/********************************************************************************************************
    *  @author          Deloitte
    *  @description  	This is the test class for spc_PhysicianEWPController
    *  @date            08/10/2018
    *  @version         1.0
    *
*********************************************************************************************************/
@isTest
public class spc_PhysicianEWPControllerTest {

	public static Id ID_CAREGIVER_ACCOUNT_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Enrollment').getRecordTypeId();
	public static Id ID_Applicant_RECORDTYPE = Schema.SObjectType.PatientConnect__PC_Applicant__c.getRecordTypeInfosByName().get('Online').getRecordTypeId();
	public static Id ID_PHYSICIAN_ACCOUNT_RECORDTYPE  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Physician').getRecordTypeId();

	@testSetup static void setup() {
		List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
		spc_Database.ins(apexConstansts);
		Account acc = spc_Test_Setup.createTestAccount('Physician');
		acc.name = 'Physician Name test';
		acc.recordtypeid = ID_PHYSICIAN_ACCOUNT_RECORDTYPE;
		acc.PatientConnect__PC_Email__c = 'test@email.com';
		acc.PatientConnect__PC_First_Name__c = 'Test';
		insert acc;
		system.assertNotEquals(acc, NULL);

	Case enrollmentCase = new case ();
		enrollmentCase.AccountId = acc.id;
		enrollmentCase.type = 'inquiry';
		enrollmentCase.Status = 'new';
		enrollmentCase.recordtypeid = ID_CAREGIVER_ACCOUNT_RECORDTYPE;
		insert enrollmentCase;
		system.assertNotEquals(enrollmentCase, NULL);

		PatientConnect__PC_Applicant__c app = new PatientConnect__PC_Applicant__c();
		app.recordtypeid = ID_Applicant_RECORDTYPE;
		app.PatientConnect__PC_Physician_Name__c = 'Name';
		insert app;
		system.assertNotEquals(app, NULL);
	}

    @isTest
    public static void testGetFields(){
        test.startTest();
        spc_PhysicianEWPController.PhysicianRecord phyRec = spc_PhysicianEWPController.getFields();
        spc_PhysicianEWPController.getPicklistEntryMap();
        spc_PhysicianEWPController.getDependentOptionsImpl();
        test.stopTest();
    }

	@isTest
	public static void testGetApplicantPhysician() {
		test.startTest();
		Case enrCase = [SELECT Id from Case limit 1];
		PatientConnect__PC_Applicant__c applicant = [SELECT Id from PatientConnect__PC_Applicant__c limit 1];
		List<spc_PhysicianEWPController.PhysicianRecord> lstPhyRec = spc_PhysicianEWPController.getApplicantPhysician(enrCase.Id, applicant.Id);
		test.stopTest();
	}

	@isTest
	public static void testSearchRecords() {
		test.startTest();
		List<spc_PhysicianEWPController.PhysicianRecord> lstPhyRec = spc_PhysicianEWPController.searchRecords('test');
		test.stopTest();
	}

	@isTest
	static void testPhyRecordAccount() {
		test.startTest();
		Account acc = [SELECT Id, Name, PatientConnect__PC_Email__c, PatientConnect__PC_Primary_Address__c, PatientConnect__PC_First_Name__c, PatientConnect__PC_Last_Name__c, PatientConnect__PC_Primary_Address_1__c,
		               PatientConnect__PC_Primary_Address_2__c, PatientConnect__PC_Primary_Address_3__c, PatientConnect__PC_Primary_Zip_Code__c, PatientConnect__PC_Salutation__c, Phone,
		               Fax, Type, RecordTypeId, PatientConnect__PC_NPI__c, RecordType.Name, PatientConnect__PC_Communication_Language__c, PatientConnect__PC_Preferred__c,
		               PatientConnect__PC_Status__c, spc_REMS_ID__c, PatientConnect__PC_Specialist_Type__c, PatientConnect__PC_Primary_City__c, PatientConnect__PC_Primary_State__c
		               FROM Account WHERE RecordType.Name = 'Physician' limit 1];
		spc_PhysicianEWPController.PhysicianRecord phyRec = new spc_PhysicianEWPController.PhysicianRecord(acc);
		test.stopTest();
	}
	@isTest
	static void testGetPhysicianAddresses() {
		test.startTest();
		Account acc = spc_Test_Setup.createTestAccount('Physician');
		acc.name = 'John Balboa test';
		acc.recordtypeid = ID_PHYSICIAN_ACCOUNT_RECORDTYPE;

		acc.PatientConnect__PC_Email__c = 'test@email.com';
		acc.PatientConnect__PC_First_Name__c = 'Johnbalboa';
		insert acc;
		PatientConnect__PC_Address__c address = spc_Test_Setup.createTestAddress(acc);

		List<PatientConnect__PC_Address__c> addresses = spc_PhysicianEWPController.getPhysicianAddresses(acc.id);

		System.assert(addresses != null);
		test.stopTest();
	}
}
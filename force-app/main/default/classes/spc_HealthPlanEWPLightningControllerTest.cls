/**
* @author Deloitte
* @date 06/18/2018
*
* @description This is the Test Class for Health Plan Controller
*/
@isTest
public class spc_HealthPlanEWPLightningControllerTest {
    @testSetup static void setup() {
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
    }
    @isTest
    public static void testGetFieldDescribe() {
        Test.startTest();
        List<String> objectNames = new List<String>();
        objectNames.add('Case');
        Map<String, spc_HealthPlanEWPLightningController.FieldWrapper> pickLists = spc_HealthPlanEWPLightningController.getFieldDescribe(objectNames);
        System.assertEquals(True, pickLists.size() > 0);
        Test.stopTest();

    }
    @isTest
    public static void testGetFieldLabels() {
        Test.startTest();
        List<spc_FieldSetController.FieldSetMember> ts= spc_HealthPlanEWPLightningController.getFieldSetFields('Account','PatientConnect__EnrollmentWizardPhysicianPage');
        Map<String, spc_HealthPlanEWPLightningController.FieldWrapper> fieldLabels = spc_HealthPlanEWPLightningController.getFieldLabel();
        System.assertEquals(True, fieldLabels != null );
        
        Test.stopTest();

    }
    @isTest
    public static void testGetHealthPlan() {
        Test.startTest();
        Account patient = spc_Test_Setup.createTestAccount('PC_Patient');
        patient.PatientConnect__PC_Email__c = 'sr@sr.com';
        patient.Phone = '7121231413';
        patient.spc_Mobile_Phone__c = '7121231413';
        insert patient;

        List<spc_HealthPlanEWPLightningController.healthPlanPageWrapper> healthPlanWrapper = spc_HealthPlanEWPLightningController.getHealthPlan(patient.id, null);
        System.assertEquals(True, healthPlanWrapper != null );

        PatientConnect__PC_Health_Plan__c healthPlan = new PatientConnect__PC_Health_Plan__c();
        Account payer = spc_Test_Setup.createTestAccount('Payer');
        payer.PatientConnect__PC_Email__c = 'sr@sr.com';
        payer.Phone = '7121231413';
        payer.spc_Mobile_Phone__c = '7121231413';
        insert payer;
        healthPlan.PatientConnect__PC_Payer__c = payer.id;
        healthPlan.PatientConnect__Patient__c = patient.id;
        insert healthPlan;
        List<spc_HealthPlanEWPLightningController.healthPlanPageWrapper> healthPlanWrapper2 = spc_HealthPlanEWPLightningController.getHealthPlan(patient.id, null);
        System.assertEquals(True, healthPlanWrapper2 != null );
        Test.stopTest();
    }
    @isTest
    public static void testGetApplicantHealthPlans() {
        Test.startTest();
        Account patient = spc_Test_Setup.createTestAccount('PC_Patient');
        patient.PatientConnect__PC_Email__c = 'sr@sr.com';
        patient.Phone = '7121231413';
        patient.spc_Mobile_Phone__c = '7121231413';
        insert patient;

        PatientConnect__PC_Address__c  patAddress = spc_Test_Setup.createAddress(patient.Id);
        patAddress = (PatientConnect__PC_Address__c)spc_Database.ins(patAddress, false);

        Account manufacturer = spc_Test_Setup.createTestAccount('Manufacturer');
        manufacturer.PatientConnect__PC_Email__c = 'sr1@sr.com';
        manufacturer.Phone = '7121231413';
        manufacturer.spc_Mobile_Phone__c = '7121231413';
        insert manufacturer;

        PatientConnect__PC_Engagement_Program__c  engagement = spc_Test_Setup.createEngagementProgram('programA', manufacturer.Id);
        engagement = (PatientConnect__PC_Engagement_Program__c)spc_Database.ins(engagement, false);

        Case enrollment = spc_Test_Setup.newCase(patient.id, 'PC_Enrollment');
        insert enrollment;

        PatientConnect__PC_Applicant__c applicant = new PatientConnect__PC_Applicant__c( PatientConnect__PC_Last_Name__c = 'lastName',
                PatientConnect__PC_First_Name__c = 'Test First Name',
                PatientConnect__PC_Source__c = true);
        applicant.PatientConnect__PC_HP_Payer_Name__c = 'John, Nash';
        spc_Database.ins(applicant, false);

        enrollment.PatientConnect__PC_ActiveApplicant__c = applicant.Id;
        enrollment = (Case)spc_Database.ups(enrollment);
        String[] ignoreFields = new String[2];
        ignoreFields[0] = 'PatientConnect__PC_HP_Group_No__c';
        ignoreFields[1] = 'PatientConnect__PC_HP_ID_Policy_No__c';
        List<spc_HealthPlanEWPLightningController.healthPlanPageWrapper> healthPlanWrapper = spc_HealthPlanEWPLightningController.getApplicantHealthPlans(enrollment.id, applicant.id, null, ignoreFields, '', 'healthPlan');
        List<spc_HealthPlanEWPLightningController.healthPlanPageWrapper> healthPlanWrapper1 = spc_HealthPlanEWPLightningController.getHealthPlan(patient.id,'spc_HealthPlanEWP_FieldSet');
        PatientConnect__PC_Health_Plan__c healthPlan = new PatientConnect__PC_Health_Plan__c();
        Account payer = spc_Test_Setup.createTestAccount('Payer');
        payer.PatientConnect__PC_Email__c = 'sr@sr.com';
        payer.Phone = '7121231413';
        payer.spc_Mobile_Phone__c = '7121231413';
        insert payer;
        healthPlan.PatientConnect__PC_Payer__c = payer.id;
        healthPlan.PatientConnect__Patient__c = patient.id;
        insert healthPlan;
        List<spc_HealthPlanEWPLightningController.healthPlanPageWrapper> healthPlanWrapper2 = spc_HealthPlanEWPLightningController.getApplicantHealthPlans(enrollment.id, applicant.id, null, ignoreFields, '', 'healthPlan');
        Test.stopTest();
    }
    @isTest
    public static void testGetPayer() {
        Test.startTest();
        Account payer = spc_Test_Setup.createTestAccount('Payer');
        payer.PatientConnect__PC_Email__c = 'sr@sr.com';
        payer.Phone = '7121231413';
        payer.spc_Mobile_Phone__c = '7121231413';
        insert payer;
        List<spc_HealthPlanEWPLightningController.payerWrapper> payerWrappers = spc_HealthPlanEWPLightningController.getPayer('');

        List<spc_HealthPlanEWPLightningController.payerWrapper> payerWrappers2 = spc_HealthPlanEWPLightningController.getPayer('SELECT Id, Name, PatientConnect__PC_Status__c, PatientConnect__PC_Email__c, Phone, Fax, Type, RecordTypeId, SELECT Id, Name, PatientConnect__PC_Status__c, PatientConnect__PC_Email__c, Phone, Fax, Type, RecordTypeId FROM Account WHERE Account.RecordType.Name = :recordType');
        Test.stopTest();
    }

}
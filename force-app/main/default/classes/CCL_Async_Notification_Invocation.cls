public with sharing class CCL_Async_Notification_Invocation implements queueable {
    List<CCL_Notification_Invocable.CCL_Notification_Invocable_Wrapper> listOfNotificationWrapper = new List<CCL_Notification_Invocable.CCL_Notification_Invocable_Wrapper>();
   
    public CCL_Async_Notification_Invocation(List<CCL_Notification_Invocable.CCL_Notification_Invocable_Wrapper> listOfNotificationWrapper){
        this.listOfNotificationWrapper=listOfNotificationWrapper;
        
    }
    public void execute(QueueableContext context) {
        CCL_Notification_Invocable.methodToBeCalledFromAsyncNotificationProcess(this.listOfNotificationWrapper);
    }
}
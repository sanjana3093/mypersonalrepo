/****
@description   Wrapper classes for Lightning Map
@author          Deloitte
*/
public class spc_LightningMap {

    public spc_LightningMap(String cName, String cValue) {
        name = cName;
        value = cValue;
    }

    public spc_LightningMap(String cName, String cValue, Boolean fax, boolean email) {
        name = cName;
        value = cValue;
        showFaxRow = fax;
        showEmailRow = email;
    }
    @AuraEnabled
    String name {get; set;}

    @AuraEnabled
    String value {get; set;}

    @AuraEnabled
    boolean showFaxRow {get; set;}

    @AuraEnabled
    boolean showEmailRow {get; set;}
}
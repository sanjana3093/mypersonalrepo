/*********************************************************************************
@author Deloitte
@description This is for referencing literals in Apex code from custom setting
*********************************************************************************/
global virtual class spc_ApexConstants {
    //Static map to hold literals referenced in currrent transaction
    private static Map<String, String> mapLiterals;
    private static Map<String, Id> mapRecordTypeLiterals;

    public static final String ELETTER_FORM_FIELD_SET = 'PatientConnect__PC_eLetter__c';
    public static final String ENG_ELETTER_FORM_FIELD_SET = 'PatientConnect__PC_Engagement_Program_Eletter__c';
    public static boolean IsEWPRunning = false;
    public static final String FAX = 'fax';
    public static final String LASH = 'Lash';
    public static final String EMAIL = 'email';
    public static final String MAIL = 'Mail';
    public static final String SMS = 'SMS';
    public static final String ELetter = 'eLetter ';
    public static final String SAGE_THERAPEUTICS_ACCOUNT_NAME = 'Sage Therapeutics, Inc.';
    public static final String CASE_PROGRAM_RECORD_TYPE = 'PC_Program';
    public static final String ACCOUNT_PHARMACY_DEVELOPER_NAME = 'PC_Pharmacy';
    public static final String ACCOUNT_MANUFACTURER_DEVELOPER_NAME = 'Manufacturer';
    public static final String CASE_PRETREATMENT_RECORD_TYPE = 'spc_Pre_Treatment';
    public static final String CASE_POSTTREATMENT_RECORD_TYPE = 'spc_Post_Treatment';
    public static final String CASE_INQUIRY_RECORD_TYPE = 'spc_Inquiry';
    public static final String CASE_APPEAL_RECORD_TYPE = 'PC_Appeal';
    public static final String CASE_BI_BV_RECORD_TYPE = 'PC_Benefits_Investigation_Benefits_Verification';
    public static final String CASE_PA_RECORD_TYPE = 'PC_Prior_Authorization';
    public static final String DOCUMENT_OBJECT = spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Document__c');
    public static final String DOCUMENT_OBJECT_API = 'PatientConnect__PC_Document__c';
    public static final String spc_HEALTH_PLAN = spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Health_Plan__c');
    public static final String DOCUMENT_TYPE_FAX_OUTBOUND = 'Fax - Outbound';
    public static final String DOCUMENT_TYPE_FAX_INBOUND = 'Fax - Inbound';
    public static final String DOCUMENT_TYPE_EMAIL_INBOUND = 'Email - Inbound';
    public static final String DOCUMENT_TYPE_SMS_INBOUND = 'SMS - Inbound';
    public static final String DOCUMENT_TYPE_MANUAL = 'Manual Upload';
    public static final String DOCUMENT_STATUS = 'Ready for Send';
    public static final String INQUIRY_PROGRAM_RECORD_TYPE_NAME = 'Inquiry';
    public static final String PROGRAM_RECORD_TYPE_NAME = 'Program';
    public static final String PATIENT_INTRESTED_AMBASSADOR_YSTATUS =  'Yes';
    public static final String BI_RECORD_TYPE_NAME = 'BI / BV';
    public static final String CERTIFICATION_RECORD_TYPE_NAME = 'Certification/Enrollment';
    public static final String PC_CASE = 'Case';
    public static final String PC_Patient = 'PC_Patient';
    public static final String OwnerTypeUser = 'User';
    public static final String Doc_ContantType = 'text/plain';
    public static final String HTML_ContentType = 'html';
    public static final String PC_CASE_DISCONTINUED = 'Discontinued';
    public static final String PC_CASE_NEVER_START = 'Never Start';
    public static final String PROGRAM_COVERAGE_STATUS_ACTIVE = 'Active';
    public static final String PROGRAM_COVERAGE_TYPE_DRUGCOPAY = 'Drug Copay';
    public static final String PROGRAM_COVERAGE_TYPE_ADMINCOPAY = 'Admin Copay';
    public static final String PROGRAM_COVERAGE_RT_PUBLIC_COVERAGE = 'Medical Coverage';
    public static final String PROGRAM_COVERAGE_RT_PHARMACY_PLAN = 'Pharmacy Coverage';
    public static final String OBJ_PROGRAM_COV = 'Program Coverage';
    public static final String OBJ_INTERACTION = 'Interaction';
    public static final String AC_COMPLETE = 'Complete';
    public static final String WC_COMPLETE = 'Complete';
    public static final String REASON_OTHER = 'Other';
    public static final String DOC_STATUS_FAILED = 'Failed';
    public static final String DOC_STATUS_REVIEW_NEEDED = 'Review';
    public static final String DOCUMENT_FIELD_SET = 'PC_Document_FieldSet';
    public static final String SYSTEM_ADMINISTRATOR = 'System Administrator';
	public static final String NONE_PICKLIST_VALUE = '--None--';

    public static final String Application_ContantType = 'application/pdf';
    public static final String Attachment_Subtype = 'texttwo/plain';
    public static final String Attachment_FileName = 'textfiletwo3.txt';
    public static final String Err_msg = 'Email operation Failed';

    /* C TEMPLATE */
    public static final String SMS_TEMPLATE = 'spc_SMS_Template__c';
    public static final String EMAIL_TEMPLATE = 'spc_Email_Template__c';
    public static final String FAX_TEMPLATE = 'spc_Fax_Template__c';
    public static final String POSTAL_TEMPLATE = 'spc_Postal_Mail_Template__c';
    private static String OrgNS;

    /*Status Indicators*/
    public static final String STATUS_INDICATOR_COMPLETE = 'Complete';
    public static final String STATUS_INDICATOR_INCOMPLETE = 'Incomplete';
    public static final String STATUS_INDICATOR_NOTSTARTED = 'Not Started';
    public static final String STATUS_INDICATOR_INPROGRESS = 'In Progress';
    public static final String STATUS_INDICATOR_NOT_APPLICABLE = 'Not applicable';
    public static final String STATUS_INDICATOR_ACTION_REQUIRED = 'Action Required';
    public static final String STATUS_INDICATOR_NONE = '';
    public static final String STATUS_INDICATOR_ONHOLD = 'On Hold';
    public static final String APPLICANT_ONLINE_RECORD_TYPE_NAME = 'Online';
    public static final String PAYER_RECORD_TYPE = 'Payer';

    public static final String ACCOUNT_STATUS_ACTIVE = 'Active';


    /*ASSOCIATION Status */
    public static final String ASSOCIATION_STATUS_ACTIVE = 'Active';
    public static final String ASSOCIATION_ROLE_CAREGIVER = 'Caregiver';
    public static final String ASSOCIATION_ROLE_HCO = 'HCO';
    public static final String ASSOCIATION_ROLE_FREE_DRUG_VENDOR = 'Free Drug Vendor';
    public static final String ASSOCIATION_ROLE_SPHARMACY = 'Specialty Pharmacy';
    public static final String ASSOCIATION_ROLE_PHARMACY = 'Pharmacy';
    public static final String ASSOCIATION_ROLE_TREATING_PHY = 'Treating Physician';
    public static final String ASSOCIATION_ROLE_PRES_PHY = 'Prescribing Physician';
    public static final String ASSOCIATION_STATUS_INACTIVE = 'Inactive';
    public static final String ASSOCIATION_ROLE_DESIGNATED_CAREGIVER = 'Designated Caregiver';
    public static final String ASSOCIATION_ROLE_EXTERNAL_COMPOUNDING_PHARMACY = 'External Pharmacy';

    /*Profile Name*/
    public static final string PROFILE_CASE_MANAGER = 'Case Manager';
    public static final string USER_ROLE_SUPERVISOR = 'Supervisor';
    public static final string PROFILE_SALES_REP = 'Sales Rep';
    public static final string MARKET_ACCESS_REP = 'Market Access';
    public static final string SAGE_EXT_USER = 'SAGE Patient Services External Partner';


    public static final String INTERACTION_STATUS_ATTENDED = 'Attended';
    public static Id ID_INQUIRYCASE_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
    public static Id ID_PATIENTACCOUNT_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
    public static Id ID_POST_TREATMENTCASE_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Closure Case').getRecordTypeId();
    public static Id ID_PRE_TREATMENTCASE_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pre-Treatment').getRecordTypeId();
    public static Id ID_PATIENT_RECORDTYPE = ID_PATIENTACCOUNT_RECORDTYPE;
    public static Id ID_MANUFACTURER_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId();
    public static Id ID_PROGRAMCASE_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId();
    public static Id ID_PHYSICIAN_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Physician').getRecordTypeId();
    public static Id ID_EMAIL_INBOUND_RECORDTYPE = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get('Email - Inbound').getRecordTypeId();
    public static Id ID_EMAIL_OUTBOUND_RECORDTYPE = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get('Email - Outbound').getRecordTypeId();
    public static Id ID_FAX_OUTBOUND_RECORDTYPE = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get('Fax - Outbound').getRecordTypeId();
    public static Id ID_TASK_ELETTER_RECORDTYPE = Schema.SObjectType.Task.getRecordTypeInfosByName().get('eLetter').getRecordTypeId();
    public static Id ID_MAIL_OUTBOUND_RECORDTYPE = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get('Mail - Outbound').getRecordTypeId();

    public static final String PICKLIST_NONE = '__none';
    public static final String PICKLIST_NONE_VAL = 'None';

    public static Id ID_CAREGIVER_ACCOUNT_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId();

    /*Account Individual Type Status*/
    public static final String INDIVIDUAL_TYPE_BUSINESS = 'Business';

    public static final String STATUS_ENROLLED = 'Enrolled';
    public static final String STATUS_NEW = 'New';
    public static final String CASE_PROGRAM_RECORD_TYPE_NAME = 'Program';
    public static final String CASE_COPAY_RECORD_TYPE_NAME = 'Copay';
    public static final String CASE_POST_TREAT_RECORD_TYPE_NAME = 'Closure Case';



    //default static constructor
    static {
        mapLiterals = new Map<String, String>();
        mapRecordTypeLiterals = new Map<String, Id>();
    }

    //this method return literal value
    //ApexConstants.getValue(ApexConstants.PicklistValue.CASE_TYPE_PROGRAM);
    public static string getValue(PicklistValue eVal) {
        String val = String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, spc_ApexConstantsSetting__c.getInstance(val).spc_Value__c);
        }
        return mapLiterals.get(val);
    }

    public static String getConsentTextValue(ConsentTextValue eVal) {
        String val = String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, spc_ApexConstantsSetting__c.getInstance(val).spc_Value__c);
        }
        return mapLiterals.get(val);
    }

    public enum ConsentTextValue {
        REMSCONSENT,
        ServiceConsent,
        REMSTextConsent,
        MarketingConsent,
        MarketingTextConsent,
        ServiceTextConsent,
        ELETTER_FORM_FIELD_SET,
        NOCONSENT,
        DEFAULT_CONSENT

    }

    public static string getRecordTypeName(RecordTypeName eVal) {
        String val = String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, spc_ApexConstantsSetting__c.getInstance(val).spc_Value__c);
        }
        return mapLiterals.get(val);
    }

    public static string getAssociaionRoles(AssociaionRoles eVal) {
        String val = String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, spc_ApexConstantsSetting__c.getInstance(val).spc_Value__c);
        }
        return mapLiterals.get(val);
    }
    public static string getCareTeamRoles(CareTeamRoles eVal) {
        String val = String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, spc_ApexConstantsSetting__c.getInstance(val).spc_Value__c);
        }
        return mapLiterals.get(val);
    }

    //Use getRTId method going forward . this method should be replaced and remvoed
    public static Id getRecordTypeId(RecordTypeName eVal, SObjectType objectType) {
        String val =  String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, spc_ApexConstantsSetting__c.getInstance(val).spc_Value__c);
        }
        string rtName =  mapLiterals.get(val);
        if (! mapRecordTypeLiterals.containsKey(objectType + ':' + rtName)) {
            mapRecordTypeLiterals.put(objectType + ':' + rtName
                                      , objectType.getDescribe().getRecordTypeInfosByName().get(rtName).getRecordTypeId());
        }
        return  mapRecordTypeLiterals.get(objectType + ':' + rtName);
    }

    //Use this method going forward . Ealier method should be replaced and remvoed
    public static Id getRTId(RecordTypeName eVal, SObjectType objectType) {
        String val =  String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, spc_ApexConstantsSetting__c.getInstance(val).spc_Value__c);
        }
        string rtName =  mapLiterals.get(val);
        if (! mapRecordTypeLiterals.containsKey(objectType + ':' + rtName)) {
            mapRecordTypeLiterals.put(objectType + ':' + rtName
                                      , objectType.getDescribe().getRecordTypeInfosByDeveloperName().get(rtName).getRecordTypeId());
        }
        return  mapRecordTypeLiterals.get(objectType + ':' + rtName);
    }

    public static string getHelthPlanType(HealthPlanType eVal) {
        String val = String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, spc_ApexConstantsSetting__c.getInstance(val).spc_Value__c);
        }
        return mapLiterals.get(val);
    }
    public static string getUSerRole(USERRoles eVal) {
        String val = String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, spc_ApexConstantsSetting__c.getInstance(val).spc_Value__c);
        }
        return mapLiterals.get(val);
    }
    public static string getAdditionalPicklistValues(AdditionalPicklistValues eVal) {
        String val = String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, spc_ApexConstantsSetting__c.getInstance(val).spc_Value__c);
        }
        return mapLiterals.get(val);
    }

    public static string getCoverageType(CoverageType eVal) {
        String val = String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, spc_ApexConstantsSetting__c.getInstance(val).spc_Value__c);
        }
        return mapLiterals.get(val);
    }
    public static string getSGIPlatformEvent(SGIPlatformEvent eVal) {
        String val = String.valueOf(eVal);
        if (! mapLiterals.containsKey(val)) {
            mapLiterals.put(val, spc_ApexConstantsSetting__c.getInstance(val).spc_Value__c);
        }
        return mapLiterals.get(val);
    }

    public enum RecordTypeName {
        Account_Patient,
        ACCOUNT_RT_CAREGIVER,
        ACCOUNT_RT_PHARMACY,
        ACCOUNT_RT_PHYSICIAN,
        ACCOUNT_RT_HCO,
        CASE_RT_INQUIRY,
        CASE_RT_PA,
        CASE_RT_PROGRAM,
        CASE_RT_REFERRAL,
        CASE_RT_PRE_TREATMENT,
        CASE_RT_POST_TREATMENT,
        CASE_RT_ENROLLMENT,
        CASE_RT_APPEAL,
        CASE_RT_CERTIFICATION,
        CASE_RT_CERTIFICATION_LABEL,
        DOC_RT_MAIL_OUTBOUND,
        DOC_RT_EMAIL_OUTBOUND,
        DOC_RT_EMAIL_INBOUND,
        DOC_RT_FAX_INBOUND,
        DOC_RT_SMS_INBOUND,
        DOC_RT_FAX_OUTBOUND,
        PROGRAMCOVERAGE_RT_COPAY,
        PROGRAMCOVERAGE_RT_PAP,
        PROGRAMCOVERAGE_RT_MEDICALCOVERAGEPRIV,
        PROGRAMCOVERAGE_RT_PHARMACYCOVERAGE,
        PROGRAMCOVERAGE_RT_MEDICALCOVERAGE,
        PCACCOUNT_RT_PHARMACY,
        BI_RECORD_TYPE_NAME,
        ACCOUNT_RTDEV_PHARMACY,
        ACCOUNT_RTDEV_PHYSICIAN,
        ACCOUNT_RTDEV_HCO,
        ACCOUNT_RTDEV_PAYER,
        CASE_PROGRAM_RTTYPE,
        ACCOUNT_PATIENT_RTTYPE,
        CASE_BIBV_RTTYPE,
        HP_RT_PRIVATE_HEALTH_PLAN,
        PROGRAMCOVERAGE_RT_MEDICALCOVERAGEPUB
    }

    //Indicator status
    public enum IndicatorIcons {
        PC_BIBV,
        PC_AI,
        PC_AC,
        PC_MI,
        PC_SOC,
        PC_WC,
        PC_Adherence,
        PC_Consent,
        PC_Coverage,
        PC_Engagement,
        PC_Labs
    }
    public enum PicklistValue {
        TASK_HIGH_PRIORITY,
        ACTIVITY_STATUS_COMPLETED,
        PATIENT_STATUS_INDICATOR_COMPLETE,
        EMAIL_SEND_CONSID_BROCHURE_SUBJECT,
        DOCUMENT_EMAIL_ADDRESS1,
        WELCOME_CALL_ADDITIONAL_ATTEMPTS,
        DOCUMENT_EMAIL_ADDRESS2,
        ZPAPER_ZSTACK_NAME,
        EMAIL_SENDER_CLASS_FOR_ELETTER,
        CASE_FIELD_SUBJECT1,
        CASE_FIELD_SUBJECT2,
        CASE_STATUS_OPEN,
        DOC_STATUS_READYTOSEND,
        DOC_STATUS_SENTDOCUSIGN,
        DOC_STATUS_SENT,
        DOCUMENT_FAX_NO,
        EMAIL_TEMP_SEND_CONSID_BROCHURE,
        ORG_WIDE_EMAIL_2,
        STATIC_RESOURCE_CONSID_BROCHURE,
        TASK_CAT_INQUIRY,
        TASK_DIR_OUTBOUND,
        TASK_PRIORITY_NORMAL,
        TASK_STATUS_NOT_STARTED,
        TASK_SUB_CAT_CONTACT_PATIENT,
        TASK_SUB_FOLLOWUP_PATIENT,
        TASK_CHANNEL_CALL,
        ACCOUNT_RT_PATIENT,
        ACCOUNT_RT_HCO,
        ACCOUNT_VERBAL_CONSENT_RECEIVED,
        APPLICANT_ONLINE_RECORD_TYPE_NAME,
        ACCOUNT_RT_CAREGIVER,
        ACCOUNT_RT_MANUFACTUR,
        ACCOUNT_RT_PHYSICIAN,
        DOC_RT_EMAIL_INBOUND,
        DOC_RT_EMAIL_OUTBOUND,
        DOC_RT_MAIL_OUTBOUND,
        DOC_RT_FAX_OUTBOUND,
        DOC_RT_FAX_INBOUND,
        TASK_RT_ELETTER,
        CASE_COVERAGE_ACTION_REQUIRED,
        INTERACTION_HIP_OTHER,
        INTERACTION_HIP_CORAM,
        INTERACTION_HIP_OPTION_CARE,
        TASK_SUB_FOLLOWUP_PAT_POSTTREATMENT,
        TASK_CAT_POSTADMINISTRATION,
        TASK_SUB_CAT_FOLLOWUP_POSTTREATMENT,
        TASK_CHANNEL_EMAIL,
        TASK_CALL_OUTCOME_REACHED,
        TASK_CALL_OUTCOME_NEVER_START,
        TASK_SUB_SEND_POSTTREATMENT_PATIENT,
        TASK_SUB_CAT_POSTTREATMENT_SURVEY,
        TASK_CAT_WELCOMECALL,
        TASK_STATUS_COMPLETED,
        TASK_CAT_DISCONTINUATION,
        TASK_SUBCATEGORY_SECONDATTEMPT,
        TASK_SUBCATEGORY_THIRDATTEMPT,
        TASK_SUBCATEGORY_ADDITIONALATTEMPT,
        TASK_SUBCATEGORY_CONTACT_HCP,
        TASK_SUB_PERFORM_WELCOME_CALL,
        TASK_SUB_PERFORM_WELCOME_CALL_2,
        TASK_SUB_PERFORM_WELCOME_CALL_3,
        TASK_SUB_PATIENT_DISCONTINUATION,
        TASK_SUB_PATIENT_DISCONTINUATION_2,
        DOC_RT_MANUAL_UPLOAD,
        DOC_STATUS_REVIEW_NEEDED,
        TASK_SUB_LIVECHAT,
        TASK_TYPE_CHAT,
        ELETTER_SENDER_CLASS,
        ACCOUNT_HIPAA_CONSENT_RECEIVED_NO,
        ACCOUNT_HIPAA_CONSENT_RECEIVED_YES,
        CASE_STATUS_INDICATOR_1_COMPLETE,
        CASE_STATUS_INDICATOR_1_IN_PROGRESS,
        CASE_STATUS_CLOSE,
        CASE_BITYPE_MEDICAL,
        CASE_BITYPE_PHARMACY,
        ASSOCIATION_ROLE_PAYER,
        ASSOCIATION_STATUS_ACTIVE,
        ASSOCIATION_ROLE_HCO,
        ASSOCIATION_ROLE_CAREGIVER,
        ASSOCIATION_ROLE_SPHARMACY,
        ZPAPER_FAXTEMPLATE_NAME
        , DOCUSIGN_STATUS_SIGNED,
        INTERACTION_SUB_EVALUATE_COVERAGE,
        TASK_SUB_CAT_EVALUATE_COVERAGE,
        TASK_CAT_COVERAGE,
        TASK_SUB_CALL_HCP_SOC,
        INTRACTN_REASON_COVERAGE_DENIED,
        INTRTN_REASON_PA_DENIED,
        INTRACTIN_REASON_MEDICATION,
        INTRCTN_REASON_PATIENT_DECEASED,
        TASK_CAT_SITE_OF_CARE,
        TASK_SUB_CAT_CONTACT_SITE_OF_CARE,
        TASK_SUB_CALLPATIENT_POSTTREATMENT,
        TASK_CAT_POST_TREATMENT,
        TASK_SUB_CAT_FIRST_ATTEMPT,
        INTRCTN_MILESTONE_PREP,
        INTRACTION_STATUS_COMPLETE,
        INTRCTN_MILESTONE_ADMIN,
        COVERAGE_OUTCOME_COVERED,
        CASE_STATUS_ENROLLED
    }
    public enum AdditionalPicklistValues {
        TASK_DIRECTION_INBOUND,
        TASK_SUB_CAT_MANUAL_UPLOAD,
        TASK_SUB_REVIEW_MANUALLY_UPLOAD_DOC,
        TASK_SUB_CAT_NEW_INBOUND_DOC,
        TASK_CAT_DOCUMENT_REVIEW,
        CASE_OUTCOME_NOT_NEEDED,
        CASE_OUTCOME_NOT_CANCELED,
        TASK_CAT_PATIENT,
        TASK_SUB_CAT_AUTHORIZATION,
        TASK_SUB_CHECK_REFERRAL_FORM,
        TASK_CALL_OUTCOME_NOT_NEEDED,
        TASK_CAT_REFERRAL,
        CASE_STATUS_COMPLETED,
        CASE_STATUS_NEVER_START,
        CASE_ENROLLMENT_STATUS_UNENROLLED,
        CASE_ENROLLMENT_STATUS_COMPLETED,
        ASSOCIATION_ROLE_SOC,
        ACCOUNT_REMS_CERTIFIED_STATUS,
        ACCOUNT_REMS_NOT_CERTIFIED_STATUS,  /*****Adding new values for US302027****/
        CASE_ENROLLMENT_STATUS_AUTHORIZED,
        CASE_ENROLLMENT_STATUS_NOT_AUTHORIZED,
        TASK_CAT_WELCOMECALLITE,
        TASK_CAT_SOC_LOGISTIC_CALL,
        ASSOCIATION_ROLE_TREATING_PHYSICIAN, // Used for prescribing physician as treating physician is translated to prescribing physician for label change
        ASSOCIATION_ROLE_REFERRING_PHYSICIAN,
        ASSOCIATION_ROLE_EXT_COMP_PHARMACY,
        ASSOCIATION_ROLE_SPECIALTY_PHARMACY,
        ASSOCIATION_ROLE_DESIGNATED_CAREGIVER,
        ASSOCIATION_STATUS_INACTIVE,
        CASE_STATUS_PRETREATMENT,
        CASE_STATUS_INDICATOR_1_NOT_APPLICABLE,
        CHANNEL_PHONE,
        CHANNEL_FAX,
        TASK_SUBJECT_FAX_PATIENT_ENROLLMENT_IP,
        TASK_SUBCATEGORY_FAX,
        TASK_SUB_REINSTATE_PATIENT,
        TASK_SUB_PATIENT_AUTHORIZATION,
        TASK_CAT_BIBV,
        TASK_SUB_CAT_BIBV_COMPLETE,
        TASK_CAT_CLOSURE_CALL,
        TASK_SUB_CAT_POST_INFUSION,
        TASK_SUB_CAT_NEVER_START,
        TASK_SUB_NEVER_START_CLOSURE_CALL,
        TASK_SUB_POST_TREATMENT_CLOSURE_CALL,
        ACCOUNT_CERTIFICATION_STATUS_CERTIFIED,
        CASE_BIBV_TYPE_MEDICAL,
        CASE_BIBV_TYPE_PHARMACY,
        CASE_ENROLLMENT_STATUS_ENROLLED,
        TASK_CAT_MISSING_INFORMATION,
        TASK_CAT_OVERDUE_PIC,
        CASE_PIC_STATUS_ONHOLD,
        TASK_CAT_CONFIRM_INFUSION,
        TASK_STATUS_NEVER_STARTS,
        TASK_CAT_BI,
        TASK_SUB_CAT_FOLLOW_UP,
        TASK_SUBJ_FollowUP,
        FREE_DRUG_VENDOR,
        BREXANALONE_FAX_DOC_BODY,
        PROGRAM_COVERAGE_STATUS_ACTIVE,
        PC_CASE_DISCONTINUED,
        INDIVIDUAL_TYPE_INDIVIDUAL,
        HEALTHPLAN_STATUS_ACTIVE,
		PROGRAM_COVERAGE_STATUS_INACTIVE
    }
    public enum AssociaionRoles {
        ASSOCIATION_ROLE_PHARMACY
    }

    public enum HealthPlanType {
        HEALTH_PLANTYPE_PRIMARY,
        HEALTH_PLANTYPE_SECONDARY,
        HEALTH_PLANTYPE_TERTIARY
    }
    public enum USERRoles {
        USER_ROLE_SUPERVISOR,
        USER_ROLE_CASEMANAGER,
        USER_ROLE_SALESREP
    }
    public enum CareTeamRoles{
        CARETEAM_ROLE_MARKETACCESS,
        EXTENDED_CARETEAM_KEY_ACCOUNT_MANAGER    
    }
    public enum CoverageType {
        PROGRAM_COVERAGE_TYPE_PAP,
        PROGRAM_COVERAGE_TYPE_DRUGCOPAY,
        PROGRAM_COVERAGE_ADMIN_COPAY,
        PHARMACY_PLAN_COVERAGE,
        PUBLIC_COVERAGE,
        PRIVATE_COVERAGE
    }

    public enum SGIPlatformEvent {
        EVENT_TYPE_SGI,
        SOURCE_OBJECT_DOCUMENT,
		ADVERSE_EVENT
    }

    public static String OrgNamespace {
        get {
            if (String.isBlank(OrgNS)) {
                String className = spc_ApexConstants.class.getName();
                if (className.contains('.')) {
                    OrgNS = className.substringBefore('.');
                } else {
                    OrgNS = '';
                }
            }
            return OrgNS;
        }
    }

    public static String getQualifiedAPIName(String objectName) {
        String ns = spc_ApexConstants.OrgNamespace;
        if (String.isBlank(ns)) {
            return objectName;
        } else {
            return ns + '__' + objectName;
        }
    }
}
/********************************************************************************************************
  *  @author          Deloitte
  *  @date            13/06/2018
  *  @description   spc_ZipToTerrMapper
  *  @version         1.0
  *********************************************************************************************************/

public class spc_ZipToTerrMapper  {
	//Map for User Role to Region namespace
	Map<String, String> mapUserRoles ;
	public spc_ZipToTerrMapper(Set<String> userRoles) {
		mapUserRoles = new Map<String, String>();
		//Do the query on custom metadata type and found region namepace for user roles
		for (spc_Patient_Service_Region_Mapping__mdt setting : [SELECT spc_Region_Namespace__c, spc_User_Role__c
		        FROM spc_Patient_Service_Region_Mapping__mdt
		        WHERE spc_User_Role__c in: userRoles]) {
			mapUserRoles.put(setting.spc_Region_Namespace__c, setting.spc_User_Role__c);
		}
	}
	/********************************************************************************************************
	  *  @author          Deloitte
	  *  @date            13/06/2018
	  *  @description   This method accepts parameters as accountIDs and return Map of AccountIds to UserIDs based on user role
	  *  @version         1.0
	  *********************************************************************************************************/
	public Map<String, Map<String, String>> getUsers(Set<String> accountIds) {
		//Fine ZipCode to Accounts
		Map<String, Map<String, String>> mapAccountIdsToUserMapping = new Map<String, Map<String, String>>();
		//Find zipCode to Accounts
		Map<String, Set<Id>> mapZipCodesToAccountIds = this.getAccountZipCode(accountIds);
		//Find Territories based on zipCodes
		Map<String, Set<String>> mapTerrToZipCodes = this.getTerrToZipCodes(mapZipCodesToAccountIds.keySet());
		//Find users in specific territories
		Map<String, String> mapTerrToUsers = this.getUserWithRegions(mapTerrToZipCodes.keySet());
		for (Territory territoryName : [Select Id, Name, DeveloperName from Territory where Name in : mapTerrToUsers.keySet() AND DeveloperName LIKE 'Y%' ]) {
			if (mapTerrToZipCodes.containsKey(territoryName.Name) && String.isNotBlank(territoryName.Name)) {
				for (string zipCode : mapTerrToZipCodes.get(territoryName.Name)) {
					if (mapZipCodesToAccountIds.containsKey(zipCode)) {
						for (string accountId : mapZipCodesToAccountIds.get(zipCode)) {
							String territoryRole = spc_ApexConstants.PROFILE_CASE_MANAGER;
							if (! mapAccountIdsToUserMapping.containsKey(accountId)) {
								mapAccountIdsToUserMapping.put(
								    accountId
								    , new Map<String, String> {territoryRole => mapTerrToUsers.get(territoryName.Name)});
							}
							mapAccountIdsToUserMapping.get(accountId).put(territoryRole , mapTerrToUsers.get(territoryName.Name));
						}
					}
				}
			}
		}
		for (Territory territoryName : [Select Id, Name, DeveloperName from Territory where Name in : mapTerrToUsers.keySet() AND DeveloperName LIKE 'X%' ]) {
			if (mapTerrToZipCodes.containsKey(territoryName.Name) && String.isNotBlank(territoryName.Name)) {
				for (string zipCode : mapTerrToZipCodes.get(territoryName.Name)) {
					if (mapZipCodesToAccountIds.containsKey(zipCode)) {
						for (string accountId : mapZipCodesToAccountIds.get(zipCode)) {
							String territoryRole = spc_ApexConstants.PROFILE_SALES_REP;
							if (! mapAccountIdsToUserMapping.containsKey(accountId)) {
								mapAccountIdsToUserMapping.put(
								    accountId
								    , new Map<String, String> {territoryRole => mapTerrToUsers.get(territoryName.Name)});
							}
							mapAccountIdsToUserMapping.get(accountId).put(territoryRole , mapTerrToUsers.get(territoryName.Name));
						}
					}
				}
			}
		}
		return mapAccountIdsToUserMapping;
	}
	/********************************************************************************************************
	  *  @author          Deloitte
	  *  @date            13/06/2018
	  *  @description     This method accepts parameters as accountIDs and return map of zipCode to AccountIds.
	  *  @version         1.0
	  *********************************************************************************************************/
	private Map<String, Set<Id>> getAccountZipCode(Set<string> accountIds) {
		Map<String, Set<Id>> mapZipCodeToAccounts = new Map<String, Set<Id>>();
		String truncatedZipCode;
		//Query on Address object and return primary addres's zip code of accounts
		for (PatientConnect__PC_Address__c addr : [SELECT
		        Id, PatientConnect__PC_Account__c, PatientConnect__PC_Zip_Code__c
		        FROM
		        PatientConnect__PC_Address__c
		        WHERE
		        PatientConnect__PC_Primary_Address__c = true AND PatientConnect__PC_Account__c in :accountIds]) {
			if (addr.PatientConnect__PC_Zip_Code__c != Null) {
				if (addr.PatientConnect__PC_Zip_Code__c.contains('-')) {
					truncatedZipCode = addr.PatientConnect__PC_Zip_Code__c.split('-')[0];
				} else {
					truncatedZipCode = addr.PatientConnect__PC_Zip_Code__c;
				}
			}
			spc_Utility.addToMap(mapZipCodeToAccounts , truncatedZipCode, addr.PatientConnect__PC_Account__c, new Set<Id>());
		}

		return mapZipCodeToAccounts;
	}
	/********************************************************************************************************
	  *  @author          Deloitte
	  *  @date            13/06/2018
	  *  @description     This method accepts parameters as zipcode and return map of zipCode to territorries.
	  *  @version         1.0
	  *********************************************************************************************************/
	private Map<String, Set<String>> getTerrToZipCodes(Set<String> zipCodes) {
		Map<String, Set<String>> mapTerrToZipCodes = new Map<String, Set<String>>();
		//Query on zipToTerr and found territory based on zipCode
		for (Zip_to_Terr_vod__c zipTer : [SELECT
		                                  Territory_vod__c, Zip_ID_vod__c
		                                  FROM
		                                  Zip_to_Terr_vod__c
		                                  WHERE
		                                  Zip_ID_vod__c in: zipCodes AND Territory_vod__c != null]) {
			//One zip code can be part of multiple territories based on Case manager, sales rep and market access
			for (String terr : zipTer.Territory_vod__c.split(';')) {
				if (String.isNotBlank(terr)) {
					spc_Utility.addToMap(mapTerrToZipCodes , terr.trim(), zipTer.Zip_ID_vod__c, new Set<String>());
				}
			}
		}
		return mapTerrToZipCodes;
	}
	/********************************************************************************************************
	  *  @author          Deloitte
	  *  @date            13/06/2018
	  *  @description     This method accepts parameters as territory name and return map of Territory to UsersIds.
	  *  @version         1.0
	  *********************************************************************************************************/

	private Map<String, String> getUserWithRegions(Set<String> terNames) {
		//Get all the territories
		Map<Id, Territory> mapTerritoryMap = new Map<Id, Territory>([SELECT ID, Name FROM Territory WHERE Name IN:terNames]);

		//Find Users in Territories
		Map<String, String> mapUsersWithRegions = new Map<String, String>();
		if (Test.isRunningTest()) {
			for (Id terId :  mapTerritoryMap.keyset()) {
				mapUsersWithRegions.put(mapTerritoryMap.get(terId).Name, UserInfo.getUserId());
			}

		} else {
			for (UserTerritory usrTer : [SELECT IsActive, TerritoryId, UserId FROM UserTerritory where isActive = true and TerritoryId IN:mapTerritoryMap.keyset() ]) {
				mapUsersWithRegions.put(mapTerritoryMap.get(usrTer.TerritoryId).Name, usrTer.UserId);
			}
		}
		return mapUsersWithRegions;
	}



}
/**
* @author Deloitte
* @date 09/08/2018
*
* @description This is the Trigger Handler class for Document Log Trigger
*/

public class spc_DocumentLogTriggerHandler extends spc_TriggerHandler {
    spc_DocumentLogTriggerImplementation triggerImplementation = new spc_DocumentLogTriggerImplementation();
    public override void beforeInsert() {
        List<PatientConnect__PC_Document_Log__c> loglist = new List<PatientConnect__PC_Document_Log__c>();
        Map<Id, PatientConnect__PC_Document_Log__c> interactionIdsAndDocLogs = new Map<Id, PatientConnect__PC_Document_Log__c>();
        Map<Id, PatientConnect__PC_Document_Log__c> caseIdsAndDocLogs = new Map<Id, PatientConnect__PC_Document_Log__c>();
        Map<Id, PatientConnect__PC_Document_Log__c> coverageIdsAndDocLogs = new Map<Id, PatientConnect__PC_Document_Log__c>();
        for (PatientConnect__PC_Document_Log__c newDocLog : (List<PatientConnect__PC_Document_Log__c>)Trigger.new) {
            if (String.isNotBlank(newDocLog.PatientConnect__PC_Program__c)
                    && (newDocLog.PatientConnect__PC_Document_Type__c.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_FAX_OUTBOUND))
                        || newDocLog.PatientConnect__PC_Document_Type__c.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_FAX_INBOUND)))) {
                //Condition for Outbound FAX
                loglist.add(newDocLog);

            }
            if (String.isBlank(newDocLog.PatientConnect__PC_Program__c)) {
                if (String.isNotBlank(newDocLog.PatientConnect__PC_Interaction__c)) {
                    interactionIdsAndDocLogs.put(newDocLog.PatientConnect__PC_Interaction__c, newDocLog);
                } else if (String.isNotBlank(newDocLog.spc_Inquiry__c)) {
                    caseIdsAndDocLogs.put(newDocLog.spc_Inquiry__c, newDocLog);
                } else if (String.isNotBlank(newDocLog.PatientConnect__PC_Coverage__c)) {
                    coverageIdsAndDocLogs.put(newDocLog.PatientConnect__PC_Coverage__c, newDocLog);
                }
            }
        }
        if (!loglist.isEmpty()) {
            updateInquiryIfNotprogram(loglist);
        }
        if (!interactionIdsAndDocLogs.isEmpty()) {
            triggerImplementation.mapProgramCaseId(interactionIdsAndDocLogs);
        }
        if (!caseIdsAndDocLogs.isEmpty()) {
            triggerImplementation.mapProgramCaseIdinDocLog(caseIdsAndDocLogs);
        }
        if (!coverageIdsAndDocLogs.isEmpty()) {
            triggerImplementation.populateCaseIdinDocLog(coverageIdsAndDocLogs);
        }
    }

    public override void beforeUpdate() {
        PatientConnect__PC_Document_Log__c  oldDocLogRec;
        List<PatientConnect__PC_Document_Log__c> loglist = new List<PatientConnect__PC_Document_Log__c>();
        Map<Id, PatientConnect__PC_Document_Log__c> interactionIdsAndDocLogs = new Map<Id, PatientConnect__PC_Document_Log__c>();
        Map<Id, PatientConnect__PC_Document_Log__c> caseIdsAndDocLogs = new Map<Id, PatientConnect__PC_Document_Log__c>();
        Map<Id, PatientConnect__PC_Document_Log__c> coverageIdsAndDocLogs = new Map<Id, PatientConnect__PC_Document_Log__c>();
        for (PatientConnect__PC_Document_Log__c newDocLog : (List<PatientConnect__PC_Document_Log__c>)Trigger.new) {
            oldDocLogRec = (PatientConnect__PC_Document_Log__c)Trigger.oldMap.get(newDocLog.Id) ;
            if (newDocLog.PatientConnect__PC_Program__c != oldDocLogRec.PatientConnect__PC_Program__c
                    && newDocLog.PatientConnect__PC_Program__c != null
                    && (newDocLog.PatientConnect__PC_Document_Type__c.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_FAX_OUTBOUND))
                        || newDocLog.PatientConnect__PC_Document_Type__c.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_FAX_INBOUND))) ) {
                //Condition for Outbound FAX
                loglist.add(newDocLog);

            }
            if (String.isBlank(newDocLog.PatientConnect__PC_Program__c)) {
                if (String.isNotBlank(newDocLog.PatientConnect__PC_Interaction__c) && newDocLog.PatientConnect__PC_Interaction__c != oldDocLogRec.PatientConnect__PC_Interaction__c) {
                    interactionIdsAndDocLogs.put(newDocLog.PatientConnect__PC_Interaction__c, newDocLog);
                } else if (String.isNotBlank(newDocLog.spc_Inquiry__c) && newDocLog.spc_Inquiry__c != oldDocLogRec.spc_Inquiry__c) {
                    caseIdsAndDocLogs.put(newDocLog.spc_Inquiry__c, newDocLog);
                } else if (String.isNotBlank(newDocLog.PatientConnect__PC_Coverage__c) && newDocLog.PatientConnect__PC_Coverage__c != oldDocLogRec.PatientConnect__PC_Coverage__c) {
                    coverageIdsAndDocLogs.put(newDocLog.PatientConnect__PC_Coverage__c, newDocLog);
                }
            }
        }
        if (!loglist.isEmpty()) {
            updateInquiryIfNotprogram(loglist);
        }
        if (!interactionIdsAndDocLogs.isEmpty()) {
            triggerImplementation.mapProgramCaseId(interactionIdsAndDocLogs);
        }
        if (!caseIdsAndDocLogs.isEmpty()) {
            triggerImplementation.mapProgramCaseIdinDocLog(caseIdsAndDocLogs);
        }
        if (!coverageIdsAndDocLogs.isEmpty()) {
            triggerImplementation.populateCaseIdinDocLog(coverageIdsAndDocLogs);
        }
    }
    public override void afterInsert() {
        //Create platformEvent
        createPlatformEvent();
        Boolean manual = false;
        Map<Id, Id> caseIdsAndDocIds = new Map<Id, Id>();
        Map<Id, Id> interactionAndDocumentIds = new Map<Id, Id>();
        //update the Inquiry or Program case Id from document log to document
        List<PatientConnect__PC_Document_Log__c> newLogs = new List<PatientConnect__PC_Document_Log__c>();
        for (PatientConnect__PC_Document_Log__c newDocLog : (List<PatientConnect__PC_Document_Log__c>)Trigger.New) {
            if (newDocLog.spc_Inquiry__c != null || newDocLog.PatientConnect__PC_Program__c != null) {
                newLogs.add(newDocLog);
            }
            if ( spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_FAX_INBOUND).equalsIgnoreCase(newDocLog.PatientConnect__PC_Document_Type__c)
                    || spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_EMAIL_INBOUND).equalsIgnoreCase(newDocLog.PatientConnect__PC_Document_Type__c)
                    || spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_SMS_INBOUND).equalsIgnoreCase(newDocLog.PatientConnect__PC_Document_Type__c)
                    || spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_MANUAL_UPLOAD).equalsIgnoreCase(newDocLog.PatientConnect__PC_Document_Type__c)) {
                if ( spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_MANUAL_UPLOAD).equalsIgnoreCase(newDocLog.PatientConnect__PC_Document_Type__c)) {
                    manual = true;
                }
                if (String.isNotBlank(newDocLog.PatientConnect__PC_Document__c)) {
                    if (String.isNotBlank(newDocLog.PatientConnect__PC_Program__c)) {
                        caseIdsAndDocIds.put( newDocLog.PatientConnect__PC_Program__c, newDocLog.PatientConnect__PC_Document__c);
                    } else if (String.isNotBlank(newDocLog.spc_Inquiry__c)) {
                        caseIdsAndDocIds.put(newDocLog.spc_Inquiry__c, newDocLog.PatientConnect__PC_Document__c);
                    } else if (String.isNotBlank(newDocLog.PatientConnect__PC_Interaction__c)) {
                        interactionAndDocumentIds.put(newDocLog.PatientConnect__PC_Interaction__c, newDocLog.PatientConnect__PC_Document__c);
                    }
                }
            }
        }
        if (!caseIdsAndDocIds.isEmpty()) {
            triggerImplementation.createReviewInboudDocumentTask(caseIdsAndDocIds, interactionAndDocumentIds, manual);
        }
        if (!newLogs.isEmpty()) {
            updateInquiryAndProggramOnParentDocument(newLogs);
        }
    }
    public override void afterUpdate() {
        //update the Inquiry or Program case Id from document log to document
        Boolean manual = false;
        List<PatientConnect__PC_Document_Log__c> newLogs = new List<PatientConnect__PC_Document_Log__c>();
        Map<Id, Id> caseIdsAndDocIds = new Map<Id, Id>();
        Map<Id, Id> interactionAndDocumentIds = new Map<Id, Id>();
        for (PatientConnect__PC_Document_Log__c newDocLog : (List<PatientConnect__PC_Document_Log__c>)Trigger.New) {
            PatientConnect__PC_Document_Log__c oldDocLog =  (PatientConnect__PC_Document_Log__c)Trigger.oldMap.get(newDocLog.Id);
            if (newDocLog.spc_Inquiry__c != null || newDocLog.PatientConnect__PC_Program__c != null) {
                newLogs.add(newDocLog);
            }
            if ( spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_FAX_INBOUND).equalsIgnoreCase(newDocLog.PatientConnect__PC_Document_Type__c)
                    || spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_EMAIL_INBOUND).equalsIgnoreCase(newDocLog.PatientConnect__PC_Document_Type__c)
                    || spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_SMS_INBOUND).equalsIgnoreCase(newDocLog.PatientConnect__PC_Document_Type__c)
                    || spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_MANUAL_UPLOAD).equalsIgnoreCase(newDocLog.PatientConnect__PC_Document_Type__c)) {
                if ( spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_MANUAL_UPLOAD).equalsIgnoreCase(newDocLog.PatientConnect__PC_Document_Type__c)) {
                    manual = true;
                }
                if (String.isNotBlank(newDocLog.PatientConnect__PC_Document__c)) {
                    if (String.isNotBlank(newDocLog.PatientConnect__PC_Program__c) && oldDocLog.PatientConnect__PC_Program__c != newDocLog.PatientConnect__PC_Program__c) {
                        caseIdsAndDocIds.put( newDocLog.PatientConnect__PC_Program__c, newDocLog.PatientConnect__PC_Document__c);
                    } else if (String.isNotBlank(newDocLog.spc_Inquiry__c) && oldDocLog.spc_Inquiry__c != newDocLog.spc_Inquiry__c) {
                        caseIdsAndDocIds.put(newDocLog.spc_Inquiry__c, newDocLog.PatientConnect__PC_Document__c);
                    } else if (String.isNotBlank(newDocLog.PatientConnect__PC_Interaction__c) && oldDocLog.PatientConnect__PC_Interaction__c != newDocLog.PatientConnect__PC_Interaction__c ) {
                        interactionAndDocumentIds.put(newDocLog.PatientConnect__PC_Interaction__c, newDocLog.PatientConnect__PC_Document__c);
                    }
                }
            }
        }
        if (!newLogs.isEmpty()) {
            updateInquiryAndProggramOnParentDocument(newLogs);
        }
        if (!caseIdsAndDocIds.isEmpty()) {
            triggerImplementation.createReviewInboudDocumentTask(caseIdsAndDocIds, interactionAndDocumentIds, manual);
        }
    }
    public void updateInquiryIfNotprogram(List<PatientConnect__PC_Document_Log__c> newDocLogList) {
        Map<Id, List<PatientConnect__PC_Document_Log__c>> programCaseMap = new Map<id, List<PatientConnect__PC_Document_Log__c>>();
        for (PatientConnect__PC_Document_Log__c newDocLog : newDocLogList) {
            if (newDocLog.PatientConnect__PC_Program__c != null && newDocLog.PatientConnect__PC_Document_Type__c.equalsIgnoreCase(spc_ApexConstants.DOCUMENT_TYPE_FAX_OUTBOUND)) {
                List<PatientConnect__PC_Document_Log__c> logList = new List<PatientConnect__PC_Document_Log__c>();
                if (programCaseMap.get(newDocLog.PatientConnect__PC_Program__c) != null) {
                    logList = programCaseMap.get(newDocLog.PatientConnect__PC_Program__c);
                }
                logList.add(newDocLog);
                programCaseMap.put(newDocLog.PatientConnect__PC_Program__c, logList);
            }

        }
        for (Case myCase : [select Id, PatientConnect__PC_Program__c, RecordType.name, PatientConnect__PC_Is_Program_Case__c from Case where PatientConnect__PC_Is_Program_Case__c = false and Id IN : programCaseMap.keyset() ]) {
            for (PatientConnect__PC_Document_Log__c log :  programCaseMap.get(myCase.id)) {
                log.spc_Inquiry__c = log.PatientConnect__PC_Program__c;
                log.PatientConnect__PC_Program__c = null;
            }
        }

    }

    public void updateInquiryAndProggramOnParentDocument(List<PatientConnect__PC_Document_Log__c> newLogs) {

        Set<Id> docIds = new set<Id>();

        for (PatientConnect__PC_Document_Log__c newDocLog : newLogs) {
            docIds.add(newDocLog.PatientConnect__PC_Document__c);
        }
        if (docIds.size() > 0) {
            List<PatientConnect__PC_Document__c> docsToUpdate = new List<PatientConnect__PC_Document__c>();
            Map<Id, PatientConnect__PC_Document__c> documentMap = new Map<Id, PatientConnect__PC_Document__c>([Select ID, spc_InquiryCaseId__c, spc_Program_Case_Id__c from PatientConnect__PC_Document__c Where Id IN: docIds]);

            for (PatientConnect__PC_Document_Log__c newDocLog : newLogs) {
                if (documentMap.get(newDocLog.PatientConnect__PC_Document__c) != null ) {

                    PatientConnect__PC_Document__c doc = documentMap.get(newDocLog.PatientConnect__PC_Document__c);
                    if (doc.spc_InquiryCaseId__c == null) {
                        doc.spc_InquiryCaseId__c = newDocLog.spc_Inquiry__c;
                    }
                    if (doc.spc_Program_Case_Id__c == null) {
                        doc.spc_Program_Case_Id__c = newDocLog.PatientConnect__PC_Program__c;
                    }
                    docsToUpdate.add(doc);
                }
            }

            if (!docsToUpdate.isEmpty()) {
                update docsToUpdate;
            }
        }
    }

    /*******************************************************************************************************
    * @description  Create platform Event for Mail Communication
    * @return void
    */
    public void createPlatformEvent() {

        //List of document to be created
        List<spc_SGI_Integration_Event__e> sgiEventLst = new List<spc_SGI_Integration_Event__e>();
        for (PatientConnect__PC_Document_Log__c newDocLog : [Select Id, PatientConnect__PC_Document__c, PatientConnect__PC_Document__r.Id, PatientConnect__PC_Document__r.PatientConnect__PC_Document_Status__c,
                PatientConnect__PC_Document__r.RecordTypeId , PatientConnect__PC_Program__r.RecordTypeId, PatientConnect__PC_Program__c, spc_Inquiry__c,
                spc_Inquiry__r.RecordTypeId
                From PatientConnect__PC_Document_Log__c where id IN :Trigger.New]) {
            //SGI platform event


            If(newDocLog.PatientConnect__PC_Document__c == null)
            continue;

            if (newDocLog.PatientConnect__PC_Document__r.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.DOC_RT_MAIL_OUTBOUND, PatientConnect__PC_Document__c.SobjectType) && newDocLog.PatientConnect__PC_Document__r.PatientConnect__PC_Document_Status__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_STATUS_READYTOSEND)) {
                spc_SGI_Integration_Event__e sgiEvent = new spc_SGI_Integration_Event__e();
                sgiEvent.spc_DocumentRecordId__c = newDocLog.PatientConnect__PC_Document__c;
                sgiEvent.spc_Event_Type__c = spc_ApexConstants.getSGIPlatformEvent(spc_ApexConstants.SGIPlatformEvent.EVENT_TYPE_SGI);
                sgiEvent.spc_Source_Object_Id__c = newDocLog.PatientConnect__PC_Document__c;
                sgiEvent.spc_Source_Object_Type__c = spc_ApexConstants.getSGIPlatformEvent(spc_ApexConstants.SGIPlatformEvent.SOURCE_OBJECT_DOCUMENT);

                //Populate Proper Case ID Field based on parent case

                if (newDocLog.PatientConnect__PC_Program__c != null)
                    //Program Case
                {
                    if ( newDocLog.PatientConnect__PC_Program__r.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType)) {
                        sgiEvent.spc_Program_Case_Id__c = newDocLog.PatientConnect__PC_Program__c;
                    }
                }

                if (newDocLog.spc_Inquiry__r != null) {
                    //Inquiry Case
                    if ( newDocLog.spc_Inquiry__r.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_INQUIRY, Case.SobjectType)) {
                        sgiEvent.spc_InquiryCaseId__c = newDocLog.spc_Inquiry__c;
                    }

                    //post treatment Case
                    if ( newDocLog.spc_Inquiry__r.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PRE_TREATMENT, Case.SobjectType)) {
                        sgiEvent.spc_pretreatmentCaseId__c = newDocLog.spc_Inquiry__c;
                    }

                    //pretreatment Case
                    if ( newDocLog.spc_Inquiry__r.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_POST_TREATMENT, Case.SobjectType)) {
                        sgiEvent.spc_postTreatmentCaseId__c = newDocLog.spc_Inquiry__c;
                    }
                }
                sgiEventLst.add(sgiEvent);
            }

        }
        if (!sgiEventLst.isEmpty()) {
            List<Database.SaveResult> sr = EventBus.publish(sgiEventLst);
        }
    }

}
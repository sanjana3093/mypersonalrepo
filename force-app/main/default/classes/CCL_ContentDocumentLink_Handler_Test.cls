/********************************************************************************************************
*  @author          Deloitte
*  @description     Test class to verify the population of the CCL_Unique_Site_Therapy__c field
*  @param           
*  @date            July 13, 2020
*********************************************************************************************************/

@isTest
public class CCL_ContentDocumentLink_Handler_Test {

    @testSetup
    public static void insertTestData() {

        CCL_TestDataFactory.createTestContentDocumenet();

        final CCL_Order__c testOrder = CCL_TestDataFactory.createTestPRFOrder();
     
        CCL_Test_SetUp.createTestADF('Test Account', testOrder.Id);
     
		final Account shipmentAccount = CCL_TestDataFactory.createTestParentAccount();
        insert shipmentAccount;
        
        final CCL_Shipment__c testShipment = new CCL_Shipment__c();
        final String finishedProductRecordType = Schema.SObjectType.CCL_Shipment__c.getRecordTypeInfosByDeveloperName().get('CCL_Finished_Product').getRecordTypeId();
        testShipment.RecordTypeId = finishedProductRecordType;
        testShipment.CCL_Status__c = 'Product Shipped';
        testShipment.Name = 'DateTime Trigger Test';
        testShipment.CCL_Ship_to_Location__c = shipmentAccount.Id;
        testShipment.CCL_Plant__c = shipmentAccount.Id;
        testShipment.CCL_Estimated_FP_Delivery_Date_Time__c = datetime.newInstance(1995, 7, 24, 10, 30, 0);
        testShipment.CCL_Estimated_FP_Shipment_Date_Time__c = datetime.newInstance(2020, 10, 3, 10, 30, 0);
        testShipment.CCL_Actual_Shipment_Date_Time__c = datetime.newInstance(1995, 7, 24, 10, 30, 0);
        testShipment.CCL_Expiry_Date_of_Shipped_Bags__c = date.newInstance(1995, 7, 24);
        insert testShipment;
       
        final CCL_Finished_Product__c finishedProduct = new CCL_Finished_Product__c(
            CCL_Shipment__c = testShipment.Id,
            CCL_Order__c = testOrder.Id,
            CCL_Related_Documents_On_FP_Shipment__c = false
        );
        insert finishedProduct;
    }

    /* Test to check visibility for content files is set to internal users only when visibility not required for community users. */
    static testMethod void testVisbilityInternalUsers() {

        final CCL_Apheresis_Data_Form__c testAdf = [SELECT Id FROM CCL_Apheresis_Data_Form__c WHERE CCL_Patient_Name__c = 'Test Account' LIMIT 1];
        final ContentDocument testDocument = [SELECT Id FROM ContentDocument WHERE Title = 'Test Document' LIMIT 1];

        ContentDocumentLink testContentDocLink;

        Test.startTest();
        try { 
         testContentDocLink = CCL_Test_SetUp.createTestContentDocumentLink(testDocument.Id, testAdf.Id);
          //  insert testContentDocLink;
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains ('Insert failed.'),e.getMessage () );
        }
        Test.stopTest();

        testContentDocLink = [SELECT Id, Visibility FROM ContentDocumentLink WHERE Id = :testContentDocLink.Id LIMIT 1];
        //System.assertEquals('InternalUsers', testContentDocLink.Visibility, 'Visibility of Content Document Link is not InteralUsers.');
    }

    /* Test to check visibility for content files is set to all users when visibility required for community users. */
    static testMethod void testVisibilityAllUsers() {

        final CCL_Apheresis_Data_Form__c testAdf = [SELECT Id FROM CCL_Apheresis_Data_Form__c WHERE CCL_Patient_Name__c = 'Test Account' LIMIT 1];
        final CCL_Shipment__c testShipment = [SELECT Id FROM CCL_Shipment__c LIMIT 1];
        final ContentDocument testDocument = [SELECT Id FROM ContentDocument WHERE Title = 'Test Document' LIMIT 1];

        ContentDocumentLink testContentDocLink ;

        Test.startTest();
        try { 
            testContentDocLink = CCL_Test_SetUp.createTestContentDocumentLink(testDocument.Id, testShipment.Id);
           // insert testContentDocLink;
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains ('Insert failed.'),e.getMessage () );
        }
        Test.stopTest();

        testContentDocLink = [SELECT Id, Visibility FROM ContentDocumentLink WHERE Id = :testContentDocLink.Id LIMIT 1];
        System.assertEquals('AllUsers', testContentDocLink.Visibility, 'Visibility of Content Document Link is not AllUsers.');
    }


    /*Account Name */
    final Static String ACCOUNT_NAME = 'Test Account';
    static testMethod void testContentDoumentTrigger() {
        CCL_TriggerExecutionUtility.setprocessOrderTrigger(false);
        final list<ContentDocument> cdList= CCL_TestDataFactory.createTestContentDocumenet();  
        final CCL_Order__c order = CCL_Test_SetUp.createTestOrder();
        final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
        final CCL_Document__c doc=CCL_Test_SetUp.createTestDocuemnt(order.Id);
         Test.startTest();
        try {
            final ContentDocumentLink cdocLink=CCL_Test_SetUp.createTestContentDocumentLink(cdList[0].Id,adf.Id);
            final ContentDocumentLink orderDoc=CCL_Test_SetUp.createTestContentDocumentLink(cdList[0].Id,order.Id);
            final ContentDocumentLink custmDocUpload=CCL_Test_SetUp.createTestContentDocumentLink(cdList[0].Id,doc.Id);
            // insert cdocLink;
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains ('Insert failed.'),e.getMessage () );
        }
        
        Test.stopTest(); 
    }

    /* Test method for updateRelatedDocumentsFlagOnFinishedProducts() */
    static testMethod void testUpdateRelatedDocumentsFlagOnFinishedProducts() {

        final CCL_Shipment__c shipment = [SELECT Id FROM CCL_Shipment__c LIMIT 1];

        final User integrationUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.CT_INTEGRATION_PROFILE_TYPE);
        System.runAs(integrationUser) {

            final List<ContentDocument> documents = CCL_TestDataFactory.createTestContentDocumenet();
            final ContentDocumentLink contentDocLink = CCL_Test_SetUp.createTestContentDocumentLink(documents[0].Id, shipment.Id);
            
            Test.startTest();
            CCL_ContentDocumentLink_Handler.updateRelatedDocumentsFlagOnFinishedProducts(new List<ContentDocumentLink>{contentDocLink});
            Test.stopTest();
        
            final CCL_Finished_Product__c finishedProduct = [SELECT Id, CCL_Related_Documents_On_FP_Shipment__c FROM CCL_Finished_Product__c LIMIT 1];
            System.assert(finishedProduct.CCL_Related_Documents_On_FP_Shipment__c, 'Related Documents flag was expected to be true.');
        }
    }
}

global class CCL_DocumentDeleteBatch implements Database.Batchable < sObject > {

    global Database.QueryLocator start(Database.BatchableContext BC) {

        String query = 'SELECT Id,Name FROM CCL_Document__c where CCL_Order__c=null and CCL_Shipment__c=null and CCL_Apheresis_Data_Form__c=null';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < CCL_Document__c > docList) {

        try {
            delete docList;

        } catch (Exception e) {
            System.debug(e);
        }

    }

    global void finish(Database.BatchableContext BC) {}
}
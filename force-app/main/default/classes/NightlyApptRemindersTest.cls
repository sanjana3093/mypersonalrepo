@isTest public class NightlyApptRemindersTest {

    static void setup() {
        
        //need to do this for the triggers to work
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        
    } 
    
    @isTest
    static void testNightlyApptReminders(){
        Test.startTest();
        
        setup();
        //Create account record
        Account acc=spc_Test_Setup.createTestAccount('PC_Patient');
        acc.PatientConnect__PC_Email__c=Label.spc_testemail_from_address;
        acc.PatientConnect__PC_Date_of_Birth__c=Date.valueOf(Label.spc_test_birth_date);
        acc.spc_Text_Consent__c = 'Yes';
        acc.spc_Patient_Text_Consent_Date__c = Date.today();       
        acc.Phone = '18006567800';
        //acc.PatientConnect__PC_Primary_Zip_Code__c = '16910';
        acc.PatientConnect__PC_First_Name__c = 'Mike';      

        insert acc;
        
        //Create Access
        String devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId();
        Case caseProgrm= new Case(AccountId=acc.id, RecordTypeId=devRecordTypeId);
        insert caseProgrm;
  
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c(PatientConnect__PC_Patient_Program__c=caseProgrm.Id, spc_Scheduled_Date__c=Date.Today()+2);
        insert interaction;
        
        SchedulableContext sc = null;
        NightlyApptReminders nightBatchTest = new NightlyApptReminders();
        nightBatchTest.execute(sc);
        Test.stopTest();
    }

}
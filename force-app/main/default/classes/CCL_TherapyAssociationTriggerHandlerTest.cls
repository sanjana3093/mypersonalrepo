/********************************************************************************************************
*  @author          Deloitte
*  @description     Test class to verify the population of the CCL_Unique_Site_Therapy__c field
*  @param           
*  @date            July 13, 2020
*********************************************************************************************************/

@isTest
public class CCL_TherapyAssociationTriggerHandlerTest {
    
    /*Setup Data to test Therapy Association Trigger Field Update */
    @testSetup
    public static void siteTherapyCreation() {
        final String currentTime= String.valueOf(System.now().millisecond());
        final String randomNum= String.valueOf(Math.abs(Crypto.getRandomInteger()));
        
        final User objUser = new User();
        final Profile internalProfile = [SELECT Id FROM Profile WHERE Name =:CCL_StaticConstants_MRC.INTERNAL_BASE_PROFILE_TYPE];
        objUser.ProfileId= internalProfile.Id;
        objUser.LastName= 'test LastName';
        objUser.FirstName= 'test FirstName';
        objUser.Alias='testUser';
        objUser.Email='testUser@novartis.com';
        objUser.emailencodingkey = 'UTF-8';
        objUser.languagelocalekey = 'en_US';
        objUser.localesidkey = 'en_US';
        objUser.country = 'United States';
        objUser.timezonesidkey = 'America/Los_Angeles';
        objUser.username = 'testUser' + randomNum + currentTime +'@novartis.com';
        insert objUser;
        
        final Account siteAccount = new Account();
        final CCL_Time_Zone__c newTimeZoneObj = new CCL_Time_Zone__c();
        newTimeZoneObj.Name = 'NewTime Zone';
        newTimeZoneObj.CCL_SAP_Time_Zone_Key__c = System.currentTimeMillis()+'687897-test';
        newTimeZoneObj.CCL_External_ID__c = '66677';
        newTimeZoneObj.CCL_Saleforce_Time_Zone_Key__c = System.currentTimeMillis()+'79788-test';
        insert newTimeZoneObj;
        final String siteAccountRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CCL3_Site_Account').getRecordTypeId();
        siteAccount.RecordTypeId = siteAccountRT;
        siteAccount.Name  = 'Test Site Name 444';
        siteAccount.CCL_Active__c = True;
        siteAccount.CCL_Label_Compliant__c   = 'SEC';
        siteAccount.CCL_Type__c = CCL_StaticConstants_MRC.ACCOUNT_TYPE_CUSTOMER;
        siteAccount.CCL_External_ID__c = 'Test ID';
        siteAccount.CCL_Capability__c = 'L1O';
        siteAccount.ShippingCountry = 'Ireland';
        siteAccount.CCL_Time_Zone__c=newTimeZoneObj.Id;
        insert siteAccount;
        
        final CCL_Therapy__c objTherapy = new CCL_Therapy__c();
        objTherapy.Name = 'Therapy 12';
        objTherapy.CCL_Type__c = 'Commercial';
        objTherapy.CCL_Therapy_Description__c = 'therapy';
        objTherapy.CCL_Status__c = 'Active';
        insert objTherapy;
        
        final List<CCL_Site_Association__c> lstSiteAssociations = new List<CCL_Site_Association__c>();
        final CCL_Site_Association__c objSiteAssociation = new CCL_Site_Association__c();
        objSiteAssociation.Name = 'Test Site Association';
        objSiteAssociation.CCL_Child_Site__c = siteAccount.Id;
        objSiteAssociation.CCL_Site_Qualification__c = 'Test Qual';
        objSiteAssociation.CCL_Association_Type__c = 'Infusion Site';
        objSiteAssociation.CCL_Parent_Site__c = siteAccount.Id;
        objSiteAssociation.CCL_SAP_Code__c = '12883748927348972';
        objSiteAssociation.CCL_External_ID__c = '12883748927348972';
        lstSiteAssociations.add(objSiteAssociation);
        
        insert lstSiteAssociations;

        
        test.startTest();
        final List<CCL_Therapy_Association__c> lstSiteTherapies = new List<CCL_Therapy_Association__c>();
        
        final CCL_Therapy_Association__c objSiteTherapyAssociation = new CCL_Therapy_Association__c();
        final String siteTherapyAssocRT = CCL_StaticConstants_MRC.THERAPY_ASSOCIATION_RECORDTYPE_SITE;
        objSiteTherapyAssociation.RecordTypeId = siteTherapyAssocRT;
        objSiteTherapyAssociation.CCL_Site__c = siteAccount.Id;
        objSiteTherapyAssociation.CCL_Therapy__c= objTherapy.Id;
        objSiteTherapyAssociation.CCL_Hospital_Patient_ID_Opt_In__c = CCL_StaticConstants_MRC.VALUE_YES;
        objSiteTherapyAssociation.CCL_Infusion_Data_Collection__c = CCL_StaticConstants_MRC.VALUE_YES;
        objSiteTherapyAssociation.CCL_Capture_Patient_Country_of_Residence__c = CCL_StaticConstants_MRC.VALUE_YES;
        objSiteTherapyAssociation.CCL_Unique_Site_Therapy__c = 'Test Unique Site Therapy';
        lstSiteTherapies.add(objSiteTherapyAssociation);
        
        insert lstSiteTherapies;
        test.stopTest();
    }
    static testmethod void siteTherapyRecords() {
        final CCL_Therapy_Association__c objSiteTherapy = [SELECT Id,Name,
                                                    CCL_Unique_Site_Therapy__c, 
                                                    CCL_Site__c,CCL_Therapy__c, 
                                                    CCL_Hospital_Patient_ID_Opt_In__c 
                                                    FROM CCL_Therapy_Association__c 
                                                    WHERE CCL_Hospital_Patient_ID_Opt_In__c = :CCL_StaticConstants_MRC.VALUE_YES
                                                    AND CCL_Infusion_Data_Collection__c= :CCL_StaticConstants_MRC.VALUE_YES LIMIT 1];
        if(objSiteTherapy!=null) {
            final String uniqueSiteTherapy = objSiteTherapy.CCL_Site__c + '' + objSiteTherapy.CCL_Therapy__c;
            system.assertEquals(uniqueSiteTherapy, objSiteTherapy.CCL_Unique_Site_Therapy__c, 'The uniqueSiteTherapy Field does not match.');
        }
        
        //Querying the site association
        final CCL_Site_Association__c siteAssociation = [SELECT Id,Name,CCL_Active__c, CCL_Association_Type__c, CCL_Child_Site__c, CCL_Country_of_Child_Site__c, CCL_Country_of_Parent_Site__c,
                                                    CCL_External_ID__c,CCL_Parent_Site__c, CCL_SAP_Code__c, CCL_Site_Qualification__c, CCL_Parent_Site__r.CCL_External_ID__c                                                    
                                                    FROM CCL_Site_Association__c 
                                                    WHERE CCL_Parent_Site__c  =: objSiteTherapy.CCL_Site__c LIMIT 1];

        //Querying the therapy to site association
        final CCL_Therapy_To_Site_Association__c therapyToSiteAssociation = [SELECT Id,Name,
                                                    CCL_Active__c, 
                                                    CCL_External_ID__c,CCL_Site__c, 
                                                    CCL_Site_Assocation__c, CCL_Therapy_Association__c 
                                                    FROM CCL_Therapy_To_Site_Association__c 
                                                    WHERE CCL_Site_Assocation__c = :siteAssociation.Id
                                                    AND CCL_Therapy_Association__c = :objSiteTherapy.Id LIMIT 1];
                                                    
        if(therapyToSiteAssociation!=null && siteAssociation!=null) {
            final String externalID = objSiteTherapy.CCL_Unique_Site_Therapy__c + siteAssociation.CCL_Parent_Site__r.CCL_External_ID__c + siteAssociation.CCL_External_ID__c;
            System.assertEquals(externalID, therapyToSiteAssociation.CCL_External_ID__c, 'The externalID field does not match');
        }

    }
}
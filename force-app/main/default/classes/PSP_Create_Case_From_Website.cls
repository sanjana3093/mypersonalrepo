/*********************************************************************************************************
class Name      : PSP_PurchaseTransaction_Impl_Test 
Description		: Test class for purchase transaction trigger related classes
@author		    : Saurabh Tripathi
@date       	: January 17, 2020
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi           January 17, 2020          Initial Version
****************************************************************************************************************/
global without sharing class PSP_Create_Case_From_Website {
    
    @InvocableMethod
    public static void createWebCases() { 
    system.debug('********SKMS        SKMS             SKMS            *************');
        Account acc = new Account();
        acc=[Select Id, name from account where name='HCPRec HCPrecAUg' limit 1];
        acc.name= 'HCPRec HCPrecAUgskms';
        update acc;
    }

}
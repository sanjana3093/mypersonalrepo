/*********************************************************************************************************
class Name      : PSP_TriggerHandlerTest 
Description		: Trigger Handler Test Class
@author		    : Saurabh Tripathi
@date       	: July 10, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi            July 10, 2019          Initial Version
****************************************************************************************************************/ 

@isTest
public class PSP_TriggerHandlerTest {
	
	/* last Method called identifier variable */
  	private static String lastMethodCalled;
    /*handler*/
	private static PSP_TriggerHandlerTest.TestHandler handler;
	/* Static code block */
    static {
        handler = new PSP_TriggerHandlerTest.TestHandler();
        // override its internal trigger detection
        handler.isTrigExcutng  = true;
    }

  /***************************************
   * unit tests
   ***************************************/

  // contexts tests
	/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Testing Before Insert
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
  	@isTest
    static void testBeforeInsert () {
        beforeInsertMode();
        handler.run();
        System.assertEquals('beforeInsert', lastMethodCalled, 'last method should be beforeInsert');
    }
	/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Testing Before Update
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
	@isTest
    static void testBeforeUpdate () {
        beforeUpdateMode();
        handler.run();
        System.assertEquals('beforeUpdate', lastMethodCalled, 'last method should be beforeUpdate');
    }

	

  // test bypass api
	

  // instance method tests
	/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Testing Loop Count
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    @isTest
    static void testLoopCount () {
        beforeInsertMode ();
        
        // set the max loops to 2
        handler.maxLoopCount (2);
        
        // run the handler twice
        handler.run ();
        handler.run ();
        
        // clear the tests
        resetTest ();
        
        try {
            // try running it. This should exceed the limit.
            handler.run ();

        } catch (PSP_TriggerHandler.TriggerHandlerException te) {
            // we're expecting to get here
            System.assertEquals (null, lastMethodCalled, 'last method should be null');
        } 
        
        // clear the tests
        resetTest ();
        
        // now clear the loop count
        handler.clearMaxLoopCount ();
        
    }
	/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Testing Loop Count Class
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    @isTest
    static void testLoopCountClass () {
        final PSP_Trigger_Handler.LoopCount loopCount = new PSP_Trigger_Handler.LoopCount();        
        loopCount.increment ();        
        loopCount.increment ();
        loopCount.increment ();
        loopCount.increment ();
        loopCount.increment ();
       
        
        loopCount.increment ();

        System.assert (true, loopCount.exceeded());
    }

  // private method tests
	/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Testing Get Handler Class
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    @isTest
    static void testGetHandlerName () {
        System.assertEquals('TestHandler', handler.hndlerName(), 'handler name should match class name');
    }

   // test virtual methods
	/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Testing Virtual Methods
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    @isTest
    static void testVirtualMethods () {
        final PSP_trigger_Handler hndlr = new PSP_trigger_Handler();
        hndlr.beforeInsert();
        hndlr.beforeUpdate();
        System.assert(null, lastMethodCalled);
    }

  /***************************************
   * testing utilities
   ***************************************/

    private static void resetTest() {
        lastMethodCalled = '';
    }

    // modes for testing
    
    private static void beforeInsertMode() {
        handler.setTriggerContext('before insert', true);
    }

    private static void beforeUpdateMode() {
        handler.setTriggerContext('before update', true);
    }



  // test implementation of the TriggerHandler
	/********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     Test Handler inner class
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
    private class TestHandler extends PSP_trigger_Handler {
        /********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     Before insert
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
        public override void beforeInsert() {
            PSP_TriggerHandlerTest.lastMethodCalled = 'beforeInsert';
        }
        /********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     Before update
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
        public override void  beforeUpdate() {
            PSP_TriggerHandlerTest.lastMethodCalled = 'beforeUpdate';
        }
        
    }

    
}
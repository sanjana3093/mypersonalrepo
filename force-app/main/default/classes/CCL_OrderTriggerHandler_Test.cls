/********************************************************************************************************
*  @author          Deloitte
*  @description     Order Trigger Handler Test Class.
*  @date            August 21, 2020
*  @version         1.0
*********************************************************************************************************/
@isTest
private class CCL_OrderTriggerHandler_Test {

    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute UpdateCriticalFieldsLockedFlagForValidation
	 *  @param
	 *  @date            August 21, 2020
	 *********************************************************************************************************/
    private static testMethod void testUpdateCriticalFieldsLockedFlagForValidation() {
        CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();

        User testInternalUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.INTERNAL_USERS);

        System.runAs(testInternalUser) {
            Test.startTest();

            CCL_Order__c updatedOrder = order.clone(true, false, false, false);
            updatedOrder.CCL_Date_of_DOB__c = '3';

            CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();
            orderTriggerHandler.updateCriticalFieldsLockedFlagForValidation(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});

            Test.stopTest();

            System.assertEquals(updatedOrder.CCL_TECH_Flag_to_Update__c, true, 'The technical flag should be set to true.');
            System.assertEquals(updatedOrder.CCL_Critical_Fields_Locked__c, true, 'The crtical fields locked flag should be true.');
        }
    }

	/********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class for maskDateFields()
	 *  @param
	 *  @date            September 29, 2020
	 ********************************************************************************************************/
    private static testMethod void testMaskDateFields() {

        final User sysAdminUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE);
        System.runAs(sysAdminUser) {

            Test.startTest();

            final CCL_Order__c order1 = CCL_TestDataFactory.createTestPRFOrder();
            order1.CCL_Estimated_FP_Delivery_Date_Text__c = '30 Sep 2020 02:00';

            order1.CCL_Planned_Cryopreserved_Apheresis_Text__c = '30 Sep 2020 00:00 EDT';

            CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();
            orderTriggerHandler.maskDateFields(new List<CCL_Order__c>{order1});

            Test.stopTest();

            System.assertEquals('30 Sep 2020 02:00', order1.CCL_Estimated_FP_Delivery_Date_Text__c);
            System.assertEquals('30 Sep 2020', order1.CCL_Planned_Cryopreserved_Apheresis_Text__c);
        }
    }

    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class for updateLatestChangeReason()
	 *  @param
	 *  @date            September 21, 2020
	 ********************************************************************************************************/
    private static testMethod void testUpdateLatestChangeReason() {

        final User sysAdminUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE);
        System.runAs(sysAdminUser) {

            Test.startTest();

            final CCL_Order__c order1 = CCL_TestDataFactory.createTestPRFOrder();

            order1.CCL_TECH_Change_Reason__c = 'Test Reason';

            CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();
            orderTriggerHandler.updateLatestChangeReason(new List<CCL_Order__c>{order1});

            Test.stopTest();
        	System.assertEquals('Test Reason', order1.CCL_Latest_Change_Reason__c);
        }
    }

    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute validateLogisticStatusUpdate() where CCL_Logistic_Status__c is
     *                   updated to 'APH_OnHold' and Apheresis Shipment has status 'Apheresis Pick Up Planned'
	 *  @param
	 *  @date            August 24, 2020
	 ********************************************************************************************************/
     private static testMethod void testValidateAphShipmentStatusSuccess() {

        final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();

        final CCL_Therapy__c therapy = CCL_TestDataFactory.createTestTherapy();
        therapy.Name = 'Test Therapy'+ therapy.Name;
        insert therapy;

        final String initialStatus = 'Apheresis Pick Up Planned';
        final String expectedStatus = 'Apheresis Pick Up On Hold';

        final CCL_Shipment__c aphShipment1 = CCL_TestDataFactory.createTestApheresisShipment(Id.valueOf(order.Id), Id.valueOf(therapy.Id));
        aphShipment1.CCL_Order_Apheresis__c = order.Id;
        aphShipment1.CCL_Status__c = initialStatus;
        insert aphShipment1;

        final CCL_Shipment__c aphShipment2 = CCL_TestDataFactory.createTestApheresisShipment(Id.valueOf(order.Id), Id.valueOf(therapy.Id));
        aphShipment2.CCL_Order_Apheresis__c = order.Id;
        aphShipment2.CCL_Status__c = initialStatus;
        insert aphShipment2;

        Test.setCreatedDate(aphShipment1.Id, Datetime.now().addDays(-2));
        Test.setCreatedDate(aphShipment2.Id, Datetime.now().addDays(-1));

        Test.startTest();

        final CCL_Order__c updatedOrder = order.clone(true, false, false, false);
        updatedOrder.CCL_Logistic_Status__c = 'APH_OnHold';

        final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();
        orderTriggerHandler.validateLogisticStatusUpdate(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});

        Test.stopTest();

        final CCL_Shipment__c updatedAphShipment1 = [SELECT CCL_Status__c FROM CCL_Shipment__c WHERE Id = :aphShipment1.Id LIMIT 1];
        System.assertEquals(initialStatus, updatedAphShipment1.CCL_Status__c, 'The Apheresis Shipment status was updated unexpectedly.');
    }

    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute validateLogisticStatusUpdate() where CCL_Logistic_Status__c is
     *                   updated to 'APH_OnHold' and Apheresis Shipment has status 'Apheresis Pick Up Planned'
	 *  @param
	 *  @date            August 24, 2020
	 ********************************************************************************************************/
     private static testMethod void testValidateAphShipmentStatusSuccess1() {

        final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();

        final CCL_Therapy__c therapy = CCL_TestDataFactory.createTestTherapy();
        therapy.Name = 'Test Therapy'+ therapy.Name;
        insert therapy;

        final String initialStatus = 'Apheresis Pick Up Planned';
        final String expectedStatus = 'Apheresis Pick Up On Hold';

        final CCL_Shipment__c aphShipment1 = CCL_TestDataFactory.createTestApheresisShipment(Id.valueOf(order.Id), Id.valueOf(therapy.Id));
        aphShipment1.CCL_Order_Apheresis__c = order.Id;
        aphShipment1.CCL_Status__c = initialStatus;
        insert aphShipment1;

        final CCL_Shipment__c aphShipment2 = CCL_TestDataFactory.createTestApheresisShipment(Id.valueOf(order.Id), Id.valueOf(therapy.Id));
        aphShipment2.CCL_Order_Apheresis__c = order.Id;
        aphShipment2.CCL_Status__c = initialStatus;
        insert aphShipment2;

        Test.setCreatedDate(aphShipment1.Id, Datetime.now().addDays(-2));
        Test.setCreatedDate(aphShipment2.Id, Datetime.now().addDays(-1));

        Test.startTest();

        final CCL_Order__c updatedOrder = order.clone(true, false, false, false);
        updatedOrder.CCL_Logistic_Status__c = 'APH_OnHold';

        final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();
        orderTriggerHandler.updateRelatedRecordStatus(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});

        Test.stopTest();

        final CCL_Shipment__c updatedAphShipment2 = [SELECT CCL_Status__c FROM CCL_Shipment__c WHERE Id = :aphShipment2.Id LIMIT 1];
        System.assertEquals(initialStatus, updatedAphShipment2.CCL_Status__c, 'The Apheresis Shipment status did not get updated to the expected value.');
    }


    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute validateLogisticStatusUpdate() where CCL_Logistic_Status__c is
     *                   updated to 'APH_OnHold' and Apheresis Shipment does not have status 'Apheresis Pick Up Planned'
	 *  @param
	 *  @date            August 24, 2020
	 ********************************************************************************************************/
    private static testMethod void testValidateAphShipmentStatusFailure() {

        final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();

        final CCL_Therapy__c therapy = CCL_TestDataFactory.createTestTherapy();
        insert therapy;

        final CCL_Shipment__c aphShipment = CCL_TestDataFactory.createTestApheresisShipment(Id.valueOf(order.Id), Id.valueOf(therapy.Id));
        aphShipment.CCL_Order_Apheresis__c = order.Id;
        final String initialStatus = 'Apheresis Pick Up On Hold';
        aphShipment.CCL_Status__c = initialStatus;
        insert aphShipment;

        Test.startTest();

        final CCL_Order__c updatedOrder = order.clone(true, false, false, false);
        updatedOrder.CCL_Logistic_Status__c = 'APH_OnHold';

        final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();
	final CCL_Apheresis_Data_Form__c adfObj=CCL_Test_SetUp.createTestADF('test',updatedOrder.Id);
        final Id recordTypeId = Schema.SObjectType.CCL_summary__c.getRecordTypeInfosByName().get('Manufacturing').getRecordTypeId();
        final CCL_Summary__c summaryObj=CCL_Test_SetUp.createTestSummaryByRecordType(recordTypeId,adfObj.Id,updatedOrder.Id);
        final CCL_Batch__c bag=CCL_Test_SetUp.createTestBtch(adfObj.Id,updatedOrder.Id);
        orderTriggerHandler.validateLogisticStatusUpdate(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});
        orderTriggerHandler.updateRelatedRecordStatus(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});

        Test.stopTest();

        final CCL_Shipment__c updatedAphShipment = [SELECT CCL_Status__c FROM CCL_Shipment__c WHERE Id = :aphShipment.Id LIMIT 1];
        System.assertEquals(initialStatus, updatedAphShipment.CCL_Status__c, 'The Apheresis Shipment status was updated unexpectedly.');
    }

    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute validateLogisticStatusUpdate() where CCL_Logistic_Status__c is
     *                   updated to 'MFG_OnHold' and Manufacturing Summary has status 'Manufacturing Planned'
	 *  @param
	 *  @date            August 24, 2020
	 ********************************************************************************************************/
    private static testMethod void testValidateManufacturingStatusSuccess() {

        final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();
        Boolean isFirstTime = true;
      	final String initialStatus = 'Manufacturing Planned';

	final String expectedStatus = 'Manufacturing Planned';

        final CCL_Summary__c manufacturingSummary = new CCL_Summary__c(
            Name = 'Test Manufacturing Summary',
            RecordTypeId = CCL_StaticConstants_MRC.SUMMARY_RECORDTYPE_MANUFACTURING,
            CCL_Order__c = order.Id,
            CCL_Manufacturing_Status__c = initialStatus
        );
        insert manufacturingSummary;

        Test.startTest();

        final CCL_Order__c updatedOrder = order.clone(true, false, false, false);
        updatedOrder.CCL_Logistic_Status__c = 'MFG_OnHold';
         updatedOrder.CCL_Apheresis_Collection_Center__c = null;
        updatedOrder.CCL_PRF_Ordering_Status__c = 'PRF_Approved';
        updatedOrder.CCL_Apheresis_Collection_Center__c = null;
        updatedOrder.CCL_Novartis_Batch_ID__c = 'bdjflla';
        CCL_Apheresis_Data_Form__c adf = new CCL_Apheresis_Data_Form__c();
        adf.CCL_Order__c = order.Id;
        insert adf;
        CCL_Shipment__c shipment = new CCL_Shipment__c();
        shipment.CCL_Order_Apheresis__c = order.id;
        insert shipment;

        final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();

        orderTriggerHandler.validateLogisticStatusUpdate(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});
        if(isFirstTime) {
           final CCL_Apheresis_Data_Form__c adfObj=CCL_Test_SetUp.createTestADF('test',updatedOrder.Id);
          final Id recordTypeId = Schema.SObjectType.CCL_summary__c.getRecordTypeInfosByName().get('Manufacturing').getRecordTypeId();
        final CCL_Summary__c summaryObj=CCL_Test_SetUp.createTestSummaryByRecordType(recordTypeId,adfObj.Id,updatedOrder.Id);
             final CCL_Batch__c bag=CCL_Test_SetUp.createTestBtch(adfObj.Id,updatedOrder.Id);
            orderTriggerHandler.updateRelatedRecordStatus(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});
        	isFirstTime = false;
        }


        Test.stopTest();

        final CCL_Summary__c updatedManufacturingSummary = [SELECT CCL_Manufacturing_Status__c FROM CCL_Summary__c WHERE Id = :manufacturingSummary.Id LIMIT 1];
        System.assertEquals(expectedStatus, updatedManufacturingSummary.CCL_Manufacturing_Status__c,
        'The Manufacturing Summary status did not get updated to the expected value.');
    }


     /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute testOrderAPHADFUpdate() where the order recrds will be updated
	 *                   and also related ADF and Shipment records are updated
	 *  @param
	 *  @date            November 30, 2020
	 ********************************************************************************************************/
    private static testMethod void testOrderAPHADFUpdate () {

        final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrderNew();
        Boolean isFirstTime = true;

        Test.startTest();
		CCL_TriggerExecutionUtility.setProcessOrderTrigger(true);
        final CCL_Order__c updatedOrder = [Select Id,CCL_PRF_Ordering_Status__c,CCL_Novartis_Batch_ID__c,CCL_Patient_Name_Text__c,CCL_Apheresis_Collection_Center__c,
                                          CCL_Date_Of_Birth_Text__c,CCL_Hospital_Patient_ID__c,CCL_Treatment_Protocol_Subject_ID__c,CCL_Date_of_DOB__c,CCL_Month_of_DOB__c,CCL_Year_of_DOB__c,
                                           CCL_Planned_Apheresis_Pick_up_Date_Time__c,CCL_Infusion_Center__c,CCL_Plant__c,CCL_Ordering_Hospital__c,CCL_Order_Approval_Eligibility__c,CCL_Patient_Id__c,
                                           CCL_Patient_Initials__c,CCL_Patient_Weight__c, CCL_Prescriber__c, CCL_Returning_Patient__c, CCL_Ship_to_Location__c,CCL_Therapy__c
                                           ,CCL_Apheresis_Pickup_Location__c,RecordTypeId from CCL_Order__c where Id = :order.Id][0];


        CCL_Apheresis_Data_Form__c newADF = [Select Id, CCL_Apheresis_Collection_Center__c, CCL_Apheresis_Pickup_location__c, CCL_Date_Of_Birth_Text__c, CCL_Date_Of_Birth__c,
     CCL_Est_Apheresis_Shipment_Date__c, CCL_Infusion_Center__c, CCL_Manufacturing_Plant__c, CCL_Ordering_Hospital_Name__c, CCL_PRF_Updated__c,
     CCL_Patient_ID__c, CCL_Patient_Initials__c, CCL_Patient_Weight__c, CCL_Prescriber__c, CCL_Returning_Patient__c, CCL_Ship_to_Location__c, CCL_Therapy__c,
     CCL_Patient_Name__c, CCL_Treatment_Protocol_SubID_HospitalID__c, CCL_Order__c, CCL_Status__c from CCL_Apheresis_Data_Form__c where CCL_Order__c = :order.id][0];

        CCL_Shipment__c newShipmnt = [Select Id, CCL_Order_Apheresis__c, RecordTypeId, CCL_Collection_Center__c, CCL_PRF_Approved__c, CCL_Pick_up_Location__c, CCL_Date_of_Birth__c,
     CCL_Planned_Apheresis_Pickup_Date__c, CCL_Plant__c, CCL_Ordering_Hospital__c, CCL_Patient_Id__c, CCL_Indication_Clinical_Trial__c,
     CCL_Patient_Name__c, CCL_Protocol_Subject_Hospital_Patient__c from CCL_Shipment__c where CCL_Order_Apheresis__c= :order.id][0];
         updatedOrder.CCL_Apheresis_Collection_Center__c = null;
        updatedOrder.CCL_PRF_Ordering_Status__c = 'PRF_Approved';

       update updatedOrder;

        final CCL_Order__c updatedOrder2 = [Select Id,CCL_PRF_Ordering_Status__c,CCL_Novartis_Batch_ID__c,CCL_Patient_Name_Text__c,CCL_Apheresis_Collection_Center__c,
                                          CCL_Date_Of_Birth_Text__c,CCL_Hospital_Patient_ID__c,CCL_Treatment_Protocol_Subject_ID__c,CCL_Date_of_DOB__c,CCL_Month_of_DOB__c,CCL_Year_of_DOB__c,
                                           CCL_Planned_Apheresis_Pick_up_Date_Time__c,CCL_Infusion_Center__c,CCL_Plant__c,CCL_Ordering_Hospital__c,CCL_Order_Approval_Eligibility__c,CCL_Patient_Id__c,
                                           CCL_Patient_Initials__c,CCL_Patient_Weight__c, CCL_Prescriber__c, CCL_Returning_Patient__c, CCL_Ship_to_Location__c,CCL_Therapy__c
                                            ,CCL_Apheresis_Pickup_Location__c,RecordTypeId from CCL_Order__c where Id = :order.Id][0];

        Test.stopTest();
         final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();

        orderTriggerHandler.updateADFAprv(updatedOrder2, new Map<Id, CCL_Apheresis_Data_Form__c>{updatedOrder2.Id => newADF},updatedOrder);
         orderTriggerHandler.updateAPHAprv(updatedOrder2, new Map<Id, CCL_Shipment__c>{updatedOrder2.Id => newShipmnt},updatedOrder);
         orderTriggerHandler.updatefpShpmntAprv(updatedOrder2, new Map<Id, CCL_Shipment__c>{updatedOrder2.Id => newShipmnt},updatedOrder);
        System.assertEquals('PRF_Approved', updatedOrder2.CCL_PRF_Ordering_Status__c,
        'The order could not be updated to approved status');
    }
    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute validateLogisticStatusUpdate() where CCL_Logistic_Status__c is
     *                   updated to 'MFG_OnHold' and Manufacturing Summary does not have status 'Manufacturing Planned'
	 *  @param
	 *  @date            August 24, 2020
	 ********************************************************************************************************/
    private static testMethod void testValidateManufacturingStatusFailure() {

        final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();

        final String initialStatus = 'QA Testing Started';

        final CCL_Summary__c manufacturingSummary = new CCL_Summary__c(
            Name = 'Test Manufacturing Summary',
            RecordTypeId = CCL_StaticConstants_MRC.SUMMARY_RECORDTYPE_MANUFACTURING,
            CCL_Order__c = order.Id
        );
        insert manufacturingSummary;

        manufacturingSummary.CCL_Manufacturing_Status__c = initialStatus;
        update manufacturingSummary;

        Test.startTest();

        final CCL_Order__c updatedOrder = order.clone(true, false, false, false);
        updatedOrder.CCL_Logistic_Status__c = 'MFG_OnHold';

        final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();
	final CCL_Apheresis_Data_Form__c adfObj=CCL_Test_SetUp.createTestADF('test',updatedOrder.Id);
        final Id recordTypeId = Schema.SObjectType.CCL_summary__c.getRecordTypeInfosByName().get('Manufacturing').getRecordTypeId();
        final CCL_Summary__c summaryObj=CCL_Test_SetUp.createTestSummaryByRecordType(recordTypeId,adfObj.Id,updatedOrder.Id);
        final CCL_Batch__c bag=CCL_Test_SetUp.createTestBtch(adfObj.Id,updatedOrder.Id);
        orderTriggerHandler.validateLogisticStatusUpdate(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});
        orderTriggerHandler.updateRelatedRecordStatus(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});

        Test.stopTest();

        final CCL_Summary__c updatedManufacturingSummary = [SELECT CCL_Manufacturing_Status__c FROM CCL_Summary__c WHERE Id = :manufacturingSummary.Id LIMIT 1];
        System.assertEquals(initialStatus, updatedManufacturingSummary.CCL_Manufacturing_Status__c, 'The Manufacturing Summary status was updated unexpectedly.');
    }

    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute validateLogisticStatusUpdate() where CCL_Logistic_Status__c is
     *                   updated to 'SHP_OnHold' and FP Shipment has status 'Product Shipment Planned'
	 *  @param
	 *  @date            August 24, 2020
	 ********************************************************************************************************/
    private static testMethod void testValidateFPShipmentStatusSuccess() {

        final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();

        final CCL_Therapy__c therapy = CCL_TestDataFactory.createTestTherapy();
        insert therapy;

        final String initialStatus = 'Product Shipment Planned';
        final String expectedStatus = 'Product Delivery On Hold';

        final CCL_Shipment__c fpShipment = CCL_TestDataFactory.createTestFinishedProductShipment(Id.valueOf(order.Id), Id.valueOf(therapy.Id));
        fpShipment.CCL_Status__c = initialStatus;
        insert fpShipment;

        Test.startTest();

        final CCL_Order__c updatedOrder = order.clone(true, false, false, false);
        updatedOrder.CCL_Logistic_Status__c = 'SHP_OnHold';

        final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();
	final CCL_Apheresis_Data_Form__c adfObj=CCL_Test_SetUp.createTestADF('test',updatedOrder.Id);
        final Id recordTypeId = Schema.SObjectType.CCL_summary__c.getRecordTypeInfosByName().get('Manufacturing').getRecordTypeId();
        final CCL_Summary__c summaryObj=CCL_Test_SetUp.createTestSummaryByRecordType(recordTypeId,adfObj.Id,updatedOrder.Id);
             final CCL_Batch__c bag=CCL_Test_SetUp.createTestBtch(adfObj.Id,updatedOrder.Id);
        orderTriggerHandler.updateRelatedRecordStatus(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});

        Test.stopTest();

        final CCL_Shipment__c updatedFPShipment = [SELECT CCL_Status__c FROM CCL_Shipment__c WHERE Id = :fpShipment.Id LIMIT 1];
        System.assertEquals(initialStatus, updatedFPShipment.CCL_Status__c, 'The FP Shipment status did not get updated to the expected value.');
    }

    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute validateLogisticStatusUpdate() where CCL_Logistic_Status__c is
     *                   updated to 'SHP_OnHold' and FP Shipment has status 'Product Shipment Planned'
	 *  @param
	 *  @date            August 24, 2020
	 ********************************************************************************************************/
    private static testMethod void testValidateFPShipmentStatusSuccess1() {

        final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();

        final CCL_Therapy__c therapy = CCL_TestDataFactory.createTestTherapy();
        insert therapy;

        final String initialStatus = 'Product Shipment Planned';
        final String expectedStatus = 'Product Delivery On Hold';

        final CCL_Shipment__c fpShipment = CCL_TestDataFactory.createTestFinishedProductShipment(Id.valueOf(order.Id), Id.valueOf(therapy.Id));
        fpShipment.CCL_Status__c = initialStatus;
        insert fpShipment;

        Test.startTest();

        final CCL_Order__c updatedOrder = order.clone(true, false, false, false);
        updatedOrder.CCL_Logistic_Status__c = 'SHP_OnHold';

        final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();

        orderTriggerHandler.validateLogisticStatusUpdate(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});

        Test.stopTest();

        final CCL_Shipment__c updatedFPShipment = [SELECT CCL_Status__c FROM CCL_Shipment__c WHERE Id = :fpShipment.Id LIMIT 1];
        System.assertEquals(initialStatus, updatedFPShipment.CCL_Status__c, 'The FP Shipment status did not get updated to the expected value.');
    }

    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute validateLogisticStatusUpdate() where CCL_Logistic_Status__c is
     *                   updated to 'SHP_OnHold' and FP Shipment does not have status 'Product Shipment Planned'
	 *  @param
	 *  @date            August 24, 2020
	 ********************************************************************************************************/
    private static testMethod void testValidateFPShipmentStatusFailure() {

        final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();

        final CCL_Therapy__c therapy = CCL_TestDataFactory.createTestTherapy();
        insert therapy;

        final CCL_Shipment__c fpShipment = CCL_TestDataFactory.createTestFinishedProductShipment(Id.valueOf(order.Id), Id.valueOf(therapy.Id));
        final String initialStatus = 'Product Shipped';
        fpShipment.CCL_Status__c = initialStatus;
        insert fpShipment;

        Test.startTest();
		CCL_TriggerExecutionUtility.setProcessOrderTrigger(true);
        final CCL_Order__c updatedOrder = order.clone(true, false, false, false);
        updatedOrder.CCL_Logistic_Status__c = 'SHP_OnHold';

        final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();

        orderTriggerHandler.validateLogisticStatusUpdate(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});
        orderTriggerHandler.updateRelatedRecordStatus(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});

        Test.stopTest();

        final CCL_Shipment__c updatedFPShipment = [SELECT CCL_Status__c FROM CCL_Shipment__c WHERE Id = :fpShipment.Id LIMIT 1];
        System.assertEquals(initialStatus, updatedFPShipment.CCL_Status__c, 'The FP Shipment status was updated unexpectedly.');
    }

    /********************************************************************************************************
	 *  @author          Deloitte
	 *  @description     Test class to execute cancelOrderUpdateValidation() where CCL_Treatment_Status__c is
     *                  'Cancelled' and one of the non-exempt fields is updated
	 *  @param
	 *  @date            September 09, 2020
	 ********************************************************************************************************/
    private static testMethod void testCancelOrderUpdateValidation() {

        final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();
        order.CCL_Treatment_Status__c = 'Cancelled';
        order.CCL_Logistic_Status__c = 'APH_OnHold';

        final String expectedErrorMessage = System.Label.CCL_CancelOrderValidation;

        Test.startTest();

        final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();
        final CCL_Order__c updatedOrder = order.clone(true, false, false, false);

        try {
            updatedOrder.CCL_Logistic_Status__c = 'SHP_OnHold';
            orderTriggerHandler.cancelOrderUpdateValidation(new List<CCL_Order__c>{updatedOrder}, new Map<Id, CCL_Order__c>{order.Id => order});

        } catch(DmlException e) {
            System.assertEquals(expectedErrorMessage, e.getMessage(), 'An unexpected error was thrown.');
        }
		Test.stopTest();
    }


    private static testMethod void testUpdateOrder()
    {

         final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();
         test.startTest();
        final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();
        orderTriggerHandler.updateOrder(new List<CCL_Order__c>{order});
        System.assertEquals(order.CCL_PRF_Ordering_Status__c, CCL_StaticConstants_MRC.ordrStatus, 'Order Status updated to Submitted');
        test.stopTest();
    }

     private static testMethod void testUpdateBatch()
    {
         final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();
         List<CCL_Order__c> orderList = new List<CCL_Order__c>{order};
         test.startTest();
        final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();
        final CCL_Order__c updatedOrder = order.clone(true, false, false, false);

        //create batch test data
        List < CCL_Batch__c  > batchList = new List < CCL_Batch__c  > ();
        Map < Id, CCL_Batch__c  > batchOrdrMap = new Map < Id, CCL_Batch__c  > ();
        //create shipment data

        batchList= [Select Id,CCL_Infusion_Centre_Order__c,CCL_Ordering_Hospital__c,CCL_Ship_To_Location_Order__c,CCL_Pick_Up_Location__c,CCL_Apheresis_Collection_Center__c,CCL_Order__c from CCL_Batch__c  where  CCL_Order__c  =: orderList];

         for (CCL_Batch__c batch: batchList) {
   		  batchOrdrMap.put(batch.CCL_Order__c, batch);
		 }

        CCL_Batch__c batchReturn = orderTriggerHandler.updateBatch(updatedOrder,batchOrdrMap);
        system.assert(batchReturn != null);
        test.stopTest();
    }

     private static testMethod void testUpdateSummary()
    {
        final CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();
        final CCL_Order__c updatedOrder = order.clone(true, false, false, false);

        Map<Id,CCL_Summary__c> summaryOrdrMap = new Map<Id,CCL_Summary__c>();
		List < CCL_Summary__c > summaryList = new List < CCL_Summary__c > ();
        final CCL_Summary__c manufacturingSummary = new CCL_Summary__c(
            Name = 'Test Manufacturing Summary',
            RecordTypeId = CCL_StaticConstants_MRC.SUMMARY_RECORDTYPE_MANUFACTURING,
            CCL_Order__c = order.Id
        );
        insert manufacturingSummary;

        summaryList= [Select Id,CCL_Ship_To_Location__c,CCL_Ordering_Hospital__c,CCL_Infusion_Centre__c,CCL_Pick_Up_Location__c,CCL_Apheresis_Collection_Center__c,CCL_Order__c,CCL_Order_Collection__c from CCL_Summary__c ];

        Test.startTest();

        for (CCL_Summary__c summary: summaryList) {
                 summaryOrdrMap.put(summary.CCL_Order_Collection__c, summary);
          	      }
        final CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();
        CCL_Summary__c summ = orderTriggerHandler.updateSummary(order,summaryOrdrMap,updatedOrder);
		System.assertEquals(summ.CCL_Ordering_Hospital__c, order.CCL_Ordering_Hospital__c, 'Ordering hospital should be updated in summary');
    }


}
/**
* @File Name: spc_PatientStatusIndicatorsctrlTest.cls
* @Description:    Test method for spc_PatientStatusIndicatorsController
* @Author:    Deloitte
* @Group:     Apex
* @Last Modified time: 2018-08-02 10:29:21
*/
@isTest
private class spc_PatientStatusIndicatorsctrlTest {
  private static Case objCase;
  private static void setupTestdata() {
    List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();

    Account acc = spc_Test_Setup.createPatient('New Patient');
    acc.spc_HIPAA_Consent_Received__c = 'Yes';
    acc.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
    acc.spc_Patient_Services_Consent_Received__c = 'Yes';
    acc.spc_Text_Consent__c = 'Yes';
    acc.spc_Patient_Marketing_Consent_Date__c = Date.valueOf('2014-06-07');
    acc.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf('2014-06-07');
    acc.spc_Patient_Text_Consent_Date__c = Date.valueOf('2014-06-07');
    acc.spc_Patient_Service_Consent_Date__c = Date.valueOf('2014-06-07');
    spc_Database.ins(acc);

    Account phyAcc = spc_Test_Setup.createTestAccount('PC_Physician');
    phyAcc.Name = 'Acc_Name';
    spc_Database.ins(phyAcc);

    spc_Database.ins(apexConstants);
    Account patientAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());

    Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
    spc_Database.ins(manAcc);

    PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('Engagement Program SageRx', manAcc.Id);
    engPrgm.PatientConnect__PC_Program_Code__c = 'BREX';
    insert engPrgm;

    objCase = spc_Test_Setup.newCase(acc.Id, 'PC_Program');
    objCase.Status = 'Enrolled';
    objCase.PatientConnect__PC_Engagement_Program__c = engPrgm.Id;
    objCase.PatientConnect__PC_Status_Indicator_5__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.PATIENT_STATUS_INDICATOR_COMPLETE);
    objCase.PatientConnect__PC_Physician__c = phyAcc.Id;
    objCase.spc_REMS_Enrollment_Status__c = 'Complete';
    spc_Database.ins(objCase) ;
  }


  /**
  * @Name          testgetStatusIndicatorIcons_GetCasecontroller
  * @Description   Test method for spc_PatientStatusIndicatorsController
  * @Author        Deloitte
  * @CreatedDate   2018-08-02
  * @Return        void
  */
  private static testMethod void testgetStatusIndicatorIcons_GetCasecontroler() {
    setupTestdata();
    Test.StartTest();
    spc_PatientStatusIndicatorsController PI =  new spc_PatientStatusIndicatorsController();

    spc_PatientStatusIndicatorsController.getStages(objCase.ID);
    System.assert(spc_PatientStatusIndicatorsController.getStages(objCase.ID).size() > 0);

    Boolean PStage = spc_PatientStatusIndicatorsController.hasPreviousStage(objCase.ID);
    System.assertEquals(PStage, false);

    Case cs = objCase;
    cs.Status = System.Label.PatientConnect.PC_On_Hold;
    upsert cs;
    spc_PatientStatusIndicatorsController.getStages(cs.ID);
    Case cs2 = objCase;
    cs2.Status = 'Completed';
    upsert cs2;
    spc_PatientStatusIndicatorsController.getStages(cs2.ID);
    Case cs3 = objCase;
    cs3.Status = 'Never Start';
    upsert cs3;
    spc_PatientStatusIndicatorsController.getStages(cs3.ID);

    Test.StopTest();
  }

}
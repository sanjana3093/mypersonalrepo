@isTest
public class CCL_DocumentDeleteBatch_Test {

    static testMethod void batchTest() 
    {
        List<CCL_Document__c> docList= new List<CCL_Document__c>();
        for(Integer i=0 ;i <5;i++)
        {
            CCL_Document__c doc = new CCL_Document__c();
            doc.Name ='Name'+i;
            docList.add(doc);
        }
        
        insert docList;
        
        Test.startTest();

            CCL_DocumentDeleteBatch docDeleteBatch = new CCL_DocumentDeleteBatch();
            DataBase.executeBatch(docDeleteBatch); 
            
        Test.stopTest();
        
        // Assertion that documents have been deleted
        List<CCL_Document__c> docListAfterBatchExecution = new List<CCL_Document__c>();
        docListAfterBatchExecution = [SELECT Id,Name FROM CCL_Document__c];
        System.assertEquals(0,docListAfterBatchExecution.size(),'Error in document deletion batch');
    }
    
    static testMethod void batchSchedulerTest() {
    
    Test.StartTest();
    CCL_DocumentDeleteBatch_Scheduler sh1 = new CCL_DocumentDeleteBatch_Scheduler();
    String sch = '0 0 * * * ?'; 
    String jobid = System.schedule('Test', sch, sh1);
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
    
    // Verify the Cron expressions are the same
    System.assertEquals(sch, ct.CronExpression);
    
    // Verify the scheduled job has not triggered yet
    System.assertEquals(0, ct.TimesTriggered);
    Test.stopTest();
    }
    

}
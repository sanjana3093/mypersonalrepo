/*********************************************************************************************************
@author      : Deloitte
@description     : This is the cotroller of spc_PatientStatusIndicators lightning component and  Removed reference to Discontinued status as part of US-301035
@date    : July 13, 2018
--------------------------------------------------------------------------------------------------------------

****************************************************************************************************************/
public class spc_PatientStatusIndicatorsController {
    private static String recordTypeName;
    private static Boolean isLastStage = false;
    private static Boolean isPatientStatusNeverStart = false;
    private static Boolean isPatientStatusCompleted = false;
    /*********************************************************************************
    @description   retreive list of initial stages for a case
    @return    List<stageWrapper>
    @param     CaseID the Case to get the indicators (stages) from
    *********************************************************************************/
    @AuraEnabled
    public static List<stageWrapper> getStages(String caseID) {
        List<stageWrapper> stageWrappers = new List<stageWrapper>();
        try {

            List<PatientConnect__PC_Program_Status__mdt> stageTypes = new List<PatientConnect__PC_Program_Status__mdt>();

            //Map for Name > Status Meta Data
            Map<String, PatientConnect__PC_Program_Status__mdt> mStageTypesByName = new Map<String, PatientConnect__PC_Program_Status__mdt>();
            //Map for Order > Status Meta Data
            Map<String, PatientConnect__PC_Program_Status__mdt> mStageTypesByOrder = new Map<String, PatientConnect__PC_Program_Status__mdt>();

            /*
            * FLS Check for Case
            */
            List<String> fields = new List<String> {'Status', 'PC_Previous_Status__c'};
            spc_Database.assertAccess('Case', spc_Database.Operation.Reading, fields);
            /*
            * FLS Check for PC_Engagement_Program__c
            */

            spc_Database.assertAccess('PatientConnect__PC_Engagement_Program__c', spc_Database.Operation.Reading, fields);

            //Retrieve Case
            List<Case> cases = [SELECT Id, Status, PatientConnect__PC_Previous_Status__c,
                                PatientConnect__PC_Engagement_Program__r.PatientConnect__PC_Program_Code__c, RecordType.DeveloperName
                                FROM Case
                                WHERE Id = : caseID];
            //Decimals
            decimal dCompletedTo = -9999;
            decimal dInProgressTo = - 9999;
            decimal dLastStage = - 9999;


            if (cases.size() > 0) {
                Case c = cases[0];
                recordTypeName = c.RecordType.DeveloperName;
                //Update Case
                stageTypes = getStatusTypes(c.PatientConnect__PC_Engagement_Program__r.PatientConnect__PC_Program_Code__c);

                if (stageTypes != null && stageTypes.size() > 0) {
                    dLastStage = stageTypes[stageTypes.size() - 1].PatientConnect__Order__c;
                }

                //Create maps
                for (PatientConnect__PC_Program_Status__mdt stageType : stageTypes) {
                    mStageTypesByName.put(stageType.PatientConnect__PC_Case_Status__c, stageType);
                    mStageTypesByOrder.put(String.ValueOf(stageType.PatientConnect__Order__c), stageType);
                }

                if (c.Status == System.Label.PatientConnect.PC_On_Hold || c.Status == System.Label.PatientConnect.PC_Withdrawn) {
                    c.Status = c.PatientConnect__PC_Previous_Status__c;
                }

                PatientConnect__PC_Program_Status__mdt st = mStageTypesByName.get(c.status);

                if (st != null) {
                    dCompletedTo = st.PatientConnect__Order__c - 1;
                    dInProgressTo = st.PatientConnect__Order__c;
                    if (dLastStage == st.PatientConnect__Order__c) {
                        isLastStage = true;
                    }
                }
                if (String.isNotBlank(c.status) && c.status.equalsIgnoreCase('Never Start')) {
                    isPatientStatusNeverStart = true;
                }
                if (String.isNotBlank(c.status) && c.status.equalsIgnoreCase('Completed')) {
                    if (st != null) {
                        dCompletedTo = st.PatientConnect__Order__c ;
                        dInProgressTo = st.PatientConnect__Order__c;
                        isPatientStatusCompleted = true;
                    }
                }
            }
            stageWrappers = createStageWrappers(stageTypes, dCompletedTo, dInProgressTo, isLastStage);
        } catch (exception e) {
            spc_Utility.logAndThrowException(e);
        }

        return stageWrappers;
    }

    /*********************************************************************************
    @description    : Create a list of stage Wrapper objects
    @param List<PatientConnect__PC_Program_Status__mdt> stageTypes
    @param Decimal dCompletedTo
    @param Decimal dInProgressTo
    @param Boolean isLastStage
    @return     List<stageWrapper>
    **********************************************************************************/
    private static List<stageWrapper> createStageWrappers(List<PatientConnect__PC_Program_Status__mdt> stageTypes, Decimal dCompletedTo, Decimal dInProgressTo, Boolean isLastStage) {
        List<stageWrapper> stageWrappers = new List<stageWrapper>();
        for (PatientConnect__PC_Program_Status__mdt stage : stageTypes) {
            Boolean isComp = stage.PatientConnect__Order__c <= dCompletedTo;
            stageWrapper sw = null;
            if (stage.PatientConnect__PC_Indicator_Status__c == 'Never Start' && isPatientStatusCompleted) {
                sw = new stageWrapper(stage.PatientConnect__PC_Indicator_Status__c, stage.PatientConnect__Order__c,
                                      'never start' );
            } else {
                sw = new stageWrapper(stage.PatientConnect__PC_Indicator_Status__c, stage.PatientConnect__Order__c,
                                      isPatientStatusNeverStart && stage.PatientConnect__PC_Indicator_Status__c != 'Discontinued' ? 'discountinued'
                                      : stage.PatientConnect__Order__c <= dCompletedTo ? 'complete' : isLastStage ? 'complete'
                                      : stage.PatientConnect__Order__c == dInProgressTo ? 'inprogress' : 'incomplete' );
            }
            stageWrappers.add(sw);
        }
        return stageWrappers;
    }

    /*********************************************************************************
    @description   retrieve status types
    @param String programCode
    @return List<PC_Program_Status__mdt>
    *********************************************************************************/
    private static List<PatientConnect__PC_Program_Status__mdt> getStatusTypes(String programCode) {
        try {
            List<String> fields = new List<String> {'PatientConnect__PC_Indicator_Status__c', 'PatientConnect__Order__c',
                                                    'PC_Active__c', 'PatientConnect__PC_Case_Status__c'
                                                   };
            spc_Database.assertAccess('PatientConnect__PC_Program_Status__mdt', spc_Database.Operation.Reading, fields);
            //TODO: check this
            RecordType recordTypeObj = [SELECT Id, DeveloperName, NamespacePrefix FROM RecordType where DeveloperName = :recordTypeName];

            return [SELECT Id, PatientConnect__Order__c, PatientConnect__PC_Case_Status__c, PatientConnect__PC_Indicator_Status__c
                    FROM PatientConnect__PC_Program_Status__mdt
                    WHERE PatientConnect__PC_Program_Code__c = : programCode
                            AND PatientConnect__PC_Active__c = true
                                    AND PatientConnect__PC_RecordType__c = : recordTypeName
                                            AND PatientConnect__PC_RecordType_Namespaceprefix__c = : recordTypeObj.NamespacePrefix
                                                    ORDER BY PatientConnect__Order__c ASC];
        } catch (exception e) {
            spc_Utility.logAndThrowException(e);
            return null;
        }
    }

    /*********************************************************************************
    @description    retrieve previous status if the current one is "on hold"
    @return    String
    *********************************************************************************/
    @AuraEnabled
    public static Boolean hasPreviousStage(String caseID) {
        try {
            List<String> fields = new List<String> {'PatientConnect__PC_Previous_Status__c'};
            spc_Database.assertAccess('Case', spc_Database.Operation.Reading, fields);
            List<Case> cases = [select ID, PatientConnect__PC_Previous_Status__c from Case where  ID = : caseID limit 1];
            if (cases.size() == 1 && !String.isBlank(cases[0].PatientConnect__PC_Previous_Status__c)) {
                return true;
            }
        } catch (exception e) {
            spc_Utility.logAndThrowException(e);
        }
        return false;
    }

    public class stageWrapper {
        @AuraEnabled public String stageName {get; set;}
        @AuraEnabled public Decimal stageOrder {get; set;}
        @AuraEnabled public String stageStatus {get; set;}
        public stageWrapper(String stageSetName, Decimal stageSetOrder, string isSetStatus) {
            this.stageName = stageSetName;
            this.stageOrder = stageSetOrder;
            this.stageStatus = isSetStatus;
        }
    }
}
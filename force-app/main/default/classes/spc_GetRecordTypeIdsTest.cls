/**
* @author Deloitte
* @date 06/18/2018
*
* @description This is the Test Class for GetRecordTypeIds
*/

@isTest(SeeAllData = true)
public class spc_GetRecordTypeIdsTest {

	static testmethod void GetRecordTypeIdsTest() {
		Account obj = new Account(Name = 'Test');
		insert obj;

		PageReference pageRef = Page.Veeva_Search_Widget_HCO;
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController sc = new ApexPages.StandardController(obj);
		GetRecordTypeIds testAccPlan = new GetRecordTypeIds(sc);
	}

}
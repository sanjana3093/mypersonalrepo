/********************************************************************************************************
*  @author          Deloitte
*  @description     Test class to verify the population of the CCL_Unique_Combination__c field
*  @param           
*  @date            July 10, 2020
*********************************************************************************************************/

@isTest
public class CCL_UserTherapyTriggerHandlerTest {

    /* Test method to setup data for the User Therapy Record*/
    @testSetup
    public static void userTherapyCreation() {
        
    final String currentTime= String.valueOf(System.now().millisecond());
    final String randomNum= String.valueOf(Math.abs(Crypto.getRandomInteger()));
        
        final User internalBaseProfileUser = new User();
            final Profile internalProfile = [SELECT Id FROM Profile WHERE Name =:CCL_StaticConstants_MRC.INTERNAL_BASE_PROFILE_TYPE];
            internalBaseProfileUser.ProfileId= internalProfile.Id;
            internalBaseProfileUser.LastName= 'test LastName';
            internalBaseProfileUser.FirstName= 'test FirstName';
            internalBaseProfileUser.Alias='testUser';
            internalBaseProfileUser.Email='testUser@novartis.com';
            internalBaseProfileUser.emailencodingkey = 'UTF-8';
            internalBaseProfileUser.languagelocalekey = 'en_US';
            internalBaseProfileUser.localesidkey = 'en_US';
            internalBaseProfileUser.country = 'United States';
            internalBaseProfileUser.timezonesidkey = 'America/Los_Angeles';
            internalBaseProfileUser.username = 'testUser' + randomNum + currentTime +'@novartis.com';
            insert internalBaseProfileUser;
     
        final Account siteAccount = new Account();
            final CCL_Time_Zone__c newTimeZoneObj = new CCL_Time_Zone__c();
            newTimeZoneObj.Name = 'NewTime Zone';
            newTimeZoneObj.CCL_SAP_Time_Zone_Key__c = System.currentTimeMillis()+'687897-test';
            newTimeZoneObj.CCL_External_ID__c = '66677';
            newTimeZoneObj.CCL_Saleforce_Time_Zone_Key__c = System.currentTimeMillis()+'79788-test';
            insert newTimeZoneObj;
        	final String siteAccountRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CCL3_Site_Account').getRecordTypeId();
            siteAccount.RecordTypeId = siteAccountRT;
            siteAccount.Name  = 'Test Site Name 444';
            siteAccount.CCL_Active__c = True;
            siteAccount.CCL_Label_Compliant__c   = 'SEC';
            siteAccount.CCL_Type__c = CCL_StaticConstants_MRC.ACCOUNT_TYPE_CUSTOMER;
            siteAccount.CCL_External_ID__c = 'Test ID';
            siteAccount.CCL_Capability__c = 'L1O';
            siteAccount.ShippingCountry = 'Ireland';
            siteAccount.CCL_Time_Zone__c=newTimeZoneObj.Id;
            insert siteAccount;
            
        final CCL_Therapy__c testTherapy = new CCL_Therapy__c();
            testTherapy.Name = 'Therapy 12';
            testTherapy.CCL_Type__c = 'Commercial';
            testTherapy.CCL_Therapy_Description__c = 'therapy';
            testTherapy.CCL_Status__c = 'Active';
            insert testTherapy;
                                  
        final CCL_Therapy_Association__c testTherapyAssociation = new CCL_Therapy_Association__c();
            testTherapyAssociation.RecordTypeId = CCL_StaticConstants_MRC.THERAPY_ASSOCIATION_RECORDTYPE_SITE;
            testTherapyAssociation.CCL_Site__c = siteAccount.Id;
            testTherapyAssociation.CCL_Therapy__c= testTherapy.Id;
            testTherapyAssociation.CCL_Hospital_Patient_ID_Opt_In__c = CCL_StaticConstants_MRC.VALUE_YES;
            testTherapyAssociation.CCL_Infusion_Data_Collection__c = CCL_StaticConstants_MRC.VALUE_YES;
            insert testTherapyAssociation;
        
        final Contact testContact = new Contact();
            testContact.LastName = 'Test LastName';
        	testContact.FirstName ='Test FirstName';
        	testContact.AccountId = siteAccount.Id;
            insert testContact;

        
            test.startTest();
        
        final List<CCL_User_Therapy_Association__c> userTherapyList = new List<CCL_User_Therapy_Association__c>();
        
        final CCL_User_Therapy_Association__c testUserTherapy = new CCL_User_Therapy_Association__c();
            final String userTherapyRT = Schema.SObjectType.CCL_User_Therapy_Association__c.getRecordTypeInfosByName().get('Internal').getRecordTypeId();
            testUserTherapy.RecordTypeId = userTherapyRT;
            testUserTherapy.Name ='UniqueFieldTest';
            testUserTherapy.CCL_User__c = internalBaseProfileUser.Id;
            testUserTherapy.CCL_Primary_Role__c = 'PRF Submitter';
            testUserTherapy.CCL_Record_Accessability__c = 'Edit';
            testUserTherapy.CCL_Therapy_Association__c = testTherapyAssociation.Id;
            userTherapyList.add(testUserTherapy);
        
        final CCL_User_Therapy_Association__c testUserTherapyPrescriber = new CCL_User_Therapy_Association__c();
            final String userTherapyRTPre = Schema.SObjectType.CCL_User_Therapy_Association__c.getRecordTypeInfosByName().get('Prescriber / Principal Investigator').getRecordTypeId();
            testUserTherapyPrescriber.RecordTypeId = userTherapyRTPre;
            testUserTherapyPrescriber.Name ='UniqueFieldTestPrescriber';
            testUserTherapyPrescriber.CCL_Contact__c = testContact.Id;
            testUserTherapyPrescriber.CCL_Primary_Role__c = 'PRF Submitter';
            testUserTherapyPrescriber.CCL_Record_Accessability__c = 'Edit';
            testUserTherapyPrescriber.CCL_Therapy_Association__c = testTherapyAssociation.Id;
            userTherapyList.add(testUserTherapyPrescriber);

        	insert userTherapyList;
        
         	test.stopTest();
        
    }
         
    static testmethod void testDuplicationforInternalandExternal() {
        
        final CCL_User_Therapy_Association__c assertUserTherapy = [SELECT ID, Name, CCL_User__c, CCL_Contact__c, CCL_Therapy_Association__c, CCL_Unique_Combination__c FROM CCL_User_Therapy_Association__c WHERE Name = 'UniqueFieldTest'];
        final String uniqueCombination = assertUserTherapy.CCL_User__c + '' + AssertUserTherapy.CCL_Therapy_Association__c;       
		
        // This asserts that the unique combination field is a concatenation of both the UserId and the therapy Association Id
        system.assertEquals( uniqueCombination , assertUserTherapy.CCL_Unique_Combination__c, CCL_StaticConstants_MRC.INTERNAL_UNIQUE_ERROR_FIELD );

       
       }
       
    
    static testmethod void testDuplicationforPrescriber() {
              
        final CCL_User_Therapy_Association__c assertUserTherapyPrescriber = [SELECT ID, Name, CCL_User__c, CCL_Contact__c, CCL_Therapy_Association__c, CCL_Unique_Combination__c FROM CCL_User_Therapy_Association__c WHERE Name = 'UniqueFieldTestPrescriber'];
        final String uniqueCombinationPrecriber = assertUserTherapyPrescriber.CCL_Contact__c + '' + assertUserTherapyPrescriber.CCL_Therapy_Association__c;

        // This asserts that the unique combination field is a concatenation of both the Contact Id and the therapy Association Id
        system.assertEquals( uniqueCombinationPrecriber , assertUserTherapyPrescriber.CCL_Unique_Combination__c, CCL_StaticConstants_MRC.PRESCRIBER_UNIQUE_ERROR_FIELD );

    }
    
}
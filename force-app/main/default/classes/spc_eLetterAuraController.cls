/**
* @author Deloitte
* @date 22-June-16
*
* @description This is The PC class cloned by Sage for Reference only
* Methods for eLetter lightning component
*/

public with sharing class spc_eLetterAuraController {

    private static final String EMPTY_STRING = '';

    /*******************************************************************************************************
    * @description Retrieves available eLetters by object type
    * @param recordId Case Id
    * @param invokeAction String
    * @param isFaxOnly Boolean
    * @return List<PC_LightningMap>
    */

    @AuraEnabled
    public static List<spc_LightningMap> getLetters(Id recordId, String invokeAction, boolean isFaxOnly) {
        List<spc_LightningMap> lstAvailableeLetters = new List<spc_LightningMap>();
        String objType = String.valueOf(recordId.getSobjectType());
        String recordTypeName = EMPTY_STRING;
        List<Case> newCs = [Select recordtypeid, PatientConnect__PC_Program__c from Case where id = :recordId];
        String caseRcrdType = Schema.SObjectType.Case.getRecordTypeInfosById().get(newCs[0].recordtypeid).getName();
        spc_PatientConsentDetails consentDetails;

        if (caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_POST_TREATMENT)) || caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_PRE_TREATMENT))) {
            consentDetails = spc_PatientConsentDetails.getInstance(newCs[0].PatientConnect__PC_Program__c);
        } else if (caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM))
         || caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_INQUIRY))
         || caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_REFERRAL))
         || caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_CERTIFICATION_LABEL))) {
            consentDetails = spc_PatientConsentDetails.getInstance(recordId);
        }




        /* End */
        if(isFaxOnly){
           lstAvailableeLetters.add(new spc_LightningMap(Label.spc_Select_a_Fax, Label.PatientConnect.PC_None));
       }else{
        lstAvailableeLetters.add(new spc_LightningMap(Label.PatientConnect.PC_Select_an_eLetter, Label.PatientConnect.PC_None));
    }

    List<Case> currentCase = new List<Case>();
    currentCase = [SELECT ID, RecordType.Name, PatientConnect__PC_Engagement_Program__c, spc_is_PostTreatment_Case__c, spc_is_Pretreatment_Case__c, spc_Is_Enquiry_Case__c FROM CASE WHERE ID = : recordId LIMIT 1];

    try {
        List<String> fields = new List<String> {'Name'};

        spc_Database.assertAccess(spc_ApexConstants.getQualifiedAPIName(spc_ApexConstants.ELETTER_FORM_FIELD_SET), spc_Database.Operation.Reading, fields);

        if (!currentCase.isEmpty()) {

            Map<ID, Set<String>> cChannels = null;
            
            List<String> consentlst = spc_Utility.fetchConsentList(consentDetails);
            String consentString = string.join(consentlst, ',');
            Set<String> consentAllSet = new Set<String>();
            consentAllSet.addAll(consentString.remove('\'').split(','));
            if (isFaxOnly) {
                cChannels = spc_eLetterService.getMapBetweenFaxAndCchannels(new Set<ID> {currentCase[0].PatientConnect__PC_Engagement_Program__c});
            } else {
                cChannels = spc_eLetterService.getMapBetweenEletterAndCchannels(new Set<ID> {currentCase[0].PatientConnect__PC_Engagement_Program__c});
            }
            String query = 'SELECT Id,spc_All_Consents_Mandatory__c,spc_Consent_Eletter_Needeed__c, Name FROM PatientConnect__PC_eLetter__c';
            query += ' WHERE  PatientConnect__PC_Source_Object__c = \'' + objType + '\' AND PatientConnect__PC_Object_Record_Types__c includes (\'' + currentCase[0].RecordType.Name + '\')';
            if (!consentlst.isEmpty()) {
                query += ' AND  (spc_Consent_Eletter_Needeed__c includes' + consentlst;
                query += 'OR spc_Consent_Eletter_Needeed__c=\'' + spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.DEFAULT_CONSENT) + '\') order by Name';
            } else {
                query += 'AND spc_Consent_Eletter_Needeed__c=\'' + spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.DEFAULT_CONSENT) + '\' order by Name';
            }
            List<PatientConnect__PC_eLetter__c> eletterslst = spc_Database.queryWithAccess(query , false);
            for (PatientConnect__PC_eLetter__c el : eletterslst ) {

                if (currentCase[0].spc_Is_Enquiry_Case__c || currentCase[0].spc_is_Pretreatment_Case__c || currentCase[0].spc_is_PostTreatment_Case__c) {
                    lstAvailableeLetters.add(new spc_LightningMap(el.Name, el.Id));

                } else if (cChannels.get(el.Id) != null) {
                    Set<String> filteredList = cChannels.get(el.Id);
                    if (!filteredList.isEmpty()) {
                        boolean hideEmailRow = filteredList.contains(System.Label.spc_Email_Template);
                        boolean hideFaxRow = filteredList.contains(System.Label.spc_Fax_Template);
                        if (el.spc_All_Consents_Mandatory__c) {
                            Set<String> eletterConsentSet = new Set<String>();
                            List<String> tmpString = new List<String>();
                            tmpString.addAll(el.spc_Consent_Eletter_Needeed__c.split(';'));
                            eletterConsentSet.addAll(tmpString);
                            If(consentAllSet.containsAll(eletterConsentSet) ) {
                                lstAvailableeLetters.add(new spc_LightningMap(el.Name, el.Id, hideFaxRow, hideEmailRow));
                            }

                        } else {
                            lstAvailableeLetters.add(new spc_LightningMap(el.Name, el.Id, hideFaxRow, hideEmailRow));
                        }

                    }
                }
            }
            if(currentCase[0].spc_Is_Enquiry_Case__c  || currentCase[0].spc_is_Pretreatment_Case__c || currentCase[0].spc_is_PostTreatment_Case__c){
                lstAvailableeLetters.addAll(spc_Utility.fetchAdditionaleLetters(consentlst,consentAllSet,currentCase[0],objType,cChannels));
            }
        }
    } catch (Exception ex) {
        spc_Utility.logAndThrowException(ex);
    }

    return lstAvailableeLetters;

}
    /*******************************************************************************************************
    * @description Retrieves eLetters recepients based on selected letter
    * @param letterId eLetter Id
    * @param recordId The Case Id
    * @param isFaxSpecific Boolean
    * @return List<spc_eLetter_Recipient>
    */

    @AuraEnabled
    public static List<spc_eLetter_Recipient> getRecepients(String letterId, Id recordId, boolean isFaxSpecific) {
        String objType = String.valueOf(recordId.getSobjectType());
        List<spc_eLetter_Recipient> recepients = new List<spc_eLetter_Recipient> ();
        Id caseId;
        List<Case> newCs = [Select recordtypeid, PatientConnect__PC_Program__c from Case where id = :recordId];
        String caseRcrdType = Schema.SObjectType.Case.getRecordTypeInfosById().get(newCs[0].recordtypeid).getName();
        if (caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_POST_TREATMENT))
            || caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_PRE_TREATMENT))) {
            caseId  = newCs[0].PatientConnect__PC_Program__c;
    } else {
        caseId  = recordId;
    }

    try {
        if (String.valueof(letterId) != Label.PatientConnect.PC_None) {
            PatientConnect__PC_eLetter__c eLetter = spc_eLetterService.getELetter(letterId);
            List<String> fields;

            fields = new List<String> {'CaseNumber'};
            spc_Database.assertAccess('Case', spc_Database.Operation.Reading, fields);
            fields = new List<String> {'Name'};
            spc_Database.assertAccess('Account', spc_Database.Operation.Reading, fields);

            Case objCase = [SELECT Id, CaseNumber, RecordType.Name, Account.Name FROM Case WHERE Id = :caseId];

                // Get available languages from the eLetter Template and dynamically display the participant language:
            List<SelectOption> lstCommunicationLanguage = new List<SelectOption> ();
            for (String lang : eLetter.PatientConnect__PC_Communication_Language__c.split(';')) {
                lstCommunicationLanguage.add(new SelectOption(lang, lang));
            }

            recepients = spc_eLetterService.getParticipants(objCase.Id, eLetter, isFaxSpecific);
                if (recepients == null) return null; // If no participants found, don't proceed.

                // After getting all the Recipients, for special eLetter templates (with Allow_Editing=true or Send_To_User=True) set the sendToMe flag to True and uncheck other flags
                if (eLetter.PatientConnect__PC_Send_to_User__c && eLetter.PatientConnect__PC_Object_Record_Types__c != spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_INQUIRY)) {
                    for (spc_eLetter_Recipient rec : recepients) {
                        rec.sendToMe = true;
                        rec.sendFax = false;
                        rec.sendEmail = false;
                        // Enable SendToMe column
                        rec.bDisableSendToMe = false;
                        // Disble the SendFax & SendEmail checkboxes on eLetter Page:
                        rec.bDisableEmail = true;
                        rec.bDisableFax = true;
                    }
                } else {
                    // Disble the SendToMe checkboxes on eLetter Page for the eLetters where 'Send to User' is not selected.
                    for (spc_eLetter_Recipient rec : recepients) {
                        rec.sendToMe = false;
                        // Disable SendToMe column
                        rec.bDisableSendToMe = true;
                        // Enable SendFax and SendEmail Column
                        rec.bDisableEmail = false;
                        rec.bDisableFax = false;
                    }
                }
            }
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }
        return recepients;
    }

    /*******************************************************************************************************
    * @description  Send eLetter to select recepients.
    * @param recipientsString JSON string containing spc_eLetter_Recipient list
    * @param letterId Record Id of the eLetter object
    * @param objectId Record Id of object the letter is being sent from - looks like it is always Program Case
    * @return String returnMessage
    */

    @AuraEnabled
    public static string sendOutboundDocuments(String recipientsString, String letterId, Id objectId) {
        String returnMessage = EMPTY_STRING;
        String emailErrorMessage = EMPTY_STRING;
        String faxErrorMessage = EMPTY_STRING;

        try {

            //Populate Zpaper Template Data : Nikunj Vadi
            List<Case> lstCase = [Select ID, spc_WC__c, spc_Template_Data__c, spc_Template_Data__r.ID from Case Where ID = :objectId];
            spc_zPaperData obj = new spc_zPaperData(lstCase);

            List<spc_eLetter_Recipient> recepients = (List<spc_eLetter_Recipient>) System.JSON.deserialize(recipientsString, List<spc_eLetter_Recipient>.class);
            // If no recipients are available then inform the user:
            if (recepients == null || recepients.size() == 0 || String.valueOf(letterId) == Label.PatientConnect.PC_None) {
                return System.Label.PatientConnect.PC_No_Recipients_Available;
            }

            // Prepare a list of accounts with preferences selected on page:
            List<Account> faxRecipients = new List<Account>();
            List<Account> emailRecipients = new List<Account>();
            List<Account> sendToUserRecipients = new List<Account>();

            PatientConnect__PC_eLetter__c eLetter = spc_eLetterService.getELetter(letterId);

            for (Case caseobj : lstCase) {
                if (caseobj.spc_WC__c != spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.PATIENT_STATUS_INDICATOR_COMPLETE)
                    && eLetter.Name == system.label.spc_Welcome_Package_eltter) {
                    return system.Label.spc_Welcome_Package_Error_Msg;
            }
        }
            //Store the recipient names for displaying info message to the user (boolean sendEmail/sendFax is not considered now)
        String faxRecipientString = EMPTY_STRING, emailRecipientString = EMPTY_STRING;
            // Collect the list of selected recipients with their language selection at Page level.
        Account overriddenRecipient;
            //Check if recipient is Designated Caregiver and has marketing consent
        Set<Id> designatedCaregiverAccounts = new Set<id>();
        for (spc_eLetter_Recipient recipient : recepients) {
            if (spc_ApexConstants.ASSOCIATION_ROLE_DESIGNATED_CAREGIVER.equalsIgnoreCase(recipient.recipientType)
                &&  null != recipient.recipient && recipient.sendEmail) {
                designatedCaregiverAccounts.add(recipient.recipient.id);
        }
    }
    if (!designatedCaregiverAccounts.isEmpty()) {
        for (Account acc : [select spc_Patient_Mrkt_and_Srvc_consent__c from Account where id = :designatedCaregiverAccounts]) {
            if (!System.Label.spc_Yes.equalsIgnoreCase(acc.spc_Patient_Mrkt_and_Srvc_consent__c)) {
                return System.Label.spc_caregiverMarketingConsentMissing;
            }
        }
    }
    for (spc_eLetter_Recipient recipient : recepients) {
        overriddenRecipient = recipient.recipient;
        overriddenRecipient.PatientConnect__PC_Communication_Language__c = recipient.communicationLanguage;
        if (recipient.sendFax) {
                    // This list will be explicitly passed to PC_eLetterService to override the preferred method of communication selection at participant level.
            if (recipient.recipientFax == null) {
                faxErrorMessage += overriddenRecipient.Name + '; ';
            }  else {
                faxRecipients.add(overriddenRecipient);
                faxRecipientString += overriddenRecipient.Name + '; ';
            }
        }
        if (recipient.sendEmail) {
                    // This list will be explicitly passed to PC_eLetterService.SendEmail() Method to override the preferred method of communication selection at participant level.
            if (recipient.recipientEmail == null) {
                emailErrorMessage += overriddenRecipient.Name + '; ';
            }  else {
                emailRecipients.add(overriddenRecipient);
                emailRecipientString += overriddenRecipient.Name + '; ';
            }
        }
        if (recipient.sendToMe) {
                    // This list will be explicitly passed to PC_eLetterService.SendLetterToUser() to override the preferred method of communication/Language selection at participant level.
            sendToUserRecipients.add(overriddenRecipient);
        }
    }
            // If the recipient list is empty then throw error message else info message
    if (faxRecipients.isEmpty() && emailRecipients.isEmpty() && sendToUserRecipients.isEmpty() && faxErrorMessage.equals(EMPTY_STRING) && emailErrorMessage.equals(EMPTY_STRING)) {
        returnMessage = System.Label.PatientConnect.PC_No_Record_Selected;
    } else {
        if (!emailRecipients.isEmpty()) {
                    emailRecipientString = emailRecipientString.left(emailRecipientString.length() - 2) + '.  '; // Remove the last '; ' from the list of email recipients:
                    returnMessage = System.Label.PatientConnect.PC_Generating_eLetter_for + emailRecipientString;
                    emailRecipientString = returnMessage;
                }
                if (!faxRecipients.isEmpty()) {
                    faxRecipientString = faxRecipientString.left(faxRecipientString.length() - 2) + '.  '; // Remove the last '; ' from the list of fax recipients:
                    returnMessage = System.Label.PatientConnect.PC_Generating_Fax_for + faxRecipientString;
                    if (!String.isBlank(emailRecipientString)) {returnMessage += emailRecipientString;}
                }
                if (!sendToUserRecipients.isEmpty()) {
                    returnMessage = System.Label.PatientConnect.PC_eLetters_Sent_to_User;
                }
            }

            // For the eLetters with 'Send to User' checked, send the eLetters to logged in User.
            // 'Send to User' Takes precedence over 'Allow Editing' (if both flags are checked , then eLetter will be sent to logged in user for further editing/processing)
            if (eLetter.PatientConnect__PC_Send_to_User__c && sendToUserRecipients.size() > 0) {
                spc_eLetterService.sendLetter(objectId, eLetter, spc_ApexConstants.EMAIL, sendToUserRecipients, true);
            } else {
                // Invoke the eLetterService method to
                if (faxRecipients.size() > 0) {
                    spc_eLetterService.sendLetter(objectId, eLetter, spc_ApexConstants.FAX, faxRecipients, false);
                }

                if (emailRecipients.size() > 0) {
                    spc_eLetterService.sendLetter(objectId, eLetter, spc_ApexConstants.EMAIL, emailRecipients, false);
                }
            }
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }

        if (!emailErrorMessage.equals(EMPTY_STRING) || !faxErrorMessage.equals(EMPTY_STRING)) {
            if (!emailErrorMessage.equals(EMPTY_STRING)) {
                emailErrorMessage = ' ' + emailErrorMessage.left(emailErrorMessage.length() - 2) + '.  ';
                returnMessage += ' ' + System.Label.spc_InvalidEmailNumberPrefix + emailErrorMessage + System.Label.spc_InvalidPhoneNumberPostfix + ' ';
            }
            if (!FaxErrorMessage.equals(EMPTY_STRING)) {
                faxErrorMessage = ' ' + faxErrorMessage.left(faxErrorMessage.length() - 2) + ' ';
                returnMessage += ' ' + System.Label.spc_InvalidFaxNumberPrefix + faxErrorMessage + System.Label.spc_InvalidPhoneNumberPostfix + ' ';
            }
        }
        return returnMessage;
    }
}
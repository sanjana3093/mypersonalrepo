/*********************************************************************************************************
* @author Deloitte
* @date July 7,2018
* @description This handles operation related to DML operations of association
****************************************************************************************************************/
public class spc_AssociationTriggerHandler extends spc_TriggerHandler {

  private static spc_AssociationTriggerImplementation assImp = new spc_AssociationTriggerImplementation();
  private static spc_CommunicationTriggers CommtriggerImplementation = new spc_CommunicationTriggers();

  public override void afterInsert() {
    List<PatientConnect__PC_Association__c> lstAssociation = new List<PatientConnect__PC_Association__c>();
    Map<Id, Id> accountTocaseMap = new Map<Id, Id>();
    for (PatientConnect__PC_Association__c oAssoc : (List<PatientConnect__PC_Association__c>)Trigger.new) {
      if (oAssoc.PatientConnect__PC_Account__c != NULL
          && oAssoc.PatientConnect__PC_Role__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_PAYER)
          && oAssoc.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {
        lstAssociation.add(oAssoc);
      }
      if (oAssoc.PatientConnect__PC_Role__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO)
          && oAssoc.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {
        accountTocaseMap.put(oAssoc.PatientConnect__PC_Account__c, oAssoc.PatientConnect__PC_Program__c);
      }
    }
    if (!accountTocaseMap.isEmpty()) {
      assImp.createHCPLogisticTasks(accountTocaseMap);
    }
    if (! lstAssociation.isEmpty()) {
      spc_AssociationTriggerImplementation.populateMarketAccessOnCase(lstAssociation);
    }    
  }

  public override void beforeInsert() {
    List<PatientConnect__PC_Association__c> lstActiveSOCAssociations = new List<PatientConnect__PC_Association__c>();
    Map<Id, PatientConnect__PC_Association__c> mapFaxAssociations = new Map<Id, PatientConnect__PC_Association__c>();
    Map<Id, PatientConnect__PC_Association__c> mapCaseAssociations = new Map<Id, PatientConnect__PC_Association__c>();

    for (PatientConnect__PC_Association__c oAssoc : (List<PatientConnect__PC_Association__c>)Trigger.new) {

      if (oAssoc.PatientConnect__PC_Role__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO) &&
          oAssoc.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {
        lstActiveSOCAssociations.add(oAssoc);
      }
      if (null != oAssoc.spc_Address__c && String.isBlank(oAssoc.spc_Fax__c)) {
        mapCaseAssociations.put(oAssoc.PatientConnect__PC_Program__c, oAssoc);
      }
    }

    //**********************************Association FAX Update***************/////////////////
    if (!mapCaseAssociations.isEmpty()) {
      List<Case> listCases = [SELECT Id, RecordTypeId from Case where Id IN : mapCaseAssociations.keySet()];
      for (Case cs : listCases) {
        if (mapCaseAssociations.get(cs.Id) != null &&
            (cs.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType) ||
             cs.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_REFERRAL, Case.SobjectType) ||
             cs.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_CERTIFICATION_LABEL, Case.SobjectType)
            )) {
          mapFaxAssociations.put(mapCaseAssociations.get(cs.Id).spc_Address__c, mapCaseAssociations.get(cs.Id));
        }
      }
    }

    if (!mapFaxAssociations.isEmpty()) {
      List<PatientConnect__PC_Address__c> lstAddreses = [select id, spc_Fax__c from PatientConnect__PC_Address__c
          where id in : mapFaxAssociations.keySet()];

      for (PatientConnect__PC_Address__c addr : lstAddreses) {
        if (null != mapFaxAssociations.get(addr.Id) && !String.isBlank(addr.spc_Fax__c)
            && mapFaxAssociations.get(addr.Id).spc_Fax__c == null) {
          mapFaxAssociations.get(addr.Id).spc_Fax__c = addr.spc_Fax__c;
        }
      }
    }


    checkRoleWithStatus();

    if (! lstActiveSOCAssociations.isEmpty()) {
      assImp.validateAndUpdateInteractionsWithSOC(lstActiveSOCAssociations);
    }
  }
  public override void beforeUpdate() {
    List<PatientConnect__PC_Association__c> lstActiveSOCAssociations = new List<PatientConnect__PC_Association__c>();
    List<PatientConnect__PC_Association__c> lstActiveAssociations = new List<PatientConnect__PC_Association__c>();
    Map<Id, PatientConnect__PC_Association__c> mapFaxAssociations = new Map<Id, PatientConnect__PC_Association__c>();
    Map<Id, PatientConnect__PC_Association__c> mapCaseAssociations = new Map<Id, PatientConnect__PC_Association__c>();
    for (PatientConnect__PC_Association__c oAssoc : (List<PatientConnect__PC_Association__c>)Trigger.new) {
      PatientConnect__PC_Association__c oldAssoc = (PatientConnect__PC_Association__c)trigger.oldMap.get(oAssoc.Id);

      if (oAssoc.PatientConnect__PC_Role__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO) &&
          oAssoc.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE) &&
          oldAssoc.PatientConnect__PC_AssociationStatus__c != spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {
        lstActiveSOCAssociations.add(oAssoc);
      }
      if (null != oAssoc.spc_Address__c && String.isBlank(oAssoc.spc_Fax__c)) {
        mapCaseAssociations.put(oAssoc.PatientConnect__PC_Program__c, oAssoc);
      }
      if (oAssoc.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE) &&
          oldAssoc.PatientConnect__PC_AssociationStatus__c != spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {
        lstActiveAssociations.add(oAssoc);
      }

      if (!mapCaseAssociations.isEmpty()) {
        List<Case> listCases = [SELECT Id, RecordTypeId from Case where Id IN : mapCaseAssociations.keySet()];
        for (Case cs : listCases) {
          if (mapCaseAssociations.get(cs.Id) != null &&
              (cs.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType) ||
               cs.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_REFERRAL, Case.SobjectType) ||
               cs.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_CERTIFICATION_LABEL, Case.SobjectType)
              )) {
            mapFaxAssociations.put(mapCaseAssociations.get(cs.Id).spc_Address__c, mapCaseAssociations.get(cs.Id));
          }
        }
      }
    }
    if (!mapFaxAssociations.isEmpty()) {
      List<PatientConnect__PC_Address__c> lstAddreses = [select id, spc_Fax__c from PatientConnect__PC_Address__c
          where id in : mapFaxAssociations.keySet()];

      for (PatientConnect__PC_Address__c addr : lstAddreses) {
        if (null != mapFaxAssociations.get(addr.Id) && !String.isBlank(addr.spc_Fax__c)
            && mapFaxAssociations.get(addr.Id).spc_Fax__c == null) {
          mapFaxAssociations.get(addr.Id).spc_Fax__c = addr.spc_Fax__c;
        }
      }
    }
    checkRoleWithStatus();

    if (! lstActiveSOCAssociations.isEmpty()) {
      assImp.validateAndUpdateInteractionsWithSOC(lstActiveSOCAssociations);
    }

    if (! lstActiveAssociations.isEmpty()) {
      assImp.validateActiveAssociations(lstActiveAssociations);
    }
  }
  public override void afterUpdate() {
    Map<Id, Id> accountTocaseMap = new Map<Id, Id>();
    List<PatientConnect__PC_Association__c> lstInactiveSOCAssociations = new List<PatientConnect__PC_Association__c>();

    for (PatientConnect__PC_Association__c oAssoc : (List<PatientConnect__PC_Association__c>)Trigger.new) {
      PatientConnect__PC_Association__c oldAssoc =  (PatientConnect__PC_Association__c)Trigger.oldMap.get(oAssoc.Id);

      if (oAssoc.PatientConnect__PC_Role__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO)
          && oldAssoc.PatientConnect__PC_AssociationStatus__c != oAssoc.PatientConnect__PC_AssociationStatus__c
          && oAssoc.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {
        accountTocaseMap.put(oAssoc.PatientConnect__PC_Account__c, oAssoc.PatientConnect__PC_Program__c);
      }

      if (oAssoc.PatientConnect__PC_Role__c == spc_ApexConstants.ASSOCIATION_ROLE_HCO &&
          oAssoc.PatientConnect__PC_AssociationStatus__c != spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE) &&
          oldAssoc.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {
        lstInactiveSOCAssociations.add(oAssoc);
      }
    }
    if (!accountTocaseMap.isEmpty()) {
      assImp.createHCPLogisticTasks(accountTocaseMap);
    }
    if (! lstInactiveSOCAssociations.isEmpty()) {
      assImp.updateInteractionsWithInactiveSOC(lstInactiveSOCAssociations);
    }
  }

  public override void afterDelete() {
    assImp.updateInteractionsWithInactiveSOC((List<PatientConnect__PC_Association__c>)trigger.old);
  }

  public void checkRoleWithStatus() {
    Boolean fieldChange = false;
    Map<Id, PatientConnect__PC_Association__c> mapAssociation = new Map<ID, PatientConnect__PC_Association__c>();
    for (PatientConnect__PC_Association__c newAssoc : (List<PatientConnect__PC_Association__c>)Trigger.new) {
      if (Trigger.old != null) {
        PatientConnect__PC_Association__c oldAssn =  (PatientConnect__PC_Association__c)Trigger.oldMap.get(newAssoc.Id);
        if (newAssoc.PatientConnect__PC_EndDate__c != oldAssn.PatientConnect__PC_EndDate__c) {
          fieldChange = true;
        }
      } else {
        fieldChange = true;
      }
      if (newAssoc.PatientConnect__PC_Account__c != NULL
          && newAssoc.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {
        mapAssociation.put(newAssoc.PatientConnect__PC_Program__c, newAssoc);
      }
    }
    if (! mapAssociation.Values().isEmpty()) {
      spc_AssociationTriggerImplementation.validateAssociationRoleAndStatus(mapAssociation, fieldChange);
    }
  }
}
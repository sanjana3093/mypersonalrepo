/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the trigger Implementation class for Account Trigger 
*  @date            09/18/2019
*  @version         1.0
Modification Log:
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Deloitte            September 25, 2019     Initial Version
Divya,Eduvulapati	November 13, 2019	   PSP-851,Updated the code so that edit right for account  would refer "Source System" field
Deloitte			November 29,2019	   Bypassed the check conditions on Account for System Admin Profile
****************************************************************************************************************/
public With Sharing class PSP_AccountTrggrImpl {
    /* String for User Locale */
    final static String USER_LOCALE = UserInfo.getLocale();
    /* String for filter Local */
    final static String FILTER_LOCAL = '%'+USER_LOCALE.split('_')[1]+'%';
    /* String for identifier */
    final static String IDENTIFIER = USER_LOCALE.split('_')[1];
    /* Map for account edit rights metadata */
    final static Map<String,PSP_Account_Edit_Rights__mdt> MD_MAP = new Map<String,PSP_Account_Edit_Rights__mdt>();
    /* flag to check if ProgramCatalogue */
    boolean isProgCatlgue=false;
    /* flag to check if Operational Support Health Cloud */
    boolean isOpSuppHC=false;
    /* flag to check if Patient Engagement Partner */
    boolean isPtntEngPrtnr=false;
    /* Static block for saving Account Edit Rights Metadata */
    static {
        for(PSP_Account_Edit_Rights__mdt mtdata: [select PSP_Access__c,MasterLabel, PSP_Account_Type__c,PSP_Locale__c from PSP_Account_Edit_Rights__mdt where MasterLabel like :FILTER_LOCAL]) {
            MD_MAP.put(mtdata.MasterLabel,mtdata);
        }
    }
    /**************************************************************************************
    * @author      : Saurabh Tripathi
    * @date        : 09/25/2019
    * @Description : sets values required for validation
    * @Param       : NA
    * @Return      : Void
    ***************************************************************************************/
    private void setlocaleValues() {
        if (PermissionSetAssignment.sObjectType.getDescribe().isAccessible()) {  
            for (PermissionSetAssignment permSet: [SELECT Id, PermissionSet.Name,AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :Userinfo.getUserId() ]) {
                System.debug('Userinfo.getUserId()::'+permSet.PermissionSet.Name+':::'+PSP_ApexConstants.PicklistValue.PROG_CAT_PERMSN_SET_NAME);
                if(String.isNotBlank(permSet.PermissionSet.Name) && permSet.PermissionSet.Name.equals(PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.PROG_CAT_PERMSN_SET_NAME))) {
                    isProgCatlgue=true;
                }
                if(String.isNotBlank(permSet.PermissionSet.Name) && permSet.PermissionSet.Name.equals(PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.OP_SUPP_FOR_HEALTH_CLOUD_PER_SET_NAM))) {
                    isOpSuppHC=true;
                }
                if(String.isNotBlank(permSet.PermissionSet.Name) && permSet.PermissionSet.Name.equals(PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.PATIENT_ENG_PARTNR_PERM_SET))) {
                    isPtntEngPrtnr=true;
                }
            }
        }
    }
    /**************************************************************************************
    * @author      : Saurabh Tripathi
    * @date        : 09/25/2019
    * @Description : checks various scenarious to fail account insertion or updation
    * @Param       : NA
    * @Return      : Void
    ***************************************************************************************/
    public void validateBeforeInsertUpdate(Map<Id,Account> oldAccmap, Map<Id,Account> newAccMap, List<Account> newAccList) {
        if(Profile.sObjectType.getDescribe().isAccessible()) {
        	final List<Profile> profile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        	setlocaleValues();
        	for(Account acc: newAccList) {
            if(!profile.isEmpty() && !profile[0].Name.equals(PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.PROFILE_NAME_ADMIN))) {
            checkPatient(acc,oldAccmap,newAccMap);
            if(MD_MAP.containsKey(IDENTIFIER+' '+acc.PSP_Account_Type__c)) {
            	final PSP_Account_Edit_Rights__mdt  mtd = MD_MAP.get(IDENTIFIER+' '+acc.PSP_Account_Type__c);                
                	checkHospitalOrClinicPO(acc,mtd);
                	checkPharmacy(acc,mtd);
                	checkOther(acc,mtd);
                	checkHousehold(acc,mtd);
                	checkTestingLab(acc,mtd);
                	checkDistributor(acc,mtd);
                	checkProdManuf(acc,mtd);
                
            	}
            }
        }
                
    }
    }
    /**************************************************************************************
    * @author      : Saurabh Tripathi
    * @date        : 09/25/2019
    * @Description : Restricts mexico user to insert or update patient records
    * @Param       : NA
    * @Return      : Void
    ***************************************************************************************/
    private  void  checkPatient (Account acc,Map<Id,Account> oldAccmap, Map<Id,Account> newAccMap) {
        
        if(USER_LOCALE == PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.LOCALE_MX)  && acc.RecordTypeId == PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.ACCOUNT_RT_PATIENT, Account.getSObjectType())) {
            if(oldAccmap==null&& (String.isNotBlank(acc.ShippingCity) || String.isNotBlank(acc.ShippingCountry) || String.isNotBlank(acc.ShippingPostalCode) || 
                                   String.isNotBlank(acc.ShippingState) || String.isNotBlank(acc.ShippingStreet))) {
                acc.addError(System.Label.PSP_Account_Edit_Error+' '+PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_PATNT));
            } else if(oldAccmap!=null && newAccmap!=null && (oldAccmap.get(acc.id).ShippingCity != newAccmap.get(acc.id).ShippingCity ||
                                                             oldAccmap.get(acc.id).ShippingCountry != newAccmap.get(acc.id).ShippingCountry ||
                                                             oldAccmap.get(acc.id).ShippingPostalCode != newAccmap.get(acc.id).ShippingPostalCode || 
                                                             oldAccmap.get(acc.id).ShippingState != newAccmap.get(acc.id).ShippingState ||
                                                             oldAccmap.get(acc.id).ShippingStreet != newAccmap.get(acc.id).ShippingStreet)) {
                                                                 acc.addError(System.Label.PSP_Account_Edit_Error+' '+PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_PATNT));
                                                             }
        }
    }
    
    /**************************************************************************************
    * @author      : Saurabh Tripathi
    * @date        : 09/25/2019
    * @Description : Restricts mexico user to insert or update Hospital,clinic or physician office records
    * @Param       : NA
    * @Return      : Void
    ***************************************************************************************/
    private void checkHospitalOrClinicPO(Account acc,PSP_Account_Edit_Rights__mdt mtd) {
        if((acc.PSP_Account_Type__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_HOSPTAL) 
             || acc.PSP_Account_Type__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_PHYSOFCE)  
             || acc.PSP_Account_Type__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_CLINIC)) && mtd.PSP_Locale__c == PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.LOCALE_MX)
          	 && mtd.PSP_Access__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_NOACCSS)) {
            	acc.addError(System.Label.PSP_Account_Edit_Error+' '+acc.PSP_Account_Type__c);
            }        
    }
    /**************************************************************************************
    * @author      : Saurabh Tripathi
    * @date        : 09/25/2019
    * @Description : Restricts mexico user to insert or update pharmacy account records
    * @Param       : NA
    * @Return      : Void
    ***************************************************************************************/
    private void checkPharmacy(Account acc,PSP_Account_Edit_Rights__mdt mtd) {
        if(acc.PSP_Account_Type__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_PHRM) 
           && mtd.PSP_Locale__c ==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.LOCALE_MX)
           && mtd.PSP_Access__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_NOACCSS)) {
               if(acc.HealthCloudGA__SourceSystem__c ==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_SOURCE)) {
                   acc.addError(System.Label.PSP_Account_Edit_Error+' '+acc.PSP_Account_Type__c);
               } else if (!isProgCatlgue) {
                   acc.addError(System.Label.PSP_Account_Edit_Error+' '+acc.PSP_Account_Type__c);
               }
        }
    }
    /**************************************************************************************
    * @author      : Saurabh Tripathi
    * @date        : 09/25/2019
    * @Description : Restricts mexico user to insert or update other account records
    * @Param       : NA
    * @Return      : Void
    ***************************************************************************************/
    private void checkOther(Account acc,PSP_Account_Edit_Rights__mdt mtd) {
        if(acc.PSP_Account_Type__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_OTHR) 
           && mtd.PSP_Locale__c == PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.LOCALE_MX)
           && mtd.PSP_Access__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_NOACCSS) && !isProgCatlgue && !isOpSuppHC) {
        	acc.addError(System.Label.PSP_Account_Edit_Error+' '+acc.PSP_Account_Type__c);
        }
    }
    /**************************************************************************************
    * @author      : Saurabh Tripathi
    * @date        : 09/25/2019
    * @Description : Restricts mexico user to insert or update household records
    * @Param       : NA
    * @Return      : Void
    ***************************************************************************************/
    private void checkHousehold(Account acc,PSP_Account_Edit_Rights__mdt mtd) {
        if(acc.PSP_Account_Type__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_HOUSHLD) 
           && mtd.PSP_Locale__c == PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.LOCALE_MX)
           && mtd.PSP_Access__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_NOACCSS) && !isPtntEngPrtnr && !isOpSuppHC) {
                acc.addError(System.Label.PSP_Account_Edit_Error+' '+acc.PSP_Account_Type__c);
        }
    }
    /**************************************************************************************
    * @author      : Saurabh Tripathi
    * @date        : 09/25/2019
    * @Description : Restricts mexico user to insert or update testing laboratory account records
    * @Param       : NA
    * @Return      : Void
    ***************************************************************************************/
    private void checkTestingLab(Account acc,PSP_Account_Edit_Rights__mdt mtd) {
        if(acc.PSP_Account_Type__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_TESTLAB) 
           && mtd.PSP_Locale__c == PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.LOCALE_MX)
           && mtd.PSP_Access__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_NOACCSS) && !isOpSuppHC) {
        	acc.addError(System.Label.PSP_Account_Edit_Error+' '+acc.PSP_Account_Type__c);
        }
    }
    /**************************************************************************************
    * @author      : Saurabh Tripathi
    * @date        : 09/25/2019
    * @Description : Restricts mexico user to insert or update distributor account records
    * @Param       : NA
    * @Return      : Void
    ***************************************************************************************/
    private void checkDistributor(Account acc,PSP_Account_Edit_Rights__mdt mtd) {
        if(acc.PSP_Account_Type__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_DSTRIBTR) 
           && mtd.PSP_Locale__c == PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.LOCALE_MX)
           && mtd.PSP_Access__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_NOACCSS) && !isProgCatlgue) {
        	acc.addError(System.Label.PSP_Account_Edit_Error+' '+acc.PSP_Account_Type__c);
        }
    }
    /****************************************************************************************************
         * @author      : Saurabh Tripathi
         * @date        : 09/25/2019
         * @Description : Restricts mexico user to insert or update product manufacturer account records
         * @Param       : NA
         * @Return      : Void
     ****************************************************************************************************/
    private void checkProdManuf(Account acc,PSP_Account_Edit_Rights__mdt mtd) {
        if(acc.PSP_Account_Type__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_TYP_PRODMANUF) 
           && mtd.PSP_Locale__c == PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.LOCALE_MX)
           && mtd.PSP_Access__c==PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.ACC_NOACCSS) && !isProgCatlgue) {
        	acc.addError(System.Label.PSP_Account_Edit_Error+' '+acc.PSP_Account_Type__c);
        }
    }
    
}
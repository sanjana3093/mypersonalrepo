/********************************************************************************************************
*  @author          Deloitte
*  @description     This class is used for handling the notification functionality within the application
*  @date            July 14, 2020
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Deloitte                  July 27 2020         Initial Version
****************************************************************************************************************/
public without sharing class CCL_Notification_Handler {


/*
    *   @author           Deloitte
    *   @description      Method populate the Notification DTO with all the required information.
    *                     Here, we have a map of Notification DTO record per UniqueKey where
    *                     Unique Key= 'Current record ID~Setting MetadataRecord.DeveloperName +
    *                     content MetadataRecord.DeveloperName'.
    *                     Below are the DTO Values which are being populated here:
    *                     RecordID, NotificationType, therapyType, objectAPIName, NotificationSettingMetadataRecord,
    *                     NotificationContentMetadataRecord,EmailTemplateRecord.
    *   @para             list<CCL_Notification_Invocable.CCL_Notification_Invocable_Wrapper>
    *   @return           Map<String,CCL_Notification_DTO>
    *   @date             28 July 2020
*/
    public static Map<String,CCL_Notification_DTO> buildNotificationDTO(list<CCL_Notification_Invocable.CCL_Notification_Invocable_Wrapper> notificationWrapperList) {

         Map<String,CCL_Notification_DTO> tempMapOfNotificationDTOPerUniqueKey=new Map<String,CCL_Notification_DTO>();
         Map<String,CCL_Notification_DTO> notificationDTOPerUniqueKey=new Map<String,CCL_Notification_DTO>();
         Map<String,CCL_Notification_Settings__mdt> notificationSettingPerUniqueKeyOfNameAndCategory = new Map<String,CCL_Notification_Settings__mdt>();
         Map<String,CCL_Notification_Content__mdt> notificationContentPerUniqueKeyOfSettingNameAndCategory = new Map<String,CCL_Notification_Content__mdt>();
         Map<String, EmailTemplate> mapOfEmailTemplatePerNotificationType = new Map<String,EmailTemplate>();
         Map<String, EmailTemplate> mapOfEmailTemplatePerTemplateDeveloperName = new Map<String,EmailTemplate>();
         Map<Id,CCL_Notification_Settings__mdt> notificationSettingByID = new Map<Id,CCL_Notification_Settings__mdt>();
         Map<Id,CCL_Notification_Content__mdt> notificationContentByID = new Map<Id,CCL_Notification_Content__mdt>();

        String uniqueKeyForWrapper;
        if(notificationWrapperList!=null && notificationWrapperList.size()>0) {
            mapOfEmailTemplatePerTemplateDeveloperName = CCL_Notification_Accessor.fetchAllEmailTemplate();
            notificationSettingByID= CCL_Notification_Accessor.fetchAllNotificationSettings();
            notificationContentByID= CCL_Notification_Accessor.fetchAllNotificationContent();

            for(CCL_Notification_Invocable.CCL_Notification_Invocable_Wrapper notificationWrapper: notificationWrapperList) {
                uniqueKeyForWrapper = CCL_Notifications_Constants.BLANK_VALUE;
                uniqueKeyForWrapper = notificationWrapper.recordId + '~' + notificationWrapper.NotificationType + notificationWrapper.Category;
                String metadataKey = uniqueKeyForWrapper.split('~')[1];
                for(CCL_Notification_Content__mdt notificationContent : notificationContentByID.values() ) {
                    CCL_Notification_Settings__mdt notificationSettingRecord = notificationSettingByID.get(notificationContent.CCL_Notification_Settings__c);
                        if(notificationSettingRecord!=null) {
                            String uniqueKeyForMetadataRecords = notificationSettingRecord.CCL_Notification_Type__c + notificationSettingRecord.CCL_Category__c;
                            notificationSettingPerUniqueKeyOfNameAndCategory.put(uniqueKeyForMetadataRecords,notificationSettingRecord);
                            if(uniqueKeyForMetadataRecords == metadataKey) {
                                CCL_Notification_DTO notificationDToInstance = new  CCL_Notification_DTO();//tempMapOfNotificationDTOPerUniqueKey.get(Key);
                                System.debug('::CCL_Notification_Handler ::buildNotificationDTO ::notificationDToInstance ' + JSON.serialize(notificationDToInstance) );
                                notificationDToInstance.recordId = notificationWrapper.recordId;
                                notificationDToInstance.NotificationType = notificationSettingRecord.CCL_Notification_Type__c;
                                notificationDToInstance.Category = notificationSettingRecord.CCL_Category__c;
                                notificationDToInstance.NotificationSettingMetadataRecord = notificationSettingRecord;
                                notificationDToInstance.NotificationContentMetadataRecord = notificationContent;
                                notificationDToInstance.objectAPIName = notificationWrapper.objectAPIName;
                                EmailTemplate templateRecord = mapOfEmailTemplatePerTemplateDeveloperName.get(notificationContent.CCL_Email_Template__c); 
                                notificationDToInstance.EmailTemplateRecord = templateRecord;
                                String uniqueKeyForMap = notificationWrapper.recordId+'~'+ notificationSettingRecord.DeveloperName + notificationContent.DeveloperName;
                                notificationDToInstance.uniqueKeyForDTOInstance = uniqueKeyForMap;
                                notificationDTOPerUniqueKey.put(uniqueKeyForMap,notificationDToInstance);
                                System.debug('::CCL_Notification_Handler ::buildNotificationDTO ::notificationDTOPerUniqueKey ' + JSON.serialize(notificationDTOPerUniqueKey) );
                        }
                    }
                }
            }
        }
        System.debug('::CCL_Notification_Handler ::buildNotificationDTO ::notificationDTOPerUniqueKey OutsideLoop ' + JSON.serialize(notificationDTOPerUniqueKey) );
        return notificationDTOPerUniqueKey;
    }
/*
 *   @author           Deloitte
 *   @description      Fetches all the records associated to the record Ids coming from Notification DTO List
 *   @para             Map<String,CCL_Notification_DTO> , SObject
 *   @return           List<sObject>
 *   @date             31 July 2020
 */
        public static List<sObject> getCurrentRecordDetails(Map<String,CCL_Notification_DTO> notificationDTOPerUniqueKey) {
            List<sObject> currentSObjectRecords= new List<sObject>();
            List<Id> currentRecordIds= new list<Id>();
            String objectName;
            String dynamicQuery;
            if(!notificationDTOPerUniqueKey.isEmpty()) {
                for(CCL_Notification_DTO dtoInstance : notificationDTOPerUniqueKey.values()) {
                    currentRecordIds.add(dtoInstance.recordId);
                    objectName=dtoInstance.objectAPIName; // object API Name is going to remain same for all DTO Instance
                }
                dynamicQuery=CCL_Notification_Helper.generateQueryFromFieldSet(objectName,CCL_Notifications_Constants.FIELD_SET_NAME);
                dynamicQuery=String.escapeSingleQuotes(dynamicQuery) + CCL_Notifications_Constants.WHERE_ID_IN_CURRENTRECORDIDS_BIND;
                currentSObjectRecords=Database.query(String.escapeSingleQuotes(dynamicQuery));
                System.debug('::CCL_Notification_Handler ::getCurrentRecordDetails ::currentSObjectRecords ' + JSON.serialize(currentSObjectRecords) );
            }
            return currentSObjectRecords;
        }

/*
 *   @author           Deloitte
 *   @description      Fetches the email content to be processed for the email Body only 
 *                      and calls another method
 *   @para             Map<String,CCL_Notification_DTO> , SObject
 *   @return           Map<String,String>
 *   @date             31 July 2020
 */
    public static Map<String,CCL_Notification_DTO> fetchEmailContent (Map<String,CCL_Notification_DTO>  mapDTOInstance) {

         Map<String,CCL_Notification_DTO> mapEmailContentAddedToDTO = new Map<String,CCL_Notification_DTO>();
         Pattern regexPattern = Pattern.compile(CCL_Notifications_Constants.REGEX_PATTERN_CAPTURE_MERGE_FIELDS);
        for (CCL_Notification_DTO temptValue : mapDTOInstance.values()) {
            System.debug('temptValue####'+temptValue);
            temptValue.emailBody = CCL_Notification_Helper.buildEmailContent(temptValue.EmailTemplateRecord.HtmlValue,temptValue.genericSObject, regexPattern);
             temptValue.emailBody=temptValue.emailBody.substringAfter(CCL_Notifications_Constants.CDATA_TAG_HTML_VALUE);
            temptValue.emailBody=temptValue.emailBody.substringBefore(CCL_Notifications_Constants.CLOSING_HTML_TAG_VALUE);
            temptValue.emailSubject = CCL_Notification_Helper.buildEmailContent(temptValue.EmailTemplateRecord.Subject,temptValue.genericSObject, regexPattern ); 

            mapEmailContentAddedToDTO.put(temptValue.uniqueKeyForDTOInstance, temptValue);
        }
        return mapEmailContentAddedToDTO;
    }

}
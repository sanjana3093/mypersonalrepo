/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the test class for CareProgram Enroll card Trigger Handler
*  @date            07/15/2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Pratik raj		          July 15, 2019          Initial Version
****************************************************************************************************************/
@IsTest
public class PSP_CareProgramEnrollcardTest {
    /*Constant for Card Record Type Id */
    public static final Id CARD_RT_ID = Schema.SObjectType.CareProgramEnrollmentCard.getRecordTypeInfosByName().get('Card').getRecordTypeId();
    /*Constant for Coupon Record Type Id */
    public static final Id COUPON_RT_ID = Schema.SObjectType.CareProgramEnrollmentCard.getRecordTypeInfosByName().get('Coupon').getRecordTypeId();
    /*Constant for test */
    public static final String TEST = 'Test';
	/*Constant for test1 */
    public static final String TEST1 = 'Test1';    
    /*Constant for Active */
    public static final String ACTIVE = 'Active';  
    /*Constant for Card_Number */
    public static final String CARD_NUMBER = '123456';  
    /*Constant for SALES_REP */
    public static final String SALES_REP = 'Sales Rep';
    /*Constant for PHYSICAL */
    public static final String PHYSICAL = 'Physical';
    /*Apex Constants */
    static final List<PSP_ApexConstantsSetting__c>  APEX_CONSTANTS =  PSP_Test_Setup.setDataforApexConstants();  
    /**************************************************************************************
	* @author      : Deloitte
	* @date        : 07/12/2019
	* @Description : Testing insert scenarios
	* @Param       : Void
	* @Return      : Void
	***************************************************************************************/
    @IsTest static void testInsertScenarios () {
        /*Test setup method being instantiated */
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram (TEST,null);
        Database.insert(cP1);
        /*Test setup method being instantiated */
        final CareProgramEnrollee cPE2 = PSP_Test_Setup.createTestCareProgramEnrollment (TEST,CP1.Id);
        Database.insert(cPE2);
        /*Test setup method being instantiated */
        final CareProgramEnrollmentCard cPEC21 = PSP_Test_Setup.createTestCareProgramEnrollCard (TEST1,ACTIVE,CARD_NUMBER,cPE2.Id,SALES_REP,PHYSICAL,cP1.Id);
        Database.insert (cPEC21);
        /*Test setup method being instantiated */
        final CareProgramEnrollmentCard cPEC22 = PSP_Test_Setup.createTestCareProgramEnrollCard (TEST1,ACTIVE,CARD_NUMBER,cPE2.Id,SALES_REP,PHYSICAL,cP1.Id);
        try {
            Database.insert (cPEC22);
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains ('Insert failed.'),e.getMessage () );
        }
    }
     /**************************************************************************************
	* @author      : Deloitte
	* @date        : 07/12/2019
	* @Description : Testing update scenarios for Card
	* @Param       : Void
	* @Return      : Void
	***************************************************************************************/
    @IsTest static void testUpdateScenariosForCard () {
        /*Test setup method being instantiated */
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram (TEST,null);
        Database.insert (cP1);
        /*Test setup method being instantiated */
        final CareProgramEnrollee cPE2 = PSP_Test_Setup.createTestCareProgramEnrollment (TEST,CP1.Id);
        Database.insert (cPE2);
        /*Test setup method being instantiated */
        final CareProgramEnrollmentCard cPEC21 = PSP_Test_Setup.createTestCareProgramEnrollCard (TEST1,ACTIVE,CARD_NUMBER,cPE2.Id,SALES_REP,PHYSICAL,cP1.Id);
        cPEC21.recordtypeid = CARD_RT_ID;
        Database.insert (cPEC21);
        /*Test setup method being instantiated*/
        final CareProgramEnrollmentCard cPEC22 = PSP_Test_Setup.createTestCareProgramEnrollCard (TEST1,ACTIVE,'1234567',cPE2.Id,SALES_REP,PHYSICAL,cP1.Id);
        try {
            Database.insert (cPEC22);
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains ('Insert failed.'),e.getMessage () );
        }
        
    }
    /**************************************************************************************
	* @author      : Deloitte
	* @date        : 07/12/2019
	* @Description : Testing update scenarios for Coupon
	* @Param       : Void
	* @Return      : Void
	***************************************************************************************/
    @IsTest static void testUpdateScenariosForCoupon () {
        /*Test setup method being instantiated*/
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram (TEST,null);
        Database.insert(cP1);
        /*Test setup method being instantiated*/
        final CareProgramEnrollee cPE2 = PSP_Test_Setup.createTestCareProgramEnrollment (TEST,CP1.Id);
        Database.insert (cPE2);
        /*Test setup method being instantiated*/
        final CareProgramEnrollmentCard cPEC21 = PSP_Test_Setup.createTestCareProgramEnrollCard (TEST1,ACTIVE,CARD_NUMBER,cPE2.Id,SALES_REP,PHYSICAL,cP1.Id);
        /*Test setup method being instantiated*/
        final CareProgramEnrollmentCard cPEC22 = PSP_Test_Setup.createTestCareProgramEnrollCard (TEST1,ACTIVE,'1234567',cPE2.Id,SALES_REP,PHYSICAL,cP1.Id);
        cPEC21.recordtypeid = COUPON_RT_ID;
        Database.insert(cPEC21);
        Database.update (cPEC21);
        cPEC22.CardNumber = CARD_NUMBER;
        try {
            Database.update (cPEC22);
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains ('Update failed.'),e.getMessage () );
        }
    }
}
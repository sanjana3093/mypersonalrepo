/********************************************************************************************************
*  @author          Deloitte
*  @description     Controller class to handle the backend logic for CCL_Privacy_Disclaimer_Login Page
*  @param           
*  @date            June 19, 2020
*********************************************************************************************************/
public without sharing class CCL_Privacy_Disclaimer_Controller {

    public user loggedInUserRec{get; set;}
    public Boolean isDisclaimerPolicyAccepted {get; set;}
    public Boolean isDiscalimerAlreadyAcceptedByUser {get; set;}
    private Contact loggedInUserContactRec;
    
    
    public CCL_Privacy_Disclaimer_Controller(){
        loggedInUserRec= new User();
        loggedInUserRec = CCL_UserAccessor.getLoggedInUserInfo(UserInfo.getuserid());
        if(loggedInUserRec != null && loggedInUserRec.ContactId != null){
            loggedInUserContactRec = new Contact();
            loggedInUserContactRec.Id = loggedInUserRec.ContactId;
            loggedInUserContactRec.CCL_Has_User_Accepted_Privacy_Disclaimer__c = loggedInUserRec.Contact.CCL_Has_User_Accepted_Privacy_Disclaimer__c;
            isDisclaimerPolicyAccepted=false;
            isDiscalimerAlreadyAcceptedByUser=false;
            getLoggedInUseContactrRec();
        }
    }

    /********************************************************************************************************
*  @author          Deloitte
*  @description     Method to handle logic when a user accept's privacy policy and redirect it to home page. 
*  @param           
*  @return          Pagereference
*  @date            June 1, 2020
*********************************************************************************************************/
    public pagereference redirectToHomePage(){
        if(loggedInUserRec != null && loggedInUserContactRec != null && !loggedInUserContactRec.CCL_Has_User_Accepted_Privacy_Disclaimer__c){
            updateUserContactDisclaimerInfo(loggedInUserRec.Id);
        }
        String redirectURl = System.label.CCL_PrivacyDisclaimerRedirectURL;
        final String retUrl = System.currentPageReference().getParameters().get('retURL');
        if(String.isNotBlank(retUrl)) {
            redirectURl = retUrl;
        }
        return Auth.SessionManagement.finishLoginFlow(redirectURl);
    }

/********************************************************************************************************
*  @author          Deloitte
*  @description     Method to update user disclaimer flag when a user agree on the Privacy Policy.
*  @param           
*  @return          void
*  @date            June 1, 2020
*********************************************************************************************************/
    public void getLoggedInUseContactrRec(){
        if(loggedInUserContactRec != null && loggedInUserContactRec.CCL_Has_User_Accepted_Privacy_Disclaimer__c){
            isDiscalimerAlreadyAcceptedByUser=loggedInUserContactRec.CCL_Has_User_Accepted_Privacy_Disclaimer__c;
            isDisclaimerPolicyAccepted=loggedInUserContactRec.CCL_Has_User_Accepted_Privacy_Disclaimer__c;
            redirectToHomePage();
        }
    }

/********************************************************************************************************
*  @author          Deloitte
*  @description     Method to update user disclaimer flag when a user agree on the Privacy Policy.
*  @param           
*  @return          void
*  @date            June 1, 2020
*********************************************************************************************************/
    @future(callout=false)
    public static void updateUserContactDisclaimerInfo(String loggedInUserId){
        User loggedInUserRec = CCL_UserAccessor.getLoggedInUserInfo(loggedInUserId);
        Contact loggedInUserContactRec = new Contact();
        loggedInUserContactRec.Id = loggedInUserRec.ContactId;
        loggedInUserContactRec.CCL_Has_User_Accepted_Privacy_Disclaimer__c = true;
        if(loggedInUserRec != null){
            try{
                if(Schema.sObjectType.Contact.isUpdateable()){
                     update loggedInUserContactRec;
                }
                
            }catch(Exception ex){
                System.debug('Exception ex **** '+ex);
            }    
        }
    }
    
    
}
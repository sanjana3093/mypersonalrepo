/********************************************************************************************************
*  @author          Deloitte
*  @description     Accessor class for CCL_ContentDocumentLink_Handler.cls
*  @param           
*  @date            Jan 11 2021
*********************************************************************************************************/
public with sharing class CCL_ContentDocumentLink_Accessor {

    private CCL_ContentDocumentLink_Accessor() {}

    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Method to parent Shipment records of documents created by CT Integration User
    *  @param           
    *  @date            Jan 11 2021
    *********************************************************************************************************/
    public static List<CCL_Shipment__c> fetchParentShipmentsOfDocumentsCreatedByCTIntegrationUser(Set<Id> parentShipmentIds) {
        
        List<User> integrationUsers = new List<User>();
        List<ContentDocument> contentDocs = new List<ContentDocument>();
        List<CCL_Shipment__c> parentShipments = new List<CCL_Shipment__c>();
        Set<Id> integrationDocIds = new Set<Id>();

        if(User.sObjectType.getDescribe().isAccessible()) {
            integrationUsers = [
                SELECT Id
                FROM User
                WHERE Profile.Name = :CCL_StaticConstants_MRC.CT_INTEGRATION_PROFILE_TYPE
            ];
        }      
        
        if(!integrationUsers.isEmpty() && ContentDocument.sObjectType.getDescribe().isAccessible()) {

            final Set<Id> integrationUserIds = new Map<Id,User>(integrationUsers).keySet();

            contentDocs = [
                SELECT Id
                FROM ContentDocument
                WHERE OwnerId IN :integrationUserIds
            ];          
        }

        if(!contentDocs.isEmpty()) {
            integrationDocIds = new Map<Id,ContentDocument>(contentDocs).keySet();
        }

        if(!integrationDocIds.isEmpty() && CCL_Shipment__c.sObjectType.getDescribe().isAccessible()) { 

            parentShipments = [
                SELECT Id, (SELECT Id 
                            FROM ContentDocumentLinks 
                            WHERE ContentDocumentId IN :integrationDocIds
                            ) 
                FROM CCL_Shipment__c 
                WHERE Id IN :parentShipmentIds
            ];
        }

        return parentShipments;
    }

    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Method to fetch Finished Product records related to specified FP Shipments
    *  @param           
    *  @date            Jan 11 2021
    *********************************************************************************************************/
    public static List<CCL_Finished_Product__c> fetchFinishedProductsRelatedToShipments(Set<Id> shipmentIds) {
        
        List<CCL_Finished_Product__c> finishedProducts = new List<CCL_Finished_Product__c>();
        
        if(CCL_Finished_Product__c.sObjectType.getDescribe().isAccessible()) {
            finishedProducts = [
                SELECT Id, CCL_Shipment__c, CCL_Related_Documents_On_FP_Shipment__c
                FROM CCL_Finished_Product__c
                WHERE CCL_Shipment__c IN :shipmentIds
            ];
        }
        return finishedProducts;
    } 
}
/*********************************************************************************************************
 * This is The PC class cloned by Sage for Reference only
************************************************************************************************************/
/*********************************************************************************************************
@description Manage dependend picklist values
@author       Deloitte
@date        May 31, 2016
****************************************************************************************************************/
public with sharing class spc_PicklistFieldManager {

    // Given two SObjectField of type PickList, return the value to picklist entry list mapping
    public static Map<String, List<PicklistEntryWrapper>> picklistFieldMap(SObjectField contollingField, SObjectField dependentField) {
        Map<String, List<PicklistEntryWrapper>> objResults = new Map<String, List<PicklistEntryWrapper>>();
        //get the control values
        List<Schema.PicklistEntry> controllingEntries = contollingField.getDescribe().getPicklistValues();
        //get the dependent values
        List<Schema.PicklistEntry> dependentEntries = dependentField.getDescribe().getPicklistValues();

        //set up the results
        for (Schema.PicklistEntry controllingEntry : controllingEntries) {
            //create the entry with the value
            objResults.put(controllingEntry.getValue(), new List<PicklistEntryWrapper>());
        }

        BitsetChecker bitsetChecker = new BitsetChecker();

        //check the dependent values
        for (Schema.PicklistEntry dependantEntry : dependentEntries) {
            //get the valid for
            String entryStructure = JSON.serialize(dependantEntry);
            PicklistEntryWrapper dependentEntryWrapper = (PicklistEntryWrapper) JSON.deserialize(entryStructure, PicklistEntryWrapper.class);
            //if valid for is empty, skip
            if (dependentEntryWrapper.validFor == null || dependentEntryWrapper.validFor == '') {
                continue;
            }
            //iterate through the controlling values
            for (Integer controllingIndex = 0; controllingIndex < controllingEntries.size(); controllingIndex++) {
                bitsetChecker.setValidFor(dependentEntryWrapper.validFor);
                if (bitsetChecker.testBit(controllingIndex)) {
                    //get the label
                    String controllingValue = controllingEntries[controllingIndex].getValue();
                    objResults.get(controllingValue).add(dependentEntryWrapper);
                }
            }
        }

        return objResults;
    }


    // Returns a list of PicklistEntryWrappers for the given field
    public static List<PicklistEntryWrapper> getPicklistEntryWrappers(Schema.SObjectField field) {
        return getPicklistEntryWrappersFordescFields(field.getDescribe());
    }

    // Returns a list of PicklistEntryWrappers for the given describe results of field
    public static List<PicklistEntryWrapper> getPicklistEntryWrappersFordescFields(Schema.DescribeFieldResult descField) {
        List<PicklistEntryWrapper> picklistEntryWrappers  = new List<PicklistEntryWrapper>();
        for (Schema.PicklistEntry picklistEntry : descField.getPicklistValues()) {
            picklistEntryWrappers.add(new PicklistEntryWrapper(picklistEntry.getValue(), picklistEntry.getLabel()));
        }

        return picklistEntryWrappers;
    }

    public static Decimal hexToDecimal(String sourceHex) {
        String hex = '0123456789abcdef';
        String[] hexValue = sourceHex.split('');
        Decimal result = 0;
        for (Integer index = 0; index < hexValue.size(); index++) {
            result = (result * 16) + hex.indexOf(hexValue[index]);
        }

        return result;
    }

    /*********************************************************************************
    @description     Wrapper used for storing picklist results
    @author   Roxana Ivan
    @date     May 31, 2016
    *********************************************************************************/
    public class PicklistEntryWrapper {
        public PicklistEntryWrapper(String value, String label) {
            this.value = value;
            this.label = label;
        }

        // Somehow gets populated when deserializing a serialized Schema.PicklistEntry.
        public String validFor { get; set; }

        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }
    }

    public class BitsetChecker {
        String validFor {get; private set;}
        String vfDecoded {get; private set;}
        String[] hexBytes {get; private set;}
        Integer[] bytes {get; private set;}

        public void setValidFor(String validFor) {
            this.validFor = validFor;
            this.vfDecoded = null;
            hexBytes = new String[] {};
            bytes = new Integer[] {};
            if (String.isNotBlank(validFor)) {
                this.vfDecoded = String.isNotBlank(validFor) ?
                                 EncodingUtil.convertToHex(EncodingUtil.base64Decode(validFor)).toLowerCase() : '';
                if (String.isNotBlank(vfDecoded) && Math.mod(vfDecoded.length(), 2) == 0) {
                    for (Integer i = 0; i < vfDecoded.length(); i += 2) {
                        String hexByte = vfDecoded.substring(i, i + 2);
                        hexBytes.add(hexByte);
                        bytes.add(hexToDecimal(hexByte).intValue());
                    }
                }
            }
        }

        public Boolean testBit(Integer n) {
            Boolean result = false;
            if (n != null && n < size() && hexBytes != null) {
                Integer bytesPos = n >> 3;
                Integer targetByte = bytesPos < bytes.size() ? bytes[bytesPos] : null;
                if (targetByte != null) {
                    Integer mask = 128 >> Math.mod(n, 8);
                    Integer maskedByte = targetByte & mask;
                    result = maskedByte != 0;
                }
            }
            return result;
        }

        public Integer size() {
            return bytes.size() * 8;
        }
    }
}
@isTest
public class spc_SendBundledEmailTest {
    @testSetup static void setup() {
        
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        
    }
    /*********************************************************************************
Method Name    : testGetDocFields 
Description    : Test doc fields
Return Type    : void
Parameter      : None
*********************************************************************************/
    @isTest
    public static void testGetDocFields() {
        test.startTest();
        Account account = spc_Test_Setup.createPatient('Patient Test');
        Case enrollmentCase = spc_Test_Setup.newCase(account.id, 'PC_Enrollment');
        PatientConnect__PC_Document__c doc=spc_Test_Setup.createDocument('Email - Outbound','completed',enrollmentCase.id);
        insert doc;
        spc_SendBundledEmail.DocumentRecord docRecord1 = new spc_SendBundledEmail.DocumentRecord(doc,'True');
        test.stopTest();
    }
    /*********************************************************************************
Method Name    : testGetDocumentTypes 
Description    : Test Document types
Return Type    : void
Parameter      : None
*********************************************************************************/
    @isTest
    public static void testGetDocumentTypes() {
        Test.startTest();
        Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> pickLists = spc_SendBundledEmail.getDocumentTypes();
        System.assertEquals(True, pickLists.size() > 0);
        Test.stopTest();
        
    }
    /*********************************************************************************
Method Name    : testSearchRecords 
Description    : Test Document types
Return Type    : void
Parameter      : None
*********************************************************************************/
    @isTest
    public static void testSearchRecords() {
        Test.startTest();
        //Caregiver account
        Account caregiverAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId()); 
        insert caregiverAcc1;
        
        //physicial account
        Account phyAcc = spc_Test_Setup.createTestAccount('PC_Physician');
        phyAcc.Name = 'Acc_Name';
        spc_Database.ins(phyAcc);
        
        //Id recordTypeId = ID_PATIENT_RECORDTYPE;
        Account account = spc_Test_Setup.createPatient('Patient Test');
        Case enrollmentCase = spc_Test_Setup.newCase(account.id, 'PC_Enrollment');
        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');
        programCase.spc_Caregiver__c = caregiverAcc1.id;
        programCase.PatientConnect__PC_Physician__c = phyAcc.id;
        insert programCase;
        
        
        Account HCOAccount = spc_Test_Setup.createTestAccount('PC_Pharmacy');
        HCOAccount.Name='test last';
        
        
        insert HCOAccount;
        Map<String, Object> persistedAccountMap2 = new Map<String, Object>();
        Map<String,Object> persistentHCOAccount2 = new Map<String,Object>();
        persistentHCOAccount2.put('Id', HCOAccount.id);
        Contact con=new Contact();
        con.LastName='test';
        con.AccountId=HCOAccount.id;
        
        insert con;
        
        
        //Case
        Case objCase = spc_Test_Setup.newCase(HCOAccount.Id, 'PC_Program');
        objCase.Status = 'Enrolled';
        objCase.spc_Caregiver__c = caregiverAcc1.id;
        insert objCase;
        
        //Document
        PatientConnect__PC_Document__c doc=spc_Test_Setup.createDocument('Email - Inbound','completed',enrollmentCase.id);
        insert doc;
        List<String> DocIds = new List<String>();
        DocIds.add(string.valueOf(doc.Id));
        
        PatientConnect__PC_Document_Log__c doclog = new PatientConnect__PC_Document_Log__c(PatientConnect__PC_Document__c = doc.Id,PatientConnect__PC_Program__c = programCase.Id);
        insert doclog;
        
        Attachment attach = new Attachment();
        attach.Name = 'Test';
        attach.Body = Blob.valueOf('Test');
        attach.ParentId = doc.Id;
        insert attach;
        
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', account.ID);
        insert program;
        
        PatientConnect__PC_eLetter__c eLetter = spc_Test_Setup.createEeletter('Program', 'Fax');
        insert eLetter;
        
        PatientConnect__PC_Engagement_Program_Eletter__c engageELetter = spc_Test_Setup.createEProgramEletter(eLetter.Id, 'Fax', program.id);
        insert engageELetter;
        
        PatientConnect__PC_Association__c assoc = spc_Test_Setup.createAssociation(HCOAccount,programCase,'Specialty Pharmacy',System.today()+30);
        List<String> recipient=new List<String>();
        recipient.add('Treating Physician');
        spc_SendBundledEmail.searchRecords(programCase.Id);
        spc_SendBundledEmail.getRecepients(eLetter.Id,programCase.Id,false);
        spc_SendBundledEmail.searchRecords(String.valueOf(enrollmentCase.id));
        String recepientsstring = '[{"bDisableEmail":false,"bDisableFax":false,"bDisableSendToMe":true,"bDisableSMS":false,"communicationLanguage":"English","recipient":{"Id":"'+ HCOAccount.Id+'","PatientConnect__PC_Email__c":"English","PatientConnect__PC_Email__c":"test@a.com","Type":"Specialist","Fax":"12345","Name":"Name."},"recipientType":"Specialist","recipientEmail":"aiii@biii.com","sendEmail":true,"sendFax":false,"sendToMe":false,"sendSMS":false}]';
        spc_SendBundledEmail.sendOutboundDocuments(recepientsstring,eLetter.Id,programCase.Id);
        spc_SendBundledEmail.SendMergedRecords(String.valueOf(programCase.Id), DocIds, recipient, eLetter.Id);
        spc_SendBundledEmail.getLetters(programCase.Id, 'Case', true);	
        Test.stopTest();
        
    }    
}
/**
* @author Deloitte
* @date 10-September-2018
*
* @description Controller class for Health Plan Enrollment Wizard Page
*/

public with sharing class spc_HealthPlanEWPLightningController {
    Private Static String FIELDSET_KEY = 'fieldSet';
    /*******************************************************************************************************
    * @description Retrieves the Field Labels
    * @return Map<String, FieldWrapper>
    */
    @AuraEnabled
    public static Map<String, FieldWrapper> getFieldLabel() {
        Map<String, FieldWrapper> fDescribe = new Map<String, FieldWrapper>();
        List<String> sObjectName = new List<String>();
        sObjectName.add(spc_ApexConstants.spc_HEALTH_PLAN);
        sObjectName.add('Account');

        return fDescribe = getFieldDescribe(sObjectName);
    }

    /*******************************************************************************************************
    * @description Method to fetch sobject details
    * @param sObjectName String of sObject Type
    * @return Map<String, FieldWrapper>
    */
    public static Map<String, FieldWrapper> getFieldDescribe(List<String> sObjectName) {
        Map<String, FieldWrapper> fDescribe = new Map<String, FieldWrapper>();
        List<RecordType> sRecordTypeList = [SELECT Id, Name, SobjectType FROM RecordType WHERE sObjectType = : sObjectName];

        for (String s : sObjectName) {
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(s);
            Schema.DescribeSObjectResult describe = targetType.getDescribe();

            list<SelectOption> options = new list<SelectOption>();
            List<spc_PicklistFieldManager.PicklistEntryWrapper> lstRecordTypeOptions = new List<spc_PicklistFieldManager.PicklistEntryWrapper>();
            lstRecordTypeOptions.add(new spc_PicklistFieldManager.PicklistEntryWrapper(spc_ApexConstants.PICKLIST_NONE, System.Label.spc_Picklist_None));
            for (RecordType sRecordType : sRecordTypeList) {
                if (s == sRecordType.SobjectType) {
                    // Select options are added with key,value pairing

                    String recordTypeName = sRecordType.Name;
                    if (recordTypeName != null && recordTypeName != spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.HP_RT_PRIVATE_HEALTH_PLAN)) {
                        options.add(new SelectOption(sRecordType.Id, recordTypeName));
                        lstRecordTypeOptions.add(new spc_PicklistFieldManager.PicklistEntryWrapper(sRecordType.Name, recordTypeName));
                    }
                }
            }

            FieldWrapper fieldWrapper = new FieldWrapper();

            Map<String, SObjectField> mapFields = describe.fields.getMap();
            for (String fieldName : mapFields.keySet()) {
                SObjectField fieldToken = mapFields.get(fieldName);
                Schema.DescribeFieldResult f = fieldToken.getDescribe();

                fieldWrapper = new FieldWrapper(f, options, lstRecordTypeOptions);

                fDescribe.put(describe.getLocalName() + '_' + f.getLocalName(), fieldWrapper);
            }
        }

        return fDescribe;
    }

    /*******************************************************************************************************
    * @description Retrieves the Health Plan of the Patient
    * @param patientId Id of the Patient account
    * @param fieldSetName String
    * @return List<healthPlanPageWrapper>
    */
    @AuraEnabled
    public static List<healthPlanPageWrapper> getHealthPlan(String patientId, String fieldSetName) {

        List<healthPlanPageWrapper> lstHealthPlanWrapper = new List<healthPlanPageWrapper>();
        try {
            if (String.isNotBlank(patientId)) {
                List<String> fsList = new List<String>();
                if (String.isNotBlank(fieldSetName)) {
                    fsList = spc_FieldSetController.getFieldsByName(
                                 spc_ApexConstants.spc_HEALTH_PLAN,
                                 fieldSetName
                             );
                }

                List<String> fields = new List<String> {'LastModifiedDate', 'RecordType.Name', 'PatientConnect__Group_No__c', 'PatientConnect__ID_Policy_No__c',
                                                        'PatientConnect__PC_Card_Holder_Name__c', 'PatientConnect__PC_Cardholder_Employer__c', 'PatientConnect__PC_Cardholder_Relationship_to_Patient__c',
                                                        'PatientConnect__PC_Cardholder_s_Date_of_Birth__c', 'PatientConnect__PC_Effective_Date__c', 'PatientConnect__PC_Expiration_Date__c',
                                                        'PatientConnect__PC_Payer__c', 'PatientConnect__PC_Payer_Name__c', 'PatientConnect__PC_Plan_Claim_Card_No__c', 'PatientConnect__PC_Plan_Status__c',
                                                        'PatientConnect__PC_Plan_Type__c', 'PatientConnect__PC_Rx_Bin__c', 'PatientConnect__PC_Rx_Grp__c', 'spc_Policy_Holder_Phone__c', 'PatientConnect__PC_Rx_PCN__c', 'PatientConnect__PC_Plan_Status__c'
                                                       };

                spc_Database.assertAccess('PatientConnect__PC_Health_Plan__c', spc_Database.Operation.Reading, fields);

                if (fsList != null && fsList.size() > 0) { fields.addAll(fsList); }

                Set<String> fieldsAsSet = new Set<String>();
                List<String> fieldsWithoutDuplicates = new List<String>();
                fieldsAsSet.addAll(fields);
                fieldsWithoutDuplicates.addAll(fieldsAsSet);

                // Had to go with dynamic query due to fieldsets
                String query = '' + String.join(fieldsWithoutDuplicates, ', ');
                query = 'SELECT ' + query + ' ' + 'FROM ' + spc_ApexConstants.spc_HEALTH_PLAN +
                        ' WHERE ' + 'PatientConnect__Patient__c' + '=' + '\'' + patientId + '\'';

                List<PatientConnect__PC_Health_Plan__c> lstHealthPlan = spc_Database.queryWithAccess(query, fieldsWithoutDuplicates);

                if (lstHealthPlan != null && lstHealthPlan.size() > 0) {
                    for (PatientConnect__PC_Health_Plan__c a : lstHealthPlan) {
                        lstHealthPlanWrapper.add(new healthPlanPageWrapper(a, fsList));
                    }
                }
            }
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }
        return lstHealthPlanWrapper;
    }

    /*******************************************************************************************************
    * @description Method to get Applicant Field from Health Plan
    * @param hpFieldName String
    * @param index Integer
    * @param replaceTarget String
    * @param replaceText String
    * @return applicant Field String
    */
    static String getApplicantFieldFromHealthPlanField(String hpFieldName, Integer index, String replaceTarget, String replaceText) {
        String applicantField;
        String formattedReplaceText;
        if (index == 0) {
            formattedReplaceText = replaceText.replace('{n}', '');
        } else {
            formattedReplaceText = replaceText.replace('{n}', string.valueOf(index));
        }
        applicantField = hpFieldName.replace(replaceTarget, formattedReplaceText);
        return applicantField;
    }

    /*******************************************************************************************************
    * @description Method to get the Applicant from Health Plan
    * @param enrollmentCaseId String
    * @param activeApplicantId Id
    * @param fieldSetName String
    * @param ignoreFields String[]
    * @param ignoreFields String[]
    * @param replaceTarget String
    * @param replaceText String
    * @return List<healthPlanPageWrapper>
    */
    @AuraEnabled
    public static List<healthPlanPageWrapper> getApplicantHealthPlans(String enrollmentCaseId, Id activeApplicantId,
            String fieldSetName, String[] ignoreFields, String replaceTarget, String replaceText) {

        try {
            if (String.isNotBlank(enrollmentCaseId)) {
                List<String> fsList = new List<String>();
                if (String.isNotBlank(fieldSetName)) {
                    fsList = spc_FieldSetController.getFieldsByName(
                                 spc_ApexConstants.spc_HEALTH_PLAN,
                                 fieldSetName
                             );
                }

                //FLS Added; TEJAS PATEL; Feb 09 2017
                List<String> fields = new List<String> {'LastModifiedDate', 'RecordType.Name',
                                                        'PatientConnect__PC_HP_Group_No__c', 'PatientConnect__PC_HP_ID_Policy_No__c',
                                                        'PatientConnect__PC_HP_Cardholder_Name__c', 'PatientConnect__PC_HP_Cardholder_Employer__c',
                                                        'PatientConnect__PC_HP_Cardholder_Rel_to_Patient__c', 'PatientConnect__PC_HP_Cardholder_s_Date_of_Birth__c',
                                                        'PatientConnect__PC_HP_Effective_Date__c', 'PatientConnect__PC_HP_Expiration_Date__c',
                                                        'PatientConnect__PC_HP_Payer_Name__c', 'PatientConnect__PC_HP_Plan_Status__c',
                                                        'PatientConnect__PC_HP_Plan_Type__c', 'PatientConnect__PC_HP_Rx_Bin__c',
                                                        'PatientConnect__PC_HP_Rx_Grp__c', 'PatientConnect__PC_HP_Rx_PCN__c', 'PatientConnect__PC_HP_Record_Type__c',
                                                        'PatientConnect__PC_HP1_Group_No__c', 'PatientConnect__PC_HP1_ID_Policy_No__c', 'PatientConnect__PC_HP1_Cardholder_Name__c',
                                                        'PatientConnect__PC_HP1_Cardholder_Employer__c', 'PatientConnect__PC_HP1_Cardholder_s_Date_of_Birth__c', 'PatientConnect__PC_HP1_Effective_Date__c',
                                                        'PatientConnect__PC_HP1_Expiration_Date__c', 'PatientConnect__PC_HP1_Plan_Status__c', 'PatientConnect__PC_HP1_Plan_Type__c',
                                                        'PatientConnect__PC_HP1_Record_Type__c', 'PatientConnect__PC_HP1_Rx_Bin__c', 'PatientConnect__PC_HP1_Rx_Grp__c',
                                                        'PatientConnect__PC_HP1_Payer_Name__c', 'PatientConnect__PC_HP1_Cardholder_Rel_to_Patient__c', 'PatientConnect__PC_HP1_Rx_PCN__c',
                                                        'PatientConnect__PC_HP2_Group_No__c', 'PatientConnect__PC_HP2_ID_Policy_No__c', 'PatientConnect__PC_HP2_Cardholder_Name__c',
                                                        'PatientConnect__PC_HP2_Cardholder_Employer__c', 'PatientConnect__PC_HP2_Cardholder_s_Date_of_Birth__c', 'PatientConnect__PC_HP2_Effective_Date__c',
                                                        'PatientConnect__PC_HP2_Expiration_Date__c', 'PatientConnect__PC_HP2_Plan_Status__c', 'PatientConnect__PC_HP2_Plan_Type__c',
                                                        'PatientConnect__PC_HP2_Record_Type__c', 'PatientConnect__PC_HP2_Rx_Bin__c', 'PatientConnect__PC_HP2_Rx_Grp__c',
                                                        'PatientConnect__PC_HP2_Payer_Name__c', 'PatientConnect__PC_HP2_Cardholder_Rel_to_Patient__c', 'PatientConnect__PC_HP2_Rx_PCN__c'
                                                       };

                List<String> fsListNew = new List<String>();
                if (fsList != null && fsList.size() > 0) {

                    String fName;
                    for (String fsListField : fsList) {
                        for (integer i = 0; i <= 2; i++) {
                            fName = '';
                            fName = getApplicantFieldFromHealthPlanField(fsListField, i, replaceTarget, replaceText);

                            fsListNew.add(fName);
                        }
                    }
                    fields.addAll(fsListNew);

                }

                Set<String> fieldsAsSet = new Set<String>();
                List<String> fieldsWithoutDuplicates = new List<String>();
                fieldsAsSet.addAll(fields);

                List<String> ignoreFieldsOfApplicant = new List<String>();
                String applicantIgnoreFieldName;
                for (String ignoreField : ignoreFields) {

                    for (integer i = 0; i <= 2; i++) {
                        applicantIgnoreFieldName = '';
                        applicantIgnoreFieldName = getApplicantFieldFromHealthPlanField(ignoreField, i, replaceTarget, replaceText);

                        ignoreFieldsOfApplicant.add(applicantIgnoreFieldName);
                    }
                }
                fieldsAsSet.removeAll(ignoreFieldsOfApplicant);

                fieldsWithoutDuplicates.addAll(fieldsAsSet);

                //check access before SOQL
                spc_Database.assertAccess('PatientConnect__PC_Applicant__c', spc_Database.Operation.Reading, fields);

                // Had to go with dynamic query due to fieldsets
                String query = '' + String.join(fieldsWithoutDuplicates, ', ');
                query = 'SELECT ' + query + ' ' + 'FROM ' + 'PatientConnect__PC_Applicant__c' +
                        ' WHERE ' +
                        'PatientConnect__PC_Applicant__c' + '.RecordType.Name' + '=' + '\'' + spc_ApexConstants.APPLICANT_ONLINE_RECORD_TYPE_NAME + '\'' +
                        ' and Id' + '=' + '\'' + activeApplicantId + '\'' + ' Limit 1';
                List<PatientConnect__PC_Applicant__c> lstapplicant = spc_Database.queryWithAccess(query, fieldsWithoutDuplicates);

                List<healthPlanPageWrapper> lstHealthPlanWrapper = new List<healthPlanPageWrapper>();

                if (lstapplicant != null && !lstapplicant.isEmpty()) {

                    PatientConnect__PC_Health_Plan__c c;
                    String fieldPrefix = 'PatientConnect__PC_HP_';
                    for (integer i = 0; i <= 2; i++) {

                        if (!String.isEmpty((String)lstapplicant[0].get(fieldPrefix + 'Payer_Name__c'))) {
                            lstHealthPlanWrapper.add(new healthPlanPageWrapper(lstapplicant[0], fsList, fieldPrefix, i, replaceTarget, replaceText));
                        }
                    }
                }
                return lstHealthPlanWrapper;
            }
        } catch (Exception ex) {
            system.debug('Exception -->' + ex.getLineNumber());
            spc_Utility.logAndThrowException(ex);
        }
        return null;

    }

    /*******************************************************************************************************
     * @description To fetch the Payer from the Search String
    * @param searchString String
    * @return List<payerWrapper>
    */
    @AuraEnabled
    Public static List<payerWrapper> getPayer(String searchString) {
        List<payerWrapper> lstPayers = new List<payerWrapper>();
        String recordType = spc_ApexConstants.PAYER_RECORD_TYPE;
        String soslSearchString = '*' + String.escapeSingleQuotes(searchString) + '*';

        try {
            List<String> fields = new List<String> {'Id', 'Name', 'Phone', 'Fax', 'Type', 'RecordTypeId', 'RecordType.Name', 'PatientConnect__PC_Status__c', 'PatientConnect__PC_Email__c',
                                                    'PatientConnect__PC_Sub_Type__c'
                                                   };

            //check access before SOQL
            spc_Database.assertAccess('Account', spc_Database.Operation.Reading, fields);

            List<Account> accts;
            if (String.isEmpty(searchString)) {
                accts = [SELECT Id, Name, PatientConnect__PC_Status__c, PatientConnect__PC_Email__c,
                         Phone, Fax, Type, RecordTypeId,
                         PatientConnect__PC_Sub_Type__c, RecordType.Name FROM Account
                         WHERE Account.RecordType.Name = :recordType];
            } else {
                List<List<SObject>> soslQueryResults = [FIND :soslSearchString IN ALL FIELDS
                                                        RETURNING Account
                                                        (Id, Name, PatientConnect__PC_Status__c, PatientConnect__PC_Email__c,
                                                                Phone, Fax, Type, RecordTypeId,
                                                                PatientConnect__PC_Sub_Type__c, RecordType.Name
                                                                WHERE Account.RecordType.Name = :recordType)];
                accts = (List<Account>) soslQueryResults[0];
            }


            payerWrapper payerRecord;

            for (Account a : accts) {
                payerRecord = new payerWrapper(a);
                lstPayers.add(payerRecord);
            }
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }

        return lstPayers;
    }

    /*******************************************************************************************************
    * @description Wrapper class used for the healthPlanPage
    *
    */
    public class healthPlanPageWrapper {
        @AuraEnabled public Id healthPlanId {get; set;}
        @AuraEnabled public String groupNo {get; set;}
        @AuraEnabled public String policyNo {get; set;}
        @AuraEnabled public String cardholderName {get; set;}
        @AuraEnabled public String cardholderEmployer {get; set;}
        @AuraEnabled public String cardholderRelationshipToPatient {get; set;}
        @AuraEnabled public Date cardholdersBirthDate {get; set;}
        @AuraEnabled public Date healthPlanEffectiveDate {get; set;}
        @AuraEnabled public Date healthPlanExpirationDate {get; set;}
        @AuraEnabled public String payerName {get; set;}
        @AuraEnabled public String payerRef {get; set;}
        @AuraEnabled public String healthPlanClaimCardNo {get; set;}
        @AuraEnabled public String healthPlanStatus {get; set;}
        @AuraEnabled public String healthPlanType {get; set;}
        @AuraEnabled public String healthPlanTypeLabel {get; set;}
        @AuraEnabled public String planRxBin {get; set;}
        @AuraEnabled public String planRxGrp {get; set;}
        @AuraEnabled public String planRxPCN {get; set;}
        @AuraEnabled public String recordName {get; set;}
        @AuraEnabled public String recordNameLabel {get; set;}
        @AuraEnabled public DateTime lastModifiedDate {get; set;}
        @AuraEnabled public String source {get; set;}
        @AuraEnabled public Map<String, Object> fsFields {get; set;}
        @AuraEnabled public String test {get; set;}
        @AuraEnabled public Boolean isValid {get; set;}
        @AuraEnabled public String applicantSourceId {get; set;}
        @AuraEnabled public Boolean isApplicantSelected {get; set;}
        @AuraEnabled public String policyholderPhone {get; set;}


        public healthPlanPageWrapper(PatientConnect__PC_Health_Plan__c c, List<String> fsList) {
            healthPlanId = c.Id;
            groupNo = c.PatientConnect__Group_No__c;
            policyNo = c.PatientConnect__ID_Policy_No__c;
            cardholderName = c.PatientConnect__PC_Card_Holder_Name__c;
            cardholderEmployer = c.PatientConnect__PC_Cardholder_Employer__c;
            cardholderRelationshipToPatient = c.PatientConnect__PC_Cardholder_Relationship_to_Patient__c;
            cardholdersBirthDate = c.PatientConnect__PC_Cardholder_s_Date_of_Birth__c;
            healthPlanEffectiveDate = c.PatientConnect__PC_Effective_Date__c;
            healthPlanExpirationDate = c.PatientConnect__PC_Expiration_Date__c;
            payerName = c.PatientConnect__PC_Payer_Name__c;
            payerRef = c.PatientConnect__PC_Payer__c;
            healthPlanClaimCardNo = c.PatientConnect__PC_Plan_Claim_Card_No__c;
            healthPlanStatus = c.PatientConnect__PC_Plan_Status__c;
            healthPlanType = c.PatientConnect__PC_Plan_Type__c;
            healthPlanTypeLabel = c.PatientConnect__PC_Plan_Type__c;
            planRxBin = c.PatientConnect__PC_Rx_Bin__c;
            planRxGrp = c.PatientConnect__PC_Rx_Grp__c;
            planRxPCN = c.PatientConnect__PC_Rx_PCN__c;
            recordName = c.RecordType.Name;
            recordNameLabel = c.RecordType.Name;
            policyholderPhone = c.spc_Policy_Holder_Phone__c;
            this.lastModifiedDate = c.LastModifiedDate;
            source = System.Label.spc_EWP_Source_Existing;
            test = String.join(fsList, ',');
            fsFields = new Map<String, Object>();
            Object fsFieldVal;
            for (String fieldName : fsList) {
                fsFieldVal = spc_FieldSetController.castFieldValueBasedOnType(fieldName, c.get(fieldName), c);
                if (fsFieldVal == null) {
                    fsFieldVal = '';
                } else {

                }
                fsFields.put(fieldName, fsFieldVal);
            }
            isValid = true;
        }


        public healthPlanPageWrapper(PatientConnect__PC_Applicant__c a, List<String> fsList, String fieldPrefix, Integer index, String replaceTarget, String replaceText) {
            // Empty check to be done wherever string conversion happens. Otherwise, the property will not be available on the front end.
            groupNo = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Group_No__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Group_No__c'));
            policyNo = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'ID_Policy_No__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'ID_Policy_No__c'));
            cardholderName = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Cardholder_Name__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Cardholder_Name__c'));
            cardholderEmployer = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Cardholder_Employer__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Cardholder_Employer__c'));
            cardholderRelationshipToPatient = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Cardholder_Rel_to_Patient__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Cardholder_Rel_to_Patient__c'));
            cardholdersBirthDate = (Date)a.get(fieldPrefix + 'Cardholder_s_Date_of_Birth__c');
            healthPlanEffectiveDate = (Date)a.get(fieldPrefix + 'Effective_Date__c');
            healthPlanExpirationDate = (Date)a.get(fieldPrefix + 'Expiration_Date__c');
            payerName = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Payer_Name__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Payer_Name__c'));
            healthPlanClaimCardNo = '';
            healthPlanStatus = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Plan_Status__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Plan_Status__c'));
            healthPlanType = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Plan_Type__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Plan_Type__c'));
            healthPlanTypeLabel = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Plan_Type__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Plan_Type__c'));
            planRxBin = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Rx_Bin__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Rx_Bin__c'));
            planRxGrp = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Rx_Grp__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Rx_Grp__c'));
            planRxPCN = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Rx_PCN__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Rx_PCN__c'));
            recordName = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Record_Type__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Record_Type__c'));
            recordNameLabel = String.isEmpty(String.valueOf(a.get(fieldPrefix + 'Record_Type__c'))) ? '' : String.valueOf(a.get(fieldPrefix + 'Record_Type__c'));
            source = Label.spc_EWP_Source_Online;
            fsFields = new Map<String, Object>();

            String position;
            position = index == 0 ? '' : string.valueOf(index);
            Object fsFieldVal;
            String applicantFieldName;
            for (String fieldName : fsList) {
                try {
                    applicantFieldName = getApplicantFieldFromHealthPlanField(fieldName, index, replaceTarget, replaceText);

                    fsFieldVal = spc_FieldSetController.castFieldValueBasedOnType(applicantFieldName, a.get(applicantFieldName), a);
                } catch (Exception e) {
                    fsFieldVal = '';
                }
                if (fsFieldVal == null) {
                    fsFieldVal = '';
                }
                fsFields.put(fieldName, fsFieldVal);
            }

            isValid = true;
            applicantSourceId = fieldPrefix;
            isApplicantSelected = false;
        }

    }


    /*******************************************************************************************************
    * @description Wrapper class used for the Payer
    *
    */
    public class payerWrapper {
        @AuraEnabled public Id payerId {get; set;}
        @AuraEnabled public String payerName {get; set;}
        @AuraEnabled public String status {get; set;}
        @AuraEnabled public String email {get; set;}
        @AuraEnabled public String fax {get; set;}
        @AuraEnabled public String phone {get; set;}
        @AuraEnabled public String payerType {get; set;}
        @AuraEnabled public String payerTypeLabel {get; set;}
        @AuraEnabled public String subType {get; set;}
        @AuraEnabled public String recordName {get; set;}
        @AuraEnabled public String isSelected {get; set;}

        public payerWrapper(Account a) {
            payerId = a.Id;
            payerName = a.Name;
            status = a.PatientConnect__PC_Status__c;
            email = a.PatientConnect__PC_Email__c;
            fax = a.Fax;
            phone = a.Phone;
            payerType = a.Type;
            payerTypeLabel = a.Type;
            subType = a.PatientConnect__PC_Sub_type__c;
            recordName = a.RecordType.Name;
            isSelected = 'false';
        }
    }

    /*******************************************************************************************************
    * @description Wrapper class used for the Fields
    *
    */
    public class FieldWrapper {
        @AuraEnabled public String fieldLabel { get; set; }
        @AuraEnabled public String fieldName { get; set; }
        @AuraEnabled public String fieldType { get; set; }
        @AuraEnabled public Boolean fieldRequired { get; set; }
        @AuraEnabled public String referenceTo { get; set; }
        @AuraEnabled public List<spc_PicklistFieldManager.PicklistEntryWrapper> lstOptions { get; set; }
        @AuraEnabled public List<spc_PicklistFieldManager.PicklistEntryWrapper> lstRecordTypeOptions { get; set; }
        @AuraEnabled public Map<Id, String> RecordTypes { get; set; }
        public FieldWrapper() { }
        public FieldWrapper(Schema.DescribeFieldResult f, List<SelectOption> recordType, List<spc_PicklistFieldManager.PicklistEntryWrapper> lstRecordTypeLabelOptions) {
            fieldType = String.valueOf(f.getType());
            fieldLabel = f.getLabel();
            fieldName = f.getName();

            if (f.isCreateable() && !f.isNillable() && !f.isDefaultedOnCreate()) {
                fieldRequired = true;
            } else {
                fieldRequired = false;
            }

            if (f.getReferenceTo() != null && f.getReferenceTo().size() > 0) {
                referenceTo = (f.getReferenceTo()[0]).getDescribe().getLabel();
            }

            if (String.valueOf(f.getType()) == String.valueOf(Schema.DisplayType.Multipicklist) || String.valueOf(f.getType()) == String.valueOf(Schema.DisplayType.Picklist)) {
                lstOptions = new List<spc_PicklistFieldManager.PicklistEntryWrapper>();
                lstOptions.add(new spc_PicklistFieldManager.PicklistEntryWrapper(spc_ApexConstants.PICKLIST_NONE, System.Label.spc_Picklist_None));
                lstOptions.addAll(spc_PicklistFieldManager.getPicklistEntryWrappersFordescFields(f));
            }

            if (f.getLocalName() == 'RecordTypeId') {
                lstRecordTypeOptions = new List<spc_PicklistFieldManager.PicklistEntryWrapper>();
                lstRecordTypeOptions = lstRecordTypeLabelOptions;
                RecordTypes = new Map<Id, String>();
                for (SelectOption rt : recordType) {
                    RecordTypes.put(rt.getValue(), rt.getLabel());
                }
                fieldRequired = true;
            }
        }
    }

    /*******************************************************************************************************
    * @description Method to get the Field Names from the Field Set
    * @param typeName String
    * @param fsName String
    * @return List<spc_FieldSetController.FieldSetMember>
    */
    @AuraEnabled
    public static List<spc_FieldSetController.FieldSetMember> getFieldSetFields(String typeName, String fsName) {
        return spc_FieldSetController.getFields(typeName, fsName);
    }

}
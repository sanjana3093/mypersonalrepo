/**
* @author Deloitte
* @date Jan 15, 2019
*
* @description This is the test class for spc_CreateTasksOnScheduledDate
*/

@isTest
public class spc_CreateTasksOnScheduledDateTest {

    public static testMethod void testschedule() {
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        Account acc = spc_Test_Setup.createPatient('Test');
        acc.spc_HIPAA_Consent_Received__c = 'Yes';
        acc.spc_Patient_HIPAA_Consent_Date__c = System.today();
        insert acc;
        Case prgmCase = spc_Test_Setup.createCases(new List<Account> {acc}, 1, 'PC_Program').get(0);
        insert prgmCase;
        PatientConnect__PC_Interaction__c interaction = spc_Test_Setup.createInteraction(prgmCase.Id);
        interaction.spc_Scheduled_Date__c = System.today();
        insert interaction;
        try {
            Task tk = [Select id from Task where spc_Interaction__c = : interaction.Id];
            delete tk;
        } catch (Exception e) {
            //no tasks
        }
        Test.StartTest();
        spc_CreateTasksOnScheduledDateSchedular testsche = new spc_CreateTasksOnScheduledDateSchedular();
        String sch = '0 0 23 * * ?';
        system.schedule('Create task', sch, testsche );
        Test.stopTest();
    }

}
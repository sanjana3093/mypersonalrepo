public class OutboundSMSForCase {

    public static final String TEMPLATE_NAME='Confirmation of Receipt of Enrollment Form';
    
    @InvocableMethod(label='sendSMSforCase' description='Sends SMS text for a case if text consent is yes.')
    public static void sendSMSforCase(List<Case> cases) {
        
        if(cases != null && cases.size()>0){
                     
            System.debug(Logginglevel.DEBUG, 'Number of cases = '+cases.size());          
            for(Case theCase: cases){
                
                System.debug(Logginglevel.DEBUG, 'Account id = '+theCase.AccountId); 
                if(theCase.AccountId != null){
                    Account acct = [SELECT spc_Text_Consent__c, Phone, PatientConnect__PC_Primary_Zip_Code__c, PatientConnect__PC_First_Name__c FROM Account WHERE Id =:theCase.AccountId limit 1];
                    System.debug(Logginglevel.DEBUG, 'Case Number = '+theCase.CaseNumber);          
                    System.debug(Logginglevel.DEBUG, 'Consent = '+acct.spc_Text_Consent__c);  
                    String consent = acct.spc_Text_Consent__c;
                    
                    if('yes'.equalsIgnoreCase(consent)){
                        
                        Task newTask = createTask(theCase, TEMPLATE_NAME);
                        
                        //create and populate OutboundSMSDataObject
                        OutboundSMSDataObject smsObj = new OutboundSMSDataObject();
                        smsObj.phoneNumber = acct.Phone.replaceAll('\\D','');
                        smsObj.zipCode = acct.PatientConnect__PC_Primary_Zip_Code__c;
                        smsObj.firstName = acct.PatientConnect__PC_First_Name__c;         
                        smsObj.taskID = newTask.Id;
                        smsObj.templateName = TEMPLATE_NAME;
                        
                        if(smsObj.isValidRequest()){
                            OutboundSMSCallout.makeCallout(smsObj.toJSONString());                       
                        }else{
                            System.debug(Logginglevel.ERROR, 'Will not make SMS callout because required field is missing');                         
                        }
                        
                    }                      
                }else{
                    System.debug(Logginglevel.INFO, 'No AccountID associated with case ['+theCase.Id+']');                                             
                }
                
            } 
        }else{
                    
            System.debug(Logginglevel.DEBUG, 'No cases passed in by process builder!');          
        }          
     }
    
    public static Task createTask(Case theCase, String templateName)
    {
        //get the custom settings for the constants
        Outbound_SMS_Settings__c smsSettings = Outbound_SMS_Settings__c.getOrgDefaults();
        
        //create new task
        Task newTask = new Task();
        //newTask.WhoId = theCase.ContactId;
        newTask.WhatId = theCase.Id;
        newTask.Subject = 'SMS Outbound ' + templateName;
        newTask.Status = 'Completed';
        newTask.PatientConnect__PC_Channel__c = smsSettings.Channel__c;
        newTask.PatientConnect__PC_Direction__c = smsSettings.Direction__c;
        newTask.Priority = 'Normal';
        newTask.Type = 'Other';
        newTask.ActivityDate = Date.today();
        
        try{
            insert newTask;                          
            System.debug(Logginglevel.DEBUG, 'Successfully created task with id ['+newTask.Id+']');
        }catch(DMLException dmle){
            System.debug(Logginglevel.ERROR, 'Caught exception inserting task for case id ['+theCase.Id+'] ==> '+dmle);
        }
        
        return newTask;
    }   

    public static Task createTaskwithRecipient(Case theCase, String templateName, String recipient)
    {
        //get the custom settings for the constants
        Outbound_SMS_Settings__c smsSettings = Outbound_SMS_Settings__c.getOrgDefaults();
        
        //create new task
        Task newTask = new Task();
        //newTask.WhoId = theCase.ContactId;
        newTask.WhatId = theCase.Id;
        newTask.Subject = 'SMS Outbound ' + templateName;
        newTask.Status = 'Completed';
        newTask.PatientConnect__PC_Channel__c = smsSettings.Channel__c;
        newTask.PatientConnect__PC_Direction__c = smsSettings.Direction__c;
        newTask.Priority = 'Normal';
        newTask.Type = 'Other';
        newTask.ActivityDate = Date.today();
        newTask.Description = templateName + ' was sent to ' + recipient;
        
        try{
            insert newTask;                          
            System.debug(Logginglevel.DEBUG, 'Successfully created task with id ['+newTask.Id+']');
        }catch(DMLException dmle){
            System.debug(Logginglevel.ERROR, 'Caught exception inserting task for case id ['+theCase.Id+'] ==> '+dmle);
        }
        
        return newTask;
    }   
    
}
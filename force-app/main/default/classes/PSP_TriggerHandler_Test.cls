/*********************************************************************************************************
class Name      : PSP_TriggerHandler_Test 
Description		: Trigger Handler Test Class
@author		    : Saurabh Tripathi
@date       	: July 10, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi            July 10, 2019          Initial Version
****************************************************************************************************************/ 

@isTest
public class PSP_TriggerHandler_Test {
    /* Trigger Context Generic error message */
  	private static final String TR_CNTXT_ER = 'Trigger handler called outside of Trigger execution';
	/* last Method called identifier variable */
  	private static String lastMethodCalled; 
    /*handler*/
	private static PSP_TriggerHandler_Test.TestHandler handler;
    /* Test Handler Constant */
    private static final String TEST_HANDLER = 'TestHandler';
    //Static block for setting handler and isTriggerExecuting
	static {
        handler = new PSP_TriggerHandler_Test.TestHandler ();
        // override its internal trigger detection
        handler.isTrigExcutng = true;
    }
    // test implementation of the TriggerHandler
	/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Testing Non Trigger Context
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    @isTest
    static void testNonTriggerContext () {
        try {
            handler.run ();
           // System.assert (false, 'the handler ran but should have thrown');
        } catch (PSP_TriggerHandler.TriggerHandlerException te) {
            System.assertEquals (TR_CNTXT_ER, te.getMessage(), 'the exception message should match');
        } 
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Testing By Pass Api
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    @isTest
    static void testBypassAPI () {
     try{
        // afterUpdateMode();        
        // test a bypass and run handler
        PSP_trigger_Handler.bypass (TEST_HANDLER);
        handler.run ();
        System.assertEquals (null, lastMethodCalled, 'last method should be null when bypassed');
        resetTest();
      } catch (PSP_TriggerHandler.TriggerHandlerException te) {
            System.assertEquals (TR_CNTXT_ER, te.getMessage(), 'the exception message should match');
        } 
    }
    @isTest
    static void testBypassAPI1 () {
     try{
        // clear that bypass and run handler
        PSP_trigger_Handler.clearBypass (TEST_HANDLER);
        handler.run ();
        System.assertEquals ('afterUpdate', lastMethodCalled, 'last method called is afterUpdate');
        resetTest();
        } catch (PSP_TriggerHandler.TriggerHandlerException te) {
            System.assertEquals (TR_CNTXT_ER, te.getMessage(), 'the exception message should match');
        } 
    }  
     @isTest
    static void testBypassAPI2 () {
     try{
        // test a re-bypass and run handler
        PSP_trigger_Handler.bypass (TEST_HANDLER);
        handler.run ();
        System.assertEquals (null, lastMethodCalled, 'last method should be null when bypassed');
        resetTest ();
        } catch (PSP_TriggerHandler.TriggerHandlerException te) {
            System.assertEquals (TR_CNTXT_ER, te.getMessage(), 'the exception message should match');
        } 
    }
	 @isTest
    static void testBypassAPI3 () {
     try{
        // clear all bypasses and run handler
        PSP_trigger_Handler.clearAllBypasses ();
        handler.run ();
        System.assertEquals ('afterUpdate', lastMethodCalled, 'last method called should be afterUpdate');
        resetTest ();
        } catch (PSP_TriggerHandler.TriggerHandlerException te) {
            System.assertEquals (TR_CNTXT_ER, te.getMessage(), 'the exception message should match');
        } 
        
    }
    /*
     * Reset test method
     * */
    private static void resetTest () {
        lastMethodCalled = '';
    }
    
	/********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     Test Handler inner class
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
    private class TestHandler extends PSP_trigger_Handler {
        /********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     Before insert
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
        public override void beforeInsert () {
            PSP_TriggerHandler_Test.lastMethodCalled = 'beforeInsert';
        }
         /********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     Before update
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
        public override void  beforeUpdate () {
            PSP_TriggerHandler_Test.lastMethodCalled = 'beforeUpdate';
        }
        
    }
}
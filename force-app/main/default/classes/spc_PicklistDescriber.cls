/**
    @description Class which client code needs to interact with. Call any of the overloaded describe method,
    make sure you are passing all the params in the method signature
    @author Abhinav
*/
public class spc_PicklistDescriber {
    static final Pattern OPTION_PATTERN = Pattern.compile('<option.+?>(.+?)</option>');
    static final Pattern OPTION_PATTERN_VALUE = Pattern.compile('<option value="(.*?)">(.*?)</option>');
    static final Pattern DEPENDENT_PICKLIST_PATTERN_VALUE = Pattern.compile('pl.vals_(.*?)=(.*?);');
    public static Map<String, String> describeAsMap(String sobjectType, Id recordTypeId, String pickListFieldAPIName) {
        return parseOptionsAsMap(
        new Map<String, String> {
            'sobjectType' => sobjectType,
            'recordTypeId' => recordTypeId,
            'pickListFieldName' => pickListFieldAPIName
        },
        false);
    }

    public static Map<String, String> describeAsMap(String sobjectType, Id recordTypeId, String dependentFieldName, String controllingFieldName) {
        return parseOptionsAsMap(
        new Map<String, String> {
            'sobjectType' => sobjectType,
            'recordTypeId' => recordTypeId,
            'dependentFieldName' => dependentFieldName,
            'controllingFieldName' => controllingFieldName
        },
        true);
    }

    public static Map<String, String> parseOptionsAsMap(Map<String, String> params, Boolean isDependentPicklist) {
        Pagereference pr = Page.PatientConnect__PC_PicklistDesc;
        Matcher mchr = null;

        // to handle development mode, if ON
        pr.getParameters().put('core.apexpages.devmode.url', '1');

        for (String key : params.keySet()) {
            pr.getParameters().put(key, params.get(key));
        }
        Blob pdfBody;
        String xmlContent = '<option value="(val)">(val)</option><option value="(vals)">(vals)</option>';
        if (Test.isRunningTest()) {
            pdfBody = Blob.valueOf(xmlContent);
        } else {
            xmlContent = pr.getContent().toString();
        }

        if (isDependentPicklist) {
            mchr = DEPENDENT_PICKLIST_PATTERN_VALUE.matcher(xmlContent);
        } else {
            mchr = OPTION_PATTERN_VALUE.matcher(xmlContent);
        }
        Map<String, String> options = new Map<String, String>();
        String name;
        String value;
        while (mchr.find()) {
            system.debug(mchr.groupCount());
            value = mchr.group(1);
            if (isDependentPicklist) {
                name = mchr.group(2).substring(1, mchr.group(2).length() - 1);
            } else {
                name = mchr.group(2);
            }

            if (value.indexOf('"', 0) != -1) {
                value = value.substring(0, value.indexOf('"', 0));
            }
            options.put(value, name);
        }
        return options;
    }
}
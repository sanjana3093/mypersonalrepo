/*********************************************************************************************************
class Name      : PSP_CarePrgmEnrolleProTrggrImpl 
Description     : Trigger Implementation Class for CareProgramEnrolleeProduct
@author         : Soumya Mohapatra  
@date           : November 29, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Soumya Mohapatra            November 29, 2019          Initial Version
****************************************************************************************************************/ 
public with sharing class PSP_CarePrgmEnrolleProTrggrImpl {
    /* Map of care plan template Id and care plan template */
    private Map<Id, HealthCloudGA__CarePlanTemplate__c> tmpltMap = new Map<Id, HealthCloudGA__CarePlanTemplate__c> ();
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 11/26/2019
    * @Description : Method to create Care Plan and apply the care plan template.
    * @Param       : List of Enrolled Prescription And Service
    * @Return      : Void
    **************************************************************************************/
    public void createCarePlans(List<CareProgramEnrolleeProduct> enproductLst) {
        /* Set of CareProgramEnrollee Ids*/ 
        final Set<Id> progEnrollees = new Set<Id>();
        /* Set of CareProgramProduct Ids*/
        final Set<Id> careProgPrdIds = new Set<Id>();
        /* Set of Care Plan Templates */
        final Set<Id> cPlanTemplates = new Set<Id>();
        /* Set of Product2 Ids */
        final Set<ID> prodServs = new Set<ID>();
        /* New list of Cases of record type Care Plan */
        final List<Case> carePLans = new List<Case>();
         /* Map of Care Plan Template and care plan template problems */
        final Map<String, Set<HealthCloudGA__CarePlanTemplateProblem__c>> tempProMap = new Map<String, Set<HealthCloudGA__CarePlanTemplateProblem__c>>();
       /* Map of Care Plan Case and problems */
        final Map<HealthCloudGA__CarePlanProblem__c, Id> CaseIdProMap = new Map<HealthCloudGA__CarePlanProblem__c, Id>();
        /* Map of Care Plan Case and problems */
        final Map<Id, Set<HealthCloudGA__CarePlanProblem__c>> tempProToProMap = new Map<Id, Set<HealthCloudGA__CarePlanProblem__c>>();
        final Map<Id, Set<HealthCloudGA__CarePlanGoal__c>> tempGoalToGoalMap = new Map<Id, Set<HealthCloudGA__CarePlanGoal__c>>();
        final Map<Id, Id> proToTempMap = new Map<Id,Id>();
        final Map<Id, HealthCloudGA__CarePlanProblem__c> tempProIdToRecMap = new Map<Id, HealthCloudGA__CarePlanProblem__c>();
        /* Map of Care Plan Template Problem and Care Plan Template Goals */
        final Map<Id, Set<HealthCloudGA__CarePlanTemplateGoal__c>> tempProToGoalMap = new Map<Id, Set<HealthCloudGA__CarePlanTemplateGoal__c>>();
        /* Map of Care Plan Template Goal and Care Plan Template Tasks */
        final Map<Id, Set<HealthCloudGA__CarePlanTemplateTask__c>> tempGoalToTskMap = new Map<Id, Set<HealthCloudGA__CarePlanTemplateTask__c>>();
        /* Map of Care Plan Case Id and care plan template problems */
        final Map<Id, Set<HealthCloudGA__CarePlanTemplateProblem__c>> caseIdTempProMap = new Map<Id, Set<HealthCloudGA__CarePlanTemplateProblem__c>>();
         /* Case Id and Case Rec map */
        final Map<Id,Case> caseIdToRec = new Map<Id,Case>();
       
        
        /* Map of care program product id and care plan template id */
        final Map<Id, Id> cProdAndTmpltMap = new Map<Id, Id>();
        /* Map of care program enrolle and care program enrolle product map */
        final Map<Id, Id> cEnrolleeAndcEnrolleeProdMap = new Map<Id, Id>();
        /* account Ids set */
        final Set<ID> accntIds = new Set<ID> ();
        /* template Problem Ids set */
        final Set<ID> tempProIds = new Set<ID> ();
        /* account Id and contact Id map */
        final Map<ID, ID> accContMap = new Map<ID, ID>();
        for(CareProgramEnrolleeProduct cpEnrllPrd : enproductLst) {
            cEnrolleeAndcEnrolleeProdMap.put(cpEnrllPrd.CareProgramEnrolleeId,cpEnrllPrd.Id);
            progEnrollees.add(cpEnrllPrd.CareProgramEnrolleeId);
            careProgPrdIds.add(cpEnrllPrd.CareProgramProductId);
        }
        if(CareProgramProduct.sObjectType.getDescribe().isAccessible()) {
            // SOQL for getting Care Program Products
            final List<CareProgramProduct> carePrgProducts = new List<CareProgramProduct> ([Select Id, ProductId, PSP_Care_Plan_Template__c, CareProgramId from CareProgramProduct where ID in :careProgPrdIds ]);
            for(CareProgramProduct careProgProd : carePrgProducts) {
                cPlanTemplates.add(careProgProd.PSP_Care_Plan_Template__c);
                //care program product id, care plan template id
                cProdAndTmpltMap.put(careProgProd.id, careProgProd.PSP_Care_Plan_Template__c);
                prodServs.add(careProgProd.ProductId);
            }       
            updateCarePlans(cPlanTemplates, progEnrollees, accntIds, accContMap, carePrgProducts, carePLans,cEnrolleeAndcEnrolleeProdMap);
        }
        if(case.sObjectType.getDescribe().isAccessible() && !carePlans.isEmpty()) {
            Database.insert(carePlans);
            for(case cs : carePlans){
               caseIdToRec.put(cs.Id,cs);
            }
            final List<HealthCloudGA__CarePlanProblem__c> problems = new List<HealthCloudGA__CarePlanProblem__c>();
            final String carePlanRT = PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CASE_RT_CAREPLAN, Case.getSObjectType());
            if(HealthCloudGA__CarePlanTemplateProblem__c.sObjectType.getDescribe().isAccessible() && !tmpltMap.isEmpty()) {
                for(HealthCloudGA__CarePlanTemplateProblem__c cpTmpltProb : [Select Name, HealthCloudGA__Description__c, HealthCloudGA__Priority__c, HealthCloudGA__SortOrder__c, HealthCloudGA__CarePlanTemplate__c,
                                                                             HealthCloudGA__CarePlanTemplate__r.name,HealthCloudGA__CarePlanTemplate__r.HealthCloudGA__Description__c from 
                                                                             HealthCloudGA__CarePlanTemplateProblem__c where HealthCloudGA__CarePlanTemplate__c in :tmpltMap.keySet() AND HealthCloudGA__Active__c= true]) {
                                                                                 Set<HealthCloudGA__CarePlanTemplateProblem__c> cpTemProSet = new Set<HealthCloudGA__CarePlanTemplateProblem__c>();
                                                                                 if(tempProMap.containsKey(cpTmpltProb.HealthCloudGA__CarePlanTemplate__r.name + ' ' + cpTmpltProb.HealthCloudGA__CarePlanTemplate__r.HealthCloudGA__Description__c)){
                                                                                     cpTemProSet = tempProMap.get(cpTmpltProb.HealthCloudGA__CarePlanTemplate__r.name + ' ' + cpTmpltProb.HealthCloudGA__CarePlanTemplate__r.HealthCloudGA__Description__c);
                                                                                 }
                                                                                 cpTemProSet.add(cpTmpltProb);
                                                                                 tempProMap.put(cpTmpltProb.HealthCloudGA__CarePlanTemplate__r.name + ' ' + cpTmpltProb.HealthCloudGA__CarePlanTemplate__r.HealthCloudGA__Description__c,cpTemProSet);
                                                                             }
                  for(Case cs : carePlans){
                        for(HealthCloudGA__CarePlanTemplateProblem__c temPro : tempProMap.get(cs.Subject + ' ' + cs.description)){
                            problems.add(createCPProb(temPro, cs)); 
                            Set<HealthCloudGA__CarePlanTemplateProblem__c> cpTemProSet = new Set<HealthCloudGA__CarePlanTemplateProblem__c>();
                            if(caseIdTempProMap.containsKey(cs.Id)){
                                cpTemProSet = caseIdTempProMap.get(cs.Id);
                            }
                            cpTemProSet.add(temPro);
                            caseIdTempProMap.put(cs.Id,cpTemProSet);
                            tempProIds.add(temPro.Id);
                        }
                }
            }
            if(HealthCloudGA__CarePlanProblem__c.sObjectType.getDescribe().isAccessible() && !problems.isEmpty()) {
                Database.insert(problems);
                for(HealthCloudGA__CarePlanProblem__c problem : problems){
                    tempProIdToRecMap.put(problem.Id,problem);
                    CaseIdProMap.put(problem,problem.HealthCloudGA__CarePlan__c);
                }
                for(HealthCloudGA__CarePlanProblem__c pro : CaseIdProMap.keySet()){
                    for(HealthCloudGA__CarePlanTemplateProblem__c tempPro: caseIdTempProMap.get(CaseIdProMap.get(pro))){
                        if(pro.HealthCloudGA__Description__c == tempPro.HealthCloudGA__Description__c && pro.Name == tempPro.Name){
                            Set<HealthCloudGA__CarePlanProblem__c> cpProSet = new Set<HealthCloudGA__CarePlanProblem__c>();
                            if(tempProToProMap.containsKey(tempPro.Id)){
                                cpProSet = tempProToProMap.get(tempPro.Id);
                            }
                            cpProSet.add(pro);
                            tempProToProMap.put(tempPro.Id,cpProSet);
                            proToTempMap.put(pro.Id,tempPro.HealthCloudGA__CarePlanTemplate__c);
                        }
                    }
                }
                final List<HealthCloudGA__CarePlanGoal__c> goals = new List<HealthCloudGA__CarePlanGoal__c>();
                final List<HealthCloudGA__CarePlanTemplateGoal__c> tempGoals = new List<HealthCloudGA__CarePlanTemplateGoal__c>();
                if(HealthCloudGA__CarePlanTemplateGoal__c.sObjectType.getDescribe().isAccessible() && !tmpltMap.isEmpty()) {
                    for(HealthCloudGA__CarePlanTemplateGoal__c cpTmpltGoal : [Select Name, HealthCloudGA__Description__c, HealthCloudGA__CarePlanTemplateProblem__c,HealthCloudGA__Priority__c, HealthCloudGA__SortOrder__c, HealthCloudGA__CarePlanTemplate__c from 
                                                                              HealthCloudGA__CarePlanTemplateGoal__c where HealthCloudGA__CarePlanTemplateProblem__c in :tempProIds AND HealthCloudGA__Active__c= true]) {
                                                                                  Set<HealthCloudGA__CarePlanTemplateGoal__c> cpTemGoalSet = new Set<HealthCloudGA__CarePlanTemplateGoal__c>();     
                                                                                  if(tempProToGoalMap.containsKey(cpTmpltGoal.HealthCloudGA__CarePlanTemplateProblem__c)){
                                                                                      cpTemGoalSet = tempProToGoalMap.get(cpTmpltGoal.HealthCloudGA__CarePlanTemplateProblem__c);
                                                                                  }
                                                                                  cpTemGoalSet.add(cpTmpltGoal);
                                                                                  tempProToGoalMap.put(cpTmpltGoal.HealthCloudGA__CarePlanTemplateProblem__c,cpTemGoalSet);
                                                                                  tempGoals.add(cpTmpltGoal);
                                                                              }
                    for(Id tempProID : tempProToGoalMap.keySet()){
                        for(HealthCloudGA__CarePlanProblem__c cPlanPro : tempProToProMap.get(tempProID)) { 
                            for(HealthCloudGA__CarePlanTemplateGoal__c goal : tempProToGoalMap.get(tempProID)) {
                                final HealthCloudGA__CarePlanGoal__c carePlanGoal = new HealthCloudGA__CarePlanGoal__c();
                                carePlanGoal.name = goal.Name;
                                carePlanGoal.HealthCloudGA__Description__c = goal.HealthCloudGA__Description__c;
                                carePlanGoal.HealthCloudGA__Priority__c = goal.HealthCloudGA__Priority__c;
                                carePlanGoal.HealthCloudGA__SortOrder__c = goal.HealthCloudGA__SortOrder__c;
                                carePlanGoal.HealthCloudGA__CarePlan__c = cPlanPro.HealthCloudGA__CarePlan__c;
                                carePlanGoal.HealthCloudGA__CarePlanProblem__c = cPlanPro.id;
                                goals.add(carePlanGoal);
                            }
                        }
                    }
                    if(HealthCloudGA__CarePlanGoal__c.sObjectType.getDescribe().isAccessible() && !goals.isEmpty()) {
                        Database.insert(goals);
                        for(HealthCloudGA__CarePlanTemplateGoal__c tempGoal : tempGoals){
                            for(HealthCloudGA__CarePlanGoal__c goal : goals){
                                if(goal.name == tempGoal.name && goal.HealthCloudGA__Description__c == tempGoal.HealthCloudGA__Description__c && tempProToProMap.get(tempGoal.HealthCloudGA__CarePlanTemplateProblem__c).contains(tempProIdToRecMap.get(goal.HealthCloudGA__CarePlanProblem__c))){
                                    Set<HealthCloudGA__CarePlanGoal__c> cpGoalSet = new Set<HealthCloudGA__CarePlanGoal__c>();
                                    if(tempGoalToGoalMap.containsKey(tempGoal.Id)){
                                        cpGoalSet = tempGoalToGoalMap.get(tempGoal.Id);
                                    }
                                    cpGoalSet.add(goal);
                                    tempGoalToGoalMap.put(tempGoal.Id,cpGoalSet);
                                }
                            } 
                        }
                        final List<Task> tsks = new List<Task>();
                        if(HealthCloudGA__CarePlanTemplateTask__c.sObjectType.getDescribe().isAccessible()) {
                            for(HealthCloudGA__CarePlanTemplateTask__c cpTmplttask : [Select Name, HealthCloudGA__Status__c,HealthCloudGA__CarePlanTemplateGoal__c, HealthCloudGA__Priority__c, HealthCloudGA__SortOrder__c, HealthCloudGA__Subject__c, HealthCloudGA__CarePlanTemplate__c, PSP_Type__c, PSP_Sub_type__c, HealthCloudGA__Offset__c from 
                                                                                      HealthCloudGA__CarePlanTemplateTask__c where HealthCloudGA__CarePlanTemplateGoal__c in :tempGoalToGoalMap.keySet() AND HealthCloudGA__Active__c= true]) {
                                                                                          Set<HealthCloudGA__CarePlanTemplateTask__c> cpTemTaskSet = new Set<HealthCloudGA__CarePlanTemplateTask__c>();     
                                                                                          if(tempGoalToTskMap.containsKey(cpTmplttask.HealthCloudGA__CarePlanTemplateGoal__c)){
                                                                                              cpTemTaskSet = tempGoalToTskMap.get(cpTmplttask.HealthCloudGA__CarePlanTemplateGoal__c);
                                                                                          }
                                                                                          cpTemTaskSet.add(cpTmplttask);
                                                                                          tempGoalToTskMap.put(cpTmplttask.HealthCloudGA__CarePlanTemplateGoal__c,cpTemTaskSet);
                                                                                      }
                            for(Id tempGoalID : tempGoalToTskMap.keySet()){
                                for(HealthCloudGA__CarePlanGoal__c cPlanGoal : tempGoalToGoalMap.get(tempGoalID)) { 
                                    for(HealthCloudGA__CarePlanTemplateTask__c task : tempGoalToTskMap.get(tempGoalID)) {
                                        tsks.add(createTask(accContMap, caseIdToRec.get(cPlanGoal.HealthCloudGA__CarePlan__c), task, cPlanGoal,proToTempMap));
                                    }
                                }
                            }                                                             
                            
                            if(Task.sObjectType.getDescribe().isAccessible() && !tsks.isEmpty()) {
                                Database.insert(tsks);
                            }                       
                        }
                    }
                }
            }
        } 
        
    }
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 11/26/2019
    * @Description : Private method to update care plans
    * @Param       : List of Enrolled Prescription And Service
    * @Return      : Void
    **************************************************************************************/
    private void updateCarePlans (Set<Id> cPlanTemplates, Set<Id> progEnrollees, Set<ID> accntIds, Map<ID, ID> accContMap, List<CareProgramProduct> carePrgProducts, List<Case> carePLans, Map<Id, Id> cEnrolleeAndcEnrolleeProdMap) {
        if(HealthCloudGA__CarePlanTemplate__c.sObjectType.getDescribe().isAccessible() && !cPlanTemplates.isEmpty()) {
                tmpltMap = new Map<Id, HealthCloudGA__CarePlanTemplate__c> ([select ID, Name, HealthCloudGA__Description__c, PSP_Default_Care_Coordinator__c 
                                                                                                                          from HealthCloudGA__CarePlanTemplate__c where ID in :cPlanTemplates]);
            	if(CareProgramEnrollee.sObjectType.getDescribe().isAccessible()) {
                    final List<CareProgramEnrollee> cpEnrlleLst = new List<CareProgramEnrollee>([Select Id, AccountId, CareProgramId from CareProgramEnrollee where Id in :progEnrollees]);
                    
                    
                    for(CareProgramEnrollee cpEn : cpEnrlleLst) {
                        accntIds.add(cpEn.AccountId);
                    }
                    if(Contact.sObjectType.getDescribe().isAccessible()) {
                        for(Contact cntc : [Select AccountId from contact where AccountId in : accntIds]) {
                            accContMap.put(cntc.AccountId, cntc.Id);
                        }
                    }
                    // Iterating care program product and care program enrollee to get parameters for care plan (case)
                    for(CareProgramEnrollee cpEnrollee : cpEnrlleLst) {
                        for(CareProgramProduct cProgPrd : carePrgProducts) {
                            if(cProgPrd.CareProgramId == cpEnrollee.CareProgramId && tmpltMap.containsKey(cProgPrd.PSP_Care_Plan_Template__c)) {
                                // Creating new Care Plan case
                                final Case carePlan = new Case();
                                carePlan.RecordTypeId = PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CASE_RT_CAREPLAN, Case.getSObjectType());
                                carePlan.ContactId = accContMap.get(cpEnrollee.AccountId);
                                carePlan.AccountId = cpEnrollee.AccountId;
                                carePlan.Status = PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.CASE_PV_IN_PROGRESS);
                                carePlan.Subject = tmpltMap.get(cProgPrd.PSP_Care_Plan_Template__c).name;
                                if(cEnrolleeAndcEnrolleeProdMap.containsKey(cpEnrollee.Id) && String.isNotBlank(cEnrolleeAndcEnrolleeProdMap.get(cpEnrollee.Id))) {
                                    carePlan.PSP_Enrolled_Prescription_and_Service__c = cEnrolleeAndcEnrolleeProdMap.get(cpEnrollee.Id);
                                }
                                carePlan.PSP_Product__c = cProgPrd.ProductId;
                                carePlan.description = tmpltMap.get(cProgPrd.PSP_Care_Plan_Template__c).HealthCloudGA__Description__c;
                                carePlan.Origin = PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.CASE_PV_OTHER);
                                carePlan.Priority = PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.CASE_PV_NORMAL);
                                carePlan.OwnerId = UserInfo.getUserId();
                                carePLans.add(carePlan);
                            }
                        }
                    }
        		}                                                                                                      
            }
    	}
    	/**************************************************************************************
        * @author      : Deloitte
        * @date        : 11/26/2019
        * @Description : Private method to create task care plan problem
        * @Param       : List of Enrolled Prescription And Service
        * @Return      : Void
        **************************************************************************************/
        private HealthCloudGA__CarePlanProblem__c createCPProb(HealthCloudGA__CarePlanTemplateProblem__c cpTmpltProb, Case cPlanCase) {
            final HealthCloudGA__CarePlanProblem__c carePlanProb = new HealthCloudGA__CarePlanProblem__c();
            carePlanProb.name = cpTmpltProb.Name;
            carePlanProb.HealthCloudGA__Description__c = cpTmpltProb.HealthCloudGA__Description__c;
            carePlanProb.HealthCloudGA__Priority__c = cpTmpltProb.HealthCloudGA__Priority__c;
            carePlanProb.HealthCloudGA__SortOrder__c = cpTmpltProb.HealthCloudGA__SortOrder__c;
            carePlanProb.HealthCloudGA__CarePlan__c = cPlanCase.id;
            carePlanProb.HealthCloudGA__Account__c = cPlanCase.AccountId;
            return carePlanProb;
        }
        /**************************************************************************************
        * @author      : Deloitte
        * @date        : 11/26/2019
        * @Description : Private method to create task record
        * @Param       : List of Enrolled Prescription And Service
        * @Return      : Void
        **************************************************************************************/
        private Task createTask(Map<ID, ID> accContMap, Case cPlanCase, HealthCloudGA__CarePlanTemplateTask__c cpTmplttask, HealthCloudGA__CarePlanGoal__c goal,Map<Id,Id>proToTempMap) {
            final Task task = new Task();
            task.WhoId = accContMap.get(cPlanCase.AccountId);
            task.WhatId = cPlanCase.id;
            task.RecordTypeId = PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.TASK_RT_CAREPLAN, Task.getSObjectType());
            task.OwnerId = UserInfo.getUserId();
            task.status = cpTmplttask.HealthCloudGA__Status__c;
            task.HealthCloudGA__SortOrder__c = cpTmplttask.HealthCloudGA__SortOrder__c;
            task.HealthCloudGA__CarePlanGoal__c = goal.id;
            task.Subject = cpTmplttask.HealthCloudGA__Subject__c;
            task.Priority = cpTmplttask.HealthCloudGA__Priority__c;
            task.PSP_Type__c = cpTmplttask.PSP_Type__c;
            task.PSP_Sub_type__c = cpTmplttask.PSP_Sub_type__c;
            task.HealthCloudGA__CarePlanTemplate__c = proToTempMap.get(goal.HealthCloudGA__CarePlanProblem__c);
            if(cpTmplttask.HealthCloudGA__Offset__c != null) {
                task.ActivityDate = system.today() + Integer.valueOf(cpTmplttask.HealthCloudGA__Offset__c);   
            }
            return task;
        }
}
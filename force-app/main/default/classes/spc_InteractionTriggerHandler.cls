/**
* @author Deloitte
* @date 6-6-2018
*
* @description This is the Trigger Handler class for Interaction Trigger
*/

public class spc_InteractionTriggerHandler extends spc_TriggerHandler {
    spc_InteractionTriggerImplementation triggerImplementation = new spc_InteractionTriggerImplementation();
    spc_CommunicationTriggers CommtriggerImplementation = new spc_CommunicationTriggers();
    public override void afterInsert() {
        List<PatientConnect__PC_Interaction__c> recToCreateDoc = new List<PatientConnect__PC_Interaction__c>();
        Set<PatientConnect__PC_Interaction__c> lstTaskPACoverageDenied = new Set<PatientConnect__PC_Interaction__c>();
        Set<PatientConnect__PC_Interaction__c> lstTaskMedicationDeceased = new Set<PatientConnect__PC_Interaction__c>();
        List<PatientConnect__PC_Interaction__c> lstScheduledDateInteractions = new List<PatientConnect__PC_Interaction__c>();

        CommtriggerImplementation.afterInsertInteractionTriggers((List<PatientConnect__PC_Interaction__c>)Trigger.new);
        for (PatientConnect__PC_Interaction__c intract : (List<PatientConnect__PC_Interaction__c>)Trigger.New) {
            // If HIP is Other and SOC is not blank then create document
            if (intract.spc_HIP__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.INTERACTION_HIP_OTHER) && String.isNotBlank(intract.PatientConnect__PC_Participant__c)) {
                recToCreateDoc.add(intract);
            }
            if (intract.spc_Reason_for_Status__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.INTRTN_REASON_PA_DENIED) || intract.spc_Reason_for_Status__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.INTRACTN_REASON_COVERAGE_DENIED)) {
                lstTaskPACoverageDenied.add(intract);
            } else if (intract.spc_Reason_for_Status__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.INTRACTIN_REASON_MEDICATION) || intract.spc_Reason_for_Status__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.INTRCTN_REASON_PATIENT_DECEASED)) {
                lstTaskMedicationDeceased.add(intract);
            }

            //Checking Scheduled date for Call SOC to Confirm Infusion task
            if (intract.spc_Scheduled_Date__c != null && intract.spc_Scheduled_Date__c == System.today()) {
                lstScheduledDateInteractions.add(intract);
            }

        }
        if (!lstTaskPACoverageDenied.isEmpty()) {
            triggerImplementation.createEvalCovTask(lstTaskPACoverageDenied);
        }
        if (!lstTaskMedicationDeceased.isEmpty()) {
            triggerImplementation.createCallSOCHCPTask(lstTaskMedicationDeceased);
        }

        if (!recToCreateDoc.isempty()) {
            triggerImplementation.createDocument(recToCreateDoc);
        }
        if (! lstScheduledDateInteractions.isEmpty()) {
            triggerImplementation.createCallSOCToConfirmInfusionTask(lstScheduledDateInteractions);
        }
    }

    public override void afterUpdate() {
        List<PatientConnect__PC_Interaction__c> recToCreateDoc = new List<PatientConnect__PC_Interaction__c>();
        Set<PatientConnect__PC_Interaction__c> lstTaskSubEvaluateCoverage = new Set<PatientConnect__PC_Interaction__c>();
        Set<PatientConnect__PC_Interaction__c> lstTaskSubCallSOCHCP = new Set<PatientConnect__PC_Interaction__c>();
        CommtriggerImplementation.afterUpdateInteractionTriggers((List<PatientConnect__PC_Interaction__c>)Trigger.new);
        /*****Process Builder Code Starts for 189011 ***/
        List<Task> filteredReasonStatus = new List<Task>();
        Set<Id> evalCov = new Set<Id>();
        Set<Id> SocHcp = new Set<Id>();
        Set<PatientConnect__PC_Interaction__c> interactRecsSet = new Set<PatientConnect__PC_Interaction__c>();
        Set<PatientConnect__PC_Interaction__c> socinteractRecsSet = new Set<PatientConnect__PC_Interaction__c>();
        List<PatientConnect__PC_Interaction__c> lstScheduledDateInteractions = new List<PatientConnect__PC_Interaction__c>();

        for (PatientConnect__PC_Interaction__c intract : (List<PatientConnect__PC_Interaction__c>)Trigger.New) {
            Map<Id, PatientConnect__PC_Interaction__c> oldMap = (Map<Id, PatientConnect__PC_Interaction__c>) Trigger.oldMap;
            PatientConnect__PC_Interaction__c oldRec = oldMap.get(intract.Id);

            if (intract.spc_HIP__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.INTERACTION_HIP_OTHER) && intract.PatientConnect__PC_Participant__c != null && ( intract.spc_HIP__c != oldRec.spc_HIP__c || intract.PatientConnect__PC_Participant__c != oldRec.PatientConnect__PC_Participant__c)) {
                recToCreateDoc.add(intract);
            }
            if (intract.spc_Reason_for_Status__c != oldRec.spc_Reason_for_Status__c && (intract.spc_Reason_for_Status__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.INTRTN_REASON_PA_DENIED) || intract.spc_Reason_for_Status__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.INTRACTN_REASON_COVERAGE_DENIED))) {
                lstTaskSubEvaluateCoverage.add(intract);
            }
            if (intract.spc_Reason_for_Status__c != oldRec.spc_Reason_for_Status__c && (intract.spc_Reason_for_Status__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.INTRACTIN_REASON_MEDICATION) || intract.spc_Reason_for_Status__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.INTRCTN_REASON_PATIENT_DECEASED))) {
                lstTaskSubCallSOCHCP.add(intract);
            }
            //Checking Scheduled date for Call SOC to Confirm Infusion task
            if (intract.spc_Scheduled_Date__c != oldRec.spc_Scheduled_Date__c && intract.spc_Scheduled_Date__c != null && intract.spc_Scheduled_Date__c == System.today()) {
                lstScheduledDateInteractions.add(intract);
            }
        }

        /*****Coverage , PA Denied***/
        // Removing all those interactions which already have an open Evaluate Coverage task, to avoid repetitive task
        if (!lstTaskSubEvaluateCoverage.isEmpty()) {
            filteredReasonStatus = [select id, spc_Interaction__c, whatid from Task where spc_Interaction__c in :lstTaskSubEvaluateCoverage and Status != :spc_ApexConstants.getValue(spc_ApexConstants.PickListValue.TASK_STATUS_COMPLETED)
                                    and Subject = :spc_ApexConstants.getValue(spc_ApexConstants.PickListValue.TASK_SUB_CAT_EVALUATE_COVERAGE)];
        }
        for (Task filtertask : filteredReasonStatus) {
            evalCov.add(filtertask.spc_Interaction__c);
        }
        if (!evalCov.isEmpty()) {
            List<PatientConnect__PC_Interaction__c> interactRecs = [select id from PatientConnect__PC_Interaction__c where id in :evalCov];
            for (PatientConnect__PC_Interaction__c iR : interactRecs) {
                interactRecsSet.add(iR);
            }
            lstTaskSubEvaluateCoverage.removeAll(interactRecsSet);
        }
        if (!lstTaskSubEvaluateCoverage.isEmpty()) {
            triggerImplementation.createEvalCovTask(lstTaskSubEvaluateCoverage);
        }

        /*****SOC Medication Task ****/
        // Removing all those interactions which already have an open Call SOC/HIP task, to avoid repetitive task
        if (!lstTaskSubCallSOCHCP.isEmpty()) {
            filteredReasonStatus.clear();
            filteredReasonStatus = [select id, spc_Interaction__c, whatid from Task where spc_Interaction__c in :lstTaskSubCallSOCHCP and Status != :spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_COMPLETED)
                                    and Subject = :spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_CALL_HCP_SOC)];
        }
        for (Task filtertask : filteredReasonStatus) {
            SocHcp.add(filtertask.spc_Interaction__c);
        }
        if (!SocHcp.isEmpty()) {
            List<PatientConnect__PC_Interaction__c> socinteractRecs = [select id from PatientConnect__PC_Interaction__c where id in :SocHcp];
            for (PatientConnect__PC_Interaction__c iR : socinteractRecs) {
                socinteractRecsSet.add(iR);
            }
            lstTaskSubCallSOCHCP.removeAll(socinteractRecsSet);
        }
        if (!lstTaskSubCallSOCHCP.isEmpty()) {
            triggerImplementation.createCallSOCHCPTask(lstTaskSubCallSOCHCP);
        }

        if (!recToCreateDoc.isempty()) {
            triggerImplementation.createDocument(recToCreateDoc);
        }
        if (! lstScheduledDateInteractions.isEmpty()) {
            triggerImplementation.createCallSOCToConfirmInfusionTask(lstScheduledDateInteractions);

        }
    }

    public override void beforeInsert() {
        List<PatientConnect__PC_Interaction__c> lstInteractionsWithSOC = new List<PatientConnect__PC_Interaction__c>();

        for (PatientConnect__PC_Interaction__c interaction : (List<PatientConnect__PC_Interaction__c>)Trigger.New) {
            if (interaction.PatientConnect__PC_Participant__c != null) {
                lstInteractionsWithSOC.add(interaction);
            }
        }

        if (!lstInteractionsWithSOC.isEmpty()) {
            triggerImplementation.checkSOC(lstInteractionsWithSOC);
        }
    }

    public override void beforeUpdate() {
        List<PatientConnect__PC_Interaction__c> lstInteractionsWithSOC = new List<PatientConnect__PC_Interaction__c>();
        List<PatientConnect__PC_Interaction__c> lstInteractionsWithoutSOC = new List<PatientConnect__PC_Interaction__c>();

        for (PatientConnect__PC_Interaction__c interaction : (List<PatientConnect__PC_Interaction__c>)Trigger.New) {
            PatientConnect__PC_Interaction__c oldInteracion = (PatientConnect__PC_Interaction__c)trigger.OldMap.get(interaction.Id);

            if (oldInteracion.PatientConnect__PC_Participant__c != interaction.PatientConnect__PC_Participant__c && interaction.PatientConnect__PC_Participant__c != null) {
                lstInteractionsWithSOC.add(interaction);
            }

            if (oldInteracion.PatientConnect__PC_Participant__c != null && interaction.PatientConnect__PC_Participant__c == null) {
                lstInteractionsWithoutSOC.add(interaction);
            }
        }

        if (!lstInteractionsWithSOC.isEmpty()) {
            triggerImplementation.checkSOC(lstInteractionsWithSOC);
        }

        if (!lstInteractionsWithoutSOC.isEmpty()) {
            for (PatientConnect__PC_Interaction__c interaction : lstInteractionsWithoutSOC) {
                interaction.spc_Has_Active_SOC__c = false;
            }
        }
    }
}
/**
* @author Deloitte
* @date Jan 15, 2019
*
* @description This is the test class for spc_AssignToMeController
*/

@isTest
public class spc_AssignToMeControllerTest {
    @testSetup static void setup() {


        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);

    }
    TestMethod static void updateOwnershipTest() {

        UserRole usRole = [Select id from UserRole where name = 'Supervisor' limit 1];
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                          EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US', ProfileId = p.Id,
                          TimeZoneSidKey = 'America/Los_Angeles',
                          UserName = uniqueUserName, UserRoleId = usRole.Id);

        // This code runs as the system user




        System.runAs(u) {
            Account account = spc_Test_Setup.createAccount(spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.ACCOUNT_RT_CAREGIVER, Account.getSobjectType()));
            insert account;
            Task t = SPC_Test_Setup.createTask(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_NOT_STARTED), account.id, spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_PRIORITY_NORMAL), 'TEST', spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CHANNEL_CALL), Date.Today());
            insert t;
            spc_AssignToMeController.updateOwnership(t.id);
        }
    }
    TestMethod static void updateOwnershipTest2() {

        UserRole usRole = [Select id from UserRole where name = 'Case Manager' limit 1];
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                          EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US', ProfileId = p.Id,
                          TimeZoneSidKey = 'America/Los_Angeles',
                          UserName = uniqueUserName, UserRoleId = usRole.Id);

        // This code runs as the system user
        System.runAs(u) {
            Account account = spc_Test_Setup.createAccount(spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.ACCOUNT_RT_CAREGIVER, Account.getSobjectType()));
            //Account acc=new Account();
            insert account;
            Task t = SPC_Test_Setup.createTask(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_NOT_STARTED), account.id, spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_PRIORITY_NORMAL), 'TEST', spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CHANNEL_CALL), Date.Today());
            insert t;
            spc_AssignToMeController.updateOwnership(t.id);
        }
    }

}
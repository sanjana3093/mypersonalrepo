/**
* @author Deloitte
* @date 12-June-18
*
* @description This is the class created to Create Document Records on Inbound Email sent By HCP and relate Document logs to recognized senders case
*/

global class spc_InboundEmailByHCP implements Messaging.InboundEmailHandler {
  /*******************************************************************************************************
    * @description Creates document on recieving an email message
    * @return Email message
    */
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,
      Messaging.Inboundenvelope envelope) {
    Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

    try {
      PatientConnect__PC_Document__c docs = new PatientConnect__PC_Document__c();
      List<PatientConnect__PC_Document__c> documentList = new List<PatientConnect__PC_Document__c>();
      docs.PatientConnect__PC_From_Email_Address__c = email.fromAddress;
      spc_Incoming_Document_Routing_Mappings__c incomingDocMappingRecLists = spc_Incoming_Document_Routing_Mappings__c.getValues('OrgWideEmail2');
      if (incomingDocMappingRecLists != null) {
        docs.spc_to_Email_address__c = incomingDocMappingRecLists.spc_Email_Fax_Id__c;
      } else {
        docs.spc_to_Email_address__c = email.toAddresses[0];
      }
      String recTypeId = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_EMAIL_INBOUND)).getRecordTypeId();
      docs.recordtypeid = recTypeId;
      insert docs;
      List<Attachment> attachmentList = new List<Attachment>();
      Attachment emailContent;
      if (String.isNotBlank(email.htmlBody)) {
        emailContent = new Attachment();
        emailContent.Name = email.subject + '.pdf';
        emailContent.Body = blob.toPdf(email.htmlBody.stripHtmlTags());

        emailContent.ParentId = docs.Id;

        emailContent.ContentType = spc_ApexConstants.Application_ContantType;
        attachmentList.add(emailContent);
      } else if (String.isNotBlank(email.plainTextBody)) {

        emailContent = new Attachment();
        emailContent.Name = email.subject + '.txt';
        emailContent.Body = blob.valueOf(email.plainTextBody);

        emailContent.ParentId = docs.Id;
        emailContent.ContentType = spc_ApexConstants.Doc_ContantType;

        attachmentList.add(emailContent);
      }
      if (email.textAttachments != null) {
        for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) {
          Attachment attachment = new Attachment();
          attachment.Name = tAttachment.fileName;
          attachment.Body = Blob.valueOf(tAttachment.body);
          attachment.ParentId = docs.Id;
          attachment.ContentType = spc_ApexConstants.Doc_ContantType;
          attachmentList.add(attachment);
        }

      } else if (email.binaryAttachments != null) {
        for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
          Attachment attachment = new Attachment();
          attachment.ContentType = bAttachment.mimeTypeSubType;
          attachment.Name = bAttachment.fileName;
          attachment.Body = bAttachment.body;
          attachment.ParentId = docs.Id;
          attachmentList.add(attachment);
        }

      }
      if (!attachmentList.isEmpty()) {
        upsert attachmentList;
      }
      docs.PatientConnect__PC_Attachment_Id__c = emailContent.Id;
      update docs;
    } catch (Exception e) {
      spc_utility.logAndThrowException(e);
    }

    return result;
  }

}
/********************************************************************************************************
*  @author          Deloitte
*  @description     Therapy Association Trigger Handler Class to populate the CCL_Unique_Site_Therapy__c Field.
*  @param           
*  @date            July 13, 2020
*********************************************************************************************************/

public class CCL_TherapyAssociationTriggerHandler {
         
    /* This method updates the unique site therapy field for the Site record type*/
    public void updateUniqueSiteTherapyForDuplicationRule(List<CCL_Therapy_Association__c> siteTherapyList) {
        for(CCL_Therapy_Association__c siteTherapy :siteTherapyList) { 
            if(siteTherapy.RecordTypeId == CCL_StaticConstants_MRC.THERAPY_ASSOCIATION_RECORDTYPE_SITE && !String.isBlank(siteTherapy.CCL_Site__c) && !String.isBlank(siteTherapy.CCL_Therapy__c)) {
                siteTherapy.CCL_Unique_Site_Therapy__c  = siteTherapy.CCL_Site__c +''+ siteTherapy.CCL_Therapy__c;       
            }
        }
    }
}
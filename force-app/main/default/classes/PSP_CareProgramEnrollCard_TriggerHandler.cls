/*********************************************************************************************************
class Name      : PSP_CareProgramEnrollCard_TriggerHandler 
Description		: Trigger Handler Class for Care Program Object
@author     	: Pratik Raj  
@date       	: July 12, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Pratik Raj       July 12, 2019          Initial Version
****************************************************************************************************************/ 
public class PSP_CareProgramEnrollCard_TriggerHandler extends PSP_trigger_Handler {
    /*Trigger impementation being invoked.*/
    PSP_CareProgramEnrollCard_TriggerImpl triggerImpl = new PSP_CareProgramEnrollCard_TriggerImpl ();
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Before Insert method of Care Program Enrollment Card
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    public override void beforeInsert () {
        triggerImpl.uniqueCheck(trigger.new);
        triggerImpl.recordType(trigger.new);
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Before Update method of Care Program Enrollment Card
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    public override void beforeUpdate () {
        /*List of Program Card To Check */
        final List<CareProgramEnrollmentCard> prgCrdToChk = new List<CareProgramEnrollmentCard> ();
        /* List of Record Type Programs */
        final List<CareProgramEnrollmentCard> recTypeprograms = new List<CareProgramEnrollmentCard> ();
        for (CareProgramEnrollmentCard prgCard : (List<CareProgramEnrollmentCard>)Trigger.new) { 
            final CareProgramEnrollmentCard oldPrgCard = (CareProgramEnrollmentCard)Trigger.oldMap.get(PrgCard.Id);
            if (String.isNotBlank(prgCard.CardNumber) && prgCard.CardNumber != oldPrgCard.CardNumber) {
                prgCrdToChk.add (prgCard);
            }
            if (prgCard.RecordTypeId != oldPrgCard.RecordTypeId) {
                recTypeprograms.add (prgCard);
            }
        }
        if (!prgCrdToChk.isEmpty ()) {
            triggerImpl.uniqueCheck (prgCrdToChk);
        }
        if (!recTypeprograms.isEmpty ()) {
            triggerImpl.recordType (recTypeprograms);
        }
    }
    
}
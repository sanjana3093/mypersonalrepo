/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Processor class for Missing Information Enrollment Wizard Page
    *  @date            19-June-2018
    *  @version         1.0
    *
    *********************************************************************************************************/
global with sharing class spc_MissingInfoEWPProcessor  implements PatientConnect.PC_EnrollmentWizard.PageProcessor {
    /********************************************************************************************************
     *  @author           Deloitte
     *  @date             19/06/2018
     *  @description      Implemented method from PC_EnrollmentWizard.PageProcessor
     *  @param            enrollmentCase - Case Object
     *  @param            pageState - Map with field values from page
     *  @return           None
     *********************************************************************************************************/
    public static void processEnrollment(Case enrollmentCase, Map<String, Object> pageState) {
        ID programId = enrollmentCase.PatientConnect__PC_Program__c;
        boolean missingInfoAvailable = false;
        Case caseProgramEnroll = new Case(Id = programId);
        if (pageState != null && programId != null) {
            List<PatientConnect__PC_Association__c> associations = createAssociation(programId, enrollmentCase);
            if (pageState.get('missingInfo') != null) {
                for (String key : pageState.keySet()) {
                    if (key == 'missingInfo') {
                        if (programId != null) {
                            Object persistentMissingInfo = (Object) pageState.get('missingInfo');
                            Map<String, Object> persistentMissingInfoMap = (Map<String, Object>) persistentMissingInfo;
                            if (String.isNotBlank((String) persistentMissingInfoMap.get('hcpSignatureDateVal'))) {
                                caseProgramEnroll.spc_HCP_Signature_Date__c = true;
                                missingInfoAvailable = true;
                            }
                            if (String.isNotBlank((String) persistentMissingInfoMap.get('insuranceInformationVal'))) {
                                caseProgramEnroll.spc_Insurance_Information__c = true;
                                missingInfoAvailable = true;
                            }
                            if (String.isNotBlank((String) persistentMissingInfoMap.get('siteOfCareVal'))) {
                                caseProgramEnroll.spc_Site_of_Care__c = true;
                                missingInfoAvailable = true;
                            }
                            if (String.isNotBlank((String) persistentMissingInfoMap.get('rxInformationVal'))) {
                                caseProgramEnroll.spc_Rx_Information__c = true;
                                missingInfoAvailable = true;
                            }
                            if (!missingInfoAvailable) {
                                caseProgramEnroll.spc_No_Missing_Info_Found__c = true;
                            }
                        }
                    }
                }
            }
            if (!associations.isEmpty()) {
                spc_Database.ups(associations);
            }
        }
        spc_ApexConstants.IsEWPRunning = true;
        caseProgramEnroll.Is_No_Services_Consent__c = enrollmentCase.Is_No_Services_Consent__c;
        caseProgramEnroll.spc_Is_None_Service_Consent__c = enrollmentCase.spc_Is_None_Service_Consent__c;
        caseProgramEnroll.Status = spc_ApexConstants.STATUS_ENROLLED;

        spc_Database.upd(caseProgramEnroll);
        List<PatientConnect__PC_Document_Log__c> doclogs = [select
                ID, PatientConnect__PC_Association_Record_Id__c, PatientConnect__PC_Document__c, PatientConnect__PC_Program__c
                from PatientConnect__PC_Document_Log__c where PatientConnect__PC_Program__c = : enrollmentCase.Id];
        if (!doclogs.isEmpty()) {
            doclogs[0].PatientConnect__PC_Program__c  = programId;
            doclogs[0].PatientConnect__PC_Association_Record_Id__c   = null;
            doclogs[0].PatientConnect__PC_Association_Record_Name__c =  null;
            update doclogs;
            PatientConnect__PC_Document__c enrollmentForm = new PatientConnect__PC_Document__c(Id = docLogs[0].PatientConnect__PC_Document__c);
            enrollmentForm.spc_InquiryCaseId__c = null;
            enrollmentForm.spc_Program_Case_Id__c = programId;
            update enrollmentForm;
        }
    }
    /**
     * Method for updating association
     **/
    private static List<PatientConnect__PC_Association__c> createAssociation(ID programId, Case enrollmentCase) {
        List<PatientConnect__PC_Association__c> associations = new List<PatientConnect__PC_Association__c>();
        boolean associationExists = false;
        String addressId = '';
        String phone = '';
        String fax = '';
        boolean caregiverAssociationExists = false;
        Map<Id, String> caregiverMap = new Map<Id, String>();
        List<Case> cs = [Select PatientConnect__PC_Physician__c, spc_Caregiver__c from Case where Id = : programId];

        if (!String.isBlank(enrollmentCase.PatientConnect__PC_EnrollmentWizardState__c)) {
            Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(enrollmentCase.PatientConnect__PC_EnrollmentWizardState__c);
            Map<String, Object> objectNames = (Map<String, Object>)result.get('Caregiver_Information');
            Map<String, Object> resultAddressmap = (Map<String, Object>)result.get('Physician');
            Map<String, Object> resultSelectmap = (Map<String, Object>)resultAddressmap.get('selectedResult');
            Map<String, Object> persistentAddress = (Map<String, Object>)resultAddressmap.get('persistentAddress');
            if (resultSelectmap != null) {

                addressId = (String) resultSelectmap.get('addressId');
                phone = (String) resultSelectmap.get('assnPhone');

                fax = (String) resultSelectmap.get('assnFax');
            }


            if (!objectNames.isEmpty()) {
                Map<String, Object> caregiverAccount = (Map<String, Object>)objectNames.get('caregiverAccount');
                if (!caregiverAccount.isEmpty()) {
                    if (Label.spc_Yes.equalsIgnoreCase((String) caregiverAccount.get('spc_Permission_from_Patient_to_Share_PHI__c'))) {
                        caregiverMap.put(enrollmentCase.PatientConnect__PC_Program__c, Label.spc_Designated_Caregiver);
                    }
                }
            }
        }
        String role = spc_ApexConstants.ASSOCIATION_ROLE_TREATING_PHY;
        String caregiverRole = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_CAREGIVER);
        List<PatientConnect__PC_Association__c> existingPhysician = [SELECT Id FROM PatientConnect__PC_Association__c
                WHERE PatientConnect__PC_Program__c = :programId
                        AND PatientConnect__PC_Role__c = :role];

        List<PatientConnect__PC_Association__c> existingPhysicianList = new List<PatientConnect__PC_Association__c>();
        for (PatientConnect__PC_Association__c association : existingPhysician) {
            if (association != null) {
                associationExists = true;
                if (String.isNotBlank(phone) ) {
                    association.spc_Phone__c = FormatPhone(phone);
                }
                if (String.isNotBlank(fax) ) {
                    association.spc_Fax__c = FormatPhone(fax);
                }
                if (String.isNotBlank(addressId) ) {
                    association.PatientConnect__PC_AssociationStatus__c = 'Inactive';
                    association.spc_Address__c = addressId;

                }
                existingPhysicianList.add(association);
                break;
            }
        }
        if (!existingPhysicianList.isEmpty()) {

            spc_Database.upd(existingPhysicianList);
        }

        for (PatientConnect__PC_Association__c association : existingPhysician) {
            if (association != null) {
                existingPhysicianList = new List<PatientConnect__PC_Association__c>();
                associationExists = true;
                if (String.isNotBlank(addressId) ) {
                    association.PatientConnect__PC_AssociationStatus__c = 'Active';
                    existingPhysicianList.add(association);
                }
                break;
            }
        }
        if (!existingPhysicianList.isEmpty()) {
            spc_Database.upd(existingPhysicianList);
        }

        for (Case cas : cs) {
            if (!String.isBlank(cas.spc_Caregiver__c) ) {
                PatientConnect__PC_Association__c caregiverAssociation = new PatientConnect__PC_Association__c();
                caregiverAssociation.PatientConnect__PC_Program__c = programId;
                caregiverAssociation.PatientConnect__PC_Account__c = cas.spc_Caregiver__c;
                if (caregiverMap.containsKey(cas.id)) {
                    caregiverRole = caregiverMap.get(cas.id);
                }
                caregiverAssociation.PatientConnect__PC_Role__c = caregiverRole;
                associations.add(caregiverAssociation);
            }
            if (!String.isBlank(cas.PatientConnect__PC_Physician__c) && !associationExists) {
                PatientConnect__PC_Association__c physicianAssociation = new PatientConnect__PC_Association__c();
                physicianAssociation.PatientConnect__PC_Program__c = programId;
                physicianAssociation.PatientConnect__PC_Account__c = cas.PatientConnect__PC_Physician__c;
                physicianAssociation.PatientConnect__PC_Role__c = role;
                if (String.isNotBlank(addressId)) {

                    physicianAssociation.spc_Address__c = addressId;
                }
                if (String.isNotBlank(phone) ) {
                    physicianAssociation.spc_Phone__c = FormatPhone(phone);
                }
                if (String.isNotBlank(fax) ) {
                    physicianAssociation.spc_Fax__c = FormatPhone(fax);
                }
                associations.add(physicianAssociation);
            }
        }
        return associations;
    }
    /********************************************************************************************************
        *  @author         Sanjana Tripathy
        *  @date             24/10/2018
        *  @description      Returns formatted phone number
        *  @param           Phoe number
        *  @return          Phone
    *********************************************************************************************************/

    private static String FormatPhone(String Phone) {
        string nondigits = '[^0-9]';
        string PhoneDigits;

        // remove all non numeric
        PhoneDigits = Phone.replaceAll(nondigits, '');

        // 10 digit: reformat with dashes
        if (PhoneDigits.length() == 10)
            return '(' + PhoneDigits.substring(0, 3) + ')' + '-' +
                   PhoneDigits.substring(3, 6) + '-' +
                   PhoneDigits.substring(6, 10);
        // 11 digit: if starts with 1, format as 10 digit
        if (PhoneDigits.length() == 11) {
            if (PhoneDigits.substring(0, 1) == '1') {
                return '(' + PhoneDigits.substring(1, 4) + ')' + '-' +
                       PhoneDigits.substring(4, 7) + '-' +
                       PhoneDigits.substring(7, 11);

            }
        }

        // if it isn't a 10 or 11 digit number, return the original because
        // it may contain an extension or special information
        return ( Phone );
    }
}
/*********************************************************************************************************
class Name      : PSP_Assign_Enrolled_Patient_Handler 
Description		: Trigger Handler Class for CareProgramEnrollee (Program Enrollee) Object
@author     	: Saurabh Tripathi 
@date       	: December 2, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi            December 2, 2019          Initial Version
****************************************************************************************************************/ 
public class PSP_Assign_Enrolled_Patient_Handler extends PSP_trigger_Handler {
    /*Instantiation of Trigger impelementation class *****/ 
    final PSP_Assign_Enrolled_Patient_Implement triggerImpl = new PSP_Assign_Enrolled_Patient_Implement();
/**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 12/02/2019
  	* @Description : Before Insert Method being overriden here
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/    
	public override void beforeInsert () {
    	triggerImpl.assignPatient (Trigger.New,(Map<Id,CareProgramEnrollee>)Trigger.oldMap);        
    }
    /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 12/02/2019
  	* @Description : Before Update Method being overriden here
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/    
	public override void beforeUpdate () {
    	triggerImpl.assignPatient (Trigger.New,(Map<Id,CareProgramEnrollee>)Trigger.oldMap);        
    }
}
/**
* @author Deloitte
* @date 08-Aug-2018
*
* @description This is the test for spc_inquirycaseControllerTest
*/

@isTest
public class spc_inquirycaseControllerTest {
    static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

    }
    @isTest(SeeAllData = true)
    static void getCase() {
        Account account = spc_Test_Setup.createTestAccount('PC_Patient');
        account.spc_HIPAA_Consent_Received__c = 'Yes';
        account.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        account.spc_Patient_Marketing_Consent_Date__c = Date.valueOf('2014-06-07');
        account.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf('2014-06-07');
        account.PatientConnect__PC_Email__c = 'test@test.com';
        spc_Database.ins(account);

        Case inquiry = spc_Test_Setup.newCase(account.id, 'spc_Inquiry');
        spc_Database.ins(inquiry);

        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');

        spc_Database.ins(programcase);

        List<StaticResource> srs = new List<StaticResource>();
        List<PatientConnect__PC_Document__c> docs = new List<PatientConnect__PC_Document__c>();
        Map<Id, Id> mapAccountConTest = new Map<Id, Id>();
        Account   manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        spc_Database.ins(manAcc);
        PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('Engagement Program SageRx', manAcc.Id);
        engPrgm.PatientConnect__PC_Program_Code__c = 'SAGE';
        insert engPrgm;
        //Create Program Case

        Map<String, Object> eletterdoc = new Map<String, Object>();
        eletterdoc.put('eletter', 'test name');
        eletterdoc.put('status', 'Sent');
        eletterdoc.put('desc', 'test');
        eletterdoc.put('caseId', programCase.Id);
        eletterdoc.put('pmrc', '11111');
        eletterdoc.put('engagementProgram', engPrgm.Id);
        eletterdoc.put('statusDate', '2019-01-22');
        eletterdoc.put('scheduledDate', '2019-01-22');
        spc_InquiryCaseController.createDocument(eletterdoc);
        spc_InquiryCaseController.getPicklistEntryMap();

        Map<String, Object> eletterdoc2 = new Map<String, Object>();
        eletterdoc2.put('eletter', 'test name');
        eletterdoc2.put('status', 'Sent');
        eletterdoc2.put('desc', 'test2');
        eletterdoc2.put('caseId', inquiry.Id);
        eletterdoc2.put('pmrc', '111112');
        eletterdoc2.put('engagementProgram', engPrgm.Id);
        eletterdoc2.put('statusDate', '2019-01-23');
        eletterdoc2.put('scheduledDate', '2019-01-23');
        spc_InquiryCaseController.createDocument(eletterdoc2);

        Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
          EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
          LocaleSidKey = 'en_US', ProfileId = p.Id,
          TimeZoneSidKey = 'America/Los_Angeles',
          UserName = 'TestUserabc@test.com');
        test.startTest();
        Id recordtypeid = spc_InquiryCaseController.getRecordTypeIdForInquiry();

        spc_InquiryCaseController.sendBrochure(inquiry.id);
        mapAccountConTest = spc_InquiryCaseController.mapAccountwithContact(inquiry);
        spc_InquiryCaseController.sendEmailBrochure(inquiry, mapAccountConTest, srs);
        spc_InquiryCaseController.createDocumentsAndDocumentsLogs(inquiry, srs);
        spc_InquiryCaseController.createDocLogsFromDocs(docs, mapAccountConTest, srs);
        test.stopTest();
    }

    @isTest
    static void getRecordTypeIdTest() {
        setup();
        Account acc = spc_Test_Setup.createTestAccount('PC_Patient');
        acc.spc_HIPAA_Consent_Received__c = 'Yes';
        acc.spc_Patient_Services_Consent_Received__c = 'Yes';
        acc.spc_Patient_Marketing_Consent_Date__c = Date.valueOf('2014-06-07');
        acc.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf('2014-06-07');
        acc.spc_Patient_Service_Consent_Date__c = Date.today();
        acc.PatientConnect__PC_Email__c = 'test@test.com';
        acc.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';

        acc.spc_Text_Consent__c = 'Yes';
        acc.spc_Services_Text_Consent__c = 'Yes';
        acc.spc_REMS_Text_Consent__c = 'Yes';
        acc.spc_HIPAA_Consent_Received__c = 'Yes';
        acc.spc_REMS_Text_Consent_Date__c = Date.today();
        acc.spc_Services_Text_Consent_Date__c = Date.today();
        acc.spc_Patient_Marketing_Consent_Date__c = Date.today();
        acc.spc_Marketing_Consent_Date__c = Date.today();
        acc.spc_Patient_Text_Consent_Date__c = Date.today();
        spc_Database.ins(acc);

        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        spc_Database.ins(manAcc);

        PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        spc_Database.ins(engPrgm);

        Case prgCase = spc_Test_Setup.newCase(acc.id, 'PC_Program');
        prgCase.PatientConnect__PC_Engagement_Program__c = engPrgm.Id;
        spc_Database.ins(prgCase);

        Case preTrt = spc_Test_Setup.newCase(acc.id, 'spc_Pre_Treatment');
        preTrt.PatientConnect__PC_Program__c = prgCase.Id;
        preTrt.PatientConnect__PC_Engagement_Program__c = prgCase.PatientConnect__PC_Engagement_Program__r.Id;
        spc_Database.ins(preTrt);

        Case postTrt = spc_Test_Setup.newCase(acc.id, 'spc_Post_Treatment');
        postTrt.PatientConnect__PC_Program__c = prgCase.Id;
        postTrt.PatientConnect__PC_Engagement_Program__c = prgCase.PatientConnect__PC_Engagement_Program__r.Id;
        spc_Database.ins(postTrt);

        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Pre-Treatment', 'Email');
        eletter.spc_Consent_Needed__c = 'Service Consent';
        insert eletter;
        PatientConnect__PC_Engagement_Program_Eletter__c engprgmeletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Email', engPrgm.Id);
        insert engprgmeletter;

        PatientConnect__PC_eLetter__c eletter1 = spc_Test_Setup.createEeletter('Pre-Treatment', 'Postal mail');
        eletter1.spc_Consent_Needed__c = 'Service Consent';
        eletter1.PatientConnect__PC_Template_Name__c = 'Test Name';
        eletter1.spc_eLetter_Data_Migration_Id__c = 'Test Id';
        insert eletter1;
        PatientConnect__PC_Engagement_Program_Eletter__c engprgmeletter1 = spc_Test_Setup.createEProgramEletter(eletter1.Id, 'Postal mail', engPrgm.Id);
        insert engprgmeletter1;

        String invokeAction = System.Label.spc_inquiry;
        String invokeAction1 = System.Label.spc_actioncenter;


        test.startTest();
        spc_InquiryCaseController.getLetters(prgCase.Id, invokeAction);
        List<spc_LightningMap> getRecordType = spc_InquiryCaseController.getLetters(preTrt.Id, invokeAction);
        List<spc_LightningMap> getRecordType2 = spc_InquiryCaseController.getLetters(postTrt.Id, invokeAction);
        system.assertNotEquals(getRecordType.size(), Null);
        List<spc_LightningMap> getRecordType1 = spc_InquiryCaseController.getLetters(preTrt.Id, invokeAction1);
        system.assertNotEquals(getRecordType1.size(), Null);

        Map<String, Object> eletterdoc6 = new Map<String, Object>();
        eletterdoc6.put('eletter', 'test name3');
        eletterdoc6.put('status', 'Sent');
        eletterdoc6.put('desc', 'test42');
        eletterdoc6.put('caseId', postTrt.Id);
        eletterdoc6.put('pmrc', '11231114');
        eletterdoc6.put('engagementProgram', engPrgm.Id);
        eletterdoc6.put('statusDate', '2019-01-20');
        eletterdoc6.put('scheduledDate', '2019-01-20');
        spc_InquiryCaseController.createDocument(eletterdoc6);
        test.stopTest();
    }

    @isTest
    static void getRecordTypeIdTest1() {
        setup();
        Id accountRecordTypeId = spc_System.recordTypeId('Account', 'PC_Patient');
        Account acc = new Account(Name = 'Account Name', RecordTypeId = accountRecordTypeId);
        acc.PatientConnect__PC_Email__c = 'test@test.com';
        acc.PatientConnect__PC_Date_of_Birth__c = System.today() - 30;
        acc.Phone = '1234567890';
        acc.spc_REMS_ID__c = '1234';
        acc.PatientConnect__PC_Gender__c = 'Male';
        spc_Database.ins(acc);

        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        spc_Database.ins(manAcc);

        PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        spc_Database.ins(engPrgm);

        Case prgCase = spc_Test_Setup.newCase(acc.id, 'PC_Program');
        prgCase.PatientConnect__PC_Engagement_Program__c = engPrgm.Id;
        spc_Database.ins(prgCase);

        Case preTrt = spc_Test_Setup.newCase(acc.id, 'spc_Pre_Treatment');
        preTrt.PatientConnect__PC_Program__c = prgCase.Id;
        preTrt.PatientConnect__PC_Engagement_Program__c = prgCase.PatientConnect__PC_Engagement_Program__r.Id;
        spc_Database.ins(preTrt);
        
        PatientConnect__PC_Document__c docNew = spc_Test_Setup.createDocument(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_FAX_INBOUND), 'Sent', engPrgm.Id);
        spc_Database.ins(docNew);
        
        
        Map<String,Object> eletterdoc5=new Map<String,Object>();
        eletterdoc5.put('eletter','test name2');
        eletterdoc5.put('status','Sent');
        eletterdoc5.put('desc','test23');
        eletterdoc5.put('caseId',preTrt.Id);
        eletterdoc5.put('pmrc','11111212');
        eletterdoc5.put('engagementProgram',engPrgm.Id);
        eletterdoc5.put('statusDate','2019-01-25');
        eletterdoc5.put('scheduledDate','2019-01-25');
        eletterdoc5.put('parentDoc',docNew.Id);
        spc_InquiryCaseController.createDocument(eletterdoc5);

        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Pre-Treatment', 'Email');
        eletter.spc_Consent_Needed__c = 'Service Consent';
        insert eletter;
        PatientConnect__PC_Engagement_Program_Eletter__c engprgmeletter = spc_Test_Setup.createEProgramEletter(eletter.Id, 'Email', engPrgm.Id);
        insert engprgmeletter;

        PatientConnect__PC_eLetter__c eletter1 = spc_Test_Setup.createEeletter('Pre-Treatment', 'Postal mail');
        eletter1.spc_Consent_Needed__c = 'Service Consent';
        eletter1.PatientConnect__PC_Template_Name__c = 'Test Name';
        eletter1.spc_eLetter_Data_Migration_Id__c = 'Test Id';
        insert eletter1;
        PatientConnect__PC_Engagement_Program_Eletter__c engprgmeletter1 = spc_Test_Setup.createEProgramEletter(eletter1.Id,'Postal mail',engPrgm.Id);
        engprgmeletter1.spc_Channel__c = 'Postal mail';
        engprgmeletter1.spc_PMRC_Code__c = 'TESTPMRC';
        insert engprgmeletter1;

        String invokeAction = System.Label.spc_inquiry;
        String invokeAction1 = System.Label.spc_actioncenter;
        
        
        test.startTest(); 
        acc.spc_Services_Text_Consent__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_VERBAL_CONSENT_RECEIVED);
        acc.spc_Patient_Mrkt_and_Srvc_consent__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_VERBAL_CONSENT_RECEIVED);
        update acc;
        List<spc_LightningMap> getRecordType = spc_InquiryCaseController.getLetters(preTrt.Id, invokeAction);
        system.assertNotEquals(getRecordType.size(), Null);
        List<spc_LightningMap> getRecordType1 = spc_InquiryCaseController.getLetters(preTrt.Id, invokeAction1);
        system.assertNotEquals(getRecordType1.size(), Null);
        List<spc_LightningMap> getRecordType2 = spc_InquiryCaseController.getLetters(prgCase.Id, invokeAction1);
        test.stopTest();
    }
    
    @isTest
    public static void testgetEletters() {
        setup();
        Id accountRecordTypeId = spc_System.recordTypeId('Account', 'PC_Patient');
        Account acc = new Account(Name = 'Account Name', RecordTypeId = accountRecordTypeId);
        acc.PatientConnect__PC_Email__c = 'test@test.com';
        acc.PatientConnect__PC_Date_of_Birth__c = System.today()-30;
        acc.Phone = '1234567890';
        acc.spc_REMS_ID__c = '1234';
        acc.PatientConnect__PC_Gender__c = 'Male';
        spc_Database.ins(acc);
       
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        spc_Database.ins(manAcc);
        
        PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        spc_Database.ins(engPrgm);
        
        Case prgCase = spc_Test_Setup.newCase(acc.id, 'PC_Program');
        prgCase.PatientConnect__PC_Engagement_Program__c = engPrgm.Id;
        spc_Database.ins(prgCase);
        
        Case preTrt = spc_Test_Setup.newCase(acc.id, 'spc_Pre_Treatment');
        preTrt.PatientConnect__PC_Program__c = prgCase.Id;
        preTrt.PatientConnect__PC_Engagement_Program__c = prgCase.PatientConnect__PC_Engagement_Program__r.Id;
        spc_Database.ins(preTrt);
        
        PatientConnect__PC_Document__c docNew = spc_Test_Setup.createDocument(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_FAX_INBOUND), 'Sent', engPrgm.Id);
        spc_Database.ins(docNew);
        
        Map<String,Object> eletterdoc5=new Map<String,Object>();
        eletterdoc5.put('eletter','test name2');
        eletterdoc5.put('status','Sent');
        eletterdoc5.put('desc','test23');
        eletterdoc5.put('caseId',preTrt.Id);
        eletterdoc5.put('pmrc','11111212');
        eletterdoc5.put('engagementProgram',engPrgm.Id);
        eletterdoc5.put('statusDate','2019-01-25');
        eletterdoc5.put('scheduledDate','2019-01-25');
        eletterdoc5.put('parentDoc',docNew.Id);
        spc_InquiryCaseController.createDocument(eletterdoc5);
        
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program','Postal Mail');
        eletter.spc_Consent_Needed__c = 'Service Consent';
        insert eletter;
        PatientConnect__PC_Engagement_Program_Eletter__c engprgmeletter = spc_Test_Setup.createEProgramEletter(eletter.Id,'Postal Mail',engPrgm.Id);
        engprgmeletter.spc_Channel__c = 'Postal Mail';
        engprgmeletter.spc_PMRC_Code__c = 'PMRC_Mail';
        insert engprgmeletter;
        
        test.startTest(); 
        List<spc_LightningMap> getRecordType2 = spc_InquiryCaseController.getLetters(prgCase.Id, System.Label.spc_actioncenter);
        test.stopTest();
    }
}
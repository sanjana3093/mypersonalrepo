/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the test class for CareProgram Trigger Handler
*  @date            07/10/2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Shourya Solipuram           July 10, 2019          Initial Version
****************************************************************************************************************/

@IsTest
public class PSP_CareProgram_TriggerHandlerTest {
    /*Constant for Test */
    private static final String TEST = 'Test';
    /* Apex constants */
    static final List<PSP_ApexConstantsSetting__c>  APEX_CONSTANTS =  PSP_Test_Setup.setDataforApexConstants();
    /********************************************************************************************************
*  @author          Deloitte
*  @description     Testing Before Insert Scenarios
*  @date            July 12, 2019
*  @version         1.0
*********************************************************************************************************/
    @IsTest static void testInsertScenarios () {
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram (TEST,null);
        cP1.RecordTypeId=PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CAREPROGRAM_RT_FAMILY, CareProgram.getSObjectType());
        Database.insert (cP1);
        
        final CareProgram cP2 = PSP_Test_Setup.createTestCareProgram (TEST,cP1.Id);
        try {
            Database.insert (cP2);
        } catch (DmlException e) {
            System.assert( e.getMessage ().contains ('Insert failed.'),e.getMessage () );
        }
    }
    /********************************************************************************************************
*  @author          Deloitte
*  @description     Testing Before Update Scenarios of Care program
*  @date            July 12, 2019
*  @version         1.0
*********************************************************************************************************/
    @IsTest static void testUpdateScenarios () {
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram ('Test1',null);
        
        cP1.RecordTypeId=PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CAREPROGRAM_RT_FAMILY, CareProgram.getSObjectType());
        Database.insert (cP1);
        final CareProgram cP2 = PSP_Test_Setup.createTestCareProgram ('Test2',cP1.Id);
        Database.insert (cP2);
        cP2.Name = TEST;
        try {
            Database.update (cP2);
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains ('Update failed.'),e.getMessage () );
        }
    }
}
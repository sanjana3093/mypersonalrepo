
/*******************************************************************************************************
    *  @author          Deloitte
    *  @description     spc_picklistDescriberTest
    *  @date            08-Aug-2018
    *  @version         1.0
    *
    *********************************************************************************************************/
@isTest
public class spc_picklistDescriberTest {

    @isTest
    static void getpicklist() {
        Map<String, String> myMapOne = new Map<String, String>();
        Map<String, String> myMapTwo = new Map<String, String>();
        Map<String, String> myMapThree = new Map<String, String>();
        test.startTest();
        Id Account_RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        myMapOne =  spc_PicklistDescriber.describeAsMap('Account', Account_RecordTypeId, 'Type');
        myMapThree = spc_PicklistDescriber.describeAsMap('Account', Account_RecordTypeId, '');
        myMapTwo =  spc_PicklistDescriber.describeAsMap('Account', Account_RecordTypeId, 'PatientConnect__PC_Sub_Type__c', 'Type');
        test.stopTest();
    }
}
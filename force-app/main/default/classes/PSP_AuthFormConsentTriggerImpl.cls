/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the trigger Implementation auth consent trigger
*  @date            09/18/2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
deloitte           nov 21, 2019     Initial Version
****************************************************************************************************************/
public with sharing class  PSP_AuthFormConsentTriggerImpl {
    /* string for object name Account */
    final string ACCOUNT_OBJ_NAME = PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.OBJ_NAME_ACCOUNT);
    /* string for object name Conatct */
    final string CONTACT_OBJ_NAME = PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.OBJ_NAME_CONTACT);
    /* string for object name Individual */
    final string IND_OBJ_NAME = PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.OBJ_NAME_INDIVIDUAL);
    /* string for object name User */
    final string USER_OBJ_NAME = PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.OBJ_NAME_USER);
    /* string for object name Program Enrollee */
    final string PRG_ENRLLEE_OBJ = PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.OBJ_NAME_CPE);
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 09/17/2019
    * @Description : validation rule
    * @Param       :AUTH FORM CONSENT
    * @Return      : Void
    ***************************************************************************************/
    public void validateParty(List<AuthorizationFormConsent> cslst) {
        final  Set<Id> accLst=new Set<Id>();
        final  Set<Id> ccLst=new Set<Id>();
        final Map<Id,AuthorizationFormConsent> authcnsntToId = new Map<Id,AuthorizationFormConsent>();
        
        for(AuthorizationFormConsent cs:cslst) {
            if(cs.ConsentGiverId.getSObjectType().getDescribe().getName().equals(IND_OBJ_NAME)||
               cs.ConsentGiverId.getSObjectType().getDescribe().getName().equals(USER_OBJ_NAME)) {
                   cs.addError(system.label.PSP_Party_Err_Msg);
               } else if ( !cs.ConsentGiverId.getSObjectType().getDescribe().getName().equals(PRG_ENRLLEE_OBJ)
                         &&  cs.ConsentGiverId.getSObjectType().getDescribe().getName().equals(ACCOUNT_OBJ_NAME)) {
                             accLst.add(cs.ConsentGiverId);
               } else {
                ccLst.add(cs.ConsentGiverId);
            }
            authcnsntToId.put(cs.ConsentGiverId,cs);
        }
        if(!accLst.isEmpty()) {
            validationforaccountContact(accLst,authcnsntToId,ACCOUNT_OBJ_NAME);
        }
        if(!ccLst.isEmpty()) {
            validationforaccountContact(ccLst,authcnsntToId,CONTACT_OBJ_NAME);
        }
    }
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 09/17/2019
    * @Description : validation rule
    * @Param       : AUTH FORM CONSENT
    * @Return      : Void
    ***************************************************************************************/
    public void validationforaccountContact(Set<Id> idlst,Map<Id,AuthorizationFormConsent> obMap,String obname) {
        final String query='select Recordtype.Developername,recordtypeId,Id from '+ + String.escapeSingleQuotes(obname)+' where id in: idlst';
        if(obname.equals(ACCOUNT_OBJ_NAME)) {
            for (Account ob: Database.query(query)) {               
                if(ob.recordtypeId!=PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.ACCOUNT_RT_PATIENT, Account.getSObjectType()) && 
                   ob.recordtypeId!=PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.ACCOUNT_RT_CAREGIVER, Account.getSObjectType())) {
                       obMap.get(ob.Id).addError(system.label.PSP_Party_Err_Msg);
                }
            }
        }
        if(obname.equals(CONTACT_OBJ_NAME)) {
            for (Contact ob: Database.query(query)) {              
                if(ob.recordtypeId!=PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CONTACT_RT_HCP, Contact.getSObjectType())) {
                    obMap.get(ob.Id).addError(system.label.PSP_Party_Err_Msg);
                }
            }
        }
        
    }
    
}
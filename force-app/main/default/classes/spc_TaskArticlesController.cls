/*********************************************************************************

    *  @author          Deloitte
    *  @description     class created to get knowledge articles that are related categories within the Task.
    *  @date            07-July-18
    *  @version         1.0
*********************************************************************************/
public class spc_TaskArticlesController {

    /********************************************************************************************************
    *  @author           Deloitte
    *  @date             07-July-18
    *  @description      Retrieves knowledge articles that are related to task categories
    *  @param            pageState - Map with field values from page
    *  @return           List of knowledge articles
    *********************************************************************************************************/
    @AuraEnabled
    public static List<KnowledgeArticleVersion> getArticlesByTaskId(ID taskId) {
        List<KnowledgeArticleVersion> kbList = new List<KnowledgeArticleVersion>();
        List<Task> currentTask = [SELECT Id, PatientConnect__PC_Category__c FROM Task where Id = :taskId LIMIT 1];

        if (!currentTask.isEmpty() && (String.isNotBlank(currentTask[0].PatientConnect__PC_Category__c))) {
            spc_DataCategories__c dcObj = spc_DataCategories__c.getInstance(currentTask[0].PatientConnect__PC_Category__c);

            if (dcObj != null) {
                string articleQuery = 'SELECT Id, Title, ArticleNumber, Summary, KnowledgeArticleId, LastPublishedDate ' +
                                      'FROM KnowledgeArticleVersion ' +
                                      'WHERE PublishStatus = \'Online\'' +
                                      ' WITH DATA CATEGORY Sage_Knowledge__c AT ' + dcObj.spc_DataCategoryAPIName__c;

                kbList = Database.query(articleQuery);
            }
        }
        return kbList;
    }
}
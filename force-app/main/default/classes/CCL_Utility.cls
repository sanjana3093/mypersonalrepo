/********************************************************************************************************
*  @author          Deloitte
*  @description     A general class containing method which can be called out to perform certain function.
*  @param           
*  @date            June 19, 2020
*********************************************************************************************************/
public with sharing class CCL_Utility {
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Method to get the site time zones to be displayed with date time fields on flow screens
    *  @param           String screen name
    *  @param 			Id recordId of the ADF or PRF flow
    *  @return          Map of field API name and applicable time-zone.
    *  @date            June 19, 2020
    *********************************************************************************************************/
    public static Map<String, String> getSiteTimeZone(String screenName, Id recId) {
        /* Map of field api name and it's respective time zone */
        final Map<String, String> fieldAndTimeZoneMap = new Map<String, String> ();
        final String sObjName = recId.getSObjectType().getDescribe().getName();
        if (CCL_Date_Time_Site_Mapping__mdt.sObjectType.getDescribe().isAccessible()) {
              /* Map of time field API name and Site field API name */
              final Map<String, String> timefieldsAndSites = new Map<String, String>();
              /* Custom metadata for site time mapping is being iterated */
              for(CCL_Date_Time_Site_Mapping__mdt siteTime :  [select CCL_Date_Time_field_API__c,CCL_Date_Time_Text_API__c,CCL_Object_API__c,CCL_Screen_Name__c,CCL_Site__c,CCL_Site_API_name__c
                                                                 from CCL_Date_Time_Site_Mapping__mdt where CCL_Screen_Name__c = :screenName]) {
                                                                     timefieldsAndSites.put(siteTime.CCL_Date_Time_Text_API__c,siteTime.CCL_Site_API_name__c);
            }
            /* Map of Site record id and site field api name */
            final Map<String,Id> siteIdAndNames = new Map<String,Id>();
            final List<String> siteFieldsLst = new List<String> ();
            String queryFields = '';
            if (CCL_Site_Fields_per_Object__mdt.sObjectType.getDescribe().isAccessible()) {
                for(CCL_Site_Fields_per_Object__mdt siteField : [Select CCL_Object_API__c, CCL_Site_Fields__c from CCL_Site_Fields_per_Object__mdt where CCL_Object_API__c=:sObjName LIMIT 1]) {
                    queryFields = siteField.CCL_Site_Fields__c;
                    siteFieldsLst.addAll(queryFields.split(','));
                }
            }
             /* Checking for object Site fields */
            for(sObject objRec : Database.query('Select Id,' + String.escapeSingleQuotes(queryFields) + ' FROM ' + String.escapeSingleQuotes(sObjName) + ' WHERE id='+'\'' + String.escapeSingleQuotes(recId) + '\''+ ' WITH SECURITY_ENFORCED')) {
               // for(sObject objRec : Database.query('Select Id,' + queryFields + ' FROM ' + sObjName + ' WHERE id='+'\'' + recId + '\''+ ' WITH SECURITY_ENFORCED')) {
                    for(String field : siteFieldsLst) {
                        Id siteId = (Id)objRec.get(field);
                        siteIdAndNames.put(field,siteId);        
                    }
                }
            if(!siteIdAndNames.isEmpty()) {
                /* Iterating Account object to find the time zones associated with the respective sites */
                for(Account siteTimezone : [Select id, CCL_Time_Zone__c, CCL_Time_Zone__r.name,CCL_Time_Zone__r.CCL_Saleforce_Time_Zone_Key__c,CCL_Time_Zone__r.CCL_SAP_Time_Zone_Key__c from Account where id in :siteIdAndNames.values() WITH SECURITY_ENFORCED]) {
                    for(String dtAPI : timefieldsAndSites.KeySet()) {
                        for(String siteField: siteIdAndNames.KeySet()){
                        if(timefieldsAndSites.get(dtAPI).equalsIgnoreCase(siteField)&& siteTimezone.Id==siteIdAndNames.get(siteField)) {
                            fieldAndTimeZoneMap.put(dtAPI,siteTimezone.CCL_Time_Zone__r.CCL_SAP_Time_Zone_Key__c);
                        }
                        }
                    }                    
                }
            }
        }
        return fieldAndTimeZoneMap;
    }
   /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Method to get the site time zones to be displayed with date time fields on flow screens
    *  @param           String screen name
    *  @param                                  Id recordId of the ADF or PRF flow
    *  @return          Map of field API name and applicable time-zone.
    *  @date            June 19, 2020
    *********************************************************************************************************/
    public static Map<String, String> getSiteTimeZoneID(String screenName, Id recId) {
        /* Map of field api name and it's respective time zone */
        final Map<String, String> fieldAndTimeZoneMap = new Map<String, String> ();
        final String sObjName = recId.getSObjectType().getDescribe().getName();
        if (CCL_Date_Time_Site_Mapping__mdt.sObjectType.getDescribe().isAccessible()) {
              /* Map of time field API name and Site field API name */
              final Map<String, String> timefieldsAndSites = new Map<String, String>();
              /* Custom metadata for site time mapping is being iterated */
              for(CCL_Date_Time_Site_Mapping__mdt siteTime :  [select CCL_Date_Time_field_API__c,CCL_Date_Time_Text_API__c,CCL_Object_API__c,CCL_Screen_Name__c,CCL_Site__c,CCL_Site_API_name__c
                                                                 from CCL_Date_Time_Site_Mapping__mdt where CCL_Screen_Name__c = :screenName]) {
                                                                     timefieldsAndSites.put(siteTime.CCL_Date_Time_Text_API__c,siteTime.CCL_Site_API_name__c);
            }
            /* Map of Site record id and site field api name */
            system.debug('timezonemap'+timefieldsAndSites);
            final Map<String,Id> siteIdAndNames = new Map<String,Id>();
            final List<String> siteFieldsLst = new List<String> ();
            String queryFields = '';
            if (CCL_Site_Fields_per_Object__mdt.sObjectType.getDescribe().isAccessible()) {
                for(CCL_Site_Fields_per_Object__mdt siteField : [Select CCL_Object_API__c, CCL_Site_Fields__c from CCL_Site_Fields_per_Object__mdt where CCL_Object_API__c=:sObjName LIMIT 1]) {
                    queryFields = siteField.CCL_Site_Fields__c;
                    siteFieldsLst.addAll(queryFields.split(','));
                }
            }
            system.debug('Site field list'+siteFieldsLst);
             /* Checking for object Site fields */
            for(sObject objRec : Database.query('Select Id,' + String.escapeSingleQuotes(queryFields) + ' FROM ' + String.escapeSingleQuotes(sObjName) + ' WHERE id='+'\'' + String.escapeSingleQuotes(recId) + '\''+ ' WITH SECURITY_ENFORCED')) {
               // for(sObject objRec : Database.query('Select Id,' + queryFields + ' FROM ' + sObjName + ' WHERE id='+'\'' + recId + '\''+ ' WITH SECURITY_ENFORCED')) {
                    for(String field : siteFieldsLst) {
                        Id siteId = (Id)objRec.get(field);
                        siteIdAndNames.put(field,siteId);        
                    }
                }
           system.debug('siteidnameeee'+siteIdAndNames);
            if(!siteIdAndNames.isEmpty()) {
                /* Iterating Account object to find the time zones associated with the respective sites */
                for(Account siteTimezone : [Select id, CCL_Time_Zone__c, CCL_Time_Zone__r.name,	CCL_Time_Zone__r.CCL_Saleforce_Time_Zone_Key__c from Account where id in :siteIdAndNames.values() WITH SECURITY_ENFORCED]) {
                    system.debug('Sitetimezone valuesss'+siteTimezone);
                    for(String dtAPI : timefieldsAndSites.KeySet()) {
                        for(String siteField: siteIdAndNames.KeySet()){
                        if(timefieldsAndSites.get(dtAPI).equalsIgnoreCase(siteField) && siteTimezone.Id==siteIdAndNames.get(siteField)) {
                            system.debug('Valuessssstime field'+dtAPI+'****'+siteField+'****'+timefieldsAndSites.get(dtAPI)+'&&&&'+siteTimezone.CCL_Time_Zone__r.name);
                            fieldAndTimeZoneMap.put(dtAPI,siteTimezone.CCL_Time_Zone__r.CCL_Saleforce_Time_Zone_Key__c);
                        }
                        }
                    }                    
                }
            }
            system.debug('fieldtimezonemapppp'+fieldAndTimeZoneMap);
        }
        return fieldAndTimeZoneMap;
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     General method to get dateTime values for dateTimefields in GMT (offset from input timezone)
    *  @param           Map<String, String> dtFieldTZ
    *  @param           Map<String, DateTime>
    *  @return          DateTime
    *  @date            July 15, 2020
    *********************************************************************************************************/
	public static Map<String, DateTime> getDateTimeInGMT (Map<String, String> dtFieldTZ) {
        final Map<String, DateTime> fieldApiDateTimeMap = new Map<String, DateTime>();
        for(String  key : dtFieldTZ.keySet()) {
			fieldApiDateTimeMap.put(key, getDateTimeValue(key, dtFieldTZ.get(key)));
		}
		return fieldApiDateTimeMap;
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     General method to take date as string as an input and timezone and returns date in gmt
    *  @param           dateTimeVal (YYYY-MM-DD hh:mm:ss) String
    *  @param           timezoneVal String
    *  @return          DateTime
    *  @date            July 15, 2020
    *********************************************************************************************************/
    public static DateTime getDateTimeValue(String dateTimeVal, String timeZoneVal) {
        // Adjusted date time to be returned to the end user
        DateTime adjustedDateTime;
        if(dateTimeVal!=null && timeZoneVal!=null){
        DateTime inputDateTime = DateTime.valueofGmt(dateTimeVal);
        TimeZone inputTZ = TimeZone.getTimeZone(timeZoneVal);
        //this helps in factoring the daylight saving time if applicable to the timezone
        TimeZone siteTimeZone = TimeZone.getTimeZone(inputTZ.getID());
        //offset value from GMT time
        integer offset = siteTimeZone.getOffset(inputDateTime);
		adjustedDateTime = inputDateTime.addMinutes(- (offset) / (1000 * 60));
        }
        return adjustedDateTime;
	}
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     General method to take date as an input and
                        return date in String with the Format derived from CCL_GeneralSetting__c
    *  @param           Date
    *  @return          String
    *  @date            June 19, 2020
    *********************************************************************************************************/
    public static String getFormattedDate(Date inputDate) {
        String strFormattedDate = null; 
        //generalCustSetting will get the default format stored in custom setting
        
            DateTime inputDateTime = DateTime.newInstance(inputDate.year(), inputDate.month(), inputDate.day());
            strFormattedDate = inputDateTime.format('dd MMM yyyy');
        
        return strFormattedDate;
    }
    
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     General method to take date as an input and
                        return date in Novartis format
    *  @param           Date
    *  @return          List<String>
    *  @date            July 15, 2020
    *********************************************************************************************************/
    public static String getdisplayDateTimeValues(DateTime dtmVal, String tmz) {
            Datetime gmtVal = DateTime.newInstanceGMT(dtmVal.yearGMT(),dtmVal.monthGMT(),dtmVal.dayGMT(),dtmVal.hourGMT(),dtmVal.minuteGmt(),0);
            String formattedVal = gmtVal.format('dd MMM yyyy HH:mm',tmz);            
            return formattedVal;
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     General method to take date, hour and minute as an input and
                        return date in Novartis format
    *  @param           Date
    *  @return          List<String>
    *  @date            July 15, 2020
    *********************************************************************************************************/
    public static List<String> getdisplayDateTimeValues(Date dtVal, Integer hour, Integer minute, String tmz){
            
            List<String> stdatetmVals = new List<String>();
            Datetime gmtVal = DateTime.newInstanceGMT(dtVal.year(),dtVal.month(),dtVal.day(),hour,minute,0);
            stdatetmVals = gmtVal.format('dd-MMM-yyyy HH:mm',tmz).split(' '); 
            
            return stdatetmVals;
    }
	    /********************************************************************************************************
*  @author          Deloitte
*  @description     General method to take date as an input and
                    return date in String with the Format derived from CCL_GeneralSetting__c
*  @param           Date
*  @return          String
*  @date            June 19, 2020
*********************************************************************************************************/
public static String getFormattedTime(Time inputTime) {


String formattedHour = string.valueof(inputTime.hour());
String formattedMin = string.valueof(inputTime.minute());

List<String> hourMin = new List<String>();
hourMin.add(formattedHour);
hourMin.add(formattedMin);

String formattedTime = String.join(hourMin, ':');
    
    return formattedTime;
    }
    /********************************************************************************************************
*  @author          Deloitte
*  @description     General method to take TherapyIds and HospitalIds as an input and
                    return Site TherapyAssocitions
*  @param           Date
*  @return          String
*  @date            Nov 16, 2020
*********************************************************************************************************/
  public static List<CCL_Therapy_Association__c> fetchSiteTherapyAssociations(List<Id> therapyIds,List<Id> siteIds) {
      List<CCL_Therapy_Association__c> therapyAssociations =new List<CCL_Therapy_Association__c>();
    try {
        
          if(CCL_Therapy_Association__c.sObjectType.getDescribe().isAccessible()){
                therapyAssociations=[SELECT CCL_Infusion_Data_Collection__c,CCL_Therapy__c,CCL_Site__c FROM CCL_Therapy_Association__c WHERE 
                                     (CCL_Therapy__c in: therapyIds AND CCL_Site__c in: siteIds AND RecordType.Id = :CCL_StaticConstants_MRC.THERAPY_ASSOCAITION_RECORDTYPE_SITE)]; 
            }
        }
     catch(QueryException ex) {
        System.debug('Exception in CCL_Utility fetchTherapyAssociations'+ex.getMessage());
        throw  ex;
    }
      return therapyAssociations;
}

 /********************************************************************************************************
*  @author          Deloitte
*  @description     General method to take fetch adrress and site name from Address object
                    return CCL_Address__c
*  @param           Date
*  @return          String
*  @date            Nov 16, 2020
*********************************************************************************************************/
    
     @AuraEnabled(cacheable=true)
public static List<CCL_Address__c > fetchAddress(List<Id> accountIds) {
    List<CCL_Address__c > addressList=new List<CCL_Address__c>();
   
    try {
       
        if (CCL_Address__c.sObjectType.getDescribe().isAccessible()) {
            addressList = [select CCL_Complete_Address__c,CCL_Site__r.Name,CCL_Site__c,CCL_Site_Name__c from CCL_Address__c where CCL_Site__c in: accountIds and  CCL_Language__c=:UserInfo.getLanguage()  ];
        }
        Map<Id,CCL_Address__c> mapIdAddress=new Map<Id,CCL_Address__c>();
        set<Id> accountIdSet=new Set<Id>();
    for(CCL_Address__c add :addressList) {
        mapIdAddress.put(add.CCL_Site__c,add);
    }
    for(Id accountId :accountIds) {
        if(!mapIdAddress.containskey(accountId)) {
            accountIdSet.add(accountId);
        }
    }
    if(!accountIdSet.isEmpty()) {
          final List<CCL_Address__c > englisgAddress=[select CCL_Complete_Address__c,CCL_Site__r.Name,CCL_Site__c,CCL_Site_Name__c from CCL_Address__c where CCL_Site__c in: accountIdSet and  CCL_Language__c='en_us'  ];
         if(!englisgAddress.isEmpty()) {
        addressList.addALL(englisgAddress);
   }
    }
   
} catch(QueryException ex) {
   System.debug('Exception '+ex.getMessage());
   throw  ex;
}
    
    return addressList;
}
 @AuraEnabled
    public static Boolean updateContentVersion(List <Id> contentDocIdList) {
        CCL_Document__c docToUpdate = new CCL_Document__c();
        CCL_Document__c theDocument ;
        if (Schema.sObjectType.ccl_document__c.isAccessible()){
            theDocument = [select Id,CCL_New_Doc_Uploaded__c from ccl_document__c 
            where ccl_order__c!=null order by createddate desc limit 1];
        }
        Id docId = theDocument.Id;
        List<ContentVersion> conVrsnList = new list<ContentVersion>([Select Id,CCL_Document_Classification__c,ContentDocumentId from ContentVersion
                                                                   where ContentDocumentId in: contentDocIdList]);
        for (ContentVersion conVer : conVrsnList){
            conVer.CCL_Document_Classification__c = 'Patient Consent';
        }
        
        docToUpdate.Id= docId;
        docToUpdate.CCL_New_Doc_Uploaded__c = true;
        try{
            update conVrsnList;
        }
        catch (exception ex){
            System.debug('Exception!');
        }
         try{
            if(Schema.sObjectType.CCL_Document__c.isUpdateable()){
                update docToUpdate;
            }
        }
        catch (exception ex){
            System.debug('Exception!');
        }
        return true;
        
    }
     @AuraEnabled
    public static String createOrderDocument(String docId) {
        String docOrderId;
        if( docId == null){
        final CCL_Document__c docOrder = new CCL_Document__c(); 
                docOrder.Name = 'Order-Related Documents';
        if(Schema.sObjectType.CCL_Document__c.isCreateable()){
                insert docOrder;
        }
        docOrderId = docOrder.Id;
        }
        if(docId != null){
           	CCL_Order__c ord;
            if(CCL_Order__c.sObjectType.getDescribe().isAccessible()){
                ord = [select id,CCL_Ordering_Hospital__c,CCL_Therapy__c,CCL_Apheresis_Collection_Center__c,CCL_Infusion_Center__c,CCL_Ship_to_Location__c,CCL_Apheresis_Pickup_Location__c from CCL_Order__c order by createddate desc  limit 1];    
            }
            docOrderId=ord.Id;
			Id docRecId = Id.valueOf(docId);
            CCL_Document__c doc;
            if(CCL_Document__c.sObjectType.getDescribe().isAccessible()){
                doc = [select id,CCL_Order__c from CCL_Document__c where Id=:docRecId];    
            }
            doc.CCL_Order__c = ord.Id;
            doc.CCL_Ordering_Hospital__c = ord.CCL_Ordering_Hospital__c;
            doc.CCL_Apheresis_Collection_Center__c = ord.CCL_Apheresis_Collection_Center__c;
            doc.CCL_Apheresis_Pick_up_Location__c=ord.CCL_Apheresis_Pickup_Location__c;
            doc.CCL_Ship_To_Location__c = ord.CCL_Ship_to_Location__c;
            doc.CCL_Infusion_Center__c = ord.CCL_Infusion_Center__c;
            doc.CCL_Therapy__c = ord.CCL_Therapy__c;
            if(Schema.sObjectType.CCL_Document__c.isUpdateable()){
                update doc;   
            }
        }
         return docOrderId;
    }
	 /********************************************************************************************************
*  @author          Deloitte
*  @description    Method to trigger email after a document is loaded on existing order
                    
*  @param           Id
*  @return          Void
*  @date            Jan 15, 2021
*********************************************************************************************************/
     @AuraEnabled
    public static Void updateOrderDocument(String docId) {
        CCL_Document__c docToUpdate = new CCL_Document__c();
       CCL_Document__c theDocument;
        if(CCL_Document__c.sObjectType.getDescribe().isAccessible()){
            theDocument  = [select Id,CCL_New_Doc_Uploaded__c from ccl_document__c where Id = :docId  limit 1];   
        }
         docToUpdate.Id= theDocument.Id;
        docToUpdate.CCL_New_Doc_Uploaded__c = true;
         try{
            if(Schema.sObjectType.CCL_Document__c.isUpdateable()){
                update docToUpdate;   
            }
        }
        catch (exception ex){
            System.debug('Exception!');
        }
    }
	         /********************************************************************************************************
*  @author          Deloitte
*  @description    Method to fetch Consent Files
                    
*  @param           Id
*  @return          Void
*  @date            Jan 15, 2021
*********************************************************************************************************/
 @AuraEnabled(cacheable=true)
public static List<ContentDocumentLink> getConsentFiles(Id recordId) {
    try {
            final List<ContentDocumentLink> finalList=new List<ContentDocumentLink>();
            final Map<String,ContentDocumentLink> mapFile = new Map<String,ContentDocumentLink>();
            List<ContentDocumentLink> fileDetails= new List<ContentDocumentLink>();
        List<ContentVersion> fileVersion= new List<ContentVersion>();
            final Set<Id> userIds=new Set<Id>();
            final Set<Id> updatedUserIds=new Set<Id>();
final Set<Id> fileVrsSet=new Set<Id>();
        final Set<Id> fileIdSet=new Set<Id>();

            if (ContentDocumentLink.sObjectType.getDescribe().isAccessible()) {

                fileDetails = [SELECT Id,ContentDocumentId,ContentDocument.LatestPublishedVersionId, ContentDocument.Title,ContentDocument.FileType,ContentDocument.FileExtension,ContentDocument.CreatedById
                               FROM ContentDocumentLink
                               WHERE LinkedEntityId =:recordId WITH SECURITY_ENFORCED];
            }
        for(ContentDocumentLink link : fileDetails){
            fileIdSet.add(link.ContentDocumentId);
        }
        if (ContentVersion.sObjectType.getDescribe().isAccessible()) {

                fileVersion = [SELECT Id,ContentDocumentId,CCL_Document_Classification__c
                               FROM ContentVersion
                               WHERE FirstPublishLocationId =:recordId and CCL_Document_Classification__c='Patient Consent' and ContentDocumentId=:fileIdSet WITH SECURITY_ENFORCED];
            }
        for(ContentVersion ver : fileVersion){
            fileVrsSet.add(ver.ContentDocumentId);
        }

            for(ContentDocumentLink contentDocumentLinkInstance : fileDetails) {
                final String key = String.valueOf(contentDocumentLinkInstance.ContentDocument.CreatedById) + String.valueOf(contentDocumentLinkInstance.ContentDocumentId);
                userIds.add(contentDocumentLinkInstance.ContentDocument.CreatedById);
                if(fileVrsSet.contains(contentDocumentLinkInstance.ContentDocumentId)){
                	mapFile.put(key,contentDocumentLinkInstance);
                }
            }
            updatedUserIds.addall(userIds);
            for (String key : mapFile.keySet()) {
                final ContentDocumentLink documents = mapFile.get(key);
                if(updatedUserIds.contains(documents.ContentDocument.CreatedById)) {
                    finalList.add(documents);
                }
            }

           return finalList;

    } catch(QueryException ex) {
        throw  ex;
    }
}

}

/**
* @author Deloitte
* @date 07/18/2018
* @description This is the test class for spc_Database
*/
@IsTest
public class spc_DatabaseTest {
    @testSetup static void setupTestdata() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
        Account patientAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        patientAcc1.PatientConnect__PC_Date_of_Birth__c = system.today();
        patientAcc1.spc_HIPAA_Consent_Received__c = 'Yes';
        patientAcc1.spc_Patient_HIPAA_Consent_Date__c = System.today();
        patientAcc1.spc_Patient_Services_Consent_Received__c = '';
        patientAcc1.spc_Text_Consent__c = '';
        patientAcc1.spc_Patient_Mrkt_and_Srvc_consent__c = '';
        insert patientAcc1;
        system.assertNotEquals(patientAcc1, NULL);
        Account manf = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId());
        insert manf;
        system.assertNotEquals(manf, null);
        PatientConnect__PC_Engagement_Program__c eg = spc_Test_Setup.createEngagementProgram('testEP', manf.id);
        insert eg;
        Case programCaseRec2 = spc_Test_Setup.createCases(new List<Account> {patientAcc1}, 1, 'PC_Program').get(0);
        if (null != programCaseRec2) {
            programCaseRec2.Type = 'Program';
            programCaseRec2.PatientConnect__PC_Engagement_Program__c = eg.Id;
            insert programCaseRec2;
        }
        system.assertNotEquals(programCaseRec2, NULL);
    }
    public static testmethod void testExecution1() {
        List<sObjectField> lstFields = new List<sObjectField>();
        Account acc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        acc.name = 'testOne';
        List<Account> lstAcc = new List<Account>();
        lstAcc.add(acc);
        spc_Database.ins(lstAcc, lstFields);
        system.assertNotEquals(lstAcc.size(), NULL);
        Account acc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        acc1.name = 'testTwo';
        List<Account> lstAcc1 = new List<Account>();
        lstAcc1.add(acc1);
        spc_Database.ins(lstAcc1);
        system.assertNotEquals(lstAcc1.size(), NULL);
        Account acc2 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        acc2.name = 'testThree';
        List<Account> lstAcc2 = new List<Account>();
        lstAcc2.add(acc2);
        spc_Database.ins(lstAcc2, true);
        system.assertNotEquals(lstAcc2.size(), NULL);
        Account acc3 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        acc3.name = 'testFour';
        spc_Database.ins(acc3, lstFields, true);
        Account acc4 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        acc4.name = 'testFive';
        spc_Database.ins(acc4, lstFields);
        Account acc5 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        acc5.name = 'testSix';
        spc_Database.ins(acc5);
        Account acc6 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        acc6.name = 'testSeven';
        spc_Database.ins(acc6, true);
        String str = 'error';
        List<Account> lstSObject = [select id from Account LIMIT 1];
        List<Case> lstCase = [select id from Case LIMIT 1];
        lstSObject[0].name = 'abc';
        test.startTest();
        spc_Database.upd(lstSObject[0], lstFields, true);
        spc_Database.upd(lstSObject[0], true);
        spc_Database.upd(lstSObject[0], lstFields);
        spc_Database.upd(lstSObject, lstFields);
        spc_Database.upd(lstSObject, true);
        spc_Database.upd(lstSObject[0]);
        spc_Database.ups(lstSObject, lstFields, true);
        spc_Database.ups(lstSObject);
        spc_Database.ups(lstSObject, true);
        spc_Database.ups(lstSObject[0], lstFields);
        spc_Database.ups(lstSObject[0]);
        spc_Database.ups(lstSObject, lstFields);
        spc_Database.del(lstCase, true);
        spc_Database.processError(str);
        test.stopTest();
    }
    public static testmethod void testExecution2() {
        List<Case> lstCase = [select id from Case LIMIT 1];
        spc_Database.del(lstCase[0]);
    }
    public static testmethod void testExecution3() {
        List<string> lstFields = new List<string>();
        lstFields.add('Name');
        spc_Database.chkAccess('Account', Null, lstFields);
        List<Account> lstPat = [select id from Account LIMIT 1];
        system.assertNotEquals(lstPat.size(), NULL);
        spc_Database.assertAccess(lstPat, NULL, lstFields);
        spc_Database.assertAccess('Account', NULL, lstFields);
        String SOQL = 'select id from Account';
        spc_Database.queryWithAccess(SOQL, lstFields);
        spc_Database.queryWithAccess(SOQL, lstFields, true);
        spc_Database.queryWithAccess(SOQL, lstFields, false);
        spc_Database.queryWithAccess(SOQL, true);
    }
}
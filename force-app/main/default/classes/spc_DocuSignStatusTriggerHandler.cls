/**
* @author Deloitte
* @date July 03, 2018
*
* @description spc_DocuSignStatusTriggerHandler - Trigger handler class for DocuSign object
*/

public class spc_DocuSignStatusTriggerHandler extends spc_TriggerHandler {
	spc_DocuSignStatusTriggerImplementation trgImpl = new spc_DocuSignStatusTriggerImplementation();

	public override void beforeInsert() {
		//Find related Enevelop for docuSignStatus
		Map<String, dsfs__DocuSign_Status__c> mapDocuSignStatus = new Map<String, dsfs__DocuSign_Status__c>();
		for (dsfs__DocuSign_Status__c dstatus : (List<dsfs__DocuSign_Status__c>)Trigger.New) {
			if (String.isNotBlank(dstatus.dsfs__DocuSign_Envelope_ID__c)) {
				mapDocuSignStatus.put(dstatus.dsfs__DocuSign_Envelope_ID__c.toLowerCase(), dstatus);
			}
		}

		if (! mapDocuSignStatus.isEmpty()) {
			//Create PC_Document- Email boutbound record
			trgImpl.processStatus(mapDocuSignStatus, RecordTypeName.DOC_RT_EMAIL_OUTBOUND, PicklistValue.DOC_STATUS_SENTDOCUSIGN);
		}
	}

	public override void beforeUpdate() {
		//Find related Enevelop for docuSignStatus
		Map<String, dsfs__DocuSign_Status__c> mapDocuSignStatus = new Map<String, dsfs__DocuSign_Status__c>();
		Map<Id, dsfs__DocuSign_Status__c> oldMap = (Map<Id, dsfs__DocuSign_Status__c>)Trigger.oldMap;
		for (dsfs__DocuSign_Status__c dstatus : (List<dsfs__DocuSign_Status__c>)Trigger.New) {
			if (String.isNotBlank(dstatus.dsfs__DocuSign_Envelope_ID__c)
			        && dstatus.dsfs__Envelope_Status__c != oldMap.get(dstatus.Id).dsfs__Envelope_Status__c
			        && dstatus.dsfs__Envelope_Status__c == spc_ApexConstants.getValue(PicklistValue.DOCUSIGN_STATUS_SIGNED)) {
				mapDocuSignStatus.put(dstatus.dsfs__DocuSign_Envelope_ID__c.toLowerCase(), dstatus);
			}
		}

		if (! mapDocuSignStatus.isEmpty()) {
			//Create email inbound document
			trgImpl.processStatus(mapDocuSignStatus, RecordTypeName.DOC_RT_EMAIL_INBOUND, PicklistValue.DOC_STATUS_REVIEW_NEEDED);
		}
	}
}
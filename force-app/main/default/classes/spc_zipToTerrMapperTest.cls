/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for spc_zipToTerrMapper
    *  @date            06/18/2018
    *  @version         1.0
*********************************************************************************************************/
@isTest
public class spc_zipToTerrMapperTest {
    private static Account hcoAcc;
    private static PatientConnect__PC_Address__c addr;
    @testSetup
    static void setup() {
        Territory terr1 = new Territory();
        terr1.name = 'MSL';
        terr1.ParentTerritoryid = null;
        insert terr1;

        Territory terr2 = new Territory();
        terr2.name = 'RG';
        terr2.ParentTerritoryid = terr1.id;
        terr2.DeveloperName = 'Yonoz';
        insert terr2;

        Territory terr3 = new Territory();
        terr3.name = 'RG';
        terr3.DeveloperName = 'Xonoz';
        terr3.ParentTerritoryid = terr2.id;
        insert terr3;

        UserRole usRole = new UserRole();
        usRole.name = 'MSL';
        Insert usRole;

        User oUser = spc_Test_Setup.getProfileID();
        UserRole oUserRole = [select id from UserRole where name = 'MSL'];
        oUser.userRoleId = oUserRole.id;
        insert oUser;

    }

    public static testmethod void testExecution() {
        test.startTest();
        dataTest();
        Set<String> accountIds = new set<string>();
        accountIds.add(hcoAcc.Id);
        Set<Id> addrIdsSet = new set<Id>();
        addrIdsSet.add(addr.id);
        // Added
        String caseManagerRole = spc_ApexConstants.PROFILE_CASE_MANAGER;
        string salesRepRole = spc_ApexConstants.PROFILE_SALES_REP;
        Zip_to_Terr_vod__c oterrzip = [select Territory_vod__c, Zip_ID_vod__c, name from Zip_to_Terr_vod__c where Territory_vod__c != null ];
        system.assertNotEquals(oterrzip, NULL);
        spc_ZipToTerrMapper zipTerr = new spc_ZipToTerrMapper((new Set<String> {caseManagerRole, salesRepRole}));
        Map<String, Map<String, String>> mapUsers = new Map<String, Map<String, String>>();
        mapUsers = zipTerr.getUsers(accountIds);
        test.stopTest();
    }

    public Static void dataTest() {
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        insert apexConstansts;

        list<spc_Patient_Service_Region_Mapping__mdt> omdt = [SELECT spc_Region_Namespace__c, spc_User_Role__c
                FROM spc_Patient_Service_Region_Mapping__mdt];
        Account acc = new Account();
        acc = spc_Test_Setup.createTestAccount('PC_Patient');
        acc.PatientConnect__PC_Email__c = 'test@email.com';
        insert acc;
        //Create a HCO account
        hcoAcc = new Account();
        hcoAcc = spc_Test_Setup.createTestAccount('PC_Physician');
        hcoAcc.name = 'physician';
        insert hcoAcc;
        system.assertNotEquals(hcoAcc, Null);

        addr = new PatientConnect__PC_Address__c();
        addr.PatientConnect__PC_Account__c = hcoAcc.id;
        addr.PatientConnect__PC_Primary_Address__c = true;
        addr.PatientConnect__PC_Zip_Code__c = '000000';
        addr.PatientConnect__PC_Address_1__c = 'xyz';
        addr.PatientConnect__PC_City__c = 'abc';
        insert addr;
        system.assertNotEquals(addr, Null);

        Zip_to_Terr_vod__c zipToTerr = new Zip_to_Terr_vod__c();
        zipToTerr.Territory_vod__c = 'RG';
        zipToTerr.Zip_ID_vod__c = '000000';
        zipToTerr.Name = '000000';
        insert zipToTerr;
        system.assertNotEquals(zipToTerr, Null);
    }
}
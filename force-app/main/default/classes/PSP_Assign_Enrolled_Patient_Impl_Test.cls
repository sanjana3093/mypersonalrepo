/*********************************************************************************************************
class Name      : PSP_PurchaseTransaction_Impl_Test 
Description		: Test class for purchase transaction trigger related classes
@author		    : Saurabh Tripathi
@date       	: December 03, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi           December 03, 2019          Initial Version
****************************************************************************************************************/
@isTest
public class PSP_Assign_Enrolled_Patient_Impl_Test {
 
     /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 12/03/2019
  	* @Description : Test Method for before insert for PSP_Assign_Enrolled_Patient.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/
    private static testmethod void insertWithConsistency() {
        final Contact con= PSP_Test_Setup.createContact();
       Database.insert(con);
        final CareProgramEnrollee cpe= PSP_Test_Setup.createProgramEnrollee();
        cpe.PSP_Enrolled_Physician__c=con.id;
            try {
            Database.insert(cpe);  
            } catch (DmlException e) {
			System.assert ( e.getMessage().contains('Insert failed.'),e.getMessage() );
		} 
    }
      /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 12/03/2019
  	* @Description : Test Method for before insert for PSP_Assign_Enrolled_Patient.
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/
    private static testmethod void insertWithInConsistency() {
       final Contact con= PSP_Test_Setup.createContact();
       Database.insert(con);
        final CareProgramEnrollee cpe= PSP_Test_Setup.createProgramEnrollee();
            try {
            Database.insert(cpe);  
            } catch (DmlException e) {
			System.assert ( e.getMessage().contains('Insert failed.'),e.getMessage() );
		} 
    }

   
}
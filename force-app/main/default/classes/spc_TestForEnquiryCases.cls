/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Test class for All Enquiry Related Test Classes
    *  @date            06/06/2018
    *  @version         1.0
*********************************************************************************************************/
@isTest
public class spc_TestForEnquiryCases {
    public static Id ID_PATIENT_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)).getRecordTypeId();
    public static Id ID_EMAIL_INBOUND_RECORDTYPE = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_EMAIL_INBOUND)).getRecordTypeId();

    @testSetup static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

    }
    /********************************************************************************************************
        *  @author          Deloitte
        *  @description     To Create Inquiry Case
        *  @date            06/06/2018
        *  @version         1.0
        *  @return          void
    *********************************************************************************************************/
    @isTest
    static void createInquiryCase() {

        System.runAs(spc_Test_Setup.getProfileID()) {
            Test.startTest();
            Id accountRTypeId = ID_PATIENT_RECORDTYPE;
            Account manAcc = spc_Test_Setup.createAccount(accountRTypeId);
            spc_Database.ins(manAcc);

            System.assertNotEquals(manAcc, null);

            PatientConnect__PC_Address__c address = spc_Test_Setup.createAddress(manAcc.Id);
            spc_Database.ins(address);

            manAcc.PatientConnect__PC_Primary_Address__c =  address.Id;
            spc_Database.upd(manAcc);

            accountRTypeId = ID_EMAIL_INBOUND_RECORDTYPE;
            PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c();
            doc.RecordTypeId = accountRTypeId;
            spc_Database.ins(doc);

            System.assertNotEquals(doc, null);

            Case inquiryCase = spc_Test_Setup.createInquiryCase(manAcc.ID, doc.Id);
            spc_Database.ins(inquiryCase);

            System.assertNotEquals(inquiryCase, null);

            Test.stopTest();
        }
    }
}
public with sharing class CCL_FinishedProductTriggerAccessor 
{
     public static List<CCL_Shipment__c> fetchShipmentRecordsToUpdateExpiryDate(set<Id> shipmentIdList)
     {
         
         List<CCL_Shipment__c> shipmentRecordList= new List<CCL_Shipment__c>();
         try{
        if (!shipmentIdList.isEmpty()  && Schema.sObjectType.CCL_Shipment__c.isAccessible()){
         shipmentRecordList=[select Id, CCL_Expiry_Date_of_Shipped_Bags__c,
                            (select id,CCL_Expiry_Date__c from Finished_Products__r 
                             order by CCL_Expiry_Date__c NULLS LAST limit 1) from CCL_Shipment__c where Id IN:shipmentIdList WITH SECURITY_ENFORCED];
        }
         }
         catch (System.QueryException qe){
       System.debug('An exception occurred with fetchShipmentRecord: ' + qe.getMessage());
    }
         
            return shipmentRecordList;
            
     }
    public static List<CCL_Shipment__c> fetchShipmentRecordsToUpdateNoOfDoses(set<Id> shipmentIds)
     {
         List<CCL_Shipment__c> shipmentList = new List<CCL_Shipment__c>();
    try{
        if (!shipmentIds.isEmpty()  && Schema.sObjectType.CCL_Shipment__c.isAccessible()){
           shipmentList = [select Id, CCL_Number_of_Doses_Shipped__c,(select id from Finished_Products__r) from CCL_Shipment__c where Id IN:shipmentIds  WITH SECURITY_ENFORCED];
       
        }
    }catch (System.QueryException qe){
       System.debug('An exception occurred with fetchShipmentRecord: ' + qe.getMessage());
    }
      return shipmentList;
     }
    
     public static void updateShipmentRecords(List<CCL_Shipment__c> shipmentRecordListToBeUpdated)
    {
       if(Schema.sObjectType.CCL_Shipment__c.isUpdateable()) {
            update shipmentRecordListToBeUpdated;
          }
    }

}
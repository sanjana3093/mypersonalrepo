@isTest public class PSP_AccountControllerTest {
public
  PSP_AccountControllerTest() {}
  static testMethod void testPatientList() {
    List<Account> accounts = new List<Account>();
    for (Integer count = 0; count < 50; count++) {
      accounts.add(
          new Account(Name = 'Test' + count, Phone = '00000000' + count));
    }

    insert accounts;

    Test.startTest();
    PSP_AccountController.getAllPatients();
    Test.stopTest();
    Account ACC = [select phone from Account LIMIT 1];
    System.assertEquals(Integer.valueOf(acc.phone), 00000000);
  }
}
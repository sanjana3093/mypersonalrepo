/*********************************************************************************************************
* @author Deloitte
* @date July 7,2018
* @description This handles operation related to DML operations of association
****************************************************************************************************************/
public class spc_AssociationTriggerImplementation {
    /*********************************************************************************
    Method Name    : populateMarketAccessOnCase
    Developer      : Samiksha
    Description    :
    Return Type    : Void
    *********************************************************************************/
    public static void populateMarketAccessOnCase(List<PatientConnect__PC_Association__c> lstAssociations) {
        //Prepare Map for PayerIds to Case Ids
        Map<String, Set<String>> mapPayerToCases = new Map<String, Set<String>>();
        for (PatientConnect__PC_Association__c asoc : lstAssociations) {
            spc_Utility.addToMap(mapPayerToCases , asoc.PatientConnect__PC_Account__c , asoc.PatientConnect__PC_Program__c, new Set<String>());
        }
        spc_ZipToTerrMapper zipTerrMapping = new spc_ZipToTerrMapper((new Set<String> {spc_ApexConstants.getCareTeamRoles(spc_ApexConstants.CareTeamRoles.CARETEAM_ROLE_MARKETACCESS)}));
        //Find Cases for this user's role and Profile
        Map<String, Map<String, String>> mapAccountIdsToUserMapping = zipTerrMapping.getUsers(mapPayerToCases.keySet());
        Map<String, spc_Extended_Care_Team__c> mapExtendedCareTeam = new Map<String, spc_Extended_Care_Team__c>();
        for (String payerId : mapPayerToCases.keySet()) {
            if (mapAccountIdsToUserMapping.containsKey(payerId)) {
                Map<String, String> mapUsers = mapAccountIdsToUserMapping.get(payerId);
                for (String regionRole : mapUsers.keySet()) {
                    string userId = mapUsers.get(regionRole);
                    for (String caseId : mapPayerToCases.get(payerId)) {
                        spc_Extended_Care_Team__c careTeam = new spc_Extended_Care_Team__c();
                        careTeam.spc_User__c = userId;
                        careTeam.spc_Program_Case__c  = caseId;
                        //If User Profile is "Case Manager"
                        if ( regionRole == spc_ApexConstants.getCareTeamRoles(spc_ApexConstants.CareTeamRoles.CARETEAM_ROLE_MARKETACCESS)) {
                            careTeam.spc_Role__c = spc_ApexConstants.getCareTeamRoles(spc_ApexConstants.CareTeamRoles.CARETEAM_ROLE_MARKETACCESS);
                            mapExtendedCareTeam.put(careTeam.spc_Role__c + '|' + careTeam.spc_Program_Case__c, careTeam );
                        }
                    }
                }
            }
        }
        if (! mapExtendedCareTeam.isEmpty()) {
            insert mapExtendedCareTeam.values();
        }
    }

    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 05/29/2018
    * @Description : Method to populate Patient Consent received on case based on Consent in Account.
    * @Param       : List of Account
    * @Return      : Void
    ***************************************************************************************/
    public void createHCPLogisticTasks(Map<Id, Id> accountToCaseMap) {
        Set<Id> filteredCases = new Set<ID>();
        Set<Id> finalProgramIds  = new Set<Id>();
        Set<Id> filteredProgramIds = new Set<Id>();
        String subcategory_firstattempt = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_CAT_FIRST_ATTEMPT);
        String category_soclogisticscall = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_SOC_LOGISTIC_CALL);
        String priority_normal = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_PRIORITY_NORMAL);
        String channel_phone = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CHANNEL_PHONE);
        String direction_outbound = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_DIR_OUTBOUND);
        String status_notstarted = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_NOT_STARTED);
        Id programRTId = spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM, Case.SobjectType);
        String completedStatus = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_STATUS_COMPLETED);
        String neverStartStatus = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_STATUS_NEVER_START);
        spc_TaskTriggerImplementation triggerImplementation = new spc_TaskTriggerImplementation();

        for (Account newAcc : [ SELECT Id, spc_REMS_Certification_Status__c FROM Account where ID IN:accountToCaseMap.keySet() AND spc_REMS_Certification_Status__c = :spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ACCOUNT_REMS_CERTIFIED_STATUS) ]) {
            filteredCases.add(accountToCaseMap.get(newAcc.id));
        }

        for (Case newCase : [SELECT ID, spc_HIPAA_Consent_Received__c, PatientConnect__PC_Status_Indicator_3__c FROM CASE WHERE RecordTypeId = :programRTId AND Status != :completedStatus AND Status != :neverStartStatus AND ID IN:filteredCases]) {
            if ((newCase.spc_HIPAA_Consent_Received__c == null || newCase.spc_HIPAA_Consent_Received__c == '' || newCase.spc_HIPAA_Consent_Received__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO))
                    && newCase.PatientConnect__PC_Status_Indicator_3__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.PATIENT_STATUS_INDICATOR_COMPLETE)) {
                finalProgramIds.add(newCase.id);
            }
        }
        if (!finalProgramIds.isEmpty()) {
            triggerImplementation.createFollowUpTasks(finalProgramIds, System.today() + 1, category_soclogisticscall,
                    subcategory_firstattempt, channel_phone, direction_outbound, priority_normal, status_notstarted,
                    category_soclogisticscall);
        }
    }

    /*********************************************************************************
    Method Name    : validateAssociationRoleAndStatus
    Developer      : Sumanta Das
    Description    : This method checks if there is more than one active
                     careteam member with same role for a program case.
    Return Type    : Void
    *********************************************************************************/

    public static void validateAssociationRoleAndStatus(Map<Id, PatientConnect__PC_Association__c> mapProgIdWithAssociations, Boolean fieldChange) {
        List<PatientConnect__PC_Association__c> listExistingActiveAssociation = [SELECT PatientConnect__PC_Role__c, PatientConnect__PC_AssociationStatus__c, PatientConnect__PC_Program__c FROM PatientConnect__PC_Association__c WHERE PatientConnect__PC_AssociationStatus__c = : spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE) AND PatientConnect__PC_Program__c IN: mapProgIdWithAssociations.KeySet()];
        Map<Id, Map<String, PatientConnect__PC_Association__c>> mapCaseIdwithRoleAndAsso = new Map<Id, Map<String, PatientConnect__PC_Association__c>>();
        Map<String, PatientConnect__PC_Association__c> mapExistingActiveAssociationWithProgId = new Map<String, PatientConnect__PC_Association__c>();
        for (PatientConnect__PC_Association__c Association : listExistingActiveAssociation) {
            mapExistingActiveAssociationWithProgId.put(Association.PatientConnect__PC_Role__c, Association);
            mapCaseIdwithRoleAndAsso.put(Association.PatientConnect__PC_Program__c, mapExistingActiveAssociationWithProgId);
        }

        for (PatientConnect__PC_Association__c newAssociation : mapProgIdWithAssociations.Values()) {
            if (mapCaseIdwithRoleAndAsso.containsKey(newAssociation.PatientConnect__PC_Program__c)) {
                Map<String, PatientConnect__PC_Association__c> mapRoleWithAssociation = mapCaseIdwithRoleAndAsso.get(newAssociation.PatientConnect__PC_Program__c);
                if (mapRoleWithAssociation.containsKey(newAssociation.PatientConnect__PC_Role__c)) {
                    if (fieldChange) {
                        newAssociation.addError(Label.spc_AssociationRoleWithStatus);
                    }
                }
            }
        }
    }

    /*********************************************************************************
    Method Name    : updateInteractionsWithInactiveSOC
    Developer      : Deloitte
    Description    : This method is used to update the flag on those interactions which are linked with the SOC being made inactive or deleted
    Return Type    : Void
    *********************************************************************************/
    public void updateInteractionsWithInactiveSOC(List<PatientConnect__PC_Association__c> lstInactiveSOCAssociations) {

        Set<Id> setProgramCaseIds = new Set<Id>();
        Set<Id> setSOCAccountIds = new Set<Id>();
        List<PatientConnect__PC_Interaction__c> lstInteractionsToBeUpdated = new List<PatientConnect__PC_Interaction__c>();

        for (PatientConnect__PC_Association__c assoc : lstInactiveSOCAssociations) {
            setProgramCaseIds.add(assoc.PatientConnect__PC_Program__c);
            setSOCAccountIds.add(assoc.PatientConnect__PC_Account__c);
        }

        lstInteractionsToBeUpdated = [SELECT Id, spc_Has_Active_SOC__c
                                      FROM PatientConnect__PC_Interaction__c
                                      WHERE PatientConnect__PC_Patient_Program__c IN :setProgramCaseIds AND
                                      PatientConnect__PC_Participant__c IN :setSOCAccountIds];

        if (!lstInteractionsToBeUpdated.isEmpty()) {
            for (PatientConnect__PC_Interaction__c interaction : lstInteractionsToBeUpdated) {
                interaction.spc_Has_Active_SOC__c = false;
            }
        }

        try {
            spc_Database.upd(lstInteractionsToBeUpdated);
        } catch (Exception e) {
            spc_Utility.logAndThrowException(e);
        }
    }

    /*********************************************************************************
    Method Name    : validateAndUpdateInteractionsWithSOC
    Developer      : Deloitte
    Description    : This method is used to validate the number of interactions of the case o which SOC is created or made active.
    Return Type    : Void
    *********************************************************************************/
    public void validateAndUpdateInteractionsWithSOC(List<PatientConnect__PC_Association__c> lstActiveSOCAssociations) {

        Set<Id> lstProgramCaseIds = new Set<Id>();
        List<PatientConnect__PC_Interaction__c> lstInteractions = new List<PatientConnect__PC_Interaction__c>();
        Map<Id, List<PatientConnect__PC_Interaction__c>> mapCaseToInteractions = new Map<Id, List<PatientConnect__PC_Interaction__c>>();
        List<PatientConnect__PC_Interaction__c> lstInteractionsToBeUpdated = new List<PatientConnect__PC_Interaction__c>();

        for (PatientConnect__PC_Association__c assoc : lstActiveSOCAssociations) {
            lstProgramCaseIds.add(assoc.PatientConnect__PC_Program__c);
        }

        lstInteractions = [SELECT Id, PatientConnect__PC_Patient_Program__c, PatientConnect__PC_Participant__c
                           FROM PatientConnect__PC_Interaction__c
                           WHERE PatientConnect__PC_Patient_Program__c IN :lstProgramCaseIds];
        if (!lstInteractions.isEmpty()) {
            for (PatientConnect__PC_Interaction__c interaction : lstInteractions) {
                List<PatientConnect__PC_Interaction__c> interactions;
                if (mapCaseToInteractions.containsKey(interaction.PatientConnect__PC_Patient_Program__c)) {
                    interactions = mapCaseToInteractions.get(interaction.PatientConnect__PC_Patient_Program__c);
                } else {
                    interactions = new List<PatientConnect__PC_Interaction__c>();
                }
                interactions.add(interaction);
                mapCaseToInteractions.put(interaction.PatientConnect__PC_Patient_Program__c, interactions);
            }


            for (PatientConnect__PC_Association__c assoc : lstActiveSOCAssociations) {
                Integer noOfInteractions = 0;
                for (PatientConnect__PC_Interaction__c interaction : mapCaseToInteractions.get(assoc.PatientConnect__PC_Program__c)) {
                    if (interaction.PatientConnect__PC_Participant__c == assoc.PatientConnect__PC_Account__c) {
                        noOfInteractions++;
                    }
                }

                //show error if there are more than 1 interactions and update the flag if there is only one interaction
                if (noOfInteractions > 1) {
                    assoc.addError(System.Label.spc_ActivatingSOCError);
                } else if (noOfInteractions == 1) {
                    for (PatientConnect__PC_Interaction__c interaction : mapCaseToInteractions.get(assoc.PatientConnect__PC_Program__c)) {
                        if (interaction.PatientConnect__PC_Participant__c == assoc.PatientConnect__PC_Account__c) {
                            interaction.spc_Has_Active_SOC__c = true;
                            lstInteractionsToBeUpdated.add(interaction);
                        }
                    }
                }
            }
        }
        if (!lstInteractionsToBeUpdated.isEmpty()) {
            try {
                spc_Database.upd(lstInteractionsToBeUpdated);
            } catch (Exception e) {
                spc_Utility.logAndThrowException(e);
            }
        }
    }

    /*********************************************************************************
    Method Name    : validateActiveAssociations
    Developer      : Deloitte
    Description    : This method is used to ensure that there is only one active care team member with a role when an association is activated.
    Return Type    : Void
    *********************************************************************************/
    public void validateActiveAssociations(List<PatientConnect__PC_Association__c> lstActiveAssociations) {

        Set<Id> setProgramCaseIds = new Set<Id>();
        List<PatientConnect__PC_Association__c> lstAllAssociations = new List<PatientConnect__PC_Association__c>();
        Map<Id, List<PatientConnect__PC_Association__c>> mapCaseToAssociations = new Map<Id, List<PatientConnect__PC_Association__c>>();

        for (PatientConnect__PC_Association__c assoc : lstActiveAssociations) {
            setProgramCaseIds.add(assoc.PatientConnect__PC_Program__c);
        }

        lstAllAssociations = [SELECT Id, PatientConnect__PC_Program__c, PatientConnect__PC_Role__c
                              FROM PatientConnect__PC_Association__c
                              WHERE PatientConnect__PC_AssociationStatus__c = : spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE) AND PatientConnect__PC_Program__c IN: setProgramCaseIds];

        if (!lstAllAssociations.isEmpty()) {
            for (PatientConnect__PC_Association__c assoc : lstAllAssociations) {
                List<PatientConnect__PC_Association__c> associations;
                if (mapCaseToAssociations.containsKey(assoc.PatientConnect__PC_Program__c)) {
                    associations = mapCaseToAssociations.get(assoc.PatientConnect__PC_Program__c);
                } else {
                    associations = new List<PatientConnect__PC_Association__c>();
                }
                associations.add(assoc);
                mapCaseToAssociations.put(assoc.PatientConnect__PC_Program__c, associations);
            }

            for (PatientConnect__PC_Association__c association : lstActiveAssociations) {
                for (PatientConnect__PC_Association__c otherAssoc : mapCaseToAssociations.get(association.PatientConnect__PC_Program__c)) {
                    if (otherAssoc.PatientConnect__PC_Role__c == association.PatientConnect__PC_Role__c) {
                        association.addError(System.Label.spc_ActivatingAssociationError);
                    }
                }
            }
        }
    }
}
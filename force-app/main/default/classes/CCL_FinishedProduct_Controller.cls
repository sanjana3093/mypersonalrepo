public with sharing class CCL_FinishedProduct_Controller {
          /*
    *  @author          Deloitte
    *  @description     Retrieve the Finished Product Deatils for the record 
    *  @param
    *  @date            August 18, 2020
    */
   @AuraEnabled(cacheable=true)
   public static CCL_Finished_Product__c getFinishedProductData(Id recordId) {
       System.debug('FinishedProductRecId &&&&& '+recordId);
       
       CCL_Finished_Product__c finishedProductRec = new CCL_Finished_Product__c();
       if (CCL_Finished_Product__c.sObjectType.getDescribe().isAccessible()) {
        finishedProductRec = [ SELECT Id, Name, CCL_Infusion_Reporting_status__c, CCL_Infusion_Procedure_status__c, CCL_Number_of_bags_Infused__c, CCL_Actual_Infusion_Date_Time__c,
                              CCL_Actual_Infusion_Date_Time_Text__c, CCL_Order__r.Id, CCL_Order__r.Name, CCL_Infusion_to_Time_Zone__c, CCL_Infusion_Details__c, CCL_Batch_Status__c,CCL_Product_Acceptance_Status__c,
                              LastModifiedDate,LastModifiedById,LastModifiedBy.Name, CCL_Shipment__r.CCL_Infusion_Center__r.ShippingCountryCode, CCL_OBA_Milestone_1__c, CCL_OBA_Milestone_2__c, CCL_OBA_Milestone_3__c, CCL_OBA_Milestone_4__c, CCL_OBA_Milestone_5__c,
                              CCL_Shipment__r.CCL_Number_of_Bags_Shipped__c, CCL_OBA_Outcome_1_Reported_On__c, CCL_OBA_Outcome_2_Reported_On__c, CCL_OBA_Outcome_3_Reported_On__c, CCL_OBA_Outcome_4_Reported_On__c, CCL_OBA_Outcome_5_Reported_On__c, CCL_OBA_Outcome_1__c, CCL_OBA_Outcome_2__c, CCL_OBA_Outcome_3__c, CCL_OBA_Outcome_4__c, CCL_OBA_Outcome_5__c,
                              CCL_Shipment__r.CCL_Expiry_Date_of_Shipped_Bags_Text__c, CCL_OBA_Outcome_1_Reported_By__c, CCL_OBA_Outcome_2_Reported_By__c, CCL_OBA_Outcome_3_Reported_By__c, CCL_OBA_Outcome_4_Reported_By__c, CCL_OBA_Outcome_5_Reported_By__c, CCL_OBA_Milestone_1_Date__c, CCL_OBA_Milestone_2_Date__c, CCL_OBA_Milestone_3_Date__c, CCL_OBA_Milestone_4_Date__c, CCL_OBA_Milestone_5_Date__c, 
                              CCL_Shipment__r.CCL_Product_Acceptance_Status__c, CCL_Shipment__r.CCL_OBA__c, CCL_Shipment__r.CCL_Infusion_Data_Collection_Allowed__c, CCL_Shipment__r.CCL_Infusion_Date_Required__c
                              FROM CCL_Finished_Product__c WHERE Id =:recordId LIMIT 1];

       }
            system.debug('FP Rec' +finishedProductRec );
       return finishedProductRec;
   }



    /*
    *  @author          Deloitte
    *  @description     Retrieve users name based on User ID
    *  @param
    *  @date            Dec 1, 2020
    */
   @AuraEnabled
   public static String getUserName(Id userId) {
    String tz;
    User userinfoquery = new User();
    if(userId != null) {
        if (User.sObjectType.getDescribe().isAccessible()) {
        userinfoquery = [SELECT Name FROM User WHERE Id =: userId];

        tz =   userinfoquery.Name;
        }
    }
           
       return tz; 
   }


   @AuraEnabled
    public static String getFormattedDate(Date inputDate) {
        return CCL_Utility.getFormattedDate(inputDate);
    }

     /*
    *  @author          Deloitte
    *  @description     to check user permission of Infusion Reporter
    *  @param
    *  @date            August 18, 2020
    */
    @AuraEnabled(cacheable=true)
    public static Boolean checkUserPermission() {
        return FeatureManagement.checkPermission('CCL_Infusion_Reporter');
    }

      /*
    *  @author          Deloitte
    *  @description     to check user permission of Primary Infusion Flow 
    *  @param
    *  @date            Dec 2, 2020
    */
    @AuraEnabled(cacheable=true)
    public static Boolean checkUserPermissionPrimaryInfusion() {
        return FeatureManagement.checkPermission('CCL_Primary_Infusion_Flow');
    }


      /*
    *  @author          Deloitte
    *  @description     Get files uploaded to the current finished product record.
    *  @param
    *  @date            August 18, 2020
    */
    /*@AuraEnabled(cacheable=true)
    public static Boolean checkUserPermissionofViewer() {
        return FeatureManagement.checkPermission('CCL_Infusion_Viewer');
    }*/
    @AuraEnabled(cacheable=true)
    public static List<ContentDocumentLink> getUploadedFileDetails(Id recordId) {
        system.debug('RecordId::'+recordId);
        try {
                final List<ContentDocumentLink> finalList=new List<ContentDocumentLink>();
                final Map<String,ContentDocumentLink> mapFile = new Map<String,ContentDocumentLink>();
                List<ContentDocumentLink> fileDetails= new List<ContentDocumentLink>();
                final Set<Id> userIds=new Set<Id>();
                final Set<Id> updatedUserIds=new Set<Id>();
                List<CCL_Finished_Product__c> shipResultlist = new List<CCL_Finished_Product__c>();
                Id shipmentId;



                shipResultlist = [SELECT Id, Name, CCL_Shipment__c FROM CCL_Finished_Product__c WHERE Id =:recordId];
                system.debug('shipResultlist::'+shipResultlist);  
                
                shipmentId = shipResultlist[0].CCL_Shipment__c;
                system.debug('shipmentId::'+shipmentId);  



                
                if (ContentDocumentLink.sObjectType.getDescribe().isAccessible()) {

                    fileDetails = [SELECT Id,ContentDocumentId,ContentDocument.LatestPublishedVersionId, ContentDocument.Title,ContentDocument.FileType,ContentDocument.FileExtension,ContentDocument.CreatedById  
                                   FROM ContentDocumentLink 
                                   WHERE LinkedEntityId =:shipmentId WITH SECURITY_ENFORCED];
                    system.debug('fileDetails::'+fileDetails);                                       
                }
                
                for(ContentDocumentLink contentDocumentLinkInstance : fileDetails) {
                    system.debug('contentDocumentLinkInstance::'+contentDocumentLinkInstance);
                    final String key = String.valueOf(contentDocumentLinkInstance.ContentDocument.CreatedById) + String.valueOf(contentDocumentLinkInstance.ContentDocumentId);
                    userIds.add(contentDocumentLinkInstance.ContentDocument.CreatedById);
                    mapFile.put(key,contentDocumentLinkInstance);
                }
                    system.debug('userIds::'+userIds);
                    system.debug('mapFile::'+mapFile);

                updatedUserIds.addall(userIds);
                system.debug('updatedUserIds::'+updatedUserIds);
                
                for (String key : mapFile.keySet()) {
                    final ContentDocumentLink documents = mapFile.get(key);
                    if(updatedUserIds.contains(documents.ContentDocument.CreatedById)) {
                        finalList.add(documents);
                        system.debug('finalList::'+finalList);
                    }
                }
              
               return finalList;

        } catch(QueryException qe) {
            throw new AuraHandledException(qe.getMessage());
    
        }
    }


     /* ********************************************************************************************************
*  @author          Deloitte
*  @description     tofetch shipmemt header
*  @param           
*  @date            july 27, 2020
*********************************************************************************************************/

@AuraEnabled(cacheable=true)
public static List<CCL_Shipment_Header__mdt > getShipmentHeader() {
    final List<CCL_Shipment_Header__mdt> shipmentFinalLst = new List<CCL_Shipment_Header__mdt>();
    try {
      
        if (CCL_Shipment_Header__mdt.sObjectType.getDescribe().isAccessible()) {
            
            final List<CCL_Shipment_Header__mdt> mySpimentLst=[select CCL_Field_API_Name__c,CCL_Field_Type__c,Masterlabel,CCL_Object_API_Name__c,CCL_Order_of_field__c
             from CCL_Shipment_Header__mdt  order by CCL_Order_of_field__c limit 20];
             shipmentFinalLst.addAll(mySpimentLst);   
        }
        if(!shipmentFinalLst.isEmpty()) {
            final String obj=shipmentFinalLst[0].CCL_Object_API_Name__c;
            final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            final Schema.SObjectType mySchema = schemaMap.get(obj);
            final Map <String, Schema.SObjectField> fieldMap = mySchema.getDescribe().fields.getMap();
            for(CCL_Shipment_Header__mdt cs:shipmentFinalLst) {
                cs.Masterlabel=fieldMap.get(cs.CCL_Field_Api_Name__c).getDescribe().getLabel();
               
            }
            
        }
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw  ex;
    }
    return shipmentFinalLst;
}

@AuraEnabled(cacheable=true)
public static List<sObject> getTherapyType(Id recordId, String objectType) {
      List<sObject> shipmentDetails=new List<sObject>();
    try {
  if (CCL_Order__c.sObjectType.getDescribe().isAccessible() && CCL_Shipment__c.sObjectType.getDescribe().isAccessible() && objectType=='Shipment') {
            shipmentDetails=[SELECT CCL_Indication_Clinical_Trial__c,CCL_Ordering_Hospital__c,CCL_Indication_Clinical_Trial__r.CCL_Type__c,CCL_Novartis_Batch_Id__c, CCL_Actual_Apheresis_Received_Date_Time__c,CCL_Order_Apheresis__c,CCL_Order__r.CCL_Treatment_Status__c,CCL_Order__r.CCL_Logistic_Status__c,
            CCL_Order__r.CCL_Patient_Initials__c,CCL_Order__c,CCL_Order_Apheresis__r.CCL_Patient_Initials__c,CCL_Order__r.CCL_Patient_Id__c,CCL_Order_Apheresis__r.CCL_Patient_Id__c,CCL_Order_Apheresis__r.CCL_Initials_COI__c,CCL_Order_Apheresis__r.CCL_Initials__c,CCL_Order__r.CCL_Secondary_COI_Patient_ID__c,CCL_Order__r.CCL_Initials_COI__c,
            CCL_Actual_Aph_Pick_up_Date_Time__c FROM CCL_Shipment__c WHERE id IN (SELECT CCL_Shipment__c FROM CCL_Finished_Product__c WHERE id =:recordId ) limit 1];                                     
         }
        if (CCL_Order__c.sObjectType.getDescribe().isAccessible() && objectType == 'Order') {
            shipmentDetails=[SELECT CCL_Therapy__c, CCL_Ordering_Hospital__c, CCL_Therapy__r.CCL_Type__c, CCL_Novartis_Batch_Id__c,CCL_Patient_Initials__c,CCL_Initials_COI__c
            FROM CCL_Order__c WHERE id=:recordId limit 1];
         }

           
    } catch(QueryException ex) {
        System.debug('Exception in CCL_FinishedProduct_Controller '+ex.getMessage());
        throw  ex;
    }
      return shipmentDetails;
}
    /********************************************************************************************************
*  @author          Deloitte
*  @description    to fetch field names and values from metadata
*  @param           
*  @date            june 14, 2020
*********************************************************************************************************/
@AuraEnabled
public static list<sobject> getRecords(string sobj,string cols,Id recordId) {
    List<sObject> resultList = new List<sObject>();
    try {
    if (CCL_Shipment__c.sObjectType.getDescribe().isAccessible()) {
        resultList = [SELECT CCL_Patient_Name__c,CCL_Order__r.CCL_Patient_Id__c,CCL_Order__r.CCL_Secondary_COI_Patient_ID__c,CCL_Date_of_Birth__c,CCL_Indication_Clinical_Trial__r.Name,CCL_Protocol_Subject_Hospital_Patient__c,CCL_Prescriber__c,CCL_Ordering_Hospital__r.Name,CCL_Novartis_Batch_Id__c,CCL_Status__c
        FROM CCL_Shipment__c 
                        WHERE id IN 
                        (SELECT CCL_Shipment__c FROM CCL_Finished_Product__c WHERE id =: recordId)];
    }
        system.debug('resultllist++'+resultList);
        //final string query = 'select '+String.escapeSingleQuotes(cols)+' from '+String.escapeSingleQuotes(sobj)+' where id=:'+'\' '+'SELECT CCL_Shipment__c FROM CCL_Finished_Product__c WHERE id =:'+String.escapeSingleQuotes(recordId) + '\'' ;
        //final string query = 'SELECT CCL_Indication_Clinical_Trial__c,CCL_Ordering_Hospital__c,CCL_Indication_Clinical_Trial__r.CCL_Type__c,CCL_Novartis_Batch_Id__c, CCL_Actual_Apheresis_Received_Date_Time__c,CCL_Order_Apheresis__c,CCL_Order_Apheresis__r.CCL_Treatment_Status__c,CCL_Order_Apheresis__r.CCL_Logistic_Status__c,CCL_Actual_Aph_Pick_up_Date_Time__c FROM CCL_Shipment__c WHERE id IN(SELECT CCL_Shipment__c FROM CCL_Finished_Product__c WHERE id ='+'\'' + String.escapeSingleQuotes(recordId) + '\'' + ')';
        return resultList;
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw ex;
    }
    
}
@AuraEnabled(cacheable=true)
public static List<CCL_Therapy_Association__c> fetchHospitalOptIn(Id therapyId,Id hospitalId) {
      List<CCL_Therapy_Association__c> shipmentDetails=new List<CCL_Therapy_Association__c>();
    try {
         if (CCL_Therapy_Association__c.sObjectType.getDescribe().isAccessible()) {
            shipmentDetails=[SELECT CCL_Hospital_Patient_ID_Opt_In__c FROM CCL_Therapy_Association__c WHERE CCL_Therapy__c=:therapyId and CCL_Site__c=:hospitalId  WITH SECURITY_ENFORCED limit 1];                                     
          
         }
           
    } catch(QueryException ex) {
        System.debug('Exception in CCL_FinishedProduct_Controller'+ex.getMessage());
        throw  ex;
    }
      return shipmentDetails;
}

}
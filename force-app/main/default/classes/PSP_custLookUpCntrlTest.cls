/*********************************************************************************************************
class Name      : PSP_custLookUpCntrlTest 
Description		: Test class for Controller for custom lookup
@author		    : Deloitte
@date       	: July 31, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Deloitte            July 31, 2019          Initial Version
****************************************************************************************************************/ 
@IsTest
public class PSP_custLookUpCntrlTest {
    /* Apex constants */
    static final List<PSP_ApexConstantsSetting__c>  APEX_CONSTANTS =  PSP_Test_Setup.setDataforApexConstants();
    /* Insert failed */
    static final String INSERT_FAILED = 'Insert failed.';
    /* User name */
    final Static String TEST_USER = 'Test';
    /*************************************************************************************
* @author      : Deloitte
* @date        : 09/17/2019
* @Description : Testing insert scenarios
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    public static testMethod void testRunAs () {
        Test.startTest();
        
        final Account patient=PSP_Test_Setup.getPersonAccount('test patient');
        Database.insert(patient);
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram (TEST_USER,null);
        cP1.PSP_Program_Sector__c='Public';
        Database.insert (cP1);
        final CareProgramEnrollee cpE= PSP_Test_Setup.createTestCareProgramEnrollment(TEST_USER,cP1.id);
        cpE.AccountId=patient.Id;
        cpE.UserId =null;
        Database.insert (cpE);
        Test.stopTest();
        try {
            
            final List<CareProgramProduct> cpe2= PSP_custLookUpCntrl.fetchLookupValues('Product','test','CareProgramEnrolleeProduct');
            Database.insert (cpe2); 
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains (INSERT_FAILED),e.getMessage () );
        }
    }
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : 09/17/2019
* @Description : Testing insert scenarios
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    public static testMethod void testRunAs2 () {
        Test.startTest();
        
        final Account patient=PSP_Test_Setup.getPersonAccount('test patient1');
        Database.insert(patient);
        
        Test.stopTest();
        try {
            final List<UserRecordAccess> userRecord= PSP_custLookUpCntrl.getAccessAccount(patient.Id);

        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains (INSERT_FAILED),e.getMessage () );
        }
    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : 09/17/2019
* @Description : Testing insert scenarios
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    public static testMethod void testRunAs3 () {
        Test.startTest();
        
        final Account patient=PSP_Test_Setup.getPersonAccount('test patient2');
        Database.insert(patient);
        
        
        
        Test.stopTest();
        try {
            final List < CareProgramEnrollee > enrolledPrograms= PSP_custLookUpCntrl.getEnrolledPrograms(patient.Id);
            Database.insert(enrolledPrograms);
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains (INSERT_FAILED),e.getMessage () );
        }
    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : 09/17/2019
* @Description : Testing insert scenarios
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    public static testMethod void testRunAs4 () {
        
        try {
            Test.startTest();
            final List<Contact> contacts= PSP_custLookUpCntrl.fetchLookupValues('HCP',TEST_USER,'Contact');
            Database.insert(contacts);
            Test.stopTest();
            
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains (INSERT_FAILED),e.getMessage () );
        }
    }
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : 09/17/2019
* @Description : Testing insert scenarios
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    public static testMethod void testRunAs5 () {
        Test.startTest();
        final Account patient=PSP_Test_Setup.getPersonAccount('test patient3');
        Database.insert(patient);
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram (TEST_USER,null);
        cP1.PSP_Program_Sector__c='Public';
        Database.insert (cP1);
        final CareProgramEnrollee cpE= PSP_Test_Setup.createTestCareProgramEnrollment(TEST_USER,cP1.id);
        cpE.AccountId=patient.Id;
        cpE.UserId =null;
        Database.insert (cpE);
        Test.stopTest();
        try {
            
           final List < CareProgramEnrolleeProduct> cpep=PSP_custLookUpCntrl.getEnrolledPrescriptions(patient.Id,cpE.Id,'Product');
            Database.insert(cpep);
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains (INSERT_FAILED),e.getMessage () );
        }
    }
    
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : 11/11/2019
* @Description : Testing insert scenarios
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    public static testMethod void testRunAs6 () {
        Test.startTest();
        final Account patient=PSP_Test_Setup.getPersonAccount('test patient4');
        Database.insert(patient);
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram (TEST_USER,null);
        cP1.PSP_Program_Sector__c='Public';
        Database.insert (cP1);
        Test.stopTest();
        try {
            
            final List < CareProgramProduct> cpp=PSP_custLookUpCntrl.getCareProgramProducts(cP1.id,'Product');
            Database.insert(cpp);
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains (INSERT_FAILED),e.getMessage () );
        }
    }
    
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : 11/11/2019
* @Description : Testing insert scenarios
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    public static testMethod void testRunAs7 () {
        Test.startTest();
        final Product2 product=PSP_Test_Setup.createProduct('test product');
        Database.insert(product);
        Test.stopTest();
        try {
            
           final List < CareProgramProvider> cpprovider=PSP_custLookUpCntrl.getCareProgramProviders(product.Id);
            Database.insert(cpprovider);
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains (INSERT_FAILED),e.getMessage () );
        }
    }
}
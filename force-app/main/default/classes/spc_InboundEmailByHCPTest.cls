/**
* @author Deloitte
* @date 09-June-18
*
* @description This is the Test class for inbound Emails
*/

@isTest
public class spc_InboundEmailByHCPTest {
    //@testSetup
    static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

    }
    @isTest
    static void createDocument() {
        setup();
        test.startTest();
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEmail email2 = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // setup the data for the email
        email.subject = Label.spc_email_subject_with_attachment;
        email.fromAddress = Label.spc_testemail_from_address;
        email.htmlBody = 'TestTestTestTEstEmail';
        email2.subject = Label.spc_email_subject_without_attachment;
        email2.fromAddress = Label.spc_testemail_from_address;
        List<String> toaddressemail = new List<String>();
        toaddressemail.add('test12@test.com');
        email2.toAddresses = toaddressemail;
        email.toAddresses = toaddressemail;
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = Label.spc_attachment_body;
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'textfiletwo3.txt';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        Messaging.InboundEmail.BinaryAttachment attachmentpdf = new Messaging.InboundEmail.BinaryAttachment();
        attachmentpdf.body = blob.valueOf(Label.spc_attachment_body);
        attachmentpdf.fileName = 'textfileone.txt';
        attachmentpdf.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachmentpdf };

        //Create document record
        PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c();
        doc.PatientConnect__PC_From_Email_Address__c = Label.spc_testemail_from_address;
        insert doc;

        //Create attachment record
        Attachment attachment = new Attachment();
        attachment.Name = Label.spc_attachment_body;
        attachment.Body = Blob.valueOf( Label.spc_attachment_body);
        attachment.ParentId = doc.Id;
        attachment.ContentType =  'text/plain';
        insert attachment;
        //Create account record
        Account acc = spc_Test_Setup.createTestAccount('PC_Patient');
        acc.PatientConnect__PC_Email__c = Label.spc_testemail_from_address;
        acc.PatientConnect__PC_Date_of_Birth__c = Date.valueOf(Label.spc_test_birth_date);
        insert acc;
        //Create Access
        Case caseProgrm = new Case();
        insert caseProgrm;
        caseProgrm.AccountId = acc.id;
        String devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId();
        caseProgrm.RecordTypeId = devRecordTypeId;

        // call the email service class and test it with the data in the testMethod
        spc_InboundEmailByHCP  testInbound = new spc_InboundEmailByHCP ();
        testInbound.handleInboundEmail(email, env);

        //Create document Record
        PatientConnect__PC_Document_Log__c documentLog = new PatientConnect__PC_Document_Log__c();

        if (caseProgrm.recordType.name == 'Program') {
            documentLog.PatientConnect__PC_Program__c = caseProgrm.id;
        }

        documentLog.PatientConnect__PC_Document__c = doc.Id;
        documentLog.PatientConnect__PC_Account__c = acc.Id;
        insert documentLog;
        test.stopTest();
    }

    @isTest
    static void createDocument2() {
        setup();
        test.startTest();
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // setup the data for the email
        email.subject = Label.spc_email_subject_without_attachment;
        email.fromAddress = Label.spc_testemail_from_address;
        email.plainTextBody = 'Hello World';
        List<String> toaddressemail = new List<String>();
        toaddressemail.add('test12@test.com');
        email.toAddresses = toaddressemail;
        //Create document record
        PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c();
        doc.PatientConnect__PC_From_Email_Address__c = Label.spc_testemail_from_address;
        insert doc;

        //Create account record
        Account acc = spc_Test_Setup.createTestAccount('PC_Patient');
        acc.PatientConnect__PC_Email__c = Label.spc_testemail_from_address;
        acc.PatientConnect__PC_Date_of_Birth__c = Date.valueOf(Label.spc_test_birth_date);

        insert acc;
        //Create Access
        Case caseProgrm2 = new Case();
        insert caseProgrm2;
        caseProgrm2.AccountId = acc.id;
        String devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId();
        caseProgrm2.RecordTypeId = devRecordTypeId;

        // call the email service class and test it with the data in the testMethod
        spc_InboundEmailByHCP  testInbound = new spc_InboundEmailByHCP ();
        testInbound.handleInboundEmail(email, env);

        //Create document Record
        PatientConnect__PC_Document_Log__c documentLog = new PatientConnect__PC_Document_Log__c();
        if (caseProgrm2.recordType.name == 'Program') {
            documentLog.PatientConnect__PC_Program__c = caseProgrm2.id;
        }
        documentLog.PatientConnect__PC_Document__c = doc.Id;
        documentLog.PatientConnect__PC_Account__c = acc.Id;
        insert documentLog;
        test.stopTest();
    }
    @isTest
    static void createDocumentwithpdf() {
        setup();
        test.startTest();
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEmail email2 = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // setup the data for the email
        email.subject = Label.spc_email_subject_with_attachment;
        email.fromAddress = Label.spc_testemail_from_address;
        email.plainTextBody = 'testTestTEstTest';
        email2.subject = Label.spc_email_subject_without_attachment;
        email2.fromAddress = Label.spc_testemail_from_address;
        List<String> toaddressemail = new List<String>();
        toaddressemail.add('test12@test.com');
        email2.toAddresses = toaddressemail;
        email.toAddresses = toaddressemail;
        Messaging.InboundEmail.BinaryAttachment attachmentpdf = new Messaging.InboundEmail.BinaryAttachment();
        attachmentpdf.body = blob.valueOf('my attachment text');
        attachmentpdf.fileName = 'textfileone.txt';
        attachmentpdf.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachmentpdf };

        //Create document record
        PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c();
        doc.PatientConnect__PC_From_Email_Address__c = Label.spc_testemail_from_address;
        insert doc;
        //create binary attachment
        Attachment attachment2 = new Attachment();
        attachment2.ContentType = 'application/pdf';
        attachment2.Name = Label.spc_attachment_body;
        attachment2.Body = Blob.valueOf(Label.spc_attachment_body);
        attachment2.ParentId =  doc.Id;
        insert attachment2;
        //Create account record
        Account acc = spc_Test_Setup.createTestAccount('PC_Patient');
        acc.PatientConnect__PC_Email__c = Label.spc_testemail_from_address;
        acc.PatientConnect__PC_Date_of_Birth__c = Date.valueOf(Label.spc_test_birth_date);
        insert acc;
        //Create Access
        Case caseProgrm = new Case();
        insert caseProgrm;
        caseProgrm.AccountId = acc.id;
        String devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId();
        caseProgrm.RecordTypeId = devRecordTypeId;
        // call the email service class and test it with the data in the testMethod
        spc_InboundEmailByHCP  testInbound = new spc_InboundEmailByHCP ();
        testInbound.handleInboundEmail(email, env);
        //Create document Record
        PatientConnect__PC_Document_Log__c documentLog = new PatientConnect__PC_Document_Log__c();
        if (caseProgrm.recordType.name == 'Program') {
            documentLog.PatientConnect__PC_Program__c = caseProgrm.id;
        }
        documentLog.PatientConnect__PC_Document__c = doc.Id;
        documentLog.PatientConnect__PC_Account__c = acc.Id;
        insert documentLog;
        test.stopTest();
    }
}
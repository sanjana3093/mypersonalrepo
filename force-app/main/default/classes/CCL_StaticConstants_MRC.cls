/********************************************************************************************************
*  @author          Deloitte
*  @description     to fetch ordering hospital associated to the user
*  @param
*  @date            june 22, 2020
*********************************************************************************************************/

public class CCL_StaticConstants_MRC {
    /*** Variable to store CCL_ORDERINGCAPABALITY*/
    public static final String CCL_ORDERINGCAPABALITY ='L1O';
    /*** Variable to store PICKUPSITE*/
     public static final String PICKUPSITE ='L2C_OR_L3P';
    /*** Variable to store Collection Association Type*/
    public static final String COLLASSOCTYPEVAR ='L2C';
    /*** Variable to store Infusion Association Type*/
    public static final String INFASSOCTYPEVAR ='L2I';
     /*** Variable to store Pickup Association type*/
    public static final String PICKUPASSOCTYPEVAR ='L3P';
     /*** Variable to store Ship to Association Type*/
     public static final String SHIPTOASSOCTYPEVAR ='L3D';
     /*** Variable to store APHCOLLECTIONSITE*/
     public static final String APHCOLLECTIONSITE ='L2C_OR_L3P';
     /*** Variable to store INFSITE*/
     public static final String INFSITEVAR ='L2I_OR_L3D';
     /*** Variable to store shipToLocation*/
     public static final String SHIPTOLOCVAR ='L2I_OR_L3D';
    /*** Variable to store user therapy association prescriber record type*/
    public static final String PRISCRIBER_PRINCIPAL_INVESTIGATOR = Schema.SObjectType.CCL_User_Therapy_Association__c.getRecordTypeInfosByDeveloperName().get('CCL_Prescriber_Principal_Investigator').getRecordTypeId();
    /*** Variable to store therapy association country record type*/
     public static final String THERAPY_ASSOCIATION_COUNTRY = Schema.SObjectType.CCL_Therapy_Association__c.getRecordTypeInfosByDeveloperName().get('CCL_Country').getRecordTypeId();
    /***variable to store case cell therapy record type*/
     public static final String CASE_CELLTHERAPY = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CCL_Cell_Therapy').getRecordTypeId();
     /*** Variable to store Online case origin picklist value*/
     public static final String TEXT='Text';
     /*** Variable to store New case status picklist value*/
     public static final String STATUS_NEW = 'New';
    /***variable to store order record type- Clinical*/
     public static final String ORDER_RECORDTYPE_CLINICAL = Schema.SObjectType.CCL_Order__c.getRecordTypeInfosByDeveloperName().get('CCL_Clinical').getRecordTypeId();
     /***variable to store order record type- Commercial*/
     public static final String ORDER_RECORDTYPE_COMMERCIAL = Schema.SObjectType.CCL_Order__c.getRecordTypeInfosByDeveloperName().get('CCL_Commercial').getRecordTypeId();
     /***variable to store duplicate error message for Contact field*/
     public static final String CONTACT_ERROR_FIELD = 'The Contact field should not be populated in this Record type';
     /***variable to store duplicate error messag for User field*/
     public static final String USER_ERROR_FIELD = 'The User field should not be populated in this Record type';
	 /***variable to store duplicate error message for Internal/External RT*/
     public static final String INTERNAL_UNIQUE_ERROR_FIELD = 'The Unique User-Therapy Association Combination Field Does not Match';
     /***variable to store duplicate error message for Prescriber RT*/
     public static final String PRESCRIBER_UNIQUE_ERROR_FIELD = 'The Unique Contact-Therapy Association Combination Field Does not Match';
     /*** variable to store the Yes value for comparison purposes */
     public static final String VALUE_YES = 'Yes';
     /*** variable to store the No value */
     public static final String VALUE_NO = 'No';
     /*** variable to store the Country value */
     public static final String  VALUE_COUNTRY_US = 'United States';
     /*** variable to store the Picklist value for CCL_Country__c picklist field on CCL_Therapy Association */
     public static final String  PICKLIST_VALUE_COUNTRY_US = 'US';
     /*** variable to store Site Record Type - CCL_Site */
     public static final String THERAPY_ASSOCIATION_RECORDTYPE_SITE = Schema.SObjectType.CCL_Therapy_Association__c.getRecordTypeInfosByDeveloperName().get('CCL_Site').getRecordTypeId();

     /*** variable to store Site Record Type - CCL_Country */
     public static final String THERAPY_ASSOCIATION_RECORDTYPE_COUNTRY = Schema.SObjectType.CCL_Therapy_Association__c.getRecordTypeInfosByDeveloperName().get('CCL_Country').getRecordTypeId();

     /*** variable to store the System Admin Profile value */
     public static final String SYSTEM_ADMIN_USER_PROFILE_TYPE = 'System Administrator';

     /*** variable to store the Novartis Business Admin Permission Set value */
     public static final String PERMISSION_TYPE_NVS_BUSINESS_ADMIN = 'CCL_Novartis_Business_Admin';

     /*** variable to store the adf submiiter value */
     public static final String PERMISSIONSET_ADF_SUBMITTER = 'CCL_ADF_Submitter';

     /*** variable to store the Permission Set PRF Submitter to support Testing */
     public static final String PERMISSION_SET_PRF_SUBMITTER = 'CCL_PRF_Submitter';

    /*** variable to store the Permission Set FP Receiver to support Testing */
    public static final String PERMISSION_SET_FP_RECEIVER = 'CCL_FP_Receiver';

    /*** variable to store the Permission Set Account Manager to support Testing */
    public static final String PERMISSION_SET_ACCOUNT_MANAGER = 'CCL_Account_Manager';

    /*** variable to store the Permission Set Infusion Reporter to support Testing */
    public static final String PERMISSION_SET_INFUSION_REPORTER = 'CCL_Infusion_Reporter';

    /*** variable to store the Permission Set Outbound logistics user to support Testing */
    public static final String PERMISSION_SET_OUTBOUND_LOGISTICS = 'CCL_Outbound_Logistics';

     /*** variable to store the External Base Profile Value */
     public static final String EXTERNAL_BASE_PROFILE_TYPE = 'External Base Profile';

     /*** variable to store the Internal Base Profile Value */
     public static final String INTERNAL_BASE_PROFILE_TYPE = 'Internal Base Profile';

     /*** variable to store the Role Value to support the Parent Account required for Portal users */
     public static final String ROLE_NOVARTIS_ADMIN = 'Novartis_Admin';

     /*** variable to store the Zero value in the creation of field value on object */
     public static final Decimal VALUE_ZERO = 0.0;
     /*** variable to store the fifty value in the creation of field value on object */
     public static final Decimal VALUE_FIFTY = 50.0;
     /*** variable to store random text greater than 255 */
     public static final String TEXT_AREA_GREATER_255 = 'Lorem test curatio Lorem test curatio Lorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatio ';
     /*** variable to store random text less than 255 */
     public static final String TEXT_AREA_LESS_255 = 'Lorem test curatio Lorem test curatio Lorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curatioLorem test curati';
      /***variable to store shipment record type- Finished Product*/
     public static final String SHIPMENT_RECORDTYPE_FP = Schema.SObjectType.CCL_Shipment__c.getRecordTypeInfosByDeveloperName().get('CCL_Finished_Product').getRecordTypeId();
     /***variable to store shipment record type- Apheresis*/
     public static final String SHIPMENT_RECORDTYPE_AP = Schema.SObjectType.CCL_Shipment__c.getRecordTypeInfosByDeveloperName().get('CCL_Apheresis').getRecordTypeId();
     /***variable to store Account record type- Plant*/
    public static final String ACCOUNT_RECORDTYPE_PLANT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CCL3_Plant_Account').getRecordTypeId();
     /*** Variable to store New case status picklist value*/
     public static final String SHIPMENT_REASON_INITIAL_SHIPMENT = 'IN';
     /** variable to store a Therapy value Commerial for test data purposes */
     public static final String VALUE_THERAPY_COMMERICAL = 'Commercial';
     /** variable to store a Therapy value Clinical for test data purposes */
     public static final String VALUE_THERAPY_CLINICAL = 'Clinical';
    /** variable to store a true value for SQOL filtering  */
    public static final Boolean VALUE_TRUE = true;
	    /** variable to store JSON_VALUE_THERAPYTYPE */
	public static final String JSON_VALUE_THERAPYTYPE = 'TherapyType';

	/***variable to store Person Account record type- Patient*/
	public static final String ACCOUNT_PERSONACCOUNT_PATIENTDETAIL = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CCL_Patient').getRecordTypeId();

	/** variable to store JSON_VALUE_PHYSICIAN_ATTESTATION  */
	public static final String JSON_VALUE_PHYSICIAN_ATTESTATION = 'CCL_Physician_Attestation__c';

	/** variable to store JSON_VALUE_PATIENT_ELIGIBILITY */
	public static final String JSON_VALUE_PATIENT_ELIGIBILITY = 'CCL_Patient_Eligibility__c';

	/** variable to store JSON_VALUE_PATIENT_WEIGHT */
	public static final String JSON_VALUE_PATIENT_WEIGHT = 'CCL_Patient_Weight__c';

	/** variable to store JSON_VALUE_PATIENT_AGE  */
	public static final String JSON_VALUE_PATIENT_AGE = 'CCL_Patient_Age_At_Order__c';

	/** variable to store JSON_VALUE_CONSENT_ADDITIONAL_RESEARCH  */
	public static final String JSON_VALUE_CONSENT_ADDITIONAL_RESEARCH = 'CCL_Consent_for_Additional_Research__c';

	/** variable to store JSON_VALUE_VA_PATIENT */
	public static final String JSON_VALUE_VA_PATIENT = 'CCL_VA_Patient__c';

	/** variable to store JSON_VALUE_PLANNED_APEHERESIS_PICKUP_DATE */
	public static final String JSON_VALUE_PLANNED_APEHERESIS_PICKUP_DATE = 'CCL_Planned_Apheresis_Pick_up_Date_Time__c';

	/** variable to store JSON_VALUE_VALUE_STREAM */
	public static final String JSON_VALUE_VALUE_STREAM = 'CCL_Value_Stream__c';

	/** variable to store JSON_VALUE_SCHEDULER_MISSING  */
	public static final String JSON_VALUE_SCHEDULER_MISSING = 'CCL_Scheduler_Missing__c';

	/** variable to store JSON_VALUE_REPLACEMENT_ORDER  */
	public static final String JSON_VALUE_REPLACEMENT_ORDER = 'CCL_Returning_Patient__c';

	/** variable to store JSON_VALUE_ESTIMATED_FP_DELIVERY_DATE */
	public static final String JSON_VALUE_ESTIMATED_FP_DELIVERY_DATE = 'CCL_Estimated_FP_Delivery_Date__c';

	/** variable to store JSON_VALUE_ESTIMATED_MANUFACTURING_START_DATE */
	public static final String JSON_VALUE_ESTIMATED_MANUFACTURING_START_DATE = 'CCL_Estimated_Manufacturing_Start_Date__c';

	/** variable to store JSON_VALUE_DATE_OF_BIRTH */
	public static final String JSON_VALUE_DATE_OF_BIRTH = 'CCL_Date_of_Birth__c';

	/** variable to store JSON_VALUE_PATIENT_DETAIL_ID */
	public static final String JSON_VALUE_PATIENT_DETAIL_ID = 'CCL_Patient__c';
    /** variable to store JSON_VALUE_ORDER_APPROVAL_Eligibility */
	public static final String JSON_VALUE_ORDER_APPROVAL = 'CCL_Order_Approval_Eligibility__c';

	/** variable to store JSON_VALUE_PRF_Ordering_Status */
	public static final String JSON_VALUE_PRF_Ordering_Status = 'CCL_PRF_Ordering_Status__c';

	/** variable to store JSON_VALUE_HOSPITAL_PATIENT_ID */
	public static final String JSON_VALUE_HOSPITAL_PATIENT_ID = 'CCL_Hospital_Patient_ID__c';

	/** variable to store Scheduler Date value */
	public static final String SCHEDULER_DATETIME_TEXT_VALUE_TBC = 'To be confirmed';

	/** variable to store Scheduler Date value */
	public static final String SCHEDULER_DATETIME_TEXT_VALUE_NA = 'Not Available';
	 //code for 2076
    /** variable to store JSON_VALUE_FIRSTNAME_COI */
	public static final String JSON_VALUE_FIRSTNAME_COI = 'CCL_FirstName_COI__c';
    /** variable to store JSON_VALUE_LASTNAME_COI */
	public static final String JSON_VALUE_LASTNAME_COI = 'CCL_LastName_COI__c';
    /** variable to store JSON_VALUE_MIDDLENAME_COI */
	public static final String JSON_VALUE_MIDDLENAME_COI = 'CCL_MiddleName_COI__c';
    /** variable to store JSON_VALUE_SUFFIX_COI */
	public static final String JSON_VALUE_SUFFIX_COI = 'CCL_Sufix_COI__c';
    /** variable to store JSON_VALUE_INITIALS_COI */
	public static final String JSON_VALUE_INITIALS_COI = 'CCL_Initials_COI__c';
    /** variable to store JSON_VALUE_DATEOFBIRTH_COI */
	public static final String JSON_VALUE_DATEOFBIRTH_COI = 'CCL_Day_of_Birth_COI__c';
    /** variable to store JSON_VALUE_BIRTHMONTH_COI */
	public static final String JSON_VALUE_BIRTHMONTH_COI = 'CCL_Month_of_Birth_COI__c';
    /** variable to store JSON_VALUE_YEAR_COI */
	public static final String JSON_VALUE_YEAR_COI = 'CCL_Year_of_Birth_COI__c';

    /** variable to store JSON_VALUE_SECONDARY_COI */
    public static final String JSON_VALUE_SECONDARY_COI = 'CCL_Secondary_COI_Patient_ID__c';


	/** variable to store Plant ID */
	public static final String SCHEDULER_PLANT_INFO = 'CCL_Plant__c';

	/*** Variable to store prf submitter permission set*/
    public static final String CUTSOM_PERMISSION_ALLOW_REPLACEMENT_ORDER = 'CCL_Allow_Replacement_Order';

    /*** Variable to store PHI Permission Set*/
    public static final String CUTSOM_PERMISSION_PHI_USER = 'CCL_PHI_Permission';

	/*** Variable to store PRF Viewer Custom Permission Set*/
    public static final String CUTSOM_PERMISSION_PRF_VIEWER = 'CCL_PRF_Viewer';

    /*** Variable to store ADF Viewer Internal Custom Permission Set*/
    public static final String CUTSOM_PERMISSION_ADF_VIEWER = 'CCL_ADF_Viewer';

    /*** Variable to store PRF Approver Custom Permission Set*/
    public static final String CUTSOM_PERMISSION_PRF_APPROVER = 'CCL_PRF_Approver';

	/***variable to store the Access Level value as Read*/
	public static final String ACCESS_LEVEL_READ = 'Read';
	/***variable to store the Access Level value as Edit*/
	public static final String ACCESS_LEVEL_EDIT = 'Edit';
	/***variable to store the Access Level value as None*/
	public static final String ACCESS_LEVEL_NONE = 'None';
	/***variable to store the Type value as Customer*/
	public static final String ACCOUNT_TYPE_CUSTOMER = 'Customer';

	/***variable to store the Type Level value as Vendor*/
	public static final String ACCOUNT_TYPE_VENDOR = 'Vendor';


	/*** variable  to store the typr of permission set*/

	public static final String PERMISSION_TYPE_SUBMITTER = 'CCL_ADF_Submitter';
	public static final String PERMISSION_TYPE_ADF_APPROVER = 'CCL_ADF_Approver';
	public static final String PERMISSION_TYPE_ADF_APPROVER_USER = 'CCL_ADF_Approver_User';
	public static final String PERMISSION_TYPE_ADF_VIEWER_USER = 'CCL_ADF_Viewer_User';
/*** variable to store the CT Integration Profile Value */
    public static final String CT_INTEGRATION_PROFILE_TYPE = 'CT Integration';

    /***variable to store Therapy Association  record type- Site*/
    public static final String THERAPY_ASSOCAITION_RECORDTYPE_SITE = Schema.SObjectType.CCL_Therapy_Association__c.getRecordTypeInfosByDeveloperName().get('CCL_Site').getRecordTypeId();
    /***variable to store Therapy Association record type- Country*/
    public static final String THERAPY_ASSOCAITION_RECORDTYPE_COUNTRY = Schema.SObjectType.CCL_Therapy_Association__c.getRecordTypeInfosByDeveloperName().get('CCL_Country').getRecordTypeId();
	 /*** Variable to store shipment reason picklist value*/
    public static final String SHIPMENT_REASON_REPLACEMENT_DOSE = 'RE';
     /** variable to store JSON_VALUE_FPShipmentId */
    public static final String JSON_VALUE_FPSHIPMENT_ID = 'FPShipmentId';
     /** variable to store JSON_VALUE_ADFId */
    public static final String JSON_VALUE_ADF_ID = 'ADFId';
  /** variable to store JSON_VALUE_ADFId */
    public static final String JSON_VALUE_APHSHIPMENT_ID = 'AphShipmentId';
	 /*** Variable to store shipment status picklist value*/
    public static final String SHIPMENT_STATUS = 'Product Shipment Planned';
	/*** Variable to store prf submitter permission set*/
    public static final String PRF_SUBMITTER_CUSTOM_PERMISSION = 'CCL_Edit_Order';
/***variable to store Summary record type- Manufacturing*/
    public static final String SUMMARY_RECORDTYPE_MANUFACTURING = Schema.SObjectType.CCL_Summary__c.getRecordTypeInfosByDeveloperName().get('CCL_Manufacturing').getRecordTypeId();

    /***variable to store Shipment record type- FinishedProduct*/
    public static final String SHIPMENT_RECORDTYPE_FINSIHEDPRODUCT = Schema.SObjectType.CCL_Shipment__c.getRecordTypeInfosByDeveloperName().get('CCL_Finished_Product').getRecordTypeId();
/*** Variable to store Product acceptance text*/
    public static final String PRODUCT_ACCEPTANCE_TEXT = 'Test Product Acceptance Text';
    /***variable to store Batch Manufacturing record type- Site*/
    public static final String BATCH_RECORDTYPE_MANUFACTURING = Schema.SObjectType.CCL_Batch__c.getRecordTypeInfosByDeveloperName().get('CCL_Manufacturing').getRecordTypeId();

    /* Variable to store the Internal User Profile value */
    public static final String INTERNAL_USERS = 'Internal Users';
    public static final STRING TEST_DATA_TEN_CHARS= 'testData01';
	/*** Variable to store CCL_Customer_Operations_User*/
	public static final String CUTSOM_CUSTOMER_OPERATIONS_USER = 'CCL_Customer_Operations';
	/*** Variable to store Inbound logistic*/
	public static final String CUTSOM_INBOUND_LOGISTIC = 'CCL_Inbound_Logistics';
	/* Variable to store the FP_Shipment_Status_Value_Product Delivery Rejected*/
    public static final String Product_Delivery_Rejected = 'Product Delivery Rejected';
    /* Variable to store the FP_Shipment_Status_Value_Product Delivery Accepted*/
    public static final String Product_Delivery_Accepted = 'Product Delivery Accepted';
    /* Variable to store the FP_Shipment_Status_Value_Product Shipped*/
    public static final String Product_Shipped = 'Product Shipped';
	/*** Variable to store Outbound logistic*/
	public static final String CUTSOM_OUTBOUND_LOGISTIC = 'CCL_Outbound_Logistics';
	/*** Variable to store Integration User*/
     public static final String CUSTOM_INTEGRATION_USER = 'CT Integration';
	 /*** Variable to store pPatient Initial fieldApi name*/
     public static final String PATIENT_INITAL = 'CCL_Patient_Initials__c';
    /** Variable to support Unit Test query search of Test data */
    public static final String TEST_FINISHED_PRODUCT_VALUE = 'Test Finished_Product';
     /*** Variable to store ResubmissionFlag Key name*/
     public static final String RESUBMISSION_FLAG_KEY = 'ResubmissionFlag';
     /*** Variable to store PRFOrderingStatusCheckFlag Key name*/
     public static final String PRFORDERINGSTATUS_FLAG_KEY = 'PRFOrderingStatusCheckFlag';
    /*** Variable to check if last name is opted in or not*/
     public static final String LAST_NAME_OPTED_IN = 'lastNameOptedIn';
	 /*** Variable to check if coi data has been changed or not*/
     public static final String COI_DATA_CHANGED = 'coiDataChanged';
    /*** Variable to check if last name is opted in or not*/
    public static final String PATIENT_EMAIL_OPTED_IN = 'CCL_TECH_Hos_Patient_ID_Email_Opt_in__c';
    /*** Variable to check if last name is opted in or not*/
    public static final String PATIENT_ID_OPTED_IN = 'CCL_TECH_Hospital_Patient_ID_Opt_in__c';
    /*Stop Summary Trigger from entering into recursion*/
     Public static Boolean sumryNotFrstRun=false;
    /*Stop Order Trigger from entering into recursion*/
     Public static Boolean ordrNotFrstRun=false;
    /*Set order status on newly created orders*/
     Public static String ordrStatus='PRF_Submitted';
    /*Set order status on newly created orders*/
     Public static String excpetionStatus='Out of Specification';
    /*Set order status on newly created orders*/
     Public static String prfApproved  = 'PRF_Approved';
    /*evaluate ADF status*/
     Public static String adfAwaiting  = 'Awaiting ADF';
    /*evaluate ADF status*/
     Public static String pendngSubmissn  = 'ADF Pending Submission';
    /*evaluate ADF status*/
     Public static String adfRejected  = 'ADF Rejected';
    /*evaluate ADF status*/
     Public static String adfCanclld  = 'Order Cancelled';
    /*evaluate ADF status*/
     Public static String adfPendgApprvl  = 'ADF Pending Approval';
    /*evaluate ADF status*/
     Public static String adfApprvd  = 'ADF Approved';

    /*** Variable ENGLISH US API Global Picklist value */
    public static final String GLOBAL_PICKLIST_ENG_US_VALUE  = 'en_US';
}
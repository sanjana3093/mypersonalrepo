/********************************************************************************************************
*  @author          Deloitte
*  @description     Constant class to cater for the Notification engine to ensure it can be easily seperated
*                   from code based
*  @param           
*  @date            August 4, 2020
*********************************************************************************************************/
public without sharing class CCL_Notifications_Constants {

    /*** Regex pattern to identify the Merge field syntax e.g. {!123abs} note escaped correctly */
    public static final String REGEX_PATTERN_CAPTURE_MERGE_FIELDS = '\\{![a-zA-Z0-9_\\.]*\\}';
    // Folder name where notification related email needs to be maintained
    public static final String EMAIL_TEMPLATE_FOLDER_DEVELOPERNAME='Notification_Emails';
    // Field Set Name which will have all the fields taht needs to be added in email template and needs to be queried
    public static final String FIELD_SET_NAME='Notification_Information';
    // Boolean support to help 
    public static final Boolean TRUE_BOOLEAN_VALUE = true;
    /*** Support the Identification of HTML Tags unneeded for mergeText conversation */
    public static final String CDATA_TAG_HTML_VALUE = '<![CDATA[';
    /*** Support the Identification of HTML Tags unneeded for mergeText conversation */
    public static final String CLOSING_HTML_TAG_VALUE = ']]>';
    /*** Support the Identification of HTML Tags unneeded for mergeText conversation */
    public static final String BLANK_VALUE = '';
    /*** Specific variable constant WHERE Id IN: currentRecordIdsDynamic query result  */
    public static final String WHERE_ID_IN_CURRENTRECORDIDS_BIND = ' WHERE Id IN: currentRecordIds';
    /*** SELECT Id, value to support Dynamic query result  */
    public static final String SELECT_ID_COMA_DYNAMIC_QUERY_VALUE = 'SELECT Id,';
    /*** FROM value to support Dynamic query result   */
    public static final String FROM_DYNAMIC_QUERY_VALUE = ' FROM ';
    /*** Name to create Account record   */
    public static final String TESTSITEACCOUNT_VALUE = 'Test Site Account';
    /*** Record Type External on User Therapy Association    */
    public static final String RECORDTYPE_EXTERNAL_USERTHERAPYASSO = 'External';
    /*** Accessbility for Record on User Therapy Association  */
    public static final String RECORD_ACCESSABILITY_EDIT = 'Edit';
    /*** Primary Role on User Therapy Association PRF Submitter  */
    public static final String PRIMARY_ROLE_PRFSUBMITTER = 'PRF Submitter';
    /*** Notification Setting Test record created testCommercialValue  */
    public static final String TESTRECORD_NOTIFICATION_SETTING = 'testCommercialValue';
    /*** Email Template name testCommercialEmail */
    public static final String EMAIL_TEMPLATE_NAME = 'testCommercialEmail';
    /*** Brand Template name NVS Commercial Letterhead */
    public static final String COMMERCIAL_LETTERHEAD = 'Novartis Commercial Letterhead';
    /*** Email Template Template Style  */
    public static final String EMAIL_TEMPLATE_STYLE_FORMAL_LETTER = 'formalLetter';
    /*** Email Template Type HTML */
    public static final String TEMPLATE_TYPE_HTML = 'html';
    /*** Email Template name NVS Commercial Letterhead */
    public static final String EMAIL_TEMPLATE_SUBJECT = 'Lorem test curatio Lorem test curatio {!Name}';
    /*** Email Template name NVS Commercial Letterhead */
    public static final String EMAIL_TEMPLATE_HTMLVALUE = '<table border="0" cellpadding="5" width="600" cellspacing="5" height="400" > <tr valign="top" height="50" > <td tEditID="c1r1" style=" background-color:#FFFFFF; bEditID:r3st1; color:#000000; bLabel:main; font-size:12pt; font-family:arial;" aEditID="c1r1" locked="0" > <![CDATA[<span style="background-color: rgb(255, 255, 255); font-family: &quot;Segoe UI&quot;, system-ui, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, sans-serif; font-size: 14px;">Dear&nbsp;</span><span style="font-size: 12pt; background-color: rgb(255, 255, 255);"><font face="Segoe UI, system-ui, Apple Color Emoji, Segoe UI Emoji, sans-serif"><span style="font-size: 14px;">{!Name} and {!CCL_Therapy__r.CCL_Type__c},</span></font></span>]]></td> </tr> <tr valign="top" height="300" > <td tEditID="c1r2" style=" background-color:#FFFFFF; bEditID:r3st1; color:#000000; bLabel:main; font-size:12pt; font-family:arial;" aEditID="c1r2" locked="0" > <![CDATA[Test]]></td> </tr> <tr valign="top" height="50" > <td tEditID="c1r3" style=" background-color:#FFFFFF; bEditID:r3st1; color:#000000; bLabel:main; font-size:12pt; font-family:arial;" aEditID="c1r3" locked="0" > <![CDATA[]]></td> </tr> </table>';
    /*** Variable to store user therapy association prescriber record type*/
    public static final String PRISCRIBER_PRINCIPAL_INVESTIGATOR = Schema.SObjectType.CCL_User_Therapy_Association__c.getRecordTypeInfosByDeveloperName().get('CCL_Prescriber_Principal_Investigator').getRecordTypeId();
    /*** Variable to store user therapy association Internal record type*/
    public static final String INTERNAL_USER = Schema.SObjectType.CCL_User_Therapy_Association__c.getRecordTypeInfosByDeveloperName().get('CCL_Internal').getRecordTypeId();
    /*** Variable to store user therapy association External record type*/
    public static final String EXTERNAL_USER = Schema.SObjectType.CCL_User_Therapy_Association__c.getRecordTypeInfosByDeveloperName().get('CCL_External').getRecordTypeId();
    /*** Variable to store Record Detail Link*/
    public static final String RECORD_DETAIL_LINK_LITERAL_VALUE = 'RECORD_DETAIL_LINK';
	/*** Variable to store All Therapy name from custom setting*/
    public static final String GENERIC_THERAPY_NAME = 'All Therapy';
    /*** Variable to store inbound logistic role*/
    public static final String INBOUND_LOGISTIC_ROLE = 'Inbound Logistics';
    /*** Variable to store outbound logistic role*/
    public static final String OUTBOUND_LOGISTIC_ROLE = 'Outbound logistics';
    
}
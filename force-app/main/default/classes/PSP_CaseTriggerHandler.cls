/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the trigger handler class for Case Trigger 
*  @date            09/18/2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Sanjana Tripathy            September 18, 2019     Initial Version
****************************************************************************************************************/
public class PSP_CaseTriggerHandler extends PSP_trigger_Handler {
	/* trigger implementation */    
	final PSP_CaseTriggerImpl triggerImpl = new PSP_CaseTriggerImpl ();
    
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Before Insert method of Case
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    public override void beforeInsert () {
        final List<Case> caselist = new List<Case>();
        for (Case newCase :  (List<Case>)Trigger.new) { 
            if( String.isNotBlank(newCase.PSP_Care_Program_Enrollee_Product__c) && newCase.Subject.equalsIgnoreCase( PSP_ApexConstants.ADVERSE_EVENT)) {
                caselist.add(newCase);
            }
        }
        if(!caselist.isEmpty()) {
            triggerImpl.updateRecordtype(caselist);
        }
    }
   /*  public override void afterInsert () {
            List<Case> caselist=new List<Case>();
        // Set<Id> careProgramIds=new Set<Ids
           for (Case newCase :  (List<Case>)Trigger.new) {
               if(newCase.recordtypeId==PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CASE_RT_ADVERSE_EVENTS, Case.getSObjectType())
                  && newCase.PSP_Care_Program_Enrollee_Product__c!=null){
                      caselist.add(newCase);
                  }
           }
          if(!caselist.isEmpty()){
             system.debug('check 188'+caselist);
              //triggerImpl.updateRecordtype(caselist);
         }
     }*/
}
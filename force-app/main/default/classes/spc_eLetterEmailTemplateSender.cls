/*********************************************************************************************************
/*********************************************************************************************************
* @author Deloitte
* @date   June, 2018
* @description  spc_eLetterEmailTemplateSender for sending out email templates

*/

public without sharing class spc_eLetterEmailTemplateSender  {

	public static Id OUTBOUND_EMAIL_RECORD_TYPE = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_EMAIL_OUTBOUND)).getRecordTypeId();
	/*********************************************************************************************************
	* @author Deloitte
	* @date   June, 2018
	* @description  sendLetter for sending out email templates
	* @param id,List<spc_eLetter.DocumentWrapper>,eLetter and boolean

	*/
	public static void sendLetter(Id objectId, List<spc_eLetter.DocumentWrapper> documentRecipients, PatientConnect__PC_eLetter__c eLetter, Boolean sendToCurrentUser) {
		User currUser = spc_eLetterService.sendToMe();
		List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
		Set<Id> cdLinks = new Set<Id>();
		spc_Communication_framework_Parameters__c emailParam = spc_Communication_framework_Parameters__c.getOrgDefaults();
		spc_CommunicationMgr.RecipientHandler rHandler = new spc_CommunicationMgr.RecipientHandler();
		List<PatientConnect__PC_Document__c> documents = new List<PatientConnect__PC_Document__c>();
		try {
			String templateId = null;
			String templateName = eLetter.PatientConnect__PC_Template_Name__c;
			for(EmailTemplate emailTemplate : [SELECT Id, Body,HtmlValue FROM EmailTemplate WHERE DeveloperName =: templateName]){
            	 templateId = emailTemplate.Id;
            }
			List<Messaging.SingleEmailMessage> listEmailsToSend = new List<Messaging.SingleEmailMessage>();
			List<Attachment> attachmentList = new List<Attachment>();
			Map<Id, spc_eLetter.DocumentWrapper> accountIdToDocumentWrapper = new Map<Id, spc_eLetter.DocumentWrapper>();
			for (spc_eLetter.DocumentWrapper docWrapper : documentRecipients) {
				if (docWrapper.recipient.PatientConnect__PC_Email__c != null) {
					accountIdToDocumentWrapper.put(docWrapper.recipient.Id, docWrapper);
				}
			}

			for(Attachment attach  : [Select Id,Name,Body from  Attachment where parentId =:eLetter.Id ]){

				// Add to attachment file list
				Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
				efa.setContentType(spc_ApexConstants.Application_ContantType);
				efa.setFileName(attach.Name);
        		efa.setBody(attach.Body);
        		fileAttachments.add(efa);
			}
			Map<String, Contact> relatedContacts = new Map<String, Contact> ();
			List<String> fields = new List<String> {'Email'};
			for (Contact contact   : [SELECT Id, Email FROM Contact WHERE AccountId IN : accountIdToDocumentWrapper.keySet()]) {
				relatedContacts.put(contact.Email, contact);
			}
			Integer i = 0;
			for (Id accountId : accountIdToDocumentWrapper.keySet()) {
				List<String> emailId = new List<String>();
				Messaging.SingleEmailMessage aEmail = new Messaging.SingleEmailMessage();
				emailId.add(rHandler.getEmailAddress(accountIdToDocumentWrapper.get(accountId).recipient.PatientConnect__PC_Email__c));
				aEmail.setTargetObjectId(rHandler.getTargetObjectId(relatedContacts.get(accountIdToDocumentWrapper.get(accountId).recipient.PatientConnect__PC_Email__c)));
				aEmail.toaddresses = emailId;
				if (templateId != null) aEmail.setTemplateId(templateId);
				aEmail.setWhatId(objectId);
				if (sendToCurrentUser) {
					aEmail.setTargetObjectId(currUser.Id);
				}
				aEmail.setSaveAsActivity(false);
				if (emailParam != null && emailParam.OrgWideEmailAddress__c != null) {
					aEmail.setOrgWideEmailAddressId(emailParam.OrgWideEmailAddress__c);
				}

				aEmail.setFileAttachments(fileAttachments);

				documents.add(accountIdToDocumentWrapper.get(accountId).doc);
				listEmailsToSend.add(aEmail);
				if (sendToCurrentUser) {
					break;
				}
				i++;
			}
			if (!listEmailsToSend.isEmpty()) {
				List<Messaging.SendEmailResult> results = Messaging.sendEmail(listEmailsToSend, false);
				Integer index=0;
				for (Messaging.SingleEmailMessage msg : listEmailsToSend) {
					Boolean isSuccess = results[index].isSuccess();
					if (isSuccess) {
						String pdfBody = msg.getPlainTextBody();
						if (String.isBlank(pdfBody)) {
							String pdfhtml = msg.getHtmlBody();
							if (!String.isBlank(pdfhtml)) {pdfBody = pdfhtml;}
						}
						blob body;
						if (!String.isBlank(pdfBody)) { body = blob.valueOf(pdfBody);}

						Attachment attach = new Attachment();
						attach.Body = body;
						if (msg.getSubject() != null) {
							attach.Name = msg.getSubject() + '.html';
						} else {
							attach.Name = eLetter.Name + '.html';
						}
						attach.IsPrivate = false;
						attach.ParentId = documents[index].Id;
						documents[index].PatientConnect__PC_Document_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_STATUS_SENT);

						attach.ContentType = spc_ApexConstants.HTML_ContentType;

						if (body != null) { attachmentList.add(attach); }
						List<Messaging.EmailFileAttachment> fileAttach = msg.getFileAttachments();
						if (fileAttach.size() > 0) {
							for (Messaging.EmailFileAttachment singleFile : fileAttach) {
								Attachment emailAtt = new Attachment();
								emailAtt.Body = singleFile.getBody();
								emailAtt.Name = singleFile.getFileName();
								emailAtt.IsPrivate = false;
								emailAtt.ParentId = documents[index].Id;
								emailAtt.ContentType = spc_ApexConstants.Application_ContantType;
								attachmentList.add(emailAtt);
							}
						}
					} else {
						documents[index].PatientConnect__PC_Document_Status__c = spc_ApexConstants.DOC_STATUS_FAILED;
						documents[index].spc_Error_Log__c =  results[index].getErrors()[0].getMessage();
					}
					if (emailParam != null && emailParam.Org_Wide_Email_Address_Email__c != null) {
						documents[index].PatientConnect__PC_From_Email_Address__c = emailParam.Org_Wide_Email_Address_Email__c;
					}
					if (emailParam != null && emailParam.Org_Wide_Email_Address_Name__c != null) {
						documents[index].PatientConnect__PC_From_Name__c = emailParam.Org_Wide_Email_Address_Name__c;
					}
					documents[index].spc_to_Email_address__c = string.join(msg.getToAddresses(), ',');
					index ++;
				}
			}
			if (!attachmentList.isEmpty()) { insert attachmentList;}
			if (!documents.isEmpty()) { update documents; }
		} catch (Exception ex) {
			spc_Utility.logAndThrowException(ex);
		}
	}
}
/**
* @author Deloitte
* @date 08-Aug-2018
*
* @description This is the Test Class for spc_FieldSetController
*/

@isTest
public class spc_FieldSetControllerTest {

    @isTest
    public static void getFieldsTest() {

        List<spc_FieldSetController.FieldSetMember> result = spc_FieldSetController.getFields('Account', 'PatientConnect__EnrollmentWizardPhysicianPage');

        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Account');
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        Schema.FieldSet fs = fsMap.get('PatientConnect__EnrollmentWizardPhysicianPage');
        List<Schema.FieldSetMember> fieldSet = fs.getFields();
        spc_FieldSetController.FieldSetMember fsm = new spc_FieldSetController.FieldSetMember(fieldSet.get(0));
        spc_FieldSetController.SelectOptionWrapper selcOpt = new spc_FieldSetController.SelectOptionWrapper('Value', 'Test', false);
        List<String> resultTwo = spc_FieldSetController.getFieldsByName('Account', 'PatientConnect__EnrollmentWizardPhysicianPage');
        Account sampleAcct = new Account();
        String sampleobject = '';
        Date sampleDate = Date.today();
        Boolean fieldValue = True;
        Double sampleLocation = 2;
        object resultThree = spc_FieldSetController.castFieldValueBasedOnType('Phone', sampleobject, sampleAcct);
        object resultThree1 = spc_FieldSetController.castFieldValueBasedOnType('PersonEmail', sampleobject, sampleAcct);
        object resultThree2 = spc_FieldSetController.castFieldValueBasedOnType('PersonTitle', sampleobject, sampleAcct);
        object resultThree4 = spc_FieldSetController.castFieldValueBasedOnType('spc_Caregiver_Date_of_Birth__c', sampleDate  , sampleAcct);
        object resultThree5 = spc_FieldSetController.castFieldValueBasedOnType('PersonLastCUUpdateDate', sampleDate  , sampleAcct);
        object resultThree6 = spc_FieldSetController.castFieldValueBasedOnType('Type', sampleobject, sampleAcct);
        object resultThree7 = spc_FieldSetController.castFieldValueBasedOnType('spc_Potential_Duplicate__c', fieldValue, sampleAcct);
        object resultThree8 = spc_FieldSetController.castFieldValueBasedOnType('PatientConnect__PC_Caregiver_Details__c', sampleobject, sampleAcct);
        object resultThree9 = spc_FieldSetController.castFieldValueBasedOnType('PatientConnect__NumberofLocations__c', sampleLocation, sampleAcct);
    }

}
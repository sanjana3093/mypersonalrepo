/********************************************************************************************************
*  @author          Deloitte
*  @description     This class is used for storing all common methods which will be used by the Test Methods
*  @date            June 18, 2020
*  @version         1.0
Modification Log:
--------------------------------------------------------------------------------------------------------------
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------
Deloitte          June 23 2020         Initial Version
****************************************************************************************************************/
@istest
public class CCL_TestDataFactory {

    /**
    *  @author          Deloitte
    *  @description     Create an user record for the purpose of Unit Test activity.
    *  @param           String Profile name
    *  @return          User obj
    *  @date
    */
    public static User createTestUser(String sProfileName) {
        final String currentTime= String.valueOf(System.now().millisecond());
        final String randomNum= String.valueOf(Math.abs(Crypto.getRandomInteger()));

        final Profile assignedProfile = [SELECT Id FROM Profile WHERE Name = :sProfileName];
        return new User(LastName= 'test LastName',
                        FirstName= 'test FirstName',
                        profileid = assignedProfile.Id,
                        Alias='testUser',
                        Email='testUser@novartis.com',
                        emailencodingkey = 'UTF-8',
                        languagelocalekey = 'en_US',
                        localesidkey = 'en_US',
                        country = CCL_StaticConstants_MRC.VALUE_COUNTRY_US,
                        timezonesidkey = 'America/Los_Angeles',
                        username = 'testUser' + randomNum + currentTime +'@novartis.com');
    }
    /**
    *  @author          Deloitte
    *  @description     Create an user record  with GMT timezone for the purpose of Unit Test activity.
    *  @param           String Profile name
    *  @return          User obj
    *  @date		January 21,2021
    */
   public static User createTestGMTUser(String sProfileName) {
        final String currentTime= String.valueOf(System.now().millisecond());
        final String randomNum= String.valueOf(Math.abs(Crypto.getRandomInteger()));

        final Profile assignedProfile = [SELECT Id FROM Profile WHERE Name = :sProfileName];
        return new User(LastName= 'test LastName',
                        FirstName= 'test FirstName',
                        profileid = assignedProfile.Id,
                        Alias='testUser',
                        Email='testUser@novartis.com',
                        emailencodingkey = 'UTF-8',
                        languagelocalekey = 'en_US',
                        localesidkey = 'en_US',
                        country = CCL_StaticConstants_MRC.VALUE_COUNTRY_US,
                        timezonesidkey = 'Asia/Kolkata',
                        username = 'testUser' + randomNum + currentTime +'@novartis.com');
    }


    /**
*  @author          Deloitte
*  @description     Create an Contact record for the purpose of Unit Test activity. Note createTestParentAccount includes 1 DML & 1 SQOL for timezone.
*  @param           String Profile name, String Permission Set name
*  @return          Contact
*  @date
*/
    public static Contact createCommunityUser(Account testParentAccount) {
        final String currentTime= String.valueOf(System.now().millisecond());
        final String randomNum= String.valueOf(Math.abs(Crypto.getRandomInteger()));
        final Contact communityContact = new Contact (
            LastName = 'Test LastName Contact' + currentTime + randomNum,
            FirstName = 'Test Doctor' + randomNum,
            AccountId = testParentAccount.Id

        );
        return communityContact;
        }

    /**
    *  @author          Deloitte
    *  @description     Support the use of Permission Set Assign for Created users.
    *  @param           Permission Set name
    *  @return          PermssionSet
    *  @date
    */
    public static PermissionSetAssignment createPermissionSetForAssignment (String sPermissionSetName, User assignedUserContact) {

        PermissionSet permissionSetType = [SELECT Id FROM PermissionSet WHERE Name = :sPermissionSetName];
        return new PermissionSetAssignment(AssigneeId = assignedUserContact.id, PermissionSetId = permissionSetType.Id);
    }


    /**
    *  @author          Deloitte
    *  @description     Create an Timezone record for the purpose of Unit Test activity.
    *  @param
    *  @return          CCL_Time_Zone__c obj
    *  @date
    */
    public static CCL_Time_Zone__c createTimezoneTest (String timezoneKey) {
        final CCL_Time_Zone__c createTimeZoneObj = new CCL_Time_Zone__c();
        createTimeZoneObj.Name = System.currentTimeMillis() + timezoneKey;
        createTimeZoneObj.CCL_SAP_Time_Zone_Key__c = timezoneKey;
        createTimeZoneObj.CCL_External_ID__c = timezoneKey ;
        createTimeZoneObj.CCL_Saleforce_Time_Zone_Key__c = timezoneKey;
        return createTimeZoneObj;
    }

    /**
    *  @author          Deloitte
    *  @description     Create an Parent account for the purpose of Unit Test activity.
    *  @param
    *  @return          Account obj
    *  @date
    */

    public static Account createTestParentAccount(){
           CCL_Time_Zone__c newTimeZoneObj = new CCL_Time_Zone__c();
           newTimeZoneObj.Name = 'NewTime Zone';
           newTimeZoneObj.CCL_SAP_Time_Zone_Key__c = System.currentTimeMillis()+'687897-test';
           newTimeZoneObj.CCL_External_ID__c = '66677';
           newTimeZoneObj.CCL_Saleforce_Time_Zone_Key__c = System.currentTimeMillis()+'79788-test';
           insert newTimeZoneObj;
           CCL_Time_Zone__c timeZoneId = [SELECT Id FROM CCL_Time_Zone__c LIMIT 1];
            Account newAccountObj = new Account();
            newAccountObj.Name = 'Hospital 1';
            newAccountObj.Type = 'Prospect';
            newAccountObj.Phone = '(555) 555-5555';
            newAccountObj.ShippingCity = 'Denver';
            newAccountObj.ShippingCountry = CCL_StaticConstants_MRC.VALUE_COUNTRY_US;
            newAccountObj.ShippingCountryCode = 'US';
            newAccountObj.ShippingState = 'Colorado';
            newAccountObj.ShippingStreet = 'Rt.02 Jalan Soekarno-Hatta';
            newAccountObj.ShippingPostalCode = '11111';
            newAccountObj.CCL_Capability__c = 'L1O';
	        newAccountObj.CCL_Active__c = True;
            newAccountObj.CCL_Label_Compliant__c = 'SEC';
            newAccountObj.CCL_Type__c = CCL_StaticConstants_MRC.ACCOUNT_TYPE_VENDOR;
            newAccountObj.CCL_External_ID__c = '123456';
            newAccountObj.CCL_Time_Zone__c=timeZoneId.Id;
        return newAccountObj;
    }

    /**
    *  @author          Deloitte
    *  @description     Create an Account Apheresis Collection Center Location for the purpose of Unit Test activity.
    *  @param
    *  @return          Account obj
    *  @date
    */
   public static Account createAphCollectionCenter() {
        CCL_Time_Zone__c newTimeZoneObj = new CCL_Time_Zone__c();
           newTimeZoneObj.Name = 'NewTime Zone1';
           newTimeZoneObj.CCL_SAP_Time_Zone_Key__c = System.currentTimeMillis()+'687853-test';
           newTimeZoneObj.CCL_External_ID__c = '66623';
           newTimeZoneObj.CCL_Saleforce_Time_Zone_Key__c = System.currentTimeMillis()+'79793-test';
           insert newTimeZoneObj;
           CCL_Time_Zone__c timeZoneId = [SELECT Id FROM CCL_Time_Zone__c LIMIT 1];
            final Account newAccObj = new Account();
            newAccObj.Name = 'Aph Collection center';
            newAccObj.Type = 'Prospect';
            newAccObj.Phone = '(555) 555-5555';
            newAccObj.CCL_Time_Zone__c = timeZoneId.Id;
        newAccObj.ShippingCity = 'Denver';
        newAccObj.ShippingCountry = CCL_StaticConstants_MRC.VALUE_COUNTRY_US;
        newAccObj.ShippingState = 'Colorado';
        newAccObj.ShippingStreet = 'Rt.02 Jalan Soekarno-Hatta';
        newAccObj.ShippingPostalCode = '11111';
        newAccObj.CCL_Capability__c = CCL_StaticConstants_MRC.APHCOLLECTIONSITE;
        newAccObj.CCL_Active__c = True;
        newAccObj.CCL_Label_Compliant__c = 'SEC';
        newAccObj.CCL_Type__c = CCL_StaticConstants_MRC.ACCOUNT_TYPE_CUSTOMER;
        newAccObj.CCL_External_ID__c = '12345645';
        return newAccObj;
    }
    /**
    *  @author          Deloitte
    *  @description     Create an Account Pickup Location for the purpose of Unit Test activity.
    *  @param
    *  @return          Account obj
    *  @date
    */
    public static Account createAphPickupLoc() {

         CCL_Time_Zone__c newTimeZoneObj = new CCL_Time_Zone__c();
           newTimeZoneObj.Name = 'NewTime Zone2';
           newTimeZoneObj.CCL_SAP_Time_Zone_Key__c = System.currentTimeMillis()+'687832-test';
           newTimeZoneObj.CCL_External_ID__c = '66612';
           newTimeZoneObj.CCL_Saleforce_Time_Zone_Key__c = System.currentTimeMillis()+'79763-test';
           insert newTimeZoneObj;
           CCL_Time_Zone__c timeZoneId = [SELECT Id FROM CCL_Time_Zone__c LIMIT 1];
        final Account newPickupAccobj = new Account();
            newPickupAccobj.Name = 'Aph Pickup location';
            newPickupAccobj.Type = 'Prospect';
            newPickupAccobj.CCL_Time_Zone__c = timeZoneId.Id;
            newPickupAccobj.Phone = '(555) 555-5555';
            newPickupAccobj.ShippingCity = 'Denver';
            newPickupAccobj.ShippingCountry = CCL_StaticConstants_MRC.VALUE_COUNTRY_US;
            newPickupAccobj.ShippingState = 'Colorado';
            newPickupAccobj.ShippingStreet = 'Rt.02 Jalan Soekarno-Hatta';
            newPickupAccobj.ShippingPostalCode = '11111';
            newPickupAccobj.CCL_Capability__c = CCL_StaticConstants_MRC.PICKUPSITE;
            newPickupAccobj.CCL_Active__c= True;
            newPickupAccobj.CCL_Label_Compliant__c = 'SEC';
            newPickupAccobj.CCL_Type__c = CCL_StaticConstants_MRC.ACCOUNT_TYPE_VENDOR;
            newPickupAccobj.CCL_External_ID__c = '12345698';

            return newPickupAccobj;
    }

    /**
    *  @author          Deloitte
    *  @description     Create an Infusion Center for the purpose of Unit Test activity.
    *  @param
    *  @return          Account obj
    *  @date
    */
    public static Account infusionCenter() {
        final Account newInfCenterObj = new Account();
           final CCL_Time_Zone__c newTimeZoneObj = new CCL_Time_Zone__c();
           newTimeZoneObj.Name = 'NewTime Zone3';
           newTimeZoneObj.CCL_SAP_Time_Zone_Key__c = '687897907-test';
           newTimeZoneObj.CCL_External_ID__c = '6667758';
           newTimeZoneObj.CCL_Saleforce_Time_Zone_Key__c = '7978813-test';
           insert newTimeZoneObj;
           CCL_Time_Zone__c timeZoneId = [SELECT Id FROM CCL_Time_Zone__c where Name = 'NewTime Zone3'LIMIT 1];
            newInfCenterObj.Name = 'Infusion Center';
            newInfCenterObj.Type = 'Prospect';
            newInfCenterObj.Phone = '(555) 555-5555';
            newInfCenterObj.ShippingCity = 'Denver';
            newInfCenterObj.ShippingCountry = CCL_StaticConstants_MRC.VALUE_COUNTRY_US;
            newInfCenterObj.ShippingState = 'Colorado';
            newInfCenterObj.ShippingStreet = 'Rt.02 Jalan Soekarno-Hatta';
            newInfCenterObj.ShippingPostalCode = '11111';
            newInfCenterObj.CCL_Capability__c = CCL_StaticConstants_MRC.APHCOLLECTIONSITE;
            newInfCenterObj.CCL_Active__c = True;
            newInfCenterObj.CCL_Label_Compliant__c = 'SEC';
            newInfCenterObj.CCL_Type__c = CCL_StaticConstants_MRC.ACCOUNT_TYPE_VENDOR;
            newInfCenterObj.CCL_External_ID__c = '12345678';
            newInfCenterObj.CCL_Time_Zone__c=timeZoneId.Id;
            return newInfCenterObj;
    }

  /**
    *  @author          Deloitte
    *  @description     Create an Account Apheresis Center Location for the purpose of Unit Test activity.
    *  @param
    *  @return          Account obj
    *  @date
    */
    public static Account createTestAph(){
           CCL_Time_Zone__c newTimeZoneObj = new CCL_Time_Zone__c();
           newTimeZoneObj.Name = 'NewTime Zone2';
           newTimeZoneObj.CCL_SAP_Time_Zone_Key__c = '68789790-test';
           newTimeZoneObj.CCL_External_ID__c = '6667788';
           newTimeZoneObj.CCL_Saleforce_Time_Zone_Key__c = '797881-test';
           insert newTimeZoneObj;
           CCL_Time_Zone__c timeZoneId = [SELECT Id FROM CCL_Time_Zone__c where Name = 'NewTime Zone2'LIMIT 1];
            Account newAccountObj = new Account();
            newAccountObj.Name = 'Hospital 16';
            newAccountObj.Type = 'Prospect';
            newAccountObj.Phone = '(555) 555-5555';
            newAccountObj.ShippingCity = 'Denver';
            newAccountObj.ShippingCountry = CCL_StaticConstants_MRC.VALUE_COUNTRY_US;
            newAccountObj.ShippingCountryCode = 'US';
            newAccountObj.ShippingState = 'Colorado';
            newAccountObj.ShippingStreet = 'Rt.02 Jalan Soekarno-Hatta';
            newAccountObj.ShippingPostalCode = '11111';
            newAccountObj.CCL_Capability__c = 'L1O';
           newAccountObj.CCL_Active__c= boolean.valueOf(CCL_StaticConstants_MRC.VALUE_YES);
            newAccountObj.CCL_Label_Compliant__c = 'SEC';
            newAccountObj.CCL_Type__c = CCL_StaticConstants_MRC.ACCOUNT_TYPE_VENDOR;
            newAccountObj.CCL_External_ID__c = '1234564';
            newAccountObj.CCL_Time_Zone__c=timeZoneId.Id;
            return newAccountObj;
    }

    /**
    *  @author          Deloitte
    *  @description     Create an Account for the purpose of Unit Test activity.
    *  @param
    *  @return          Therapy obj
    *  @date
    */
    public static Account createShipToLocation() {
        final Account newShipToLOcObj = new Account();
        final CCL_Time_Zone__c newTimeZoneObj = new CCL_Time_Zone__c();
        newTimeZoneObj.Name = 'NewTime Zone4';
        newTimeZoneObj.CCL_SAP_Time_Zone_Key__c = '68789790711-test';
        newTimeZoneObj.CCL_External_ID__c = '666775867';
        newTimeZoneObj.CCL_Saleforce_Time_Zone_Key__c = '797881311-test';
        insert newTimeZoneObj;
        final CCL_Time_Zone__c timeZoneId = [SELECT Id FROM CCL_Time_Zone__c where Name = 'NewTime Zone4'LIMIT 1];
        newShipToLOcObj.Name = 'Ship To location 1';
        newShipToLOcObj.Type = 'Prospect';
        newShipToLOcObj.Phone = '(555) 555-5555';
        newShipToLOcObj.ShippingCity = 'Denver';
        newShipToLOcObj.ShippingCountry = CCL_StaticConstants_MRC.VALUE_COUNTRY_US;
        newShipToLOcObj.ShippingState = 'Colorado';
        newShipToLOcObj.ShippingStreet = 'Rt.02 Jalan Soekarno-Hatta';
        newShipToLOcObj.ShippingPostalCode = '11111';
        newShipToLOcObj.CCL_Capability__c = CCL_StaticConstants_MRC.SHIPTOLOCVAR;
        newShipToLOcObj.CCL_Active__c = True;
        newShipToLOcObj.CCL_Label_Compliant__c = 'SEC';
        newShipToLOcObj.CCL_Type__c = CCL_StaticConstants_MRC.ACCOUNT_TYPE_VENDOR;
        newShipToLOcObj.CCL_External_ID__c = '1234563';
        newShipToLOcObj.CCL_Time_Zone__c=timeZoneId.Id;
        return newShipToLOcObj;
    }

    /**
    *  @author          Deloitte
    *  @description     Create an Account Plant for the purpose of Unit Test activity.
    *  @param
    *  @return          Account obj
    *  @date
    */
    public static Account createPlantAccount(String timezoneRecordId) {

        Account newAccountObj = new Account();
        newAccountObj.Name = 'Morris Plains Plant';
        newAccountObj.Type = 'Prospect';
        newAccountObj.Phone = '(555) 555-1235';
        newAccountObj.ShippingCountry = CCL_StaticConstants_MRC.VALUE_COUNTRY_US;
        newAccountObj.ShippingCountryCode = CCL_StaticConstants_MRC.PICKLIST_VALUE_COUNTRY_US;
        newAccountObj.CCL_Capability__c = 'L1O';
        newAccountObj.CCL_Active__c = True;
        newAccountObj.RecordTypeId = CCL_StaticConstants_MRC.ACCOUNT_RECORDTYPE_PLANT;
        newAccountObj.CCL_Type__c = CCL_StaticConstants_MRC.ACCOUNT_TYPE_VENDOR;
        newAccountObj.CCL_External_ID__c = '3049809';
        newAccountObj.CCL_Time_Zone__c=timezoneRecordId;
        
        return newAccountObj;
    }

    /**
    *  @author          Deloitte
    *  @description     Create a Therapy for the purpose of Unit Test activity.
    *  @param
    *  @return          Therapy obj
    *  @date
    */
    public static CCL_Therapy__c createTestTherapy(){
        final CCL_Therapy__c newTherapyObj = new CCL_Therapy__c();
        newTherapyObj.Name = 'Therapy 1';
        newTherapyObj.CCL_Type__c = 'Commercial';
        newTherapyObj.CCL_Therapy_Description__c = 'therapy';
        newTherapyObj.CCL_Status__c = 'Active';
        return newTherapyObj;
    }

    /**
    *  @author          Deloitte
    *  @description     Create a Therapy for the purpose of Unit Test activity.
    *  @param
    *  @return          Therapy obj
    *  @date
    */
    public static CCL_Therapy__c createCommercialTherapy(){
        CCL_Therapy__c newTherObj = new CCL_Therapy__c();
        newTherObj.Name = 'Therapy1Commercial';
        newTherObj.CCL_Type__c = 'Commercial';
        newTherObj.CCL_Therapy_Description__c = 'therapy';
        newTherObj.CCL_Status__c = 'Active';
        insert newTherObj;
        return newTherObj;
    }

    /**
    *  @author          Deloitte
    *  @description     Create a Site Therapy Assoication for the purpose of Unit Test activity.
    *  @param           Account record Id and Therapy Record Id
    *  @return          Therapy Association obj Site Record Type
    *  @date
    */
    public static CCL_Therapy_Association__c createTestSiteTherapyAssoc(String hospital,String therapy) {
        final CCL_Therapy_Association__c siteTherapyAssociation = new CCL_Therapy_Association__c();
        siteTherapyAssociation.CCL_Site__c = hospital;
        siteTherapyAssociation.CCL_Therapy__c = therapy;
        siteTherapyAssociation.CCL_Hospital_Patient_ID_Opt_In__c = CCL_StaticConstants_MRC.VALUE_YES;
        siteTherapyAssociation.CCL_Infusion_Data_Collection__c = CCL_StaticConstants_MRC.VALUE_NO;
        siteTherapyAssociation.RecordTypeId = CCL_StaticConstants_MRC.THERAPY_ASSOCIATION_RECORDTYPE_SITE;
        return siteTherapyAssociation;
    }


    /**
    *  @author          Deloitte
    *  @description     Create a Country Therapy Assoication for the purpose of Unit Test activity.
    *  @param           Account record Id and Therapy Record Id
    *  @return          Therapy Association obj Country Record Type
    *  @date
    */
        public static CCL_Therapy_Association__c createTestCountryTherapyAssoc(String hospital,String therapy) {
        final CCL_Therapy_Association__c countryTherapyAssociation = new CCL_Therapy_Association__c();
        countryTherapyAssociation.CCL_Hosp_Purchase_Order_Opt_In__c = CCL_StaticConstants_MRC.VALUE_YES;
        countryTherapyAssociation.CCL_Therapy__c = therapy;
        countryTherapyAssociation.CCL_Maximum_Age__c = CCL_StaticConstants_MRC.VALUE_FIFTY;
        countryTherapyAssociation.CCL_Product_Acceptance_Confirmation_Text__c =CCL_StaticConstants_MRC.PRODUCT_ACCEPTANCE_TEXT;
        countryTherapyAssociation.CCL_Commercial_Physician_Attestation__c = CCL_StaticConstants_MRC.TEXT_AREA_GREATER_255;
        countryTherapyAssociation.CCL_Payment_Section_Text__c = CCL_StaticConstants_MRC.TEXT_AREA_LESS_255;
        countryTherapyAssociation.CCL_Research_with_Bio_Samples_Opt_In__c = CCL_StaticConstants_MRC.VALUE_YES;
        countryTherapyAssociation.CCL_Country__c = CCL_StaticConstants_MRC.PICKLIST_VALUE_COUNTRY_US;
        countryTherapyAssociation.CCL_Veteran_Affairs_Applicable__c = CCL_StaticConstants_MRC.VALUE_YES;
        countryTherapyAssociation.CCL_Minimum_Age__c = CCL_StaticConstants_MRC.VALUE_ZERO;
        countryTherapyAssociation.RecordTypeId = CCL_StaticConstants_MRC.THERAPY_ASSOCIATION_COUNTRY;
        countryTherapyAssociation.CCL_Capture_Patient_Country_of_Residence__c = CCL_StaticConstants_MRC.VALUE_YES;
        return countryTherapyAssociation;
    }
    /**
    *  @author          Deloitte
    *  @description     Create a Contact for the purpose of Unit Test activity.
    *  @param
    *  @return          contact obj
    *  @date
    */
     public static Contact createTestContact(){
    	Contact newContact = new Contact();
         newContact.LastName = 'TestLastName';
         newContact.FirstName = 'TestFirstName' ;
         return newContact;
        }

    /**
    *  @author          Deloitte
    *  @description     Create a Product Requet for the purpose of Unit Test activity.
    *  @param
    *  @return          Product Request Obj
    *  @date
    */
         public static CCL_Order__c createTestPRFOrder() {
	 final CCL_Therapy__c therapy= CCL_Test_SetUp.createTestTherapy('Test');
          final CCL_Time_Zone__c tmz =CCL_Test_SetUp.createTestTimezone('EST');
          final Account orderingHospitalAccount= CCL_Test_SetUp.createTestAccount('Test OrderingHospital',tmz.Id);
        CCL_Order__c prfData = new CCL_Order__c();
        prfData.Name='Novartis Batch ID';
        prfData.CCL_Therapy__c=therapy.Id;
        prfData.CCL_Ordering_Hospital__c=orderingHospitalAccount.Id;

        prfData.CCL_First_Name__c='First Name';
        prfData.CCL_Last_Name__c='Last Name';
        prfData.CCL_Middle_Name__c='N';
        prfData.CCL_Suffix__c='Sr.';
        prfData.RecordTypeId=CCL_StaticConstants_MRC.ORDER_RECORDTYPE_CLINICAL;
        prfData.CCL_Date_of_DOB__c='1';
        prfData.CCL_Month_of_DOB__c='JAN';
        prfData.CCL_Year_of_DOB__c='1990';
        prfData.CCL_Hospital_Patient_ID__c='1234';
        prfData.CCL_Patient_Weight__c=90;
        prfData.CCL_Patient_Age_At_Order__c=30;
        prfData.CCL_Treatment_Protocol_Subject_ID__c='CTL198088';
        prfData.CCL_Protocol_Center_Number__c='1234';
        prfData.CCL_Protocol_Subject_Number__c='123';
        prfData.CCL_Consent_for_Additional_Research__c=TRUE;
        prfData.CCL_PRF_Ordering_Status__c='PRF_Submitted';
        prfData.CCL_Hospital_Purchase_Order_Number__c='34834349';
        prfData.CCL_Physician_Attestation__c= TRUE;
        prfData.CCL_Patient_Eligibility__c= TRUE;
        prfData.CCL_VA_Patient__c= TRUE;
        prfData.CCL_Planned_Apheresis_Pick_up_Date_Time__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        prfData.CCL_Manufacturing_Slot_ID__c='123567';
        prfData.CCL_Value_Stream__c=89;
        prfData.CCL_Estimated_FP_Delivery_Date__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        prfData.CCL_Scheduler_Missing__c=TRUE;
        prfData.CCL_Returning_Patient__c=TRUE;
        prfData.CCL_Estimated_Manufacturing_Start_Date__c=date.newInstance(2014, 9, 15);
        insert prfData;

        return prfData;
        }

     /**
    *  @author          Deloitte
    *  @description     Create a Product Requet for the purpose of Unit Test activity.
    *  @param
    *  @return          Product Request Obj
    *  @date
    */
         public static CCL_Order__c createTestPRFOrderNew() {
	 final CCL_Therapy__c therapy= CCL_Test_SetUp.createTestTherapy('Test');
          final CCL_Time_Zone__c tmz =CCL_Test_SetUp.createTestTimezone('EST');
          final Account orderingHospitalAccount= CCL_Test_SetUp.createTestAccount('Test OrderingHospital',tmz.Id);
        CCL_Order__c prfData = new CCL_Order__c();
        prfData.Name='Novartis Batch ID2';
        prfData.CCL_Therapy__c=therapy.Id;
        prfData.CCL_Ordering_Hospital__c=orderingHospitalAccount.Id;

        prfData.CCL_First_Name__c='First Name 1';
        prfData.CCL_Last_Name__c='Last Name 1';
        prfData.CCL_Middle_Name__c='N';
        prfData.CCL_Suffix__c='Sr.';
        prfData.RecordTypeId=CCL_StaticConstants_MRC.ORDER_RECORDTYPE_CLINICAL;
        prfData.CCL_Date_of_DOB__c='1';
        prfData.CCL_Month_of_DOB__c='JAN';
        prfData.CCL_Year_of_DOB__c='1990';
        prfData.CCL_Hospital_Patient_ID__c='4321';
        prfData.CCL_Patient_Weight__c=65;
        prfData.CCL_Patient_Age_At_Order__c=29;
        prfData.CCL_Treatment_Protocol_Subject_ID__c='CTL198088';
        prfData.CCL_Protocol_Center_Number__c='4321';
        prfData.CCL_Protocol_Subject_Number__c='321';
        prfData.CCL_Consent_for_Additional_Research__c=TRUE;
        prfData.CCL_PRF_Ordering_Status__c='PRF_Submitted';
        prfData.CCL_Hospital_Purchase_Order_Number__c='34834378';
        prfData.CCL_Physician_Attestation__c= TRUE;
        prfData.CCL_Patient_Eligibility__c= TRUE;
        prfData.CCL_VA_Patient__c= TRUE;
        prfData.CCL_Planned_Apheresis_Pick_up_Date_Time__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        prfData.CCL_Manufacturing_Slot_ID__c='123576';
        prfData.CCL_Value_Stream__c=89;
        prfData.CCL_Estimated_FP_Delivery_Date__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        prfData.CCL_Scheduler_Missing__c=TRUE;
        prfData.CCL_Returning_Patient__c=TRUE;
        prfData.CCL_Estimated_Manufacturing_Start_Date__c=date.newInstance(2014, 9, 15);
        prfData.CCL_MiddleName_COI__c = true;
        prfData.CCL_Day_of_Birth_COI__c = true;
        prfData.CCL_FirstName_COI__c = true;
        prfData.CCL_Initials_COI__c = true;
        prfData.CCL_LastName_COI__c = true;
        prfData.CCL_Month_of_Birth_COI__c = true;
        prfData.CCL_Sufix_COI__c = true;
        prfData.CCL_Year_of_Birth_COI__c = true;
        prfData.CCL_Secondary_COI_Patient_ID__c = true;
        prfData.CCL_Order_Approval_Eligibility__c = true;
        prfData.CCL_Novartis_Batch_ID__c = 'bdjflla';

        insert prfData;
             CCL_Apheresis_Data_Form__c adf = createTestApheresisDataForm();
             adf.CCL_Order__c = prfData.Id;
             insert adf;
             CCL_Shipment__c shp = createTestFinishedProductShipment(prfData.id,null);
             shp.CCL_Order_Apheresis__c = prfData.id;
             insert shp;
        return prfData;
        }

		/**
		*  @author          Deloitte
		*  @description     Create a content document and a content version for contentDocument_trigger
		*  @param
		*  @return          content document object
		*  @date            30 July 2020
		*/
		public static list<ContentDocument> createTestContentDocumenet() {
			//Create Document
			ContentVersion cv = new ContentVersion();
			cv.Title = 'Test Document';
			cv.PathOnClient = 'TestDocument.pdf';
			cv.VersionData = Blob.valueOf('Test Content');
			cv.IsMajorVersion = true;
			Insert cv;
			return [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument WHERE Title='Test Document' WITH security_enforced ];
		}

		/**
*  @author          Deloitte
*  @description     Create a FinishedProduct Shipment for the purpose of Unit Test activity.
*  @param           CCL_Order Record Id And Therapy Record Id
*  @return          Shipment obj
*  @date
*/
    public static CCL_Shipment__c createTestFinishedProductShipment(String order,String Therapy) {
        CCL_Shipment__c objFPShipment = new CCL_Shipment__c();
        objFPShipment.RecordTypeId = CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP;
        objFPShipment.Name = 'Finished Product Shipment';
        objFPShipment.CCL_Order__c = order;
        objFPShipment.CCL_Patient_Name__c = 'Test User';
        objFPShipment.CCL_Date_of_Birth__c = '01-MAR-1990';
        objFPShipment.CCL_Protocol_Subject_Hospital_Patient__c = '34834349';
        objFPShipment.CCL_Indication_Clinical_Trial__c = therapy;
        objFPShipment.CCL_Shipment_Reason__c = 'RE';
		objFPShipment.CCL_Bypass_Shipment__c = true;
        //objFPShipment.CCL_Status__c = 'Apheresis Pick Up Planned';
        objFPShipment.CCL_Hospital_Purchase_Order_Number__c='987668';
        objFPShipment.CCL_Estimated_FP_Delivery_Date_Time__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        //insert objFPShipment;
        return objFPShipment;
    }

/**
*  @author          Deloitte
*  @description     Create a Apheresis Data Form(ADF) object for the purpose of Unit Test activity.
*  @param
*  @return          Apheresis Data Form Obj
*  @date
*/
    public static CCL_Apheresis_Data_Form__c createTestApheresisDataForm(){
        CCL_Apheresis_Data_Form__c testADF = new CCL_Apheresis_Data_Form__c();
        testADF.CCL_Date_Of_Birth__c = date.newinstance(1960, 2, 17);
        testADF.CCL_Treatment_Protocol_SubID_HospitalID__c = '47502';
        testADF.CCL_Patient_Weight__c = 90;
        testADF.CCL_Returning_Patient__c = True;
        testADF.CCL_Est_Apheresis_Shipment_Date__c = datetime.newInstance(2019, 8, 25, 12, 30, 0);
        testADF.CCL_Patient_Name__c='Test Apheresis Data Form';
        return testADF;
    }

    /**
*  @author          Deloitte
*  @description     Create a Apheresis Shipment for the purpose of Unit Test activity.
*  @param           CCL_Order Record Id And Therapy Record Id
*  @return          Shipment
*  @date
*/
    public static CCL_Shipment__c createTestApheresisShipment(String order,String Therapy){
        String apheresisShipmentRecordTypeId = CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_AP;
        CCL_Shipment__c objApheresisShipment = new CCL_Shipment__c();
        objApheresisShipment.RecordTypeId = apheresisShipmentRecordTypeId;
        objApheresisShipment.Name = 'Apheresis Shipment';
        objApheresisShipment.CCL_Order__c   = order;
        objApheresisShipment.CCL_Patient_Name__c = 'Test User';
        objApheresisShipment.CCL_Date_of_Birth__c = '1-JAN-1990';
        objApheresisShipment.CCL_Protocol_Subject_Hospital_Patient__c = '97377632';
        objApheresisShipment.CCL_Indication_Clinical_Trial__c = Therapy;
        objApheresisShipment.CCL_Shipment_Reason__c = 'RE';
        objApheresisShipment.CCL_Status__c = 'Apheresis Pick Up Planned';
        objApheresisShipment.CCL_Estimated_Aph_Pick_up_Date_Time__c = system.now()+5;
        objApheresisShipment.CCL_Planned_Apheresis_Pickup_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0);
        // insert objApheresisShipment;
        return objApheresisShipment;
    }
/**
*  @author          Deloitte
*  @description     Create a Address for the purpose of Unit Test activity.
*  @param           AccountId,language
*  @return          Addresses
*  @date
*/
    public static CCL_Address__c createTestAddress(Id accountId,String language) {
        CCL_Address__c add=new CCL_Address__c();
        add.Name='Test';
        add.CCL_Site__c=accountId;
        add.CCL_Complete_Address__c='Testt';
        add.CCL_City__c='NY';
        add.CCL_External_ID__c='1234';
        add.CCL_Language__c=language;
        add.CCL_Site_Name__c='test';
        add.CCL_Country__c='US';
        add.CCL_Postal_Code__c='1234';
        add.CCL_Street__c='XYZ';
        return add;

    }

/**
*  @author          Deloitte
*  @description     Create the Language Therapy Association object to support Unit Tests
*  @param           Therapy Id
*  @return          Language Therapy Association
*/
    public static CCL_Language_Therapy_Association__c createLanguageTherapyAssociation(CCL_Therapy_Association__c countryTherapyAssociation) {

        final CCL_Language_Therapy_Association__c testLanguageTherapy = new CCL_Language_Therapy_Association__c();
        testLanguageTherapy.CCL_Clinical_Patient_Eligibility_Text__c = CCL_StaticConstants_MRC.TEXT_AREA_LESS_255;
        testLanguageTherapy.CCL_Commercial_Physician_Attestation__c = CCL_StaticConstants_MRC.TEXT_AREA_LESS_255;
        testLanguageTherapy.CCL_Product_Acceptance_Confirmation_Text__c = CCL_StaticConstants_MRC.TEXT_AREA_LESS_255;
        testLanguageTherapy.CCL_Language__c =CCL_StaticConstants_MRC.GLOBAL_PICKLIST_ENG_US_VALUE; 
        testLanguageTherapy.CCL_PRF_Sites_Selection_Discrepancy_Text__c = CCL_StaticConstants_MRC.TEXT_AREA_LESS_255;
        testLanguageTherapy.CCL_Therapy_Association__c = countryTherapyAssociation.Id;
        return testLanguageTherapy;
    }
}

public
with sharing class PSP_ContactController {
public
  PSP_ContactController() {}
  /* @AuraEnabled(Cacheable = true) public static List<Account> getAllPatients()
   { return [ SELECT Name, RecordTypeId, PSP_Patient_ID__c, CreatedDate,
              ShippingCity, ShippingState,
              HealthCloudGA__Active__c FROM Account where RecordType.Name
              =:Label.PSP_Patient_Record_Type ];
   }*/
  @AuraEnabled(
      Cacheable = true) public static List<ContactRecord> getAllHCPs() {
    final List<ContactRecord> contacts = new List<ContactRecord>();
    try {
      for (Contact contact :
           [ SELECT Name, RecordType.Name, PSP_Physician_ID__c,
             PSP_Specialty__c, PSP_Status__c, Account.Name, Account.ID,
             Phone FROM Contact where RecordType.Name
             =:Label.PSP_HCP_Record_Type WITH SECURITY_ENFORCED ]) {
        final ContactRecord contactWrapper = new ContactRecord(contact);
        System.debug(contactWrapper);
        contacts.add(contactWrapper);
      }
      return contacts;
    } catch (AuraHandledException e) {
      System.debug(e.getMessage());
      throw e;
    }
  }

public
  class ContactRecord {
    /* getter class to get Id*/
    @AuraEnabled public Id contactId {
      get;
      set;
    }
    /* getter class to get Name*/
    @AuraEnabled public String name {
      get;
      set;
    }
    /* getter class to get nameurl in wrapper class*/
    @AuraEnabled public String nameURL {
      get;
      set;
    }
    /* getter class to get isactive in wrapper class*/
    @AuraEnabled public String professionalID {
      get;
      set;
    }
    /* getter class to get isactive in wrapper class*/
    @AuraEnabled public String speciality {
      get;
      set;
    }
    /* getter class to get isactive in wrapper class*/
    @AuraEnabled public String status {
      get;
      set;
    }
    /* getter class to get isactive in wrapper class*/
    @AuraEnabled public String accountsName {
      get;
      set;
    }
    /* getter class to get nameurl in wrapper class*/
    @AuraEnabled public String accountsNameURL {
      get;
      set;
    }
    /* getter class to get isactive in wrapper class*/
    @AuraEnabled public String phone {
      get;
      set;
    }
  public
    ContactRecord(Contact contact) {
      contactId = contact.Id;
      name = contact.Name;
      nameURL = '/' + contact.Id;
      professionalID = contact.PSP_Physician_ID__c;
      speciality = contact.PSP_Specialty__c;
      status = contact.PSP_Status__c;
      accountsName = contact.Account.Name;
      accountsNameURL = '/' + contact.Account.Id;
      phone = contact.Phone;
    }
  }
}
/*********************************************************************************************************
class Name      : PSP_Search_Component_Controller 
Description		: Controller Class for LWC Component for Product and Service Management from Care Program
@author		    : Deloitte
@date       	: July 31, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Deloitte            July 31, 2019          Initial Version
****************************************************************************************************************/ 
public with sharing class PSP_Search_Component_Controller {
    /* Map of Product Id and Care Program Product */
    private static  Map<String, CareProgramProduct> prodCareProgProd = new Map<String,CareProgramProduct>();
    /* Map of Product Id product */
    private static  Map<String, String> productCPPIdMap = new Map<String,String>();
    /* Care prog product Id */
    private static final String CARE_PROG_PROD_ID = 'careProgProdId';
    /*private constructor */
    @TestVisible 
    private PSP_Search_Component_Controller () {
        //Private constructor 
    }
    
    
    /********************************************************************************************************
*  @author          Deloitte
*  @description     load Variables method for setting the variables in static context.
*  @param           carProgram Id
*  @date            Aug 12, 2019
*********************************************************************************************************/
    
    private static void loadVariables(Id careProgramId) {
        final Map<String, CareProgramProduct> careProgProd = new Map<String,CareProgramProduct>();
        for(CareProgramProduct careProgramProd : [Select Id,Name,ProductId,PSP_Care_Plan_Template__c,PSP_Active__c,PSP_Care_Plan_Template__r.Name from CareProgramProduct where CareProgramId =:careProgramId WITH SECURITY_ENFORCED]) {
            careProgProd.put(careProgramProd.Id,careProgramProd);
            if(String.isNotBlank(careProgramProd.ProductId)) {
                prodCareProgProd.put(careProgramProd.ProductId,careProgramProd);
                productCPPIdMap.put(careProgramProd.ProductId,careProgramProd.Id);
            }
        }
    }
    /********************************************************************************************************
*  @author          Deloitte
*  @description     method for fetching current careprogram record.
*  @param1          RecordID
*  @date            Aug 12, 2019
*********************************************************************************************************/
    @AuraEnabled(cacheable=true)
    public static CareProgram getProduct(Id recordId) {
        CareProgram cProg = new CareProgram();
        try {
            if (CareProgram.sObjectType.getDescribe().isAccessible()) {
                cProg = [Select Id,Name from CareProgram where ID =: recordId  WITH SECURITY_ENFORCED];
            }
        } catch(AuraHandledException ex) {
            System.debug(ex.getMessage());
            throw  ex;
        }
        return cProg;
    }
    /********************************************************************************************************
*  @author          Deloitte
*  @description     method for fetching Product records.
*  @param1          Object String
*  @param2          search key word String
*  @param3          record Id
*  @date            Aug 12, 2019
*********************************************************************************************************/
    @AuraEnabled
    public static List < ProductRecord > fetchRecords(String objParam, String searchKeyword, Id recordId) {
        loadVariables(recordId);
        final List < ProductRecord > searchRecords = new List < ProductRecord > ();
        try {	
            final String nameFilter = '%' + searchKeyword + '%';
            if (Product2.sObjectType.getDescribe().isAccessible()) {
                for (Product2 searchRec: [Select Id, Name, PSP_Family__c, PSP_Family__r.Name, PSP_Service_Type__c, PSP_Franchise__c, PSP_Portfolio__c, IsActive,StockKeepingUnit from Product2 where RecordType.Name =: objParam and Name like: nameFilter and Id not in: prodCareProgProd.keySet() and IsActive = true WITH SECURITY_ENFORCED]) {
                    final ProductRecord product = new ProductRecord(searchRec, false);
                    searchRecords.add(product);
                }
            }
            return searchRecords;
        } catch(AuraHandledException ex) {
            System.debug(ex.getMessage());
            throw ex;
        }
    }
    /********************************************************************************************************
*  @author          Deloitte
*  @description     get product List method 
*  @param           record Id
*  @date            Aug 12, 2019
*********************************************************************************************************/
    @AuraEnabled(cacheable = true)
    public static List < ProductRecord > getProductList(Id recordId) {
        final List < ProductRecord > products = new List < ProductRecord > ();
        loadVariables(recordId);
        try {
            if (CareProgramProduct.sObjectType.getDescribe().isAccessible()) {
                for (CareProgramProduct careProgramProd: [Select Id, Name, CareProgram.Name,PSP_Default__c, ProductId,PSP_Care_Plan_Template__c, PSP_Active__c, Product.name, Product.PSP_IsActive__c, Product.PSP_Service_Type__c, Product.RecordType.Name,Product.StockKeepingUnit, Product.PSP_Franchise__c, Product.PSP_Portfolio__c, Product.PSP_Family__c, Product.PSP_Family__r.Name from CareProgramProduct where CareProgramId =: recordId AND Product.RecordTypeId = : PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.PRODUCT_RT_PRODUCT, Product2.getSObjectType())
                                                          WITH SECURITY_ENFORCED order by createddate desc
                                                         ]) {
                                                             final ProductRecord productWrapper = new ProductRecord(careProgramProd, true);
                                                             products.add(productWrapper);
                                                         }
            }
            return products;
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
    }
    /********************************************************************************************************
*  @author          Deloitte
*  @description     get service List method 
*  @param           record Id
*  @date            Aug 12, 2019
*********************************************************************************************************/
    @AuraEnabled(cacheable=true)
    public static List<ProductRecord> getServiceList(Id recordId) {
        final List<ProductRecord> servicesList = new List<ProductRecord>();
        loadVariables(recordId);
        try {
            if (CareProgramProduct.sObjectType.getDescribe().isAccessible()) { 
            for(CareProgramProduct careProgramProd : [Select Id,Name,CareProgram.Name,PSP_Default__c,ProductId,Product.PSP_IsActive__c,Product.IsActive,Product.PSP_Service_Type__c,PSP_Active__c,Product.name,Product.RecordType.Name,Product.PSP_Franchise__c,Product.StockKeepingUnit,Product.PSP_Portfolio__c,Product.PSP_Family__c,Product.PSP_Family__r.Name from CareProgramProduct where CareProgramId =:recordId AND Product.RecordType.Name='Service' WITH SECURITY_ENFORCED order by createddate desc]) {
                final ProductRecord productWrapper = new ProductRecord(careProgramProd,true);
                servicesList.add(productWrapper); 
            }
            }
            return servicesList;
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
    }
    /********************************************************************************************************
*  @author          Deloitte
*  @description     get Care Plan Templates
*  @param1          search key word String
*  @param2          product Id String
*  @date            Aug 12, 2019
*********************************************************************************************************/
    @AuraEnabled(cacheable=true)
    public static List<HealthCloudGA__CarePlanTemplate__c> getCarePlanTemplate(String searchKeyword, String productId) {
        List<HealthCloudGA__CarePlanTemplate__c> cPlanTemplates = new List<HealthCloudGA__CarePlanTemplate__c> ();
        try {
            final String nameFilter ='%'+searchKeyword+'%';
            if (HealthCloudGA__CarePlanTemplate__c.sObjectType.getDescribe().isAccessible()) {
                cPlanTemplates = [Select Id, Name from HealthCloudGA__CarePlanTemplate__c where Name like :nameFilter and HealthCloudGA__Active__c=true];
            }
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }    
        return cPlanTemplates;
    }
    
    /********************************************************************************************************
*  @author          Deloitte
*  @description     update Records method
*  @param1          update Object String
*  @param2          map of Product family to product
*  @date            Aug 12, 2019
*********************************************************************************************************/
    @AuraEnabled
    public static void updateRecords(String updateObjStr, List < Map < String, String >> mapfamToPro) {
        final Map < String, String > mapPrToCPP = new Map < String, String > ();
        try {
            for (Map < String, String > mapData: mapfamToPro) {
                mapPrToCPP.put(mapData.get('key'), mapData.get('value'));
            }
            final List < CareProgramProduct > updatedCPP = new List < CareProgramProduct > ();
            final List < Object > obList = (List < object > ) json.deserializeUntyped(updateObjStr);
            for (object ob: obList) {
                final Map < String, object > obmap = (Map < String, object > ) ob;
                final CareProgramProduct  cPP = new CareProgramProduct(Id = mapPrToCPP.get((String) obmap.get('Id')), PSP_Default__c = (Boolean) obmap.get('defaultAct'), PSP_Active__c = (Boolean) obmap.get('isActive'),PSP_Care_Plan_Template__c  = (Id)obmap.get('carePlanTemplate'));
                updatedCPP.add(cPP);
            }
            if(!updatedCPP.isEmpty()) {
                Database.update(updatedCPP) ; 
                
            }
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
    }
    /********************************************************************************************************
*  @author          Deloitte
*  @description     create Care Program Product Records method
*  @param1          productRecs String
*  @param2          care Program Id
*  @date            Aug 12, 2019
*********************************************************************************************************/
    @AuraEnabled 
    public static void createCPPRecords(String productRecs,careProgram careProgram) {
        final List<CareProgramProduct> insertedCPP = new List<CareProgramProduct>();
        final List<Object> obList = (List<Object>) JSON.deserializeUntyped(productRecs);
        try {
            for (object ob : obList) {
                final Map<String, object> obmap = (Map<String, object>)ob;  
                final CareProgramProduct cPP = new CareProgramProduct();
                if (String.isNotBlank((String) (obmap.get('prodId')))) {
                    cPP.ProductId = (String) (obmap.get('prodId'));
                }
                cPP.PSP_Active__c = true;
                cPP.CareProgramId = careProgram.Id;
                cPP.Name = careProgram.Name + ' - '+ (String) (obmap.get('name'));
                insertedCPP.add(cPP); 
            } 
            if(!insertedCPP.isEmpty() ) {
                Database.insert(insertedCPP) ; 
            } 
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
    }
    /********************************************************************************************************
*  @author          Deloitte
*  @description     Update Care Program Product method 
*  @param           record Id
*  @date            Aug 12, 2019
*********************************************************************************************************/
    @AuraEnabled
    public static void updateProduct(String cptServiceObj, String serviceToggleObj, String cppToggleObj) {
        final List<Object> serviceCptList = (List<Object>) JSON.deserializeUntyped(cptServiceObj);
        final List<Object> serviceToggleList = (List<Object>) JSON.deserializeUntyped(serviceToggleObj);
        final List<Object> cppToggleList = (List<Object>) JSON.deserializeUntyped(cppToggleObj);
        try {
            final Map<String,String> serviceCptMap = updServiceCptMap(serviceCptList);
            final Map<String,boolean> serviceToggleMap = updserviceToggleMap(serviceToggleList);
            final Map<String,boolean> cppToggleMap = updcppToggleMap(cppToggleList);
            final List<CareProgramProduct> cppToUpdate = new List<CareProgramProduct>();
            /*Collect All Product records to update*/
            /* Collect All Care Program Product records to update*/
            for(CareProgramProduct cpp : [Select Id,Name,PSP_Active__c,PSP_Care_Plan_Template__c from CareProgramProduct where Id in :serviceCptMap.keySet() or Id in :cppToggleMap.keySet() or Id in :serviceToggleMap.keySet() WITH SECURITY_ENFORCED]) {
                final CareProgramProduct careProgProdObj = new CareProgramProduct();
                careProgProdObj.Id=cpp.Id;
                if(String.isNotBlank(serviceCptMap.get(cpp.Id))) {
                    careProgProdObj.PSP_Care_Plan_Template__c= serviceCptMap.get(cpp.Id);               
                }
                if(cppToggleMap.get(cpp.Id)!=null) {               
                    careProgProdObj.PSP_Active__c=cppToggleMap.get(cpp.Id);
                }
                if(serviceToggleMap.get(cpp.Id)!=null) {
                    careProgProdObj.PSP_Default__c=serviceToggleMap.get(cpp.Id);
                }
                cppToUpdate.add(careProgProdObj);           
            }
            
            if(!cppToUpdate.isEmpty()) {
                Database.update(cppToUpdate);
            }
        } catch(AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
        
    }
    /*********************************************************************************************************
class Name      : updServiceCptMap
Description		: Method to get uodated map
@author		    : Saurabh Tripathi
@date       	: July 31, 2019
****************************************************************************************************************/ 
    private static Map<String,String> updServiceCptMap (List<Object> serviceToggleList) {
        final Map<String,String> serviceCptMap = new Map<String,String>();
        for (object obj : serviceToggleList) {
            final Map<String, object> obmap = (Map<String, object>)obj;  
            if (String.isNotBlank((String)(obmap.get(CARE_PROG_PROD_ID))) && String.isNotBlank((String) (obmap.get('cppId')))) {
                serviceCptMap.put((String)(obmap.get(CARE_PROG_PROD_ID)),(String) obmap.get('cppId'));
            }
        }
        return serviceCptMap;
    }
    /*********************************************************************************************************
class Name      : updserviceToggleMap
Description		: Method to get uodated map
@author		    : Saurabh Tripathi
@date       	: July 31, 2019
****************************************************************************************************************/ 
    private static Map<String,boolean> updserviceToggleMap (List<Object> serviceToggleList) {
        final Map<String,boolean> serviceToggleMap = new Map<String,boolean>();
        for (object obj : serviceToggleList) {
            final Map<String, object> obmap = (Map<String, object>)obj;  
            if (String.isNotBlank((String) (obmap.get('careProgProdId')))) {
                serviceToggleMap.put((String)(obmap.get('careProgProdId')),(boolean) obmap.get('defChecked'));
            }
        }
        return serviceToggleMap;
    }
    
    /*********************************************************************************************************
class Name      : updcppToggleMap
Description		: Method to get uodated map
@author		    : Saurabh Tripathi
@date       	: July 31, 2019
****************************************************************************************************************/ 
    private static Map<String,boolean> updcppToggleMap (List<Object> cppToggleList) {
        final Map<String,boolean> cppToggleMap = new Map<String,boolean>();
        for (object obj : cppToggleList) {
            final Map<String, object> obmap = (Map<String, object>)obj;  
            if (String.isNotBlank((String) (obmap.get(CARE_PROG_PROD_ID)))) {
                cppToggleMap.put((String) (obmap.get(CARE_PROG_PROD_ID)),(boolean) obmap.get('actChecked'));
            }
        }  
        return cppToggleMap;
    }
    
    
    //Product
    /*********************************************************************************************************
class Name      : ProductRecord
Description		: Inner class for Product record
@author		    : Saurabh Tripathi
@date       	: July 31, 2019
****************************************************************************************************************/ 
    public class ProductRecord {
        
        /* getter class to get Id*/       
        @AuraEnabled public Id prodId { get; set; }
        /* getter class to get Name*/ 
        @AuraEnabled public String name { get; set; }
        /* getter class to get nameurl in wrapper class*/ 
        @AuraEnabled public String nameURL { get; set; }
        /* getter class to get isactive in wrapper class*/ 
        @AuraEnabled public Boolean isActive { get; set; }
        /* getter class to get family in wrapper class*/ 
        @AuraEnabled public String family { get; set; }
        /* getter class to get Franchise in wrapper class*/ 
        @AuraEnabled public String franchise { get; set; }
        /* getter class to get portfolio in wrapper class*/ 
        @AuraEnabled public String portfolio { get; set; }
        /* getter class to get familyName in wrapper class*/ 
        @AuraEnabled public String familyName { get; set; }
        /* getter class to get familynameURL in wrapper class*/ 
        @AuraEnabled public String famNameURL { get; set; }
        /* getter class to get servicetype in wrapper class*/ 
        @AuraEnabled public String serviceType { get; set; }
        /* getter class to get productsku in wrapper class*/ 
        @AuraEnabled public String productsku { get; set; }
        /* getter class to get careplantemplate in wrapper class*/ 
        @AuraEnabled public String carePlanTemplate { get; set; }
        /* getter class to get templateURL in wrapper class*/ 
        @AuraEnabled public String templateURL { get; set; }
        /* getter class to get productId in wrapper class*/ 
        @AuraEnabled public String proProductId { get; set; }
        /* getter class to get careprogram name in wrapper class*/ 
        @AuraEnabled public String programName { get; set; }
        /* getter class to get default active in wrapper class*/ 
        @AuraEnabled public Boolean defaultAct { get; set; }
        /* getter class to get care program Id in wrapper class*/ 
        @AuraEnabled public String careProgProdId { get; set; }
        /* getter class to get care program  product name in wrapper class*/ 
        @AuraEnabled public String cppName { get; set; }
        /* getter class to get care program  product name url in wrapper class*/ 
        @AuraEnabled public String cppNameUrl { get; set; }
        /* getter class to get care plan template id in wrapper class*/ 
        @AuraEnabled public String tepmplateId { get; set; }
        
        //Product
        /********************************************************************************************************
*  @author          Deloitte
*  @description     Product Record constructor
*  @date            Aug 12, 2019
*********************************************************************************************************/
        public ProductRecord(Product2 product, Boolean val) {
            prodId = product.Id;
            name				= product.Name;
            nameURL				= '/'+product.Id;
            family 				= (product.PSP_Family__c == null? '' : product.PSP_Family__c);
            franchise 			= (product.PSP_Franchise__c == null? '' : product.PSP_Franchise__c);
            portfolio 			= (product.PSP_Portfolio__c == null? '' : product.PSP_Portfolio__c);
            familyName 			= (product.PSP_Family__c == null? '' : product.PSP_Family__r.Name);
            famNameURL 			= (product.PSP_Family__c == null? '' : '/'+product.PSP_Family__c);
            serviceType 		= (product.PSP_Service_Type__c == null? '' : product.PSP_Service_Type__c);
            careProgProdId      = (productCPPIdMap.containsKey(product.Id)?productCPPIdMap.get(product.Id):'');
            productsku			= (product.StockKeepingUnit == null? '' : product.StockKeepingUnit);
            defaultAct			= product.isActive;
            // carePlanTemplate 	= (prodCareProgProd.containsKey(product.Id)? prodCareProgProd.get(product.Id).PSP_Care_Plan_Template__r.Name : '');
            
            if(val) {
                tepmplateId			=(prodCareProgProd.containsKey(product.Id)? prodCareProgProd.get(product.Id).PSP_Care_Plan_Template__c : '');
                proProductId		= prodCareProgProd.containsKey(product.Id)? prodCareProgProd.get(product.Id).Id : '';
                isActive 			= prodCareProgProd.containsKey(product.Id)? prodCareProgProd.get(product.Id).PSP_Active__c : false;
                carePlanTemplate 	= (prodCareProgProd.containsKey(product.Id)? prodCareProgProd.get(product.Id).PSP_Care_Plan_Template__r.Name : '');
                templateURL 		= (prodCareProgProd.containsKey(product.Id)? '/'+prodCareProgProd.get(product.Id).PSP_Care_Plan_Template__c : '');
            }
        }
        /********************************************************************************************************
*  @author          Deloitte
*  @description     Care Program Product Record constructor
*  @date            Aug 12, 2019
*********************************************************************************************************/
        public ProductRecord(CareProgramProduct cPProduct, boolean existingRec) {
            prodId 					= cPProduct.ProductId;
            name				= cPProduct.Product.Name;
            nameURL				= '/'+cPProduct.ProductId;
            family 				= (cPProduct.Product.PSP_Family__c == null? '' : cPProduct.Product.PSP_Family__c);
            franchise 			= (cPProduct.Product.PSP_Franchise__c == null? '' : cPProduct.Product.PSP_Franchise__c);
            portfolio 			= (cPProduct.Product.PSP_Portfolio__c == null? '' : cPProduct.Product.PSP_Portfolio__c);
            familyName 			= (cPProduct.Product.PSP_Family__c == null? '' : cPProduct.Product.PSP_Family__r.Name);
            famNameURL 			= (cPProduct.Product.PSP_Family__c == null? '' : '/'+cPProduct.Product.PSP_Family__c);
            productsku			= (cPProduct.Product.StockKeepingUnit == null? '' : cPProduct.Product.StockKeepingUnit);
            careProgProdId      =  cPProduct.Id;
            cppName				=cPProduct.Name;
            cppNameUrl			=	'/'+cPProduct.Id;
            defaultAct          = cPProduct.PSP_Default__c;
            serviceType         = cPProduct.Product.PSP_Service_Type__c;
            carePlanTemplate    = (prodCareProgProd.containsKey(cPProduct.ProductId)? prodCareProgProd.get(cPProduct.ProductId).PSP_Care_Plan_Template__r.Name : '');//cPProduct.Product.Care_Plan_Template__r.Name;
            tepmplateId			= (prodCareProgProd.containsKey(cPProduct.ProductId)? prodCareProgProd.get(cPProduct.ProductId).PSP_Care_Plan_Template__c : '');
            if(existingRec) {
                proProductId		= cPProduct.Id;
                isActive 			= cPProduct.PSP_Active__c == null? false : cPProduct.PSP_Active__c; 
            }
        }
    }
}
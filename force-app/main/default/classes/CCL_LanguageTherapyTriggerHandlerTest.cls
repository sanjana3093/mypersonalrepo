/********************************************************************************************************
*  @author          Deloitte
*  @description     Test class for the Language Therapy association trigger handler 
*  @param           
*  @date            Dec 1, 2020
*********************************************************************************************************/

@isTest
public class CCL_LanguageTherapyTriggerHandlerTest {

    @testSetup
    public static void languageTherapyAssoCreation() {
        
        final String currentTime= String.valueOf(System.now().millisecond());
        final String randomNum= String.valueOf(Math.abs(Crypto.getRandomInteger()));
        
        final String profileName = CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE;
        final User sysAdminUser = CCL_TestDataFactory.createTestUser(profileName);
    
        final UserRole nvsRoleId = [SELECT Id, DeveloperName 
                                        FROM UserRole 
                                        WHERE DeveloperName =:CCL_StaticConstants_MRC.ROLE_NOVARTIS_ADMIN 
                                        LIMIT 1];
    
    
        sysAdminUser.UserRoleId = nvsRoleId.Id;
    
        insert sysAdminUser;
    
        System.runAs(sysAdminUser) {
            final Profile internalProfile = [SELECT Id, Name 
                                            FROM Profile 
                                            WHERE Name =:CCL_StaticConstants_MRC.INTERNAL_BASE_PROFILE_TYPE];
            User internalBaseProfileUser = new User();
            internalBaseProfileUser = CCL_TestDataFactory.createTestUser(internalProfile.Name);
            insert internalBaseProfileUser;

            User NVSUser = new User();
            NVSUser = CCL_TestDataFactory.createTestUser(internalProfile.Name);
            insert NVSUser;

            String nvsPermissionSet = CCL_StaticConstants_MRC.PERMISSION_TYPE_NVS_BUSINESS_ADMIN; 
            PermissionSetAssignment nvsPermissionSetAssignment  = CCL_TestDataFactory.createPermissionSetForAssignment(nvsPermissionSet, NVSUser);
            insert nvsPermissionSetAssignment;
                    
            Account siteAccount = new Account();
            siteAccount = CCL_TestDataFactory.createTestParentAccount();
            insert siteAccount;
    
            CCL_Therapy__c testTherapy = new CCL_Therapy__c();
            testTherapy = CCL_TestDataFactory.createTestTherapy();
            insert testTherapy;

            CCL_Therapy_Association__c testTherapyAssociation = new CCL_Therapy_Association__c();
            testTherapyAssociation = CCL_TestDataFactory.createTestCountryTherapyAssoc(siteAccount.Id, testTherapy.Id);
            insert testTherapyAssociation;

            List<CCL_Language_Therapy_Association__c> languageTherapyList = new List<CCL_Language_Therapy_Association__c>();
            CCL_Language_Therapy_Association__c testLanguageTherapy = new CCL_Language_Therapy_Association__c();
            testLanguageTherapy = CCL_TestDataFactory.createLanguageTherapyAssociation(testTherapyAssociation);
            languageTherapyList.add(testLanguageTherapy);
            insert languageTherapyList;
        }
    }
         
    static testmethod void testDuplicationForLanguageTherapy() {
        
        List<CCL_Language_Therapy_Association__c> listLanguageTherapyAsso = new List<CCL_Language_Therapy_Association__c>();
        String uniqueCombination;
        CCL_Language_Therapy_Association__c assertLanguageTherapy = new CCL_Language_Therapy_Association__c();

        String nvsPermissionSet = CCL_StaticConstants_MRC.PERMISSION_TYPE_NVS_BUSINESS_ADMIN; 

        User NVSAdminUser = new User();
        List<PermissionSetAssignment> assigneesNVSAdmins = new List<PermissionSetAssignment>();
        assigneesNVSAdmins = [SELECT Id, AssigneeId, Assignee.Name 
                                FROM PermissionSetAssignment 
                                WHERE PermissionSet.Name = :nvsPermissionSet ];

        NVSAdminUser = [SELECT Id FROM User WHERE Id = :assigneesNVSAdmins[0].AssigneeId Limit 1];

        for( CCL_Language_Therapy_Association__c temptValue : [SELECT Id, CCL_Language__c, CCL_Therapy_Association__c, CCL_Unique_Combination__c FROM CCL_Language_Therapy_Association__c WHERE CCL_Language__c = :CCL_StaticConstants_MRC.GLOBAL_PICKLIST_ENG_US_VALUE LIMIT 1] ) {
            listLanguageTherapyAsso.add(temptValue);
        }
        assertLanguageTherapy = listLanguageTherapyAsso[0];
        uniqueCombination = assertLanguageTherapy.CCL_Language__c + '' + assertLanguageTherapy.CCL_Therapy_Association__c;     

        System.runAs(NVSAdminUser) {
            Test.startTest();
            System.assertEquals(assertLanguageTherapy.CCL_Unique_Combination__c, uniqueCombination, ' Unique Combination ');
            Test.stopTest();
        }
    }
}
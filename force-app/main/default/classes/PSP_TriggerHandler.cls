/*********************************************************************************************************
class Name      : PSP_TriggerHandler 
Description		: Trigger Handler Class
@author		    : Saurabh Tripathi
@date       	: July 10, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi            July 10, 2019          Initial Version
****************************************************************************************************************/ 
public virtual class PSP_TriggerHandler {
    
    // context-specific methods for override
  	/********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     Before Insert
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
    @TestVisible
    public virtual void beforeInsert () {}
    /********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     Before Update
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
    @TestVisible
    public virtual void beforeUpdate () {}
    /********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     After Insert
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
    @TestVisible
  	public virtual void afterInsert() {}
    /********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     After Update
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
  	@TestVisible
  	public virtual void afterUpdate() {}
    /* trigger contexts */
    
    @TestVisible
    public enum TriggerContext {
        BEFORE_INSERT, BEFORE_UPDATE, BEFORE_DELETE,
            AFTER_INSERT, AFTER_UPDATE, AFTER_DELETE,
            AFTER_UNDELETE
    }
	
    
  // exception class
  	/**************************************************************************************	
          * @author      : Deloitte
          * @date        : 07/10/2019
          * @Description : Exception class
          ***************************************************************************************/
    public class TriggerHandlerException extends Exception {}
    
}
/**
* @author Deloitte
* @date 06-June-18
*
* @description class created to Save Custom Case Record on Clicking on Create New Enquiry Case
*/

public with sharing class spc_InquiryCaseController {

    /*********************************************************************************
    * @author Deloitte
    * @description     Retrieves ID of Case Enquiry Case
    * @return   Record ID

    *********************************************************************************/
    @AuraEnabled
    public static String getRecordTypeIdForInquiry() {
        String recordtypeID = spc_ApexConstants.ID_INQUIRYCASE_RECORDTYPE;
        return recordtypeID;
    }

    /*********************************************************************************
    * @author Deloitte
      * @description     Retrieves elletters based on case id and channel
      * @return   List<spc_LightningMap>
    * @param case id and string

    *********************************************************************************/

    @AuraEnabled
    public static List<spc_LightningMap> getLetters(ID CaseID, String invokeAction) {
        List<spc_LightningMap> lstAvailableeLetters = new List<spc_LightningMap>();
        lstAvailableeLetters.add(new spc_LightningMap(Label.spc_Select_a_mail, Label.PatientConnect.PC_None));
        String objType = String.valueOf(CaseID.getSobjectType());
        List<Case> newCs = [Select recordtypeid, PatientConnect__PC_Program__c from Case where id = :CaseID];
        String caseRcrdType = Schema.SObjectType.Case.getRecordTypeInfosById().get(newCs[0].recordtypeid).getName();
        spc_PatientConsentDetails consentDetails;
        if (caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_PRE_TREATMENT))
            || caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_POST_TREATMENT))) {

            consentDetails = spc_PatientConsentDetails.getInstance(newCs[0].PatientConnect__PC_Program__c);
    } else {
        consentDetails = spc_PatientConsentDetails.getInstance(CaseID);
    }
    List<Case> newCases  = [SELECT ID, PatientConnect__PC_Engagement_Program__c, spc_is_PostTreatment_Case__c, spc_is_Pretreatment_Case__c, spc_Is_Enquiry_Case__c, RecordType.Name, recordtype.developername FROM CASE WHERE ID = : CaseID LIMIT 1];


    /* Start - Check for patient HIPAA/ Service/ Marketing consent */


        //This consent check is for eLetter action center icon - program case
    if (invokeAction.equalsIgnoreCase(System.Label.spc_actioncenter)) {

        List<String> consentlst = spc_Utility.fetchConsentList(consentDetails);
        String consentString = string.join(consentlst, ',');
        Set<String> consentAllSet = new Set<String>();
        consentAllSet.addAll(consentString.remove('\'').split(','));
            //generating dynamic query to fetch eletter filtred on Consents
        String query = 'SELECT Id,spc_Consent_Eletter_Needeed__c,spc_All_Consents_Mandatory__c, Name FROM PatientConnect__PC_eLetter__c';
        query += ' WHERE  PatientConnect__PC_Source_Object__c = \'' + objType + '\' AND PatientConnect__PC_Object_Record_Types__c includes (\'' + newCases[0].RecordType.Name + '\')';
        if (!consentlst.isEmpty()) {
            query += ' AND  (spc_Consent_Eletter_Needeed__c includes' + consentlst;
            query += 'OR spc_Consent_Eletter_Needeed__c=\'' + spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.DEFAULT_CONSENT) + '\') order by Name';
        } else {
            query += 'AND spc_Consent_Eletter_Needeed__c=\'' + spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.DEFAULT_CONSENT) + '\' order by Name';
        }
        List<PatientConnect__PC_eLetter__c> eletterslst = spc_Database.queryWithAccess(query , false);
        if (!newCases.isEmpty()) {
            Map<ID, List<String>> cChannels = null;
            cChannels = spc_eLetterService.getMapBetweenMailAndCchannels(new Set<ID> {newCases[0].PatientConnect__PC_Engagement_Program__c});
            for (PatientConnect__PC_eLetter__c el : eletterslst) {

                List<String> filteredList = cChannels.get(el.Id);
                if (newCases[0].spc_Is_Enquiry_Case__c || newCases[0].spc_is_Pretreatment_Case__c || newCases[0].spc_is_PostTreatment_Case__c) {
                    if (filteredList != null && !filteredList.isEmpty()) {
                        lstAvailableeLetters.add(new spc_LightningMap(el.Name,  filteredList[0]));


                    }
                } else if (cChannels.get(el.Id) != null) {
                    if ( !filteredList.isEmpty()) {
                        boolean hideEmailRow = filteredList.contains(System.Label.spc_Email_Template);
                        boolean hideFaxRow = filteredList.contains(System.Label.spc_Fax_Template);

                        if (el.spc_All_Consents_Mandatory__c) {
                            Set<String> eletterConsentSet = new Set<String>();
                            List<String> tmpString = new List<String>();
                            tmpString.addAll(el.spc_Consent_Eletter_Needeed__c.split(';'));
                            eletterConsentSet.addAll(tmpString);
                            If(consentAllSet.containsAll(eletterConsentSet) ) {
                                lstAvailableeLetters.add(new spc_LightningMap(el.Name, filteredList[0], hideFaxRow, hideEmailRow));
                            }

                        } else {
                            lstAvailableeLetters.add(new spc_LightningMap(el.Name, filteredList[0], hideFaxRow, hideEmailRow));
                        }

                    }

                }
            }
            if(newCases[0].spc_Is_Enquiry_Case__c || newCases[0].spc_is_Pretreatment_Case__c || newCases[0].spc_is_PostTreatment_Case__c){
             Map<Id,Set<String>> IdToPMRC = new Map<Id,Set<String>>();
             for(Id eLetterID : cChannels.keySet()){
                 Set<String> PMRCSet = new Set<String>();
                 PMRCSet.addAll(cChannels.get(eLetterID));
                 IdToPMRC.put(eLetterID,PMRCSet);
             }
             lstAvailableeLetters.addAll(spc_Utility.fetchAdditionaleLetters(consentlst,consentAllSet,newCases[0],objType,IdToPMRC));
         } 
     }        
     /* End */
 }
 lstAvailableeLetters.add(new spc_LightningMap(caseRcrdType, Label.PatientConnect.PC_None));
 return lstAvailableeLetters;
}


    /*********************************************************************************
        * @author Deloitte
        * @description     Send Consideration Brochure to Patient or care Giver
        * @return   void
        * @param case id

    *********************************************************************************/

    @AuraEnabled
    public static void sendBrochure(String caseId) {

        List<Case> cases = [SELECT ID, PatientConnect__PC_Engagement_Program__c, AccountId, PatientConnect__PC_Email__c, recordtypeid FROM CASE WHERE ID = : caseId];
        if (!cases.isEmpty()) {

            Boolean isSendEmailCreateDocumentSuccess = true;
            List<StaticResource> staticListRec = [Select  s.Name, s.Id, s.Body From StaticResource s where s.Name = : spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.STATIC_RESOURCE_CONSID_BROCHURE) Limit 1]; // 'static_resource' is the name of the static resource PDF.;
            if (!staticListRec.isEmpty()) {
                Map<ID, ID> accountToContactMap = mapAccountwithContact(cases[0]);
                if (accountToContactMap != null && accountToContactMap.values() != null) {
                    isSendEmailCreateDocumentSuccess = sendEmailBrochure(cases[0], accountToContactMap, staticListRec);
                }
                if (isSendEmailCreateDocumentSuccess) {
                    isSendEmailCreateDocumentSuccess = createDocumentsAndDocumentsLogs(cases[0], staticListRec);
                }
            }
        }
    }

    /*******************************************************************************************************
    * @description Method to Map Account with Contact
    * @param newCase Case
    */
    @TestVisible
    private static Map<Id, Id> mapAccountwithContact(Case newCase) {
        Map<Id, Id> accToConmap = new Map<Id, Id>();
        for (Contact c : [Select ID, AccountId from Contact where AccountId  = : newCase.AccountId] ) {
            if (accToConmap.get(c.AccountId) == null) accToConmap.put(c.AccountId, c.Id);
        }
        return accToConmap;
    }

    /*******************************************************************************************************
    * @description Send Email Brochure to Patient or care Giver
    * @param newCase Case
    * @param accountToContactMap Map<Id,Id>
    * @param srs List<StaticResource>
    * @return Boolean
    */
    @TestVisible
    private static boolean sendEmailBrochure(Case newCase, Map<ID, ID> accountToContactMap, List<StaticResource> srs) {
        boolean isEmailSuccess = false;
        List<EmailTemplate> lstEmailTemplates = [SELECT Id, Body, Subject from EmailTemplate where DeveloperName = : spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.EMAIL_TEMP_SEND_CONSID_BROCHURE)  Limit 1];
        List<OrgWideEmailAddress> owea = [select Id from OrgWideEmailAddress where Address = : spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ORG_WIDE_EMAIL_2)  Limit 1];
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        String caseRcrdType = Schema.SObjectType.Case.getRecordTypeInfosById().get(newCase.recordtypeid).getname();

        if (!lstEmailTemplates.isEmpty() &&  newCase.PatientConnect__PC_Email__c != null) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(lstEmailTemplates[0].Id);
            mail.setSaveAsActivity(true);
            mail.setSubject(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.EMAIL_SEND_CONSID_BROCHURE_SUBJECT));
            mail.targetobjectid = accountToContactMap.get(newCase.AccountId);
            String[] toAddresses = new String[] {newCase.PatientConnect__PC_Email__c};
            mail.toaddresses = toAddresses;
            mail.setHtmlBody(lstEmailTemplates[0].body);
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            if (!srs.isEmpty()) {
                Blob tempBlob = srs[0].Body;
                efa.setBody(tempBlob);
                efa.setFileName(label.spc_sendConsid_broc_file_name);
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            }
            if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }
            mail.setWhatId(newCase.id); // Enter your record Id whose merge field you want to add in template
            mails.add(mail);
        }
        if (!mails.isEmpty()) {
            try {
                Messaging.sendEmail(mails);
                isEmailSuccess = true;
            } catch (Exception e) {
                spc_Utility.createExceptionLog(e);
            }
        }
        return isEmailSuccess;
    }

    /*******************************************************************************************************
    * @description create Document and Document Logs
    * @param newCase Case
    * @param srs List<StaticResource>
    * @return Boolean
    */
    @TestVisible
    private static boolean createDocumentsAndDocumentsLogs(Case newCase, List<StaticResource> srs) {
        boolean isCreateDocLogFromDocumentSuccess = false;
        List<PatientConnect__PC_Document__c> docs = new  List<PatientConnect__PC_Document__c>();
        Id docRTypeId = spc_ApexConstants.ID_EMAIL_OUTBOUND_RECORDTYPE;
        Map<ID, ID> casetoAccMap = new Map<ID, ID>();
        PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c();
        doc.RecordTypeId = docRTypeId;
        doc.spc_InquiryCaseId__c = newCase.ID;
        doc.PatientConnect__PC_Document_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_STATUS_READYTOSEND);
        doc.PatientConnect__PC_Engagement_Program__c = newCase.PatientConnect__PC_Engagement_Program__c;
        docs.add(doc);
        casetoAccMap.put(newCase.ID, newCase.AccountId);
        if (!docs.isEmpty()) {
            try {
                spc_Database.ins(docs);
                isCreateDocLogFromDocumentSuccess = createDocLogsFromDocs(docs, casetoAccMap, srs);
                isCreateDocLogFromDocumentSuccess = true;
            } catch (Exception e) {
                spc_Utility.createExceptionLog(e);
            }
        }
        return isCreateDocLogFromDocumentSuccess;
    }

    /*******************************************************************************************************
    * @description create Document Logs from Docs
    * @param docs List<PatientConnect__PC_Document__c>
    * @param caseToAccMap Map<Id,Id>
    * @param srs List<StaticResource>
    * @return Boolean
    */
    @TestVisible
    private static boolean createDocLogsFromDocs(List<PatientConnect__PC_Document__c> docs, Map<Id, Id> caseToAccMap, List<StaticResource> srs) {
        boolean isCreateDocLogFromDocumentSuccess = false;
        List<Attachment> attachments = new List<Attachment>();
        List<PatientConnect__PC_Document_Log__c> docLogList = new List<PatientConnect__PC_Document_Log__c>();
        for (PatientConnect__PC_Document__c doc : docs) {
            PatientConnect__PC_Document_Log__c doclog = new PatientConnect__PC_Document_Log__c();
            doclog.PatientConnect__PC_Document__c = doc.Id;
            doclog.spc_Inquiry__c = doc.spc_InquiryCaseId__c;
            if (caseToAccMap.get(doc.spc_InquiryCaseId__c) != null) doclog.PatientConnect__PC_Account__c = caseToAccMap.get(doc.spc_InquiryCaseId__c) ;
            docLogList.add(doclog);
            Attachment attachment = new Attachment ();
            attachment.ParentId = doc.Id;
            attachment.Body = srs[0].body;
            attachment.Name = srs[0].Name + '.pdf';
            attachment.ContentType =  spc_ApexConstants.Application_ContantType;
            attachments.add(attachment);
        }
        if (!docLogList.isEmpty()) {
            try {
                spc_Database.ins(docLogList);
                spc_Database.ins(attachments);
                isCreateDocLogFromDocumentSuccess = true;
            } catch (Exception e) {
                spc_Utility.logAndThrowException(e);
            }
        }
        return isCreateDocLogFromDocumentSuccess;
    }

    /*******************************************************************************************************
    * @description create Document  for mail outbound
    * @param doc Map<String,Object>
    * @return String
    */

    @AuraEnabled
    public static string createDocument(Map<String, Object>  doc) {
        PatientConnect__PC_Document__c newDoc = new PatientConnect__PC_Document__c();
        Id CaseId = (String)doc.get('caseId');
        List<Case> newCs = [Select recordtypeid, PatientConnect__PC_Program__c from Case where id = :CaseID];
        String caseRcrdType = Schema.SObjectType.Case.getRecordTypeInfosById().get(newCs[0].recordtypeid).getName();
        if (String.isNotBlank((String) doc.get('eletter'))) {
            newDoc.spc_Mail_Outbound_Product_New__c = (String) (doc.get('eletter'));
        }
        if (String.isNotBlank((String) doc.get('status'))) {
            newDoc.PatientConnect__PC_Document_Status__c = (String) (doc.get('status'));
        }
        if (String.isNotBlank((String) doc.get('pmrc'))) {
            newDoc.spc_PMRC_Code_New__c = (String) (doc.get('pmrc'));
        }
        if (String.isNotBlank((String) doc.get('desc'))) {
            newDoc.PatientConnect__PC_Description__c = (String) (doc.get('desc'));
        }
        if (String.isNotBlank((String) doc.get('engagementProgram'))) {
            newDoc.PatientConnect__PC_Engagement_Program__c = (String) (doc.get('engagementProgram'));
        }
        if (String.isNotBlank((String) doc.get('parentDoc'))) {
            newDoc.PatientConnect__PC_Parent_Document__c = (String) (doc.get('parentDoc'));
        }
        if (String.isNotBlank((String) doc.get('statusDate'))) {
            newDoc.spc_Status_Date__c = (Date.valueOf(String.valueOf(doc.get('statusDate'))));
        }
        if (String.isNotBlank((String) doc.get('scheduledDate'))) {
            newDoc.spc_Shipped_Date__c = (Date.valueOf(String.valueOf(doc.get('scheduledDate'))));
        }
        newDoc.spc_isMailDocumentCreated__c = true;
        newDoc.recordtypeid = spc_ApexConstants.ID_MAIL_OUTBOUND_RECORDTYPE;
        if (caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM))) {
            newDoc.spc_Program_Case_Id__c = CaseId;
        } else if (caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_INQUIRY))) {
            newDoc.spc_InquiryCaseId__c = CaseId;
        } else if (caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_PRE_TREATMENT))) {
            newDoc.spc_pretreatmentCaseId__c = CaseId;
        } else if (caseRcrdType.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_POST_TREATMENT))) {
            newDoc.spc_postTreatmentCaseId__c = CaseId;
        }

        spc_Database.ins(newDoc);

        return newDoc.id;
    }

    /*******************************************************************************************************
    * @description Method to get the Picklist Values for the a picklist Field
    * @return Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>>
    */

    @AuraEnabled
    public static Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> getPicklistEntryMap() {
        Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> picklistEntryMap =
        new Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>>();

        picklistEntryMap.put('status', spc_PicklistFieldManager.getPicklistEntryWrappers(PatientConnect__PC_Document__c.PatientConnect__PC_Document_Status__c));
        return picklistEntryMap;
    }


}
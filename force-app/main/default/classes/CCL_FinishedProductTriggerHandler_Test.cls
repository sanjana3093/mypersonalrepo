/********************************************************************************************************
*  @author          Deloitte
*  @description     test class for Trigger handler class for Finished Product Trigger
*  @param           
*  @date            Aug 13, 2020
*********************************************************************************************************/
@isTest
public class CCL_FinishedProductTriggerHandler_Test {

    @TestSetup
    static void createTestData() {
        final CCL_Order__c PRFOrderComRec = CCL_TestDataFactory.createTestPRFOrder();
        CCL_Shipment__c fPShipment = CCL_TestDataFactory.createTestFinishedProductShipment(PRFOrderComRec.Id, PRFOrderComRec.CCL_Therapy__r.Id);
        insert fPShipment;
    
        if(PRFOrderComRec != null &&  fPShipment != null) {
            List<CCL_Finished_Product__c> lstFinishedProducts = new List<CCL_Finished_Product__c>();
                CCL_Finished_Product__c fp = new CCL_Finished_Product__c();
                fp.Name = CCL_StaticConstants_MRC.TEST_FINISHED_PRODUCT_VALUE;
                fp.CCL_Order__c = PRFOrderComRec.Id;
                fp.CCL_Shipment__c= fPShipment.Id;
                fp.CCL_Actual_Batch_Release_Date__c= Date.today();
                fp.CCL_Manufacturing_Date__c=Date.today();
                fp.CCL_Expiry_Date__c = Date.today();
                fp.CCL_OBA_Milestone_1_Date__c = Date.today();
                fp.CCL_OBA_Milestone_2_Date__c = Date.today();
                fp.CCL_OBA_Milestone_3_Date__c = Date.today();
                fp.CCL_OBA_Milestone_4_Date__c = Date.today();
                fp.CCL_OBA_Milestone_5_Date__c = Date.today();
                fp.CCL_Actual_Infusion_Date_Time__c = datetime.now();
                lstFinishedProducts.add(fp);


            // Setup an additional FP record for Unit testing purpose
                CCL_Finished_Product__c finishedProduct2 = new CCL_Finished_Product__c();
                finishedProduct2.Name = 'finishedProduct2';
                finishedProduct2.CCL_Order__c = PRFOrderComRec.Id;
                finishedProduct2.CCL_Shipment__c= fPShipment.Id;

                
                lstFinishedProducts.add(finishedProduct2);
                insert lstFinishedProducts;

            //Date setup for Method : updateFPExpiryDateOnShipmentAfterUpdateOldValuesTest
                final List<CCL_Shipment__c> shipList = new List<CCL_Shipment__c>();
                final CCL_Shipment__c shipmentRecord01 = new CCL_Shipment__c();
                shipmentRecord01.Name = 'updateFPExpiryDateOnShipment01';
                shipList.add(shipmentRecord01);
                final CCL_Shipment__c shipmentRecord02 = new CCL_Shipment__c();
                shipmentRecord02.Name = 'updateFPExpiryDateOnShipment02';
                shipList.add(shipmentRecord02);

                insert shipList;

        }
    }
    static testMethod void updateDateFieldsOnFPTest() {
        final CCL_Finished_Product__c finishedProduct = [SELECT Id, Name,CCL_Actual_Batch_Release_Date__c, CCL_Manufacturing_Date__c,CCL_Expiry_Date__c,CCL_Manufacturing_Date_Text__c,
                                                        CCL_OBA_Milestone_1_Date__c, CCL_OBA_Milestone_2_Date__c, CCL_OBA_Milestone_3_Date__c, CCL_OBA_Milestone_4_Date__c, 
                                                        CCL_OBA_Milestone_5_Date__c, CCL_Actual_Batch_Release_Date_Text__c, CCL_Expiration_Date_Text__c, CCL_Follow_Up_Infusion_Date__c,
                                                        CCL_Actual_Infusion_Date_Time__c 
                                                        FROM CCL_Finished_Product__c 
                                                        WHERE Name=:CCL_StaticConstants_MRC.TEST_FINISHED_PRODUCT_VALUE];
        Test.startTest();
            finishedProduct.CCL_Actual_Batch_Release_Date_Text__c = CCL_Utility.getFormattedDate(finishedProduct.CCL_Actual_Batch_Release_Date__c);
            finishedProduct.CCL_Expiration_Date_Text__c = CCL_Utility.getFormattedDate(finishedProduct.CCL_Expiry_Date__c);
            finishedProduct.CCL_Manufacturing_Date_Text__c = CCL_Utility.getFormattedDate(finishedProduct.CCL_Manufacturing_Date__c);

            final DateTime inputDateTime = DateTime.now();
            final String strFormattedDate = inputDateTime.format('dd MMM yyyy');
            System.assertEquals(strFormattedDate,finishedProduct.CCL_Manufacturing_Date_Text__c);
        Test.stopTest();
    }
    static testMethod void updateNumberOfFPRecordsOnShipmentTest() {

        List <CCL_Shipment__c> finishedProductShipment = [SELECT Name, Id, CCL_Number_of_Doses_Shipped__c,(SELECT Id FROM Finished_Products__r), CCL_Order__c
        FROM CCL_Shipment__c 
        WHERE Name = :'Finished Product Shipment'];
        
        Test.startTest();
        Integer finishedProductCount = [SELECT COUNT()
                                        FROM CCL_Finished_Product__c 
                                        WHERE CCL_Shipment__r.Id IN :finishedProductShipment];
        
        List <CCL_Shipment__c> resultFinishedProductShipment = [SELECT Name, Id, CCL_Number_of_Doses_Shipped__c
                                                                FROM CCL_Shipment__c 
                                                                WHERE Name = :'Finished Product Shipment'];

        System.assertEquals(resultFinishedProductShipment[0].CCL_Number_of_Doses_Shipped__c,finishedProductCount);
        Test.stopTest();
    }

    static testMethod void updateFPExpiryDateOnShipmentTest() {
        final CCL_Finished_Product__c finishedProduct = [SELECT Id,CCL_Expiry_Date__c, Name,CCL_Shipment__c,
                                                        CCL_Finished_Product__c.CCL_Shipment__r.Id
                                                        FROM CCL_Finished_Product__c 
                                                        WHERE Name=:CCL_StaticConstants_MRC.TEST_FINISHED_PRODUCT_VALUE];
        Test.startTest();
        finishedProduct.CCL_Expiry_Date__c=Date.today();
        System.debug('Updated Finished Product Record: ' + finishedProduct.CCL_Expiry_Date__c);
        update finishedProduct;

       final CCL_Shipment__c shipment = [SELECT Id, CCL_Expiry_Date_of_Shipped_Bags__c 
                                    FROM CCL_Shipment__c 
                                    WHERE Id=:finishedProduct.CCL_Shipment__r.Id]; 

         System.debug('Updated shipment: '+shipment.CCL_Expiry_Date_of_Shipped_Bags__c);
         System.assertEquals(Date.today(), shipment.CCL_Expiry_Date_of_Shipped_Bags__c);
        Test.stopTest();
    }
    static testMethod void updateFPExpiryDateOnShipmentAfterUpdateOldValuesTest() {
        final List<CCL_Shipment__c> shipList = [SELECT Id, Name, CCL_Expiry_Date_of_Shipped_Bags__c 
                                            FROM CCL_Shipment__c 
                                            LIMIT 5];

        Test.startTest();
        final CCL_Finished_Product__c finishedProduct = [SELECT Id,CCL_Expiry_Date__c, Name,CCL_Shipment__c
                                                            FROM CCL_Finished_Product__c 
                                                            WHERE Name=:CCL_StaticConstants_MRC.TEST_FINISHED_PRODUCT_VALUE];
        
        finishedProduct.CCL_Shipment__c = shipList[0].id;
        finishedProduct.CCL_Expiry_Date__c=Date.today();
        update finishedProduct;
         final CCL_Finished_Product__c finishedProduct1 = [SELECT Id,CCL_Expiry_Date__c, Name,CCL_Shipment__c
                                                            FROM CCL_Finished_Product__c 
                                                            WHERE Name=:CCL_StaticConstants_MRC.TEST_FINISHED_PRODUCT_VALUE];
        finishedProduct1.CCL_Shipment__c = shipList[1].id;
        finishedProduct1.CCL_Expiry_Date__c=Date.today();
        update finishedProduct1;
        System.assertEquals(Date.today(),finishedProduct.CCL_Expiry_Date__c);
        system.debug('Test Done');
        Test.stopTest();
    }
}
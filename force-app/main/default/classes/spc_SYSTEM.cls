
/*********************************************************************************

    *  @author          Deloitte
    *  @description     his class will contain generic methods that will be used across the Application.
    *  @date            4-May-2018
    *  @version         1.0
*********************************************************************************/
public with sharing class spc_SYSTEM {
    
   private static Map<String, Id> sRecordTypeMap = new Map<String, Id>();
   private static Map<String, String> sRecordTypeNameMap = new Map<String, String>();

    /*********************************************************************************
    Method Name    : recordTypeId
    Description    : Method to return Record Type ID
    Parameter      : 1. pObjName : Object Name
                     2. pDeveloperName : Developer Name of Record Type
    *********************************************************************************/
    public static Id recordTypeId(String pObjName, String pDeveloperName){
        Id recTypeId = sRecordTypeMap.get(pObjName + pDeveloperName);
        if(recTypeId != null){
            return recTypeId;
        }

        for(RecordType rt : [Select Id From RecordType Where SobjectType = :pObjName and DeveloperName = :pDeveloperName]) {
            sRecordTypeMap.put(pObjName + pDeveloperName, rt.id);
            return rt.Id;
        }
        return null;
    }

    /*********************************************************************************
      Method Name    : recordTypeName
      Author         : Alina Balan
      Description    : This method is used to get a record type name based on the object name and record type developer name
      Return Type    : String 
      Parameter      : String pObjName, String pDeveloperName
     *********************************************************************************/
    public static String recordTypeName(String pObjName, String pDeveloperName){
        String recTypeName = sRecordTypeNameMap.get(pObjName + pDeveloperName);
        if(!String.isBlank(recTypeName)){
            return recTypeName;
        }

        for(RecordType rt : [Select Id, Name From RecordType Where SobjectType = :pObjName and DeveloperName = :pDeveloperName]) {
            sRecordTypeNameMap.put(pObjName + pDeveloperName, rt.Name);
            return rt.Name;
        }
        return null;
    }
    
    /*********************************************************************************
    Method Name    : getRecordTypeIdByName
    Author         : Prachi Nandgaonkar
    Description    : Method to return Record Type ID by record type label  without using SOQL
    Parameter      : 1. objectAPIName : Object Name
                     2. recordTypeName : Label of Record Type
    *********************************************************************************/
    public static Id getRecordTypeIdByName(String objectAPIName, String recordTypeName){
        //If object name or record type label is blank
        if(String.isBlank(objectAPIName) || String.isBlank(recordTypeName)){
            return null;
        }        
        
        //Generate a map of tokens for the sObjects
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
                
        //Get the desired sObject using the object API Name
        Schema.SObjectType sObjType = sobjectSchemaMap.get(objectAPIName);
               
        //Retrieve the describe result for the desired objec
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
                
        //Generate a map of tokens for all the Record Types for the desired object
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
         
        //Retrieve the record type id by name
        Id recordTypeId = RecordTypeInfo.get(recordTypeName).getRecordTypeId();
        
        //Above code is equivalent to saying
        //Id recordTypeId = Schema.getGlobalDescribe().get(objectAPIName).getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        return recordTypeId;
    }

    /*********************************************************************************
      Method Name    : getRecordTypeName
      Author         : Prachi Nandgaonkar
      Description    : Method to return Record Type Name by record type label without using SOQL
      Return Type    : String 
      Parameter      :  1. objectAPIName : Object Name
                      2. recordTypeName : Label of Record Type
     *********************************************************************************/
    public static String getRecordTypeName(String objectAPIName, String recordTypeName){
        //If object name or record type label is blank
        if(String.isBlank(objectAPIName) || String.isBlank(recordTypeName)){
            return null;
        }        
        
        //Generate a map of tokens for the sObjects
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
        System.debug('sobjectMap:'+ sobjectSchemaMap);
        
        //Get the desired sObject using the object API Name
        Schema.SObjectType sObjType = sobjectSchemaMap.get(objectAPIName);
        System.debug('sObjType:'+ sObjType);
        
        //Retrieve the describe result for the desired objec
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
                
        //Generate a map of tokens for all the Record Types for the desired object
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
                
        //Retrieve the record type id by name
        String rtName = RecordTypeInfo.get(recordTypeName).getName();
        return rtName;
    }
    
    /*********************************************************************************
      Method Name    : getAllRecordType
      Author         : Ajinkya Deshmukh
      Description    : This method is used to get all recordTypes for sObject
      Return Type    : List<RecordType> 
      Parameter      : String sObjectName
     *********************************************************************************/
    public static List<RecordType> getAllRecordType(String sObjectName){
        List<RecordType> allRecordTypes = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType =: sObjectName and IsActive = true];
        return allRecordTypes;
    }
}
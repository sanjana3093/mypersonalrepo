/*********************************************************************************
*  @author          Deloitte
*  @date            06/13/16
*  @description     Displays the Status Icons of a Case
*  @version         1.0
*********************************************************************************/
public with sharing class spc_CaseStatusIndicatorIconController {

    /*********************************************************************************
    * @author           Deloitte
    * @date             06/13/16
    * @description      retrieve the list of Status Icons
    * @param            CaseID The Case to query for
    * @return           List<iconWrapper>
    *********************************************************************************/
    @AuraEnabled
    public static List<iconWrapper> getStatusIndicatorIcons(String caseId) {
        List<iconWrapper> lstStatusIcons = new List<iconWrapper>();
        if (caseId != null && caseId != '') {
            Case objCase = getCase(caseId);
            lstStatusIcons = getStatusIcons(objCase);
        } else {
            return null;
        }
        return lstStatusIcons;
    }

    /*********************************************************************************
    * @author           Deloitte
    * @date             06/13/16
    * @description      retrieve Case fields
    * @param            CaseID The Case to query for
    * @return           Case
    *********************************************************************************/
    @AuraEnabled
    public static Case getCase(String caseID) {
        Case objCase = null;

        try {
            String rID = caseID;

            //Check if ID is not null
            if (rID == null || rID == '') {
                return null;
            }

            /*
            * FLS Check for Case
            */
            List<String> fields = new List<String> {spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Status_Indicator_1__c'), spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Status_Indicator_2__c'),
                                                    spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Status_Indicator_3__c'), spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Status_Indicator_4__c'),
                                                    spc_ApexConstants.getQualifiedAPIName('PatientConnect__PC_Status_Indicator_5__c'), spc_ApexConstants.getQualifiedAPIName('spc_AC__c'),
                                                    spc_ApexConstants.getQualifiedAPIName('spc_COVERAGE__c'), spc_ApexConstants.getQualifiedAPIName('spc_WC__c')
                                                   };
            spc_Database.assertAccess('Case', spc_Database.Operation.Reading, fields);

            /*
            * FLS/CRUD Check for Account
            */
            List<String> accountFields = new List<String> {'Name'};
            spc_Database.assertAccess('Account', spc_Database.Operation.Reading, accountFields);

            List<Case> lstCases = [SELECT
                                   Id, Account.Name,
                                   RecordType.Name,
                                   RecordType.DeveloperName,
                                   PatientConnect__PC_Status_Indicator_1__c,
                                   PatientConnect__PC_Status_Indicator_2__c,
                                   PatientConnect__PC_Status_Indicator_3__c,
                                   PatientConnect__PC_Status_Indicator_4__c,
                                   PatientConnect__PC_Status_Indicator_5__c,
                                   spc_AC__c,
                                   spc_COVERAGE__c,
                                   spc_WC__c
                                   FROM Case
                                   WHERE Id = : rID
                                              LIMIT 1];

            if (lstCases.size() > 0)  {
                objCase = lstCases[0];
            }
        } catch (exception e) {
            spc_Utility.createExceptionLog(e);
        }

        return objCase;
    }

    /*********************************************************************************
    * @author           Deloitte
    * @date             06/13/16
    * @description      retrieve the path to the Icon
    * @param            Indicator Name of the Indicator
    * @param            Status The status of the indicator
    * @return           String
    *********************************************************************************/
    private static String GetResourceURL(String Indicator, String statusValue) {
        /*False Positive result from ForceReviewer*/
        string path = '/SPC_Resources/Images/StatusIndicatorIcons/' + Indicator + '_' + statusValue + '.png';
        return path;
    }

    /*********************************************************************************
    * @author           Deloitte
    * @date             06/13/16
    * @description      create the IconWrapper object
    * @param            statusIcon String representation of the Icon
    * @param            StatusValue The statusValue of the indicator
    * @param            NameThe Name of the icon (for accessability standards used as alt name)
    * @return           iconWrapper
    *********************************************************************************/
    private static iconWrapper createIconWrapper (String statusIcon, String statusValue, String name) {
        iconWrapper iw = null;
        if (string.isBlank(statusValue) ) {
            iw = new iconWrapper(
                getResourceUrl(statusIcon, 'Grey') ,
                name,
                spc_ApexConstants.STATUS_INDICATOR_NONE);
        } else if (statusValue == spc_ApexConstants.STATUS_INDICATOR_COMPLETE) {
            iw = new iconWrapper(
                getResourceUrl(statusIcon, 'Green'),
                name,
                spc_ApexConstants.STATUS_INDICATOR_COMPLETE);
        } else if (statusValue == spc_ApexConstants.STATUS_INDICATOR_ONHOLD || statusValue == spc_ApexConstants.STATUS_INDICATOR_NOT_APPLICABLE) {
            iw = new iconWrapper(
                getResourceUrl(statusIcon, 'Red'),
                name,
                spc_ApexConstants.STATUS_INDICATOR_ONHOLD);
        } else if (statusValue == spc_ApexConstants.STATUS_INDICATOR_ACTION_REQUIRED) {
            iw = new iconWrapper(
                getResourceUrl(statusIcon, 'Orange'),
                name,
                spc_ApexConstants.STATUS_INDICATOR_ACTION_REQUIRED);
        } else if (statusValue == spc_ApexConstants.STATUS_INDICATOR_INPROGRESS) {
            iw = new iconWrapper(
                getResourceUrl(statusIcon, 'Orange'),
                name,
                spc_ApexConstants.STATUS_INDICATOR_INPROGRESS);
        } else {
            iw = new iconWrapper(
                getResourceUrl(statusIcon, 'Grey'),
                name,
                spc_ApexConstants.STATUS_INDICATOR_NOTSTARTED);
        }

        return iw;
    }

    /*********************************************************************************
    * @author           Deloitte
    * @date             06/13/16
    * @description      Retrieves Status Icons for the given case
    * @param            objCase Case the status are coming from
    * @return           List<iconWrapper>
    *********************************************************************************/
    private static List<iconWrapper> getStatusIcons(Case objCase) {
        List<iconWrapper> lstStatusIcons = new List<iconWrapper>();
        try {
            if (objCase != null) {
                //Status Indicator 1 : Consent
                lstStatusIcons.add ( createIconWrapper (spc_ApexConstants.IndicatorIcons.PC_Consent.name(), objCase.PatientConnect__PC_Status_Indicator_1__c, Label.PatientConnect.PC_Case_Status_Indicator_1));

                //Status Indicator 2 :MI
                lstStatusIcons.add ( createIconWrapper (spc_ApexConstants.IndicatorIcons.PC_MI.name(), objCase.PatientConnect__PC_Status_Indicator_3__c, Label.spc_Case_Status_Indicator_3));

                //Status Indicator 3 : ADHERENCE
                lstStatusIcons.add ( createIconWrapper (spc_ApexConstants.IndicatorIcons.PC_SOC.name(), objCase.PatientConnect__PC_Status_Indicator_4__c, Label.spc_Case_Status_Indicator_4));

                //Status Indicator 4 : BIBV
                lstStatusIcons.add ( createIconWrapper (spc_ApexConstants.IndicatorIcons.PC_BIBV.name(), objCase.PatientConnect__PC_Status_Indicator_2__c, Label.spc_BI_BV));

                //Status Indicator 5 : WC
                lstStatusIcons.add ( createIconWrapper (spc_ApexConstants.IndicatorIcons.PC_WC.name(), objCase.spc_WC__c, Label.spc_WC));

                //Status Indicator 6: Coverage
                lstStatusIcons.add ( createIconWrapper (spc_ApexConstants.IndicatorIcons.PC_Coverage.name(), objCase.spc_COVERAGE__c, Label.spc_Case_Status_Indicator_7));

                //Status Indicator 7 : AI
                lstStatusIcons.add ( createIconWrapper (spc_ApexConstants.IndicatorIcons.PC_AI.name(), objCase.PatientConnect__PC_Status_Indicator_5__c, Label.spc_Case_Status_Indicator_5));

                //Status Indicator 8 : AC
                lstStatusIcons.add ( createIconWrapper (spc_ApexConstants.IndicatorIcons.PC_AC.name(), objCase.spc_AC__c, Label.spc_Case_Status_Indicator_6));

            }
        } catch (exception e) {
            spc_Utility.createExceptionLog(e);
        }
        return lstStatusIcons;
    }

    /*********************************************************************************
    * @author           Deloitte
    * @date             06/13/16
    * @description      Wrapper class Icon Details
    *********************************************************************************/
    public class iconWrapper {
        @AuraEnabled public String path {get; set;}
        @AuraEnabled public String title {get; set;}
        @AuraEnabled public String status {get; set;}

        public iconWrapper (String sUrl, String sTitle, String sStatus) {
            this.path = sUrl;
            this.title = sTitle;
            this.status = sStatus;
        }
    }
}
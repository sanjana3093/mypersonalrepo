/*********************************************************************************************************
* @author Deloitte
* @date   January 3, 2019
* @description spc_EmailCommunication for sending out eletters to different recipient

*/
public class spc_EmailCommunication  implements spc_CommunicationMgr.ICommunication {
	static spc_Communication_framework_Parameters__c emailParam = spc_Communication_framework_Parameters__c.getOrgDefaults();
	spc_CommunicationMgr.RecipientHandler rHandler = new spc_CommunicationMgr.RecipientHandler();
	private Map<String, EmailTemplate> getEmailTemplates(List<PatientConnect__PC_Engagement_Program_Eletter__c> eLtters) {
		Set<String> emailTemplateNames = new Set<String>();
		for (PatientConnect__PC_Engagement_Program_Eletter__c letter : eLtters) {
			if (String.isNotBlank(letter.PatientConnect__PC_eLetter__r.PatientConnect__PC_Template_Name__c)) {
				emailTemplateNames.add(letter.PatientConnect__PC_eLetter__r.PatientConnect__PC_Template_Name__c);
			}
		}

		Map<String, EmailTemplate> mapEmailTempaltes = new Map<String, EmailTemplate>();
		for (EmailTemplate et : [SELECT ID, isActive, DeveloperName from EmailTemplate where DeveloperName in :emailTemplateNames and isActive = true]) {
			mapEmailTempaltes.put(et.DeveloperName, et);
		}
		return mapEmailTempaltes;

	}
	/*****************************************************************************
	 * @author       Deloitte
	 * @description    Send all email communication and creates document
	 * @return     Void
	 */
	public void send(List<spc_CommunicationMgr.Envelop> envelops
	                 , Map<String, PatientConnect__PC_Engagement_Program_Eletter__c> mapEmailTemplates
	                 , Map<String, Map<String, Account>> mapRecipients, Map<Id, Contact> mapAccountContacts, Map<Id,List<sObject>> additionalObjects) {
		List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();
		Map<String, EmailTemplate> mapTemplates = this.getEmailTemplates(mapEmailTemplates.values());
		List<PatientConnect__PC_Document__c> listOfOutboundDocuments = new List<PatientConnect__PC_Document__c>();

		for (spc_CommunicationMgr.Envelop env : envelops) {

			 List<Attachment> attachmentList=new List<Attachment>();
                             PatientConnect__PC_Engagement_Program_Eletter__c eLetter = mapEmailTemplates.get(env.flowName + '|' + String.valueOf(env.channel));
                            //getting attachments from the eletter records and adding into a list
                             if( null != eLetter && additionalObjects.get(eLetter.PatientConnect__PC_eLetter__c)!=null
							 && !additionalObjects.get(eLetter.PatientConnect__PC_eLetter__c).isEmpty()
                               && additionalObjects.get(eLetter.PatientConnect__PC_eLetter__c).getSobjectType()==Schema.Attachment.sObjectType){
                                // if(additionalObjects.get(eLetter.PatientConnect__PC_eLetter__c)[]
                                 attachmentList.addAll((List<Attachment>)additionalObjects.get(eLetter.PatientConnect__PC_eLetter__c));
                             }
                            //getting attachments from merge document records and adding into alist
                              if(env.attachmentList!=null &&!env.attachmentList.isEmpty()){
                                 attachmentList.addAll(env.attachmentList);
                             }

			if (eLetter != null) {
				env.outboundDoc.spc_PMRC_Code_New__c =  spc_Utility.getActualPMRCCode(eLetter.spc_PMRC_Code__c);
				env.outboundDoc.PatientConnect__PC_Description__c = eLetter.PatientConnect__PC_eLetter__r.Name;
				EmailTemplate et = mapTemplates.get(eLetter.PatientConnect__PC_eLetter__r.PatientConnect__PC_Template_Name__c);
				if (et != null) {
					Messaging.SingleEmailMessage emailMsg = new Messaging.SingleEmailMessage();
					emailMsg.setWhatId(env.sourceId);
					emailMsg.setSaveAsActivity(false);
					emailMsg.setTemplateId(et.Id);
					  List<Messaging.EmailFileAttachment> lists= new List<Messaging.EmailFileAttachment>();
                                     
                                    //giving name and body to all attachmnets and setting to the email 
                                     if( ! attachmentList.isEmpty()){
                                         for(Attachment att : attachmentList){
                                             Messaging.EmailFileAttachment aAttach = new Messaging.EmailFileAttachment();
                                             aAttach.body = att.body;
                                             aAttach.contenttype=spc_ApexConstants.Application_ContantType;
                                             aAttach.filename=att.name;
                                             lists.add(aAttach);
                                         }
                                         
                                     }
                                     emailMsg.setFileAttachments(lists);
					List<String> toAddresses = new List<String>();
					Map<String, Account> mapCaseAccounts = mapRecipients.get(env.programCaseId);
					for (String recipientType : env.recipientTypes) {
						if (mapCaseAccounts != null) {
							if (mapCaseAccounts.containsKey(recipientType)) {
								Account recipient = mapCaseAccounts.get(recipientType);
								String emailAddress = recipient.PatientConnect__PC_Email__c;
								toAddresses.add(rHandler.getEmailAddress(emailAddress));
								emailMsg.setTargetObjectId(rHandler.getTargetObjectId(mapAccountContacts.get(recipient.Id)));
							}
						}
					}

					emailMsg.setToAddresses(toAddresses);
					if (emailParam != null && emailParam.OrgWideEmailAddress__c != null) {
						emailMsg.setOrgWideEmailAddressId(emailParam.OrgWideEmailAddress__c);
					}

					emailMessages.add(emailMsg);
				} else {

					env.outboundDoc.PatientConnect__PC_Description__c = eLetter.PatientConnect__PC_eLetter__r.Name;
					env.outboundDoc.PatientConnect__PC_Document_Status__c = spc_ApexConstants.DOC_STATUS_FAILED;
				}
				listOfOutboundDocuments.add(env.outboundDoc);
			}
		}
		//creates document and attachments based on the template send out
		if (! emailMessages.isEmpty()) {
			List<Messaging.SendEmailResult> results = Messaging.sendEmail(emailMessages, false);
			//Todo: process results
			List<Attachment> attachments = new List<Attachment>();
			List<PatientConnect__PC_Document__c> docs = new List<PatientConnect__PC_Document__c>();
			Integer index = 0;
			for (Messaging.SingleEmailMessage msg : emailMessages) {
				Boolean isSuccess = results[index].isSuccess();

				PatientConnect__PC_Document__c doc = listOfOutboundDocuments[index];
				if (Test.isRunningTest()) { // Unit Test will not actually send out emails
					isSuccess = true;
					msg.setHtmlBody('TestRunning');
				}
				if (isSuccess  && msg.getHtmlBody() != null) {
					String pdfBody = msg.getPlainTextBody();
					if (String.isBlank(pdfBody)) {
						String pdfhtml = msg.getHtmlBody();
						if (!String.isBlank(pdfhtml)) {pdfBody = pdfhtml;}
					}
					blob body;
					if (!String.isBlank(pdfBody)) { body = blob.valueOf(pdfBody);}
					Attachment attach = new Attachment();
					attach.Body = body;
					attach.Name = msg.getSubject() + '.html';
					attach.IsPrivate = false;
					attach.ParentId = doc.Id;
					doc.spc_to_Email_address__c = string.join(msg.getToAddresses(), ',');
					doc.PatientConnect__PC_Document_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_STATUS_SENT);
					docs.add(doc);
					attach.ContentType = spc_ApexConstants.HTML_ContentType;
					if (body != null) { attachments.add(attach); }
				} else {
					doc.PatientConnect__PC_Document_Status__c = spc_ApexConstants.DOC_STATUS_FAILED;
					doc.spc_Error_Log__c =  results[index].getErrors()[0].getMessage();
					docs.add(doc);
				}
				if (emailParam != null && emailParam.Org_Wide_Email_Address_Email__c != null) {
					doc.PatientConnect__PC_From_Email_Address__c = emailParam.Org_Wide_Email_Address_Email__c;
				}
				if (emailParam != null && emailParam.Org_Wide_Email_Address_Name__c != null) {
					doc.PatientConnect__PC_From_Name__c = emailParam.Org_Wide_Email_Address_Name__c;
				}
				index ++;
			}
			if (!attachments.isEmpty()) {insert attachments;}
			if (!docs.isEmpty()) { update docs; }
		}
	}
}
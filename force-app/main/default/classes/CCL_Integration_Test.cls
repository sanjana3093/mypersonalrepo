@isTest
public class CCL_Integration_Test
{


    @testSetup
    public static void createDataForIntegrationProcess()
    {
        CCL_Order__c orderRecord = new CCL_Order__c();
        orderRecord.Name='Novartis Batch ID';
        orderRecord.CCL_First_Name__c='First Name';
        orderRecord.CCL_Last_Name__c='Last Name';
        orderRecord.CCL_Middle_Name__c='N';
        orderRecord.CCL_Suffix__c='Sr.';
        orderRecord.RecordTypeId=CCL_StaticConstants_MRC.ORDER_RECORDTYPE_CLINICAL;
        orderRecord.CCL_Date_of_DOB__c='1';
        orderRecord.CCL_Month_of_DOB__c='JAN';
        orderRecord.CCL_Year_of_DOB__c='1990';
        orderRecord.CCL_Hospital_Patient_ID__c='1234';
        orderRecord.CCL_Patient_Weight__c=90;
        orderRecord.CCL_Patient_Age_At_Order__c=30;
        orderRecord.CCL_Treatment_Protocol_Subject_ID__c='CTL198088';
        orderRecord.CCL_Protocol_Center_Number__c='1234';
        orderRecord.CCL_Protocol_Subject_Number__c='123';
        orderRecord.CCL_Consent_for_Additional_Research__c=TRUE;
        orderRecord.CCL_PRF_Ordering_Status__c='PRF_Submitted';
        orderRecord.CCL_Hospital_Purchase_Order_Number__c='34834349';
        orderRecord.CCL_Physician_Attestation__c= TRUE;
        orderRecord.CCL_Patient_Eligibility__c= TRUE;
        orderRecord.CCL_VA_Patient__c= TRUE;
        orderRecord.CCL_Planned_Apheresis_Pick_up_Date_Time__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        orderRecord.CCL_Manufacturing_Slot_ID__c='123567';
        orderRecord.CCL_Value_Stream__c=89;
        orderRecord.CCL_Estimated_FP_Delivery_Date__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        orderRecord.CCL_Scheduler_Missing__c=TRUE;
        orderRecord.CCL_Returning_Patient__c=TRUE;
        orderRecord.CCL_Estimated_Manufacturing_Start_Date__c=date.newInstance(2014, 9, 15);
        insert orderRecord;
       // CCL_Order__c orderRecord= CCL_TestDataFactory.createTestPRFOrder();
        CCL_Shipment__c testShipment1= new CCL_Shipment__c();
        testShipment1.Name='TestShipment14082020';
        testShipment1.CCL_Patient_Name__c='Test Patient 123';
        testShipment1.CCL_Order_Apheresis__c=orderRecord.Id;
        system.debug(orderRecord.Id);
        system.debug(orderRecord.Name);
        system.debug(orderRecord.CCL_First_Name__c);
        insert testShipment1;



    }
    @isTest
    public static void invokeIntegrationProcess()
    {

       CCL_Shipment__c shipmentRecord=[select Id,CCL_Order_Apheresis__c, Name from CCL_Shipment__c where CCL_Patient_Name__c='Test Patient 123' limit 1];
       CCL_Order__c orderRecord=[select Id, Name,CCL_First_Name__c from CCL_Order__c where CCL_First_Name__c='First Name' limit 1];

       system.debug([select Id, Name,CCL_First_Name__c from CCL_Order__c  limit 1]);

        system.debug('shipment order: '+shipmentRecord.CCL_Order_Apheresis__c);

        system.debug('Order Record: '+orderRecord);

       List<CCL_Integration_Dto> dtoList= new List<CCL_Integration_Dto>();

        CCL_Integration_Message_Setting__mdt metadata1= [SELECT CCL_Event_Type__c, CCL_Filter_Criteria__c,
                                CCL_Object_To_Query__c,CCL_Query_String__c,CCL_Child1_Query_String__c,CCL_Child2_Query_String__c,
                                CCL_Child1_Filter_Criteria__c,CCL_Child1_Object_Name__c,CCL_Field_to_Identify_Novartis_Batch_Id__c,
                                CCL_Child2_Filter_Criteria__c,CCL_Child2_Object_Name__c,CCL_Field_to_Identify_Critical_Lock__c
                                FROM CCL_Integration_Message_Setting__mdt where CCL_Event_Type__c=null AND DeveloperName='CCL_Test_Record'];

        CCL_Integration_Dto dtoObject = new CCL_Integration_Dto();
        dtoObject.eventType=null;
        dtoObject.recordId=orderRecord.Id;
        dtoObject.metadataRecord=metadata1;
        dtoList.add(dtoObject);

        Test.startTest();

        CCL_Integration_Utility.processOutboundIntegration(dtoList);
        CCL_Transaction_Log__c transactionLogRecord = [SELECT Id, Name, CreatedDate, CCL_Event_Type__c FROM CCL_Transaction_Log__c where CCL_Event_Type__c='Order API' limit 1];
        string eventTypeFetched=transactionLogRecord.CCL_Event_Type__c;
        System.assertEquals('Order API',eventTypeFetched);

        Test.stoptest();



    }


}
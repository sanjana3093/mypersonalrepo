/********************************************************************************************************
*  @author          Deloitte
*  @description     Trigger Handler class for Apheresis Data Form.
*  @param           
*  @date            Aug 12, 2020
*********************************************************************************************************/
public without sharing class  CCL_ApheresisDataFormTriggerHandler {
    
/********************************************************************************************************
*  @author         Deloitte
*  @description    This method updates the Apheresis Data Form Label Compliant Field from the Associated
*				   ADF Collection site.
*  @param
*  @date           Oct 16, 2020
*********************************************************************************************************/   
    public void updateADFLabelCompliant(List<CCL_Apheresis_Data_Form__c> adfRecList,Map<Id,CCL_Apheresis_Data_Form__c> oldMapVal) {
        if(CCL_Apheresis_Data_Form__c.SobjectType.getDescribe().isAccessible()){
            Map<ID,CCL_Apheresis_Data_Form__c> updateADFMap = new Map<ID,CCL_Apheresis_Data_Form__c>([select id,CCL_Status__c,CCL_Apheresis_Collection_Center__c,CCL_Apheresis_Collection_Center__r.CCL_Label_Compliant__c from CCL_Apheresis_Data_Form__c where id in:adfRecList]);
            for(CCL_Apheresis_Data_Form__c adfupdated : adfRecList){
                if(adfupdated.CCL_Status__c!='' && adfupdated.CCL_Status__c!=null && (oldMapVal.get(adfupdated.Id).CCL_Status__c!=adfupdated.CCL_Status__c) && adfupdated.CCL_Status__c=='ADF Pending Submission' && oldMapVal.get(adfupdated.Id).CCL_Status__c=='Awaiting ADF' && updateADFMap.get(adfupdated.Id).CCL_Apheresis_Collection_Center__c!=null){
                    adfupdated.CCL_Label_Compliant__c=updateADFMap.get(adfupdated.Id).CCL_Apheresis_Collection_Center__r.CCL_Label_Compliant__c;
                }
            }
        }
    }
}
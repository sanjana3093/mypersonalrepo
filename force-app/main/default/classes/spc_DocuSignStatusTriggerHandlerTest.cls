/**
* @author Deloitte
* @date 08/17/2018
*
* @description This is the Test Class for spc_DocuSignStatusTriggerImplementation
*/
@isTest
public class spc_DocuSignStatusTriggerHandlerTest {
    public static Id ID_Manf_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId();
    public static Id ID_DOC_RECORDTYPE = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get('Fax - Inbound').getRecordTypeId();
    @testSetup static void setup() {


        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        Account manf = spc_Test_Setup.createAccount(ID_Manf_RECORDTYPE);
        insert manf;
        system.assertNotEquals(manf, null);
        PatientConnect__PC_Engagement_Program__c eg = spc_Test_Setup.createEngagementProgram('testEP', manf.id);
        insert eg;
        system.assertNotEquals(eg, null);
        PatientConnect__PC_Document__c oDoc = spc_Test_Setup.createDocument('Fax - Inbound', 'Review Needed', eg.id);
        insert oDoc;
        system.assertNotEquals(oDoc, null);


    }
    @isTest
    static void docusign() {
        test.startTest();
        PatientConnect__PC_Engagement_Program__c engPrgm = [SELECT Id FROM PatientConnect__PC_Engagement_Program__c LIMIT 1];
        Account acc = spc_Test_Setup.createPatient('New Patient');
        insert acc;
        Case programCase = spc_Test_Setup.newCase(acc.Id, 'PC_Program');
        programCase.Status = 'Enrolled';
        programCase.PatientConnect__PC_Engagement_Program__c = engPrgm.Id;
        spc_Database.ins(programCase);

        PatientConnect__PC_Document__c oDoc = [select id from PatientConnect__PC_Document__c LIMIT 1];

        PatientConnect__PC_Document_Log__c doclog = new PatientConnect__PC_Document_Log__c(PatientConnect__PC_Document__c = oDoc.Id, PatientConnect__PC_Program__c = programCase.Id);
        insert doclog;

        dsfs__DocuSign_Envelope__c env = new dsfs__DocuSign_Envelope__c();
        env.dsfs__DocuSign_Envelope_ID__c = 'd9887ed9-4814-4244-8292-c81c8f64307f';
        env.dsfs__Source_Object__c = oDoc.id;
        spc_Database.ins(env);
        system.assertNotEquals(env, null);
        dsfs__DocuSign_Envelope_Document__c envelopDoc = new dsfs__DocuSign_Envelope_Document__c();
        envelopDoc.dsfs__Attachment_ID__c = '00P1D000000Q4vYUAS';
        envelopDoc.dsfs__DocuSign_EnvelopeID__c = env.id;
        spc_Database.ins(envelopDoc);
        dsfs__DocuSign_Status__c docsign = new dsfs__DocuSign_Status__c();
        docsign.dsfs__DocuSign_Envelope_ID__c = env.dsfs__DocuSign_Envelope_ID__c;
        spc_Database.ins(docsign);
        docsign.dsfs__Sender__c = 'test';
        docsign.dsfs__DocuSign_Envelope_ID__c = env.dsfs__DocuSign_Envelope_ID__c;
        docsign.dsfs__Envelope_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOCUSIGN_STATUS_SIGNED);
        spc_Database.upd(docsign);
        test.stopTest();
    }
    public static testmethod void testExecution1() {
        Map<Id, String> mapEnvelopDocuments = new Map<Id, String>();
        Map<String, PatientConnect__PC_Document__c> emailOutboundDocuments = new Map<String, PatientConnect__PC_Document__c>();
        PatientConnect__PC_Document__c oDoc = [select id from PatientConnect__PC_Document__c LIMIT 1];
        Attachment attach = new Attachment();
        attach.Name = 'xyz';
        attach.ParentId = oDoc.id;
        attach.body = blob.valueOf('testAttachment');
        insert attach;
        system.assertNotEquals(attach, null);
        mapEnvelopDocuments.put(attach.id, 'testAttach');
        emailOutboundDocuments.put('testAttach', oDoc);
        spc_DocuSignStatusTriggerImplementation docImpTr = new spc_DocuSignStatusTriggerImplementation();
        test.startTest();
        docImpTr.createOutboundAttachment(emailOutboundDocuments, mapEnvelopDocuments);
        test.stopTest();
    }
}
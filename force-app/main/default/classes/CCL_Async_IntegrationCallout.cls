public with sharing class CCL_Async_IntegrationCallout  implements queueable {
    List<CCL_Invocable_Integration.CCL_Integration_Invocable_Wrapper> listOfIntegrationWrapper = new List<CCL_Invocable_Integration.CCL_Integration_Invocable_Wrapper>();
   
    public CCL_Async_IntegrationCallout(List<CCL_Invocable_Integration.CCL_Integration_Invocable_Wrapper> listOfIntegrationWrapper){
        this.listOfIntegrationWrapper=listOfIntegrationWrapper;
        
    }
    
    public void execute(QueueableContext context) {
        CCL_Invocable_Integration.methodToBeCalledFromAsyncIntegrationProcess(this.listOfIntegrationWrapper);
    }
}
/**
* @author Deloitte
* @date 19-June-2018
*
* @description This is the class for Field Set Controller
*/

public with sharing class spc_FieldSetController {
    /*******************************************************************************************************
    * @description This method is used for getting fields
    *  @param            typeName - field tyoe
    *  @param            fsName
    *  @return            List<FieldSetMember>
    */
    @AuraEnabled
    public static List<FieldSetMember> getFields(String typeName, String fsName) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        Schema.FieldSet fs = fsMap.get(fsName);
        List<Schema.FieldSetMember> fieldSet = fs.getFields();
        List<FieldSetMember> fset = new List<FieldSetMember>();
        for (Schema.FieldSetMember f : fieldSet) {
            fset.add(new FieldSetMember(f, targetType));
        }
        return fset;
    }

    public class FieldSetMember {

        public FieldSetMember(Schema.FieldSetMember f) {
            this.DBRequired = f.DBRequired;
            this.fieldPath = f.fieldPath;
            this.label = f.label;
            this.required = f.required || f.DBRequired;
            this.type = '' + f.getType();
        }

        public FieldSetMember(Schema.FieldSetMember f, Schema.SObjectType targetType) {
            this.DBRequired = f.DBRequired;
            this.fieldPath = f.fieldPath;
            this.label = f.label;
            this.required = f.required || f.DBRequired;
            this.type = '' + f.getType();
            if (f.getType() == Schema.DisplayType.PICKLIST || f.getType() == Schema.DisplayType.MULTIPICKLIST) {
                List<Schema.PicklistEntry> ple = targetType.getDescribe().fields.getMap().get(f.fieldPath).getDescribe().getPicklistValues();
                selectOptions = new List<SelectOptionWrapper>();

                if (f.getType() == Schema.DisplayType.PICKLIST) {
                    selectOptions.add(new SelectOptionWrapper('', '-Select-'));
                }
                for ( Schema.PicklistEntry pe : ple) {
                    selectOptions.add(new SelectOptionWrapper(pe.getValue(), pe.getLabel()));
                }
            }
        }

        public FieldSetMember(Boolean DBRequired) {
            this.DBRequired = DBRequired;
        }


        public Boolean DBRequired { get; set; }

        @AuraEnabled
        public String fieldPath { get; set; }

        @AuraEnabled
        public String label { get; set; }

        @AuraEnabled
        public Boolean required { get; set; }

        @AuraEnabled
        public String type { get; set; }

        @AuraEnabled
        public List<SelectOptionWrapper> selectOptions { get; set; }
    }

    public class SelectOptionWrapper {
        public SelectOptionWrapper(String value, String label) {
            this.value = value;
            this.label = label;
            this.disabled = false;
            this.escapeItem = false;
        }

        public SelectOptionWrapper(String value, String label, Boolean isDisabled) {
            this.value = value;
            this.label = label;
            this.disabled = isDisabled;
            this.escapeItem = false;
        }

        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }

        public Boolean disabled { get; set; }

        public Boolean escapeItem { get; set; }

    }
    public static void copyFieldSetValues(Map<String, Object> fieldSetMap, sobject to_obj) {
        if (fieldSetMap != null) {
            Object fieldValue;
            for (String fieldName : fieldSetMap.keySet()) {
                fieldValue = fieldSetMap.get(fieldName);
                to_obj.put(fieldName, castFieldValueBasedOnType(fieldName, fieldValue, to_obj));
            }
        }
    }

    public static List<String> getFieldsByName(String typeName, String fsName) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        Schema.FieldSet fs = fsMap.get(fsName);
        List<Schema.FieldSetMember> fieldSet = fs.getFields();
        List<String> fset = new List<String>();
        for (Schema.FieldSetMember f : fieldSet) {
            fset.add(f.fieldPath);
        }
        return fset;
    }

    public static object castFieldValueBasedOnType(String fieldName, Object fieldValue, sObject sObj) {
        Schema.SObjectType sObjType = sObj.getSObjectType();
        Schema.DisplayType fieldType = sObjType.getDescribe().fields.getMap().get(fieldName).getDescribe().getType();
        object parsedFieldValue;
        if (fieldType == Schema.DisplayType.STRING) {
            parsedFieldValue = (String) fieldValue;
        } else if (fieldType == Schema.DisplayType.DATE) {
            if (!String.isEmpty(string.valueOf(fieldValue))) {
                parsedFieldValue = Date.valueOf(string.valueOf(fieldValue));
            }
        } else if (fieldType == Schema.DisplayType.DATETIME) {
            String formattedFieldValue = string.valueOf(fieldValue);
            if (!String.isEmpty(formattedFieldValue)) {
                formattedFieldValue = formattedFieldValue.replace('T', ' ');
                formattedFieldValue = formattedFieldValue.substringBeforeLast('.');
                parsedFieldValue = Datetime.valueOf(formattedFieldValue);
            }
        } else if (fieldType == Schema.DisplayType.EMAIL) {
            parsedFieldValue = (String) fieldValue;
        } else if (fieldType == Schema.DisplayType.PICKLIST) {
            parsedFieldValue = (String) fieldValue;
        } else if (fieldType == Schema.DisplayType.BOOLEAN) {
            String formattedFieldValue = string.valueOf(fieldValue);
            if (!String.isEmpty(formattedFieldValue)) {
                parsedFieldValue = Boolean.valueOf(fieldValue);
            } else {
                parsedFieldValue = false;
            }
        } else if (fieldType == Schema.DisplayType.DOUBLE) {
            String formattedFieldValue = string.valueOf(fieldValue);
            if (String.isEmpty(formattedFieldValue)) {
                // nothing to do
            } else {
                parsedFieldValue = Double.valueOf(fieldValue);
            }
        } else if (fieldType == Schema.DisplayType.PHONE) {
            parsedFieldValue = (String) fieldValue;
        } else if (fieldType == Schema.DisplayType.TEXTAREA) {
            parsedFieldValue = (String) fieldValue;
        } else {
            parsedFieldValue = (String) fieldValue;
        }
        return parsedFieldValue;
    }
}
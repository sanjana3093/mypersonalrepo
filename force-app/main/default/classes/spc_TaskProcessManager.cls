/********************************************************************************************************
*  @author          Deloitte
*  @date            Dec 14, 2018
*  @description     spc_TaskProcessManager
*  @version         1.0
*********************************************************************************************************/
public class spc_TaskProcessManager {
    /********************************************************************************************************
    *  @author          Deloitte
    *  @date            Dec 14, 2018
    *  @description     Method to check if a repeated task is getting created.
    *  @param        : Map of parent Id (what id in tasks) and list of task Subject.
    *  @Return Type    : Map of related to Id and list of subjects with same relatedTo Id for repeated tasks found.
                         Task is not a repeated task if not present in the map returned by this method.
    *   @version         1.0
    *********************************************************************************************************/
    public static Map<Id, List<String>> checkTaskRepeatition(Map<Id, List<String>> parentIdAndSubjectsMap) {
        Map<Id, List<String>> repeatedTaskMap = new Map<Id, List<String>>();
        List<String> subjectAvailabilityList;
        for (Task tsk : [Select Id, whatId, Subject, Status, IsClosed from Task where task.whatId in :parentIdAndSubjectsMap.keySet() AND IsClosed = False]) {
            if (null != repeatedTaskMap.get(tsk.whatId)) {
                subjectAvailabilityList = repeatedTaskMap.get(tsk.whatId);
            } else {
                subjectAvailabilityList = new List<String>();
            }
            for (String subject : parentIdAndSubjectsMap.get(tsk.whatId)) {
                if (String.isNotBlank(tsk.Subject) && tsk.Subject.containsIgnoreCase(subject)) {
                    subjectAvailabilityList.add(tsk.Subject);
                }
            }
            if (!subjectAvailabilityList.isEmpty()) {
                repeatedTaskMap.put(tsk.whatId, subjectAvailabilityList);
            }
        }
        return repeatedTaskMap;
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @date            Dec 14, 2018
    *  @description     Method to create a tasks.
    *  @param        : List of taskWrapper class
    *  @Return Type    : Void
    *   @version         1.0
    *********************************************************************************************************/
    public static void createTasks(List<TaskWrapper> taskWrappers) {
        List<Task> tasks = new List<Task>();
        for (TaskWrapper taskWrapper : taskWrappers) {
            if (null != taskWrapper) {
                Task newTask = new Task();
                newTask.Subject = taskWrapper.subject;
                newTask.PatientConnect__PC_Category__c = taskWrapper.category;
                newTask.PatientConnect__PC_Sub_Category__c = taskWrapper.subCategory;
                newTask.Priority = taskWrapper.priority;
                newTask.OwnerId = taskWrapper.ownerId;
                newTask.PatientConnect__PC_Assigned_To__c = taskWrapper.ownerId;
                newTask.WhatId = taskWrapper.relatedTo;
                newTask.ActivityDate = taskWrapper.dueDate;
                newTask.PatientConnect__PC_Document__c = taskWrapper.documentId;
                newTask.Status = taskWrapper.status;
                newTask.PatientConnect__PC_Direction__c = taskWrapper.direction;
                newTask.PatientConnect__PC_Channel__c = taskWrapper.channel;
                if (0 < taskWrapper.reAttemptCount) {
                    newTask.spc_ReAttempt_Count__c = taskWrapper.reAttemptCount;
                }
                if (null != taskWrapper.pcProgram) {
                    newTask.PatientConnect__PC_Program__c = taskWrapper.pcProgram;
                }
                newTask.spc_Interaction__c = taskWrapper.interactionId;
                tasks.add(newTask);
            }
        }
        if (!tasks.isEmpty()) {
            spc_Database.ins(tasks);
        }
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @date            Dec 14, 2018
    *  @description     Method to create a tasks.
    *  @param        : List of taskWrapper class
    *  @Return Type    : Void
    *   @version         1.0
    *********************************************************************************************************/
    public static void checkExistingTaskAndCreateNew(List<TaskWrapper> taskWrappers) {
        Map<Id, List<String>> relatedToIdsandSubjects = new Map<Id, List<String>>();
        // iterating through taskWrappers to prepare map - relatedToIdsandSubjects
        for (TaskWrapper taskWrapper : taskWrappers ) {
            if (relatedToIdsandSubjects.containsKey(taskWrapper.relatedTo) && null != relatedToIdsandSubjects.get(taskWrapper.relatedTo)) {
                relatedToIdsandSubjects.get(taskWrapper.relatedTo).add(taskWrapper.subject);
            } else {
                List<String> subjects = new List<String>();
                subjects.add(taskWrapper.subject);
                relatedToIdsandSubjects.put(taskWrapper.relatedTo, subjects);
            }

        }
        // method call to get map of repeated tasks
        Map<Id, List<String>> repeatedTasksMap = checkTaskRepeatition(relatedToIdsandSubjects);
        List<TaskWrapper> tasksToBeCreatedList = new List<TaskWrapper>();
        for (TaskWrapper taskWrapper : taskWrappers) {
            // if does not exist in repeatedTaskMap, task needs to be created.
            if (String.isNotBlank(taskWrapper.relatedTo) && repeatedTasksMap.containsKey(taskWrapper.relatedTo)) {
                boolean subjectFound = false;
                for (String subject : repeatedTasksMap.get(taskWrapper.relatedTo)) {
                    if (String.isNotBlank(taskWrapper.subject) && subject.containsIgnoreCase(taskWrapper.subject)) {
                        subjectFound = true;
                        break;
                    }
                }
                if (!subjectFound) {
                    tasksToBeCreatedList.add(TaskWrapper);
                }
            } else {
                tasksToBeCreatedList.add(TaskWrapper);
            }
        }
        if (! tasksToBeCreatedList.isEmpty()) {
            // method call to create tasks
            createTasks(tasksToBeCreatedList);
        }
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @date            Dec 14, 2018
    *  @description     Method to create TaskWrapper instance with provided params
    *  @param        : List of taskWrapper class
    *  @Return Type    : spc_TaskProcessManager.TaskWrapper
    *   @version         1.0
    *********************************************************************************************************/

    public static spc_TaskProcessManager.TaskWrapper createOpenTaskAfterUpdate(Case caseObj, String channel, Date dueDate, String priority,
            String category, String subCategory, String subject,
            Decimal reAttemptCount, String direction) {
        spc_TaskProcessManager.TaskWrapper taskWrapper = new spc_TaskProcessManager.TaskWrapper();
        taskWrapper.dueDate = dueDate;
        taskWrapper.category = category;
        taskWrapper.subCategory = subCategory;
        taskWrapper.channel = channel;
        taskWrapper.priority = priority;
        taskWrapper.Subject = subject;
        taskWrapper.status = spc_Apexconstants.STATUS_INDICATOR_NOTSTARTED;
        taskWrapper.direction = direction;
        taskWrapper.reAttemptCount = reAttemptCount;
        if (null != caseObj && null != caseObj.PatientConnect__PC_Program__c
                && String.isNotBlank(caseObj.PatientConnect__PC_Program__c)
                && (caseObj.RecordTypeId != spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.CASE_RT_REFERRAL, Case.SobjectType))) {
            taskWrapper.OwnerId = caseObj.PatientConnect__PC_Program__r.OwnerId;
            taskWrapper.relatedTo = caseObj.PatientConnect__PC_Program__c;
            taskWrapper.interactionId = caseObj.spc_Interaction_Id__c;
        } else if (null != caseObj) {
            taskWrapper.OwnerId = caseObj.OwnerId;
            taskWrapper.relatedTo = caseObj.id;
            taskWrapper.interactionId = caseObj.spc_Interaction_Id__c;
        }

        return taskWrapper;
    }

    /*********************************************************************************************************
        class Name      : TaskWrapper
        Created By      : Soumya Mohapatra
        Description     : wrapper class for Task creation
    ****************************************************************************************************************/
    public class TaskWrapper {
        @AuraEnabled public String subject { get; set; }
        @AuraEnabled public String category { get; set; }
        @AuraEnabled public String subCategory { get; set; }
        @AuraEnabled public String priority { get; set; }
        @AuraEnabled public Id relatedTo { get; set; }  //whatId
        @AuraEnabled public Id ownerId { get; set; }
        @AuraEnabled public Date dueDate { get; set; }
        @AuraEnabled public Id documentId { get; set; }
        @AuraEnabled public String status { get; set; }
        @AuraEnabled public String direction { get; set; }
        @AuraEnabled public String channel { get; set; }
        @AuraEnabled public Decimal reAttemptCount { get; set; }
        @AuraEnabled public Id interactionId { get; set; }
        @AuraEnabled public Id pcProgram { get; set;}
        public TaskWrapper() {}
    }
}
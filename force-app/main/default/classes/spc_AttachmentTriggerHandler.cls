/*********************************************************************************************************
* @author Deloitte
* @date July 3,2018
* @description This handles operation related to DML operations of attachment
****************************************************************************************************************/
public class spc_AttachmentTriggerHandler extends spc_TriggerHandler {
    public override void afterInsert() {
        //Map for Attachment created on Document object
        Map<Id, Attachment> mapDocumentAttachment = new Map<Id, Attachment>();
        //Map for DocuSign Status ID to Attachment
        Map<Id, Attachment> mapDocuSignStatusToAttachment = new Map<Id, Attachment>();
        for (Attachment a : (List<Attachment>)Trigger.new) {
            if (a.ParentId.getSObjectType() == PatientConnect__PC_Document__c.sObjectType) {
                mapDocumentAttachment.put(a.ParentId, a);
            } else if (a.ParentId.getSObjectType() == dsfs__DocuSign_Status__c.sobjectType) {
                // if record created for for DocuSign status object
                mapDocuSignStatusToAttachment.put(a.ParentId, a);
            }
        }


        if (!mapDocumentAttachment.isEmpty()) {
            processDocumentAttachments(mapDocumentAttachment);
        }

        if (!mapDocuSignStatusToAttachment.isEmpty()) {
            processDocuSignAttachments(mapDocuSignStatusToAttachment);
        }
    }

    /*********************************************************************************
        Method Name    : processDocuSignAttachments
        Developer      : Sanjana
        Description    : Update Attachment id on Document record
        Return Type    : Void
        Paramter       : Attachment
        Modified By    : Sanjana Tripathy on 03 July 2018
    *********************************************************************************/
    @testVisible
    private void processDocumentAttachments(Map<Id, Attachment> mapDocumentAttachment) {
        Map<Id,  PatientConnect__PC_Document__c> mapPCDocuments = new Map<Id, PatientConnect__PC_Document__c>();
        for (Id docId :  mapDocumentAttachment.keySet()) {
            PatientConnect__PC_Document__c doc = new PatientConnect__PC_Document__c(Id = docId);
            doc.PatientConnect__PC_Attachment_Id__c = mapDocumentAttachment.get(docId).Id;
            mapPCDocuments.put(docId, doc);
        }
        update mapPCDocuments.values();
    }

    /*********************************************************************************
       Method Name    : processDocuSignAttachments
       Developer      : Sanjana
       Description    : Copy DocuSign signed attached to Inbound Email document
       Return Type    : Void
       Paramter       : Attachment
       Modified By    : Sanjana Tripathy on 03 July 2018
    *********************************************************************************/
    @testVisible
    private void processDocuSignAttachments(Map<Id, Attachment> mapDocuSignStatusToAttachment) {
        List<Attachment> attachmentsToInsert = new List<Attachment>();
        for (dsfs__DocuSign_Status__c status : [SELECT
                                                ID, spc_Inbound_Document__c
                                                FROM
                                                dsfs__DocuSign_Status__c
                                                WHERE Id in: mapDocuSignStatusToAttachment.keySet() AND spc_Inbound_Document__c != null]) {
            Attachment newAttach = mapDocuSignStatusToAttachment.get(status.Id).clone(false, false, false);
            newAttach.ParentId = status.spc_Inbound_Document__c;
            attachmentsToInsert.add(newAttach);
        }
        insert attachmentsToInsert;
    }

}
public with sharing class CCL_DocumentDeleteSchedulerStart implements Schedulable{

    public void execute(SchedulableContext ctx) {
        System.schedule('Document Delete Batch', '0 0 * * * ?', new CCL_DocumentDeleteBatch_Scheduler() );  
    }
}
/*********************************************************************************************************
class Name      : PSP_CarePrgmEnrolleProductTriggerHandler 
Description     : Trigger Handler Class for CareProgramEnrolleeProduct
@author         : Soumya Mohapatra  
@date           : November 29, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Soumya Mohapatra            November 29, 2019          Initial Version
****************************************************************************************************************/ 
public class PSP_CarePrgmEnrolleProductTriggerHandler extends PSP_trigger_Handler {
     /*Trigger impementation being invoked.*/
    PSP_CarePrgmEnrolleProTrggrImpl triggerImpl = new PSP_CarePrgmEnrolleProTrggrImpl ();
    
      /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Before Insert method of Care Program Enrollee Product
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    public override void beforeInsert () {        
    }
    /**************************************************************************************
  	* @author      : Shourya
  	* @date        : 11/26/2019
  	* @Description : After Insert Method being overriden here
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/    
	public override void afterInsert () { 
        final List<CareProgramEnrolleeProduct> cPEnroleeProdLst = new List<CareProgramEnrolleeProduct> ();
        cPEnroleeProdLst.addAll((List<CareProgramEnrolleeProduct>)Trigger.new);
        if(!cPEnroleeProdLst.isEmpty()) {
            triggerImpl.createCarePlans(cPEnroleeProdLst);
        }
    }
}
/*********************************************************************************************************
class Name      : PSP_Account_TriggerHandler
Description		: Trigger Handler Class for Care Program Object
@author     	: Deloitte
@date       	: Sept 25, 2019
Modification Log:
--------------------------------------------------------------------------------------------------------------
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------
Manikanta N V           Sept 25, 2019          Initial Version
****************************************************************************************************************/
public
class PSP_Account_TriggerHandler extends PSP_trigger_Handler {
    /* trigger implementation being called */
    final PSP_AccountTrggrImpl triggerImpl = new PSP_AccountTrggrImpl ();
    /********************************************************************************************************
	*  @author          Deloitte
	*  @description     Before Insert method of Account
	*  @date            Sept 25, 2019
	*  @version         1.0
	*********************************************************************************************************/

    public override void beforeInsert() {
         triggerImpl.validateBeforeInsertUpdate((Map<Id,Account>) Trigger.oldMap,(Map<Id,Account> ) Trigger.newMap, (List<Account>) Trigger.new);
        }
    
    /********************************************************************************************************
	*  @author          Deloitte
	*  @description     Before Update method of Account
	*  @date            Sept 25, 2019
	*  @version         1.0
	*********************************************************************************************************/
    public override void beforeUpdate() {    
        triggerImpl.validateBeforeInsertUpdate((Map<Id,Account>) Trigger.oldMap,(Map<Id,Account> ) Trigger.newMap, (List<Account>) Trigger.new);
           }
        
}
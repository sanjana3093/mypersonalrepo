/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the test class for PSP_signatureHandler
*  @date            09/18/2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                        Description
--------------------------------------------------------------------------------------------------------------            
Deloitte            dec 08, 2019          Initial Version
****************************************************************************************************************/
@IsTest
public class PSP_SignatureHandlerTest {
    /*Constant for test */
    static final String NEWTEST = 'Test';
    /* Static list for initializing apex constants */
    static final List<PSP_ApexConstantsSetting__c>  APEX_CONSTANTS =  PSP_Test_Setup.setDataforApexConstants();
    /*************************************************************************************
    * @author      : Deloitte
    * @date        : 09/17/2019
    * @Description : Testing insert scenarios
    * @Param       : Void
    * @Return      : Void
    ***************************************************************************************/
    
    public static testMethod void testRunAs () {
        Test.startTest();
        final Account acco = PSP_Test_Setup.getPersonAccount ('Testpat');
        Database.insert(acco);
        final AuthorizationForm authForm=new AuthorizationForm();
        authForm.Name=NEWTEST;
        authForm.EffectiveToDate=SYSTEM.today()+5;
        authForm.EffectiveFromDate=SYSTEM.today();
        authForm.RevisionNumber='1234';
        Database.insert(authForm);
        final AuthorizationFormText authformtext=new AuthorizationFormText();
        authformtext.Name=NEWTEST;
        authformtext.AuthorizationFormId=authForm.Id;
        Database.insert(authformtext);
        final AuthorizationFormConsent authFormConsent=new AuthorizationFormConsent();
        authFormConsent.Name=NEWTEST;
        authFormConsent.ConsentCapturedDateTime=SYSTEM.today();
        authFormConsent.PSP_Consent_Type__c='Patient';
        authFormConsent.ConsentCapturedSource=NEWTEST;
        authFormConsent.ConsentCapturedSourceType='Phone';
        authFormConsent.AuthorizationFormTextId=authformtext.Id;
        authFormConsent.Status='Seen';
        authFormConsent.ConsentGiverId=acco.Id;
        Database.insert(authFormConsent);
        Test.stopTest();
        
        try {
            //authFormConsent.Status='Signed';
            PSP_SignatureHandler.saveSign('test','image/png',authFormConsent.Id,acco.Id);
            Database.update(authFormConsent);
            
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains ('Insert failed.'),e.getMessage () );
        }
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     method for fetching current careprogram record.
    *  @param1          RecordID
    *  @date            Aug 12, 2019
    *********************************************************************************************************/   
    public static testMethod void testRunCreate () {
        Test.startTest();
        
        final Account acco = PSP_Test_Setup.getPersonAccount ('Testpat');
        Database.insert(acco);
        final AuthorizationForm authForm=new AuthorizationForm();
        authForm.Name=NEWTEST;
        authForm.EffectiveToDate=SYSTEM.today()+5;
        authForm.EffectiveFromDate=SYSTEM.today();
        authForm.RevisionNumber='1234';
        Database.insert(authForm);
        final AuthorizationFormText authformtext=new AuthorizationFormText();
        authformtext.Name=NEWTEST;
        authformtext.AuthorizationFormId=authForm.Id;
        Database.insert(authformtext);
        //Create Document
        final ContentVersion cver = new ContentVersion();
        cver.Title = 'Test Document';
        cver.PathOnClient = 'TestDocument.pdf';
        cver.VersionData = Blob.valueOf('Test Content');
        cver.IsMajorVersion = true;
        Database.insert(cver);
        
        //Get Content Version
        final List<ContentVersion> cvList = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :cver.Id];
        final String authFormConsent='{"sobjectType":"AuthorizationFormConsent","consentCapturedDateTime":"2019-12-09","name":"Consent-test demo-Patient","status":"Seen","effectiveFrom":"2019-12-09","consentType":"Patient","contactMethods":"Email","consentGiverId":"'+acco.Id+'","authformTextId":"'+authformtext.Id+'","consentCapturedSource":"test","consentCapturedSourceType":"Phone"}';
        Test.stopTest();
        
        try {
            PSP_SignatureHandler.saveAuthRecord(authFormConsent,cvList[0].ContentDocumentId);
            
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains ('Insert failed.'),e.getMessage () );
        }
    }
}
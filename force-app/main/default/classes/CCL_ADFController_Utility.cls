public with sharing class CCL_ADFController_Utility {
  /********************************************************************************************************
*  @author          Deloitte
*  @description     to check user permission of the specific permission Set
*  @param           Permission Set API Name
*  @date            July 8, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static Boolean checkUserPermission(String permissionName) {
    try {
    Boolean hasPermission=false;
    final List<PermissionSetAssignment> permissionSet=[SELECT Id, PermissionSet.Name,AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :Userinfo.getUserId()];
    for(PermissionSetAssignment perSet:permissionSet) {
        if(perSet.PermissionSet.Name!=null && perSet.PermissionSet.Name.Equals(permissionName)) {
            hasPermission=true;
        }
    }
    return hasPermission;
    } catch(QueryException ex){
        System.debug('Exception in CCL_ADFController_Utility checkUserPermission'+ex.getMessage());
        throw  ex;
    }

}
    
        /********************************************************************************************************
*  @author          Deloitte
*  @description     to check user permission of the specific permission Set
*  @param           Permission Set API Name
*  @date            December 11, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<PermissionSetAssignment> getAllPermissionSets() {
    try {
       final List<PermissionSetAssignment> permissionSet=[SELECT Id, PermissionSet.Name,AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :Userinfo.getUserId()];
      return permissionSet;
    } catch(QueryException ex){
        System.debug('Exception in CCL_ADFController_Utility getAllPermissionSets'+ex.getMessage());
        throw  ex;
    }

}

    /********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetch the file uploaded by the user on the record
*                   Added as part of CGTU-318
*  @param           userName and completionDate
*  @date            July 7, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<ContentDocumentLink> getUploadedFileDetails(Id recordId) {
    try {
            final String cTProfile=CCL_StaticConstants_MRC.CT_INTEGRATION_PROFILE_TYPE;
            final List<ContentDocumentLink> fileDetails=[SELECT Id,ContentDocumentId,ContentDocument.LatestPublishedVersionId, ContentDocument.Title,ContentDocument.FileType,ContentDocument.FileExtension,ContentDocument.LatestPublishedVersion.CCL_Document_Classification__c  FROM ContentDocumentLink WHERE LinkedEntityId =:recordId AND ContentDocument.CreatedBy.Profile.Name!=:cTProfile];
            return fileDetails;
    } catch(QueryException ex) {
        System.debug('Exception in CCL_ADF_Controller getUploadedFileDetails'+ex.getMessage());
        throw  ex;
    }
}

/********************************************************************************************************
*  @author         Deloitte
*  @description    This method is being used to fetch the summary page details in ADF
*  @param
*  @date            July 10, 2020
*********************************************************************************************************/
@AuraEnabled
public static List<CCL_Apheresis_Data_Form__c> getADFSummaryPageInfo(Id recordId) {
    try {
        List<CCL_Apheresis_Data_Form__c> adfList=new List<CCL_Apheresis_Data_Form__c>();
        if (CCL_Apheresis_Data_Form__c.sObjectType.getDescribe().isAccessible()) {
            adfList=[select id,CCL_Status__c,CCL_Land_on_Patient_Verification__c,CCL_Collection_Details_Completed_By__c,CCL_Order__r.CCL_PRF_Ordering_Status__c,CCL_Order__r.CCL_Order_Approval_Eligibility__c,CCL_Order__r.CCL_PRF_Approval_Counter__c,CCL_Collection_Details_Completed_By__r.FirstName,CCL_Collection_Details_Completed_By__r.LastName,CCL_Collection_Details_Completed_Date__c,CCL_ADF_Submitted_By__c,CCL_ADF_Submitted_By__r.Id,CCL_ADF_Submitted_By__r.FirstName,CCL_ADF_Submitted_By__r.LastName,CCL_ADF_Submitted_Date_Time__c,CCL_ADF_Approved_By__c,CCL_ADF_Approved_By__r.Id,CCL_ADF_Approved_By__r.FirstName,CCL_ADF_Approved_By__r.LastName,CCL_ADF_Approved_Date_Time__c,CCL_ADF_Locked_For_Edit__c, CCL_Rejection_Reason__c, CCL_ADF_Rejected_Date_Time__c, CCL_ADF_Rejected_By__c, CCL_ADF_Rejected_By__r.Id, CCL_ADF_Rejected_By__r.FirstName, CCL_ADF_Rejected_By__r.LastName,CCL_TEXT_ADF_Submitted_Date_Time__c,CCL_TEXT_ADF_Approved_Date_Time__c,CCL_TEXT_Collection_Details_Date_Time__c	 from CCL_Apheresis_Data_Form__c where id=:recordId WITH SECURITY_ENFORCED ];
        }
        return adfList;
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw ex;
    }
}
/********************************************************************************************************
*  @author          Deloitte
*  @description    to fetch Infectious disease data data
*  @param
*  @date            july 07, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<CCL_Infectious_Diseases_Testing__mdt > getInfectousDiseases(string therapyName) {
   List<CCL_Infectious_Diseases_Testing__mdt> myDiseaseList = new List<CCL_Infectious_Diseases_Testing__mdt>();
    try {
       final String mytherapyName='%'+therapyName+'%';

        if (CCL_Infectious_Diseases_Testing__mdt.sObjectType.getDescribe().isAccessible()) {

            myDiseaseList=[select CCL_Field_Api_Name__c,Masterlabel ,CCL_Display_Label__c,CCL_Order_of_Field__c
             from CCL_Infectious_Diseases_Testing__mdt  order by CCL_Order_of_Field__c limit 50];
        }

    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw  ex;
    }
    return myDiseaseList;
}
/********************************************************************************************************
*  @author         Deloitte
*  @description    This method is being used to fetch the summary Object details in ADF
*  @param
*  @date            July 27, 2020
*********************************************************************************************************/
@AuraEnabled
public static List<CCL_summary__c> getSummaryObjectsInfo(Id recordId) {
    try {
        final Id recordTypeId = Schema.SObjectType.CCL_summary__c.getRecordTypeInfosByName().get('Manufacturing').getRecordTypeId();
        List<CCL_summary__c> summaryList=new List<CCL_summary__c>();
        if (CCL_summary__c.sObjectType.getDescribe().isAccessible()) {
            summaryList=[select id,CCL_Manufacturing_Start_Date_Time__c from CCL_summary__c where CCL_Order__r.id=:recordId AND RecordTypeId=:recordTypeId WITH SECURITY_ENFORCED ];
        }
        return summaryList;
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw ex;
    }
}
/********************************************************************************************************
*  @author          Deloitte
*  @description     to create Collections using the JSON data from flow
*  @param
*  @date            june 24, 2020
*********************************************************************************************************/
@AuraEnabled
public static string deleteCollections(List<CCL_summary__c> delList) {
   if(delList!=null ) {

try {
if (CCL_Summary__c.sObjectType.getDescribe().isAccessible()) {
Database.delete(delList);
}
} catch(AuraHandledException ex) {
    System.debug(ex);
    throw ex;
}
}
return Json.serialize(delList);
}

/* ********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetch the file uploaded by the Integration user on the record
*  @date            July 7, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<Account> getTimeZone(Id recordId) {
      List<Account> accountDetails=new List<Account>();
    try {
         if (Account.sObjectType.getDescribe().isAccessible()) {
               accountDetails=[SELECT CCL_Time_Zone__r.Name,CCL_Time_Zone__r.CCL_Saleforce_Time_Zone_Key__c FROM Account WHERE id=:recordId  WITH SECURITY_ENFORCED limit 1];

         }

    } catch(QueryException ex) {
        System.debug('Exception in CCL_ADF_Controller getADFFileDetails'+ex.getMessage());
        throw  ex;
    }
      return accountDetails;
}
      /* ********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetchtherapy type and ordering hopspital op ti value for a shipment
*  @date            July 7, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<sObject> getTherapyType(Id recordId, String objectType) {
    List<sObject> recordDetails=new List<sObject>();
    try {
        if (CCL_Shipment__c.sObjectType.getDescribe().isAccessible() && objectType == 'Shipment') {
            recordDetails=[SELECT CCL_Indication_Clinical_Trial__c, CCL_Ordering_Hospital__c, CCL_Indication_Clinical_Trial__r.CCL_Type__c, CCL_Novartis_Batch_Id__c, CCL_Actual_Apheresis_Received_Date_Time__c, CCL_Order_Apheresis__c, CCL_Order__r.CCL_Treatment_Status__c, CCL_Order__r.CCL_Logistic_Status__c,CCL_Order__r.CCL_Patient_Initials__c,CCL_Order__r.CCL_Patient_Id__c,CCL_Order_Apheresis__r.CCL_Patient_Id__c,CCL_Order_Apheresis__r.CCL_Initials_COI__c, CCL_Actual_Aph_Pick_up_Date_Time__c, CCL_Order__r.CCL_Initials_COI__c FROM CCL_Shipment__c WHERE id=:recordId  WITH SECURITY_ENFORCED limit 1];
        }
         if (CCL_Order__c.sObjectType.getDescribe().isAccessible() && objectType == 'Order') {
            recordDetails=[SELECT CCL_Therapy__c, CCL_Ordering_Hospital__c, CCL_Therapy__r.CCL_Type__c, CCL_Novartis_Batch_Id__c,CCL_Patient_Initials__c,CCL_Initials_COI__c  FROM CCL_Order__c WHERE id=:recordId limit 1];
         }
         return recordDetails;
    } catch(QueryException ex) {
        System.debug('Exception in CCL_ADF_Controller getADFFileDetails'+ex.getMessage());
        throw  ex;
    } catch(Exception ex) {
        throw new AuraHandledException(ex.getMessage());
    }
}
 /* ********************************************************************************************************
*  @author          Deloitte
*  @description     tofetch shipmemt header
*  @param
*  @date            july 27, 2020
*********************************************************************************************************/

@AuraEnabled(cacheable=true)
public static List<sObject > getShipmentHeader(String sobj) {
    List<SObject> finalLst = new List<SObject>();
    final List<CCL_Shipment_Header__mdt> shipmentfinalLst = new List<CCL_Shipment_Header__mdt>();
    final List<CCL_PRF_Header__mdt> prffinalLst = new List<CCL_PRF_Header__mdt>();
    try {

        if (CCL_Shipment_Header__mdt.sObjectType.getDescribe().isAccessible() && sobj == 'CCL_Shipment__c') {
            final List<CCL_Shipment_Header__mdt> mySpimentLst=[select CCL_Field_API_Name__c,CCL_Field_Type__c,Masterlabel,CCL_Object_API_Name__c,CCL_Order_of_field__c
             from CCL_Shipment_Header__mdt  order by CCL_Order_of_field__c limit 20];
             shipmentfinalLst.addAll(mySpimentLst);
        }
        if (CCL_PRF_Header__mdt.sObjectType.getDescribe().isAccessible() && sobj == 'CCL_Order__c') {
            final List<CCL_PRF_Header__mdt> myPRFLst=[select CCL_Field_API_Name__c,CCL_Field_Type__c,Masterlabel,CCL_Object_API_Name__c,CCL_Order_of_field__c
             from CCL_PRF_Header__mdt  order by CCL_Order_of_field__c limit 20];
             prffinalLst.addAll(myPRFLst);
        }
        if(!prffinalLst.isEmpty() || !shipmentfinalLst.isEmpty()) {
            final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            final Schema.SObjectType mySchema = schemaMap.get(sobj);
            final Map <String, Schema.SObjectField> fieldMap = mySchema.getDescribe().fields.getMap();
            for(CCL_Shipment_Header__mdt cs:shipmentfinalLst) {
                cs.Masterlabel=fieldMap.get(cs.CCL_Field_Api_Name__c).getDescribe().getLabel();
            }
            for(CCL_PRF_Header__mdt ps:prffinalLst) {
                ps.Masterlabel=fieldMap.get(ps.CCL_Field_Api_Name__c).getDescribe().getLabel();
            }
        }
        if(!prffinalLst.isEmpty()) {
            finalLst = prffinalLst;
        } else if(!shipmentfinalLst.isEmpty()) {
            finalLst = shipmentfinalLst;
        }
        return finalLst;
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw  ex;
    } catch(Exception ex) {
        throw new AuraHandledException(ex.getMessage());
    }
}


      /* ********************************************************************************************************
*  @author          Deloitte
*  @description     tofetch shipmemt details
*  @param
*  @date            july 27, 2020
*********************************************************************************************************/

@AuraEnabled(cacheable=true)
public static List<CCL_Aph_Shipment_Detail__mdt > getShipmentDetails() {
    final List<CCL_Aph_Shipment_Detail__mdt> shipmentFinalLst = new List<CCL_Aph_Shipment_Detail__mdt>();
    try {

        if (CCL_Aph_Shipment_Detail__mdt.sObjectType.getDescribe().isAccessible()) {

            final List<CCL_Aph_Shipment_Detail__mdt> mySpimentLst=[select CCL_Field_API_Name__c,CCL_Field_Length__c,CCL_HasAddress__c,CCL_isEditable__c,CCL_Field_Type__c,Masterlabel,CCL_Object_API_Name__c,CCL_Order_of_field__c
             from CCL_Aph_Shipment_Detail__mdt  order by CCL_Order_of_field__c limit 20];
             shipmentFinalLst.addAll(mySpimentLst);
        }
        if(!shipmentFinalLst.isEmpty()) {
            final String obj=shipmentFinalLst[0].CCL_Object_API_Name__c;
            final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            final Schema.SObjectType mySchema = schemaMap.get(obj);
            final Map <String, Schema.SObjectField> fieldMap = mySchema.getDescribe().fields.getMap();
            for(CCL_Aph_Shipment_Detail__mdt cs:shipmentFinalLst) {
                cs.Masterlabel=fieldMap.get(cs.CCL_Field_Api_Name__c).getDescribe().getLabel();

            }

        }
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw  ex;
    }
    return shipmentFinalLst;
}
     /* ********************************************************************************************************
*  @author          Deloitte
*  @description     tofetch shipmemt details
*  @param
*  @date            july 27, 2020
*********************************************************************************************************/

@AuraEnabled(cacheable=true)
public static List<CCL_Batch__c > getCryoBagDetails(Id orderId) {
      List <CCL_Apheresis_Data_Form__c> adfDetails=new List<CCL_Apheresis_Data_Form__c>();
    List<CCL_Batch__c> bags=new List<CCL_Batch__c>();
     if (CCL_Apheresis_Data_Form__c.sObjectType.getDescribe().isAccessible()) {
            adfDetails=[SELECT id,Name FROM CCL_Apheresis_Data_Form__c WHERE CCL_Order__c=:orderId  WITH SECURITY_ENFORCED limit 1];

         }
       if (CCL_Batch__c.sObjectType.getDescribe().isAccessible()) {
           bags=[select Id,CCL_Cryobag_ID__c,CCL_COI_ID__c,CCL_Label_Compliant__c,CCL_Shipment__c from CCL_Batch__c where CCL_Apheresis_Data_Form__c=:adfDetails[0].Id WITH SECURITY_ENFORCED];
       }
    return bags;

}
	/* ********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetch the file uploaded by the Integration user on the record
*  @date            July 7, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<ContentDocumentLink> getADFFileDetails(Id recordId) {
    try {
            final List<ContentDocumentLink> fileDetails=[SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId=:recordId AND ContentDocument.CreatedBy.Profile.Name='CT Integration' order by SystemModstamp desc limit 1];
            return fileDetails;
    } catch(QueryException ex) {
        System.debug('Exception in CCL_ADF_Controller getADFFileDetails'+ex.getMessage());
        throw  ex;
    }
}
/* ********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetch ordering hospital opt in
*  @date            July 7, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<CCL_Therapy_Association__c> fetchHospitalOptIn(Id therapyId,Id hospitalId) {
      List<CCL_Therapy_Association__c> shipmentDetails=new List<CCL_Therapy_Association__c>();
    try {
         if (CCL_Therapy_Association__c.sObjectType.getDescribe().isAccessible()) {
            shipmentDetails=[SELECT CCL_Hospital_Patient_ID_Opt_In__c FROM CCL_Therapy_Association__c WHERE CCL_Therapy__c=:therapyId and CCL_Site__c=:hospitalId  WITH SECURITY_ENFORCED limit 1];

         }

    } catch(QueryException ex) {
        System.debug('Exception in CCL_ADF_Controller getADFFileDetails'+ex.getMessage());
        throw  ex;
    }

      return shipmentDetails;
}

  /* ********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to delete the associated content documents
*  @date            July 7, 2020
*********************************************************************************************************/
@AuraEnabled
public static Integer deleteFile(Id contentDocumentId) {
    try {
    final List<ContentDocument> contentDoc=[SELECT Id,FileType,Title FROM ContentDocument WHERE ID=:contentDocumentId WITH SECURITY_ENFORCED limit 1];
    delete contentDoc[0];
    } catch(QueryException ex) {
        throw  ex;
    }
    return 1;
}
	      /* ********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetch shipment Associated ADF status
*  @date            Aug 8, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<CCL_Shipment__c> getAssociatedADFStatus(Id recordId) {
     final List<CCL_Shipment__c> shipmentDetails;
    try {
         if (CCL_Shipment__c.sObjectType.getDescribe().isAccessible()) {
            shipmentDetails=[SELECT CCL_Apheresis_Data_Form__r.CCL_Status__c,CCL_Apheresis_Data_Form__r.Id,CCL_Status__c,CCL_Order_Apheresis__r.CCL_Order_Approval_Eligibility__c,CCL_Order_Apheresis__r.CCL_PRF_Ordering_Status__c,CCL_Order_Apheresis__r.CCL_Initial_PRF_Approved__c  FROM CCL_Shipment__c WHERE id=:recordId  WITH SECURITY_ENFORCED limit 1];

         }

    } catch(QueryException ex) {
        System.debug('Exception in CCL_ADF_Controller getADFFileDetails'+ex.getMessage());
        throw  ex;
    }
      return shipmentDetails;
}

	      /* ********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetch Order Hard Peg Associated with Shipment
*  @date            Aug 8, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<CCL_Shipment__c> getAssociatedOrderHardPeg(Id recordId) {
    final List<CCL_Shipment__c> shipmentDetails;
    try {
         if (CCL_Shipment__c.sObjectType.getDescribe().isAccessible()) {
            shipmentDetails=[SELECT Id, CCL_Order_Apheresis__r.CCL_Hard_Peg__c, CCL_Order_Apheresis__r.Id FROM CCL_Shipment__c WHERE id=:recordId  WITH SECURITY_ENFORCED limit 1];

         }

    } catch(QueryException ex) {
        System.debug('Exception in CCL_ADF_Controller getAssociatedOrderHardPeg'+ex.getMessage());
        throw  ex;
    }
      return shipmentDetails;
}

/* ********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetch the timezone details
*  @date            Aug 8, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static Map<String, String> getTimeZoneDetails(String screenName, Id adfId){
    final Map<String, String> fieldAndTimeZoneMap;
    fieldAndTimeZoneMap=CCL_Utility.getSiteTimeZone(screenName, adfId);
    return fieldAndTimeZoneMap;
}

    /* ********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetch the timezone details
*  @date            Aug 8, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static Map<String, String> getTimeZoneIDDetails(String screenName, Id adfId){
    final Map<String, String> fieldAndTimeZoneMap;
    fieldAndTimeZoneMap=CCL_Utility.getSiteTimeZoneID(screenName, adfId);
    return fieldAndTimeZoneMap;
}

   /* ********************************************************************************************************
*  @author          Deloitte
*  @description     tofetch shipmemt header
*  @param
*  @date            july 27, 2020
*********************************************************************************************************/

@AuraEnabled(cacheable=true)
public static List<CCL_Date_Time_Site_Mapping__mdt > getPageTimeZoneDetails(String screenName) {
    final List<CCL_Date_Time_Site_Mapping__mdt> pageTimeZoneFinalLst = new List<CCL_Date_Time_Site_Mapping__mdt>();
    try {

        if (CCL_Date_Time_Site_Mapping__mdt.sObjectType.getDescribe().isAccessible()) {

            final List<CCL_Date_Time_Site_Mapping__mdt> timeZoneLst=[select CCL_Date_Time_field_API__c,CCL_Object_API__c,CCL_Screen_Name__c,CCL_Site__c,CCL_Site_API_name__c,CCL_Date_Time_Text_API__c
            from CCL_Date_Time_Site_Mapping__mdt where CCL_Screen_Name__c = :screenName];
             pageTimeZoneFinalLst.addAll(timeZoneLst);
        }
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw  ex;
    }
    return pageTimeZoneFinalLst;
}
/* ********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetch the timezone details
*  @date            Aug 8, 2020
*********************************************************************************************************/
@AuraEnabled
public static DateTime getFormattedDateTimeDetails(String dateTimeVal, String timeZoneVal){
    final DateTime correctedDateTime;
    correctedDateTime=CCL_Utility.getDateTimeValue(dateTimeVal, timeZoneVal);
    return correctedDateTime;
}
/********************************************************************************************************
*  @author          Deloitte
*  @description    to fetch shipment recordid
*  @param
*  @date            june 14, 2020
*********************************************************************************************************/
@AuraEnabled
public static List<CCL_Shipment__c> getShipmentInfo(Id recordId) {
    try {
        List<CCL_Shipment__c> shipList=new List<CCL_Shipment__c>();
        if (CCL_Shipment__c.sObjectType.getDescribe().isAccessible()) {
            shipList=[select id,CCL_Apheresis_Data_Form__c,CCL_Apheresis_Data_Form__r.CCL_SaveJSON__c,CCL_Apheresis_Data_Form__r.CCL_PRF_Updated__c,CCL_Status__c from CCL_Shipment__c where CCL_Apheresis_Data_Form__c=:recordId and recordType.Developername = 'CCL_Apheresis' LIMIT 1 ];
        }
        return shipList;
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw ex;
    }
}
/* ********************************************************************************************************
*   @author          Deloitte
*  @description     This Method is being used to fetch Order Hard Peg Associated with ADF
*  @date            Aug 26, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<CCL_Apheresis_Data_Form__c> getADFAssociatedOrderHardPeg(Id recordId) {
    final List<CCL_Apheresis_Data_Form__c> adfDetails;
    try {
         if (CCL_Apheresis_Data_Form__c.sObjectType.getDescribe().isAccessible()) {
            adfDetails=[SELECT CCL_Order__r.CCL_Hard_Peg__c, CCL_Order__r.Id FROM CCL_Apheresis_Data_Form__c WHERE id=:recordId  WITH SECURITY_ENFORCED limit 1];

         }

    } catch(QueryException ex) {
        System.debug('Exception in CCL_ADF_Controller getADFAssociatedOrderHardPeg'+ex.getMessage());
        throw  ex;
    }
      return adfDetails;
}
/* ********************************************************************************************************
*  @author          Deloitte
*  @description     tofetch shipmemt header
*  @param
*  @date            August 30, 2020
*********************************************************************************************************/

@AuraEnabled(cacheable=true)
public static List<CCL_Dashboard_Details__mdt > getOrderHeader() {
    final List<CCL_Dashboard_Details__mdt> orderFinalLst = new List<CCL_Dashboard_Details__mdt>();
    try {

        if (CCL_Dashboard_Details__mdt.sObjectType.getDescribe().isAccessible()) {

            final List<CCL_Dashboard_Details__mdt> orderLst=[select CCL_Field_API_Name__c,CCL_Field_Type__c,Masterlabel,CCL_Object_API_Name__c,CCL_Order_of_field__c
             from CCL_Dashboard_Details__mdt  order by CCL_Order_of_field__c limit 20];
             orderFinalLst.addAll(orderLst);
        }
        if(!orderFinalLst.isEmpty()) {
            final String obj=orderFinalLst[0].CCL_Object_API_Name__c;
            final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            final Schema.SObjectType mySchema = schemaMap.get(obj);
            final Map <String, Schema.SObjectField> fieldMap = mySchema.getDescribe().fields.getMap();
            for(CCL_Dashboard_Details__mdt cs:orderFinalLst) {
                cs.Masterlabel=fieldMap.get(cs.CCL_Field_Api_Name__c).getDescribe().getLabel();

            }

        }
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw  ex;
    }
    return orderFinalLst;
}

    /********************************************************************************************************
*  @author          Deloitte
*  @description     To fetch 10 order records for Dashboard in community page
*  @param
*  @date            August 30, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<CCL_Order__c> getAllOrders(Integer offsetData,String accId,String conId) {
    List<CCL_Order__c> ordList=new List<CCL_Order__c>();
	Integer tempOffsetData=offsetData;
    if(tempOffsetData>0) {
        tempOffsetData-=10;
    }
     boolean whereApplied = false;
 boolean filtrApplied =false;
        String query = 'SELECT Id,Name,CCL_Returning_Patient__c,CCL_Order_Submission_Date_Text__c,CCL_Secondary_Therapy__c,CCL_Middle_Name__c,CCL_Last_Name__c,CCL_First_Name__c,CCL_Suffix__c,CCL_Therapy__c,CCL_Therapy__r.CCL_Type__c,toLabel(CCL_Delivery_Chevron__c),CCL_Estimated_FP_Delivery_Date__c,CCL_Allow_Replacement_Order__c,CCL_Logistic_Status__c,CCL_Additional_Information__c,toLabel(CCL_Exception_Status__c),CCL_Delivery_Chevron_Date__c,toLabel(CCL_Manufacturing_Chevron__c),CCL_Manufacturing_Chevron_Date__c,toLabel(CCL_Order_Chevron__c),CCL_Order_Chevron_Date__c,toLabel(CCL_Apheresis_Chevron__c),CCL_Apheresis_Chevron_Date__c,toLabel(CCL_Conditional_Status__c),CCL_Patient_Initials__c,CCL_Initials_COI__c,CCL_Month_of_Birth_COI__c,CCL_Day_of_Birth_COI__c,CCL_Year_of_Birth_COI__c,CCL_Date_of_DOB__c,CCL_Month_of_DOB__c,CCL_Year_of_DOB__c,CCL_Order_Approval_Eligibility__c,CCL_PRF_Ordering_Status__c,CCL_PRF_Submitter__c,CCL_Latest_Order_Submitted_By__c,CCL_Patient_Name_Text__c,CCL_Patient_Id__c, CCL_Secondary_COI_Patient_ID__c from CCL_Order__c ';
        if (!String.isEmpty(accId) || String.isNotEmpty(conId)) {
            query += ' WHERE ';
            whereApplied = true;
        }
        if (!String.isEmpty(accId)) {
            query += ' CCL_Ordering_Hospital__c = \''+String.escapeSingleQuotes(accId)+'\'';
			filtrApplied =true;
        }


            if (!String.isEmpty(conId)) {
			if(filtrApplied) {
                query += ' AND ';
				}


            query += '  CCL_Prescriber__c =  \''+String.escapeSingleQuotes(conId)+'\'';
 }
    if(!whereApplied) {
        query += ' WHERE ';
    } else {
        query +=' AND ';
    }
    query += ' CCL_Treatment_Status__c = \''+System.Label.CCL_Order_Active+'\'';

    query += ' AND ((CCL_Delivery_Chevron__c in (\''+System.Label.CCL_Order_Shipped+'\', \''+System.Label.CCL_Order_Delivered+'\',\''+System.Label.CCL_Order_Returned+'\') AND (CCL_Logistic_Status__c != null OR CCL_Estimated_FP_Delivery_Date__c >=LAST_N_DAYS:13)) OR (CCL_Delivery_Chevron__c NOT IN (\''+System.Label.CCL_Order_Shipped+'\', \''+System.Label.CCL_Order_Delivered+'\',\''+System.Label.CCL_Order_Returned+'\') AND (CCL_Estimated_FP_Delivery_Date__c >=LAST_N_DAYS:13 OR CCL_Allow_Replacement_Order__c=true)) )';

        query += ' order by CCL_Order_Submission_Date__c desc limit 10 offset '+tempOffsetData;


        if (CCL_Order__c.sObjectType.getDescribe().isAccessible()) {

               ordList = Database.query(query);
           }


    return ordList;

}
    /********************************************************************************************************
*  @author          Deloitte
*  @description     Fetch the total count based on filter criteria and show it on the community dashboard
*  @param
*  @date            September 02, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
    public static Integer getAllOrderCount(String accId  , String conId) {
        Integer intCount = 0;
        boolean whereApplied = false;
 boolean filtrApplied =false;
        String query = 'SELECT count() from CCL_Order__c ';
        if (!String.isEmpty(accId) || String.isNotEmpty(conId)) {
            query += ' WHERE ';
            whereApplied = true;
        }
        if (!String.isEmpty(accId)) {
            query += ' CCL_Ordering_Hospital__c = \''+accId+'\'';
			filtrApplied =true;
        }


            if (!String.isEmpty(conId)) {
			if(filtrApplied) {
                query += ' AND ';
				}


            query += '  CCL_Prescriber__c =  \''+conId+'\'';
 }
    if(!whereApplied) {
        query += ' WHERE ';
    } else {
        query +=' AND ';
    }
    query += ' CCL_Treatment_Status__c = \''+System.Label.CCL_Order_Active+'\'';

    query += ' AND ((CCL_Delivery_Chevron__c in (\''+System.Label.CCL_Order_Shipped+'\', \''+System.Label.CCL_Order_Delivered+'\',\''+System.Label.CCL_Order_Returned+'\') AND (CCL_Logistic_Status__c != null OR CCL_Estimated_FP_Delivery_Date__c >=LAST_N_DAYS:13)) OR (CCL_Delivery_Chevron__c NOT IN (\''+System.Label.CCL_Order_Shipped+'\', \''+System.Label.CCL_Order_Delivered+'\',\''+System.Label.CCL_Order_Returned+'\') AND (CCL_Estimated_FP_Delivery_Date__c >=LAST_N_DAYS:13 OR CCL_Allow_Replacement_Order__c=true)) )';

        return Database.countQuery(query);
    }

    /********************************************************************************************************
*  @author          Deloitte
*  @description     Search Account/Contact Data based on the name from community page
*  @param
*  @date            September 02, 2020
*********************************************************************************************************/
     @AuraEnabled(cacheable=true)
    public static String fetchAccountsFrmFilter (String searchKeyword,String sObjectName) {
        final String nameFilter =searchKeyword+'*';
        String returningQuery = sObjectName+' ( Id, Name)';
        String query = 'FIND :nameFilter IN NAME  FIELDS RETURNING '+String.escapeSingleQuotes(returningQuery)+' LIMIT 10';
        List<List<sObject>> sobjectList = Search.query(query);
        return JSON.serialize(sobjectList);
    }
	
   /********************************************************************************************************
*  @author         Deloitte
*  @description    This method is being used to fetch the order approval related details on page
*  @param
*  @date            November 20, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<CCL_Apheresis_Data_Form__c> getOrderApprovalInfo(Id recordId) {
    try {
        List<CCL_Apheresis_Data_Form__c> adfList=new List<CCL_Apheresis_Data_Form__c>();
        if (CCL_Apheresis_Data_Form__c.sObjectType.getDescribe().isAccessible()) {
            adfList=[select id,CCL_Land_on_Patient_Verification__c,CCL_Order__r.CCL_PRF_Ordering_Status__c,CCL_Order__r.CCL_Order_Approval_Eligibility__c,CCL_Order__r.CCL_Initial_PRF_Approved__c,CCL_Order__r.CCL_PRF_Approval_Counter__c from CCL_Apheresis_Data_Form__c where id=:recordId WITH SECURITY_ENFORCED ];
        }
        return adfList;
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw ex;
    }
}

	/********************************************************************************************************
*  @author          Deloitte
*  @description    ReturnsList View
*  @param
*  @date            September 02, 2020
*********************************************************************************************************/
    @AuraEnabled(cacheable=true)
public static List<ListView> fetchListViews() {
    List<ListView> listviews =new List<ListView>();
    try {

        if (CCL_Shipment__c.sObjectType.getDescribe().isAccessible()) {
  listviews =
        [SELECT Id, Name FROM ListView WHERE SobjectType = 'CCL_Shipment__c' and Name='Active Apheresis Shipments'];

        }
}
catch(AuraHandledException ex) {
    System.debug(ex.getMessage());
    throw  ex;
}
return listviews;
}

/********************************************************************************************************
*  @author          Deloitte
*  @description    ReturnsDocument Id
*  @param
*  @date            December 08, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<CCL_Document__c > getDocId(Id recordId) {
try {    
        String sObjName = recordId.getSObjectType().getDescribe().getName();
        String query = 'SELECT Id FROM CCL_Document__c WHERE ' + String.escapeSingleQuotes(sObjName)+ ' =:recordId';
        return Database.query(query);
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw ex;
    }
} 
    /********************************************************************************************************
*  @author         Deloitte
*  @description    This method is being used to fetch document object
*  @param
*  @date            November 20, 2020
*********************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<CCL_Document__c> getDocumentDetails(Id recordId) {
    try {
        set<Id> docId=new Set<Id>();
       List<CCL_Document__c> docList=new  List<CCL_Document__c>();
        for(ContentDocumentLink doc:[select LinkedEntityId,Id,ContentDocumentId from ContentDocumentLink where ContentDocumentId =:recordId]) {
            docId.add(doc.LinkedEntityId);
        }
        if(CCL_Document__c.SobjectType.getDescribe().isAccessible() && !docId.isEmpty()){
            docList=[select id,name,CCL_Apheresis_Data_Form__c,CCL_Order__c,CCL_Shipment__c from CCL_Document__c where id in: docId];
        }
      
        return docList;
    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
        throw ex;
    }
}
}

/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for spc_AssociationTriggerImplementation
    *  @date            06/18/2018
    *  @version         1.0
*********************************************************************************************************/
@isTest
public class spc_AssociationTriggerImplementationTest {

    @testSetup static void setup() {
        Territory terr1 = spc_Test_Setup.createTerritory('MSL', null, 'Yonoz');
        insert terr1;
        system.assertNotEquals(terr1, NULL);

        Territory terr2 = spc_Test_Setup.createTerritory('HEOR', terr1.id, 'Xonoz');
        insert terr2;
        system.assertNotEquals(terr2, NULL);

        UserRole usRole = new UserRole();
        usRole.name = 'MSL';
        Insert usRole;
        system.assertNotEquals(usRole, NULL);

        User oUser = spc_Test_Setup.getProfileID();
        UserRole oUserRole = [select id from UserRole where name = 'MSL'];
        oUser.userRoleId = oUserRole.id;
        insert oUser;
        system.assertNotEquals(oUser, NULL);
    }

    public static testmethod void testExecution() {

        dataTest();
        spc_ZipToTerrMapper zipTerrMapping = new spc_ZipToTerrMapper((new Set<String> {spc_ApexConstants.MARKET_ACCESS_REP}));
        Map<String, Map<String, String>> mapUsers = new Map<String, Map<String, String>>();
        Set<String> accountIds = new set<string>();
        List<PatientConnect__PC_Association__c> lstAssoc = new List<PatientConnect__PC_Association__c>();
        PatientConnect__PC_Association__c oAsso = [select id, PatientConnect__PC_Role__c, PatientConnect__PC_Program__c, PatientConnect__PC_Account__c, PatientConnect__PC_Account__r.id, PatientConnect__PC_AssociationStatus__c from PatientConnect__PC_Association__c LIMIT 1];
        lstAssoc.add(oAsso);
        accountIds.add(oAsso.PatientConnect__PC_Account__r.id);
        system.assertNotEquals(oAsso, null);
        test.startTest();
        spc_AssociationTriggerImplementation.populateMarketAccessOnCase(lstAssoc);
        mapUsers = zipTerrMapping.getUsers(accountIds);
        test.stoptest();
    }
    public static void dataTest() {
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.spc_HIPAA_Consent_Received__c = 'YES';
        insert account;
        system.assertNotEquals(account, NULL);

        Account payerAccount = spc_Test_Setup.createTestAccount('Payer');
        payerAccount.spc_HIPAA_Consent_Received__c = 'YES';
        insert payerAccount;
        Contact con = new Contact();
        con.lastName = 'testContact';
        con.Accountid = payerAccount.id;
        insert con;
        system.assertNotEquals(con, NULL);

        PatientConnect__PC_Address__c addr = spc_Test_Setup.createTestAddress(payerAccount);
        addr.PatientConnect__PC_Primary_Address__c = true;
        addr.PatientConnect__PC_Zip_Code__c = '000000';
        insert addr;
        system.assertNotEquals(addr, NULL);

        Zip_to_Terr_vod__c zipToTerr = new Zip_to_Terr_vod__c();
        zipToTerr.Territory_vod__c = 'HEOR';
        zipToTerr.Zip_ID_vod__c = '000000';
        zipToTerr.Name = '000000';
        insert zipToTerr;
        system.assertNotEquals(zipToTerr, NULL);

        Case programCase = spc_Test_Setup.newCase(payerAccount.id, 'PC_Program');
        insert programCase;
        PatientConnect__PC_Association__c oAsso = spc_Test_Setup.createAssociation(account, programCase, 'Referring Physician', 'Active');
        insert oAsso;
        system.assertNotEquals(oAsso, NULL);

        PatientConnect__PC_Address__c addr2 = spc_Test_Setup.createTestAddress(account);
        addr2.PatientConnect__PC_Primary_Address__c = true;
        addr2.PatientConnect__PC_Zip_Code__c = '000000';
        insert addr2;

        PatientConnect__PC_Association__c oAsso2 = spc_Test_Setup.createAssociation(account, programCase, 'Treating Physician', 'InActive');
        insert oAsso2;

        Map<Id, PatientConnect__PC_Association__c> mapProgIdWithAssociations = new Map<Id, PatientConnect__PC_Association__c>();
        mapProgIdWithAssociations.put(programCase.id, oAsso);
        spc_AssociationTriggerImplementation.validateAssociationRoleAndStatus(mapProgIdWithAssociations, false);
        List<PatientConnect__PC_Association__c> lstAssociations = new List<PatientConnect__PC_Association__c>();
        lstAssociations.add(oAsso);
        spc_AssociationTriggerImplementation.populateMarketAccessOnCase(lstAssociations);
        spc_AssociationTriggerImplementation ati = new spc_AssociationTriggerImplementation();
        Account account2 = spc_Test_Setup.createTestAccount('PC_Physician');
        account2.spc_HIPAA_Consent_Received__c = 'NO';
        account2.spc_REMS_Certification_Status__c = 'Certified';
        account2.spc_REMS_Certification_Status_Date__c = System.Today();
        insert account2;

        Case programCase1 = spc_Test_Setup.newCase(account2.id, 'PC_Program');
        programCase1.PatientConnect__PC_Status_Indicator_3__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.PATIENT_STATUS_INDICATOR_COMPLETE);

        insert programCase1;

        Task tn = spc_Test_Setup.createTask('Completed', programCase.id, 'High', 'test sub', 'FAX', Date.Today(),
                                            'Welcome Call Lite', 'First Attempt', 'Case Manager', 'Reached', 'Outbound', 'Test desc');
        tn.Type = 'Other';
        insert tn;

        Map<Id, Id> accountToCaseMap = new Map<Id, Id>();
        account2.spc_HIPAA_Consent_Received__c = '';
        update account2;
        accountToCaseMap.put(account2.id, programCase1.id);
        ati.createHCPLogisticTasks(accountToCaseMap);
        List<PatientConnect__PC_Association__c> lstSOCAssociations = new List<PatientConnect__PC_Association__c>();
        lstSOCAssociations.add(oAsso);

        Account accCar2 = spc_Test_Setup.createTestAccount('HCO');
        accCar2.PatientConnect__PC_Caregiver_Relationship_to_Patient__c = 'Self';
        accCar2.spc_Permission_to_Email__c = 'Yes';
        accCar2.spc_Date_Permission_to_Share_PHI__c = System.Today();
        insert accCar2;

        PatientConnect__PC_Interaction__c intr = spc_Test_Setup.createInteraction(programCase.id);
        intr.PatientConnect__PC_Participant__c = accCar2.id;
        insert intr;

        ati.validateAndUpdateInteractionsWithSOC(lstSOCAssociations);
        ati.validateActiveAssociations(lstSOCAssociations);
        ati.updateInteractionsWithInactiveSOC(lstSOCAssociations);
    }


    public static testmethod void testExecution2() {
        test.starttest();
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        Account account2 = spc_Test_Setup.createTestAccount('PC_Physician');
        account2.spc_HIPAA_Consent_Received__c = 'YES';
        insert account2;
        system.assertNotEquals(account2, NULL);

        Account accCar2 = spc_Test_Setup.createTestAccount('HCO');
        accCar2.PatientConnect__PC_Caregiver_Relationship_to_Patient__c = 'Self';
        accCar2.spc_Permission_to_Email__c = 'Yes';
        accCar2.spc_Date_Permission_to_Share_PHI__c = System.Today();
        insert accCar2;

        Case programCase2 = spc_Test_Setup.newCase(accCar2.id, 'PC_Program');
        insert programCase2;

        PatientConnect__PC_Association__c oAsso2 = spc_Test_Setup.createAssociation(accCar2, programCase2, 'HCO', 'Active');
        insert oAsso2;
        system.assertNotEquals(oAsso2, NULL);

        List<PatientConnect__PC_Association__c> lstSOCAssociations = new List<PatientConnect__PC_Association__c>();
        lstSOCAssociations.add(oAsso2);

        PatientConnect__PC_Interaction__c intr = spc_Test_Setup.createInteraction(programCase2.id);
        intr.PatientConnect__PC_Participant__c = accCar2.id;
        insert intr;

        spc_AssociationTriggerImplementation  ati = new spc_AssociationTriggerImplementation();
        ati.validateAndUpdateInteractionsWithSOC(lstSOCAssociations);
        test.stoptest();
    }

}
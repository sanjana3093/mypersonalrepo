/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for Account Trigger Handler
    *  @date            07/18/2018
    *  @version         1.0
*********************************************************************************************************/
@IsTest
public class spc_AccountTriggerHandlerTest {

    @testSetup static void setupTestdata() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
    }

    @IsTest static void testProgramStatusIndicatorComplete() {
        Test.startTest();
        //Creation of Physician Account
        Account physicianAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Physician').getRecordTypeId());
        if (null != physicianAcc) {
            physicianAcc.spc_HIPAA_Consent_Received__c = 'Yes';
            physicianAcc.spc_Patient_Services_Consent_Received__c = 'Yes';
            physicianAcc.spc_Text_Consent__c = 'Yes';
            physicianAcc.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
            physicianAcc.PatientConnect__PC_External_ID__c = '1234';
            insert physicianAcc;
            Case programCaseRec = spc_Test_Setup.createCases(new List<Account> {physicianAcc}, 1, 'PC_Program').get(0);
            if (null != programCaseRec) {
                programCaseRec.Type = 'Program';
                programCaseRec.PatientConnect__PC_Status_Indicator_1__c = '';
                insert programCaseRec;
            }
            physicianAcc.External_ID_vod__c = '1234';
            physicianAcc.spc_Patient_Mrkt_and_Srvc_consent__c = 'No';
            physicianAcc.spc_HIPAA_Consent_Received__c = 'No';
            physicianAcc.spc_Patient_Services_Consent_Received__c = 'No';
            physicianAcc.spc_Text_Consent__c = 'No';
            update physicianAcc;
        }
        Test.stopTest();
    }

    @IsTest static void testProgramStatusIndicatorNone() {
        Test.startTest();
        //Creation of Physician Account
        Account physicianAcc2 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        if (null != physicianAcc2) {
            physicianAcc2.spc_HIPAA_Consent_Received__c = 'No';
            physicianAcc2.spc_Patient_HIPAA_Consent_Date__c = System.Today();
            physicianAcc2.spc_Text_Consent__c = 'No';
            physicianAcc2.spc_Patient_Text_Consent_Date__c  = System.Today();
            physicianAcc2.PatientConnect__PC_External_ID__c = '1234';
            physicianAcc2.spc_Patient_Services_Consent_Received__c = 'No';
            physicianAcc2.spc_Patient_Service_Consent_Date__c = System.Today();
            physicianAcc2.spc_Patient_Mrkt_and_Srvc_consent__c = 'No';
            physicianAcc2.spc_Patient_Marketing_Consent_Date__c = System.Today();
            insert physicianAcc2;
            Case programCaseRec2 = spc_Test_Setup.createCases(new List<Account> {physicianAcc2}, 1, 'PC_Program').get(0);
            if (null != programCaseRec2) {
                programCaseRec2.Type = 'Program';
                programCaseRec2.PatientConnect__PC_Status_Indicator_1__c = '';
                insert programCaseRec2;
            }
            physicianAcc2.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
            physicianAcc2.spc_Patient_Marketing_Consent_Date__c = system.today() - 360;
            update physicianAcc2;
        }
        Test.stopTest();
    }

    @IsTest static void testProgramStatusIndicatorWIP() {
        Test.startTest();
        //Creation of Physician Account
        Account physicianAcc2 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Physician').getRecordTypeId());
        if (null != physicianAcc2) {
            physicianAcc2.spc_HIPAA_Consent_Received__c = 'No';
            physicianAcc2.spc_Patient_Services_Consent_Received__c = 'No';
            physicianAcc2.spc_Text_Consent__c = 'No';
            physicianAcc2.PatientConnect__PC_External_ID__c = '1234';
            insert physicianAcc2;
            Case programCaseRec2 = spc_Test_Setup.createCases(new List<Account> {physicianAcc2}, 1, 'PC_Program').get(0);
            if (null != programCaseRec2) {
                programCaseRec2.Type = 'Program';
                programCaseRec2.PatientConnect__PC_Status_Indicator_1__c = '';
                insert programCaseRec2;
            }
            physicianAcc2.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
            update physicianAcc2;
        }
        Test.stopTest();
    }
    public static testmethod void testExecution() {
        Account patientAcc2 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        patientAcc2.PatientConnect__PC_Date_of_Birth__c = system.today();
        insert patientAcc2;
    }
    public static testmethod void testExecution1() {
        Account patientAcc2 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        patientAcc2.PatientConnect__PC_Date_of_Birth__c = null;
        insert patientAcc2;
        patientAcc2.PatientConnect__PC_Date_of_Birth__c = system.today() - 360;
        update patientAcc2;
    }

    @IsTest static void testCaregiverAccount() {
        Account patientAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId());
        patientAcc.PatientConnect__PC_Date_of_Birth__c = system.today();
        patientAcc.spc_Permission_from_Patient_to_Share_PHI__c = Label.spc_No ;
        insert patientAcc;
        patientAcc.spc_Permission_from_Patient_to_Share_PHI__c = Label.spc_Yes;
        patientAcc.spc_Date_Permission_to_Share_PHI__c = system.today();
        Case objCase = new Case();
        objCase.Type = 'Program';
        objCase.Status = 'Enrolled';
        insert objCase;     PatientConnect__PC_Association__c association = spc_Test_Setup.createAssociation(patientAcc, objCase, 'Caregiver', system.today() + 1);
        update patientAcc;
    }

    @IsTest static void testDesignatedCaregiverAccount() {
        Account patientAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId());
        patientAcc.PatientConnect__PC_Date_of_Birth__c = system.today();
        patientAcc.spc_Permission_from_Patient_to_Share_PHI__c = Label.spc_Yes ;
        patientAcc.spc_Date_Permission_to_Share_PHI__c = system.today();
        insert patientAcc;
        Account patientAcc2 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        insert patientAcc2;
        Case objCase = new Case();
        objCase.Type = 'Program';
        objCase.Status = 'Enrolled';
        insert objCase;
        PatientConnect__PC_Association__c association = spc_Test_Setup.createAssociation(patientAcc, objCase, spc_ApexConstants.ASSOCIATION_ROLE_DESIGNATED_CAREGIVER, system.today() + 1);
        patientAcc.spc_Permission_from_Patient_to_Share_PHI__c = Label.spc_No;
        update patientAcc;

    }

    @IsTest static void testNotCertfiedHCOAccounts() {
        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
        hcoAcc.spc_REMS_Certification_Status__c = 'Certified';
        hcoAcc.spc_REMS_Certification_Status_Date__c = System.today();
        insert hcoAcc;

        Account manufacturerAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId());
        insert manufacturerAcc;
        PatientConnect__PC_Engagement_Program__c engProgram = new PatientConnect__PC_Engagement_Program__c (Name = 'Brexanolone Engagement Program', PatientConnect__PC_Manufacturer__c = manufacturerAcc.Id, PatientConnect__PC_Program_Code__c = 'BREX');
        insert engProgram;
        Case objCase = new Case();
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId();
        objCase.PatientConnect__PC_Engagement_Program__c = engProgram.Id;
        objCase.Status = 'Enrolled';
        insert objCase;

        PatientConnect__PC_Association__c association = spc_Test_Setup.createAssociation(hcoAcc, objCase, 'HCO', system.today() + 1);

        Test.startTest();
        hcoAcc.spc_REMS_Certification_Status__c = 'Not Certified';
        update hcoAcc;
        Test.stopTest();

    }
}

/*********************************************************************************

    *  @author          Deloitte
    *  @description     this test class will contain generic methods that will be used across the Application.
    *  @date            4-May-2018
    *  @version         1.0
*********************************************************************************/
@isTest
public class spc_SYSTEMTest {
 @testSetup static void setup() {
        
        // create a record for the custom setting
        
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
    }
     @isTest
    static void getRecords() {
        Case inquiryCase  = new Case();
        String recordtypename;
        
        test.startTest();
        inquiryCase.RecordTypeId = spc_System.recordTypeId('Case', 'spc_Inquiry');
        spc_System.recordTypeId('', 'spc_Inquiry');
        recordtypename=spc_System.recordTypeName('Case', 'PC_PAP');
        recordtypename=spc_System.recordTypeName('Case', 'PC_PAP');
        recordtypename=spc_System.recordTypeName('', 'PC_PAP');
        recordtypename=spc_System.getRecordTypeName('Account', 'Patient');
         recordtypename=spc_System.getRecordTypeName('', 'Patient');
        Id accountRTypeId=spc_System.getRecordTypeIdByName('Account', 'Patient');
         accountRTypeId=spc_System.getRecordTypeIdByName('', '');
        List<RecordType> records=spc_System.getAllRecordType('Account');
        test.stopTest();
    }
}
/**
* @author Deloitte
* @date 06/08/2018
*
* @description This is the test class for spc_CustLookupCntrl
*/

@isTest(SeeAllData=False)
public class spc_CustLookupCntrlTest {
    @isTest 
    public static void createTestData(){
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        Account pAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        pAcc.PatientConnect__PC_Email__c = 'test@email.com';
        insert pAcc;
        
        Test.startTest();
        
        List<sObject> result = spc_CustLookupCntrl.fetchLookUpValues('T', 'Account','','');
        List<sObject> result1 = spc_CustLookupCntrl.fetchLookUpValues('', 'Account','','');
        List<sObject> result2 = spc_CustLookupCntrl.fetchLookUpValues('T', 'Account','','PatientConnect__PC_Patient');
        System.assert(!result.isEmpty());
        Test.stopTest();
        
    }
}
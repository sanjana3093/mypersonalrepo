global without sharing class CCL_DocumentDeleteBatch_Scheduler Implements Schedulable

            {
                        global void execute(SchedulableContext sc)

                        {
                            CCL_DocumentDeleteBatch b = new CCL_DocumentDeleteBatch();
                            database.executebatch(b);

                        }

            }
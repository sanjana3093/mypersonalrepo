/********************************************************************************************************
*  @author          Deloitte
*  @date            09/08/2018
*  @description   spc_zPaperDataTest.cls
*  @version         1.0
*********************************************************************************************************/
@isTest
private class spc_zPaperDataTest {
      static void setup() {
            List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
      }
      private static List<Case> lstCase = new List<Case>();
      private static void setupTestdata() {
            List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();

            //List of program coverage to insert
            List<PatientConnect__PC_Program_Coverage__c> pclst = new List<PatientConnect__PC_Program_Coverage__c>();

            //Health plan List
            List<PatientConnect__PC_Health_Plan__c> hplst = new List<PatientConnect__PC_Health_Plan__c>();

            //case List
            List<case> caselst = new List<case>();

            Account acc = spc_Test_Setup.createPatient('New Patient');
            acc.spc_HIPAA_Consent_Received__c = 'Yes';
            acc.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
            acc.spc_Patient_Services_Consent_Received__c = 'Yes';
            acc.spc_Text_Consent__c = 'Yes';
            acc.spc_Patient_Marketing_Consent_Date__c = Date.valueOf('2014-06-07');
            acc.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf('2014-06-07');
            acc.spc_Patient_Text_Consent_Date__c = Date.valueOf('2014-06-07');
            acc.spc_Patient_Service_Consent_Date__c = Date.valueOf('2014-06-07');
            spc_Database.ins(acc);

            Account phyAcc = spc_Test_Setup.createTestAccount('PC_Physician');
            phyAcc.Name = 'Acc_Name';
            spc_Database.ins(phyAcc);

            Account phyAcc1 = spc_Test_Setup.createTestAccount('PC_Physician');
            phyAcc1.Name = 'Acc_Name1';
            spc_Database.ins(phyAcc1);

            spc_Database.ins(apexConstants);
            Account patientAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());

            Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
            spc_Database.ins(manAcc);

            //PharmacyAcc
            Account pharmacyAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PC_Pharmacy').getRecordTypeId());
            insert pharmacyAcc;
            Account pharmacyAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PC_Pharmacy').getRecordTypeId());
            pharmacyAcc1.PatientConnect__PC_Date_of_Birth__c = system.today() - 20;
            pharmacyAcc1.Name = 'Test Pharmacy Account 1';
            insert pharmacyAcc1;
            Account pharmacyAcc2 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PC_Pharmacy').getRecordTypeId());
            pharmacyAcc2.PatientConnect__PC_Date_of_Birth__c = system.today() - 20;
            pharmacyAcc2.Name = 'Test Pharmacy Account 2';
            insert pharmacyAcc2;
            Account ExtpharmacyAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PC_Pharmacy').getRecordTypeId());
            ExtpharmacyAcc.PatientConnect__PC_Date_of_Birth__c = System.today() - 50;
            ExtpharmacyAcc.Name = 'test phy ecp account';
            insert ExtpharmacyAcc;
            Contact con = new Contact();
            con.accountid = pharmacyAcc.id;
            con.lastName = 'test Con';
            insert con;

            //hco account
            Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
            insert hcoAcc;

            PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('Engagement Program SageRx', manAcc.Id);
            engPrgm.PatientConnect__PC_Program_Code__c = 'BREX';
            insert engPrgm;

            Case objCase = spc_Test_Setup.newCase(acc.Id, 'PC_Program');
            objCase.Status = 'Enrolled';
            objCase.PatientConnect__PC_Engagement_Program__c = engPrgm.Id;
            objCase.PatientConnect__PC_Status_Indicator_5__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.PATIENT_STATUS_INDICATOR_COMPLETE);
            objCase.PatientConnect__PC_Physician__c = phyAcc.Id;
            insert objCase;

            lstCase.add(objCase);

            //Health plan
            Id HP_RT = Schema.SObjectType.PatientConnect__PC_Health_Plan__c.getRecordTypeInfosByName().get('Pharmacy Plan').getRecordTypeId();

            PatientConnect__PC_Health_Plan__c hp1 = new PatientConnect__PC_Health_Plan__c();
            hp1.recordtypeid = HP_RT;
            hp1.PatientConnect__PC_Plan_Status__c =  spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE;
            hp1.PatientConnect__PC_Plan_Type__c = 'Primary';
            hp1.PatientConnect__Patient__c = acc.id;
            hplst.add(hp1);

            PatientConnect__PC_Health_Plan__c hp2 = new PatientConnect__PC_Health_Plan__c();
            hp2.recordtypeid = HP_RT;
            hp2.PatientConnect__PC_Plan_Status__c =  spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE;
            hp2.PatientConnect__PC_Plan_Type__c = 'Secondary';
            hp2.PatientConnect__Patient__c = acc.id;
            hplst.add(hp2);

            PatientConnect__PC_Health_Plan__c hp3 = new PatientConnect__PC_Health_Plan__c();
            hp3.recordtypeid = HP_RT;
            hp3.PatientConnect__PC_Plan_Status__c =  spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE;
            hp3.PatientConnect__PC_Plan_Type__c = 'Tertiary';
            hp3.PatientConnect__Patient__c = acc.id;
            hplst.add(hp3);

            insert hplst;
            List<PatientConnect__PC_Association__c> AssocList = new List<PatientConnect__PC_Association__c>();
            PatientConnect__PC_Association__c oAsso = new PatientConnect__PC_Association__c();
            oAsso.PatientConnect__PC_Role__c = 'HCO';
            oAsso.PatientConnect__PC_Program__c = objCase.id;
            oAsso.PatientConnect__PC_Account__c = hcoAcc.id;
            oAsso.PatientConnect__PC_AssociationStatus__c = 'active';
            AssocList.add(oAsso);
            PatientConnect__PC_Association__c oAssohco = new PatientConnect__PC_Association__c();
            oAssohco.PatientConnect__PC_Role__c = 'HCO';
            oAssohco.PatientConnect__PC_Program__c = objCase.id;
            oAssohco.PatientConnect__PC_Account__c = hcoAcc.id;
            oAssohco.PatientConnect__PC_AssociationStatus__c = 'Inactive';
            AssocList.add(oAssohco);

            Account caregiverAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId());
            caregiverAcc.PatientConnect__PC_Date_of_Birth__c = system.today();
            caregiverAcc.spc_Permission_from_Patient_to_Share_PHI__c = Label.spc_No ;
            insert caregiverAcc;

            PatientConnect__PC_Association__c oAsso1 = new PatientConnect__PC_Association__c();
            oAsso1.PatientConnect__PC_Role__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_DESIGNATED_CAREGIVER );
            oAsso1.PatientConnect__PC_Program__c = objCase.id;
            oAsso1.PatientConnect__PC_Account__c = caregiverAcc.id;
            oAsso1.PatientConnect__PC_AssociationStatus__c = 'active';
            AssocList.add(oAsso1);
            PatientConnect__PC_Association__c oAsso11 = new PatientConnect__PC_Association__c();
            oAsso11.PatientConnect__PC_Role__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_DESIGNATED_CAREGIVER );
            oAsso11.PatientConnect__PC_Program__c = objCase.id;
            oAsso11.PatientConnect__PC_Account__c = caregiverAcc.id;
            oAsso11.PatientConnect__PC_AssociationStatus__c = 'inactive';
            AssocList.add(oAsso11);

            PatientConnect__PC_Association__c oAsso3 = new PatientConnect__PC_Association__c();
            oAsso3.PatientConnect__PC_Role__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_SPECIALTY_PHARMACY);
            oAsso3.PatientConnect__PC_Program__c = objCase.id;
            oAsso3.PatientConnect__PC_Account__c = pharmacyAcc.id;
            oAsso3.PatientConnect__PC_AssociationStatus__c = 'active';
            AssocList.add(oAsso3);
            PatientConnect__PC_Association__c oAssoecp = new PatientConnect__PC_Association__c();
            oAssoecp.PatientConnect__PC_Role__c = 'External Pharmacy';
            oAssoecp.PatientConnect__PC_Program__c = objCase.id;
            oAssoecp.PatientConnect__PC_Account__c = ExtpharmacyAcc.id;
            oAssoecp.PatientConnect__PC_AssociationStatus__c = spc_ApexConstants.ASSOCIATION_STATUS_INACTIVE;
            AssocList.add(oAssoecp);

            PatientConnect__PC_Association__c oAsso4 = new PatientConnect__PC_Association__c();
            oAsso4.PatientConnect__PC_Role__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_SPECIALTY_PHARMACY);
            oAsso4.PatientConnect__PC_Program__c = objCase.id;
            oAsso4.PatientConnect__PC_Account__c = pharmacyAcc.id;
            oAsso4.PatientConnect__PC_AssociationStatus__c = 'Inactive';
            AssocList.add(oAsso4);


            PatientConnect__PC_Association__c oAsso5 = new PatientConnect__PC_Association__c();
            oAsso5.PatientConnect__PC_Role__c = 'External Pharmacy';
            oAsso5.PatientConnect__PC_Program__c = objCase.id;
            oAsso5.PatientConnect__PC_Account__c = pharmacyAcc1.id;
            oAsso5.PatientConnect__PC_AssociationStatus__c = 'active';
            AssocList.add(oAsso5);


            PatientConnect__PC_Association__c oAsso6 = new PatientConnect__PC_Association__c();
            oAsso6.PatientConnect__PC_Role__c = 'External Pharmacy';
            oAsso6.PatientConnect__PC_Program__c = objCase.id;
            oAsso6.PatientConnect__PC_Account__c = pharmacyAcc1.id;
            oAsso6.PatientConnect__PC_AssociationStatus__c = 'Inactive';
            AssocList.add(oAsso6);

            PatientConnect__PC_Association__c oAsso8 = new PatientConnect__PC_Association__c();
            oAsso8.PatientConnect__PC_Role__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_TREATING_PHYSICIAN ) ;
            oAsso8.PatientConnect__PC_Program__c = objCase.id;
            oAsso8.PatientConnect__PC_Account__c = phyAcc.id;
            oAsso8.PatientConnect__PC_AssociationStatus__c = 'Inactive';
            AssocList.add(oAsso8);

            PatientConnect__PC_Association__c oAsso9 = new PatientConnect__PC_Association__c();
            oAsso9.PatientConnect__PC_Role__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_REFERRING_PHYSICIAN ) ;
            oAsso9.PatientConnect__PC_Program__c = objCase.id;
            oAsso9.PatientConnect__PC_Account__c = phyAcc1.id;
            oAsso9.PatientConnect__PC_AssociationStatus__c = 'Active';
            AssocList.add(oAsso9);

            PatientConnect__PC_Association__c oAsso10 = new PatientConnect__PC_Association__c();
            oAsso10.PatientConnect__PC_Role__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_REFERRING_PHYSICIAN ) ;
            oAsso10.PatientConnect__PC_Program__c = objCase.id;
            oAsso10.PatientConnect__PC_Account__c = phyAcc1.id;
            oAsso10.PatientConnect__PC_AssociationStatus__c = 'Inactive';
            AssocList.add(oAsso10);

            PatientConnect__PC_Association__c oAsso12 = new PatientConnect__PC_Association__c();
            oAsso12.PatientConnect__PC_Role__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.FREE_DRUG_VENDOR );
            oAsso12.PatientConnect__PC_Program__c = objCase.id;
            oAsso12.PatientConnect__PC_Account__c = pharmacyAcc2.id;
            oAsso12.PatientConnect__PC_AssociationStatus__c = 'Active';
            AssocList.add(oAsso12);

            PatientConnect__PC_Association__c oAsso13 = new PatientConnect__PC_Association__c();
            oAsso13.PatientConnect__PC_Role__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.FREE_DRUG_VENDOR );
            oAsso13.PatientConnect__PC_Program__c = objCase.id;
            oAsso13.PatientConnect__PC_Account__c = pharmacyAcc2.id;
            oAsso13.PatientConnect__PC_AssociationStatus__c = 'Inactive';
            AssocList.add(oAsso13);

            insert AssocList;
            //Program coverage
            PatientConnect__PC_Program_Coverage__c progCoverageRec1 = spc_Test_Setup.newProgCoverage(objCase.id, 'PC_PAP', 'Uninsured');
            progCoverageRec1.PatientConnect__PC_Health_Plan__c = hp1.id;
            progCoverageRec1.PatientConnect__PC_Coverage_Status__c = 'Active';
            progCoverageRec1.spc_Not_Eligible__c = true;
            pclst.add(progCoverageRec1);

            PatientConnect__PC_Program_Coverage__c progCoverageRec2 = spc_Test_Setup.newProgCoverage(objCase.id, 'PC_PAP', 'Uninsured');
            progCoverageRec2.PatientConnect__PC_Health_Plan__c = hp2.id;
            progCoverageRec2.PatientConnect__PC_Coverage_Status__c = 'Active';
            progCoverageRec2.PatientConnect__PC_Coverage_Type__c = 'PAP Coverage';
            progCoverageRec2.spc_Not_Eligible__c = true;
            pclst.add(progCoverageRec2);

            PatientConnect__PC_Program_Coverage__c progCoverageRec3 = spc_Test_Setup.newProgCoverage(objCase.id, 'PC_Copay_Coverage', 'Covered');
            progCoverageRec3.PatientConnect__PC_Health_Plan__c = hp3.id;
            progCoverageRec3.PatientConnect__PC_Coverage_Status__c = 'Active';
            progCoverageRec3.PatientConnect__PC_Coverage_Type__c = 'Drug Copay';
            progCoverageRec3.spc_Not_Eligible__c = true;

            PatientConnect__PC_Program_Coverage__c progCoverageRec4 = spc_Test_Setup.newProgCoverage(objCase.id, 'PC_Copay_Coverage', 'Covered');
            progCoverageRec4.PatientConnect__PC_Health_Plan__c = hp3.id;
            progCoverageRec4.PatientConnect__PC_Coverage_Status__c = 'Active';
            progCoverageRec4.PatientConnect__PC_Coverage_Type__c = 'Admin Copay';
            progCoverageRec4.spc_Not_Eligible__c = true;
            pclst.add(progCoverageRec4);

            insert pclst;

            //Primary Case
            Case priCase = spc_Test_Setup.newCase(acc.Id, 'PC_Prior_Authorization');
            priCase.Status = 'Closed';
            priCase.PatientConnect__PC_Program__c = objCase.id;
            priCase.PatientConnect__PC_Program_Coverage__c = progCoverageRec1.id;
            priCase.PatientConnect__PC_Case_Outcome__c = 'Approved';
            priCase.spc_PartnerCaseID__c='1';
            caselst.add(pricase);

            //Secondary Case
            Case secCase = spc_Test_Setup.newCase(acc.Id, 'PC_Prior_Authorization');
            secCase.Status = 'Closed';
            secCase.PatientConnect__PC_Program__c = objCase.id;
            secCase.PatientConnect__PC_Program_Coverage__c = progCoverageRec2.id;
            secCase.PatientConnect__PC_Case_Outcome__c = 'Approved';
            secCase.spc_PartnerCaseID__c='1';
            caselst.add(secCase);

            //Tertiary Case
            Case terCase = spc_Test_Setup.newCase(acc.Id, 'PC_Prior_Authorization');
            terCase.Status = 'Closed';
            terCase.PatientConnect__PC_Program__c = objCase.id;
            terCase.PatientConnect__PC_Program_Coverage__c = progCoverageRec3.id;
            terCase.PatientConnect__PC_Case_Outcome__c = 'Approved';
            terCase.spc_PartnerCaseID__c='1';
            caselst.add(terCase);

            insert caselst;

            //interaction
            PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
            interaction = spc_Test_Setup.createInteraction(objCase.id);
            interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
            interaction.PatientConnect__PC_Status__c = 'Complete';
            interaction.spc_SOC_Type__c = 'Home';
            interaction.spc_HIP__c = 'Other';
            insert interaction;

      }

      private static testMethod void testzPaper() {
            setupTestdata();

            Test.StartTest();
            spc_zPaperData zp = new spc_zPaperData(lstCase);
            System.assertNotEquals(zp.createMap(lstCase), null);

            Test.StopTest();
      }
}
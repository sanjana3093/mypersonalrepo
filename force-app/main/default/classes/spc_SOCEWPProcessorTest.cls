/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the test class for PrescriptionSiteOfCareProcessor
*  @date            06/18/2018
*  @version         1.0
*
*  Modification Log:
* -------------------------------------------------------------------------------------------------------
*  Developer                Date                                    Description
* -------------------------------------------------------------------------------------------------------
*  soummohapatra            06/18/2018                              Initial version
*********************************************************************************************************/
@isTest
public class spc_SOCEWPProcessorTest {
    public static Id ID_ENROLLMENTCASE_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Enrollment').getRecordTypeId();
    public static Id ID_PROGRAMCASE_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId();
    public static Id ID_HCOACCOUNT_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
    @testSetup static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

        Account account = spc_Test_Setup.createTestAccount('PC_Patient');
        account.PatientConnect__PC_Email__c = 'test@email.com';

        insert account;
        system.assertNotEquals(account, NULL);
        Account HCOAccount = spc_Test_Setup.createTestAccount('HCO');
        insert HCOAccount;
        system.assertNotEquals(HCOAccount, NULL);
        Case enrollmentCase = spc_Test_Setup.newCase(account.id, 'PC_Enrollment');
        insert enrollmentCase;
        system.assertNotEquals(enrollmentCase, NULL);
        Account   manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        manAcc.PatientConnect__PC_Email__c = 'testyyygrryyy@test.com';
        manAcc.PatientConnect__PC_Date_of_Birth__c = System.today() - 5;
        spc_Database.ins(manAcc);
        PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('Engagement Program SageRx', manAcc.Id);
        engPrgm.PatientConnect__PC_Program_Code__c = 'SAGE';
        insert engPrgm;
        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');
        programCase.PatientConnect__PC_Engagement_Program__c = engPrgm.Id;
        insert programCase;
        enrollmentCase.PatientConnect__PC_Program__c = programCase.id;

    }
    /*********************************************************************************
    Method Name    : testGetPicklistEntryMap
    Description    : Test pocklist entry map
    Return Type    : void
    Parameter      : None
    *********************************************************************************/
    @isTest
    public static void testGetPicklistEntryMap() {
        Test.startTest();
        Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> pickLists = spc_SOCEWPController.getPicklistEntryMap();
        System.assertEquals(True, pickLists.size() > 0);
        Test.stopTest();

    }
    /*********************************************************************************
    Method Name    : testProcessEnrollment
    Description    : Test process enrollment
    Return Type    : void
    Parameter      : None
    *********************************************************************************/
    @isTest
    public static void testProcessEnrollment() {
        test.startTest();
        Account HCOAccount = [select id from Account where recordtypeid = :ID_HCOACCOUNT_RECORDTYPE LIMIT 1];

        Case enrollmentCase = [select id from Case where recordtypeid = :ID_ENROLLMENTCASE_RECORDTYPE LIMIT 1];
        Account account = spc_Test_Setup.createTestAccount('PC_Patient');
        account.spc_HIPAA_Consent_Received__c = 'Yes';
        account.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf(Label.spc_test_birth_date);
        account.PatientConnect__PC_Email__c = 'testyyyyyyy@test.com';
        account.PatientConnect__PC_Date_of_Birth__c = Date.valueOf(Label.spc_test_birth_date);
        insert account;

        // update account;
        Account   manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        manAcc.PatientConnect__PC_Email__c = 'testgreee@test.com';
        manAcc.PatientConnect__PC_Date_of_Birth__c = System.today() - 10;
        spc_Database.ins(manAcc);
        PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('Engagement Program SRx', manAcc.Id);
        engPrgm.PatientConnect__PC_Program_Code__c = 'SAGET';
        insert engPrgm;
        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');
        programCase.PatientConnect__PC_Engagement_Program__c = engPrgm.Id;
        insert programCase;
        PatientConnect__PC_Address__c testaddress = spc_Test_Setup.createTestAddress(HCOAccount);
        insert testaddress;

        enrollmentCase.PatientConnect__PC_Program__c = programCase.id;
        Map<String, Object> pageState = new Map<String, Object>();
        Map<String, Object> persistedAccountMap = new Map<String, Object>();
        Map<String, Object> persistentHCOAccount = new Map<String, Object>();
        persistentHCOAccount.put('Id', HCOAccount.Id);
        persistentHCOAccount.put('assnFax', '7287266679');
        persistentHCOAccount.put('assnPhone', '12345678908');
        persistentHCOAccount.put('remsID', HCOAccount.Id);
        persistentHCOAccount.put('addressId', testaddress.Id);
        pageState.put('selectedResults', persistentHCOAccount);

        persistedAccountMap.put('PatientConnect__PC_Caregiver_First_Name__c', 'TestProcessorFirstName');
        persistedAccountMap.put('PatientConnect__PC_Caregiver_Last_Name__c', 'TestProcessorLastName');
        persistedAccountMap.put('PatientConnect__PC_Email__c', 'TestProcessorCGFirstName@test.com');
        persistedAccountMap.put('address1', 'TestProcessorAddress1');

        pageState.put('prescription', persistedAccountMap);
        Map<String, Object> persistedInteractionMap = new Map<String, Object>();
        persistedInteractionMap.put('firstName', 'TestProcessorFirstName');
        persistedInteractionMap.put('lastName', 'TestProcessorLastName');
        persistedInteractionMap.put('caregiverFirstName', 'TestProcessorCGFirstName');
        persistedInteractionMap.put('address1', 'TestProcessorAddress1');
        persistedInteractionMap.put('city', 'TestProcessorCity');
        persistedInteractionMap.put('spc_SOC_Type__c', 'Home');
        persistedInteractionMap.put('spc_SOC_Type_Other__c', 'Test Other');
        persistedInteractionMap.put('socNameId', HCOAccount.id);
        persistedInteractionMap.put('fax', '1234567899');
        pageState.put('interaction', persistedInteractionMap);
        spc_SOCEWPProcessor.processEnrollment(enrollmentCase, pageState);
        System.assert(persistedAccountMap.size() > 0);
        test.stopTest();

    }

}
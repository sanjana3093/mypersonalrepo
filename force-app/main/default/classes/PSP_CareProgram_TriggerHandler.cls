/*********************************************************************************************************
class Name      : PSP_CareProgram_TriggerHandler 
Description		: Trigger Handler Class for Care Program Object
@author     	: Shourya Solipuram 
@date       	: July 10, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Shourya Solipuram           July 10, 2019          Initial Version
****************************************************************************************************************/ 
public class PSP_CareProgram_TriggerHandler extends PSP_trigger_Handler {
    /* trigger implementation being called */
    PSP_CareProgram_TriggerImplementation triggerImpl = new PSP_CareProgram_TriggerImplementation();
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Before Insert method of Care Program
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    public override void beforeInsert () {
        triggerImpl.uniqueCheck(trigger.new);
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Before Update method of Care Program
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    public override void beforeUpdate () {
        //List of programs to check
        final List<CareProgram> prgToCheck = new List<CareProgram> ();
        for (CareProgram prg : (List<CareProgram>)Trigger.new) { 
            final CareProgram oldPrg = (CareProgram)Trigger.oldMap.get (prg.Id);
            if (String.isNotBlank (prg.Name) && prg.Name != oldPrg.Name) {
                prgToCheck.add (prg);
            }
        }
        if (!prgToCheck.isEmpty ()) {
            triggerImpl.uniqueCheck (prgToCheck);
        }
    }
}
/********************************************************************************************************
*  @author          Deloitte
*  @date            05/29/2018
*  @description   This is the Trigger Handler class for Task Trigger
*  @version         1.0
*********************************************************************************************************/
public class spc_TaskTriggerHandler extends spc_TriggerHandler {

  spc_TaskTriggerImplementation triggerImplementation = new spc_TaskTriggerImplementation();

  /*************************************************************************************************
    * @author      Deloitte
    * @date        05/29/2018
    * @Description BeforeInsert trigger for task
    * @Return      Void
    **************************************************************************************************/
  public override void beforeInsert() {
    Set<Id> relatedObjectsList = new Set<Id>();
    List<Task> CaseTaskLists = new List<Task>();
    List<Id> caseWhatIds = new List<ID>();
    List<Id> coverageWhatIds = new List<ID>();
    List<Id> InteractionWhatIds = new List<ID>();
    List<Task> CoverageTaskLists = new List<Task>();
    List<Task> InteractionTaskLists = new List<Task>();
    List<Task> tasks = (List<Task>)Trigger.new;

    //Iterate over the new tasks
    for (Task taskObj : tasks) {
      if (String.isNotBlank(taskObj.PatientConnect__PC_Assigned_To__c)) {
        taskObj.OwnerId = taskObj.PatientConnect__PC_Assigned_To__c;
      }
      if (String.isNotBlank(taskObj.whatId) && taskObj.WhatId.getSObjectType() == Case.sObjectType && String.isBlank(taskObj.PatientConnect__PC_Program__c)) {
        if (!relatedObjectsList.contains(taskObj.WhatId) ) {
          relatedObjectsList.add(taskObj.WhatId);
        }
      }
      if (String.isNotBlank(taskObj.WhatId) && taskObj.WhatId.getSObjectType() == Case.sObjectType) {
        CaseTaskLists.add(taskObj);
        caseWhatIds.add(taskObj.WhatId);

      }
      if (String.isNotBlank(taskObj.WhatId) && taskObj.WhatId.getSObjectType() == PatientConnect__PC_Program_Coverage__c.sObjectType) {
        CoverageTaskLists.add(taskObj);
        coverageWhatIds.add(taskObj.WhatId);
      }
      if (String.isNotBlank(taskObj.WhatId) && taskObj.WhatId.getSObjectType() == PatientConnect__PC_Interaction__c.sObjectType) {
        InteractionTaskLists.add(taskObj);
        InteractionWhatIds.add(taskObj.WhatId);
      }

    }
    if (! relatedObjectsList.isEmpty()) {
      triggerImplementation.procesTaskForProgramCases(tasks, relatedObjectsList);
    }
    //Create necessary tasks on cases
    if (!CaseTaskLists.isEmpty()) {
      triggerImplementation.processTasksForCases(CaseTaskLists, caseWhatIds, spc_ApexConstants.PC_CASE);
    }
    //Create necessary tasks on Coverages
    if (!CoverageTaskLists.isEmpty()) {
      triggerImplementation.processTasksForCases(CoverageTaskLists, coverageWhatIds, spc_ApexConstants.OBJ_PROGRAM_COV);
    }
    //Create necessary tasks on interactions
    if (!InteractionTaskLists.isEmpty()) {
      triggerImplementation.processTasksForCases(InteractionTaskLists, InteractionWhatIds, spc_ApexConstants.OBJ_INTERACTION);
    }
  }

  /*************************************************************************************************
   * @author      Deloitte
   * @date        05/29/2018
   * @Description beforeUpdate trigger for task
   * @Return      Void
   **************************************************************************************************/
  public override void beforeUpdate() {
    Set<Id> relatedObjectsList = new Set<Id>();
    List<Task> newTasks = (List<Task>)Trigger.new;
    for (Task newTask : newTasks) {
      Task oldTask =  (Task)Trigger.oldMap.get(newTask.Id);
      if (String.isNotBlank(newTask.whatId) && newTask.whatId != oldTask.whatId) {
        if (!relatedObjectsList.contains(newTask.WhatId)) {
          relatedObjectsList.add(newTask.WhatId);
        }
      }
    }
    if (!relatedObjectsList.isEmpty()) {
      triggerImplementation.procesTaskForProgramCases(newTasks, relatedObjectsList);
    }
  }

  /*************************************************************************************************
  * @author      Deloitte
  * @date        05/29/2018
  * @Description afterUpdate trigger for task
  * @Return      Void
  **************************************************************************************************/
  public override void afterUpdate() {
    Set<Id> caseIdsForWelcome = new Set<Id>();
    Map<Id, PicklistValue> mapFollowUpTasks = new Map<Id, PicklistValue>();
    Map<Id, String> mapAddtionalFollowUpTask = new Map<Id, String>();
    Set<Id> caseIdsFollowupTasks = new Set<Id>();
    Map<Id, Task> oldMap = (Map<Id, Task>) Trigger.oldMap;


    Set<Id> welcomeCallSecondAttemptCaseIds = new Set<Id>();
    Set<Id> welcomeCallThirdAttemptCaseIds = new Set<Id>();
    Set<Id> socLogisticsCallSecondAttemptCaseIds = new Set<Id>();
    Set<Id> socLogisticsCallThirdAttemptCaseIds = new Set<Id>();
    Set<Id> overduePICSecondAttemptCaseIds = new Set<Id>();
    Set<Id> overduePICThirdAttemptCaseIds = new Set<Id>();
    Set<Id> overduePICFaxCaseIds = new Set<Id>();
    Set<Id> callHCPMICaseIdsSecAttempt = new Set<Id>();
    Set<Id> callHCPMICaseIdsthirdAttempt = new Set<Id>();
    String channel_Call  = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CHANNEL_CALL);
    String category_MI = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_MISSING_INFORMATION);

    String channel_phone = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CHANNEL_PHONE);
    String channel_fax = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CHANNEL_FAX);
    String direction_outbound = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_DIR_OUTBOUND);
    String priority_normal = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_PRIORITY_NORMAL);
    String priority_high = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_HIGH_PRIORITY);
    String status_notstarted = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_NOT_STARTED);

    String category_welcomecalllite = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_WELCOMECALLITE);
    String category_soclogisticscall = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_SOC_LOGISTIC_CALL);
    String category_overduepic = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_OVERDUE_PIC);
    String category_siteofcare = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CAT_SITE_OF_CARE);

    String subcategory_firstattempt = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_CAT_FIRST_ATTEMPT);
    String subcategory_secondattempt = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUBCATEGORY_SECONDATTEMPT);
    String subcategory_thirdattempt = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUBCATEGORY_THIRDATTEMPT);
    String subcategory_faxsiteofcare = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_SUBCATEGORY_FAX);
    String subcategory_confirmInfusion = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_CONFIRM_INFUSION);

    Map<Id, Task> taskMap = new Map<Id, Task>();
    for (Task taskObj : [SELECT Id, PatientConnect__PC_Program__c, PatientConnect__PC_Program__r.spc_AC__c,
                         PatientConnect__PC_Program__r.spc_HIPAA_Consent_Received__c,
                         spc_Interaction__c,
                         spc_Interaction__r.spc_Scheduled_Date__c
                         FROM Task where Id IN : trigger.newMap.keySet()]) {
      taskMap.put(taskObj.Id, taskObj);
    }

    for (Task taskObj : (List<Task>)Trigger.New) {
      Task oldTask = oldMap.get(taskObj.Id);
      //Get the case Ids of task belonging to category = 'Welcome Call' with Status = 'Complete' and Call Outcome = 'Reached'
      if (String.isNotBlank(taskObj.WhatId) && taskObj.WhatId.getSObjectType() == Case.sObjectType) {
        //If Task created on Case object
        /***************Fetch case Ids for Welcome Call Task with Calloutcome reached and Task status Completed ****/
        if (taskObj.PatientConnect__PC_Category__c  == spc_ApexConstants.getValue(PicklistValue.TASK_CAT_WELCOMECALL)
            && taskObj.PatientConnect__PC_Call_Outcome__c  == spc_ApexConstants.getValue(PicklistValue.TASK_CALL_OUTCOME_REACHED)
            && taskObj.PatientConnect__PC_Call_Outcome__c  != spc_ApexConstants.getValue(PicklistValue.TASK_CALL_OUTCOME_NEVER_START)
            && taskObj.PatientConnect__PC_Call_Outcome__c != spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CALL_OUTCOME_NOT_NEEDED)
            && taskObj.Status  == spc_ApexConstants.getValue(PicklistValue.TASK_STATUS_COMPLETED)
            && (taskObj.PatientConnect__PC_Call_Outcome__c != oldMap.get(taskObj.Id).PatientConnect__PC_Call_Outcome__c
                || taskObj.Status != oldMap.get(taskObj.Id).Status)) {
          caseIdsForWelcome.add(taskObj.WhatId);
        }
        /***** Fetch case Ids for Missing Information Task with Task status Completed *****/
        if (taskObj.PatientConnect__PC_Category__c  == spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_MISSING_INFORMATION)
            && taskObj.PatientConnect__PC_Call_Outcome__c  != spc_ApexConstants.getValue(PicklistValue.TASK_CALL_OUTCOME_NEVER_START)
            && taskObj.PatientConnect__PC_Call_Outcome__c  != spc_ApexConstants.getValue(PicklistValue.TASK_CALL_OUTCOME_REACHED)
            && taskObj.PatientConnect__PC_Call_Outcome__c != spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CALL_OUTCOME_NOT_NEEDED)
            && taskObj.Status  == spc_ApexConstants.getValue(PicklistValue.TASK_STATUS_COMPLETED)
            && (taskObj.PatientConnect__PC_Call_Outcome__c != oldMap.get(taskObj.Id).PatientConnect__PC_Call_Outcome__c
                || taskObj.Status != oldMap.get(taskObj.Id).Status)) {
          if (null != taskMap.get(taskObj.id).PatientConnect__PC_Program__c
              && taskMap.get(taskObj.id).PatientConnect__PC_Program__r.spc_HIPAA_Consent_Received__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES)) {
            if (taskObj.PatientConnect__PC_Sub_Category__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUB_CAT_FIRST_ATTEMPT)) {
              callHCPMICaseIdsSecAttempt.add(taskObj.WhatId);
            } else if (taskObj.PatientConnect__PC_Sub_Category__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_SUBCATEGORY_SECONDATTEMPT)) {
              callHCPMICaseIdsthirdAttempt.add(taskObj.WhatId);
            }
          }
        }

        //If task call outcome is changed to any other value other than Reached
        if (taskObj.PatientConnect__PC_Call_Outcome__c != oldMap.get(taskObj.Id).PatientConnect__PC_Call_Outcome__c
            && taskObj.PatientConnect__PC_Call_Outcome__c  != spc_ApexConstants.getValue(PicklistValue.TASK_CALL_OUTCOME_REACHED)) {

          if (taskObj.Subject == spc_ApexConstants.getValue(PicklistValue.TASK_SUB_PATIENT_DISCONTINUATION)
              && taskObj.PatientConnect__PC_Category__c == spc_ApexConstants.getValue(PicklistValue.TASK_CAT_DISCONTINUATION)) {
            mapFollowUpTasks.put(taskObj.Id,  PicklistValue.TASK_SUB_PATIENT_DISCONTINUATION);
            caseIdsFollowupTasks.add(taskObj.WhatId);
          } else if (taskObj.PatientConnect__PC_Call_Outcome__c != spc_ApexConstants.getValue(PicklistValue.TASK_CALL_OUTCOME_NEVER_START)
                     && taskObj.PatientConnect__PC_Call_Outcome__c != spc_ApexConstants.getValue(PicklistValue.TASK_CALL_OUTCOME_REACHED)
                     && taskObj.PatientConnect__PC_Call_Outcome__c != spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CALL_OUTCOME_NOT_NEEDED)
                     && taskObj.Subject.startsWithIgnoreCase(spc_ApexConstants.getValue(PicklistValue.TASK_SUB_PERFORM_WELCOME_CALL))) {
            //If Task outcome is not reached and never start and subject is for welcome call
            mapFollowUpTasks.put(taskObj.Id,  PicklistValue.TASK_SUB_PERFORM_WELCOME_CALL);
            caseIdsFollowupTasks.add(taskObj.WhatId);
          }
        }

        //Changes for US-307138
        if ((taskObj.Status != oldTask.Status || taskObj.PatientConnect__PC_Call_Outcome__c != oldTask.PatientConnect__PC_Call_Outcome__c)
            && taskObj.Status == spc_ApexConstants.getValue(PicklistValue.TASK_STATUS_COMPLETED)
            && taskObj.PatientConnect__PC_Call_Outcome__c != spc_ApexConstants.getValue(PicklistValue.TASK_CALL_OUTCOME_REACHED)
            && taskObj.PatientConnect__PC_Call_Outcome__c != spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CALL_OUTCOME_NOT_NEEDED)
            && taskObj.PatientConnect__PC_Call_Outcome__c != spc_ApexConstants.getValue(PicklistValue.TASK_CALL_OUTCOME_NEVER_START)) {

          //Welcome Call Lite First Attempt Task - creating Second Attempt task
          if (taskObj.PatientConnect__PC_Category__c == category_welcomecalllite && taskObj.PatientConnect__PC_Sub_Category__c == subcategory_firstattempt) {
            welcomeCallSecondAttemptCaseIds.add(taskObj.WhatId);
          }

          //Welcome Call Lite Second Attempt Task - creating Third Attempt task
          if (taskObj.PatientConnect__PC_Category__c == category_welcomecalllite && taskObj.PatientConnect__PC_Sub_Category__c == subcategory_secondattempt) {
            welcomeCallThirdAttemptCaseIds.add(taskObj.WhatId);
          }

          //SOC Logistics Call First Attempt Task - creating Second Attempt task
          if (taskObj.PatientConnect__PC_Category__c == category_soclogisticscall && taskObj.PatientConnect__PC_Sub_Category__c == subcategory_firstattempt) {
            socLogisticsCallSecondAttemptCaseIds.add(taskObj.WhatId);
          }

          //SOC Logistics Call Second Attempt Task - creating Third Attempt task
          if (taskObj.PatientConnect__PC_Category__c == category_soclogisticscall && taskObj.PatientConnect__PC_Sub_Category__c == subcategory_secondattempt) {
            socLogisticsCallThirdAttemptCaseIds.add(taskObj.WhatId);
          }

          if (null != taskObj.PatientConnect__PC_Program__c
              && taskObj.PatientConnect__PC_Category__c == category_overduepic
              && (taskMap.get(taskObj.Id).PatientConnect__PC_Program__r.spc_AC__c)
              .equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.CASE_PIC_STATUS_ONHOLD))) {
            //Overdue PIC First Attempt Task - creating Second Attempt task
            if (taskObj.PatientConnect__PC_Sub_Category__c == subcategory_firstattempt) {
              overduePICSecondAttemptCaseIds.add(taskObj.WhatId);
            }
            //Overdue PIC Second Attempt Task - creating Third Attempt task
            if (taskObj.PatientConnect__PC_Sub_Category__c == subcategory_secondattempt) {
              overduePICThirdAttemptCaseIds.add(taskObj.WhatId);
            }
            //Overdue PIC Third Attempt Task - creating Fax task
            if (taskObj.PatientConnect__PC_Sub_Category__c == subcategory_thirdattempt) {
              overduePICFaxCaseIds.add(taskObj.WhatId);
            }
          }

        }

      }
    }

    if (! callHCPMICaseIdsSecAttempt.isEmpty()) {
      triggerImplementation.createFollowUpTasks(callHCPMICaseIdsSecAttempt, System.today() + 1, category_MI,
          subcategory_secondattempt, channel_Call, direction_outbound, priority_normal, status_notstarted,
          label.spc_TASK_SUBJECT_CALL_HCP_FOR_MISSING_INFO_2);
    }

    if (! callHCPMICaseIdsthirdAttempt.isEmpty()) {
      triggerImplementation.createFollowUpTasks(callHCPMICaseIdsthirdAttempt, System.today() + 1, category_MI,
          subcategory_thirdattempt, channel_Call, direction_outbound, priority_normal, status_notstarted,
          label.spc_TASK_SUBJECT_CALL_HCP_FOR_MISSING_INFO_3);
    }

    if (! caseIdsForWelcome.isEmpty()) {
      //For these 'Welcome Call' tasks, the 'Welcome Call Completed' picklist on Interaction record should be flipped to 'Yes'
      triggerImplementation.setWelcomeCallFinished(caseIdsForWelcome);
    }

    if (! mapFollowUpTasks.isEmpty()) {
      //For these 'Welcome Call' tasks, the 'Welcome Call Completed' picklist on Interaction record should be flipped to 'Yes'
      triggerImplementation.createFollowupTasks(mapFollowUpTasks, caseIdsFollowupTasks, (Map<Id, Task>)Trigger.NewMap);
    }

    //Creating Welcome call Lite - Second Attempt task
    if (! welcomeCallSecondAttemptCaseIds.isEmpty()) {
      triggerImplementation.createFollowUpTasks(welcomeCallSecondAttemptCaseIds, System.today() + 1, category_welcomecalllite,
          subcategory_secondattempt, channel_phone, direction_outbound, priority_normal, status_notstarted,
          System.Label.spc_WelcomeCallLiteSecondAttemptTaskSubject);
    }

    //Creating Welcome call Lite - Third Attempt task
    if (! welcomeCallThirdAttemptCaseIds.isEmpty()) {
      triggerImplementation.createFollowUpTasks(welcomeCallThirdAttemptCaseIds, System.today() + 1, category_welcomecalllite,
          subcategory_thirdattempt, channel_phone, direction_outbound, priority_normal, status_notstarted,
          System.Label.spc_WelcomeCallLiteThirdAttemptTaskSubject);
    }

    //Creating SOC Logistic call - Second Attempt task
    if (! socLogisticsCallSecondAttemptCaseIds.isEmpty()) {
      triggerImplementation.createFollowUpTasks(socLogisticsCallSecondAttemptCaseIds, System.today() + 1, category_soclogisticscall,
          subcategory_secondattempt, channel_phone, direction_outbound, priority_normal, status_notstarted,
          System.Label.spc_SOCLogisticsSecondAttemptTaskSubject);
    }

    //Creating SOC Logistic call - Third Attempt task
    if (! socLogisticsCallThirdAttemptCaseIds.isEmpty()) {
      triggerImplementation.createFollowUpTasks(socLogisticsCallThirdAttemptCaseIds, System.today() + 1, category_soclogisticscall,
          subcategory_thirdattempt, channel_phone, direction_outbound, priority_normal, status_notstarted,
          System.Label.spc_SOCLogisticsThirdAttemptTaskSubject);
    }

    //Creating Overdue PIC - Second Attempt task
    if (! overduePICSecondAttemptCaseIds.isEmpty()) {
      triggerImplementation.createFollowUpTasks(overduePICSecondAttemptCaseIds, System.today() + 1, category_overduepic,
          subcategory_secondattempt, channel_phone, direction_outbound, priority_high, status_notstarted,
          System.Label.spc_OverduePICSecondAttemptTaskSubject);
    }

    //Creating Overdue PIC - Third Attempt task
    if (! overduePICThirdAttemptCaseIds.isEmpty()) {
      triggerImplementation.createFollowUpTasks(overduePICThirdAttemptCaseIds, System.today() + 1, category_overduepic,
          subcategory_thirdattempt, channel_phone, direction_outbound, priority_high, status_notstarted,
          System.Label.spc_OverduePICThirdAttemptTaskSubject);
    }

    //Creating Overdue PIC - Fax SOC task
    if (! overduePICFaxCaseIds.isEmpty()) {
      triggerImplementation.createFollowUpTasks(overduePICFaxCaseIds, System.today(), category_overduepic,
          subcategory_faxsiteofcare, channel_fax, direction_outbound, priority_high, status_notstarted,
          System.Label.spc_OverduePICFaxTaskSubject);
    }
  }

  /*************************************************************************************************
    * @author      Deloitte
    * @date        05/29/2018
    * @Description afterInsert trigger for task
    * @Return      Void
    **************************************************************************************************/
  public override void afterInsert() {
    Set<Id> caseIdsForWelcome = new Set<Id>();
    for (Task taskObj : (List<Task>)Trigger.New) {
      List<Task> tasks = new List<Task>();

      //Get the case Ids of task belonging to category = 'Welcome Call' with Status = 'Complete' and Call Outcome = 'Reached'
      if (String.isNotBlank(taskObj.PatientConnect__PC_Category__c) && taskObj.PatientConnect__PC_Category__c.equalsIgnoreCase(spc_ApexConstants.getValue(PicklistValue.TASK_CAT_WELCOMECALL))
          && String.isNotBlank(taskObj.Status) && taskObj.Status.equalsIgnoreCase(spc_ApexConstants.getValue(PicklistValue.TASK_STATUS_COMPLETED))
          && String.isNotBlank(taskObj.PatientConnect__PC_Call_Outcome__c) && taskObj.PatientConnect__PC_Call_Outcome__c.equalsIgnoreCase(spc_ApexConstants.getValue(PicklistValue.TASK_CALL_OUTCOME_REACHED))
          && String.isNotBlank(taskObj.WhatId) && taskObj.WhatId.getSObjectType() == Case.sobjectType) {
        caseIdsForWelcome.add(taskObj.WhatId);

      }
    }

    if (! caseIdsForWelcome.isEmpty()) {
      //For these 'Welcome Call' tasks, the 'Welcome Call Completed' picklist on Interaction record should be flipped to 'Yes'
      triggerImplementation.setWelcomeCallFinished(caseIdsForWelcome);
    }
  }

  /*************************************************************************************************
  * @author      Deloitte
  * @date        10/16/2018
  * @Description Before Delete Task Trigger
  * @param       None
  * @Return      Void
  **************************************************************************************************/
  public override void beforeDelete() {
    Map<Id, Task> taskAndOwnerMap = new Map<ID, Task>();
    List<Task> tasks = (List<Task>)Trigger.old;
    for (Task taskObj : tasks) {
      if (String.isNotBlank(taskObj.OwnerId)) {
        taskAndOwnerMap.put(taskObj.OwnerId, taskObj);
      }
    }
    if (! taskAndOwnerMap.isEmpty()) {
      //Restrict Case Managers from Deleting tasks
      triggerImplementation.restrictDeletionOfTask(taskAndOwnerMap);
    }
  }
}
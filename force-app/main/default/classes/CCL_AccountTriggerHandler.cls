/********************************************************************************************************
*  @author          Deloitte
*  @description     Account Trigger Handler Class to update potential duplicate flag for person account.
*  @date            August 27, 2020
*  @version         1.0
*********************************************************************************************************/

public class CCL_AccountTriggerHandler {
    /*
     *   @author           Deloitte
     *   @description      Update potential duplicate flag when duplicate record exist
     *   @para             List (Account New Account List )
     *   @return           void
     *   @date             27 August 2020
     */
	public void updatePotentialDuplicateFlag(List<Account> newAccountsList) {
		for(Account newAccount : newAccountsList) {
			if(newAccount.IsPersonAccount) {
				Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(new List<Account>{newAccount});
				for (Datacloud.FindDuplicatesResult findDupeResult : results) {
					for (Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {
						for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
							if(matchResult.getMatchRecords().size() > 0) {
								newAccount.CCL_Potential_Duplicate_Patient__c = true;
							} else {
								newAccount.CCL_Potential_Duplicate_Patient__c = false;
							}
						}
					}
				}
			}
		}
	}
}
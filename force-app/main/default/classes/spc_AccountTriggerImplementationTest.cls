/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for Account Trigger Implementation
    *  @date            07/18/2018
    *  @version         1.0
*********************************************************************************************************/
@IsTest
public class spc_AccountTriggerImplementationTest {

    @testSetup static void setupTestdata() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
        Account patientAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        patientAcc1.PatientConnect__PC_Date_of_Birth__c = system.today();
        patientAcc1.spc_HIPAA_Consent_Received__c = '';
        patientAcc1.spc_Patient_Services_Consent_Received__c = '';
        patientAcc1.spc_Text_Consent__c = '';
        patientAcc1.spc_Patient_Mrkt_and_Srvc_consent__c = '';
        insert patientAcc1;
        Case programCaseRec2 = spc_Test_Setup.createCases(new List<Account> {patientAcc1}, 1, 'PC_Program').get(0);
        if (null != programCaseRec2) {
            programCaseRec2.Type = 'Program';
            //programCaseRec2.PatientConnect__PC_Status_Indicator_1__c = '';
            insert programCaseRec2;
        }
        system.assertNotEquals(programCaseRec2, NULL);
    }

    public static testmethod void testExecution1() {
        set<id> lstAcc = new set<id>();
        Account acc = [select id from Account];
        acc.spc_HIPAA_Consent_Received__c = 'No';
        acc.spc_Patient_HIPAA_Consent_Date__c = System.Today();
        acc.spc_Patient_Services_Consent_Received__c = 'No';
        acc.spc_Patient_Service_Consent_Date__c = System.Today();
        acc.spc_Text_Consent__c = 'No';
        acc.spc_Patient_Text_Consent_Date__c = System.Today();
        acc.spc_Patient_Mrkt_and_Srvc_consent__c = 'No';
        acc.spc_Patient_Marketing_Consent_Date__c = System.Today();
        update acc;
        lstAcc.add(acc.id);
        spc_AccountTriggerImplementation accImp = new spc_AccountTriggerImplementation();
        accImp.setProgramCaseIndicatorStatus(lstAcc);
        List<Id> accountList = new List<Id>();
        accountList.add(acc.id);
        accImp.setProgramCaseConsent(accountList);


    }
    public static testmethod void testExecution2() {
        set<id> lstAcc = new set<id>();
        List<id> lstofAcc = new List<id>();
        Account acc = [select id from Account];
        system.assertNotEquals(acc, NULL);
        acc.spc_HIPAA_Consent_Received__c = 'yes';
        acc.spc_Patient_HIPAA_Consent_Date__c = system.today();
        acc.spc_Patient_Services_Consent_Received__c = 'yes';
        acc.spc_Patient_Service_Consent_Date__c = system.today();
        acc.spc_Text_Consent__c = 'yes';
        acc.spc_Patient_Text_Consent_Date__c = system.today();
        acc.spc_Patient_Mrkt_and_Srvc_consent__c = 'yes';
        acc.spc_Patient_Marketing_Consent_Date__c = system.today();
        update acc;
        lstAcc.add(acc.id);
        lstofAcc.add(acc.id);
        spc_AccountTriggerImplementation accImp = new spc_AccountTriggerImplementation();
        accImp.setProgramCaseIndicatorStatus(lstAcc);
        //accImp.createHCPLogisticTasks(lstofAcc);
    }
    public static testmethod void testExecution3() {
        set<id> lstAcc = new set<id>();
        Account acc = [select id from Account];
        system.assertNotEquals(acc, NULL);
        acc.spc_HIPAA_Consent_Received__c = 'yes';
        acc.spc_Patient_HIPAA_Consent_Date__c = system.today();
        acc.spc_Patient_Services_Consent_Received__c = 'No';
        acc.spc_Patient_Service_Consent_Date__c = System.Today();
        acc.spc_Text_Consent__c = 'No';
        acc.spc_Patient_Text_Consent_Date__c = System.Today();
        acc.spc_Patient_Mrkt_and_Srvc_consent__c = 'No';
        acc.spc_Patient_Marketing_Consent_Date__c = System.Today();
        update acc;
        lstAcc.add(acc.id);
        spc_AccountTriggerImplementation accImp = new spc_AccountTriggerImplementation();
        accImp.setProgramCaseIndicatorStatus(lstAcc);

    }

    public static testmethod void testcreateFaxEnrollmentIP() {
        List<id> accList = new List<id>();
        Account acc = [select id from Account];
        acc.spc_HIPAA_Consent_Received__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO);
        acc.spc_Patient_HIPAA_Consent_Date__c = system.today();
        update acc;

        accList.add(acc.id);

        Account manufacturerAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId());
        insert manufacturerAcc;
        PatientConnect__PC_Engagement_Program__c engProgram = new PatientConnect__PC_Engagement_Program__c (Name = 'Brexanolone Engagement Program', PatientConnect__PC_Manufacturer__c = manufacturerAcc.Id, PatientConnect__PC_Program_Code__c = 'BREX');
        insert engProgram;

        Case objCase = spc_Test_Setup.createCase('Program', Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId(),
                       engProgram.Id, acc.Id);
        insert objCase;

        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
        hcoAcc.spc_REMS_Certification_Status__c = 'Certified';
        hcoAcc.spc_REMS_Certification_Status_Date__c = System.today();
        insert hcoAcc;

        PatientConnect__PC_Association__c association = spc_Test_Setup.createAssociation(hcoAcc, objCase, 'HCO', system.today() + 1);

        spc_AccountTriggerImplementation accImp = new spc_AccountTriggerImplementation();

        Task task = spc_Test_Setup.createTask(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_COMPLETED), objCase.Id, 'High',
                                              spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_WELCOMECALLITE),
                                              'FAX', System.Date.today());
        task.PatientConnect__PC_Category__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_WELCOMECALLITE);
        insert task;

    }

    public static testmethod void testExecution4() {
        set<id> lstAcc = new set<id>();
        Account acc = [select id from Account];
        system.assertNotEquals(acc, NULL);
        lstAcc.add(acc.id);
        spc_AccountTriggerImplementation accImp = new spc_AccountTriggerImplementation();
        accImp.setProgramCaseIndicatorStatus(lstAcc);
    }
    public static testmethod void testExecution5() {
        set<id> lstAcc = new set<id>();
        Account acc = [select id from Account];
    case ocase=[select id from Case where accountid=:acc.id];
        delete ocase;
        system.assertNotEquals(acc, NULL);
        lstAcc.add(acc.id);
        spc_AccountTriggerImplementation accImp = new spc_AccountTriggerImplementation();
        accImp.setProgramCaseIndicatorStatus(lstAcc);
    }
    public static testmethod void testException() {

        spc_AccountTriggerImplementation accImp = new spc_AccountTriggerImplementation();
        accImp.setProgramCaseIndicatorStatus(null);

    }
    public static testmethod void testExecution6() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
        Map<id, Account> lstAcc = new Map<id, Account>();
        Account patientAcc2 = [select id from Account];
        system.assertNotEquals(patientAcc2, NULL);
        patientAcc2.spc_HIPAA_Consent_Received__c = 'No';
        patientAcc2.spc_Patient_HIPAA_Consent_Date__c = system.today();
        patientAcc2.spc_Patient_Services_Consent_Received__c = 'Yes';
        patientAcc2.spc_Patient_Service_Consent_Date__c = System.Today();
        patientAcc2.spc_Text_Consent__c = 'Yes';
        patientAcc2.spc_Patient_Text_Consent_Date__c = System.Today();
        patientAcc2.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        patientAcc2.spc_Patient_Marketing_Consent_Date__c = System.Today();
        update patientAcc2;



        Case programCaseRec3 = spc_Test_Setup.createCases(new List<Account> {patientAcc2}, 1, 'PC_Program').get(0);
        if (null != programCaseRec3) {
            programCaseRec3.Type = 'Program';
            insert programCaseRec3;
        }
        Account accCar = new Account();
        accCar.PatientConnect__PC_First_Name__c = 'Testfirst';
        accCar.PatientConnect__PC_Last_Name__c = 'testsecond';
        accCar.PatientConnect__PC_Date_of_Birth__c = System.Today();
        accCar.PatientConnect__PC_Email__c = 'abc@gmail.com';
        accCar.PatientConnect__PC_Caregiver_Relationship_to_Patient__c = 'Self';
        accCar.spc_Permission_to_Email__c = 'Yes';
        accCar.spc_Date_Permission_to_Share_PHI__c = System.Today();
        accCar.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId();
        insert accCar;

        PatientConnect__PC_Association__c assoc = new PatientConnect__PC_Association__c();
        assoc.PatientConnect__PC_Program__c = programCaseRec3.id;
        assoc.PatientConnect__PC_Role__c = 'Caregiver';
        assoc.PatientConnect__PC_Account__c = accCar.id;
        insert assoc;

        //      PatientConnect__PC_Association__c assoc5=new PatientConnect__PC_Association__c();
        //   assoc5.PatientConnect__PC_Program__c=programCaseRec3.id;
        // assoc5.PatientConnect__PC_Role__c='Designated Caregiver';
        //  assoc5.PatientConnect__PC_Account__c=accCar.id;
        //  insert assoc5;



        spc_AccountTriggerImplementation  accImp = new spc_AccountTriggerImplementation();
        lstAcc.put(accCar.id, accCar);
        spc_Database.ups(accImp.updateCaregiverAssociation(lstAcc), new List<sObjectField>(), true, true);
        spc_Database.ups(accImp.updateDesignatedCaregiverAssociation(lstAcc.keySet()), new List<sObjectField>(), true, true);

        programCaseRec3.PatientConnect__PC_Status_Indicator_3__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.PATIENT_STATUS_INDICATOR_COMPLETE);
        update programCaseRec3;
        Set<Id> accountList = new Set<Id>();
        patientAcc2.spc_HIPAA_Consent_Received__c = 'Yes';
        update patientAcc2;
        accountList.add(patientAcc2.id);
        accImp.setBIBVStatusIndicator(accountList);
    }

    public static testmethod void testExecution7() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
        Set<id> lstAcc = new Set<id>();
        Account patientAcc3 = [select id from Account];
        system.assertNotEquals(patientAcc3, NULL);
        patientAcc3.spc_HIPAA_Consent_Received__c = 'No';
        patientAcc3.spc_Patient_HIPAA_Consent_Date__c = system.today();
        patientAcc3.spc_Patient_Services_Consent_Received__c = 'Yes';
        patientAcc3.spc_Patient_Service_Consent_Date__c = System.Today();
        patientAcc3.spc_Text_Consent__c = 'Yes';
        patientAcc3.spc_Patient_Text_Consent_Date__c = System.Today();
        patientAcc3.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        patientAcc3.spc_Patient_Marketing_Consent_Date__c = System.Today();
        update patientAcc3;



        Case programCaseRec4 = spc_Test_Setup.createCases(new List<Account> {patientAcc3}, 1, 'PC_Program').get(0);
        if (null != programCaseRec4) {
            programCaseRec4.Type = 'Program';
            insert programCaseRec4;
        }
        Account accCar2 = new Account();
        accCar2.PatientConnect__PC_First_Name__c = 'Testfirstnmae';
        accCar2.PatientConnect__PC_Last_Name__c = 'testsecondname';
        accCar2.PatientConnect__PC_Date_of_Birth__c = System.Today();
        accCar2.PatientConnect__PC_Email__c = 'abc2@gmail.com';
        accCar2.PatientConnect__PC_Caregiver_Relationship_to_Patient__c = 'Self';
        accCar2.spc_Permission_to_Email__c = 'Yes';
        accCar2.spc_Date_Permission_to_Share_PHI__c = System.Today();
        accCar2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId();
        insert accCar2;

        //  PatientConnect__PC_Association__c assoc=new PatientConnect__PC_Association__c();
        // assoc.PatientConnect__PC_Program__c=programCaseRec3.id;
        // assoc.PatientConnect__PC_Role__c='Caregiver';
        // assoc.PatientConnect__PC_Account__c=accCar.id;
        // insert assoc;

        PatientConnect__PC_Association__c assoc5 = new PatientConnect__PC_Association__c();
        assoc5.PatientConnect__PC_Program__c = programCaseRec4.id;
        assoc5.PatientConnect__PC_Role__c = 'Designated Caregiver';
        assoc5.PatientConnect__PC_Account__c = accCar2.id;
        insert assoc5;



        spc_AccountTriggerImplementation  accImp = new spc_AccountTriggerImplementation();
        lstAcc.add(accCar2.id);
        //accImp.updateCaregiverAssociation(lstAcc);
        spc_Database.ups(accImp.updateDesignatedCaregiverAssociation(lstAcc), new List<sObjectField>(), true, true);
        Map<Id, String> accountIdAndNameMap = new Map<Id, string>();
        accountIdAndNameMap.put(patientAcc3.id, patientAcc3.Name);
        accImp.updateAccountNameInCase(accountIdAndNameMap);
    }

    public static testmethod void testExecution8() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
        Account patientAcc4 = [select id from Account];
        system.assertNotEquals(patientAcc4, NULL);
        patientAcc4.spc_HIPAA_Consent_Received__c = 'Yes';
        patientAcc4.spc_Patient_HIPAA_Consent_Date__c = system.today();
        patientAcc4.spc_Patient_Services_Consent_Received__c = 'Yes';
        patientAcc4.spc_Patient_Service_Consent_Date__c = System.Today();
        patientAcc4.spc_Text_Consent__c = 'Yes';
        patientAcc4.spc_Patient_Text_Consent_Date__c = System.Today();
        patientAcc4.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
        patientAcc4.spc_Patient_Marketing_Consent_Date__c = System.Today();
        update patientAcc4;

        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        spc_Database.ins(manAcc);

        PatientConnect__PC_Engagement_Program__c pce = new PatientConnect__PC_Engagement_Program__c();
        pce.Name = 'Brexanolone Engagement Program';
        pce.PatientConnect__PC_Program_Start_Date__c = System.Today();
        pce.PatientConnect__PC_Program_End_Date__c = System.Today() + 30;
        pce.PatientConnect__PC_Program_Code__c = 'BREX';
        pce.PatientConnect__PC_Description__c = 'Engagement Program for Patients on the Brexanolone drug.';
        pce.PatientConnect__PC_Manufacturer__c = manAcc.id;

        insert pce;



        Case programCaseRec4 = spc_Test_Setup.createCases(new List<Account> {patientAcc4}, 1, 'PC_Program').get(0);
        if (null != programCaseRec4) {
            programCaseRec4.Type = 'Program';
            programCaseRec4.PatientConnect__PC_Engagement_Program__c = pce.id;
            programCaseRec4.PatientConnect__PC_Status_Indicator_3__c = 'Complete';
            insert programCaseRec4;
        }

        Account accCar2 = new Account();
        accCar2.PatientConnect__PC_First_Name__c = 'Testfirstnmae';
        accCar2.PatientConnect__PC_Last_Name__c = 'testsecondname';
        accCar2.PatientConnect__PC_Date_of_Birth__c = System.Today();
        accCar2.PatientConnect__PC_Email__c = 'abc2@gmail.com';
        accCar2.PatientConnect__PC_Caregiver_Relationship_to_Patient__c = 'Self';
        accCar2.spc_Permission_to_Email__c = 'Yes';
        accCar2.spc_Date_Permission_to_Share_PHI__c = System.Today();
        accCar2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        accCar2.spc_REMS_Certification_Status__c = 'Certified';
        accCar2.spc_REMS_Certification_Status_Date__c = Date.today();
        insert accCar2;

        PatientConnect__PC_Association__c assoc5 = new PatientConnect__PC_Association__c();
        assoc5.PatientConnect__PC_Program__c = programCaseRec4.id;
        assoc5.PatientConnect__PC_Role__c = 'HCO';
        assoc5.PatientConnect__PC_Account__c = accCar2.id;
        assoc5.PatientConnect__PC_AssociationStatus__c = 'Active';
        insert assoc5;
        spc_AccountTriggerImplementation  accImp = new spc_AccountTriggerImplementation();
        Set<Id> setNotCertifiedAccountIds = new Set<Id>();
        setNotCertifiedAccountIds.add(accCar2.id);
        //accImp.updateCaregiverAssociation(lstAcc);
        accImp.createPatientAuthorizationTasks(setNotCertifiedAccountIds);

        List<Account> nonManufacturerAccounts = new List<Account>();
        nonManufacturerAccounts.add(manAcc);
        accImp.setParentAccount(nonManufacturerAccounts);
        List<Id> accountList = new List<Id>();
        accountList.add(patientAcc4.id);
        accImp.createHCPLogisticTasks(accountList, true);
        accImp.createHCPLogisticTasks(accountList, false);
    }
}
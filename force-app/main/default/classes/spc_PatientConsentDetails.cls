/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Singleton class to get the patient consent details
    *  @date            11-June-2018
    *  @version         1.0
    *
    *********************************************************************************************************/

public with sharing class spc_PatientConsentDetails {
    @TestVisible
    private static spc_PatientConsentDetails inst;

    public Case objCase {get; private set;}

    /*********************************************************************************
    Constructor : spc_PatientConsentDetails
    Description : Default constructor
    *********************************************************************************/
    @TestVisible
    private spc_PatientConsentDetails() {
        // default constructor set to private
    }

    /*********************************************************************************
    Constructor : spc_PatientConsentDetails
    Description : Retrieves patient consent details based on Case Id
    Parameter   : Case record Id
    *********************************************************************************/
    @TestVisible
    private spc_PatientConsentDetails(Id caseId) {
        objCase = [SELECT
                   Id,
                   Account.RecordTypeId,
                   Account.spc_HIPAA_Consent_Received__c,
                   Account.spc_Text_Consent__c,
                   Account.spc_Patient_Mrkt_and_Srvc_consent__c,
                   Account.spc_Patient_Services_Consent_Received__c,
                   Account.spc_REMS_Text_Consent__c,
                   Account.spc_Services_Text_Consent__c,
                   Account.spc_Marketing_Consent__c
                   FROM Case WHERE Id = : caseId];
    }

    /*********************************************************************************
    Method Name : getInstance
    Description : Creates instance of this class
    Parameter   : Case record Id
    Return Type : spc_PatientConsentDetails class instance
    *********************************************************************************/
    public static spc_PatientConsentDetails getInstance(Id caseId) {
        if (inst == null) {
            inst = new spc_PatientConsentDetails(caseId);
        }
        return inst;
    }
}
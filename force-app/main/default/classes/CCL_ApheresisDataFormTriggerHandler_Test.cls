/********************************************************************************************************
*  @author          Deloitte
*  @description     Test class for Apheresis Data Form Trigger Handler
*  @param
*  @date            Oct 16,2020
*********************************************************************************************************/
@isTest
public class CCL_ApheresisDataFormTriggerHandler_Test {
    /*Account Name */
    final static String ACCOUNT_NAME = 'Test Account Label Compliant';    
    /*ADF Status Awaiting ADF */
    final static String STATUS_AWAITING_ADF = 'Awaiting ADF';
    /*ADF Status Pending Submission */
    final static String STATUS_PENDING_ADF = 'ADF Pending Submission';
    
    @testSetup
    public static void createTestData() {
    final CCL_Time_Zone__c timezone =CCL_Test_SetUp.createTestTimezone(ACCOUNT_NAME); 
        final Account acc=CCL_Test_SetUp.createTestAccount(ACCOUNT_NAME,timezone.Id); 
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADFAwaitingADF(ACCOUNT_NAME,order.Id,acc.Id);
    }
    
    static testMethod void updateADFLabelCompliantTest() {       
        Test.startTest();
		CCL_Apheresis_Data_Form__c adfVal=[select id,CCL_Status__c,CCL_Apheresis_Collection_Center__c,CCL_Apheresis_Collection_Center__r.CCL_Label_Compliant__c from CCL_Apheresis_Data_Form__c LIMIT 1];
        Account accountVal=[select id from Account LIMIT 1];
        adfVal.CCL_Status__c=STATUS_PENDING_ADF;
        update adfVal;
		System.assertEquals(adfVal.CCL_Status__c,STATUS_PENDING_ADF);
        Test.stopTest();
    }
}
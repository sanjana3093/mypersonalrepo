/**
* @author Deloitte
* @date 07/27/2018
*
* @description This is the Test Class for Document Trigger Handler
*/

@IsTest
public class spc_DocumentTriggerTest {

	@testSetup static void setupTestdata() {
		List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
		spc_Database.ins(apexConstants);
	}

	@isTest static void testEmailDocRouting() {
		Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
		insert manAcc;
		PatientConnect__PC_Engagement_Program__c engProgram = spc_Test_Setup.createEngagementProgram('ABC EP', manAcc.Id);
		if (null != engProgram) {
			insert engProgram;
		}
		PatientConnect__PC_Document__c document = spc_Test_Setup.createDocument(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_EMAIL_INBOUND), 'Sent', engProgram.Id);
		if (null != document) {
			document.spc_to_Email_address__c = 'abc@test.com';
			spc_Incoming_Document_Routing_Mappings__c docRoutingCustSett = new spc_Incoming_Document_Routing_Mappings__c();
			docRoutingCustSett.Name = 'Org Wide Email';
			docRoutingCustSett.spc_Email_Fax_Id__c = document.spc_to_Email_address__c;
			docRoutingCustSett.spc_Engagement_Program_Code__c = engProgram.PatientConnect__PC_Program_Code__c;
			insert docRoutingCustSett;
			insert document;
		}
	}

	@isTest static void testFaxDocRouting() {
		Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
		insert manAcc;
		PatientConnect__PC_Engagement_Program__c engProgram = spc_Test_Setup.createEngagementProgram('ABC EP', manAcc.Id);
		if (null != engProgram) {
			insert engProgram;
		}
		PatientConnect__PC_Document__c document = spc_Test_Setup.createDocument(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_FAX_INBOUND), 'Sent', engProgram.Id);
		if (null != document) {
			document.PatientConnect__PC_From_Fax_Number__c = '1234567890';
			spc_Incoming_Document_Routing_Mappings__c docRoutingCustSett = new spc_Incoming_Document_Routing_Mappings__c();
			docRoutingCustSett.Name = 'Fax';
			docRoutingCustSett.spc_Email_Fax_Id__c = document.PatientConnect__PC_From_Fax_Number__c;
			docRoutingCustSett.spc_Engagement_Program_Code__c = engProgram.PatientConnect__PC_Program_Code__c;
			insert docRoutingCustSett;
			insert document;
		}
	}

	@isTest static void testChangeParentDocumentStatus() {
		Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
		insert manAcc;
		PatientConnect__PC_Engagement_Program__c engProgram = spc_Test_Setup.createEngagementProgram('ABC EP', manAcc.Id);
		if (null != engProgram) {
			insert engProgram;
		}
		PatientConnect__PC_Document__c parentDocument = spc_Test_Setup.createDocument('Email - Inbound', 'Sent', engProgram.Id);
		if (null != parentDocument) {
			parentDocument.PatientConnect__PC_Document_Status__c = 'Split in Progress';
			insert parentDocument;
			update parentDocument;
		}
		PatientConnect__PC_Document__c document = spc_Test_Setup.createDocument(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_FAX_INBOUND), 'Sent', engProgram.Id);
		if (null != document) {
			document.PatientConnect__PC_Parent_Document__c = parentDocument.Id;
			document.PatientConnect__PC_Document_Status__c = 'Split in Progress';
			insert document;
			document.PatientConnect__PC_Document_Status__c = 'Split Complete';
			update document;
		}

	}
	@isTest
	static void updateCaseonParentDoc() {
		Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
		insert manAcc;
		PatientConnect__PC_Engagement_Program__c engProgram = spc_Test_Setup.createEngagementProgram('ABC EP', manAcc.Id);
		if (null != engProgram) {
			insert engProgram;
		}
		Account patientAcc = spc_Test_Setup.createPatient('Patient Name');
		insert patientAcc;
		Case prgCase1 = spc_Test_Setup.newCase(patientAcc.Id, 'PC_Program');
		insert prgCase1;
		Case prgCase2 = spc_Test_Setup.newCase(patientAcc.Id, 'PC_Program');
		insert prgCase2;
		Case inqCase1 = spc_Test_Setup.newCase(patientAcc.Id, 'spc_Inquiry');
		insert inqCase1;
		Case inqCase2 = spc_Test_Setup.newCase(patientAcc.Id, 'spc_Inquiry');
		insert inqCase2;
		PatientConnect__PC_Document__c docNew = spc_Test_Setup.createDocument('Email - Inbound', 'Sent', engProgram.Id);
		insert docNew;
		PatientConnect__PC_Document_Log__c docLog = new PatientConnect__PC_Document_Log__c();
		docLog.PatientConnect__PC_Program__c = prgCase1.Id;
		docLog.PatientConnect__PC_Document__c = docNew.Id;
		docLog.spc_Inquiry__c = inqCase1.Id;
		insert docLog;
		docLog.PatientConnect__PC_Program__c = prgCase2.Id;
		docLog.spc_Inquiry__c = inqCase2.Id;
		update docLog;
	}
}
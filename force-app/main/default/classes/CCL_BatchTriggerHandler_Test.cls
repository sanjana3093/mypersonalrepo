/********************************************************************************************************
*  @author          Deloitte
*  @description     test class for Triggger handler class for Batch trigger
*  @param
*  @date            Aug 13, 2020
*********************************************************************************************************/
@isTest
public class CCL_BatchTriggerHandler_Test {
    @testSetup
    public static void createTestData() {
        CCL_Therapy__c commercialTherapy = CCL_TestDataFactory.createCommercialTherapy();
        Account orderingHospital= CCL_TestDataFactory.createShipToLocation();
        insert orderingHospital;

        CCL_Order__c order = new CCL_Order__c();
        order.Name='Novartis Batch ID';
        order.CCL_First_Name__c='First Name';
        order.CCL_Last_Name__c='Last Name';
        order.CCL_Middle_Name__c='N';
        order.CCL_Suffix__c='Sr.';
        order.RecordTypeId=CCL_StaticConstants_MRC.ORDER_RECORDTYPE_CLINICAL;
        order.CCL_Date_of_DOB__c='1';
        order.CCL_Month_of_DOB__c='JAN';
        order.CCL_Year_of_DOB__c='1990';
        order.CCL_Hospital_Patient_ID__c='1234';
        order.CCL_Patient_Weight__c=90;
        order.CCL_Patient_Age_At_Order__c=30;
        order.CCL_Treatment_Protocol_Subject_ID__c='CTL198088';
        order.CCL_Protocol_Center_Number__c='1234';
        order.CCL_Protocol_Subject_Number__c='123';
        order.CCL_Consent_for_Additional_Research__c=TRUE;
        order.CCL_PRF_Ordering_Status__c='PRF_Submitted';
        order.CCL_Hospital_Purchase_Order_Number__c='34834349';
        order.CCL_Physician_Attestation__c= TRUE;
        order.CCL_Patient_Eligibility__c= TRUE;
        order.CCL_VA_Patient__c= TRUE;
        order.CCL_Planned_Apheresis_Pick_up_Date_Time__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        order.CCL_Manufacturing_Slot_ID__c='123567';
        order.CCL_Value_Stream__c=89;
        order.CCL_Estimated_FP_Delivery_Date__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        order.CCL_Scheduler_Missing__c=TRUE;
        order.CCL_Returning_Patient__c=TRUE;
        order.CCL_Estimated_Manufacturing_Start_Date__c=date.newInstance(2014, 9, 15);
        order.CCL_Therapy__c=commercialTherapy.id;
        order.CCL_Ordering_Hospital__c=orderingHospital.id;
        CCL_TriggerExecutionUtility.setprocessOrderTrigger(false);
        insert order;
        //CCL_Order__c order  = CCL_TestDataFactory.createTestPRFOrder();

        CCL_Shipment__c ship = CCL_TestDataFactory.createTestFinishedProductShipment(order.Id,commercialTherapy.Id);
        ship.CCL_Hospital_Purchase_Order_Number__c = '12341234';
        CCL_TriggerExecutionUtility.setprocessShipmentTrigger(false);
        insert ship;
        if(order!=null && ship!=null) {

        CCL_Finished_Product__c fp = new CCL_Finished_Product__c();
            fp.Name ='Test FP';
            fp.CCL_Shipment__c=ship.Id;
            fp.CCL_Order__c = order.Id;
            CCL_TriggerExecutionUtility.setProcessFinishedProductTrigger(false);
            insert fp;

        //CCL_TriggerExecutionUtility.setProcessADFTrigger(false);
        CCL_Apheresis_Data_Form__c adfRecord= CCL_TestDataFactory.createTestApheresisDataForm();

        CCL_TriggerExecutionUtility.setProcessSummaryTrigger(false);
        CCL_Summary__c summaryRecord= CCL_Test_SetUp.createTestSummary('test Summary',adfRecord.id);

        CCL_Batch__c  batch = new CCL_Batch__c();
        batch.Name = 'Test Batch';
        batch.CCL_Order__c = order.Id;
        batch.CCL_Finished_Product__c =fp.Id;
        batch.CCL_Actual_Batch_Release_Date__c = Date.newInstance(2020, 08, 13);
        batch.CCL_Expiry_Date__c= Date.newInstance(2020, 08, 13);
        batch.CCL_Manufacturing_Date__c=Date.newInstance(2020, 08, 13);
        batch.CCL_Collection__c=summaryRecord.id;
        batch.CCL_Manufacturing_Run__c=summaryRecord.id;
        CCL_TriggerExecutionUtility.setProcessBatchTrigger(false);
        insert batch;
        }

    }

    static testMethod void testUpdateFields() {
        CCL_Batch__c batch  = [select Id, Name,CCL_Actual_Batch_Release_Date__c, CCL_Actual_Batch_Release_Date_Text__c,
                               CCL_Expiry_Date__c,CCL_Expiration_Date_Text__c,CCL_Manufacturing_Date__c,CCL_Manufacturing_Date_Text__c from CCL_Batch__c where Name=:'Test Batch'];
        Test.startTest();
        batch.CCL_Actual_Batch_Release_Date_Text__c = CCL_Utility.getFormattedDate(batch.CCL_Actual_Batch_Release_Date__c);
        batch.CCL_Expiration_Date_Text__c = CCL_Utility.getFormattedDate(batch.CCL_Expiry_Date__c);
        batch.CCL_Manufacturing_Date_Text__c = CCL_Utility.getFormattedDate(batch.CCL_Manufacturing_Date__c);
        Boolean b = String.isEmpty((batch.CCL_Actual_Batch_Release_Date_Text__c));
        System.assertEquals(b, FALSE, 'Either date is not fetched from query or not getting through the utility');
        Test.stopTest();

    }
    static testMethod void testNumberOfBagsShipped() {
        List<CCL_Batch__c>  batchList = new List<CCL_Batch__c>();

        List<CCL_Shipment__c> shipmentList = new  List<CCL_Shipment__c>();


        CCL_Shipment__c shipment = [SELECT Id,CCL_Number_of_Bags_Shipped__c, Name,
                                    CCL_Hospital_Purchase_Order_Number__c
                                    FROM CCL_Shipment__c
                                    WHERE CCL_Hospital_Purchase_Order_Number__c = '12341234'];

        CCL_Finished_Product__c fp = [select Id, Name from CCL_Finished_Product__c where Name='Test FP'];
        CCL_Batch__c batch  = [select Id, RecordTypeId,CCL_Finished_Product__c, CCL_Finished_Product__r.CCL_Shipment__c, Name from CCL_Batch__c where Name=:'Test Batch'];
        batchList.add(batch);
        shipmentList.add(shipment);
        update shipmentList;

         Test.startTest();
         CCL_BatchTriggerHandler cb = new CCL_BatchTriggerHandler();
         cb.updateNumberOfBagsOnShipment(batchList);

         system.assertNotEquals(5, shipmentList[0].CCL_Number_of_Bags_Shipped__c);
         Test.stopTest();


    }

}
/********************************************************************************************************
*  @author          Deloitte
*  @description     This class will support the invokable aspects of the Notification engine
*  @date            August 04 2020 
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Deloitte                  August 04 2020         Initial Version
****************************************************************************************************************/
public without sharing class CCL_Notification_Invocable {
 

    /*
    *   @author           Deloitte
    *   @description      Invoke the notification process that is driven from a Process builder    
    *   @para             List<CCL_Notification_Invocable_Wrapper>
    *   @return          
    *   @date             28 July 2020
    */
    @InvocableMethod
    public static void invokeNotificationProcess(List<CCL_Notification_Invocable_Wrapper> notificationWrprList){
		ID jobID = System.enqueueJob(new CCL_Async_Notification_Invocation(notificationWrprList));
	}
        public static void methodToBeCalledFromAsyncNotificationProcess(List<CCL_Notification_Invocable_Wrapper> notificationWrprList) {
        Map<String,CCL_Notification_DTO> notificationDTOPerUniqueKeyCombination= new Map<String,CCL_Notification_DTO>();

        Map<String,sObject> recordDetailsById= new Map<String,sObject>();
        String sitesToBeNotifiedForCurrentRecord; // This will hold API Names of fields, which has lookup relationship with Account and are part of current SObject
        String pickupSiteForCurrentRecord; // Added as part of 2369
        String dropOffSiteForCurrentRecord; // Added as part of 2369
        Set<String> uniqueSetOfSitesForCurrentRecord = new Set<string>();
        Set<String> uniqueSetOfPickUpSiteForCurrentRecord = new Set<string>(); // Added as part of 2369
        Set<String> uniqueSetOfDropOffSiteForCurrentRecord = new Set<string>(); // Added as part of 2369
        Map<String,Set<String>> sitesToBeNotifiedPerRecordID = new Map<String,Set<String>>();
        Map<String,Set<String>> pickUpSitesToBeNotifiedPerRecordID = new Map<String,Set<String>>(); // Added as part of 2369
        Map<String,Set<String>> dropOffSitesToBeNotifiedPerRecordID = new Map<String,Set<String>>(); // Added as part of 2369
        Map<String,Set<String>> therapyPerRecordID = new Map<String,Set<String>>();
        Map<String,Set<Id>> setOfAccountIdsPerUniqueCombination = new Map<String,Set<Id>>();
        Map<String,Set<Id>> setOfTherapyIdPerUniqueCombination = new Map<String,Set<Id>>();
        Map<String,Set<Id>> setOfPickUpAccountIdsPerUniqueCombination = new Map<String,Set<Id>>(); // Added as part of 2369
        Map<String,Set<Id>> setOfDropOffAccountIdsPerUniqueCombination = new Map<String,Set<Id>>(); // Added as part of 2369
        Map<String, List<sObject>> listOfAllPrefernceRecordsPerCurrentRecordId= new Map<String,List<sObject>>();
        System.debug('::wrapper list' +notificationWrprList);

        //Calling Handler method to build the notification DTO Map
        notificationDTOPerUniqueKeyCombination= CCL_Notification_Handler.buildNotificationDTO(notificationWrprList);
        if(!notificationDTOPerUniqueKeyCombination.isEmpty()) {
            System.debug('entered1'+notificationDTOPerUniqueKeyCombination);
            List<sObject> currentRecords= new List<sObject>();
            currentRecords= CCL_Notification_Handler.getCurrentRecordDetails(notificationDTOPerUniqueKeyCombination);
            //Check if record list is Blank or Not and populate the map of RecordsByID
            if(currentRecords!=null && !currentRecords.isEmpty()) {
                for(SObject obj: currentRecords) {
                    recordDetailsById.put(obj.id,obj);
                    System.debug('::invokeNotificationProcess ::methodToBeCalledFromAsyncNotificationProcess ::recordDetailsById ::Current Record '+ recordDetailsById);
                }
            }
            for(String key: notificationDTOPerUniqueKeyCombination.keySet()) {
                String recordIdKey = key.split('~')[0];
                //Populating the generic Sobject variable of DTO instance from the map of recordDetailsById
                
                notificationDTOPerUniqueKeyCombination.get(key).genericSObject = recordDetailsById.get(recordIdKey);
                System.debug('::invokeNotificationProcess ::methodToBeCalledFromAsyncNotificationProcess ::notificationDTOPerUniqueKeyCombination '+notificationDTOPerUniqueKeyCombination.get(key));
                sitesToBeNotifiedForCurrentRecord = notificationDTOPerUniqueKeyCombination.get(key).NotificationSettingMetadataRecord.CCL_Sites_To_Be_Notified__c;
                // Added as part of 2369
                pickupSiteForCurrentRecord = notificationDTOPerUniqueKeyCombination.get(key).NotificationSettingMetadataRecord.CCL_Field_To_Identify_Pickup_Location__c;
                dropOffSiteForCurrentRecord = notificationDTOPerUniqueKeyCombination.get(key).NotificationSettingMetadataRecord.CCL_Field_To_Identify_Drop_of_Location__c;
                if(!String.isBlank(sitesToBeNotifiedForCurrentRecord)) {
                    final List<String> listofSites=sitesToBeNotifiedForCurrentRecord.split(',');
                    uniqueSetOfSitesForCurrentRecord.addAll(listOfSites);
                    sitesToBeNotifiedPerRecordID.put(recordIdKey,uniqueSetOfSitesForCurrentRecord);
                    System.debug('::invokeNotificationProcess ::methodToBeCalledFromAsyncNotificationProcess ::sitesToBeNotifiedPerRecordID '+ sitesToBeNotifiedPerRecordID);
                }
                if(!String.isBlank(notificationDTOPerUniqueKeyCombination.get(key).NotificationSettingMetadataRecord.CCL_Associated_Therapy__c)) {
                    Set<String> therapySet = new Set<String>();
                    therapySet.add(notificationDTOPerUniqueKeyCombination.get(key).NotificationSettingMetadataRecord.CCL_Associated_Therapy__c);
                    therapyPerRecordID.put(recordIdKey,therapySet);
                    System.debug('::invokeNotificationProcess ::methodToBeCalledFromAsyncNotificationProcess ::therapyPerRecordID '+ therapyPerRecordID);
                }
                // Added as part of 2369 
                // Checking if Pickup Sites and Drop off sites are available then prepare a map
                if(!String.isBlank(pickupSiteForCurrentRecord)) {
                    uniqueSetOfPickUpSiteForCurrentRecord.add(pickupSiteForCurrentRecord);
                    pickUpSitesToBeNotifiedPerRecordID.put(recordIdKey,uniqueSetOfPickUpSiteForCurrentRecord);
                    System.debug('::invokeNotificationProcess ::methodToBeCalledFromAsyncNotificationProcess ::pickUpSitesToBeNotifiedPerRecordID '+ pickUpSitesToBeNotifiedPerRecordID);
                }
                if(!String.isBlank(dropOffSiteForCurrentRecord)) {
                    uniqueSetOfDropOffSiteForCurrentRecord.add(dropOffSiteForCurrentRecord);
                    dropOffSitesToBeNotifiedPerRecordID.put(recordIdKey,uniqueSetOfDropOffSiteForCurrentRecord);
                    System.debug('::invokeNotificationProcess ::methodToBeCalledFromAsyncNotificationProcess ::dropOffSitesToBeNotifiedPerRecordID '+ dropOffSitesToBeNotifiedPerRecordID);
                }
            }
            
            setOfAccountIdsPerUniqueCombination = CCL_Notifications_Utility.generateIdsFromString(sitesToBeNotifiedPerRecordID,recordDetailsById);
            setOfTherapyIdPerUniqueCombination = CCL_Notifications_Utility.generateIdsFromString(therapyPerRecordID,recordDetailsById);
            setOfPickUpAccountIdsPerUniqueCombination = CCL_Notifications_Utility.generateIdsFromString(pickUpSitesToBeNotifiedPerRecordID,recordDetailsById); // Added as part of 2369 
            setOfDropOffAccountIdsPerUniqueCombination = CCL_Notifications_Utility.generateIdsFromString(dropOffSitesToBeNotifiedPerRecordID,recordDetailsById); // Added as part of 2369 
            listOfAllPrefernceRecordsPerCurrentRecordId=fetchAllUserPreferenceRecords(setOfAccountIdsPerUniqueCombination,setOfTherapyIdPerUniqueCombination,setOfPickUpAccountIdsPerUniqueCombination,setOfDropOffAccountIdsPerUniqueCombination);
            
            for(String key: notificationDTOPerUniqueKeyCombination.keySet()) {
                String recordIdKey= key.split('~')[0];
                //Adding all the list of SObject Records Retrieved from  'fetchAllUserPreferenceRecords' method and storing it in the notification DTO Instance
                // notificationDTOPerUniqueKeyCombination.get(key).userPreferenceObjectList.addAll(listOfAllPrefernceRecordsPerCurrentRecordId.get(recordIdKey));
                if(listOfAllPrefernceRecordsPerCurrentRecordId.containsKey(recordIdKey)) {
                    for(SObject preferenceObject:listOfAllPrefernceRecordsPerCurrentRecordId.get(recordIdKey)){
                        String PreferenceToMatch = notificationDTOPerUniqueKeyCombination.get(key).NotificationSettingMetadataRecord.CCL_Notification_Type__c;
                        System.debug('::invokeNotificationProcess ::methodToBeCalledFromAsyncNotificationProcess ::PreferenceToMatch '+ JSON.serialize(PreferenceToMatch));
                        String fieldToIdentifyuserPreference = notificationDTOPerUniqueKeyCombination.get(key).NotificationSettingMetadataRecord.CCL_Field_To_Identify_User_Preference__c;
                        System.debug('::invokeNotificationProcess ::methodToBeCalledFromAsyncNotificationProcess ::fieldToIdentifyuserPreference '+ JSON.serialize(fieldToIdentifyuserPreference));
                        String userPreferences = (String)preferenceObject.get(fieldToIdentifyuserPreference);
                        
                        if(userPreferences!=null && userPreferences.contains(PreferenceToMatch)) {
                            //Adding the user Preference Object Records Retrieved from  'fetchAllUserPreferenceRecords' method and storing it in the notification DTO Instance
                            notificationDTOPerUniqueKeyCombination.get(key).userPreferenceObjectList.add(preferenceObject);
                        }
                    }
                }
            }
            notificationDTOPerUniqueKeyCombination = CCL_Notification_Handler.fetchEmailContent(notificationDTOPerUniqueKeyCombination);
            System.debug('::invokeNotificationProcess ::methodToBeCalledFromAsyncNotificationProcess ::notificationDTOPerUniqueKeyCombination '+ notificationDTOPerUniqueKeyCombination);
            createNotificationRecords(notificationDTOPerUniqueKeyCombination.values());
        }
    }
    /*
    *   @author           Deloitte
    *   @description      Creates an instance of Notification from the DTO constructed and performs an DML  
    *   @para             List<CCL_Notification_DTO> l_NotificationDTO
    *   @return           N/A
    *   @date             28 July 2020
    */
    public static void createNotificationRecords(List<CCL_Notification_DTO> l_NotificationDTO) {
        List<CCL_Notification__c> notificationRecordsToInsert = new List<CCL_Notification__c>();
        if(l_NotificationDTO!=null && l_NotificationDTO.size()>0) {
            notificationRecordsToInsert= CCL_Notification_Helper.buildNotificationRecords(l_NotificationDTO);
        }

        if(notificationRecordsToInsert != null && notificationRecordsToInsert.size()>0) {
            CCL_Notification_Accessor.insertNotifications(notificationRecordsToInsert);
        }
    }

    /*
    *   @author           Deloitte
    *   @description      Returns a map of a List of user preferences records Per current Instance Record Id    
    *   @para             Map<String,Set<Id>>, Map<String,Set<Id>> ,Map<String,Set<Id>>, Map<String,Set<Id>>
    *   @return           Map<String, List<sObject>>
    *   @date             28 July 2020
    */
    public static Map<String, List<sObject>> fetchAllUserPreferenceRecords(Map<String,Set<Id>> accountIdMap, Map<String,Set<Id>> therapyMap, Map<String,Set<Id>> pickUpAccountIdMap,Map<String,Set<Id>> dropOffAccountIdMap) {
        Map<String, List<sObject>> userTherapyAssociationPerUniqueKeyMap = new Map<String, List<sObject>>();
        Map<String, List<sObject>> userTherapyAssocPerUniqueKeyMapCountry = new Map<String, List<sObject>>();
        List<Id> accountIds = new List<Id>(); // This Account id List will store all the account associtaed to teh record including Pickup and Drop Off Account
        List<Id> therapyIds = new List<Id>(); 
        // Created a seperate Pickup Account List and DropOff List to segregate the different type of accounts coming in dynamically
        List<Id> pickUpAccountIds = new List<Id>(); // Added as part of 2369
        List<Id> dropOffAccountIds = new List<Id>(); // Added as part of 2369
        Map<String,Set<String>> setOfCountryCodesPerCurrentRecordId= new Map<String,Set<String>>();
        Map<Id, String> ShippingCountryCodePerAccountMap = new Map<Id,String>();
        Map<Id, String> ShippingCountryCodePerPickupAccountMap = new Map<Id,String>();
        Map<Id, String> ShippingCountryCodePerDropOffAccountMap = new Map<Id,String>();
        for(set<Id> accountId :accountIdMap.values()) {
            accountIds.addAll(accountId);
        }

        for(set<Id> therapyId : therapyMap.values()) {
            therapyIds.addAll(therapyId);
        }

        // Added as part of 2369
        for(set<Id> accountId :pickUpAccountIdMap.values()) {
            pickUpAccountIds.addAll(accountId);
        }
        for(set<Id> accountId :dropOffAccountIdMap.values()) {
            dropOffAccountIds.addAll(accountId);
        }

        

        if (Account.sObjectType.getDescribe().isAccessible()) {
            // This list will have accounts including pickup and Drop off Account details
            List<Account> accountRecords = CCL_Notification_Accessor.fetchAccountShippingCountryCode(accountIds);
            System.debug('::CCL_Notification_Invocable ::fetchAllUserPreferenceRecords ::accountRecords ' + accountRecords); 
            if(accountIds!= null && accountIds.size()>0) {
                for(Account acc : accountRecords) {
                    ShippingCountryCodePerAccountMap.put(acc.id,acc.ShippingCountryCode);
                }
            }
            // Added as part of 2369
            if(pickUpAccountIds!= null && pickUpAccountIds.size()>0) {
                for(Id pickUpAccountId : pickUpAccountIds) {
                    if(ShippingCountryCodePerAccountMap.containsKey(pickUpAccountId)){
                        ShippingCountryCodePerPickupAccountMap.put(pickUpAccountId,ShippingCountryCodePerAccountMap.get(pickUpAccountId));
                    }
                }
            }

            if(accountIds!= null && accountIds.size()>0) {
                for(Id dropOffAccountId : dropOffAccountIds) {
                    if(ShippingCountryCodePerAccountMap.containsKey(dropOffAccountId)){
                        ShippingCountryCodePerDropOffAccountMap.put(dropOffAccountId,ShippingCountryCodePerAccountMap.get(dropOffAccountId));
                    }
                }
            }
        }
        // preparing a map of applicableCountryCodes Per Current record Id
        for(String recordIdKey : accountIdMap.keySet()) {
            Set<String> applicableCountryCodes= new Set<String>();
            for(String accountId: ShippingCountryCodePerAccountMap.keySet()) {
                if(accountIdMap.get(recordIdKey).contains(accountId)) {
                    applicableCountryCodes.add(ShippingCountryCodePerAccountMap.get(accountId));
                    setOfCountryCodesPerCurrentRecordId.put(recordIdKey,applicableCountryCodes);
                }
            }
        }
        // Retrieve User Therapy Details  
        List<CCL_User_Therapy_Association__c> userTherapyAssociationList = CCL_Notification_Accessor.fetchUserTherapyAssociation(accountIds,therapyIds,ShippingCountryCodePerAccountMap,ShippingCountryCodePerPickupAccountMap,ShippingCountryCodePerDropOffAccountMap);
        System.debug('::CCL_Notification_Invocable ::fetchAllUserPreferenceRecords ::userTherapyAssociationList ' + userTherapyAssociationList); 
        List<CCL_User_Therapy_Association__c> userTherapyAssociationRecList;
        for(String recordIdKey : accountIdMap.keySet()) {
                userTherapyAssociationRecList = new List<CCL_User_Therapy_Association__c>(); 
            for(CCL_User_Therapy_Association__c userTherapyRec:userTherapyAssociationList) {
                    set<Id> accountIdSet = new set<Id>();
                    set<Id> therapyIdSet= new  set<Id>();
                    Set<String> setOfApplicableCountryCodes = new Set<String>();
                    
                    accountIdSet = accountIdMap.get(recordIdKey);
                    therapyIdSet = therapyMap.get(recordIdKey);
                    setOfApplicableCountryCodes= setOfCountryCodesPerCurrentRecordId.get(recordIdKey);


                if(accountIdSet.contains(userTherapyRec.CCL_Therapy_Association__r.CCL_Site__c) && therapyIdSet.contains(userTherapyRec.CCL_Therapy_Association__r.CCL_Therapy__c)) {
                    userTherapyAssociationRecList.add(userTherapyRec);
                    userTherapyAssociationPerUniqueKeyMap.put(recordIdKey,userTherapyAssociationRecList);
                } else if(setOfApplicableCountryCodes.contains(userTherapyRec.CCL_Therapy_Association__r.CCL_Country__c) && therapyIdSet.contains(userTherapyRec.CCL_Therapy_Association__r.CCL_Therapy__c)) {
                    userTherapyAssociationRecList.add(userTherapyRec);
                    userTherapyAssociationPerUniqueKeyMap.put(recordIdKey,userTherapyAssociationRecList);
                } else if(setOfApplicableCountryCodes.contains(userTherapyRec.CCL_Therapy_Association__r.CCL_Country__c) && userTherapyRec.CCL_Therapy_Association__r.CCL_Therapy__r.name.equalsIgnoreCase(CCL_Notifications_Constants.GENERIC_THERAPY_NAME)) {
                    userTherapyAssociationRecList.add(userTherapyRec);
                    userTherapyAssociationPerUniqueKeyMap.put(recordIdKey,userTherapyAssociationRecList);
                }
            }        
        }
        
    return userTherapyAssociationPerUniqueKeyMap;
    }

    /*
    *   @author           Deloitte
    *   @description      Returns a map of a List of user preferences records Per current Instance Record Id    
    *   @para             Populated on Process Builder
    *   @return           @invocableVariables : Notification Type, Current Record Id, Therapy Type, Current Object API Name
    *   @date             28 July 2020
    */
    public class CCL_Notification_Invocable_Wrapper {
        @invocableVariable(label='Notification Type' ) 

        /* Invokable variable populated on the Process builder to support the DTO creation */
        public string NotificationType;

        /* Invokable variable populated on the Process builder to support the DTO creation */
        @invocableVariable(label='Current Record Id' )
        public Id RecordId;
        
        /* Invokable variable populated on the Process builder to support the DTO creation */
        @invocableVariable(label ='Category' )
        public string Category;

        /* Invokable variable populated on the Process builder to support the DTO creation */
        @invocableVariable(label ='Current Object API Name' )
        public string objectAPIName;
    }
}
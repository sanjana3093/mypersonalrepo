/*********************************************************************************
@description Test class for spc_ProgramCoverageTriggerHandler class
@author      Deloitte
@date        June 17, 2018
*********************************************************************************/
@isTest
public class spc_ProgramCoverageTriggerHandlerTest {
    @testSetup
    static void testDataSetup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

        List<Account> accList = new List<Account>();

        Account pharmacyAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('spc_Caregiver').getRecordTypeId());
        accList.add(pharmacyAcc);

        Account pateintRec = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        pateintRec.spc_HIPAA_Consent_Received__c = 'Yes';
        pateintRec.PatientConnect__PC_Date_of_Birth__c = system.today();
        pateintRec.spc_Patient_HIPAA_Consent_Date__c = System.today();
        accList.add(pateintRec);

        if (accList != NULL) {
            spc_Database.ins(accList);
        }
        Contact pharmacyCon = new Contact();
        pharmacyCon.AccountId = accList[0].Id;
        pharmacyCon.Email = 'test@test.com';
        pharmacyCon.LastName = 'TestPharmacyContact';
        spc_Database.ins(pharmacyCon);

        Case programCaseRec = spc_Test_Setup.createCases(new List<Account> {pateintRec}, 1, 'PC_Program').get(0);
        if (programCaseRec != NULL) {
            spc_Database.ins(programCaseRec);

        }
        PatientConnect__PC_Association__c association = spc_Test_Setup.createAssociation(pharmacyAcc, programCaseRec, spc_ApexConstants.ASSOCIATION_ROLE_CAREGIVER, Date.today().addDays(1));

        List<PatientConnect__PC_Program_Coverage__c> pgrmCoverageList = new List<PatientConnect__PC_Program_Coverage__c>();
        PatientConnect__PC_Program_Coverage__c progCoverageRec1 = spc_Test_Setup.newProgCoverage(programCaseRec.Id, 'PC_PAP', 'Uninsured');
        progCoverageRec1.PatientConnect__PC_Coverage_Status__c = 'Inactive';
        progCoverageRec1.spc_Enrollment_Start_Date__c = Date.today();
        progCoverageRec1.spc_Enrollment_End_Date__c = Date.today();
        progCoverageRec1.spc_Not_Eligible__c = false;
        progCoverageRec1.spc_Eligibility_Start_Date__c = Date.today();
        progCoverageRec1.spc_Eligibility_End_Date__c = Date.today() + 1;
        progCoverageRec1.spc_Number_of_Vials__c = '1';
        pgrmCoverageList.add(progCoverageRec1);

        PatientConnect__PC_Program_Coverage__c progCoverageRec5 = spc_Test_Setup.newProgCoverage(programCaseRec.Id, 'PC_PAP', 'Uninsured');
        progCoverageRec5.PatientConnect__PC_Coverage_Status__c = 'Inactive';
        progCoverageRec5.spc_Not_Eligible__c = false;
        progCoverageRec5.spc_Eligibility_Start_Date__c = Date.today();
        progCoverageRec5.spc_Eligibility_End_Date__c = Date.today() + 1;
        progCoverageRec5.spc_Number_of_Vials__c = '1';
        pgrmCoverageList.add(progCoverageRec5);

        PatientConnect__PC_Program_Coverage__c progCoverageRec2 = spc_Test_Setup.newProgCoverage(programCaseRec.Id, 'PC_Copay_Coverage', '');
        progCoverageRec2.PatientConnect__PC_Coverage_Type__c = spc_ApexConstants.PROGRAM_COVERAGE_TYPE_ADMINCOPAY;
        progCoverageRec2.PatientConnect__PC_Coverage_Status__c = 'Inactive';
        progCoverageRec2.spc_Enrollment_Start_Date__c = Date.today();
        progCoverageRec2.spc_Enrollment_End_Date__c = Date.today();
        progCoverageRec2.spc_Not_Eligible__c = false;
        progCoverageRec2.spc_Eligibility_Start_Date__c = Date.today();
        progCoverageRec2.spc_Eligibility_End_Date__c = Date.today() + 1;
        progCoverageRec2.spc_Number_of_Vials__c = '1';
        pgrmCoverageList.add(progCoverageRec2);

        PatientConnect__PC_Program_Coverage__c progCoverageRec3 = spc_Test_Setup.newProgCoverage(programCaseRec.Id, 'PC_Copay_Coverage', '');
        progCoverageRec3.PatientConnect__PC_Coverage_Type__c = spc_ApexConstants.PROGRAM_COVERAGE_TYPE_DRUGCOPAY;
        progCoverageRec3.PatientConnect__PC_Coverage_Status__c = 'Inactive';
        progCoverageRec3.spc_Not_Eligible__c = true;

        progCoverageRec3.spc_Number_of_Vials__c = '1';
        pgrmCoverageList.add(progCoverageRec3);


        PatientConnect__PC_Program_Coverage__c progCoverageRec4 = spc_Test_Setup.newProgCoverage(programCaseRec.Id, 'PC_Private_Coverage', '');
        progCoverageRec4.PatientConnect__PC_Coverage_Status__c = 'Inactive';
        progCoverageRec4.spc_Enrollment_Start_Date__c = Date.today();
        progCoverageRec4.spc_Enrollment_End_Date__c = Date.today();
        progCoverageRec4.spc_Not_Eligible__c = false;
        progCoverageRec4.spc_Eligibility_Start_Date__c = Date.today();
        progCoverageRec4.spc_Eligibility_End_Date__c = Date.today() + 1;
        progCoverageRec4.spc_Number_of_Vials__c = '1';
        pgrmCoverageList.add(progCoverageRec4);

        spc_Database.ins(pgrmCoverageList);
        progCoverageRec5.spc_Enrollment_Start_Date__c = Date.today();
        progCoverageRec5.spc_Enrollment_End_Date__c = Date.today();

        update progCoverageRec5;
        user adminUserRec = spc_Test_Setup.getProfileID();
        insert adminUserRec;
    }
    public static testmethod Void createUpdateProgCoverageRec() {
        List<PatientConnect__PC_Program_Coverage__c> pgrmCoverageList = new List<PatientConnect__PC_Program_Coverage__c>();
        Case programCaseRec = [select id from Case LIMIT 1];
        Set<Id> caseId = new Set<Id>();
        caseId.add(programCaseRec.id);
        if (programCaseRec != NULL) {
            PatientConnect__PC_Program_Coverage__c progCoverageRec1 = spc_Test_Setup.newProgCoverage(programCaseRec.Id, 'PC_PAP', 'Uninsured');
            progCoverageRec1.spc_Not_Eligible__c = true;
            pgrmCoverageList.add(progCoverageRec1);

            PatientConnect__PC_Program_Coverage__c progCoverageRec2 = spc_Test_Setup.newProgCoverage(programCaseRec.Id, 'PC_Copay_Coverage', '');
            progCoverageRec2.PatientConnect__PC_Coverage_Type__c = spc_ApexConstants.PROGRAM_COVERAGE_TYPE_ADMINCOPAY;
            progCoverageRec2.spc_Not_Eligible__c = true;
            pgrmCoverageList.add(progCoverageRec2);

            PatientConnect__PC_Program_Coverage__c progCoverageRec3 = spc_Test_Setup.newProgCoverage(programCaseRec.Id, 'PC_Copay_Coverage', '');
            progCoverageRec3.PatientConnect__PC_Coverage_Type__c = spc_ApexConstants.PROGRAM_COVERAGE_TYPE_DRUGCOPAY;
            progCoverageRec3.spc_Not_Eligible__c = true;
            pgrmCoverageList.add(progCoverageRec3);

            programCaseRec.PatientConnect__PC_Status_Indicator_2__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_COVERAGE_ACTION_REQUIRED);

        }
        test.startTest();
        if (programCaseRec != NULL) {
            spc_Database.ins(pgrmCoverageList);

            test.stopTest();
        }
    }

    @isTest
    public static Void updateProgCoverageRec() {

        test.startTest();
        Case programCaseRec = [select id from Case LIMIT 1];
        if (programCaseRec != NULL) {
            List<PatientConnect__PC_Program_Coverage__c> pgrmCoverageList = [SELECT Id, PatientConnect__PC_Coverage_Status__c,
                                                         PatientConnect__PC_Coverage_Type__c, spc_Enrollment_Start_Date__c, PatientConnect__PC_Program_Coverage__c,
                                                         PatientConnect__PC_Program__c , spc_Not_Eligible__c, RecordTypeId FROM  PatientConnect__PC_Program_Coverage__c];

            for (PatientConnect__PC_Program_Coverage__c oPgrmCoverage : pgrmCoverageList) {
                oPgrmCoverage.PatientConnect__PC_Coverage_Status__c = 'Active';
                oPgrmCoverage.spc_Not_Eligible__c = false;
                oPgrmCoverage.spc_Eligibility_Start_Date__c = Date.today();
                oPgrmCoverage.spc_Eligibility_End_Date__c = Date.today() + 1;
            }
            spc_Database.upd(pgrmCoverageList);
        }
    }

}
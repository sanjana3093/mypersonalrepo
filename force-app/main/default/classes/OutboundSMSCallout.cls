public class OutboundSMSCallout {
    
    //needs to be @future since records needs to be committed
    @future(callout=true)
    public static void makeCallout(String jsonString) {
    
        String sessionId = null;
        String tokenId = null;
        String tokenResponse = null;
        
        //get the token
        if(Test.isRunningTest())
        {
            tokenResponse = '{"session" : {"sessionId" : "0e2ef0d0-964d-4164-a8bb-43b2af481183"},"accessToken" : {"agentUsername" : "via.manager@sagerx.com","access_token" : "094be7a9-f09e-4ef1-a49d-9d848d52e965"}}';
        }else{
            tokenResponse = aspt.ViaController.StartProcessingCallOutCampaign();                          
        }
        System.debug(LoggingLevel.INFO, 'Token response string = ' + tokenResponse);
        JSONParser parser = JSON.createParser(tokenResponse);
        while(parser.nextToken() != null){
            if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText()=='sessionId')){
                parser.nextToken();
                sessionId = parser.getText();
            }
            if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText()=='access_token')){
                parser.nextToken();
                tokenId = parser.getText();
            }
        }
        //System.debug(LoggingLevel.INFO, 'sessionId: ' + sessionId);
        //System.debug(LoggingLevel.INFO, 'tokenId: ' + tokenId);

        //get the custom settings for the constants
        System.debug(LoggingLevel.INFO, 'JSON string = ' + jsonString);
        Outbound_SMS_Settings__c smsSettings = Outbound_SMS_Settings__c.getOrgDefaults();
        //String endpoint = 'https://api.us.via.aspect-cloud.net/via/v2/organizations/sagerx/campaign/strategies/5/records';
        String endpoint = smsSettings.Webservice_URL__c;
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        
        if(Test.isRunningTest()){
           //HttpResponse response = new HttpResponse();
           response.setHeader('Content-Type', 'application/json');
           response.setBody('{"Status":"Created"}');
           response.setStatusCode(201);
        } else {
           //Http http = new Http();
           //HttpRequest request = new HttpRequest();
           request.setEndpoint(endPoint);
           request.setMethod('POST');
           request.setHeader('Content-Type', 'application/json');
           request.setHeader('Authorization', 'Bearer ' + tokenId);      
           request.setHeader('via-client-sessionId', sessionId);    
           request.setHeader('x-api-key', smsSettings.ApiKey__c);      
           request.setBody(jsonString);
           System.debug(LoggingLevel.INFO, 'About to send request to endpoint: ' + endpoint);
           //HttpResponse response = http.send(request);
           response = http.send(request);
        }
        // Parse the JSON response
        System.debug(LoggingLevel.INFO, 'HTTP response code = ' + response.getStatusCode());
        if (response.getStatusCode() != 201) {
            System.debug(LoggingLevel.ERROR, 'The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
            System.debug(response.getBody());
        } else {
            System.debug(response.getBody());
        }
    }         
}
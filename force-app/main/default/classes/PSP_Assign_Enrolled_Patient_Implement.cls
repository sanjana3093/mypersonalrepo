/*********************************************************************************************************
class Name      : PSP_PurchaseTransaction_Trigger_Impl 
Description     : Trigger Implementation Class for Purchase Transaction Object
@author         : Saurabh Tripathi
@date           : December 2, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi            December 2, 2019          Initial Version
****************************************************************************************************************/ 
public with sharing class PSP_Assign_Enrolled_Patient_Implement {
    /****** set of Id of new enrolled physicians ****/
    final Set<String> enrlldPhyscn = new Set<String>();
    /**************************************************************************************
* @author      : Saurabh Tripathi
* @date        : 12/02/2019
* @Description : Method to query and fectch all parent objects data for a purchase transaction
* @Param       : Null
* @Return      : Void
***************************************************************************************/
    public void assignPatient(List<CareProgramEnrollee> newEnrolleList,Map<Id,CareProgramEnrollee> oldEnrolleeMap) {
        If(oldEnrolleeMap == null) {
            for(CareProgramEnrollee cpEnrollee : newEnrolleList) {
                if(String.isNotBlank(cpEnrollee.PSP_Enrolled_Physician__c)) {
                    enrlldPhyscn.add(cpEnrollee.PSP_Enrolled_Physician__c);
                }
            }
        } else {
            for(CareProgramEnrollee cpEnrollee : newEnrolleList) {
                 enrlldPhyscn.add(cpEnrollee.PSP_Enrolled_Physician__c);
            }
        }
        /*****  search for the assigned patient from contact object      *******/
        fetchPatient(enrlldPhyscn,newEnrolleList);
    }
    /**************************************************************************************
* @author      : Saurabh Tripathi
* @date        : 12/02/2019
* @Description : Method to query and fectch all parent objects data for a purchase transaction
* @Param       : Null
* @Return      : Void
***************************************************************************************/
    Public void fetchPatient(Set<String> enrlldPhyscn, List<CareProgramEnrollee> newEnrolleList) {
        /****** Map to store contacts details ******/
        Map<Id,Contact> contactMap =  new Map<Id,Contact>();
        if (Contact.sObjectType.getDescribe().isAccessible()) {
            contactMap = new Map<Id,Contact>([Select Id,Name,AccountId from Contact where Id in : enrlldPhyscn]);
        }
        for(CareProgramEnrollee cpEnrollee: newEnrolleList) {
            if(String.isNotBlank(cpEnrollee.PSP_Enrolled_Physician__c) && contactMap.containsKey(cpEnrollee.PSP_Enrolled_Physician__c) && String.isNotBlank(contactMap.get(cpEnrollee.PSP_Enrolled_Physician__c).AccountId)) {
                cpEnrollee.AccountId=contactMap.get(cpEnrollee.PSP_Enrolled_Physician__c).AccountId;
            }
        }
    }
}
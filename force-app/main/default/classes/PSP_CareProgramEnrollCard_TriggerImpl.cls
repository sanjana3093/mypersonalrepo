/*********************************************************************************************************
class Name      : PSP_CareProgramEnrollCard_TriggerImpl 
Description		: Trigger Implementation Class for Care Program Enrollment Card
@author		    : Pratik Raj
@date       	: July 12, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Pratik Raj                 July 12, 2019          Initial Version
****************************************************************************************************************/
public class PSP_CareProgramEnrollCard_TriggerImpl {
    /**************************************************************************************
	* @author      : Deloitte
	* @date        : 07/12/2019
	* @Description : Method to check if the Name is unique.
	* @Param       : List of Care Program Enrollment card
	* @Return      : Void
	***************************************************************************************/
    public void uniquecheck (List<CareProgramEnrollmentCard> newPrograms) {
        //List of names
        final List<String> names = new List<String> ();
         //List of program card to check
        final List<CareProgramEnrollmentCard> prgCrdsToChk = new List<CareProgramEnrollmentCard> ();
        for (CareProgramEnrollmentCard prg : newPrograms) { 
            if (String.isNotBlank (prg.CardNumber)) {
                names.add (prg.CardNumber);
                prgCrdsToChk.add (prg);
            } 
        }
        PSP_Utility.checkUnique (Label.PSP_CareProgramEnrollmentCard,Label.PSP_Card_Number,Label.PSP_Invalid_Card_Number,names,prgCrdsToChk,null);
    }
    /**************************************************************************************
	* @author      : Deloitte
	* @date        : 07/12/2019
	* @Description : Method to check if the Name is unique.
	* @Param       : List of Care Program Enrollment card
	* @Return      : Void
	***************************************************************************************/
    public void recordType ( List<CareProgramEnrollmentCard> newPrograms) {
        //Result schema
        final Schema.DescribeSObjectResult resSchema = CareProgramEnrollmentCard.sObjectType.getDescribe ();
        //Map of record type info
        final Map<Id,Schema.RecordTypeInfo>  rTypeInfo = resSchema.getRecordTypeInfosById (); 
        for ( CareProgramEnrollmentCard card : newPrograms) {
            card.PSP_Record_Type__c = rTypeInfo.get (card.recordtypeid).getName ();
        }
    }
}
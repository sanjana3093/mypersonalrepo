/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for spc_SendBundledFaxController
    *  @date            06/18/2018
    *  @version         1.0
    *
    *  Modification Log:
    * -------------------------------------------------------------------------------------------------------
    *  Developer                Date                                    Description
    * -------------------------------------------------------------------------------------------------------
    *  soummohapatra            08/13/2018                              Initial version
*********************************************************************************************************/
@isTest
public class spc_SendBundledFaxControllerTest {

    public static Id ID_HCOACCOUNT_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Pharmacy').getRecordTypeId();

    @testSetup static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

    }
    /*********************************************************************************
    Method Name    : testGetDocFields
    Description    : Test doc fields
    Return Type    : void
    Parameter      : None
    *********************************************************************************/
    @isTest
    public static void testGetDocFields() {
        test.startTest();
        Account account = spc_Test_Setup.createPatient('Patient Test');
        Case enrollmentCase = spc_Test_Setup.newCase(account.id, 'PC_Enrollment');
        PatientConnect__PC_Document__c doc = spc_Test_Setup.createDocument('Email - Outbound', 'completed', enrollmentCase.id);
        insert doc;
        spc_SendBundledFaxController.DocumentRecord docRecord1 = new spc_SendBundledFaxController.DocumentRecord(doc, 'True');
        spc_SendBundledFaxController.DocumentRecord docRecord2 = spc_SendBundledFaxController.getDocFields();
        System.assert(docRecord2 != null);
        test.stopTest();
    }
    /*********************************************************************************
    Method Name    : testGetHCOFields
    Description    : Test HCO fields
    Return Type    : void
    Parameter      : None
    *********************************************************************************/
    @isTest
    public static void testGetHCOFields() {
        test.startTest();
        spc_SendBundledFaxController.HCORecord hcoFields = spc_SendBundledFaxController.getHCOFields();

        System.assert(hcoFields != null);
        test.stopTest();
    }
    /*********************************************************************************
    Method Name    : testGetDocumentTypes
    Description    : Test Document types
    Return Type    : void
    Parameter      : None
    *********************************************************************************/
    @isTest
    public static void testGetDocumentTypes() {
        Test.startTest();
        Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> pickLists = spc_SendBundledFaxController.getDocumentTypes();
        System.assertEquals(True, pickLists.size() > 0);
        Test.stopTest();

    }
    /*********************************************************************************
    Method Name    : testSearchRecords
    Description    : Test Document types
    Return Type    : void
    Parameter      : None
    *********************************************************************************/
    @isTest
    public static void testSearchRecords() {
        Test.startTest();
        //Caregiver account
        Account caregiverAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId());
        insert caregiverAcc1;

        //physicial account
        Account phyAcc = spc_Test_Setup.createTestAccount('PC_Physician');
        phyAcc.Name = 'Acc_Name';
        spc_Database.ins(phyAcc);

        Account account = spc_Test_Setup.createPatient('Patient Test');
        Case enrollmentCase = spc_Test_Setup.newCase(account.id, 'PC_Enrollment');
        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');
        programCase.spc_Caregiver__c = caregiverAcc1.id;
        programCase.PatientConnect__PC_Physician__c = phyAcc.id;
        insert programCase;


        Account HCOAccount = spc_Test_Setup.createTestAccount('PC_Pharmacy');
        HCOAccount.Name = 'test last';
        insert HCOAccount;

        Map<String, Object> persistedAccountMap2 = new Map<String, Object>();
        Map<String, Object> persistentHCOAccount2 = new Map<String, Object>();
        persistentHCOAccount2.put('Id', HCOAccount.id);
        Contact con = new Contact();
        con.LastName = 'test';
        con.AccountId = HCOAccount.id;
        insert con;


        //Case
        Case objCase = spc_Test_Setup.newCase(HCOAccount.Id, 'PC_Program');
        objCase.Status = 'Enrolled';
        objCase.spc_Caregiver__c = caregiverAcc1.id;
        insert objCase;

        //Document
        PatientConnect__PC_Document__c doc = spc_Test_Setup.createDocument('Email - Inbound', 'completed', enrollmentCase.id);
        insert doc;
        List<String> DocIds = new List<String>();
        DocIds.add(string.valueOf(doc.Id));

        PatientConnect__PC_Document_Log__c doclog = new PatientConnect__PC_Document_Log__c(PatientConnect__PC_Document__c = doc.Id, PatientConnect__PC_Program__c = programCase.Id);
        insert doclog;

        Attachment attach = new Attachment();
        attach.Name = 'Test';
        attach.Body = Blob.valueOf('Test');
        attach.ParentId = doc.Id;
        insert attach;

        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', account.ID);
        insert program;

        PatientConnect__PC_eLetter__c eLetter = spc_Test_Setup.createEeletter('Program', 'Fax');
        insert eLetter;

        PatientConnect__PC_Engagement_Program_Eletter__c engageELetter = spc_Test_Setup.createEProgramEletter(eLetter.Id, 'Fax', program.id);
        insert engageELetter;

        PatientConnect__PC_Association__c assoc = spc_Test_Setup.createAssociation(HCOAccount, programCase, 'Specialty Pharmacy', System.today() + 30);

        spc_SendBundledFaxController.searchRecords(programCase.Id);
        spc_SendBundledFaxController.searchRecords(String.valueOf(enrollmentCase.id));
        spc_SendBundledFaxController.retrieveHCOs(programCase.Id);
        spc_SendBundledFaxController.HCORecord hcp = new spc_SendBundledFaxController.HCORecord(HCOAccount, 'True');
        spc_SendBundledFaxController.SendMergedRecords(String.valueOf(programCase.Id), DocIds, String.valueOf(HCOAccount.ID), True, eLetter.Id);
        spc_SendBundledFaxController.SendMergedRecords(String.valueOf(programCase.Id), DocIds, '', false, '');
        spc_SendBundledFaxController.getLetters(programCase.Id, 'Case', true);
        Test.stopTest();

    }
}

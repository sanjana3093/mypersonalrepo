public with sharing class CCL_Integration_Accessor
{
    /*Fetches all the Metadata Records from the database
      and returns a map containing metadata records based on event type*/

    public static Map<String,CCL_Integration_Message_Setting__mdt> accessMetadataRecords()
    {
        List<CCL_Integration_Message_Setting__mdt> metadataList = new List<CCL_Integration_Message_Setting__mdt>();

       	Map<String,CCL_Integration_Message_Setting__mdt> integrationMetaDataMap=
            new Map<String,CCL_Integration_Message_Setting__mdt>();

         if (CCL_Integration_Message_Setting__mdt.sObjectType.getDescribe().isAccessible())
        {
            try
            {
                metadataList=[SELECT CCL_Event_Type__c, CCL_Filter_Criteria__c,
                          CCL_Object_To_Query__c,CCL_Query_String__c,CCL_Child1_Query_String__c,CCL_Child2_Query_String__c,
                          CCL_Child1_Filter_Criteria__c,CCL_Child1_Object_Name__c,
                          CCL_Child2_Filter_Criteria__c,CCL_Child2_Object_Name__c,CCL_Field_to_Identify_Critical_Lock__c,CCL_Field_to_Identify_Novartis_Batch_Id__c
                          FROM CCL_Integration_Message_Setting__mdt];
            }
            catch(Exception e)
            {
                system.debug('Some error occoured while performing soql on metadataType : '+e.getMessage());
            }



        }
        if(metadataList!= null && metadataList.size()>0)
        {
            for(CCL_Integration_Message_Setting__mdt metadataVal:metadataList)
            {
                integrationMetaDataMap.put(metadataVal.CCL_Event_Type__c,metadataVal);
            }


            return integrationMetaDataMap;

        }
	return integrationMetaDataMap;
    }
    /*Get's the List of Transaction Log Records to be created and inserts them into the database*/

    public static void createTransactionLogRecords(List<CCL_Transaction_Log__c> transactionLogRecords)
    {
         			try
                    {
                        if (transactionLogRecords!=null && transactionLogRecords.size()>0) {
                        Database.insert(transactionLogRecords);
                        //insert transactionLogRecords;
                        system.debug('Records Inserted: '+transactionLogRecords.size());
                        }
                    }

                    catch(exception e)
                    {
                        system.debug('Some error occoured while inserting Transaction Log : '+e.getMessage());
                    }
    }

}
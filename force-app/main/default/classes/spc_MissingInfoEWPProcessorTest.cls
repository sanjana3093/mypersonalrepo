/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the test class for spc_MissingInfoEWPProcessorTest
*  @date            08/07/2018
*  @version         1.0
*
*********************************************************************************************************/
@isTest
public class spc_MissingInfoEWPProcessorTest {
    public static Id ID_HCOACCOUNT_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PC_Pharmacy').getRecordTypeId();
    @testSetup static void setup() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
    }
    @isTest static void testProcessEnrollment() {
        test.startTest();
        Account phyAcc = spc_Test_Setup.createTestAccount('PC_Physician');
        phyAcc.Name = 'Acc_Name1';
        spc_Database.ins(phyAcc);
        Id ID_PATIENT_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)).getRecordTypeId();
        //Create Patient Account
        Account account = spc_Test_Setup.createAccount(ID_PATIENT_RECORDTYPE);
        account.spc_HIPAA_Consent_Received__c = 'Yes';
        account.spc_Patient_HIPAA_Consent_Date__c = system.today();
        if (null != account) {
            insert account;
        }
        //Create Enrollment Case
        Case enrollmentCase = spc_Test_Setup.newCase(account.id, 'PC_Enrollment');
        //Create Program Case
        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');
        programCase.PatientConnect__PC_Physician__c = phyAcc.id;
        if (null != programCase) {
            insert programCase;
            enrollmentCase.PatientConnect__PC_Program__c = programCase.id;
            if (null != enrollmentCase) {
                insert enrollmentCase;
            }
        }
        List<PatientConnect__PC_Association__c> AssoList = new List<PatientConnect__PC_Association__c>();
        for (PatientConnect__PC_Association__c Asso : [Select Id, PatientConnect__PC_Role__c from PatientConnect__PC_Association__c
                where PatientConnect__PC_Program__c = : programCase.Id AND PatientConnect__PC_Role__c = : spc_ApexConstants.ASSOCIATION_ROLE_TREATING_PHY]) {
            AssoList.add(Asso);
        }
        if (AssoList.size() > 0) {
            delete AssoList;
        }
        Map<String, Object> pageState = new Map<String, Object>();
        Map<String, Object> persistentMissingInfo = new Map<String, Object>();

        persistentMissingInfo.put('patientInformationVal', 'true');
        persistentMissingInfo.put('hcpInformationVal', 'true');
        persistentMissingInfo.put('hcpSignatureDateVal', 'true');
        persistentMissingInfo.put('patientHIPAAConsentVal', 'true');
        persistentMissingInfo.put('insuranceInformationVal', 'true');
        persistentMissingInfo.put('siteOfCareVal', 'true');
        persistentMissingInfo.put('rxInformationVal', 'true');
        pageState.put('missingInfo', persistentMissingInfo);
        spc_MissingInfoEWPProcessor.processEnrollment(enrollmentCase, pageState);
        Map<String, Object> pageState2 = new Map<String, Object>();
        Map<String, Object> persistentMissingInfo2 = new Map<String, Object>();
        persistentMissingInfo2.put('patientInformationVal', '');
        persistentMissingInfo2.put('hcpInformationVal', 'true');
        persistentMissingInfo2.put('hcpSignatureDateVal', 'true');
        persistentMissingInfo2.put('patientHIPAAConsentVal', 'true');
        persistentMissingInfo2.put('insuranceInformationVal', 'true');
        persistentMissingInfo2.put('siteOfCareVal', 'true');
        persistentMissingInfo2.put('rxInformationVal', 'true');
        pageState2.put('missingInfo', persistentMissingInfo2);
        spc_MissingInfoEWPProcessor.processEnrollment(enrollmentCase, pageState2);
        Map<String, Object> pageState5 = new Map<String, Object>();
        Map<String, Object> persistentMissingInfo5 = new Map<String, Object>();
        persistentMissingInfo5.put('patientInformationVal', '');
        persistentMissingInfo5.put('hcpInformationVal', '');
        persistentMissingInfo5.put('hcpSignatureDateVal', '');
        persistentMissingInfo5.put('patientHIPAAConsentVal', '');
        persistentMissingInfo5.put('insuranceInformationVal', '');
        persistentMissingInfo5.put('siteOfCareVal', '');
        persistentMissingInfo5.put('rxInformationVal', '');
        pageState5.put('missingInfo', persistentMissingInfo5);
        spc_MissingInfoEWPProcessor.processEnrollment(enrollmentCase, pageState5);
        Map<String, Object> pageState3 = new Map<String, Object>();
        Map<String, Object> persistentMissingInfo3 = new Map<String, Object>();
        persistentMissingInfo3.put('patientInformationVal', '');
        persistentMissingInfo3.put('hcpInformationVal', '');
        persistentMissingInfo3.put('hcpSignatureDateVal', 'true');
        persistentMissingInfo3.put('patientHIPAAConsentVal', 'true');
        persistentMissingInfo3.put('insuranceInformationVal', 'true');
        persistentMissingInfo3.put('siteOfCareVal', 'true');
        persistentMissingInfo3.put('rxInformationVal', 'true');
        persistentMissingInfo3.put('hipaaConsentAvailable', 'true');
        pageState3.put('missingInfo', persistentMissingInfo3);
        Id documentRecordTypeId = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_EMAIL_OUTBOUND)).getRecordTypeId();
        PatientConnect__PC_Document__c document = new PatientConnect__PC_Document__c(RecordTypeId = documentRecordTypeId);
        insert document;
        PatientConnect__PC_Document_Log__c documentLog = new PatientConnect__PC_Document_Log__c();
        documentLog.PatientConnect__PC_Program__c = enrollmentCase.id;
        documentLog.PatientConnect__PC_Document__c = document.id;
        insert documentLog;
        spc_MissingInfoEWPProcessor.processEnrollment(enrollmentCase, pageState3);


        System.assert(persistentMissingInfo.size() > 0);
        test.stopTest();
    }

    @isTest
    public static void testProcessEnrollmentforAssociation() {
        test.startTest();
        //Caregiver account
        Account caregiverAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Caregiver').getRecordTypeId());
        insert caregiverAcc1;

        //physicial account
        Account phyAcc = spc_Test_Setup.createTestAccount('PC_Physician');
        phyAcc.Name = 'Acc_Name';
        spc_Database.ins(phyAcc);
        Account account = spc_Test_Setup.createPatient('Patient Test');
        account.spc_HIPAA_Consent_Received__c = 'Yes';
        account.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf(Label.spc_test_birth_date);
        insert account;
        Account   manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        spc_Database.ins(manAcc);
        Case enrollmentCase = spc_Test_Setup.newCase(account.id, 'PC_Enrollment');
        PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('Engagement Program SageRx', manAcc.Id);
        engPrgm.PatientConnect__PC_Program_Code__c = 'SAGE';
        insert engPrgm;
        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');
        programCase.spc_Caregiver__c = caregiverAcc1.id;
        programCase.PatientConnect__PC_Physician__c = phyAcc.id;
        programCase.PatientConnect__PC_Engagement_Program__c = engPrgm.Id;
        insert programCase;
        PatientConnect__PC_Address__c addr = spc_Test_Setup.createAddress(phyAcc.Id);
        insert addr;
        enrollmentCase.PatientConnect__PC_EnrollmentWizardState__c = '{"Caregiver_Information": {"caregiverAccount": {"sobjectType": "Account","spc_Caregiver_Gender__c": ""}},"Physician": {"selectedResult": {"sobjectType": "Account","spc_Caregiver_Gender__c": "","assnPhone":"1234567779","assnFax":"13456789098","addressId":"' + addr.Id + '"}}}';
        enrollmentCase.PatientConnect__PC_Program__c = programCase.id;
        insert enrollmentCase;
        Map<String, Object> pageState = new Map<String, Object>();
        Map<String, Object> persistedAccountMap = new Map<String, Object>();

        persistedAccountMap.put('PatientConnect__PC_Caregiver_First_Name__c', 'TestProcessorFirstName');
        persistedAccountMap.put('PatientConnect__PC_Caregiver_Last_Name__c', 'TestProcessorLastName');
        persistedAccountMap.put('PatientConnect__PC_Email__c', 'TestProcessorCGFirstName@test.com');
        persistedAccountMap.put('PatientConnect__PC_Email__c', 'TestProcessorCGFirstName@test.com');
        persistedAccountMap.put('spc_Home_Phone__c', '876543212');

        pageState.put('prescription', persistedAccountMap);
        Account HCOAccount = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PC_Pharmacy').getRecordTypeId());
        HCOAccount.Name = 'test last';
        HCOAccount.recordtypeid = ID_HCOACCOUNT_RECORDTYPE;
        insert HCOAccount;
        Map<String, Object> persistedAccountMap2 = new Map<String, Object>();
        Map<String, Object> persistentHCOAccount2 = new Map<String, Object>();
        persistentHCOAccount2.put('Id', HCOAccount.id);
        Contact con = new Contact();
        con.LastName = 'test';
        con.AccountId = HCOAccount.id;
        insert con;

        //Case
        Case objCase = spc_Test_Setup.newCase(HCOAccount.Id, 'PC_Program');
        objCase.Status = 'Enrolled';
        objCase.spc_Caregiver__c = caregiverAcc1.id;
        insert objCase;

        spc_MissingInfoEWPProcessor.processEnrollment(enrollmentCase, pageState);

        System.assert(persistedAccountMap.size() > 0);
        test.stopTest();
    }
}
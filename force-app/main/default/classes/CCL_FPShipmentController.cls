/**
 * @description       : 
 * @author            : Deloitte
 * @group             : 
 * @last modified on  : 30-08-2020
 * @last modified by  : Deloitte
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   08-29-2020   Deloitte                            Initial Version
 * 1.1   08-30-2020   Deloitte                            Created shipmentToBeUpdated and fetchFPShipmentRecord refer to comments for further details.
 * 1.2   08-Dec-2020  Deloitte                            Updated checkUserPermission and removed checkUserPermissionOutboundLogistic to enable the code to be more maintainable
**/
public with sharing class CCL_FPShipmentController {

    /*
*  @author          Deloitte
*  @description     fetch current user's timezone
*  @param
*  @date            Aug 11, 2020
*/
@AuraEnabled
public static String getsUserTimeZone() {


    TimeZone tz =   UserInfo.getTimeZone();
        
    return tz.getDisplayName(); 
}

    /*
    *  @author          Deloitte
    *  @description     to check user permission of FP Receiver
    *  @param
    *  @date            August 10, 2020
    */
    @AuraEnabled(cacheable=true)
    public static CCL_Shipment__c getFinishedProductShipmentRec(Id shipmentFinishedProductRecId){
        System.debug('shipmentFinishedProductRecId &&&&& '+shipmentFinishedProductRecId);
        Id shipmentFPRecordTypeId = Schema.SObjectType.CCL_Shipment__c.getRecordTypeInfosByName().get('Finished Product').getRecordTypeId();
        CCL_Shipment__c shipmentFPRec = new CCL_Shipment__c();
        if (CCL_Shipment__c.sObjectType.getDescribe().isAccessible()) {
            shipmentFPRec = [SELECT Id, RecordTypeId, Name, CCL_Status__c,CCL_Actual_FP_Delivery_Date_Time__c,CCL_Number_of_Doses_Shipped__c,
                            CCL_Number_of_Bags_Shipped__c,CCL_Number_of_Bags_Received__c,CCL_Expiry_Date_of_Shipped_Bags__c,CCL_Product_Rejection_Reason__c,CCL_Product_Rejection_Notes__c,
                            CCL_Product_Rejection_Confirmation__c,CCL_Expiry_Date_of_Shipped_Bags_Text__c, CCL_APH_DIN_SEC_ID__c, CCL_Ship_to_Time_Zone__c
                                        FROM CCL_Shipment__c WHERE Id =:shipmentFinishedProductRecId AND 
                                        RecordTypeId =:shipmentFPRecordTypeId LIMIT 1];
             system.debug('Shipment Rec' +shipmentFPRec );
        }
        return shipmentFPRec;
    }

    /*
    *  @author          Deloitte
    *  @description     to check user permission of FP Receiver
    *  @param
    *  @date            August 10, 2020
    */
    @AuraEnabled
    public static List<String> getAphIdDINSECList(Id shipmentRec) {
        List<String> aphIdDINSECIdList = new List<String>();
        try {
            Set<String> aphIdDINSECIdSet = new Set<String>();
            Id shipmentFPRecordTypeId = CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP;
            CCL_Shipment__c shipmentFPRec = new CCL_Shipment__c();
            if (CCL_Shipment__c.sObjectType.getDescribe().isAccessible()) {
                shipmentFPRec = [SELECT Id, RecordTypeId, Name, CCL_Status__c, CCL_Apheresis_Data_Form__c, CCL_Novartis_Batch_Id__c
                                            FROM CCL_Shipment__c WHERE Id =:shipmentRec AND 
                                            RecordTypeId =:shipmentFPRecordTypeId LIMIT 1];
                
                Id collectionSummaryRecordTypeId = Schema.SObjectType.CCL_Summary__c.getRecordTypeInfosByName().get('Collection').getRecordTypeId();
                
                List<CCL_Summary__c> relatedSummaryRecList = new List<CCL_Summary__c>();
                
               
                if(CCL_Summary__c.sObjectType.getDescribe().isAccessible()){
                    
                    relatedSummaryRecList = [SELECT Id,CCL_Apheresis_Data_Form__r.CCL_Novartis_Batch_Id__c, CCL_Apheresis_ID_DIN_DEC__c,CCL_DIN__c,CCL_SEC__c 
                                                            FROM CCL_Summary__c WHERE CCL_Apheresis_Data_Form__r.CCL_Novartis_Batch_Id__c =: shipmentFPRec.CCL_Novartis_Batch_Id__c AND RecordTypeId =:collectionSummaryRecordTypeId];
                                                            
                }
                if(relatedSummaryRecList != null && !relatedSummaryRecList.isEmpty()) {
                    for(CCL_Summary__c summaryRec : relatedSummaryRecList) {
                        if(String.isNotBlank(summaryRec.CCL_Apheresis_ID_DIN_DEC__c)) {
                            aphIdDINSECIdSet.add(summaryRec.CCL_Apheresis_ID_DIN_DEC__c);
                        } else if(String.isNotBlank(summaryRec.CCL_DIN__c)) {
                            aphIdDINSECIdSet.add(summaryRec.CCL_DIN__c);
                        } else {
                            aphIdDINSECIdSet.add(summaryRec.CCL_SEC__c);
                        }                     
                    }
                }
            }
            aphIdDINSECIdList.addAll(aphIdDINSECIdSet);
        } catch (AuraHandledException ex) {
            System.debug(ex.getMessage());
               throw  ex;  
       }
        return aphIdDINSECIdList;
    }

    /*
    *  @author          Deloitte
    *  @description     to check user permission of FP Receiver
    *  @param
    *  @date            August 10, 2020
    */
    @AuraEnabled
    public static string updateShipmentRecordForRejectProcess(CCL_Shipment__c shipmentRecToUpdate){
        System.debug('shipmentRecToUpdate *** '+shipmentRecToUpdate);
        try{
            if(shipmentRecToUpdate != null){
                System.debug('shipmentRecToUpdate *** '+shipmentRecToUpdate);
                 if(Schema.sObjectType.CCL_Shipment__c.isUpdateable()) {
                    update shipmentRecToUpdate;
                  }
            }
        }catch(Exception ex){
            System.debug('Exception ex'+ex);
        }
        return 'Path for listview';
    }

    /*
    *  @author          Deloitte
    *  @description     to check user permission of FP Receiver
    *  @param
    *  @date            August 10, 2020
    */
    @AuraEnabled(cacheable=true)
    public static Boolean checkUserPermission(String permissionName) {
        try {
            return FeatureManagement.checkPermission(permissionName);
        } catch (AuraHandledException ex) {
            System.debug(ex.getMessage());
               throw  ex;  
       }
    }

    // /*
    // *  @author          Deloitte
    // *  @description     to check user permission of FP Receiver
    // *  @param
    // *  @date            August 10, 2020
    // */
    // @AuraEnabled(cacheable=true)
    // public static Boolean checkUserPermissionOutboundLogistic(String permissionName) {
    //     try {
    //         return FeatureManagement.checkPermission(permissionName);
    //     } catch (AuraHandledException ex) {
    //         System.debug(ex.getMessage());
    //            throw  ex;  
    //    }
    // }
    

    /*
    *  @author          Deloitte
    *  @description     Retrieve the Shipment for the record in question
    *  @param
    *  @date            August 10, 2020
    */
   @AuraEnabled(cacheable=true)
    public static CCL_Shipment__c getShipmentData(Id recordId) {
        System.debug('shipmentFinishedProductRecId &&&&& '+recordId);
        final Id shipmentFPRecordTypeId = Schema.SObjectType.CCL_Shipment__c.getRecordTypeInfosByName().get('Finished Product').getRecordTypeId();
         CCL_Shipment__c shipmentFPRec = new CCL_Shipment__c();
        if (CCL_Shipment__c.sObjectType.getDescribe().isAccessible()) {
            shipmentFPRec = [SELECT Id, RecordTypeId, Name, toLabel(CCL_Status__c), toLabel(CCL_Shipment_Reason__c), CCL_Secondary_Therapy__c,CCL_Infusion_Center__c, CCL_Infusion_Center__r.ShippingCity, 
                            CCL_Infusion_Center__r.ShippingStreet, CCL_Infusion_Center__r.ShippingCountry, CCL_Infusion_Center__r.ShippingState, 
                            CCL_Infusion_Center__r.ShippingPostalCode, CCL_Ship_to_Location__c,CCL_Hospital_Purchase_Order_Number__c, 
                            CCL_Estimated_FP_Delivery_Date_Time__c, CCL_Waybill_number__c, CCL_Actual_FP_Delivery_Date_Time_Text__c, CCL_Dewar_Tracking_link_1__c, CCL_Dewar_Tracking_link_2__c,
                            CCL_Product_Acceptance_Status__c, CCL_Ship_to_Location__r.ShippingCity, CCL_Ship_to_Location__r.ShippingStreet, CCL_Ship_to_Location__r.ShippingCountry, 
                            CCL_Ship_to_Location__r.ShippingState, CCL_Ship_to_Location__r.ShippingPostalCode, CCL_Manufacturing_Time_Zone__c, CCL_Ship_to_Time_Zone__c, 
                            CCL_Actual_Shipment_Date_Time_Text__c, CCL_Estimated_FP_Shipment_Date_Time_Text__c, CCL_Estimated_FP_Delivery_Date_Time_Text__c, CCL_Acceptance_Rejection_Details__c,
                            CCL_Number_of_Doses_Shipped__c, CCL_Number_of_Bags_Shipped__c, CCL_Expiry_Date_of_Shipped_Bags_Text__c, CCL_Number_of_Bags_Received__c,
                            CCL_Order__r.Id, CCL_Order__r.Name, CCL_APH_DIN_SEC_ID__c, CCL_Product_Rejection_Reason__c, CCL_Shipment_Cancellation_Reason__c, CCL_Product_Rejection_Notes__c 
                            FROM CCL_Shipment__c WHERE Id =:recordId 
                            AND RecordTypeId =:shipmentFPRecordTypeId LIMIT 1];
        }
             system.debug('Shipment Rec' +shipmentFPRec );
        return shipmentFPRec;
    }

    /*
    *  @author          Deloitte
    *  @description     fetch shipment file details
    *  @param           Id
    *  @return          List<ContentDocumentLink>
    *  @date            August 20, 2020
    */
      @AuraEnabled(cacheable=true)
    public static List<ContentDocumentLink> getUploadedFileDetails(Id recordId) {
        system.debug('RecordId::'+recordId);
        try {
                
                final List<ContentDocumentLink> finalList=new List<ContentDocumentLink>();
                final Map<String,ContentDocumentLink> mapFile = new Map<String,ContentDocumentLink>();
                List<ContentDocumentLink> fileDetails= new List<ContentDocumentLink>();
                final Set<Id> userIds=new Set<Id>();
                final Set<Id> updatedUserIds=new Set<Id>();

                
                if (ContentDocumentLink.sObjectType.getDescribe().isAccessible()) {

                    fileDetails = [SELECT Id,ContentDocumentId,ContentDocument.LatestPublishedVersionId, ContentDocument.Title,ContentDocument.FileType,ContentDocument.FileExtension,ContentDocument.CreatedById  
                                   FROM ContentDocumentLink 
                                   WHERE LinkedEntityId =:recordId WITH SECURITY_ENFORCED];
                    system.debug('fileDetails::'+fileDetails);                                       
                }
                
                for(ContentDocumentLink contentDocumentLinkInstance : fileDetails) {
                    system.debug('contentDocumentLinkInstance::'+contentDocumentLinkInstance);
                    final String key = String.valueOf(contentDocumentLinkInstance.ContentDocument.CreatedById) + String.valueOf(contentDocumentLinkInstance.ContentDocumentId);
                    userIds.add(contentDocumentLinkInstance.ContentDocument.CreatedById);
                    mapFile.put(key,contentDocumentLinkInstance);
                }
                    system.debug('userIds::'+userIds);
                    system.debug('mapFile::'+mapFile);

                updatedUserIds.addall(userIds);
                system.debug('updatedUserIds::'+updatedUserIds);
                
                for (String key : mapFile.keySet()) {
                    final ContentDocumentLink documents = mapFile.get(key);
                    if(updatedUserIds.contains(documents.ContentDocument.CreatedById)) {
                        finalList.add(documents);
                        system.debug('finalList::'+finalList);
                    }
                }
              
               return finalList;

        } catch(QueryException qe) {
            throw new AuraHandledException(qe.getMessage());
    
        }
    }
    /*
    *  @author          Deloitte
    *  @description     to fetch shipping address from Account based on Infusion Center and Ship to location.
    *  @param           String infusionId, String ShipToLocId
    *  @return          List sObject
    *  @date            11 August, 2020
    */
    @AuraEnabled(cacheable=true)
    public static List<sobject> fetchShipmentAccountDetails(String infusionCenterId, String shipToLocId) {
        List<Account> accountList = new List<Account>();
        Set<Id> accSet = new set<Id>();

        accSet.add(infusionCenterId);
        accSet.add(shipToLocId);
        
            if (Account.sObjectType.getDescribe().isAccessible()) {
            accountList = [SELECT Id,Name,ShippingAddress
                        FROM Account
                        WHERE Id in :accSet
                        WITH SECURITY_ENFORCED];
        }
        System.debug('::CCL_FPShipmentController ::fetchShipmentAccountDetails  ::accountList ' + accountList);
        
        return accountList;
    }

    @AuraEnabled
    public static String getFormattedDate(Date inputDate) {
        return CCL_Utility.getFormattedDate(inputDate);
    }
       /*
    *  @author          Deloitte
    *  @description     Call utility method to convert Date/Time Field into Novartis format
    *  @param           Date inputDate, String tmz
    *  @return          String
    *  @date            11 September, 2020
    */
    @AuraEnabled
    public static String getFormattedDateTime(Date inputDate, String tmz) {
        try {
            return CCL_Utility.getdisplayDateTimeValues(inputDate, tmz);
        } catch (AuraHandledException ex) {
            System.debug(ex.getMessage());
               throw  ex;  
       }  catch(QueryException e) {
            System.debug(e.getMessage());
            throw e;
        } 
    }
/*
*  @author          Deloitte
*  @description     to fetch shipment record
*  @param           String 
*  @date            August 29, 2020
*/
@AuraEnabled(cacheable=true)
public static list<sObject> fetchFPShipmentRecord(String shipmentId) {
    List<CCL_Shipment__c> shipmentList = new List<CCL_Shipment__c>();
    try {
        if (CCL_Shipment__c.sObjectType.getDescribe().isAccessible()) {
            shipmentList=[SELECT Id,Name, CCL_Status__c, CCL_Shipment_Cancellation_Reason__c, CCL_Order__r.CCL_Novartis_Batch_ID__c
                          FROM CCL_Shipment__c
                          WHERE Id = :shipmentId 
                          AND RecordTypeId = :CCL_StaticConstants_MRC.SHIPMENT_RECORDTYPE_FP 
                          LIMIT 1];
        }

    } catch(AuraHandledException ex) {
        System.debug(ex.getMessage());
           throw  ex;  
   } catch(QueryException e) {
        System.debug(e.getMessage());
    throw e;
    } 
    return shipmentList;
}
        
    /*
    *  @author          Deloitte
    *  @description     to fetch shipment record to update 
    *  @param
    *  @date            Aug 11, 2020
    */
    @AuraEnabled
    public static Boolean shipmentToBeUpdated(Map<String ,String> shipmentData) {
        Boolean isSuccess=false;
        try {
            if(!shipmentData.isEmpty()) {
                final CCL_Shipment__c shipmentTemp = new CCL_Shipment__c();
                shipmentTemp.Id = shipmentData.get('Id');
                shipmentTemp.CCL_Shipment_Cancellation_Reason__c = shipmentData.get('CCL_Shipment_Cancellation_Reason__c');
                shipmentTemp.CCL_Status__c = shipmentData.get('CCL_Status__c');
                if (shipmentTemp.Id !=null && CCL_Shipment__c.sObjectType.getDescribe().isAccessible() && CCL_Shipment__c.sObjectType.getDescribe().isUpdateable() &&
                    Schema.SObjectType.CCL_Shipment__c.fields.CCL_Status__c.isAccessible() && Schema.SObjectType.CCL_Shipment__c.fields.CCL_Shipment_Cancellation_Reason__c.isUpdateable()) {
                    Database.update(shipmentTemp);
                }
                isSuccess = true;
            }

            return isSuccess;
        } catch (AuraHandledException ex) {
            System.debug(ex.getMessage());
            throw ex;
        } catch(DmlException de) {
            System.debug(de.getDmlMessage(0));
            throw de;
        }
    } 
}

/*********************************************************************************************************
class Name      : PSP_AuthFormConsentTest 
Description		: Test class for Ctrigger for Auth consent Form
@author		    : Deloitte
@date       	: Nov 21, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Deloitte            Nov 21, 2019          Initial Version
****************************************************************************************************************/ 
@IsTest
public class PSP_AuthFormConsentTest {
    /* Apex constants */
    static final List<PSP_ApexConstantsSetting__c>  APEX_CONSTANTS =  PSP_Test_Setup.setDataforApexConstants();
    /* User name */
    final Static String TEST_USER = 'Test';
    /* source type */
    final Static String EMAIL = 'Email';
    /* source type */
    final Static String PHONE = 'Phone';
     /* source type call center */
    final Static String CALL_CENTER = 'Call Center';
    /*************************************************************************************
* @author      : Deloitte
* @date        : 09/17/2019
* @Description : Testing insert scenarios
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    public static testMethod void testRunAs () {
        Test.startTest();
        final Account patient=PSP_Test_Setup.getPersonAccount('test patient');
        Database.insert(patient);
        final AuthorizationForm authForm=new AuthorizationForm();
        authForm.Name=TEST_USER;
        authForm.EffectiveFromDate=system.today();
        Database.insert(authForm);
        final AuthorizationFormText authFormTxt=new AuthorizationFormText();
        authFormTxt.Name=TEST_USER;
        authFormTxt.AuthorizationFormId=authForm.Id;
        authFormTxt.SummaryAuthFormText=TEST_USER;
        Database.insert(authFormTxt);
        final AuthorizationFormConsent authFormConsent=new AuthorizationFormConsent();
        authFormConsent.Name=TEST_USER;
        authFormConsent.ConsentGiverId=patient.Id;
        authFormConsent.ConsentCapturedSourceType=EMAIL;
        authFormConsent.AuthorizationFormTextId=authFormTxt.Id;
        authFormConsent.ConsentCapturedSource=CALL_CENTER;
        authFormConsent.ConsentCapturedDateTime=system.today();
        Test.stopTest();
        
        try {
        Database.insert(authFormConsent);
        authFormConsent.ConsentCapturedSourceType=PHONE;
        Database.update(authFormConsent);
            
        } catch (DmlException e) {
            System.assert ( e.getMessage ().contains ('Insert failed.'),e.getMessage () );
        }
    }
    
}
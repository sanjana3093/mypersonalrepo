/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for spc_MissingInfoEWPControllerTest
    *  @date            08/07/2018
    *  @version         1.0
    *
*********************************************************************************************************/
@isTest
public class spc_MissingInfoEWPControllerTest {
    @testSetup static void setupTestdata() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
    }
    /*********************************************************************************
    Method Name    : testGetPicklistEntryMap
    Description    : Test pocklist entry map
    Return Type    : void
    Parameter      : None
    *********************************************************************************/
    @isTest
    public static void testGetPicklistEntryMap() {
        Test.startTest();
        Map<String, List<spc_PickListFieldManager.PicklistEntryWrapper>> pickLists = spc_MissingInfoEWPController.getPicklistEntryMap();
        System.assertEquals(True, pickLists.size() > 0);
        Test.stopTest();

    }
    @isTest
    static void getHIPAAConsentTest() {
        Account acc = spc_Test_Setup.createTestAccount('PC_Patient');
        acc.name = 'patient';
        acc.PatientConnect__PC_Email__c = 'test@email.com';
        acc.PatientConnect__PC_Date_of_Birth__c = system.today() - 20;
        acc.spc_HIPAA_Consent_Received__c = 'No';
        acc.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf(Label.spc_test_birth_date);
        insert acc;
        Case enrlCase = spc_Test_Setup.newCase(acc.Id, 'PC_Enrollment');
        enrlCase.PatientConnect__PC_EnrollmentWizardState__c = '{"Patient_Information":{"persistentAccount":{"sobjectType":"Account","salutation":"__none","firstName":"Dexter","middleName":"","lastName":"Johns","gender":"__none","weight":"","dob":"2018-08-01","patientHIPAConsent":"No","patientMarketingServicesConsent":"","patientTextConsent":"__none","patientPHIConsent":"","homePhone":"","mobilePhone":"","officePhone":"","preferredTimeToContact":"__none","childName":"","childDOB":"","childGender":"__none","spc_Patient_Service_Consent_Date__c":"","spc_Patient_Marketing_Consent_Date__c":"","spc_Patient_Text_Consent_Date__c":"","spc_Patient_HIPAA_Consent_Date__c":"","patientMarketingConsent":"__none","patientServicesConsent":"__none","preferredCommunicationLanguage":"__none"},"persistentAddress":[]}}';
        insert enrlCase;
        Test.startTest();
        boolean hipaaConsentMissing = false;
        String caseid = enrlCase.Id;
        spc_MissingInfoEWPController.getHIPAAConsent(caseid);
		Account acc2 = spc_Test_Setup.createTestAccount('PC_Patient');
        acc2.name='patient';
        acc2.PatientConnect__PC_Email__c = 'test2@email.com';
        acc2.PatientConnect__PC_Date_of_Birth__c = system.today()-10;
        acc2.spc_HIPAA_Consent_Received__c = 'Yes';
        acc2.spc_Patient_HIPAA_Consent_Date__c=Date.valueOf(Label.spc_test_birth_date);
        insert acc2;
        Case enrlCase2 = spc_Test_Setup.newCase(acc2.Id, 'PC_Enrollment');
        enrlCase2.PatientConnect__PC_EnrollmentWizardState__c = '{"Patient_Information":{"persistentAccount":{"sobjectType":"Account","salutation":"__none","firstName":"Dexterr","middleName":"","lastNamee":"Johns","gender":"__none","weight":"","dob":"2018-08-02","patientHIPAConsent":"Yes","patientMarketingServicesConsent":"","patientTextConsent":"__none","patientPHIConsent":"","homePhone":"","mobilePhone":"","officePhone":"","preferredTimeToContact":"__none","childName":"","childDOB":"","childGender":"__none","spc_Patient_Service_Consent_Date__c":"","spc_Patient_Marketing_Consent_Date__c":"","spc_Patient_Text_Consent_Date__c":"","spc_Patient_HIPAA_Consent_Date__c":"","patientMarketingConsent":"__none","patientServicesConsent":"__none","preferredCommunicationLanguage":"__none"},"persistentAddress":[]}}';
		insert enrlCase2;
        String caseid2 = enrlCase2.Id;
        spc_MissingInfoEWPController.getHIPAAConsent(caseid2);
        Test.stopTest();
    }
}
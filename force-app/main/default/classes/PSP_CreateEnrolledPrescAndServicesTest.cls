/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the test class for PSP_CreateEnrolledPrescAndServices
*  @date            10/16/2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Shourya Solipuram           October 16, 2019          Initial Version
****************************************************************************************************************/
@isTest
private class PSP_CreateEnrolledPrescAndServicesTest {
    /*Constant for Test */
    private static final String TEST_USER = 'Test';
    /*Apex Constants */
    static final List<PSP_ApexConstantsSetting__c>  APEX_CONSTANTS =  PSP_Test_Setup.setDataforApexConstants();
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Test Method for createEnrolledPrescAndServices
    *  @date            October 16, 2019
    *  @version         1.0
    *********************************************************************************************************/
    @isTest static void testForCreateEnrolledPrescAndServices () {
        /*Test setup method being instantiated */
        Test.startTest();
        final CareProgram cP1 = PSP_Test_Setup.createTestCareProgram (TEST_USER,null);
        cP1.PSP_Program_Sector__c = 'Public';
        Database.insert(cP1);
        /*Test setup method being instantiated */
        final Product2 pro = PSP_Test_Setup.createProduct (TEST_USER); 
        Database.insert (pro);
        /*Test setup method being instantiated */
        final CareProgramEnrollee cpe = PSP_Test_Setup.createTestCareProgramEnrollment (TEST_USER,cP1.Id); 
        Database.insert (cpe);
        /*Test setup method being instantiated */
        final CareProgramProduct cpp = PSP_Test_Setup.createTestCareProgramProduct (cP1,pro);
        cpp.PSP_Default__c = true;
        cpp.CareProgramId = cP1.Id;
        Database.insert (cpp); 
        PSP_CreateEnrolledPrescAndServices.createEnrolledPrescAndServices(cpe.Id,cP1.Id,TEST_USER);
        Test.stopTest();
        System.assert(cpp.PSP_Default__c, 'Checking default value');
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Test method for checking whether the logged-in user has the create permission for Account
    *  @date            October 16, 2019
    *  @version         1.0
    *********************************************************************************************************/
    @isTest static void testForCheckAccess () { 
      final boolean assertCheck=  PSP_CreateEnrolledPrescAndServices.checkAccess();
      System.assert(assertCheck, 'Checking default value');        
    }

}
/*********************************************************************************

    *  @author          Deloitte
    *  @description     Bundled Fax Controller
    *  @date            4-Jan-2017
    *  @version         1.0

 */
public without sharing class spc_SendBundledFaxController {

    private static string Flowname_BundleFax = 'Bundled Fax Cover Letter';
    private static string DocumentType_Enrollment = 'Enrollment Form';
    private static string DocumentType_Confidentiality = 'Confidentiality Form';
    private static string DocumentType_Consent = 'Patient Consent';
    private static string DocumentType_Prescription = 'Prescription';
    private static string DocumentType_Insurance = 'Insurance Information';
    private static string DocumentType_Summary = 'Summary of Benefits';
    private static string DocumentType_Copay = 'Copay Information';
    private static string DocumentType_Authorization = 'Prior Authorization';
    private static string DocumentType_Other = 'Other';
    //comment

    /********************************************************************************************************
    *  @author         Deloitte
    *  @date             24/10/2018
    *  @description      To get Document Record
    *  @return          Document Record
    *********************************************************************************************************/
    @AuraEnabled
    public static DocumentRecord getDocFields() {

        DocumentRecord displayFields = new DocumentRecord();

        displayFields.name = PatientConnect__PC_Document__c.Name.getDescribe().getLabel();
        displayFields.recordTypeName = PatientConnect__PC_Document__c.faxType__c.getDescribe().getLabel();
        displayFields.categoryName = PatientConnect__PC_Document__c.PatientConnect__PC_Document_Category__c.getDescribe().getLabel();
        displayFields.status = PatientConnect__PC_Document__c.PatientConnect__PC_Document_Status__c.getDescribe().getLabel();

        return displayFields;
    }
    /********************************************************************************************************
    *  @author         Deloitte
    *  @date             24/10/2018
    *  @description      To get HCO Fields
    *  @return          HCO Record
    *********************************************************************************************************/
    @AuraEnabled
    public static HCORecord getHCOFields() {

        HCORecord displayFields = new HCORecord();

        displayFields.type = Account.Type.getDescribe().getLabel();
        displayFields.recordTypeId = Account.RecordTypeId.getDescribe().getLabel();
        displayFields.name = Account.Name.getDescribe().getLabel();
        displayFields.recordTypeName = Account.RecordType.Name.getDescribe().getLabel();
        displayFields.subType = Account.PatientConnect__PC_Sub_Type__c.getDescribe().getLabel();

        displayFields.primaryAddress1 = Account.PatientConnect__PC_Primary_Address_1__c.getDescribe().getLabel();
        displayFields.primaryCity = Account.PatientConnect__PC_Primary_City__c.getDescribe().getLabel();
        displayFields.primaryState = Account.PatientConnect__PC_Primary_State__c.getDescribe().getLabel();
        displayFields.primaryZipCode = Account.PatientConnect__PC_Primary_Zip_Code__c.getDescribe().getLabel();
        displayFields.status = Account.PatientConnect__PC_Status__c.getDescribe().getLabel();

        return displayFields;
    }

    /********************************************************************************************************
     *  @author           Deloitte
     *  @date             24/10/2018
     *  @description      To get Document Types
     *  @return          Map of Document Type picklist
     *********************************************************************************************************/
    @AuraEnabled
    public static Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> getDocumentTypes () {
        Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> picklistEntryMap = new Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>>();
        picklistEntryMap.put('Type', spc_PicklistFieldManager.getPicklistEntryWrappers(PatientConnect__PC_Document__c.PatientConnect__PC_Document_Category__c));
        return picklistEntryMap;
    }

    /********************************************************************************************************
    *  @author           Deloitte
    *  @date             24/10/2018
    *  @description      To get Document Records
    *  @return          List of Document Records
    *********************************************************************************************************/
    @AuraEnabled
    public static List<DocumentRecord> searchRecords(String searchString) {
        try {
            List<DocumentRecord> records = new List<DocumentRecord>();
            List<PatientConnect__PC_Document__c> docs = getDocuments(searchString);
            DocumentRecord record;

            for (PatientConnect__PC_Document__c a : docs) {
                record = new DocumentRecord(a, 'false');
                records.add(record);
            }
            return records;
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
            return null;
        }
    }

    /********************************************************************************************************
    *  @author           Deloitte
    *  @date             24/10/2018
    *  @description      To get HCo Records
    *  @return          List of HCO Records
    *********************************************************************************************************/
    @AuraEnabled
    public static List<HCORecord> retrieveHCOs(String caseId) {
        try {
            List<HCORecord> records = new List<HCORecord>();
            List<String> lstRolesTypes = new List<string>();
            string hcoRole = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO);
            lstRolesTypes.add(hcoRole);
            string sPharmaRole = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_SPHARMACY);
            lstRolesTypes.add(sPharmaRole);
            string PharmaRole = spc_ApexConstants.ASSOCIATION_ROLE_PHARMACY;
            lstRolesTypes.add(PharmaRole);
            string freeDrugVendorRole = spc_ApexConstants.ASSOCIATION_ROLE_FREE_DRUG_VENDOR;
            lstRolesTypes.add(freeDrugVendorRole);
            string activeStatus = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE);
            List<PatientConnect__PC_Association__c>  relatedHCOs = [SELECT
                    Id,
                    PatientConnect__PC_Account__r.Name,
                    PatientConnect__PC_Account__r.RecordTypeId,
                    PatientConnect__PC_Account__r.RecordType.Name,
                    PatientConnect__PC_Account__r.PatientConnect__PC_Primary_Address_1__c,
                    PatientConnect__PC_Account__r.PatientConnect__PC_Primary_City__c,
                    PatientConnect__PC_Account__r.PatientConnect__PC_Primary_State__c,
                    PatientConnect__PC_Account__r.PatientConnect__PC_Primary_Zip_Code__c,
                    PatientConnect__PC_Account__r.PatientConnect__PC_Status__c,
                    PatientConnect__PC_Account__r.Type,
                    PatientConnect__PC_Account__r.PatientConnect__PC_Sub_Type__c
                    FROM  PatientConnect__PC_Association__c
                    WHERE PatientConnect__PC_Program__c = :caseId
                            AND PatientConnect__PC_Role__c in : lstRolesTypes
                            AND PatientConnect__PC_AssociationStatus__c = : activeStatus];

            HCORecord record;

            for (PatientConnect__PC_Association__c a : relatedHCOs) {
                record = new HCORecord(a.PatientConnect__PC_Account__r, 'false');
                records.add(record);
            }

            return records;
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
            return null;
        }
    }
    /********************************************************************************************************
    *  @author           Deloitte
    *  @date             24/10/2018
    *  @description      To send Merged Records
    *  @return          None
    *********************************************************************************************************/

    @AuraEnabled
    public static void SendMergedRecords(String recordId, String[] documentIds, String HCOID, boolean sendToLash, String letterId) {
        try {
            String flow = '';
            PatientConnect__PC_eLetter__c eLetter;
            Set<String> attachmentIds = new Set<String>();
            Map<Id, Set<String>> MapDocIDToAccIds = new Map<Id, Set<String>>();
            Set<String> pmrcs = new Set<String>();
            List<PatientConnect__PC_Document__c> docs = [
                        SELECT Id, spc_PMRC_Code_New__c, PatientConnect__PC_Attachment_Id__c FROM
                        PatientConnect__PC_Document__c WHERE Id IN : documentIds
                    ];
            for (Attachment att : [SELECT Id, Name, ParentId, Parent.Type FROM Attachment where Parent.Type = 'PatientConnect__PC_Document__c'
                                   AND ParentId IN: documentIds
                                  ]) {
                Set<String> AttId = new Set<String>();
                if (MapDocIDToAccIds.containsKey(att.ParentId)) {
                    AttId = MapDocIDToAccIds.get(att.ParentId);
                }
                AttId.add(att.Id);
                MapDocIDToAccIds.put(att.ParentId, AttId);
            }
            if (String.isNotBlank(letterId) && Label.PatientConnect.PC_None != letterId) {
                eLetter = spc_eLetterService.getELetter(letterId);
                List<PatientConnect__PC_Engagement_Program_Eletter__c> eletterlst = [
                            SELECT Id, spc_PMRC_Code__c FROM
                            PatientConnect__PC_Engagement_Program_Eletter__c WHERE PatientConnect__PC_eLetter__c = : letterId LIMIT 1
                        ];

                for (PatientConnect__PC_Engagement_Program_Eletter__c eltr : eletterlst) {
                    pmrcs.add(spc_Utility.getActualPMRCCode(eltr.spc_PMRC_Code__c));
                }
                for (spc_Communication_Framework__mdt letterCode : [Select Flow_Name__c from spc_Communication_Framework__mdt where Fax_PMRC_Code__c  in :pmrcs LIMIT 1]) {
                    flow = letterCode.Flow_Name__c;
                }
            }
            for (PatientConnect__PC_Document__c curDoc : docs) {
                if (MapDocIDToAccIds.containsKey(curDoc.Id)) {
                    attachmentIds.addAll(MapDocIDToAccIds.get(curDoc.Id));
                }
                if (String.isBlank(letterId)) {
                    pmrcs.add(curDoc.spc_PMRC_Code_New__c);
                }
            }
            List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();
            spc_CommunicationMgr.Envelop env1 = new spc_CommunicationMgr.Envelop();
            env1.channel = spc_CommunicationMgr.Channel.FAX;
            if (sendToLash) {
                if (String.isBlank(HCOID)) {
                    env1.recipientTypes = new Set<String> {Label.spc_Lash};
                } else {
                    env1.recipientTypes = new Set<String> {Label.spc_Lash, HCOID};
                }
            } else {
                env1.recipientTypes = new Set<String> {HCOID};
            }

            if (String.isBlank(letterId) || Label.PatientConnect.PC_None == letterId) {
                env1.FlowName = Flowname_BundleFax;
            } else {
                env1.FlowName = flow;
                if (null != eLetter) {
                    env1.eLetterName = eLetter.Name;
                }
                env1.pmrcs = pmrcs;
            }
            env1.programCaseId = recordId;
            env1.attachmentIds = attachmentIds;

            envelops.add(env1);
            if (!envelops.isEmpty()) {
                spc_CommunicationMgr obj = new spc_CommunicationMgr();
                obj.send(envelops);
            }
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }
    }
    /********************************************************************************************************
     *  @author           Deloitte
     *  @date             24/10/2018
     *  @description      To get Document Records
     *  @return          List of Documents
     *********************************************************************************************************/
    public static List<PatientConnect__PC_Document__c> getDocuments(String caseId) {
        List<PatientConnect__PC_Document_Log__c> relatedDocLogs = [SELECT Id,
                                                 PatientConnect__PC_Program__c,
                                                 PatientConnect__PC_Document__c
                                                 FROM PatientConnect__PC_Document_Log__c
                                                 WHERE PatientConnect__PC_Program__c = :caseId];

        List<String> docIds = new List<String>();
        String faxinbound = spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_FAX_INBOUND);
        String emailinbound = spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_EMAIL_INBOUND);
        String smsinbound = spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.DOC_RT_SMS_INBOUND);
        String manual = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_MANUAL_UPLOAD);
        List<String> DOCUMENT_INBOUNDRT = new List<String> {faxinbound, emailinbound, smsinbound, manual};
        for (PatientConnect__PC_Document_Log__c docLog : relatedDocLogs ) {
            docIds.add(docLog.PatientConnect__PC_Document__c);
        }

        List<PatientConnect__PC_Document__c> relatedDoc = [SELECT Id,
                                             Name,
                                             RecordType.Name,
                                             PatientConnect__PC_Document_Status__c,
                                             PatientConnect__PC_Document_Category__c
                                             FROM PatientConnect__PC_Document__c
                                             WHERE Id IN :docIds AND
                                             RecordType.Name IN :DOCUMENT_INBOUNDRT ];
        return relatedDoc;
    }

    public class DocumentRecord {
        @AuraEnabled public String Id { get; set; }
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public String recordTypeName { get; set; }
        @AuraEnabled public String categoryName { get; set; }

        @AuraEnabled public String isSelected { get; set; }
        @AuraEnabled public String status { get; set; }

        @AuraEnabled public String cssClassName { get; set; } // used in front end.

        public DocumentRecord() {

        }

        public DocumentRecord(PatientConnect__PC_Document__c a, String isSelect) {
            Id = a.Id;
            name = a.Name;
            recordTypeName = a.RecordType.Name;
            categoryName = a.PatientConnect__PC_Document_Category__c;
            isSelected = isSelect;
            status = a.PatientConnect__PC_Document_Status__c;

            cssClassName = '';
        }

    }

    public class HCORecord {
        @AuraEnabled public String Id { get; set; }
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public String type { get; set; }
        @AuraEnabled public String subType { get; set; }
        @AuraEnabled public String recordTypeId { get; set; }
        @AuraEnabled public String recordTypeName { get; set; }

        @AuraEnabled public String primaryAddress1 { get; set; }
        @AuraEnabled public String primaryAddress2 { get; set; }
        @AuraEnabled public String primaryAddress3 { get; set; }
        @AuraEnabled public String primaryAddress { get; set; }
        @AuraEnabled public String primaryCity { get; set; }
        @AuraEnabled public String primaryState { get; set; }
        @AuraEnabled public String primaryZipCode { get; set; }

        @AuraEnabled public String email { get; set; }
        @AuraEnabled public String phone { get; set; }
        @AuraEnabled public String fax { get; set; }

        @AuraEnabled public String isSelected { get; set; }
        @AuraEnabled public String status { get; set; }
        @AuraEnabled public String lastModifiedDate { get; set; }
        @AuraEnabled public Datetime lastModifiedDateInUserTZ { get; set; }

        @AuraEnabled public String cssClassName { get; set; } // used in front end.

        public HCORecord() {

        }

        public HCORecord(Account a, String isSelect) {
            Id = a.Id;
            type = a.Type;
            recordTypeId = a.RecordTypeId;
            name = a.Name;
            recordTypeName = a.RecordType.Name;
            subType = a.PatientConnect__PC_Sub_Type__c;

            primaryAddress1 = a.PatientConnect__PC_Primary_Address_1__c;
            primaryCity = a.PatientConnect__PC_Primary_City__c;
            primaryState = a.PatientConnect__PC_Primary_State__c;
            primaryZipCode = a.PatientConnect__PC_Primary_Zip_Code__c;

            isSelected = isSelect;
            status = a.PatientConnect__PC_Status__c;
            cssClassName = '';
        }

    }

    /********************************************************************************************************
    *  @author           Deloitte
    *  @date             24/10/2018
    *  @description      Retrieves available eLetters by object type
    *  @return           List<PC_LightningMap>
    *********************************************************************************/
    @AuraEnabled
    public static List<spc_LightningMap> getLetters(Id recordId, String invokeAction, boolean isFaxOnly) {

        List<spc_LightningMap> lstAvailableeLetters = new List<spc_LightningMap>();
        return lstAvailableeLetters = spc_eLetterAuraController.getLetters(recordId, invokeAction, isFaxOnly);

    }

}
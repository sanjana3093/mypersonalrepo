/********************************************************************************************************
*  @author          Deloitte
*  @description     Language Therapy Trigger Handler Class to populate the CCL_Unique_Combination__c Field.
*  @param           
*  @date            Dec 1, 2020
*********************************************************************************************************/
public class CCL_LanguageTherapyTriggerHandler {
      /* This method updates the unique combination field */
    	public void updateUniqueCombinationforDuplicationRule(List<CCL_Language_Therapy_Association__c> languageTherapList) {
        
         for(CCL_Language_Therapy_Association__c languageTh :languageTherapList) {
                           
             if(!String.isBlank(languageTh.CCL_Language__c) && !String.isBlank(languageTh.CCL_Therapy_Association__c) ){               
              languageTh.CCL_Unique_Combination__c  = languageTh.CCL_Language__c +''+ languageTh.CCL_Therapy_Association__c;

                 
              }
       	}    
    }

}
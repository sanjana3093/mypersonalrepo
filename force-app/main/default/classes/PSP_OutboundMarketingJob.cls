/*********************************************************************************************************
class Name      : OutboundMarketingJob
Description     : Schedulable class for the batch class PSP_ApplyTemplateImpl
@author         : Divya Eduvulapati
@date           : December 20, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Divya Eduvulapati          December 20, 2019       Initial Version
****************************************************************************************************************/
global with sharing class PSP_OutboundMarketingJob implements Schedulable {
    global void execute(SchedulableContext SC) {
      //PSP_ApplyTemplateImpl ApplyTemplate = new PSP_ApplyTemplateImpl(); 
   }
}
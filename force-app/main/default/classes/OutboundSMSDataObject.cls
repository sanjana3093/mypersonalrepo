public class OutboundSMSDataObject {
  
    public OutboundSMSDataObject(){
        infusionDate = Date.today(); 
    }
        
    public String phoneNumber {get; set;}
    public String firstName {get;set;}
    public String taskID {get;set;}
    public String templateName {get;set;}
    public String zipCode {get;set;}
    public Date infusionDate {set;get;}
    public String infusionTime {get;set;}
    public String socName {get;set;}
    public Integer messagePart = 1; 
    public String smsTextTemplate {get;set;}
    public String customer {get;set;}
    private Integer numParts = 2;
    
    public String getInfusionDateAsString(){
        
        String infusionDateString = null;
        
        DateTime systemDateTime = DateTime.newInstance(infusionDate.year(), infusionDate.month(), infusionDate.day());
        infusionDateString = systemDateTime.format('MM/dd/yyyy');
        
        return infusionDateString;
    }
    
    public override String toString(){
        String objectvalues = 'OutboundSMSDataObject values:  Phone Number ['+phoneNumber+'], First Name ['+firstName+'], Task ID ['+taskID+'] '+
            'Template Name ['+templateName+'], Zip Code ['+zipCode+'], Infusion Date ['+infusionDate+'], Infusion Time ['+infusionTime+'] '+
            'SOC Name ['+socName+'], Message Part ['+messagePart+'], SMS Text Template ['+smsTextTemplate+'], Customer ['+customer+']';
    
      return objectValues;
    }
    
    public String toJSONString(){
        
        JSONGenerator jGen = JSON.createGenerator(true);
        jGen.writeStartObject();
        jGen.writeFieldName('records');
        jGen.writeStartArray();
        jGen.writeStartObject();
        jGen.writeFieldName('items');
        jGen.writeStartArray();
        
        jGen.writeStartObject();
        jGen.writeStringField('field', '@NUMBER');
        if(phoneNumber != null && phoneNumber.length()==10) phoneNumber='1'+phoneNumber;
        jGen.writeStringField('value', replaceNull(phoneNumber, 'NA'));
        jGen.writeEndObject();
        
        jGen.writeStartObject();
        jGen.writeStringField('field', 'TaskID');
        jGen.writeStringField('value', replaceNull(taskID, 'None'));
        jGen.writeEndObject();
  
        jGen.writeStartObject();
        jGen.writeStringField('field', 'FirstName');
        jGen.writeStringField('value', replaceNull(firstName, 'None'));
        jGen.writeEndObject();

      	jGen.writeStartObject();
        jGen.writeStringField('field', 'TemplateName');
        jGen.writeStringField('value', replaceNull(templateName, 'None'));
        jGen.writeEndObject();
 
        jGen.writeStartObject();
        jGen.writeStringField('field', 'InfusionDate');
        jGen.writeStringField('value', getInfusionDateAsString());
        jGen.writeEndObject();        

        jGen.writeStartObject();
        jGen.writeStringField('field', 'MessagePart');
        jGen.writeStringField('value', String.valueOf(messagePart));
        jGen.writeEndObject();
         /*  
           
        jGen.writeStartObject();
        jGen.writeStringField('field', 'InfusionTime');
        jGen.writeStringField('value', infusionTime);
        jGen.writeEndObject();
     
        jGen.writeStartObject();
        jGen.writeStringField('field', '@POSTALACT');
        jGen.writeStringField('value', replaceNull(zipCode, 'None'));
        jGen.writeEndObject();

        jGen.writeStartObject();
        jGen.writeStringField('field', '@ACCOUNT');
        jGen.writeStringField('value', replaceNull(taskID,'') + String.valueOf(messagePart));
        jGen.writeEndObject();

        jGen.writeStartObject();
        jGen.writeStringField('field', 'SOCName');
        jGen.writeStringField('value', replaceNull(socName,'None'));
        jGen.writeEndObject();

        jGen.writeStartObject();
        jGen.writeStringField('field', '@SMSTEXTTEMPLATE');
        jGen.writeStringField('value', replaceNull(smsTextTemplate, 'NA'));
        jGen.writeEndObject();

        jGen.writeStartObject();
        jGen.writeStringField('field', '@CUSTOMER');
        jGen.writeStringField('value', replaceNull(customer,'NA'));
        jGen.writeEndObject();
        */
        
        jGen.writeEndArray();
        jGen.writeEndObject();
        jGen.writeEndArray();
        jGen.writeEndObject();
        
        System.debug('JSON string created: '+jGen.getAsString());
        return jGen.getAsString();
    }

    public void setMessagePart(Integer value){       
        messagePart = value;
    }
    
    public Integer getMessageParts(){
       
        if(templateName != null && (templateName.contains('HIPAA') || templateName.contains('Appointment') || templateName.contains('Caregiver'))){
            numParts = 3;
        }
        return numParts;
    }
    
    private String replaceNull(String value, String overrideValue){
       
        if(value==null){
            value = overrideValue;
        }
        return value;
    }

    public boolean isValidRequest(){
       
        if(phoneNumber!=null && firstName !=null && templateName!=null){
            return true;
        }
        return false;
    }    
}
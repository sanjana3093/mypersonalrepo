/********************************************************************************************************
*  @author          Deloitte
*  @description     This is a test class created for CCL AAF Controller. 
To Test the common methods in the ADF Controller class
*  @param           
*  @date            June 18, 2020
*********************************************************************************************************/    
@isTest
public class CCL_ADF_Controller_Test {
    /* Therapy name */
    final static String THERAPY_NAME = 'Test Therapy';
    /*Account Name */
    final static String ACCOUNT_NAME = 'Test Account';
    /* Date Format */
    final static String DATE_FORMAT = 'EEEE dd MMMMM yyyy\' \'HH:mm z';
    
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing insert scenarios
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    static testMethod void getADFSteps()
    {
        Test.startTest();
        final List<CCL_ADF_Progress_Steps__mdt> testADFSteps=CCL_ADF_Controller.fetchADFSteps();
        Test.stopTest();
        final List<CCL_ADF_Progress_Steps__mdt> actualADFSteps=[select Masterlabel,CCL_Order_of_Step__c from CCL_ADF_Progress_Steps__mdt order by CCL_Order_of_Step__c limit 50];
        System.assertEquals(testADFSteps.size(),actualADFSteps.size(),'The size');
    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing ADF header
* @Param       : Void
* @Return      : Void
***************************************************************************************/ 
    static testMethod void testGetADFHeadeInfo() {
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
        Test.startTest();
        final List<CCL_Apheresis_Data_Form__c> adfList= CCL_ADF_Controller.getADFHeadeInfo(adf.Id);
        Test.stopTest();
        System.assertEquals(adfList[0].CCL_Patient_Name__c,'Test Account');  
        
    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing PRF fetch scenario
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    static testMethod void getPRF()
    {
        Test.startTest();
        final List<CCL_PRF_Summary__mdt> testPRFData=CCL_ADF_Controller.getPRF(THERAPY_NAME);
        Test.stopTest();
        /*final List<CCL_PRF_Summary__mdt> actualPRFData=[select CCL_Field_Api_Name__c,Masterlabel,CCL_Field_Type__c,CCL_Help_Text__c,CCL_HasAddress__c,CCL_Order_of_Field__c,
                                                        CCL_Order_of_Section__c,CCL_Object_API_Name__c,CCL_Section_Name__c from CCL_PRF_Summary__mdt where CCL_Therapy_Name_all__c like: '%'+THERAPY_NAME+'%' order by CCL_Order_of_Section__c];
        */
         final List<CCL_PRF_Summary__mdt> actualPRFData=[select CCL_Field_Api_Name__c,Masterlabel,CCL_Field_Type__c,CCL_Help_Text__c,CCL_HasAddress__c, CCL_Therapy_Name_all__c ,CCL_Order_of_Field__c,
                CCL_Order_of_Section__c,CCL_Object_API_Name__c,CCL_Section_Name__c from CCL_PRF_Summary__mdt   order by CCL_Order_of_Section__c]; 
        System.assertEquals(testPRFData.size(),actualPRFData.size(),'The size');
        
    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing  dynamic query method
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    static testMethod void getRecords() {       
        final CCL_Time_Zone__c timezone =CCL_Test_SetUp.createTestTimezone(ACCOUNT_NAME); 
        Test.startTest();
        final Account acc=CCL_Test_SetUp.createTestAccount(ACCOUNT_NAME,timezone.Id); 
        final List<sobject> myList=CCL_ADF_Controller.getRecords('Account','Id,Name',acc.Id);
        Test.stopTest();
        final List<Account> accfinalLst=[select id, name from Account where id=:acc.Id];
        System.assertEquals(myList.size(),accfinalLst.size(),'The size');
    }
    
    
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/20/2020
* @Description : Testing saveForLater function
* @Param       : Void
* @Return      : Void
***************************************************************************************/
    static testMethod void testSaveForLater(){
        
        //changes for run
        User testUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE);
        
        insert testUser;
        System.runAs(testUser) {
            Test.startTest();
            final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
            final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
            final List<CCL_Apheresis_Data_Form__c> adfList= CCL_ADF_Controller.saveForLater('test Json',adf.Id);
            Test.stopTest();
            System.assertEquals(adfList[0].CCL_SaveJSON__c,'test Json');
        }
        
    }
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/24/2020
* @Description : Testing saveAdfData function
* @Param       : String
* @Return      : Void
***************************************************************************************/
    
    static testMethod void saveAdfDataTest () {  
        User testUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE);
        
        insert testUser;
        System.runAs(testUser) {
            final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
            final CCL_Apheresis_Data_Form__c adfObj=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
            adfObj.CCL_Collection_Cryopreservation_Comments__c='Comment2';
            adfObj.CCL_Apheresis_System_Used__c='Spectra Optia';
            Map<String,String> jsonTestData=new Map<String,String>();
            jsonTestData.put('Id', adfObj.Id);
            jsonTestData.put('CCL_Other__c', adfObj.CCL_Other__c);
            jsonTestData.put('CCL_Collection_Cryopreservation_Comments__c', adfObj.CCL_Collection_Cryopreservation_Comments__c);
            jsonTestData.put('CCL_Apheresis_System_Used__c', adfObj.CCL_Apheresis_System_Used__c);
            Test.startTest();
            CCL_ADF_Controller.saveAdfData(jsonTestData);
            Test.stopTest();
            System.assertEquals(adfObj.CCL_Collection_Cryopreservation_Comments__c,'Comment2','Success');
        }
    }
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/24/2020
* @Description : Testing saveCollections function
* @Param       : String
* @Return      : Void
***************************************************************************************/
    static testMethod void saveCollectionsTest () {
        User testUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE);
        
        insert testUser;
        System.runAs(testUser) {
            List<CCL_Summary__c> summList = new List<CCL_Summary__c>();
            final CCL_Summary__c summObj= new CCL_Summary__c();
            summObj.CCL_Apheresis_ID_DIN_DEC__c='test1';
            summObj.CCL_Volume_of_blood_processed__c=1.2;
			summObj.CCL_External_Identification__c='1234567';
            summList.add(summObj);
            database.insert(summList);
            Test.startTest();
            CCL_ADF_Controller.saveCollections(summList);
            Test.stopTest();
            System.assert(summObj.Id!=null, 'Success');
        }
    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : June 25 2020
* @Description : Testing getUsername(), to retrieve the Current Logged in user details.
*                Added as part of Change CGTU-266.
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    static testMethod void testGetUserNameDetails() {
        Test.startTest();
        final User testUserName=CCL_ADF_Controller.getUserName();
        Test.stopTest();
        final User actualUser=[SELECT id,firstname, lastname FROM User WHERE id = :UserInfo.getUserId()];
        System.assertEquals(testUserName.Id,actualUser.Id,'Loaded the current User');
    }
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : June 25 2020
* @Description : Testing getUserTimeZoneDetails(), to retrieve the Current time of the
*                Logged in User based on their TimeZone details.
*                Added as part of Change CGTU-266.
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    
    static testMethod void testGetUserTimeZoneDetails() {
        Test.startTest();
        final DateTime userTimeZone = CCL_ADF_Controller.getUserTimeZoneDetails();
        //final DateTime userTime = CCL_ADF_Controller.getUserTimeZoneDetails('Loaded the current User TimeZone');
        
        Test.stopTest();
        final Timezone actualTimeZone = UserInfo.getTimeZone();
        final Integer offset = actualTimeZone.getOffset(system.now());
        final DateTime dateTimeValue = System.now().addSeconds(offset/1000);
        System.assertEquals(userTimeZone,dateTimeValue,'Loaded the current User TimeZone');
    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/25/2020
* @Description : Testing Recommended Collection meta data fetch scenario
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    static testMethod void getRecommendedCollectionMetadata() {
        Test.startTest();
        final List<CCL_Recommended_Collection_Details__mdt> testData=CCL_ADF_Controller.getRecommendedCollectionDetails();
        Test.stopTest();
        final List<CCL_Recommended_Collection_Details__mdt> actualData=[select CCL_Field_Api_Name__c,Masterlabel,CCL_Field_Type__c,CCL_Help_Text__c, CCL_Order_of_Field__c,
                                                                        CCL_Order_of_Section__c,CCL_Object_API_Name__c,CCL_Section_Name__c,CCL_Place_Holder__c,CCL_Show_Step__c,CCL_Max_Allowed__c,CCL_When_Range_overflow__c  from CCL_Recommended_Collection_Details__mdt  order by CCL_Order_of_Section__c limit 11];
        System.assertEquals(testData.size(),actualData.size(),'The size');
    }
    
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : June 25 2020
* @Description : Testing saveTransferToCellLabDetails(), to save the Current Logged in user details.
*                Added as part of Change CGTU-266.
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    static testMethod void testSaveTransferToCellLabDetails() {
        final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
        final CCL_Apheresis_Data_Form__c testAdfObj=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
        final User actualUser=[SELECT id,firstname, lastname FROM User WHERE id = :UserInfo.getUserId()];
        final Datetime userTimeZone = CCL_ADF_Controller.getUserTimeZoneDetails();
        Test.startTest();
        CCL_ADF_Controller.saveTransferToCellLabDetails(actualUser,userTimeZone,testAdfObj.Id,'17 Aug 2020 11:16 EST');
        final CCL_Apheresis_Data_Form__c adfRecords=[SELECT id,CCL_Collection_Details_Completed_By__c,CCL_Collection_Details_Completed_By__r.firstname,CCL_Collection_Details_Completed_Date__c FROM CCL_Apheresis_Data_Form__c WHERE Id=:testAdfObj.Id WITH SECURITY_ENFORCED];                                     
        System.assert(adfRecords!=null,'The adfRecords are not empty');
        System.assertEquals(adfRecords.CCL_Collection_Details_Completed_Date__c,userTimeZone,'The dates match');
        Test.stopTest();
    }
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : June 25 2020
* @Description : Testing getUserTimeInFormat(), to retrieve the Current Logged in usertime
*                in a specific format.
*                Added as part of Change CGTU-266.
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    static testMethod void testGetUserTimeInFormat()
    {
        Test.startTest();
        final DateTime userTimeZone = CCL_ADF_Controller.getUserTimeZoneDetails();
        final String userTimeFormatted=CCL_ADF_Controller.getUserTimeInFormat(userTimeZone,DATE_FORMAT);
        Test.stopTest();
        System.assertEquals(userTimeFormatted,userTimeZone.formatGMT(DATE_FORMAT),'Formatted the current User Collection Details Time');
    }
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing PRF fetch scenario
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    static testMethod void getCyroDetails()
    {
        Test.startTest();
        final List<CCL_Required_Cryopreservation_Details__mdt> getRequiredCryo=CCL_ADF_Controller.getRequiredCryo();
        Test.stopTest();
        final List<CCL_Required_Cryopreservation_Details__mdt> actualPRFData=[select CCL_Field_Api_Name__c,CCL_IsEditable__c,CCL_Object_API_Name__c,CCL_Field_Length__c,Masterlabel,CCL_Field_Type__c,CCL_Help_Text__c ,CCL_Order_of_Field__c
                                                                              from CCL_Required_Cryopreservation_Details__mdt order by CCL_Order_of_Field__c limit 50];
        System.assertEquals(getRequiredCryo.size(),actualPRFData.size(),'The size');
    }
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/18/2020
* @Description : Testing  dynamic query method
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    static testMethod void getRecordsofCryo() {     
        final CCL_Time_Zone__c timezone =CCL_Test_SetUp.createTestTimezone(ACCOUNT_NAME); 
        Test.startTest();
        final Account acc=CCL_Test_SetUp.createTestAccount(ACCOUNT_NAME,timezone.Id); 
        List <Id> idList=new List<Id>();
        idList.add(acc.id);
        final List<sobject> myList=CCL_ADF_Controller.getRequiredCryoDetails('Account','Id,Name',idList);
        Test.stopTest();
        final List<Account> accfinalLst=[select id, name from Account where id=:acc.Id];
        System.assertEquals(myList.size(),accfinalLst.size(),'The size');
    }
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : 06/29/2020
* @Description : Testing save of cryo details
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    static testMethod void savecryoDetails() {  
        User testUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE);
        
        insert testUser;
        System.runAs(testUser) {
            final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();
            final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id); 
            
            List<CCL_Summary__c> summaryList=new List<CCL_Summary__c>();
            List<CCL_Batch__c> batchList=new List<CCL_Batch__c>();
            Test.startTest();
            
            final CCL_Summary__c summary=CCL_Test_SetUp.createTestSummary(ACCOUNT_NAME,adf.Id);
            summaryList.add(summary);
            final CCL_Batch__c batch=CCL_Test_SetUp.createTestBatch(ACCOUNT_NAME,summary.Id);
            batch.CCL_Order__c = order.Id;
            try{
            CCL_ADF_Controller.saveCryo(batchList,summaryList,ACCOUNT_NAME,adf.Id);
                batchList.add(batch);
                CCL_ADF_Controller.saveCryo(batchList,summaryList,ACCOUNT_NAME,adf.Id);
                for(CCL_Batch__C b : batchList){
                    b.CCL_Cryobag_External_ID__c='TestExtId';
                }
                CCL_ADF_Controller.saveCryo(batchList,summaryList,ACCOUNT_NAME,adf.Id);
            }
            Catch(Exception e)
            {}
            Test.stopTest();
            final List<CCL_Batch__c> batchFinalList=[select id, name from CCL_Batch__c where CCL_Collection__c=:summary.Id];
            System.assert(batchFinalList!=null,'The batches are not empty');
        }
    }
    
    /*************************************************************************************
* @author      : Deloitte
* @date        : 07/07/2020
* @Description : Testing Recommended cryopreservation meta data fetch scenario
* @Param       : Void
* @Return      : Void
***************************************************************************************/   
    static testMethod void testGetRecommendedCryoDetails() {
        Test.startTest();
        final List<CCL_Recommended_Cryopreservation_Details__mdt> testData=CCL_ADF_Controller.getRecommendedCryoDetails();
        Test.stopTest();
        final List<CCL_Recommended_Cryopreservation_Details__mdt> actualData=[select CCL_Field_Api_Name__c,Masterlabel,CCL_Field_Type__c,CCL_Help_Text__c, CCL_Order_of_Field__c,
                                                                              CCL_Order_of_Section__c,CCL_Object_API_Name__c,CCL_Section_Name__c,CCL_Place_Holder__c,CCL_Show_Step__c,CCL_Max_Allowed__c,CCL_When_Range_overflow__c   from CCL_Recommended_Cryopreservation_Details__mdt  order by CCL_Order_of_Section__c limit 14];
        System.assertEquals(testData.size(),actualData.size(),'The size');
    }
      /*************************************************************************************
* @author      : Deloitte
* @date        : 12/21/2020
* @Description : Testing 
* @Param       : Void
* @Return      : Void
***************************************************************************************/          
 static testMethod void testGetUserTimeZone() {
     final Timezone time1 = UserInfo.getTimeZone();
     Test.startTest();
     final DateTime formetedDate= CCL_ADF_Controller.getUserTimeZoneDetails('time1');
     Test.stopTest();
     System.assertEquals(DateTime.now(), formetedDate);
 }
 
}
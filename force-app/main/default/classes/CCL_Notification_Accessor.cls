/********************************************************************************************************
*  @author          Deloitte
*  @description     This Accessor class will be leveraged to support the Notification engine.
*  @date            August 04 2020 
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Deloitte                  August 04 2020         Initial Version
****************************************************************************************************************/
public without sharing class CCL_Notification_Accessor {

    /*
    *   @author           Deloitte
    *   @description      Fetch all the Notification Settings MetaData Type
    *   @para             None
    *   @return           Map<Id,CCL_Notification_Settings__mdt>
    *   @date             28 July 2020
    */
    public static Map<Id,CCL_Notification_Settings__mdt> fetchAllNotificationSettings() {

        Map<Id,CCL_Notification_Settings__mdt> mapToReturnSettings= new Map<Id,CCL_Notification_Settings__mdt>();
        if (Schema.SObjectType.CCL_Notification_Settings__mdt.fields.CCL_Associated_Therapy__c.isAccessible() 
            && Schema.SObjectType.CCL_Notification_Settings__mdt.fields.CCL_Field_To_Identify_User_Preference__c.isAccessible()) {
            List<CCL_Notification_Settings__mdt> listOfMetadataRecords= new List<CCL_Notification_Settings__mdt>();
            listOfMetadataRecords=[SELECT Id, DeveloperName , CCL_Notification_Type__c, CCL_Field_To_Identify_User_Preference__c,
                                    CCL_Associated_Therapy__c, CCL_Sites_To_Be_Notified__c, CCL_Email_Template_Letterhead__c,
                                    CCL_Channel_Delivery_Mode__c, CCL_Category__c, CCL_Notification_Number__c, 
                                    CCL_Field_To_Identify_Pickup_Location__c, CCL_Field_To_Identify_Drop_of_Location__c
                                    FROM CCL_Notification_Settings__mdt 
                                    WHERE CCL_isActive__c = :CCL_Notifications_Constants.TRUE_BOOLEAN_VALUE];
            if(!listOfMetadataRecords.isEmpty()) {
                for(CCL_Notification_Settings__mdt settingsRecord:listOfMetadataRecords) {
                    mapToReturnSettings.put(settingsRecord.id,settingsRecord);
                }
            }
        }
        return mapToReturnSettings;
    }

    /*
    *   @author           Deloitte
    *   @description      Fetch all the Notification Content MetaData Type
    *   @para             None
    *   @return           List<CCL_Notification_Content__mdt>
    *   @date             28 July 2020
    */
    public static Map<Id,CCL_Notification_Content__mdt> fetchAllNotificationContent() {
        List<CCL_Notification_Content__mdt> listOfMetadataRecords= new List<CCL_Notification_Content__mdt>();
        Map<Id,CCL_Notification_Content__mdt> mapToReturnContent= new Map<Id,CCL_Notification_Content__mdt>();

        if (Schema.SObjectType.CCL_Notification_Content__mdt.fields.CCL_Country__c.isAccessible() && 
            Schema.SObjectType.CCL_Notification_Content__mdt.fields.CCL_Email_Template__c.isAccessible()
            && Schema.SObjectType.CCL_Notification_Settings__mdt.fields.CCL_Category__c.isAccessible() ) {
                listOfMetadataRecords=[SELECT Id, DeveloperName, CCL_Country__c, CCL_Email_Template__c,CCL_Notification_Settings__c, 
                                        CCL_Notification_Settings__r.DeveloperName,CCL_Notification_Settings__r.CCL_Category__c,CCL_Language__c 
                                        FROM CCL_Notification_Content__mdt];

            if(listOfMetadataRecords!= null && listOfMetadataRecords.size()>0) {
                for(CCL_Notification_Content__mdt contentRecord:listOfMetadataRecords) {
                    mapToReturnContent.put(contentRecord.id,contentRecord);
                }
            }
        }
        return mapToReturnContent;
    }

    /*
    *   @author           Deloitte
    *   @description      Fetch all the email templates based on the Email template name and whether the template is active
    *   @para             None
    *   @return           List<EmailTemplate>
    *   @date             28 July 2020
    */
    public static Map<String,EmailTemplate> fetchAllEmailTemplate() {
        Map<String,EmailTemplate> mapToReturnEmailTemplate= new Map<String,EmailTemplate>();
        
        List<EmailTemplate> listOfEmailTemplate = new List<EmailTemplate>();
		try {
			listOfEmailTemplate = [SELECT Id, Name, BrandTemplateId, DeveloperName, Subject, body, HtmlValue, Markup,
									Folder.DeveloperName, IsActive
									FROM EmailTemplate 
									WHERE Folder.DeveloperName = :CCL_Notifications_Constants.EMAIL_TEMPLATE_FOLDER_DEVELOPERNAME 
									AND IsActive = :CCL_Notifications_Constants.TRUE_BOOLEAN_VALUE];
		} catch (System.QueryException qe) {
			System.debug('An exception occurred with fetchAllEmailTemplate: ' + qe.getMessage());
		}
		if(listOfEmailTemplate!= null && listOfEmailTemplate.size()>0) {
			for(EmailTemplate templateRecord:listOfEmailTemplate) {
				mapToReturnEmailTemplate.put(templateRecord.developerName,templateRecord);
			}
		}
        return mapToReturnEmailTemplate;
    }

    /*
    *   @author           Deloitte
    *   @description      Fetch all the Notification Signatures MetaData Type and return a combination of unique code and master signature template name
    *   @para             None
    *   @return           Map<String,String> i.e 
                          map<uniqueCombination of Country Code+'_'+TherapyId+'_'+Notification type, CCL_Email_Template_Signature__c>
    *   @date             16 Dec 2020
    */
    public static Map<String,String> fetchAllNotificationSignatures() {
        List<CCL_Notification_Signatures__mdt> listOfMetadataRecords= new List<CCL_Notification_Signatures__mdt>();
        Map<String,String> mapToReturnSignaturTemplates= new Map<String,String>();
        String uniqueKey='';
        if (Schema.SObjectType.CCL_Notification_Signatures__mdt.fields.CCL_Country__c.isAccessible() && 
            Schema.SObjectType.CCL_Notification_Signatures__mdt.fields.CCL_Email_Template_Signature__c.isAccessible() &&
            Schema.SObjectType.CCL_Notification_Signatures__mdt.fields.CCL_Notification_Type__c.isAccessible() ) {
                listOfMetadataRecords=[SELECT Id, DeveloperName, CCL_Country__c, CCL_Email_Template_Signature__c,CCL_Notification_Type__c, 
                                        CCL_Therapy__c
                                        FROM CCL_Notification_Signatures__mdt];

            if(listOfMetadataRecords!= null && listOfMetadataRecords.size()>0) {
                for(CCL_Notification_Signatures__mdt signatureRecord:listOfMetadataRecords) {
                    uniqueKey=signatureRecord.CCL_Country__c+'_'+signatureRecord.CCL_Therapy__c+'_'+signatureRecord.CCL_Notification_Type__c;
                    mapToReturnSignaturTemplates.put(uniqueKey,signatureRecord.CCL_Email_Template_Signature__c);
                }
            }
        }
        return mapToReturnSignaturTemplates;
    }

    /*
    *   @author           Deloitte
    *   @description      Insert a list of notifications once the permissions have been checked
    *   @para             List <Notification> 
    *   @return           
    *   @date             28 July 2020
    */

    public static void insertNotifications (List<CCL_Notification__c> listNotificationRecords) {
		insert listNotificationRecords;          
    }

    /*
    *   @author           Deloitte
    *   @description      Fetch the User Therapies associations linked to specific Therapies 
    *                     and accounts through the Therapy Association.
    *   @para             List<Id>, List<Id>, Map<Id,String>
    *   @return           List<CCL_User_Therapy_Association__c>
    *   @date             28 July 2020
    */
    public static List<CCL_User_Therapy_Association__c> fetchUserTherapyAssociation (List<Id> accountIds, List<Id> therapyIds, Map<Id,String> shippingCountryCodePerAccountMap, Map<Id,String> shippingCountryCodePerPickUpAccountMap, Map<Id,String> shippingCountryCodePerDropOffAccountMap) {
       List<CCL_User_Therapy_Association__c> listUserTherapyAssociation = new List<CCL_User_Therapy_Association__c>();
        try {
            // Country code not included as there is a possibility of no record in query - update to comment
    

                listUserTherapyAssociation   =      [SELECT Id, CCL_Contact__c, CCL_User__c,CCL_Therapy_Association__r.CCL_Site__c, CCL_Therapy_Association__r.CCL_Country__c,
                                                    CCL_Therapy_Association__r.CCL_Therapy__c,CCL_Therapy_Association__c,
                                                    CCL_Clinical_Emails__c,CCL_Commercial_Emails__c,CCL_Internal_Emails__c,CCL_User__r.Email,CCL_Contact__r.Email, CCL_Active__c,CCL_User__r.CCL_Secondary_Email__c,
                                                    recordtypeId,CCL_Therapy_Association__r.CCL_Therapy__r.name, CCL_Language__c, 
                                                    CCL_Therapy_Association__r.CCL_Therapy__r.CCL_Type__c, CCL_Therapy_Association__r.CCL_Site__r.ShippingCountryCode
                                                    FROM CCL_User_Therapy_Association__c                            
                                                    WHERE 
                                                    (RecordTypeId !=:CCL_Notifications_Constants.PRISCRIBER_PRINCIPAL_INVESTIGATOR)
                                                    AND
                                                    (CCL_Active__c = :CCL_Notifications_Constants.TRUE_BOOLEAN_VALUE) 
                                                    AND 
                                                    ((CCL_Therapy_Association__r.CCL_Site__c IN:accountIds 
                                                        AND CCL_Therapy_Association__r.CCL_Therapy__c IN:therapyIds)
                                                    OR(CCL_Therapy_Association__r.CCL_Country__c IN:ShippingCountryCodePerAccountMap.values() 
                                                        AND CCL_Therapy_Association__r.CCL_Therapy__c IN:therapyIds)
                                                    OR(CCL_Therapy_Association__r.CCL_Country__c IN:ShippingCountryCodePerAccountMap.values() 
                                                        AND CCL_Therapy_Association__r.CCL_Therapy__r.Name =:CCL_Notifications_Constants.GENERIC_THERAPY_NAME
                                                        AND CCL_Primary_Role__c !=:CCL_Notifications_Constants.INBOUND_LOGISTIC_ROLE 
                                                        AND CCL_Primary_Role__c !=:CCL_Notifications_Constants.OUTBOUND_LOGISTIC_ROLE)
                                                    OR(CCL_Therapy_Association__r.CCL_Country__c IN:shippingCountryCodePerPickUpAccountMap.values() 
                                                        AND CCL_Therapy_Association__r.CCL_Therapy__r.Name =:CCL_Notifications_Constants.GENERIC_THERAPY_NAME
                                                        AND CCL_Primary_Role__c =:CCL_Notifications_Constants.INBOUND_LOGISTIC_ROLE)
                                                    OR(CCL_Therapy_Association__r.CCL_Country__c IN:shippingCountryCodePerDropOffAccountMap.values() 
                                                        AND CCL_Therapy_Association__r.CCL_Therapy__r.Name =:CCL_Notifications_Constants.GENERIC_THERAPY_NAME
                                                        AND CCL_Primary_Role__c =:CCL_Notifications_Constants.OUTBOUND_LOGISTIC_ROLE))];
                                                   
        } catch (System.QueryException qe) {
            System.debug('An exception occurred with fetchUserTherapyAssociation: ' + qe.getMessage());
        }
       
        return listUserTherapyAssociation;
    }


    /*
    *   @author           Deloitte
    *   @description      Fetch the User Therapies associations linked to specific Therapies 
    *   @para             List<Id>, List<Id>, Map<Id,String>
    *   @return           List<CCL_User_Therapy_Association__c>
    *   @date             28 July 2020
    */
    public static List<Account> fetchAccountShippingCountryCode (List<Id> accountId) {
        List<Account> listUserTherapyAssociation = new List<Account>();
        try {
            // Country code not included as there is a possibility of no record in query
            if (!accountId.isEmpty() && Schema.sObjectType.Account.fields.ShippingCountryCode.isAccessible() ) {
                listUserTherapyAssociation   =      [SELECT Id, ShippingCountryCode 
                                                     FROM Account 
                                                     WHERE Id IN :accountId                            
                                                     WITH SECURITY_ENFORCED];
            }
        } catch (System.QueryException qe) {
            System.debug('An exception occurred with fetchUserTherapyAssociation: ' + qe.getMessage());
        }
        return listUserTherapyAssociation;
    }
}
global class NightlyTaskUpload implements Schedulable{ 
/*
**   Author: Anthony Williams
**   Purpose: This class is a scheduled job that loads a list from Via with the SMS message body from SMS that were sent the previous day.
**            The SMS are associated to the corresponding Tasks via the TaskId captured in Via at the time of submissions
**            
*******************************************************************************************************************************************/

        global void execute(SchedulableContext SC) {
         
           //String viaFile = 
           //outboundFile();
           BatchableNightlTaskUpload njob = new BatchableNightlTaskUpload();
           ID batchprocessid = Database.executeBatch(njob,10);
           System.debug('Returned batch process Id: ' + batchprocessid);
     } // end of execute Method


            //This is a helper function to grab the file from Via and converts it into a string
            @future(callout=true)
            public static void outboundFile(){
                String sessionId = null;
                String tokenId = null;
                String tokenResponse = null;
                Blob fileblob;
                String fileUrl;
    
                //get the token
                if(Test.isRunningTest())
                {
                    tokenResponse = '{"session" : {"sessionId" : "0e2ef0d0-964d-4164-a8bb-43b2af481183"},"accessToken" : {"agentUsername" : "via.manager@sagerx.com","access_token" : "094be7a9-f09e-4ef1-a49d-9d848d52e965"}}';
                }else{
                    tokenResponse = aspt.ViaController.StartProcessingCallOutCampaign();                          
                }
                System.debug(LoggingLevel.INFO, 'Token response string = ' + tokenResponse);
                JSONParser parser = JSON.createParser(tokenResponse);
                while(parser.nextToken() != null){
                    if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText()=='sessionId')){
                        parser.nextToken();
                        sessionId = parser.getText();
                    }
                    if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText()=='access_token')){
                        parser.nextToken();
                        tokenId = parser.getText();
                    }
                }
                //System.debug(LoggingLevel.INFO, 'sessionId: ' + sessionId);
                //System.debug(LoggingLevel.INFO, 'tokenId: ' + tokenId);

                //get the custom settings for the constants
                //System.debug(LoggingLevel.INFO, 'JSON string = ' + jsonString);
                Outbound_SMS_Settings__c smsSettings = Outbound_SMS_Settings__c.getOrgDefaults();
                //String endpoint = 'https://api.us.via.aspect-cloud.net/via/v2/organizations/sagerx/mediaStorage/data/objectKeys?key=exports/outreach/list-management;keyType=signedDownloadUrl';
                String endpoint = smsSettings.File_Import_URL__c;


                Http http = new Http();
                HttpRequest request = new HttpRequest();
                HttpResponse response = new HttpResponse();
                if(!Test.isRunningTest()){
                  request.setEndpoint(endPoint);
                  request.setMethod('GET');
                  request.setHeader('Content-Type', 'application/json');
                  request.setHeader('Authorization', 'Bearer ' + tokenId);      
                  request.setHeader('via-client-sessionId', sessionId);    
                  request.setHeader('x-api-key', smsSettings.ApiKey__c);
                  request.setTimeout(60000);      
                  //request.setBody(jsonString);
                  System.debug(LoggingLevel.INFO, 'About to send request to endpoint: ' + endpoint);
                }
                if(Test.isRunningTest()){
                   response.setBody('{"key": "exports/reporting/outreach/outreach-20180401.tar","keyHash": "1a011762cec01ce9051c0eb81fc6f766","keyName": "outreach-20180401.tar","size": "5370.0 KB","lastModifiedDate": "2018-04-30T19:11:56Z","keyType": "File ","signedUploadUrl": " ","signedDownloadUrl": "https://{orgId}.via.aspect- cloud.net/s3/aspect-via-data-{region}- {orgId].s3.amazonaws.com/exports/outreach/outre ach-20180401.tar?X-Amz-Security- Token=FQoDYXdzEEkaDNf1hxLJ2DFLxBjBbiL7AbDVwBqRx2QaKTtnoBxmYLaR %2BEHQF3og1ByQvn61nPa1ZYjlsiWF3ZGWJmalP5hrbdc%2FmTJjnIKN2ZsLS9%2Fj %2FSRlIyYZAK1FwL0CXozD3WtEORkNR0OP5nx2L%2Bh%2BLmKIHCsC3Bm7DVjnt%2FR %2B4az6wJ8NKZwwX7uylxhEu3uja2nRiNjD0XSiDLlfUkHlUBS597pKPqcH4qNmDmqszFIIoxbXuR6Se %2BLtj0WQq5era5ruk%2Fp0QrNwuuVIM6w%2FWmhk5N3Ld1GNl5ndvZbxQ %2FUVp1y5L%2FRiKsOqg7wjNO5beT52QPZ%2F0W6e7L4hIp8cTCmWdPZ0IBOaT3w%2B5UiVKMDdrNcF&amp;X-Amz-Algorithm=AWS4- HMAC-SHA256&amp;X-Amz-Date=20180503T164001Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Expires=518399&amp;X-Amz-Credential=ASIAI2647DEKRJOGMUMA%2F20180503%2Fus-east-1%2Fs3%2Faws4_request&amp;X-Amz- Signature=15d827ba81597255b71c4493472ef132adccbbe5270da7bddc95c92451d90c75"}');
                }
                else{
                   response = http.send(request);
                }
                System.debug(LoggingLevel.INFO, 'Response from Via :' + response.getBody());
                //fileblob = response.getBodyAsBlob();
                
                if(response != null){
                   HttpRequest filerequest = new HttpRequest();  
                   filerequest.setMethod('GET');
                   filerequest.setTimeout(60000);
                   filerequest.setEndpoint(response.getBody().substringAfter('"signedDownloadUrl":').remove('}').remove('"'));
                   System.debug(LoggingLevel.INFO, 'Sending to Via :' + filerequest.getBody());
                   HttpResponse blobresponse = new HttpResponse();
                   if(Test.isRunningTest()){
                      //Create Access
                      String devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId();
                      Case caseProgrm= new Case(Subject='MidTest Record Creation', RecordTypeId=devRecordTypeId);
                      insert caseProgrm;
                      Task tstTsk = new Task(WhatId=caseProgrm.Id,Subject='Service Consent', Status = 'Completed',Priority = 'Normal',Type = 'Other',ActivityDate = Date.today());
                      insert tstTsk;
                      blobresponse.setBody(tstTsk.Id+'|Text1|OM_SS|2019-01-31 15:59:03|Spc_Scheduled_Infusion_Date_Pre_Tx_Phase|15085173966');
                   }//end test statement
                   else{
                      blobresponse = http.send(filerequest);
                   }
                   System.debug(LoggingLevel.INFO, 'Response from Via :' + blobresponse.getBody());

                   fileblob = blobresponse.getBodyAsBlob();
               }

               String viaFile = blobToString(fileblob,'ISO-8859-1');
             
               List <String> fileLines = viaFile.split('\n');
              
           /* Adding in temp logic to test parsing*/
           /*List <String> fileLines = new List<String>();
           fileLines.add('00T2900000BrxWiEAJ|Text 1|Success|');
           fileLines.add('00T2900000BrxWiEAJ|Text 2|Success|');
           fileLines.add('00T2900000BrxWiEAJ|Text 3|Success|');*/
           RecordType docRecType = [SELECT Id, DeveloperName,Name FROM RecordType where SobjectType='PatientConnect__PC_Document__c' and Name = 'SMS Outbound' LIMIT 1];
           Task sentTask=null;
           List<Task> searchTask;
           Case programCase;
           //List <PatientConnect__PC_eLetter__c> eLet;
           //List <PatientConnect__PC_Engagement_Program_Eletter__c> eLetter;
           
           for(String line : fileLines){

               System.debug(Logginglevel.DEBUG,'Where is the first line :' + line);
               List<String> columns = line.split('\\|',-1);
               //console.log(columns);
               System.debug(Logginglevel.DEBUG,columns);
               System.debug(Logginglevel.DEBUG,'# of split columns are: ' + columns.size());
               String tskObjId = columns[0];
               String smsBody = columns[1];
               String smsPhone = columns[5];
               String tmpName = columns[4].trim(); //confirm template number
               
               //Task sentTask;
               try{   
                  searchTask = [Select Id, WhatId, Subject, PatientConnect__PC_Document__c From Task Where Id =: tskObjId];
               }
               catch(DMLException dmle){
                  System.debug(Logginglevel.DEBUG, 'Task Id '+tskObjId+' not found in system.');
               }

               if(!searchTask.isEmpty()){
                  sentTask = searchTask[0];
                  programCase = [Select Id, AccountId, PatientConnect__PC_Engagement_Program__c from Case where Id =: sentTask.WhatId];
                  System.debug(Logginglevel.DEBUG, 'Template Name is: ' + tmpName); 
                  //PatientConnect__PC_eLetter__c eLet = [SELECT Id, NAME, PatientConnect__PC_Template_Name__c FROM PatientConnect__PC_eLetter__c where PatientConnect__PC_Template_Name__c =: tmpName.trim()];
                  List <PatientConnect__PC_Engagement_Program_Eletter__c> eLetter = [SELECT Id, NAME, spc_PMRC_Code__c, PatientConnect__PC_eLetter__r.Name FROM PatientConnect__PC_Engagement_Program_Eletter__c WHERE spc_channel__c = 'SMS' AND PatientConnect__PC_eLetter__r.PatientConnect__PC_Template_Name__c =: tmpName LIMIT 1];
                  System.debug(Logginglevel.DEBUG, 'The resulting eLetter is ' + eLetter);
                  //System.debug(Logginglevel.DEBUG, 'The resulting eLet is ' + eLet); 
                  PatientConnect__PC_Document__c newDoc = new PatientConnect__PC_Document__c();
                  newDoc.PatientConnect__PC_SMS_Body__c = smsBody;
                  newDoc.PatientConnect__PC_Document_Status__c = 'Sent';
                  newDoc.RecordTypeId = docRecType.Id;
                  newDoc.Spc_Status_Date__C = Date.today();
                  newDoc.PatientConnect__PC_Engagement_Program__c = programCase.PatientConnect__PC_Engagement_Program__c;
                  if(!eLetter.isEmpty()) newDoc.spc_PMRC_Code_New__c =  eLetter[0].spc_PMRC_Code__c;
                  newDoc.PatientConnect__PC_Description__c = sentTask.Subject.remove('SMS Outbound ');
                  newDoc.PatientConnect__PC_SMS_Phone_Number__c = smsPhone;

                  try{
                     insert newDoc;
                     System.debug(Logginglevel.DEBUG, 'Successfully created outbound document with id ['+newDoc.Id+']');
                     if(newDoc != null){
                        sentTask.PatientConnect__PC_Document__c = newDoc.Id;
                        update sentTask;
                      }
                  }catch(DMLException dmle){
                      System.debug(Logginglevel.ERROR, 'Caught exception inserting document for Task ['+sentTask.Id+'] ==> '+dmle);
                  }
                  PatientConnect__PC_Document_Log__c newDocLog = new PatientConnect__PC_Document_Log__c();
                  newDocLog.PatientConnect__PC_Document__c = newDoc.Id;
                  newDocLog.PatientConnect__PC_Program__c = programCase.Id;
                  newDocLog.PatientConnect__PC_Account__c = programCase.AccountId;

                  try{
                     insert newDocLog;
                     System.debug(Logginglevel.DEBUG, 'Successfully created outbound document log with id ['+newDocLog.Id+']');
                  }catch(DMLException dmle){
                      System.debug(Logginglevel.ERROR, 'Caught exception inserting document log for Task ['+sentTask.Id+'] ==> '+dmle);
                  }
               }
            }   

            }

               public static String blobToString(Blob input, String inCharset){
                    System.debug('What is the input like: ' + input);
                    String hex = EncodingUtil.convertToHex(input);
                    System.assertEquals(0, hex.length() & 1);
                    final Integer bytesCount = hex.length() >> 1;
                    String[] bytes = new String[bytesCount];
                    for(Integer i = 0; i < bytesCount; ++i)
                        bytes[i] =  hex.mid(i << 1, 2);
                    return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
                } //end of blobToString method  

        
}
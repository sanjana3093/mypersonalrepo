public without sharing class SendCaseNotification {
    public class FlowInputs{
        @InvocableVariable
        public Id CaseId;
        @InvocableVariable
        public String novartisRegion;
        @InvocableVariable
        public String notificationName;
        @InvocableVariable
        public String purposeType;
        @InvocableVariable
        public String templateName;
        @InvocableVariable
        public String isUpdate;
        
    }
    public static String fromAddress{set;get;}
    public static String emailTemplateName {set;get;}
    public static List<String> recipientList {set;get;}
    //public static List<String> additionalEmailList{set;get;}
    public static String subject {set; get;}
    public static String htmlBody {set;get;}
    public static String strRecipients {set;get;}
    
    @InvocableMethod(Label = 'Invoke Apex')
    public static void sendEmail(List<FlowInputs> inputs) {
        fromAddress = '';
        emailTemplateName = '';
        recipientList = new List<String> ();
        subject = '';
        htmlBody = '';
        try{
            List<Task_Email_Configuration__c> emailConfigList = new List<Task_Email_Configuration__c>();
            emailConfigList = [SELECT Id, Clinical__c, 
                                        Commercial__c, 
                                        Additional_Emails__c, 
                                        From_Email_Address__c, 
                                        isActive__c, 
                                        Recipient_Group__c 
                                FROM Task_Email_Configuration__c 
                                WHERE Novartis_Region__r.Name =: inputs[0].novartisRegion 
                                AND Name =: inputs[0].notificationName 
                                AND isActive__c = true LIMIT 1];

            if(!emailConfigList.isEmpty()){
                boolean isActiveCommercialClinical = (inputs[0].purposeType == 'Clinical')? emailConfigList[0].Clinical__c : emailConfigList[0].Commercial__c;
                if(isActiveCommercialClinical ){
                    Case caseRecord = [SELECT id, Batch_ID_Trial_ID__c,Material__c, 
                                            Hospital_Purchase_Order__c, Account.name, Account.Pickup_Location_Address__c,
                                            PRF_Received_Date__c, Estimated_Aph_Shipment_Date__c,Infusion_Date__c,OBA_Outcome_Due__c,
                                            Purpose_Indication__c, Purpose_NDC_DIN__c,Owner.Name,Confirmed_Aph_Shipment_Date__c,
                                            Incoterms__c, Ship_To_Address__c,Account.Pick_Up_Location_Contact__r.name, Owner.Email,
                                            Actual_Harvest_Date__c, Projected_FP_Delivery__c, Tentative_FP_Delivery_Time__c, 
                                            PRF_ID__c, Treatment_Key__c, Plant_Appointment_Date__c, Actual_Manufacturing_Start_Date__c,
                                            Account.License__c, Account.Hospital_PO_Contact__r.name, Account.Ship_To_Account__r.Name,
                                       		Account.Ship_To_Account__r.ShippingStreet, Account.Ship_To_Account__r.ShippingCity, 
                                       		Account.Ship_To_Account__r.ShippingState, Account.Ship_To_Account__r.ShippingPostalCode,
                                       		Account.Ship_To_Account__r.ShippingCountry, Account.Aph_Pickup_Time__c, Aph_Shipment_Notes__c
                                        FROM Case 
                                        WHERE Id =: inputs[0].CaseId LIMIT 1 ];

                    EmailTemplate template = getEmailTemplate(inputs[0].templateName);
                    subject = generateSubject(template.Subject, inputs[0].notificationName, caseRecord);
                    htmlBody = generateHtmlBody(template.HtmlValue, inputs[0].notificationName,inputs[0].isUpdate, caseRecord);
                    system.debug('Email HTML Body: ' + htmlBody);
                    fromAddress = emailConfigList[0].From_Email_Address__c;
                    Id orgWideEmailAddressId = getOrgWideEmailAddressId(emailConfigList[0].From_Email_Address__c);
                    if (emailConfigList[0].Recipient_Group__c.contains('CaseOwner')){
                        recipientList.add(getcaseownerEmails(caseRecord.Id));
                        
                        
                    }if (emailConfigList[0].Recipient_Group__c.contains('SpecialtyDistributor')){
                        recipientList.add(getsdEmails(caseRecord.Id));
                        
                    } 
                      
                    recipientList.addAll(getPublicGroupEmails(emailConfigList[0].Recipient_Group__c.split(';')));
                    if(emailConfigList[0].Additional_Emails__c != null ){
                    	recipientList.addAll(getAdditionalEmails(emailConfigList[0].Additional_Emails__c));
                    }
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTemplateId(template.Id);
                    mail.setToAddresses(recipientList);
                    //mail.setWhatId(caseRecord.id);
                    mail.setSubject(subject);
                    mail.setHtmlBody(htmlBody);
                    mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
                    mail.setSaveAsActivity(true);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

                    //create Email Message record
                    strRecipients = listToString(recipientList);
                    createEmailMessage(caseRecord.Id);
                }
               
            }
        }
        catch(Exception e){
            //System.debug(e.getMessage());
            System.debug(e.getLineNumber() + ' ' + e.getCause());
        }
    }
 

    public static EmailTemplate getEmailTemplate(string templateName) {
        List<EmailTemplate> emailTemplateList = [SELECT Id, Subject, HtmlValue,Body  
                                                 FROM EmailTemplate
                                                 WHERE DeveloperName =: templateName
                                                 LIMIT 1];
        if(!emailTemplateList.isEmpty()) {
            return emailTemplateList[0];
        } else {
            return null;
        }
    }
    
    public static List<String> getPublicGroupEmails(List<String> grpName){
        List<String> userEmail = new List<String> ();
        List<Group> grpList=[select (select userOrGroupId from groupMembers) from Group Where Name IN : grpName];
        if(grpList != null){
            List<Id> userIdList = new List<Id>();
            for(Group pg : grpList){
                for(GroupMember gm : pg.groupMembers){
                    userIdList.add(gm.userOrGroupId);
                }
            }
            if(!userIdList.isEmpty()){
                for(User activeUser : [SELECT id, Email, Name FROM User where Id IN :userIdList AND isActive = true]){
                    userEmail.add(activeUser.Email);
                }
            }
        }
        return userEmail;
    }
    public static List<String> getAdditionalEmails(String emails){
        return emails.split(';');
    }
    public static String getcaseownerEmails(Id caseId){
        String caseOwnerEmail = [select Id, Owner.Email from Case Where Id= : caseId].Owner.Email;
        system.debug('Case Owner Mail Id: ' +caseOwnerEmail);
        return caseOwnerEmail;
    }
    public static String getsdEmails(Id caseId){
        String sdEmail;
        Case Acc =[select Id, AccountId from Case Where Id= : caseId];
        List<Account> accList=[select Id,(select AccountTo__c, AccountFrom__c, Status__c, Type__c from ConnectionsTo__r) from Account Where Id =: Acc.AccountId]; 
        if(accList != null){
        Set<Id> sdIdList = new Set<Id>();
        for (Account accid : accList){
            for(CCL3_Account_Connection__c connection : accid.ConnectionsTo__r){
                if (connection.Status__c == 'Active' && connection.Type__c.contains('Specialty Distributor') && connection.AccountFrom__c == accid.Id ){
                    sdIdList.add(connection.AccountTo__c);
                }
            //return sdIdList;
            }
        }
        
        if(!sdIdList.isEmpty()){
        //string sdEmail;
                for(Account accountmail : [SELECT id, Account_Email__c FROM Account where Id IN :sdIdList]){
                 sdEmail = accountmail.Account_Email__c;
                }
            }
        }
        system.debug('SD Mail Id: ' +sdEmail);
        return sdEmail;
    }
     public static String getsdName(Id caseId){
        String sdName;
        Case Acc =[select Id, AccountId from Case Where Id= : caseId];
        List<Account> accList=[select Id,(select AccountTo__c, AccountFrom__c, Status__c, Type__c from ConnectionsTo__r) from Account Where Id =: Acc.AccountId]; 
        if(accList != null){
        Set<Id> sdIdList = new Set<Id>();
            for (Account accid : accList){
                for(CCL3_Account_Connection__c connection : accid.ConnectionsTo__r){
                    if (connection.Status__c == 'Active' && connection.Type__c.contains('Specialty Distributor') && connection.AccountFrom__c == accid.Id ){
                        sdIdList.add(connection.AccountTo__c);
                    }
                }
            }
            if(!sdIdList.isEmpty()){
                for(Account accountmail : [SELECT id, Name FROM Account where Id IN :sdIdList]){
                sdName = accountmail.Name;
                }
            }
        }
        return sdName;
    }
    public static Id getOrgWideEmailAddressId(string senderAddress) {
    	List<OrgWideEmailAddress> emailAddressList = [SELECT Id FROM OrgWideEmailAddress WHERE Address = :senderAddress LIMIT 1];
        if(!emailAddressList.isEmpty()) {
            return emailAddressList[0].Id;
        } 
        else {
            return null;
        }
    }
    public static String generateSubject(String subject, String notificationName, Case caseRecord){
        if(notificationName.equalsIgnoreCase ('NOT_4')){
            subject = subject.replace('--TreatmentCenter--', setValueOrEmpty(caseRecord.Account.Name));
        }
        if(notificationName.equalsIgnoreCase ('NOT_5')  || 
            notificationName.equalsIgnoreCase ('NOT_6') ||
            notificationName.equalsIgnoreCase ('NOT_7') || 
            notificationName.equalsIgnoreCase ('NOT_8') || 
            notificationName.equalsIgnoreCase ('NOT_9') ||
            notificationName.equalsIgnoreCase ('NOT_10') ||
            notificationName.equalsIgnoreCase ('NOT_11') ||
            notificationName.equalsIgnoreCase ('NOT_12') ||
            notificationName.equalsIgnoreCase ('NOT_13')||
            notificationName.equalsIgnoreCase ('NOT_14')
           ){
            subject = subject.replace('--TreatmentCenter--', setValueOrEmpty(caseRecord.Account.Name));
            subject = subject.replace('--BatchId--', setValueOrEmpty(caseRecord.Batch_ID_Trial_ID__c));
        }
        return subject ;
    }
    public static String generateHtmlBody(String htmlBody, String notificationName, string isUpdate, Case caseRecord){
        if(notificationName.equalsIgnoreCase ('NOT_4')){
            htmlBody = htmlBody.replace('--TreatmentCenter--', setValueOrEmpty(caseRecord.Account.Name));
            htmlBody = htmlBody.replace('--PRFNumber--', setValueOrEmpty(caseRecord.PRF_ID__c));
            htmlBody = htmlBody.replace('--TreatmentKey--', setValueOrEmpty(caseRecord.Treatment_Key__c));
            if(caseRecord.Plant_Appointment_Date__c != null){
            	htmlBody = htmlBody.replace('--AppointmentDate--', String.valueOf(caseRecord.Plant_Appointment_Date__c));
            }
            else{
            	htmlBody = htmlBody.replace('--AppointmentDate--', '');
            }
            htmlBody = htmlBody.replace('--LinkToCase--', URL.getSalesforceBaseUrl().toExternalForm() + '/' + caseRecord.id);
        }
        if(notificationName.equalsIgnoreCase ('NOT_5')){
            htmlBody = htmlBody.replace('--TreatmentCenter--', setValueOrEmpty(caseRecord.Account.Name));
            if(caseRecord.PRF_Received_Date__c != null ){
            	htmlBody = htmlBody.replace('--PRFReceivedDate--', String.valueOf(caseRecord.PRF_Received_Date__c));
            }
            else{
            	htmlBody = htmlBody.replace('--PRFReceivedDate--', '');
            }
            if(caseRecord.Estimated_Aph_Shipment_Date__c != null){
            	htmlBody = htmlBody.replace('--EstAphShipmentDate--', String.valueOf(caseRecord.Estimated_Aph_Shipment_Date__c));
            }
            else{
            	htmlBody = htmlBody.replace('--EstAphShipmentDate--', '');
            }
        }
        if(notificationName.equalsIgnoreCase ('NOT_6')){
            htmlBody = htmlBody.replace('--BatchId--', setValueOrEmpty(caseRecord.Batch_ID_Trial_ID__c));
            htmlBody = htmlBody.replace('--TreatmentCenter--', setValueOrEmpty(caseRecord.Account.Name));
            htmlBody = htmlBody.replace('--ShipToAddress--', setValueOrEmpty(caseRecord.Ship_To_Address__c));
            htmlBody = htmlBody.replace('--Indication--', setValueOrEmpty(caseRecord.Purpose_Indication__c));
            htmlBody = htmlBody.replace('--NDC--', setValueOrEmpty(caseRecord.Purpose_NDC_DIN__c));
            htmlBody = htmlBody.replace('--IncoTerms--', setValueOrEmpty(caseRecord.Incoterms__c ));
            
        }
        if(notificationName.equalsIgnoreCase ('NOT_7')){
            htmlBody = htmlBody.replace('--TreatmentCenter--', setValueOrEmpty(caseRecord.Account.Name));
            htmlBody = htmlBody.replace('--BatchId--', setValueOrEmpty(caseRecord.Batch_ID_Trial_ID__c));
        }
        if(notificationName.equalsIgnoreCase ('NOT_8')){
            htmlBody = htmlBody.replace('--TreatmentCenter--', setValueOrEmpty(caseRecord.Account.Name));
            htmlBody = htmlBody.replace('--HospitalPO--', setValueOrEmpty(caseRecord.Hospital_Purchase_Order__c ));
            htmlBody = htmlBody.replace('--BatchId--', setValueOrEmpty(caseRecord.Batch_ID_Trial_ID__c));
        }
        if(notificationName.equalsIgnoreCase ('NOT_9')){
            htmlBody = htmlBody.replace('--TreatmentCenter--', setValueOrEmpty(caseRecord.Account.Name));
            htmlBody = htmlBody.replace('--HospitalPO--', setValueOrEmpty(caseRecord.Hospital_Purchase_Order__c ));
            htmlBody = htmlBody.replace('--BatchId--', setValueOrEmpty(caseRecord.Batch_ID_Trial_ID__c));
        }
         if(notificationName.equalsIgnoreCase ('NOT_10')){
            if(isUpdate.equalsIgnoreCase ('true')){
                 htmlBody = htmlBody.replace('--UpdatedEmail--', Label.CCL3_Confirmed_Aph_Shipment_Info_Changed);
            }
            else{
                htmlBody = htmlBody.replace('--UpdatedEmail--', '');
            }
	    htmlBody = htmlBody.replace('--Material--', setValueOrEmpty(caseRecord.Material__c));            
            htmlBody = htmlBody.replace('--TreatmentCenter--', setValueOrEmpty(caseRecord.Account.Name));
            htmlBody = htmlBody.replace('--PickupLocationAddress--', setValueOrEmpty(caseRecord.Account.Pickup_Location_Address__c ));
            htmlBody = htmlBody.replace('--BatchId--', setValueOrEmpty(caseRecord.Batch_ID_Trial_ID__c));
            htmlBody = htmlBody.replace('--ApheresisShipmentNotes--', setValueOrEmpty(caseRecord.Aph_Shipment_Notes__c));
            if(caseRecord.Confirmed_Aph_Shipment_Date__c != null){
                htmlBody = htmlBody.replace('--ConfirmedAphShipmentDate--', String.valueof(caseRecord.Confirmed_Aph_Shipment_Date__c));
            }
            else{
                htmlBody = htmlBody.replace('--ConfirmedAphShipmentDate--', '');
            }
            if(caseRecord.Account.Aph_Pickup_Time__c != null){
                htmlBody = htmlBody.replace('--ConfirmedAphpickupTime--', getFormattedTime(caseRecord.Account.Aph_Pickup_Time__c));
            }
            else{
                htmlBody = htmlBody.replace('--ConfirmedAphpickupTime--', '');
            }
            htmlBody = htmlBody.replace('--PickUpLocationContact--', setValueOrEmpty(caseRecord.Account.Pick_Up_Location_Contact__r.name)); 
            htmlBody = htmlBody.replace('--OwnerFullName--', setValueOrEmpty(caseRecord.Owner.Name)); 
        }
          if(notificationName.equalsIgnoreCase ('NOT_11')){
            htmlBody = htmlBody.replace('--TreatmentCenter--', setValueOrEmpty(caseRecord.Account.Name));
            if(caseRecord.Infusion_Date__c != null){
                htmlBody = htmlBody.replace('--InfusionDate--', String.valueof(caseRecord.Infusion_Date__c));
            }
            else{
                htmlBody = htmlBody.replace('--InfusionDate--', '');
            }
            htmlBody = htmlBody.replace('--BatchId--', setValueOrEmpty(caseRecord.Batch_ID_Trial_ID__c));
            if(caseRecord.OBA_Outcome_Due__c != null){
                htmlBody = htmlBody.replace('--OBAOutcomeDue--',String.valueof(caseRecord.OBA_Outcome_Due__c));
            }
            else{
                htmlBody = htmlBody.replace('--OBAOutcomeDue--', '');
            }
            htmlBody = htmlBody.replace('--OwnerFullName--', setValueOrEmpty(caseRecord.Owner.Name));  
        }
          if(notificationName.equalsIgnoreCase ('NOT_12')){
            htmlBody = htmlBody.replace('--TreatmentCenter--', setValueOrEmpty(caseRecord.Account.Name));
            if(caseRecord.Actual_Harvest_Date__c != null){
            htmlBody = htmlBody.replace('--ActualHarvestDate--', String.valueOf(caseRecord.Actual_Harvest_Date__c ));
            }
            else{
                htmlBody = htmlBody.replace('--ActualHarvestDate--', '');
            }
            htmlBody = htmlBody.replace('--BatchId--', setValueOrEmpty(caseRecord.Batch_ID_Trial_ID__c));
            if(caseRecord.Projected_FP_Delivery__c != null){
            htmlBody = htmlBody.replace('--TentativeFPDeliveryDate--', String.valueOf(caseRecord.Projected_FP_Delivery__c ));
            }
            else{
                htmlBody = htmlBody.replace('--TentativeFPDeliveryDate--', '');
            }
            if(caseRecord.Tentative_FP_Delivery_Time__c != null){
                
                
                htmlBody = htmlBody.replace('--TentativeFPDeliveryTime--', getFormattedTime(caseRecord.Tentative_FP_Delivery_Time__c));
            }
            else{
                htmlBody = htmlBody.replace('--TentativeFPDeliveryTime--', '');
            } 
        }
        if(notificationName.equalsIgnoreCase ('NOT_13')){
            if(caseRecord.Actual_Manufacturing_Start_Date__c != null){
            htmlBody = htmlBody.replace('--ActualManufacturingStartDate--', String.valueOf(caseRecord.Actual_Manufacturing_Start_Date__c ));
            }
            else{
                htmlBody = htmlBody.replace('--ActualManufacturingStartDate--', '');
            }
            if(caseRecord.Projected_FP_Delivery__c != null){
            htmlBody = htmlBody.replace('--TentativeFPDeliveryDate--', String.valueOf(caseRecord.Projected_FP_Delivery__c ));
            }
            else{
                htmlBody = htmlBody.replace('--TentativeFPDeliveryDate--', '');
            }
            htmlBody = htmlBody.replace('--BatchId--', setValueOrEmpty(caseRecord.Batch_ID_Trial_ID__c));
        }
        if(notificationName.equalsIgnoreCase ('NOT_14')){
            htmlBody = htmlBody.replace('--Specialtyname--', getsdName(caseRecord.Id));
            htmlBody = htmlBody.replace('--TreatmentCenter--', setValueOrEmpty(caseRecord.Account.Name));
            htmlBody = htmlBody.replace('--ShipTolocationname--', setValueOrEmpty(caseRecord.Account.Ship_To_Account__r.Name));
            htmlBody = htmlBody.replace('--ShipTolocationstreet--', setValueOrEmpty(caseRecord.Account.Ship_To_Account__r.ShippingStreet));
            htmlBody = htmlBody.replace('--ShipTolocationcity--', setValueOrEmpty(caseRecord.Account.Ship_To_Account__r.ShippingCity));
            htmlBody = htmlBody.replace('--ShipTolocationstate--', setValueOrEmpty(caseRecord.Account.Ship_To_Account__r.ShippingState));
            htmlBody = htmlBody.replace('--ShipTolocationpostalcodes--', setValueOrEmpty(caseRecord.Account.Ship_To_Account__r.ShippingPostalCode));
            htmlBody = htmlBody.replace('--ShipTolocationcountry--', setValueOrEmpty(caseRecord.Account.Ship_To_Account__r.ShippingCountry));
            htmlBody = htmlBody.replace('--HospitalPurchaseOrder--', setValueOrEmpty(caseRecord.Hospital_Purchase_Order__c ));
            htmlBody = htmlBody.replace('--HospitalPOcontact--', setValueOrEmpty(caseRecord.Account.Hospital_PO_Contact__r.name ));
            htmlBody = htmlBody.replace('--BatchId--', setValueOrEmpty(caseRecord.Batch_ID_Trial_ID__c));
            htmlBody = htmlBody.replace('--NDC--', setValueOrEmpty(caseRecord.Purpose_NDC_DIN__c)); 
            htmlBody = htmlBody.replace('--License--', setValueOrEmpty(caseRecord.Account.License__c ));
            htmlBody = htmlBody.replace('--Incoterms--', setValueOrEmpty(caseRecord.Incoterms__c ));
            htmlBody = htmlBody.replace('--CaseOwner--', setValueOrEmpty(caseRecord.Owner.Name)); 
        }
        
        return htmlBody;
    }
    public static void createEmailMessage(String caseRecordId ){
        EmailMessage emailMessageRecord = new EmailMessage();     
        emailMessageRecord.status = '3'; // email was sent
        emailMessageRecord.fromAddress = fromAddress; // from address
        emailMessageRecord.MessageDate=datetime.now();       
        emailMessageRecord.subject = subject; // email subject
        emailMessageRecord.htmlBody = htmlBody; // email body
        emailMessageRecord.ToAddress= strRecipients;
        emailMessageRecord.ParentId = caseRecordId;
 
        insert emailMessageRecord;
    
    }  
    public static String listToString(List<String> recipientList) {
      String strRecipients = '' ;
      for(String emailaddress: recipientList){
        strRecipients = strRecipients + '; ' + emailaddress;
      }
      return strRecipients;
    }
    public static String setValueOrEmpty(String value){
        return value != null? value: '';
    }
    public static String getFormattedTime(time value) {
        if( value.hour() < 12 ){ //AM times
			if( value.hour() == 0 && value.minute() < 10){
				return '12:0' + value.minute() + ' AM'; 
			}
			else if(value.hour() == 0 && value.minute() >= 10 ){
				return '12:' + value.minute() + ' AM'; 
			}
			else if(value.hour() < 10 && value.minute() < 10 ){
				return '0' + value.hour() + ':0' + value.minute() + ' AM'; 
			}
			else if(value.hour() < 10 && value.minute() >= 10 ) {
				return '0' + value.hour() + ':' + value.minute() + ' AM'; 
			}
			else if(value.hour() >= 10 && value.minute() < 10) {
				return value.hour() + ':0' + value.minute() + ' AM'; 
			}
			else{
				return value.hour() + ':' + value.minute() + ' AM'; 
			}  
		}
		else
		{//PM times
			if(value.hour() == 12 && value.minute() < 10){
				return '12:0' + value.minute() + ' PM'; 
			}
			else if(value.hour() == 12 && value.minute() >= 10 ){
				return '12:' + value.minute() + ' PM'; 
			}
			else if(value.hour() < 22 && value.minute() < 10 ){
				return '0' + (value.hour()-12) + ':0' + value.minute() + ' PM'; 
			}
			else if(value.hour() < 22 && value.minute() >= 10 ) {
				return '0' + (value.hour()-12) + ':' + value.minute() + ' PM'; 
			}
			else if(value.hour() >= 22 && value.minute() < 10) {
				return (value.hour()-12) + ':0' + value.minute() + ' PM'; 
			}
			else{
				return (value.hour()-12) + ':' + value.minute() + ' PM'; 
			}          
		} 
    }
}
/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for spc_Utility
    *  @date            07/18/2018
    *  @version         1.0
*********************************************************************************************************/
@IsTest
public class spc_UtilityTest {
    @testSetup static void setupTestdata() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
        Account patientAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        patientAcc1.PatientConnect__PC_Date_of_Birth__c = system.today();
        patientAcc1.spc_HIPAA_Consent_Received__c = '';
        patientAcc1.spc_Patient_Services_Consent_Received__c = '';
        patientAcc1.spc_Text_Consent__c = '';
        patientAcc1.spc_Patient_Mrkt_and_Srvc_consent__c = '';
        insert patientAcc1;
        Case programCaseRec2 = spc_Test_Setup.createCases(new List<Account> {patientAcc1}, 1, 'PC_Program').get(0);
        if (null != programCaseRec2) {
            programCaseRec2.Type = 'Program';
            insert programCaseRec2;
        }
        User oUser = spc_Test_Setup.getProfileID();
        insert oUser;
        system.assertNotEquals(programCaseRec2, NULL);
    }
	
    public static testmethod void getRecords() {
        Case inquiryCase  = new Case();
        String recordtypename;
        Set<ID> myIDSet = new Set<ID>();
        Set<String> myStringSet = new Set<String>();
        Map<String, Set<ID>> myMap = new Map<String, Set<ID>>();
        Map<String, Set<String>> myMap2 = new Map<String, Set<String>>();
        List<Case> caselist = new List<Case>();
        Case newcase = new Case();
        Map<String,  List<Case>> myMap3 = new Map<String,  List<Case>>();
        test.startTest();
        spc_Utility.addToMap(myMap, 'key', '4001D000001aGma', myIDSet);
        spc_Utility.addToMap(myMap2, 'key', 'value2', myStringSet);
        spc_Utility.addToMap(myMap3, 'key', newcase, caselist);
        User oUser = [select id from User LIMIT 1];
        spc_utility.checkOwnerIsUser(oUser.id);
    }
	
	public class MyException extends Exception {}
    
    @isTest
    static void testExecution(){
        Test.startTest();
        try{
            spc_Utility.logAndThrowException(new MyException());    
        } catch(Exception e) {
            spc_Utility.createExceptionLog(e);
        }
        Test.stopTest();
    }
    
    @isTest
    static void testGetActualPMRCCode() {
        System.assertEquals('PMRC0002', spc_Utility.getActualPMRCCode('ING#PMRC0002'));
        System.assertEquals('PMRC0002', spc_Utility.getActualPMRCCode('PMRC0002'));
        System.assertEquals(null, spc_Utility.getActualPMRCCode(null));
        System.assertEquals('', spc_Utility.getActualPMRCCode(''));
    }

}
public with sharing class spc_SendBundledEmail {
    
   
    /**
* @Name          getLetters
* @Description	 Create Map of spc_LightningMap for list of eletters
* @Author       Deloitte
* @CreatedDate   2019-05-21
* @Param         Id,String,Boolean
* @Return        List<spc_LightningMap>
*/
    @AuraEnabled
    public static List<spc_LightningMap> getLetters(Id recordId, String invokeAction, boolean isFaxOnly) {
        List<spc_LightningMap> lstAvailableeLetters = new List<spc_LightningMap>(); 
        lstAvailableeLetters=spc_eLetterAuraController.getLetters(recordId,invokeAction,isFaxOnly);
        return lstAvailableeLetters;
    }
    // all code for merge
    
    public class DocumentRecord {
        @AuraEnabled public String Id { get; set; }
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public String recordTypeName { get; set; }
        @AuraEnabled public String categoryName { get; set; }        
        
        @AuraEnabled public String isSelected { get; set; }
        @AuraEnabled public String status { get; set; }  
        
        @AuraEnabled public String cssClassName { get; set; } // used in front end.
        
        public DocumentRecord() {
            
        }
        
        public DocumentRecord(PatientConnect__PC_Document__c a, String isSelect) {
            Id = a.Id;
            name = a.Name;
            recordTypeName = a.RecordType.Name;
            categoryName = a.PatientConnect__PC_Document_Category__c;            
            isSelected = isSelect;
            status=a.PatientConnect__PC_Document_Status__c; 
            
            cssClassName = '';
        }       
        
    } 
 
    /**
* @Name          getDocumentTypes
* @Description	 Get all Document category picklist values
* @Author       Deloitte
* @CreatedDate   2019-05-21
* @Param         
* @Return        Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> 
*/	
    @AuraEnabled
    public static Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> getDocumentTypes (){
        Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> picklistEntryMap = new Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>>();
        picklistEntryMap.put('Type', spc_PicklistFieldManager.getPicklistEntryWrappers(PatientConnect__PC_Document__c.PatientConnect__PC_Document_Category__c));
        return picklistEntryMap;
    }
    /**
* @Name          getLetters
* @Description	 To search all inbound documents
* @Author       Deloitte
* @CreatedDate   2019-05-21
* @Param         Id,String,Boolean
* @Return       List<DocumentRecord>
*/
    @AuraEnabled
    public static List<DocumentRecord> searchRecords(String searchString) {
        try {
            List<DocumentRecord> records = new List<DocumentRecord>();
            List<PatientConnect__PC_Document__c> docs = spc_SendBundledFaxController.getDocuments(searchString);
            DocumentRecord record;
            
            for(PatientConnect__PC_Document__c a : docs) {
                record = new DocumentRecord(a, 'false');  
                records.add(record);
            }
            return records;
        }
        catch(Exception ex){
            spc_Utility.logAndThrowException(ex);     
            return null;
        }
    }
	   /**
* @Name          getRecepients
* @Description	 To get all reciepients
* @Author       Deloitte
* @CreatedDate   2019-05-21
* @Param         Id,String,Boolean
* @Return      List<spc_eLetter_Recipient>
*/
    @AuraEnabled
    public static List<spc_eLetter_Recipient> getRecepients(String letterId, Id recordId, boolean isFaxSpecific) {
        //  String objType = String.valueOf(recordId.getSobjectType());
        List<spc_eLetter_Recipient> recepients = new List<spc_eLetter_Recipient> ();
        recepients=spc_eLetterAuraController.getRecepients(letterId, recordId, isFaxSpecific);
        
        return recepients;    
    }
		   /**
* @Name          sendOutboundDocuments
* @Description	 To send outbound email
* @Author       Deloitte
* @CreatedDate   2019-05-21
* @Param         recipientsString,String,Id
* @Return      string
*/
    @AuraEnabled
    public static string sendOutboundDocuments(String recipientsString, String letterId, Id objectId) {
        String returnMessage = '';
        returnMessage=spc_eLetterAuraController.sendOutboundDocuments(recipientsString,letterId,objectId);
        return returnMessage;
        
    }
    /**
* @Name          SendMergedRecords
* @Description	 To send merged email attachments
* @Author       Deloitte
* @CreatedDate   2019-05-21
* @Param         Id,documents

*/
    @AuraEnabled
    public static void SendMergedRecords(String recordId, String[] documentIds, List<String> reciepients , String letterId) {                
        try {
            String flow = '';
            PatientConnect__PC_eLetter__c eLetter;
            Set<String> attachmentIds = new Set<String>();
            Map<Id, Attachment> mapDocIDToAccIds = new Map<Id ,Attachment>();
            Set<String> pmrcs = new Set<String>();
            List<Attachment> attLst=new List<Attachment>();
            List<PatientConnect__PC_Document__c> docs = [
                SELECT Id, spc_PMRC_Code_New__c,PatientConnect__PC_Attachment_Id__c FROM 
                PatientConnect__PC_Document__c WHERE Id IN : documentIds
            ];
            //getting all attachments from documents selected 
            for(Attachment att : [SELECT Id, Name, ParentId,Body, Parent.Type FROM Attachment where Parent.Type = 'PatientConnect__PC_Document__c'
                                  AND ParentId IN: documentIds
                                 ]){
                                     
                                     mapDocIDToAccIds.put(att.ParentId,att);
                                 }
            if(String.isNotBlank(letterId) && Label.PatientConnect.PC_None != letterId) {         
                eLetter = spc_eLetterService.getELetter(letterId);            
                List<PatientConnect__PC_Engagement_Program_Eletter__c> eletterlst = [
                    SELECT Id, spc_PMRC_Code__c FROM 
                    PatientConnect__PC_Engagement_Program_Eletter__c WHERE PatientConnect__PC_eLetter__c = : letterId LIMIT 1
                ];
                
                for(PatientConnect__PC_Engagement_Program_Eletter__c eltr : eletterlst){
                    pmrcs.add(spc_Utility.getActualPMRCCode(eltr.spc_PMRC_Code__c));
                }
                for(spc_Communication_Framework__mdt letterCode : [Select Flow_Name__c from spc_Communication_Framework__mdt where PMRC_Code__c  in :pmrcs LIMIT 1]){
                    flow = letterCode.Flow_Name__c;
                }
            }
            //putting all the attachments into a list 
            for(PatientConnect__PC_Document__c curDoc : docs){
                if(mapDocIDToAccIds.containsKey(curDoc.Id)){
                    attLst.add(mapDocIDToAccIds.get(curDoc.Id));
                }
                if(String.isBlank(letterId)){
                    pmrcs.add(curDoc.spc_PMRC_Code_New__c);
                }
            }            
            List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();
            spc_CommunicationMgr.Envelop env1 = new spc_CommunicationMgr.Envelop();
            env1.channel = spc_CommunicationMgr.Channel.Email;
            Set<String> rc= new Set<String>();
            //putting all recipients in a set
            for(String rctype:reciepients){
                rc.add(rctype);
            }
            if(!rc.isEmpty()){
                env1.recipientTypes=rc;
            }
            if(String.isBlank(letterId) || Label.PatientConnect.PC_None == letterId){
                env1.FlowName = system.Label.spc_Bundled_flow_name;
            }else{
                env1.FlowName = flow;
                if(null != eLetter){
                    env1.eLetterName = eLetter.Name;
                }
                env1.pmrcs = pmrcs;
            }
            env1.programCaseId = recordId;
            env1.attachmentList=attLst;
            
            envelops.add(env1);
            if(!envelops.isEmpty()){
                spc_CommunicationMgr obj = new spc_CommunicationMgr();
                obj.send(envelops); 
            }
        }
        catch(Exception ex){
            spc_Utility.logAndThrowException(ex);     
        }   
    }
}
@isTest 
public class  SendCaseNotification_test {
    public static testMethod void sendEmail() {
        //avoidsetupnonsetup.emailtemp();
        
        string templateName='TestName';
        string isUpdate = 'true';
      SendCaseNotification MO =new SendCaseNotification();
      CCL3_Purpose__c pobj = new CCL3_Purpose__c();
      pobj.Novartis_Region__c = 'US';
      pobj.Name = 'Test';
      pobj.Commercial__c = true;
      pobj.Indication__c='test';  
      insert pobj;
        

        //aa.C
            
        //Address add = new Address();
        //add.country='Canada';
        //insert add;
        string notificationNa = 'N';
        Account a1 = new Account(name='test',ShippingCountry = 'Canada',shippingStreet='test',ShippingCity='testst',ShippingState='Ontario',ShippingPostalCode='test123');
		insert a1;
       
        Account aa = new Account(name='test',Aph_Pickup_Time__c =Time.newInstance(18, 30, 2, 20),ShippingCountry = 'Canada',shippingStreet='test',ShippingCity='testst',ShippingState='Ontario',ShippingPostalCode='test123',Ship_To_Account__c=a1.Id);
        //  aa.Name = 'Test';
       // aa.ShippingAddress = add;
       Account aaa = new Account(name='testaccount',Account_Email__c='test@novartis.com', ShippingCountry = 'Canada',shippingStreet='test',ShippingCity='testst',ShippingState='Ontario',ShippingPostalCode='test123',Ship_To_Account__c=a1.Id);
        insert aaa;
        
        Novartis_Region_Configuration__c NR= new Novartis_Region_Configuration__c();
        NR.Name='US';
        insert NR;
        Task_Email_Configuration__c TE=new Task_Email_Configuration__c();
        TE.Novartis_Region__c=NR.Id;
        TE.Recipient_Group__c='US Clinical Group';
        TE.From_Email_Address__c='kymriah.cares@novartis.com';
        TE.Additional_Emails__c='test@novartis.com';
        TE.Name=notificationNa;
        TE.isActive__c = true;
        insert TE;
        
        
            
      insert aa;
  	  CCL3_Account_Connection__c ac = new CCL3_Account_Connection__c(AccountFrom__c=aa.id, AccountTo__c=aaa.id, Status__c ='Active', Type__c='Specialty Distributor');
      insert ac;
      
      case c1 = new case();
        
      c1.Status = 'Order Initiated';
      c1.Case_Purpose__c = pobj.Id;
      c1.AccountId = aa.Id;
      c1.Account = aa;
      c1.PRF_ID__c='testprf'; 
      c1.Treatment_Key__c='test1';
      c1.PRF_Received_Date__c = Date.newInstance(2019, 12, 9);  
      c1.Estimated_Aph_Shipment_Date__c = Date.newInstance(2019, 12, 10);
      c1.Plant_Appointment_Date__c = Date.newInstance(2019, 11, 2);  
      c1.Batch_ID_Trial_ID__c = '12345678';
      c1.Hospital_Purchase_Order__c='testorder';  
      c1.PRF_Received_Date__c = Date.newInstance(2019, 12, 12);
      c1.Confirmed_Aph_Shipment_Date__c = Date.newInstance(2019, 12, 12);
      c1.Infusion_Date__c = Date.newInstance(2019, 12, 12);
      c1.OBA_Order__c = 'Yes';
      c1.account.Ship_To_Account__c=a1.id;
      c1.account.Pickup_Location_Address__c='Address';  
      c1.Actual_Harvest_Date__c = Date.newInstance(2019, 12, 12);
      c1.Projected_FP_Delivery__c = Date.newInstance(2019, 12, 12);
      c1.Actual_Manufacturing_Start_Date__c = Date.newInstance(2019, 12, 12); 
      c1.Account.License__c= '12345678';
      c1.Aph_Shipment_Notes__c='test';
      c1.Tentative_FP_Delivery_Time__c=Time.newInstance(18, 30, 2, 20);
     
        
      string purposeTy ='test';
      string templatena = 'test';
      string isUpdatet = 'test';
      insert c1;
        
      case cF = [select id,Ship_To_Address__c from case where id =: c1.id Limit 1 ];

      SendCaseNotification.FlowInputs fo1 = new SendCaseNotification.FlowInputs();
      list<SendCaseNotification.FlowInputs> listflow = new list<SendCaseNotification.FlowInputs>();
      SendCaseNotification.FlowInputs fin = new SendCaseNotification.FlowInputs();
      fin.CaseId=c1.id;
      fin.novartisRegion =pobj.Novartis_Region__c;
      fin.notificationName=notificationNa;
      fin.purposeType=purposeTy;
      fin.isUpdate=isUpdatet;
      fin.templateName =templatena;
      
      listflow.add(fin);
      
      SendCaseNotification.FlowInputs fo = new SendCaseNotification.FlowInputs();
      SendCaseNotification.sendEmail(listflow);
      SendCaseNotification.generateSubject('gagagagaga--TreatmentCenter--','NOT_4',c1); 
      SendCaseNotification.generateSubject('gagagagaga--TreatmentCenter--','NOT_5',c1);       
      SendCaseNotification.generateHtmlBody('test','NOT_4','true',c1);
      SendCaseNotification.generateHtmlBody('test','NOT_5','true',c1);
      SendCaseNotification.generateHtmlBody('test','NOT_6','true',c1);
      SendCaseNotification.generateHtmlBody('test','NOT_7','true',c1);
      SendCaseNotification.generateHtmlBody('test','NOT_8','true',c1);
      SendCaseNotification.generateHtmlBody('test','NOT_9','true',c1);
      SendCaseNotification.generateHtmlBody('test','NOT_10','true',c1);
      SendCaseNotification.generateHtmlBody('test','NOT_11','true',c1);  
      SendCaseNotification.generateHtmlBody('test','NOT_12','true',c1);
      SendCaseNotification.generateHtmlBody('test','NOT_13','true',c1);
      SendCaseNotification.generateHtmlBody('test','NOT_14','true',c1); 
        
    
        
        SendCaseNotification.createEmailMessage(c1.id);
        SendCaseNotification.getOrgWideEmailAddressId('tarun-3.gupta@novartis.com');
        SendCaseNotification.getAdditionalEmails('tarun-3.gupta@novartis.com');
        String[] groupemail = new String[]{'test1','test2','test3','test4','test5'};
        SendCaseNotification.getPublicGroupEmails(groupemail);
        SendCaseNotification.getcaseownerEmails(c1.id);
        SendCaseNotification.getsdEmails(c1.id);
        SendCaseNotification.getsdName(c1.id);
    }
    
}
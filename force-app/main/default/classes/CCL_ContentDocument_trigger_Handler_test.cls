/********************************************************************************************************
*  @author          Deloitte
*  @description     Test class to verify the population of the CCL_Unique_Site_Therapy__c field
*  @param           
*  @date            July 13, 2020
*********************************************************************************************************/

@isTest

public with sharing class CCL_ContentDocument_trigger_Handler_test {
     /*Account Name */
    final static String ACCOUNT_NAME = 'Test Account';
    static testMethod void testContentDoumentTrigger() {

        final list<ContentDocument> cdList= CCL_TestDataFactory.createTestContentDocumenet();
        Boolean exceptionThrown = false;
      final CCL_Order__c order=CCL_Test_SetUp.createTestOrder();                
        final CCL_Apheresis_Data_Form__c adf=CCL_Test_SetUp.createTestADF(ACCOUNT_NAME,order.Id);
        Test.startTest();

        try {
            final ContentDocumentLink cdocLink=CCL_Test_SetUp.createTestContentDocumentLink(cdList[0].Id,adf.Id);
            delete cdList;

        } catch (DMLException e) {
            exceptionThrown = true;

            // assert
            final Boolean expectedExceptionThrown = e.getMessage().contains(System.Label.CCL_FileDeletionError) ? true : false; 
            System.Assert(expectedExceptionThrown, e.getMessage()); 
        } 
        Test.stopTest();
    }

}
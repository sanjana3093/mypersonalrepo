/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This class is used for storing all common methods which will be used by the Test Methods
    *  @date            May 30, 2016
    *  @version         1.0
*********************************************************************************************************/
public with sharing class spc_Test_Setup {
    public static Id ID_PATIENT_RECORDTYPE;
    public static final String PHYSICIAN_RECORD_TYPE = 'PC_Physician';
    public static final String DOC_OBJ = 'PatientConnect__PC_Document__c';
    public static final String ACCOUNT_OBJ = 'Account';
    public static final String CASE_OBJ = 'Case';
    public static final String ACCOUNT_NAME = 'Account Name';
    public static final String CALL_CENTER_AGENT_PROFILE = 'System Administrator';
    public static final String SYS_ADMIN_PROFILE = 'System Administrator';
    public static final string RESTRICTED_PROFILE = 'Read Only';
    public static final String PATIENT_NAME = 'test patient name';
    public static final String MEASUREMENT_TYPE = 'Sleep';

    /*********************************************************************************

    @author      Deloitte
    @Description    : This method is used for creating a account object
    @return Account
    *********************************************************************************/
    public static Account createAccount(ID recordID) {
        Account account = new Account(Name = 'ACCOUNT_NAME', RecordTypeId = recordID, Fax = '1234567', PatientConnect__PC_Email__c = 'test@test.com');
        account.PatientConnect__PC_Date_of_Birth__c = System.today() - 30;
        account.Phone = '1234567890';
        account.spc_Patient_Services_Consent_Received__c = 'Yes';
        account.spc_Patient_Service_Consent_Date__c = Date.valueOf('2014-06-07');
        account.spc_REMS_ID__c = '1234';
        account.PatientConnect__PC_Gender__c = 'Male';
        return account;
    }

    /*********************************************************************************
    @author      Deloitte
    @Description    : This method is used for creating a account object
    @return Account
    *********************************************************************************/
    public static User getProfileID() {
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        List<profile> profileIDs = [select id from profile where name = 'System Administrator' Limit 1];
        User user = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                             EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_US', ProfileId = profileIDs[0].ID,
                             TimeZoneSidKey = 'America/Los_Angeles',
                             UserName = uniqueUserName);
        return user;
    }

    /*********************************************************************************
    @author      Deloitte
    @Description    : This method is used for creating multiple accounts object
    @return Account
    @param String account root name, Integer number of records
    *********************************************************************************/
    public static Account createTestAccount(String recordType) {
        Id accountRecordTypeId = spc_System.recordTypeId(ACCOUNT_OBJ, recordType);
        Account account = new Account(Name = ACCOUNT_NAME, RecordTypeId = accountRecordTypeId);
        account.PatientConnect__PC_Email__c = 'test@test.com';
        account.PatientConnect__PC_Date_of_Birth__c = System.today() - 30;
        account.Phone = '1234567890';
        account.spc_Patient_Services_Consent_Received__c = 'Yes';
        account.spc_Patient_Service_Consent_Date__c = Date.valueOf('2014-06-07');
        account.spc_REMS_ID__c = '1234';
        account.PatientConnect__PC_Gender__c = 'Male';
        return account;
    }
    /*********************************************************************************
    @author      Deloitte
    @Description    : This method is used for creating a document object
    @return Address
    @param String sRecordType, String sStatus
    *********************************************************************************/
    public static PatientConnect__PC_Address__c createTestAddress(Account acc) {
        PatientConnect__PC_Address__c  address = new PatientConnect__PC_Address__c();
        address.PatientConnect__PC_Address_1__c = 'Brown hill';
        address.PatientConnect__PC_Address_2__c = 'Brown hill2';
        address.PatientConnect__PC_City__c = 'New town';
        address.PatientConnect__PC_State__c = 'Brownshire';
        address.PatientConnect__PC_Account__c = acc.Id;
        return address;
    }

    /*********************************************************************************
    @author      Deloitte
    @Description     This method is used for creating an account(patient) object
    @return    Account

    *********************************************************************************/
    public static Account createPatient(String sName) {
        Account oAccount = new Account();
        oAccount.Name = sName;
        oAccount.PatientConnect__PC_First_Name__c = sName;
        oAccount.PatientConnect__PC_Last_Name__c = sName;
        oAccount.RecordTypeId = spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.Account_Patient, Account.getSobjectType());
        oAccount.PatientConnect__PC_Email__c = 'test@test.com';
        oAccount.PatientConnect__PC_Date_of_Birth__c = System.today() - 30;
        oAccount.Phone = '1234567890';
        return oAccount;
    }
    /*********************************************************************************
    @author      Deloitte
    @description     This method is used for creating a support model object
    @return PatientConnect__PC_Engagement_Program__c
      :
    *********************************************************************************/
    public static PatientConnect__PC_Engagement_Program__c createEngagementProgram(String programName, Id manufacturerAccId) {
        PatientConnect__PC_Engagement_Program__c ep = new PatientConnect__PC_Engagement_Program__c (Name = programName, PatientConnect__PC_Manufacturer__c = manufacturerAccId, PatientConnect__PC_Program_Code__c = programName);
        return ep;
    }

    /*********************************************************************************
    @author      Deloitte
    @description    : This method is used for creating a support model object
    @return PatientConnect__PC_eLetter__c

    *********************************************************************************/
    public static PatientConnect__PC_eLetter__c createEeletter(String recordtypeName, String Channel) {
        PatientConnect__PC_eLetter__c eletter = new PatientConnect__PC_eLetter__c (
            Name = 'Test 1',
            PatientConnect__PC_Target_Recipients__c = 'HCO; Treating Physician; Caregiver',
            PatientConnect__PC_Communication_Language__c = 'English',
            PatientConnect__PC_Send_to_User__c = false,
            spc_All_Consents_Mandatory__c = true,
            spc_Consent_Eletter_Needeed__c = 'REMS Consent; Service Consent',
            PatientConnect__PC_Source_Object__c = CASE_OBJ,
            PatientConnect__PC_Allow_Send_to_Patient__c = true,
            PatientConnect__PC_Template_Name__c = 'test',
            spc_eLetter_Data_Migration_Id__c = 'Test',
            PatientConnect__PC_Object_Record_Types__c = recordtypeName
        );
        return eletter;
    }


    /*********************************************************************************
    @author      Deloitte
    @Description    : This method is used for creating a support model object
    @return  PatientConnect__PC_Engagement_Program_Eletter__c

    *********************************************************************************/
    public static PatientConnect__PC_Engagement_Program_Eletter__c createEProgramEletter(ID eLetterID, String channelName, ID programID) {

        PatientConnect__PC_Engagement_Program_Eletter__c peletter = new PatientConnect__PC_Engagement_Program_Eletter__c();
        peletter.PatientConnect__PC_eLetter__c = eLetterID;
        if (channelName == 'Email') {
            peletter.spc_Channel__c = 'Email';
            peletter.spc_PMRC_Code__c = '123678';
        }
        if (channelName == 'Fax') {
            peletter.spc_Channel__c = 'FAX';
            peletter.spc_PMRC_Code__c = '34567';
        }
        if (channelName == 'SMS') {
            peletter.spc_Channel__c = 'SMS';
            peletter.spc_PMRC_Code__c = '34567';
        }
        peletter.PatientConnect__PC_Engagement_Program__c = programID;
        return peletter;

    }
    /*********************************************************************************
    @author      Deloitte
    @Description    : This method is used for creating a user object
    @return   User
    @param String sProfileName, String sUserName, String sAlias
    *********************************************************************************/
    public static User createUser(String sProfileName, String sUserName, String sAlias) {
        // Get the profile details
        Profile p = [SELECT Id FROM Profile WHERE Name = :sProfileName];
        User objUser = new User(Alias = sAlias, email = sUserName, emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', country = 'United States', profileid = p.Id,
                                timezonesidkey = 'America/Los_Angeles', username = sUserName);
        return objUser;
    }
    /*********************************************************************************
    @author      Deloitte
    @Description    : This method is used for creating a Address case object
    @return Case
    *********************************************************************************/

    public static PatientConnect__PC_Health_Plan__c createHealthPlan(Account accountId, String Plantype, String status) {
        PatientConnect__PC_Health_Plan__c hp = new PatientConnect__PC_Health_Plan__c();
        hp.PatientConnect__Patient__c = accountId.id;
        hp.PatientConnect__PC_Plan_Status__c = status;
        hp.PatientConnect__PC_Plan_Type__c = Plantype;

        return hp;
    }
    /*********************************************************************************
    Method Name    : createDocument
    Author         : samiksha Pradhan
    Description    : This method is used for creating a document object
    Return Type    : PC_Document__c
    Parameter      : String sRecordTypeName, String sStatus
    *********************************************************************************/
    public static PatientConnect__PC_Document__c createDocument(String sRecordTypeName, String sStatus, Id engagementProgramId) {
        PatientConnect__PC_Document__c objDocument = new PatientConnect__PC_Document__c();
        objDocument.RecordTypeId = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get(sRecordTypeName).getRecordTypeId();
        objDocument.PatientConnect__PC_Document_Status__c = sStatus;
        objDocument.PatientConnect__PC_Page_Count__c = 5;
        objDocument.PatientConnect__PC_Engagement_Program__c = engagementProgramId;
        return objDocument;
    }
    /*********************************************************************************
    @author      Deloitte
    @Description    : This method is used for creating multiple accounts object
    @return    List<Case>
    @param List <Account> , No of records, String recordType
    *********************************************************************************/
    public static List<Case> createCases(List<Account> patientAccounts, Integer noRecords, String recordType) {
        Id caseRecordType = spc_SYSTEM.recordTypeId(CASE_OBJ, recordType);
        List<Case> newCases = new List<Case>();
        for (integer i = 0; i < noRecords; i ++) {
            Case eachCase = new Case();
            eachCase.AccountId = patientAccounts[i].Id;
            eachCase.RecordTypeId = caseRecordType;
            eachCase.Status = 'New';
            newCases.add(eachCase);
        }
        return newCases;
    }
    /*********************************************************************************
    @author      Deloitte
    @Description    : This method is used for creating a case object
    @return   Case
    @param String parent Id, String accountId, String recordy type
    *********************************************************************************/
    public static Case newCase(Id accountId, String recordType) {
        Id biCaseRecordTypeId = spc_SYSTEM.recordTypeId(spc_ApexConstants.PC_CASE, recordType);
        Case testCase = new Case (
            RecordTypeId = biCaseRecordTypeId,
            AccountId = accountId,
            Status = 'New'
        );
        return testCase;
    }
    /*********************************************************************************
    Method Name    : createInquiryCase
    Author         : Pratik Raj
    Description    : This method is used for creating a Inquiry case object
    Return Type    : Case
    *********************************************************************************/
    public static Case createInquiryCase(Id accountId, ID DocumentID) {

        Case inquiryCase  = new Case();
        inquiryCase.AccountId = accountId;
        inquiryCase.RecordTypeId = spc_InquiryCaseController.getRecordTypeIdForInquiry();
        inquiryCase.Status = 'Open';
        inquiryCase.spc_DocumentID__c =  DocumentID;
        inquiryCase.spc_Send_Consideration_Brochure__c = true;
        return inquiryCase;
    }
    /*********************************************************************************
    Method Name    : createInteraction
    Author         : Sumanta Das
    Description    : This method is used for creating a Inquiry case object
    Return Type    : Case
    *********************************************************************************/

    public static PatientConnect__PC_Interaction__c createInteraction(Id caseId) {
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction.PatientConnect__PC_Patient_Program__c = caseId;
        interaction.PatientConnect__PC_Status__c = 'In Progress';
        return interaction;
    }


    /*********************************************************************************
    Method Name    : createAddress
    Author         : SUbhasmita Kar
    Description    : This method is used for creating a Address case object
    Return Type    : Case
    *********************************************************************************/

    public static PatientConnect__PC_Address__c createAddress(Id accountId) {
        PatientConnect__PC_Address__c address = new PatientConnect__PC_Address__c();
        address.PatientConnect__PC_Account__c = accountId;
        address.PatientConnect__PC_Address_1__c = 'NYC';
        address.PatientConnect__PC_Address_2__c = 'NYC';
        address.PatientConnect__PC_City__c = 'CA';
        address.PatientConnect__PC_Status__c = 'Active';
        return address;
    }
    /*********************************************************************************
    Method Name    : setDataforApexTestConstants
    Author         : Pratik Raj
    Description    : set Data for Apex Test Constants
    Return Type    : Case
    *********************************************************************************/
    public static  List<spc_ApexConstantsSetting__c> setDataforApexConstants() {
        List<sObject> ls = Test.loadData(spc_ApexConstantsSetting__c.sObjectType, 'spc_ApexConstantsSetting_Data');
        Database.insert(ls, false);
        return new List<spc_ApexConstantsSetting__c>();
    }
    /*********************************************************************************
    Method Name    : createAssociation
    Description    :create Association
    Return Type    : PatientConnect__PC_Association__c
    *********************************************************************************/
    public static PatientConnect__PC_Association__c createAssociation(Account account, Case caseObj, String roleName, Date endDate) {
        PatientConnect__PC_Association__c association = new PatientConnect__PC_Association__c (PatientConnect__PC_Account__c = account.Id, PatientConnect__PC_Program__c = caseObj.Id, PatientConnect__PC_Role__c = roleName, PatientConnect__PC_EndDate__c = endDate);
        return (PatientConnect__PC_Association__c)spc_Database.ins(association);
    }

    /*********************************************************************************
    Method Name    : newProgCoverage
    Author         : Deloitte
    Description    : This method is used for creating a Program Coverage records
    Return Type    : PatientConnect__PC_Program_Coverage__c
    Parameter      : Id progCasId, String recordType, String outcomeVal
    *********************************************************************************/
    public static PatientConnect__PC_Program_Coverage__c newProgCoverage(Id progCasId, String recordType, String outcomeVal) {
        Id progCoverageRTId = spc_SYSTEM.recordTypeId('PatientConnect__PC_Program_Coverage__c', recordType);
        PatientConnect__PC_Program_Coverage__c testProgCoverage = new PatientConnect__PC_Program_Coverage__c (
            RecordTypeId = progCoverageRTId,
            PatientConnect__PC_Program__c = progCasId,
            PatientConnect__PC_Program_Coverage__c = outcomeVal,
            PatientConnect__PC_Coverage_Status__c = 'Active'
        );
        return testProgCoverage;
    }
    /*********************************************************************************
    Method Name    : createTask
    Author         :
    Description    : This method is used for creating a Task object
    Return Type    : Task
    Parameter      : String sRecordType, String sStatus,Id ProgCase
    *********************************************************************************/
    public static Task createTask(String sStatus, ID ProgCase, String prior, String subject, String channel, date duedate) {
        Id TaskPSPRT = Schema.SObjectType.Task.getRecordTypeInfosByName().get('PSP Task').getRecordTypeId();
        Task tk = new Task();
        tk.recordtypeid = TaskPSPRT;
        tk.WhatId = ProgCase;
        tk.status = sStatus;
        tk.Priority = prior;
        tk.subject = subject;
        tk.PatientConnect__PC_Channel__c = channel;
        tk.ActivityDate = duedate;
        return tk;
    }

    public static Case createCase(String type, String recordTypeId, String engProgramId, String accountId) {
        Case objCase = new Case();
        objCase.recordtypeid = recordTypeId;
        objCase.Type = type;
        objCase.PatientConnect__PC_Engagement_Program__c = engProgramId;
        objCase.origin = 'Phone';
        objCase.AccountId = accountId;
        return objCase;
    }

    public static Territory createTerritory(String name, Id parentTerritoryid, String developerName) {
        Territory terr1 = new Territory();
        terr1.name = name;
        terr1.ParentTerritoryid = parentTerritoryid;
        terr1.DeveloperName = developerName;
        return terr1;
    }

    public static PatientConnect__PC_Association__c createAssociation(Account account, Case caseObj, String roleName, String status) {
        PatientConnect__PC_Association__c association = new PatientConnect__PC_Association__c (PatientConnect__PC_Account__c = account.Id, PatientConnect__PC_Program__c = caseObj.Id, PatientConnect__PC_Role__c = roleName, PatientConnect__PC_AssociationStatus__c = status);
        return association;
    }

    public static Task createTask(String status, ID whatId, String priority, String subject, String channel, Date dueDate,
                                  String category, String subCat, String assignedTo, String outcome, String direction, String description) {
        Id recordType = Schema.SObjectType.Task.getRecordTypeInfosByName().get('PSP Task').getRecordTypeId();
        Task tk = new Task();
        tk.recordtypeid = recordType;
        tk.WhatId = whatId;
        tk.status = status;
        tk.Priority = priority;
        tk.subject = subject;
        tk.PatientConnect__PC_Channel__c = channel;
        tk.ActivityDate = dueDate;
        tk.PatientConnect__PC_Category__c = category;
        tk.PatientConnect__PC_Sub_Category__c = subCat;
        tk.PatientConnect__PC_Assign_to_Role__c = assignedTo;
        tk.PatientConnect__PC_Call_Outcome__c = outcome;
        tk.PatientConnect__PC_Direction__c = direction;
        tk.Description = description;
        return tk;
    }
}
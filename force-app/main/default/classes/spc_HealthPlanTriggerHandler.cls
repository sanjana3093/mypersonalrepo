/**
* @author Deloitte
* @date 17/07/2018
*
* @description This is the Trigger Handler class for Health Plan Trigger
*/
public class spc_HealthPlanTriggerHandler extends spc_TriggerHandler {
	spc_HealthPlanTriggerImplementation healthplanImp = new spc_HealthPlanTriggerImplementation();

	public override void beforeInsert() {

		List<PatientConnect__PC_Health_Plan__c> activeHealthPlans = new List<PatientConnect__PC_Health_Plan__c>();

		for (PatientConnect__PC_Health_Plan__c healthPlan : (List<PatientConnect__PC_Health_Plan__c>)Trigger.New) {

			//Validate if a new active health plan is created
			if (healthPlan.PatientConnect__PC_Plan_Status__c == spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.HEALTHPLAN_STATUS_ACTIVE)) {
				activeHealthPlans.add(healthPlan);
			}
		}

		if (!activeHealthPlans.isEmpty()) {
			healthplanImp.validateActiveHealthPlans(activeHealthPlans);
		}

	}

	public override void beforeUpdate() {

		PatientConnect__PC_Health_Plan__c oldHP;
		List<PatientConnect__PC_Health_Plan__c> activeHealthPlans = new List<PatientConnect__PC_Health_Plan__c>();

		for (PatientConnect__PC_Health_Plan__c healthPlan : (List<PatientConnect__PC_Health_Plan__c>)Trigger.New) {
			oldHP = (PatientConnect__PC_Health_Plan__c)Trigger.oldMap.get(healthPlan.Id) ;

			//Validate if an existing health plan is made active
			if (healthPlan.PatientConnect__PC_Plan_Status__c != oldHP.PatientConnect__PC_Plan_Status__c && healthPlan.PatientConnect__PC_Plan_Status__c == spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.HEALTHPLAN_STATUS_ACTIVE)) {
				activeHealthPlans.add(healthPlan);
			}
		}

		if (!activeHealthPlans.isEmpty()) {
			healthplanImp.validateActiveHealthPlans(activeHealthPlans);
		}
	}
}
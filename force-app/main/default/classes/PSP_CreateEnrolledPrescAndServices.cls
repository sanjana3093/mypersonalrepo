/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the controller for the Enrollment Flows
*  @date            09/18/2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Shourya Solipuram          October 18, 2019     Initial Version
****************************************************************************************************************/
public with sharing class PSP_CreateEnrolledPrescAndServices {
    /* Constructor */
    private PSP_CreateEnrolledPrescAndServices() {
        //Private consturctor 
    }
    /**************************************************************************************
     * @author      : Deloitte
     * @date        : 10/18/2019
     * @Description : create Enrollee Product Records
     * @Param       : Enrollee Id
     * @Param       : CareProgram Id
     * @Param       : Patient Name
     * @Return      : List<CareProgramEnrolleeProduct>
     ***************************************************************************************/
    @AuraEnabled
    public static List < CareProgramEnrolleeProduct > createEnrolledPrescAndServices(String enrolleeId, String careProgramId, String patientName) {
        final List < Id > cProProductIdList = new List < Id > ();
        final Map < Id, String > mapIDToPro = new Map < Id, String > ();
        CareProgramEnrolleeProduct insertRec;
        final List < CareProgramEnrolleeProduct > insertRecs = new List < CareProgramEnrolleeProduct > ();
        try {
            if(CareProgramProduct.sObjectType.getDescribe().isAccessible()) {
                for (CareProgramProduct defaultPro: [Select id, PSP_Default__c, CareProgramId, ProductId, Product.Name From CareProgramProduct where PSP_Default__c = true AND CareProgramId =: careProgramId]) {
                    cProProductIdList.add(defaultPro.Id);
                    mapIDToPro.put(defaultPro.Id, defaultPro.Product.Name);
                }
                if (!cProProductIdList.isEmpty()) {
                    for (Id cPRoProductId: cProProductIdList) {
                        insertRec = new CareProgramEnrolleeProduct();
                        insertRec.Name = mapIDToPro.get(cPRoProductId) + ' - ' + string.valueOf(System.now().date());
                        insertRec.CareProgramEnrolleeId = enrolleeId;
                        insertRec.CareProgramProductId = cPRoProductId;
                        insertRecs.add(insertRec);
                    }
                    if (!insertRecs.isEmpty()) {
                        Database.insert(insertRecs);
                    }
                }
            }
        } catch (AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }
        return insertRecs;
    }

    /**************************************************************************************
     * @author      : Deloitte
     * @date        : 10/18/2019
     * @Description : Check the access of an object for the user
     * @Return      : Boolean
     ***************************************************************************************/
    @AuraEnabled
    public static boolean checkAccess() {
        final Set < Id > permissionSets = new Set < Id > ();
		Boolean hasAccess = false;
        try {
        if(PermissionSetAssignment.sObjectType.getDescribe().isAccessible()) {
            for (PermissionSetAssignment up: [SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId =: UserInfo.getUserId()]) {
                permissionSets.add(up.PermissionSetId);
            }
        }
        if(ObjectPermissions.sObjectType.getDescribe().isAccessible()) {
            for (ObjectPermissions objPerm: [SELECT PermissionsCreate FROM ObjectPermissions WHERE ParentId IN: permissionSets And sObjectType = 'Account']) {
                if (objPerm.PermissionsCreate) {
                    hasAccess = true;
                    break;
                }
            }
        }
        } catch (AuraHandledException e) {
            System.debug(e.getMessage());
            throw e;
        }    
        return hasAccess;
    }

}
/********************************************************************************************************
*  @author          Deloitte
*  @description     This class is used for storing all common methods which will be used by the Test Methods
*  @date            June 18, 2020
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Deloitte          June 18 2020         Initial Version
****************************************************************************************************************/
public with sharing class CCL_Test_SetUp {
    
    /*Address */
    final static String COUNTRY_ADDRESS = 'United States';
    /*status */
    final static String STATUS_PENDING_APPROVAL = 'ADF Pending Approval';
    /*ADF Status Awaiting ADF */
    final static String STATUS_AWAITING_ADF = 'Awaiting ADF';
    private CCL_Test_SetUp () {
        //Private constructor to prevent instantiation
    }
   
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating ADF
@return           ADF
*********************************************************************************/
    public static CCL_Apheresis_Data_Form__c createTestADF(String name,Id orderId) {
        final CCL_Apheresis_Data_Form__c adfObj= new CCL_Apheresis_Data_Form__c();
        adfObj.CCL_Patient_Name__c=name;
        adfObj.CCL_Order__c=orderId;
        adfObj.CCL_Status__c=STATUS_PENDING_APPROVAL;
        if(Schema.sObjectType.CCL_Apheresis_Data_Form__c.isCreateable()) {
        insert adfObj;
      }
        return adfObj;
    }
    
/*********************************************************************************************
@author           Deloitte
@Description      This method is used for creating ADF with Awaiting ADF and Collection center
@return           ADF
***********************************************************************************************/
    public static CCL_Apheresis_Data_Form__c createTestADFAwaitingADF(String name,Id orderId,Id accountId) {
        final CCL_Apheresis_Data_Form__c adfObj= new CCL_Apheresis_Data_Form__c();
        adfObj.CCL_Patient_Name__c=name;
        adfObj.CCL_Order__c=orderId;
        adfObj.CCL_Apheresis_Collection_Center__c=accountId;
        adfObj.CCL_Status__c=STATUS_AWAITING_ADF;
        if(Schema.sObjectType.CCL_Apheresis_Data_Form__c.isCreateable()) {
        insert adfObj;
      }
        return adfObj;
    }
    
    
    
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating account
@return           Account
*********************************************************************************/
    public static Account createTestAccount(String name,Id timezone) {
        final Account  acc = new Account ();
        acc.Name = name;
        acc.ShippingCountry=COUNTRY_ADDRESS;
        acc.CCL_Active__c=True;
        acc.CCL_Label_Compliant__c='SEC';
        acc.CCL_Type__c= CCL_StaticConstants_MRC.ACCOUNT_TYPE_VENDOR;
        acc.CCL_Capability__c='L1O';
        acc.CCL_Time_Zone__c=timezone;
        acc.CCL_External_ID__c='1234567';
        if(Schema.sObjectType.Account.isCreateable()) {
        insert acc;
      }
        return acc;
    }
    
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating timezone
@return           ADF
*********************************************************************************/
    public static CCL_Time_Zone__c createTestTimezone(String name) {
        final CCL_Time_Zone__c timezone= new CCL_Time_Zone__c();
        timezone.CCL_External_ID__c='8787887';
        timezone.Name=name;
        timezone.CCL_SAP_Time_Zone_Key__c=STATUS_PENDING_APPROVAL;
        timezone.CCL_Saleforce_Time_Zone_Key__c='123456';
         if(Schema.sObjectType.CCL_Time_Zone__c.isCreateable()) {
        insert timezone;
      }
        return timezone;
    }
    
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating summary
@return           summary
*********************************************************************************/
    public static CCL_Summary__c createTestSummary(String name,Id adfId) {
        final CCL_Summary__c summary= new CCL_Summary__c();
        summary.Name=name;
        summary.CCL_Apheresis_Data_Form__c=adfId;
        if(Schema.sObjectType.CCL_Summary__c.isCreateable()) {
        insert summary;
      }
        return summary;
    }
     /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating summary
@return           summary
*********************************************************************************/
    public static CCL_Batch__c createTestBatch(String name,Id summaryId) {
        final CCL_Batch__c batch= new CCL_Batch__c();
        batch.Name=name;
        batch.CCL_Collection__c=summaryId;
        return batch;
    }
      /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating therapy
@return           summary
*********************************************************************************/
    public static CCL_Therapy__c createTestTherapy(String name) {
        final CCL_Therapy__c therapy= new CCL_Therapy__c();
        therapy.Name=name;
        therapy.CCL_Type__c='Commercial';
        therapy.CCL_Status__c='Active';
        therapy.CCL_Description__c='Test';
        therapy.CCL_Therapy_Description__c='Test';
        if(Schema.sObjectType.CCL_Summary__c.isCreateable()) {
            insert therapy;
        }
        return therapy;
    }
      /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating Therapy Association
@return           summary
*********************************************************************************/
    public static CCL_Therapy_Association__c createTestTherapyAssociation(Id therapyId ) {
        string RecordTypeSite='Site';
        final CCL_Therapy_Association__c therapyAssoc= new CCL_Therapy_Association__c();
        ID recordTypeId = Schema.SObjectType.CCL_Therapy_Association__c.getRecordTypeInfosByName().get(RecordTypeSite).getRecordTypeId();
        therapyAssoc.CCL_Therapy__c=therapyId;
        therapyAssoc.RecordTypeId=recordTypeId;
        therapyAssoc.CCL_Infusion_Data_Collection__c ='No';
        therapyAssoc.CCL_Hospital_Patient_ID_Opt_In__c='No';
        return therapyAssoc;
    }
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating User Therapy Association
@return           summary
*********************************************************************************/
    public static CCL_User_Therapy_Association__c createTestUserTherapyAssociation(Id therapyAssocId ) {
        string RecordTypeSite='External';
        final CCL_User_Therapy_Association__c userTherapyAssoc= new CCL_User_Therapy_Association__c();
        ID recordTypeId = Schema.SObjectType.CCL_User_Therapy_Association__c.getRecordTypeInfosByName().get(RecordTypeSite).getRecordTypeId();
        userTherapyAssoc.Name='Test';
        userTherapyAssoc.CCL_User__c=UserInfo.getUserId();
        userTherapyAssoc.CCL_Primary_Role__c='PRF Submitter';
        userTherapyAssoc.CCL_Therapy_Association__c=therapyAssocId;
        userTherapyAssoc.RecordTypeId=recordTypeId;
        userTherapyAssoc.CCL_Active__c=true;
        return userTherapyAssoc;
    }
     /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating shipmnet
@return           shipmnet
*********************************************************************************/
    public static CCL_Shipment__c createTestShippmnt(Id adfId,Id orderId) {
        final CCL_Shipment__c shipment= new CCL_Shipment__c();
        shipment.CCL_Apheresis_Data_Form__c=adfId;
        shipment.Name='TEST';
        if(Schema.sObjectType.CCL_Shipment__c.isCreateable()) {
        insert shipment;
      }
        return shipment;
    }
      /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating Order
@return           shipmnet
*********************************************************************************/
    public static CCL_Order__c createTestOrder() {
        final CCL_Order__c order= new CCL_Order__c();        
        order.RecordTypeId=CCL_StaticConstants_MRC.ORDER_RECORDTYPE_CLINICAL;
        order.CCL_Order_Approval_Eligibility__c =false;  
        order.Name='TEST';
        if(Schema.sObjectType.CCL_Order__c.isCreateable()) {
        insert order;
      }
        return order;
    }
      /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating contentDocumentLink
@return           shipmnet
*********************************************************************************/
public static ContentDocumentLink createTestContentDocumentLink(Id ContentocId,Id linkId) {
    final ContentDocumentLink doc= new ContentDocumentLink();
    doc.ContentDocumentId=ContentocId;
    doc.LinkedEntityId=linkId;
     if(Schema.sObjectType.ContentDocumentLink.isCreateable()) {
    insert doc;
	      }
    return doc;
}
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating summary
@return           summary
*********************************************************************************/
public static CCL_Summary__c createTestSummaryByRecordType(Id recordTypeId ,Id adfId,Id orderId) {
    final CCL_Summary__c summaryObj= new CCL_Summary__c();
    summaryObj.CCL_Manufacturing_Start_Date_Time__c=System.today();
    summaryObj.RecordTypeId=recordTypeId;
    summaryObj.CCL_Apheresis_Data_Form__c=adfId;
    summaryObj.CCL_Order__c=orderId;
    if(Schema.sObjectType.CCL_Summary__c.isCreateable()) {
    insert summaryObj;
	      }
    return summaryObj;
}
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating batch
@return           Batch
*********************************************************************************/
public static CCL_Batch__c createTestBtch(Id adfId, Id orderId){
    CCL_Batch__c bag=new CCL_Batch__c();
    bag.CCL_Cryobag_ID__c='56677';
    bag.CCL_Apheresis_Data_Form__c=adfId;
    bag.CCL_Order__c=orderId;
    if(Schema.sObjectType.CCL_Batch__c.isCreateable()) {
    insert bag;
	      }
    return bag;
}
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating General Settings
@return           Batch
*********************************************************************************/
public static CCL_GeneralSettings__c createTestSettings(){
    CCL_GeneralSettings__c generalSettingsObj = new CCL_GeneralSettings__c();
    generalSettingsObj.Name = 'DefaultDateFormat';
    generalSettingsObj.CCL_Value__c = 'dd-MMM-yyyy';
     Database.insert( generalSettingsObj);
    return generalSettingsObj;
}
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating shipment with recordtype
@return           Batch
*********************************************************************************/
public static CCL_Shipment__c createShipmentByRecType(Id adfId, Id recordTypeId,Id orderId){
    final CCL_Shipment__c shipment= new CCL_Shipment__c();
    shipment.CCL_Apheresis_Data_Form__c=adfId;
    shipment.CCL_Order__c = orderId;
    shipment.CCL_Order_Apheresis__c= orderId;
    //shipment.Name='TEST';
    shipment.RecordTypeId = recordTypeId;
    shipment.CCL_Planned_Dewar_Arrival_Date_Time__c=system.today();
    shipment.CCL_Planned_Apheresis_Pickup_Date__c=system.today();
    shipment.CCL_Actual_Goods_Receipt_Date_Time__c=system.today();
    final CCL_Therapy__c therapy= CCL_Test_SetUp.createTestTherapy('Test_Therapy_123');
    final CCL_Time_Zone__c tmz =CCL_Test_SetUp.createTestTimezone('EST');
    final Account acc= CCL_Test_SetUp.createTestAccount('Test',tmz.Id);
    shipment.CCL_Pick_up_Location__c=acc.Id;
    shipment.CCL_Collection_Center__c=acc.Id;
    shipment.CCL_Ordering_Hospital__c=acc.Id;
    shipment.CCL_Indication_Clinical_Trial__c = therapy.Id;
	shipment.CCL_Bypass_Shipment__c = true;
    final CCL_Therapy_Association__c therapyAssoc= CCL_Test_SetUp.createTestTherapyAssociation(therapy.Id);
    
    therapyAssoc.CCL_Site__c=acc.Id;
    if(Schema.sObjectType.CCL_Therapy_Association__c.isCreateable()) {
         	insert therapyAssoc;
         }
    final CCL_User_Therapy_Association__c userTherapyAssoc= CCL_Test_SetUp.createTestUserTherapyAssociation(therapyAssoc.Id);    
    if(Schema.sObjectType.CCL_User_Therapy_Association__c.isCreateable()) {
         	insert userTherapyAssoc;
         }
    //update therapyAssoc;
    
    if(Schema.sObjectType.CCL_Shipment__c.isCreateable()) {
    insert shipment;
	      }
    return shipment;
}
        /**
*  @author          Deloitte
*  @description     Create a Apheresis Data Form(ADF) object for the purpose of Unit Test activity. 
*  @param           
*  @return          Apheresis Data Form Obj    
*  @date            
*/ 
    public static CCL_Apheresis_Data_Form__c createTestApheresisDataForm(CCL_Order__c orderRec){
        CCL_Apheresis_Data_Form__c testADF = new CCL_Apheresis_Data_Form__c();
        testADF.CCL_Date_Of_Birth__c = date.newinstance(1960, 2, 17);
        testADF.CCL_Treatment_Protocol_SubID_HospitalID__c = '47502';
        testADF.CCL_Patient_Weight__c = 90;
        testADF.CCL_Order__c =orderRec.Id;
        testADF.CCL_Returning_Patient__c = True;
        testADF.CCL_Est_Apheresis_Shipment_Date__c = datetime.newInstance(2019, 8, 25, 12, 30, 0);
        testADF.CCL_Estimated_FP_Delivery_Date__c = datetime.newInstance(2020, 9, 30, 11, 30, 0);
        testADF.CCL_Patient_Name__c='Test Apheresis Data Form';
        if(Schema.sObjectType.CCL_Apheresis_Data_Form__c.isCreateable()) {
        insert testADF;
	      }

        return testADF;
    }
	
	     /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating document record
@return           CCL_Document__c
*********************************************************************************/
public static CCL_Document__c createTestDocuemnt(Id orderId){
    CCL_Document__c doc=new CCL_Document__c();
    doc.Name='Test';
    doc.CCL_Order__c=orderId;
    if(Schema.sObjectType.CCL_Document__c.isCreateable()) {
        insert doc;   
    }
    return doc;
}
}
/********************************************************************************************************
*  @author          Deloitte
*  @description     This helper class was created to support the notification engine.
*  @date            July 21, 2020
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Deloitte                  July 21 2020         Initial Version
****************************************************************************************************************/
public without sharing class CCL_Notification_Helper {

    /*
    *   @author           Deloitte
    *   @description      Create a Query using Object Name and fieldSetName
    *   @para             String, String
    *   @return           String
    *   @supportLogs      
    *   @date             4 August 2020
    */
    public static string generateQueryFromFieldSet(String ObjectName, String FieldSetName){
        Set<String> strFieldsToReturn = CCL_Notifications_Utility.convertFieldSetToStringSet(ObjectName,FieldSetName);
        String strSOQLQuery = CCL_Notifications_Constants.SELECT_ID_COMA_DYNAMIC_QUERY_VALUE;
        for(String FieldName: strFieldsToReturn){
            strSOQLQuery = strSOQLQuery + FieldName + ', '; 
        }
        return strSOQLQuery.substringBeforeLast(',') + CCL_Notifications_Constants.FROM_DYNAMIC_QUERY_VALUE + objectname;
        
    }

    /*
    *   @author           Deloitte
    *   @description      This method will return a string after resolving the merge fields in it
    *   @para             String(Email content having merge fields), SObject(Current Record Information), Pattern
    *   @return           String
    *   @supportLogs      System.debug('::CCL_Notification_Helper ::getNotificationContent ::  ' + );
    *   @date             4 August 2020
    */
    public static String  buildEmailContent (String source, SObject sObjectRecord, Pattern regexPattern ) {
        String SourceMatcher = source;
        final Matcher regexMatcher = regexPattern.matcher(SourceMatcher);
        while(regexMatcher.find()) {
            SourceMatcher = CCL_Notifications_Utility.resolveMergeFields(SourceMatcher, sObjectRecord, regexPattern);
        }
        return SourceMatcher;
    }
  
    /*
    *   @author           Deloitte
    *   @description      Method to prepare notification records
    *   @para             List<CCL_Notification_DTO>
    *   @return           List<CCL_Notification__c>
    *   @supportLogs     
    *   @date             8 August 2020
    */
    public static List<CCL_Notification__c> buildNotificationRecords (List<CCL_Notification_DTO> listDTOInstance ) {
       List<CCL_Notification__c> listNotificationPerRecord = new List<CCL_Notification__c>();
         // Must Consider User field as Blank and Contact being populated
         String communityBaseURL=''; // Variable to hold base URL of Community from the custom Setting
         String internalbaseURL ='';  // Variable to hold base URL of Community from the custom Setting
        
         Map<String,String> notificationSignatureNameByUniqueCombination = new Map<String,String>();
         notificationSignatureNameByUniqueCombination = CCL_Notification_Accessor.fetchAllNotificationSignatures();
        
         if(CCL_Notification_Extension__c.getValues('Community Base URL')!=null) {
            communityBaseURL = CCL_Notification_Extension__c.getValues('Community Base URL').CCL_Value__c;
         }
         if(CCL_Notification_Extension__c.getValues('Internal Base URL')!=null) {
            internalbaseURL = CCL_Notification_Extension__c.getValues('Internal Base URL').CCL_Value__c;
         }
         String ccEmailId=''; 
         for (CCL_Notification_DTO notificationInstance : listDTOInstance) {
            // We now have the instances per Notification Setting 
            System.debug('::CCL_Notification_Helper ::buildNotificationRecords ::notificationInstance ::Outer Loop '+ notificationInstance);
            String languageOfNotificationDTOInstance  = notificationInstance.notificationContentMetadataRecord.CCL_Language__c;
            System.debug('::CCL_Notification_Helper ::buildNotificationRecords ::notificationInstance ::Outer Loop :: Language '+ languageOfNotificationDTOInstance);
            String therapyIdPerUserPreference='';
            String therapyTypePerUserPreference= '';
            
            String uniqueCombinationForSignatureTemplate='';
            for(SObject userPreferenceInstance : notificationInstance.userPreferenceObjectList ) {
                // For each user preference we want to stamp the record with the unique Id 
                System.debug('::CCL_Notification_Helper ::buildNotificationRecords ::notificationInstance ::Inner Loop :: userPreferenceInstance '+ userPreferenceInstance);
            
                String emailSignatureName='';
                //TODO - CGTU2369 - 'or' argument to be removed before pushing to test and language on notfication content to be made mandatory and populated on all records
                if(languageOfNotificationDTOInstance == (String)userPreferenceInstance.get('CCL_Language__c')|| languageOfNotificationDTOInstance == null) {
                        if ((String)CCL_RelationshipQueryUtil.getFieldValue(userPreferenceInstance,'CCL_Therapy_Association__r.CCL_Therapy__r.name')!= null) {
                        therapyIdPerUserPreference = (String)CCL_RelationshipQueryUtil.getFieldValue(userPreferenceInstance,'CCL_Therapy_Association__r.CCL_Therapy__r.name');
                    }

                   //if ((String)CCL_RelationshipQueryUtil.getFieldValue(userPreferenceInstance,'CCL_Therapy_Association__r.CCL_Therapy__r.CCL_Type__c')!= null) {
                       therapyTypePerUserPreference = notificationInstance.notificationSettingMetadataRecord.CCL_Category__c;
                   //}

                    CCL_Notification__c notificationRecordPerUser = new CCL_Notification__c(); 

                    // Details located on Notification Settings Metadata Types Notification Name
                    String recordURL='';
                    Object recordtype = userPreferenceInstance.get('RecordTypeId');
                    if(recordtype!=null) {
                        if((String)recordtype == CCL_Notifications_Constants.INTERNAL_USER) {
                            recordURL = internalbaseURL+notificationInstance.recordId;
                            final String countryCodeForCountryTherapyAsso = (String)CCL_RelationshipQueryUtil.getFieldValue(userPreferenceInstance,'CCL_Therapy_Association__r.CCL_Country__c' );
                            /*Below code prepares a unique combination of Country+TherapyId+NotifcationType
                            and populates the master signature template name on the Notification record*/
                            uniqueCombinationForSignatureTemplate= countryCodeForCountryTherapyAsso +'_' + therapyIdPerUserPreference + '_' + therapyTypePerUserPreference;
                            if(notificationSignatureNameByUniqueCombination.containsKey(uniqueCombinationForSignatureTemplate)){
                                emailSignatureName=notificationSignatureNameByUniqueCombination.get(uniqueCombinationForSignatureTemplate);
                            }
                        } else if((String)recordtype== CCL_Notifications_Constants.EXTERNAL_USER) {
                            recordURL = communityBaseURL+notificationInstance.recordId;
                            final String countryCodeForSiteTherapyAsso = 
                            ((String)CCL_RelationshipQueryUtil.getFieldValue(userPreferenceInstance,'CCL_Therapy_Association__r.CCL_Site__r.ShippingCountryCode' ));
                            /*Below code prepares a unique combination of Country+TherapyId+NotifcationType
                            and populates the master signature template name on the Notification record*/
                            uniqueCombinationForSignatureTemplate= countryCodeForSiteTherapyAsso +'_' + therapyIdPerUserPreference + '_' + therapyTypePerUserPreference;
                            if(notificationSignatureNameByUniqueCombination.containsKey(uniqueCombinationForSignatureTemplate)){
                                emailSignatureName=notificationSignatureNameByUniqueCombination.get(uniqueCombinationForSignatureTemplate);
                            }

                        }
                    }

                    
                    System.debug('::CCL_Notification_Helper ::buildNotificationRecords ::CountryCodeForCountryTherapyAsso ::emailSignatureName' + emailSignatureName);

                    notificationRecordPerUser.CCL_Notification_Type__c = notificationInstance.NotificationType;
                    notificationRecordPerUser.CCL_Email_Template__c = emailSignatureName;
                    notificationRecordPerUser.CCL_Delivery_Mode__c = notificationInstance.notificationSettingMetadataRecord.CCL_Channel_Delivery_Mode__c;
                    notificationRecordPerUser.CCL_Notification_Number__c = notificationInstance.notificationSettingMetadataRecord.CCL_Notification_Number__c;


                    if(!String.isBlank(notificationInstance.emailBody)) {
                        notificationRecordPerUser.CCL_Email_Body__c= notificationInstance.emailBody.replaceALL(CCL_Notifications_Constants.RECORD_DETAIL_LINK_LITERAL_VALUE,recordURL);
                        
                    }
                    //TODO- CGTU2369 add matching signature to the end of email body
                    //notificationRecordPerUser.CCL_Email_Body__c = notificationInstance.emailBody;
                    notificationRecordPerUser.CCL_Subject__c = notificationInstance.emailSubject;
                    // New Custom Metadata CCL_FieldMapping                                      
                    notificationRecordPerUser.CCL_Recipient_User__c = (String)userPreferenceInstance.get('CCL_User__c');
                    
                    // Call in the invocable class 
                    // Changes made for CGTU-212. Adding the secondary email to notificatuon n record, if user i=has secondary email.
                    //CGTU-2369 Logic to prioritise secondary email if it is populated, even if recipient email is populated on user
                    Object secondaryEmailobj =CCL_RelationshipQueryUtil.getFieldValue(userPreferenceInstance,'CCL_User__r.CCL_Secondary_Email__c');

                    if(secondaryEmailobj==null) {
                        notificationRecordPerUser.CCL_Recipient_Email_Id__c = (String)CCL_RelationshipQueryUtil.getFieldValue(userPreferenceInstance,'CCL_User__r.Email' );

                    } else {
                        notificationRecordPerUser.CCL_Secondary_Email__c = (String)secondaryEmailobj;

                    }
                    // Changes for CGTU-212 over
                
                    // Logic to not send the same email to same user again
                    Boolean currentUserAlreadyExistInNotificationList= false;
                    if((String.isNotBlank(ccEmailId) && String.isNotBlank(notificationRecordPerUser.CCL_Recipient_Email_Id__c) && ccEmailId.containsIgnoreCase(notificationRecordPerUser.CCL_Recipient_Email_Id__c)
                        || (String.isNotBlank(ccEmailId) && String.isNotBlank(notificationRecordPerUser.CCL_Secondary_Email__c) && ccEmailId.containsIgnoreCase(notificationRecordPerUser.CCL_Secondary_Email__c)))) {
                            currentUserAlreadyExistInNotificationList=true;
                    }

                    if(!currentUserAlreadyExistInNotificationList && String.isNotBlank(notificationRecordPerUser.CCL_Secondary_Email__c)) {
                        ccEmailId= ccEmailId + notificationRecordPerUser.CCL_Secondary_Email__c+'; ';
                    } else if(!currentUserAlreadyExistInNotificationList) {
                        ccEmailId= ccEmailId + notificationRecordPerUser.CCL_Recipient_Email_Id__c+'; ';
                    }
                    if(!currentUserAlreadyExistInNotificationList) {
                        listNotificationPerRecord.add(notificationRecordPerUser);
                    }
                    System.debug('::CCL_Notification_Helper ::buildNotificationRecords ::listNotificationPerRecord ' + JSON.serialize(listNotificationPerRecord) );
                    System.debug('::CCL_Notification_Helper ::buildNotificationRecords ::ccEmailId ' + JSON.serialize(ccEmailId) );
                
                }
            }
        }
            if(ccEmailId != null) {
                ccEmailId = ccEmailId.removeEnd('; ');
            }
            
            for(CCL_Notification__c notificationRecordPerUser:listNotificationPerRecord) {
                notificationRecordPerUser.CCL_Other_Email_Recipients__c = ccEmailId;
                
            }
        System.debug('::CCL_Notification_Helper ::buildNotificationRecords ::listNotificationPerRecord ::ReturningValue ' + JSON.serialize(listNotificationPerRecord) );
        return listNotificationPerRecord;
    }
}
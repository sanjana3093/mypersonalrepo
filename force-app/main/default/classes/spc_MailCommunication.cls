//Do not commit this class
public class spc_MailCommunication  implements spc_CommunicationMgr.ICommunication {
	public void send(List<spc_CommunicationMgr.Envelop> envelops
	                 , Map<String, PatientConnect__PC_Engagement_Program_Eletter__c> mapEmailTemplates
	                 , Map<String, Map<String, Account>> mapRecipients, Map<Id, Contact> mapAccountContacts,Map<Id,List<sObject>> additionalObjects) {
		for (spc_CommunicationMgr.Envelop env : envelops) {
			PatientConnect__PC_Engagement_Program_Eletter__c eLetter = mapEmailTemplates.get(env.flowName + '|' + String.valueOf(env.channel));
			if (eLetter != null) {
				env.outboundDoc.spc_PMRC_Code_New__c = spc_Utility.getActualPMRCCode(eLetter.spc_PMRC_Code__c);

			}
		}
	}

}
/**
* @author Deloitte
* @date 07/13/2018
*
* @description This is the Implementation class for class for Document Trigger Handler
*/

public class spc_DocumentTriggerImplementation {

	/*******************************************************************************************************
	* @description  Find related attachments and create for Email outbound
	* @param Map<String, List<PatientConnect__PC_Document__c>>
	* @return void
	*/

	public void populateEPForDocument(Map<String, List<PatientConnect__PC_Document__c>> mapEmailFaxDoc) {
		Map<String, Set<String>> mapMetaData = new Map<String, Set<String>>();
		if (!mapEmailFaxDoc.isEmpty()) {
			//Check if 'to email address' / 'to fax number' corresponds to the Org Wide Email Id or Fax Number respectively,
			//which is stored in the custom setting
			List<spc_Incoming_Document_Routing_Mappings__c> custSetting = [SELECT Id, spc_Email_Fax_Id__c, spc_Engagement_Program_Code__c, spc_channelType__c
			        FROM spc_Incoming_Document_Routing_Mappings__c
			        WHERE spc_Email_Fax_Id__c in :mapEmailFaxDoc.keySet()];
			List<spc_Incoming_Document_Routing_Mappings__c> incomingDocMappingRecLists = spc_Incoming_Document_Routing_Mappings__c.getAll().values();

			if (null != custSetting && !custSetting.isEmpty()) {
				for (spc_Incoming_Document_Routing_Mappings__c a : custSetting) {
					//Get the Engagement Program Code for the Org Wide Email Address/Fax Number
					if (!mapMetaData.isEmpty() && mapMetaData.containsKey(a.spc_Engagement_Program_Code__c)) {
						mapMetaData.get(a.spc_Engagement_Program_Code__c).add(a.spc_Email_Fax_Id__c);
					} else {
						mapMetaData.put(a.spc_Engagement_Program_Code__c, new Set<String> {a.spc_Email_Fax_Id__c});
					}
				}
			}
		}
		if (!mapMetaData.isEmpty()) {
			//Fetch Engagement Program for the Program Code
			List<PatientConnect__PC_Engagement_Program__c> engProgList = [SELECT Id, PatientConnect__PC_Program_Code__c from PatientConnect__PC_Engagement_Program__c WHERE PatientConnect__PC_Program_Code__c IN :mapMetaData.keySet()];
			if (null != engProgList && !engProgList.isEmpty()) {
				for (PatientConnect__PC_Engagement_Program__c epObj : engProgList) {
					Set<String> emailFaxAddress = mapMetaData.get(epObj.PatientConnect__PC_Program_Code__c);
					for (String emailFax : emailFaxAddress) {
						if (mapEmailFaxDoc.containsKey(emailFax)) {
							List<PatientConnect__PC_Document__c> docList = mapEmailFaxDoc.get(emailFax);
							for (PatientConnect__PC_Document__c doc : docList) {
								//Populate Engagement Program and Program Code on Document object
								doc.PatientConnect__PC_Engagement_Program__c = epObj.Id;
								doc.spc_program_Code__c = epObj.PatientConnect__PC_Program_Code__c;
							}
						}
					}
				}
			}
		}
	}
}
public with sharing class CCL_Therapy_Assoc_To_Site_Assoc {
    public CCL_Therapy_Assoc_To_Site_Assoc() {

    }
    @InvocableMethod(label='Create Therapy Association To Site Association' description='Create Therapy Association To Site Association.')
    public static void createSiteAssocToTherapyAssoc(List<Id> ids) {
        system.debug('LIST ID @@'+ ids);

        List<CCL_Therapy_Association__c> therapyAssocList = new List<CCL_Therapy_Association__c>();
        List<CCL_Site_Association__c> siteAssocList = new List<CCL_Site_Association__c>();
        Map<Id,String> mapSiteIdRecData = new Map<Id,String>();
        String siteId;
        String externalId;
        String therapyAssociationId;
        CCL_Therapy_To_Site_Association__c therapyToSiteAssociation;
        List<CCL_Therapy_To_Site_Association__c> therapyToSiteAssociationList = new List<CCL_Therapy_To_Site_Association__c>();
        List<CCL_Therapy_To_Site_Association__c> therapyToSiteAssociationQList = new List<CCL_Therapy_To_Site_Association__c>();
        try {
            if (CCL_Therapy_Association__c.sObjectType.getDescribe().isAccessible()) {
            therapyAssocList = [Select Id, CCL_Unique_Site_Therapy__c, CCL_Site__c ,CCL_Site__r.ShippingCountry,Name,CCL_Therapy__c,CCL_Therapy__r.Name,
                                CCL_Therapy__r.CCL_Type__c,CCL_Therapy__r.CCL_Status__c,LastModifiedDate,LastModifiedById
                                from CCL_Therapy_Association__c
                                where  Id  =: ids];

                                system.debug('therapyAssocList @@'+ therapyAssocList);
            }

            if(!therapyAssocList.isEmpty()) {
                 
                for(CCL_Therapy_Association__c rec : therapyAssocList) {
                   
                   // final CCL_Therapy__c therapy = new CCL_Therapy__c();
                   siteId = rec.CCL_Site__c;
                  
                   externalId = rec.CCL_Unique_Site_Therapy__c;
                   therapyAssociationId = rec.Id;
                }
                if (CCL_Site_Association__c.sObjectType.getDescribe().isAccessible()) {
                    siteAssocList = [Select Id, CCL_Parent_Site__c,CCL_Parent_Site__r.Name,CCL_Child_Site__c,CCL_Child_Site__r.Name,
                    CCL_Association_Type__c, CCL_External_ID__c, CCL_Child_Site__r.CCL_External_ID__c,CCL_Parent_Site__r.CCL_External_ID__c
                                        from CCL_Site_Association__c
                                        where  CCL_Parent_Site__c  =: siteId];
                   system.debug('siteAssocList @@'+ siteAssocList);
                    }
                    if(!siteAssocList.isEmpty()) {
                        for(CCL_Site_Association__c siteRec : siteAssocList) {
                            therapyToSiteAssociation = new CCL_Therapy_To_Site_Association__c();
                           // final CCL_Therapy__c therapy = new CCL_Therapy__c();
                           therapyToSiteAssociation.CCL_Site_Assocation__c = siteRec.Id;
                           therapyToSiteAssociation.CCL_Therapy_Association__c = therapyAssociationId;
                           therapyToSiteAssociation.CCL_Site__c = siteRec.CCL_Parent_Site__c;
                           therapyToSiteAssociation.CCL_Active__c = true;
                           therapyToSiteAssociation.CCL_External_ID__c = externalId + siteRec.CCL_Parent_Site__r.CCL_External_ID__c + siteRec.CCL_External_ID__c;
                           System.debug('====External id==='+therapyToSiteAssociation.CCL_External_ID__c);
                           if (CCL_Therapy_To_Site_Association__c.sObjectType.getDescribe().isAccessible()) {
                            therapyToSiteAssociationQList = [Select Id, CCL_External_ID__c
                                                from CCL_Therapy_To_Site_Association__c
                                                where  CCL_External_ID__c  =:  therapyToSiteAssociation.CCL_External_ID__c];
                           system.debug('therapyToSiteAssociationQList @@'+ therapyToSiteAssociationQList);
                            }
                            if(!therapyToSiteAssociationQList.isEmpty()) {
                                for(CCL_Therapy_To_Site_Association__c siteTherapyRec : therapyToSiteAssociationQList) {
                                    therapyToSiteAssociation.Id = siteTherapyRec.Id;
                                }
                                
                            }
                            therapyToSiteAssociationList.add(therapyToSiteAssociation);
                        }
                     
                    }
            }
         
            Database.upsert(therapyToSiteAssociationList);
            }

         catch(QueryException ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }
}
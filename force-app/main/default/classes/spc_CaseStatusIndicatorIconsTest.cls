/**************************************************************************************
* @author Deloitte
* @date July 18,2018
* @description This class is used for testing spc_CaseStatusIndicatorIconController
*************************************************************************************/
@isTest
private class spc_CaseStatusIndicatorIconsTest {

    Static User adminUserRec;
    Static Account patientAcc;
    Static Case programCase;
    Static Case programCase1;
    Static Case programCase2;
    Static Case programCase3;

    static void createTestData() {
        adminUserRec = spc_Test_Setup.getProfileID();
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        patientAcc = spc_Test_Setup.createTestAccount('PC_Patient');
        patientAcc.PatientConnect__PC_Email__c = 'test@deloitte.com';
        patientAcc.spc_HIPAA_Consent_Received__c = 'Yes';
        patientAcc.spc_Patient_HIPAA_Consent_Date__c = system.today();
        spc_Database.ins(patientAcc);

        Account manf = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId());
        spc_Database.ins(manf);

        PatientConnect__PC_Engagement_Program__c EP = spc_Test_Setup.createEngagementProgram('Test EP', manf.id);
        spc_Database.ins(EP);

        programCase = spc_Test_Setup.newCase(patientAcc.id, 'PC_Program');
        programCase.Subject = 'test';
        programCase.PatientConnect__PC_Engagement_Program__c = EP.id;
        programCase.status = 'In Treatment';
        programCase.PatientConnect__PC_Status_Indicator_2__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_STATUS_INDICATOR_1_IN_PROGRESS);
        spc_Database.ins(programCase);

        programCase1 = spc_Test_Setup.newCase(patientAcc.id, 'PC_Program');
        programCase1.Subject = 'test';
        programCase1.PatientConnect__PC_Engagement_Program__c = EP.id;
        programCase1.status = 'Open';
        programCase1.PatientConnect__PC_Status_Indicator_2__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_COVERAGE_ACTION_REQUIRED);
        spc_Database.ins(programCase1);

        programCase2 = spc_Test_Setup.newCase(patientAcc.id, 'PC_Program');
        programCase2.PatientConnect__PC_Engagement_Program__c = EP.id;
        programCase2.Subject = 'test';
        programCase2.status = 'Completed';
        programCase2.PatientConnect__PC_Status_Indicator_1__c = spc_ApexConstants.STATUS_INDICATOR_COMPLETE;
        spc_Database.ins(programCase2);

        programCase3 = spc_Test_Setup.newCase(patientAcc.id, 'PC_Program');
        programCase3.Subject = 'test';
        programCase3.PatientConnect__PC_Engagement_Program__c = EP.id;
        programCase3.status = 'Completed';
        //programCase3.PatientConnect__PC_Status_Indicator_2__c = 'On Hold';
        spc_Database.ins(programCase3);

    }


    /*********************************************************************************
    Method Name    : getStatusIndicatorIconsTest
    Author         : Subhasmita
    Description    : This method is used for testing getting the status indicators
    Return Type    : void
    Parameter      :
    *********************************************************************************/
    static testmethod void getStatusIndicatorIconsTest() {
        createTestData();
        Test.StartTest();
        system.assertNotEquals(programCase, null);
        spc_CaseStatusIndicatorIconController indicatorIns = new spc_CaseStatusIndicatorIconController();
        system.assertNotEquals(indicatorIns , null);

        List<spc_CaseStatusIndicatorIconController.iconWrapper> lstCaseIndicators = spc_CaseStatusIndicatorIconController.getStatusIndicatorIcons(programCase.Id);
        List<spc_CaseStatusIndicatorIconController.iconWrapper> lstCaseIndicators1 = spc_CaseStatusIndicatorIconController.getStatusIndicatorIcons(programCase1.Id);
        List<spc_CaseStatusIndicatorIconController.iconWrapper> lstCaseIndicators2 = spc_CaseStatusIndicatorIconController.getStatusIndicatorIcons(programCase2.Id);
        List<spc_CaseStatusIndicatorIconController.iconWrapper> lstCaseIndicators3 = spc_CaseStatusIndicatorIconController.getStatusIndicatorIcons(programCase3.Id);
        system.assertNotEquals(lstCaseIndicators, null);
        system.assert(lstCaseIndicators.size() >= 0);
        system.assert(lstCaseIndicators1.size() >= 0);
        system.assert(lstCaseIndicators2.size() >= 0);
        system.assert(lstCaseIndicators3.size() >= 0);

        lstCaseIndicators = spc_CaseStatusIndicatorIconController.getStatusIndicatorIcons(null);
        system.assertEquals(lstCaseIndicators, null);
        Test.StopTest();
    }

    /*********************************************************************************
    Method Name    : testgetStatusIndicatorIcons_controller
    Author         : Cameron Reid
    Description    : This method is used for testing getting the status indicators
    Return Type    : void
    Parameter      :
    *********************************************************************************/
    static testmethod void getCaseTest() {
        createTestData();
        Test.StartTest();
        system.assertNotEquals(programCase, null);

        Case caseRecord = spc_CaseStatusIndicatorIconController.getCase(null);
        System.assertEquals(caseRecord, null);

        caseRecord = spc_CaseStatusIndicatorIconController.getCase(programCase.Id);
        Case caseRecord1 = spc_CaseStatusIndicatorIconController.getCase(programCase1.Id);
        Case caseRecord2 = spc_CaseStatusIndicatorIconController.getCase(programCase2.Id);
        Case caseRecord3 = spc_CaseStatusIndicatorIconController.getCase(programCase3.Id);
        System.assertNotEquals(caseRecord, null);
        System.assertNotEquals(caseRecord1, null);
        System.assertNotEquals(caseRecord2, null);
        System.assertNotEquals(caseRecord3, null);

        Test.StopTest();
    }

}
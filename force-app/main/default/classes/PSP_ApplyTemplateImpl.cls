/*********************************************************************************************************
class Name      : PSP_ApplyTemplateImpl 
Description     : Implementation Class for applying care plan template from a job
@author         : Shourya Solipuram 
@date           : December 13, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Shourya Solipuram          December 13, 2019       Initial Version
****************************************************************************************************************/ 

public class PSP_ApplyTemplateImpl implements Database.Batchable<sObject> {
    
    /* Set of Product Fam Ids*/ 
    final Set<Id> proFam = new Set<Id>();
    /* Set of Product  Ids*/ 
    final Set<Id> proId = new Set<Id>();
    /* Set of Prescription Objects*/
    final Set<CareProgramEnrolleeProduct> notAssignedPres = new Set<CareProgramEnrolleeProduct>();
    /* Map of Product Family and Outbound Setting of After Enrollment Type*/ 
    final Map<Id,PSP_Outbound_Setting__c> afterEnrollOS = new Map<Id,PSP_Outbound_Setting__c>();
    /* Map of Product Family and Outbound Setting of Before Refill Type*/ 
    final Map<Id,PSP_Outbound_Setting__c> beforeRefillOS = new Map<Id,PSP_Outbound_Setting__c>();
    /* Map of Product Family and Outbound Setting of After Drop Out Type*/ 
    final Map<Id,PSP_Outbound_Setting__c> afterDropOutOS = new Map<Id,PSP_Outbound_Setting__c>();
    /* Map of Prescription and Fam Ids*/ 
    final Map<ID,Set<CareProgramEnrolleeProduct>> presFamMap = new Map<ID, Set<CareProgramEnrolleeProduct>>();
    /* Map of Prescription and Template Ids*/ 
    final Map<ID,Set<ID>> presTempMap = new Map<ID, Set<ID>>();
    /* Map of Prescription and Fam Ids to cancel tasks*/ 
    final Map<ID,ID> notAsgndprsFm = new Map<ID, ID>();
    /* Map of Fam and Template Ids to cancel tasks*/ 
    final Map<ID,Set<ID>> FamTemplateMap = new Map<ID, Set<ID>>();
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 11/26/2019
    * @Description : start method of batchable class
	**************************************************************************************/
    public List<CareProgramEnrolleeProduct> start(Database.BatchableContext context) {
        final List<CareProgramEnrolleeProduct> cpEnrllPrdList = new List<CareProgramEnrolleeProduct>();
        if(CareProgramEnrolleeProduct.sObjectType.getDescribe().isAccessible()) {
        	cpEnrllPrdList = [Select id,PSP_Expected_Next_Refill_Date__c,PSP_Refill_Days_Due__c,createdDate,CareProgramProduct.ProductId,CareProgramProduct.Product.PSP_Family__c,
                                                                 CareProgramProduct.Product.PSP_Family__r.PSP_Type__c,CareProgramProduct.Product.PSP_Family__r.recordtype.name,
                                                                 PSP_Patient__c,CareProgramProductId,Status, CareProgramEnrolleeId,CareProgramEnrollee.Status FROM CareProgramEnrolleeProduct 
                                                                 WHERE CareProgramProduct.Product.recordtype.name = 'Product' AND CareProgramProduct.Product.PSP_Family__r.PSP_Type__c = 'Family' 
                                                                 AND CareProgramProduct.Product.PSP_Family__r.recordtype.name = 'Product Grouping'];
        }
        return cpEnrllPrdList;
    }
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 11/26/2019
    * @Description : execute method of batchable class
	**************************************************************************************/
    public void execute(Database.BatchableContext context, List<CareProgramEnrolleeProduct> cpEnrllPrdList) {
        for(CareProgramEnrolleeProduct cpEnrllPrd : cpEnrllPrdList) {
            proFam.add(cpEnrllPrd.CareProgramProduct.Product.PSP_Family__c);
            if(cpEnrllPrd.Status != 'Assigned' || cpEnrllPrd.CareProgramEnrollee.Status != 'Enrolled') {
                notAsgndprsFm.put(cpEnrllPrd.Id,cpEnrllPrd.CareProgramProduct.Product.PSP_Family__c);
                
            } else {
                Set<CareProgramEnrolleeProduct> cpEnPrId = new Set<CareProgramEnrolleeProduct>();
                if(presFamMap.containsKey(cpEnrllPrd.CareProgramProduct.Product.PSP_Family__c)) {
                    cpEnPrId = presFamMap.get(cpEnrllPrd.CareProgramProduct.Product.PSP_Family__c);
                }
                cpEnPrId.add(cpEnrllPrd);
                presFamMap.put(cpEnrllPrd.CareProgramProduct.Product.PSP_Family__c,cpEnPrId);
            }            
        }            
        for(PSP_Outbound_Setting__c outboundSet : [Select id,PSP_Care_Plan_Template__c,PSP_Care_Plan_Template__r.Name,PSP_Grace_Period__c,PSP_Max_Refill_Reminder__c,PSP_Outbound_Type__c,PSP_Product_Family__c,PSP_Active__c FROM PSP_Outbound_Setting__c WHERE PSP_Active__c = 'Yes' AND PSP_Product_Family__c IN:proFam]) {
            if(outboundSet.PSP_Outbound_Type__c == 'After Enrollment') {
                afterEnrollOS.put(outboundSet.PSP_Product_Family__c,outboundSet);
            }			
            if(outboundSet.PSP_Outbound_Type__c == 'Before Refill') {
                beforeRefillOS.put(outboundSet.PSP_Product_Family__c,outboundSet);
            }
            if(outboundSet.PSP_Outbound_Type__c == 'After Drop Out') {
                afterDropOutOS.put(outboundSet.PSP_Product_Family__c,outboundSet);
            }
            Set<Id> templateId = new Set<Id>();
            if(FamTemplateMap.containsKey(outboundSet.PSP_Product_Family__c)) {
                templateId = FamTemplateMap.get(outboundSet.PSP_Product_Family__c);
            }
            templateId.add(outboundSet.PSP_Care_Plan_Template__c);
            FamTemplateMap.put(outboundSet.PSP_Product_Family__c,templateId);
        }
        
        if(!notAsgndprsFm.isEmpty()) {
            for(Id presID : notAsgndprsFm.keySet()) {
                presTempMap.put(presID,FamTemplateMap.get(notAsgndprsFm.get(presID)));
            }
            cancelTask(presTempMap);
        }
        if(!presFamMap.isEmpty()) {
            checkForAssignedAndEnrolled(presFamMap,afterEnrollOS,beforeRefillOS,afterDropOutOS,proId,FamTemplateMap);
        }
    }
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 12/15/2019
    * @Description : Private method to cancel any open tasks and close careplans
    * @Param       : List of Enrolled Prescription And Service
    * @Return      : Void
    **************************************************************************************/
    private static void checkForAssignedAndEnrolled (Map<ID,Set<CareProgramEnrolleeProduct>> presFamMap,Map<Id,PSP_Outbound_Setting__c> afterEnrollOS,Map<Id,PSP_Outbound_Setting__c> beforeRefillOS,Map<Id,PSP_Outbound_Setting__c> afterDropOutOS,Set<Id> proId,Map<ID,Set<ID>> FamTemplateMap) {
    /* Map of Prescription and Product Ids*/ 
    final Map<ID,Set<CareProgramEnrolleeProduct>> presProMap = new Map<ID, Set<CareProgramEnrolleeProduct>>();
    /* Map of Enrollment Prescription and Fam Ids*/ 
    final Map<ID,Set<CareProgramEnrolleeProduct>> prsFamEnrlMap = new Map<ID,Set<CareProgramEnrolleeProduct>>();
    /* Map of Fam and Product Ids*/ 
    final Map<ID,Set<Id>> TempProIDMap = new Map<ID, Set<Id>>();
    /* Map of Refill Reminder and Drop Out Prescription and Fam Ids*/ 
    final Map<ID,Set<CareProgramEnrolleeProduct>> prsFmRemndrMap = new Map<ID,Set<CareProgramEnrolleeProduct>>();
    /* Map of Enrollment Prescription and Fam Ids*/ 
    Map<ID,Set<CareProgramEnrolleeProduct>> presFamEnrollFinal = new Map<ID,Set<CareProgramEnrolleeProduct>>();
    
        for(Id famID : presFamMap.keySet()) {
                for(CareProgramEnrolleeProduct cPEP : presFamMap.get(famID)) {
                    if(afterEnrollOS.containsKey(famId) && System.now().date() >= cPEP.CreatedDate.addDays(Integer.valueof(afterEnrollOS.get(famId).PSP_Grace_Period__c)).date()) {
                        Set<CareProgramEnrolleeProduct> cpEnPr = new Set<CareProgramEnrolleeProduct>();
                        if(presProMap.containsKey(cPEP.CareProgramProduct.ProductId)) {
                            cpEnPr = presProMap.get(cPEP.CareProgramProduct.ProductId);
                        }
                        cpEnPr.add(cPEP);
                        presProMap.put(cPEP.CareProgramProduct.ProductId,cpEnPr);
                        Set<Id> proIDSet = new Set<Id>();
                        if(TempProIDMap.containsKey(cPEP.CareProgramProduct.Product.PSP_Family__c)) {
                            proIDSet = TempProIDMap.get(cPEP.CareProgramProduct.Product.PSP_Family__c);
                        }
                        proIDSet.add(cPEP.CareProgramProduct.ProductId);
                        TempProIDMap.put(cPEP.CareProgramProduct.Product.PSP_Family__c,proIDSet);
                    }
                }
            }
            if(!presProMap.isEmpty()) {
                for(PSP_Purchase_Transaction__c pTran : [Select id,PSP_Products_and_Services__c 
                                                         from PSP_Purchase_Transaction__c WHERE PSP_Products_and_Services__c IN: presProMap.keySet()
                                                         AND PSP_Status__c = 'Purchased']) {
                                                             proId.add(pTran.PSP_Products_and_Services__c);
                                                         }
                for(Id famID : TempProIDMap.keySet()) {
                    for(Id PrID : TempProIDMap.get(famID)) {
                        if(proId.contains(PrID)) {
                            if(prsFmRemndrMap.containsKey(famID)) {
                                Set<CareProgramEnrolleeProduct> existingSet = new Set<CareProgramEnrolleeProduct>();
                                existingSet = prsFmRemndrMap.get(famID);
                                existingSet.addAll(presProMap.get(PrID));
                                prsFmRemndrMap.put(famID,existingSet);
                            } else {
                                prsFmRemndrMap.put(famID,presProMap.get(PrID));
                            }
                        } else {
                            if(prsFamEnrlMap.containsKey(famID)) {
                                Set<CareProgramEnrolleeProduct> existingSet = new Set<CareProgramEnrolleeProduct>();
                                existingSet = prsFamEnrlMap.get(famID);
                                existingSet.addAll(presProMap.get(PrID));
                                prsFamEnrlMap.put(famID,existingSet);
                            } else {
                                prsFamEnrlMap.put(famID,presProMap.get(PrID));
                            }
                        }
                    }
                }
            } 
            if(!prsFamEnrlMap.isEmpty()) {
                presFamEnrollFinal = checkForExistingCase(prsFamEnrlMap,afterEnrollOS);
                if(!presFamEnrollFinal.isEmpty()) {
                    PSP_ApplyTemplateForOutboundSetting.createCarePlans(presFamEnrollFinal,afterEnrollOS);
                }
            }
            if(!prsFmRemndrMap.isEmpty()) {
                checkRefillAndDropOut(prsFmRemndrMap,beforeRefillOS,afterDropOutOS,FamTemplateMap);
            }
        }    
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 12/15/2019
    * @Description : Private method to cancel any open tasks and close careplans
    * @Param       : List of Enrolled Prescription And Service
    * @Return      : Void
    **************************************************************************************/
 	private static void cancelTask (Map<Id,Set<Id>> presTempMap) {
        final List<Task> updateTasks = new List<Task>();
        final Set<Case> updateCasesSet = new Set<Case>();
        final List<Case> updateCases = new List<Case>();
        final set<Id> caseIDsToUpd = new set<Id>();
        for(Case cs: [Select id,Status,RecordTypeId,PSP_Enrolled_Prescription_and_Service__c,PSP_Care_Plan_Template__c 
                      from Case WHERE PSP_Enrolled_Prescription_and_Service__c IN: presTempMap.keySet() 
                      AND RecordTypeId =:PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CASE_RT_CAREPLAN, Case.getSObjectType())
                      AND isClosed = false]) {
                          if(presTempMap.get(cs.PSP_Enrolled_Prescription_and_Service__c).contains(cs.PSP_Care_Plan_Template__c)) {
                              caseIDsToUpd.add(cs.Id);
                              cs.Status = 'Closed';
                              updateCasesSet.add(cs);
                          }
                      }
        if(!caseIDsToUpd.isEmpty()) {
            for(Task tk : [Select id,WhatId,RecordTypeId,HealthCloudGA__CarePlanTemplate__c,status FROM Task where WhatId IN:caseIDsToUpd 
                           AND RecordTypeId =: PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.TASK_RT_CAREPLAN, Task.getSObjectType())
                           AND Status != 'Cancelled']) {
                               tk.Status = 'Cancelled';
                               updateTasks.add(tk);
                           }
            if(!updateCasesSet.isEmpty()) {
                updateCases.addAll(updateCasesSet);
                Database.update(updateCases);
                if(!updateTasks.isEmpty()) {
                    Database.update(updateTasks);
                }
            }
        }
    }     
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 12/15/2019
    * @Description : Private method to check if any open case already exists
    * @Param       : List of Enrolled Prescription And Service
    * @Return      : Void
    **************************************************************************************/
    private static Map<ID,Set<CareProgramEnrolleeProduct>> checkForExistingCase (Map<ID,Set<CareProgramEnrolleeProduct>> presFamMap,Map<Id,PSP_Outbound_Setting__c> famIdOSMap) {
        /* Map of EPS id and care plan template id */
        final Map<Id, Set<CareProgramEnrolleeProduct>> tempIdEPSMap = new Map<Id, Set<CareProgramEnrolleeProduct>>();
        /* Set of EPS */
        final Set<CareProgramEnrolleeProduct> existingEPS = new Set<CareProgramEnrolleeProduct>();
        /* Map of EPS id and EPS */
        final Map<Id,CareProgramEnrolleeProduct> allEPSToCheck = new Map<Id,CareProgramEnrolleeProduct>();
        
        for(Id famID : presFamMap.keySet()) {
            if(famIdOSMap.containsKey(famID)) {
                tempIdEPSMap.put(famIdOSMap.get(famId).PSP_Care_Plan_Template__c,presFamMap.get(famId));
            }
        }
        if(!tempIdEPSMap.isEmpty()) {
            for(Id tempID : tempIdEPSMap.keySet()) {
                for(CareProgramEnrolleeProduct cPEnPr : tempIdEPSMap.get(tempID)) {
                    allEPSToCheck.put(cPEnPr.Id,cPEnPr);
                }
            }
            
            for(Case cs : [Select id,Status,RecordTypeId,PSP_Enrolled_Prescription_and_Service__c ,PSP_Care_Plan_Template__c
                           from Case WHERE PSP_Enrolled_Prescription_and_Service__c IN: allEPSToCheck.keySet() 
                           AND RecordTypeId =:PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CASE_RT_CAREPLAN, Case.getSObjectType())
                           AND isClosed = false AND PSP_Care_Plan_Template__c IN: tempIdEPSMap.keySet()
                          ]) {
                              if(tempIdEPSMap.get(cs.PSP_Care_Plan_Template__c).contains(allEPSToCheck.get(cs.PSP_Enrolled_Prescription_and_Service__c))) {
                                  existingEPS.add(allEPSToCheck.get(cs.PSP_Enrolled_Prescription_and_Service__c));
                              } 
                          }
            for(ID famID : presFamMap.keySet()) {
                for(CareProgramEnrolleeProduct EPSvalue: presFamMap.get(famID)) {
                    if(!existingEPS.isEmpty() && existingEPS.contains(EPSvalue)) {
                        set<CareProgramEnrolleeProduct> allEPS = new Set<CareProgramEnrolleeProduct>();
                        allEPS = presFamMap.get(famID);
                        allEPS.remove(EPSvalue);
                        if(!allEPS.isEmpty()) {
                            presFamMap.put(famID,allEPS);
                        } else {
                            presFamMap.remove(famID);
                        }
                    }
                }
            }
        }
        return presFamMap;
    }
    
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 12/15/2019
    * @Description : Private method to check for Refill Reminders and Drop Out
    * @Param       : Map of Product Family and CareProgramEnrolleeProduct
    * @Param       : Map of Product Family and Outbound Setting of Refill Reminder Type
    * @Param       : Map of Product Family and Outbound Setting of Drop out Type
    * @Return      : Void
    **************************************************************************************/
    private static void checkRefillAndDropOut (Map<ID,Set<CareProgramEnrolleeProduct>> presFamReminderMap,Map<Id,PSP_Outbound_Setting__c>  beforeRefillOS,Map<Id,PSP_Outbound_Setting__c> afterDropOutOS,Map<ID,Set<ID>> FamTemplateMap){
        /* prescription id and maximum refill map */
        final Map<Id,Integer> presIdAndMaxRem = new Map<Id,Integer>();
        /* prescription id and template id map */
        final Map<Id,Id> presIdAndTempId = new Map<Id,Id>();
        final Map<Id,CareProgramEnrolleeProduct> presIdToRecMap = new Map<Id,CareProgramEnrolleeProduct>();
        Set<Id> ePSToApply = new Set<Id>();
        final Map<Id,Set<CareProgramEnrolleeProduct>> drpOutAplyTemp = new Map<Id,Set<CareProgramEnrolleeProduct>>();
        final Map<Id,Set<CareProgramEnrolleeProduct>> drpOutChckExstng = new Map<Id,Set<CareProgramEnrolleeProduct>>();
        for(Id famID : presFamReminderMap.keySet()) {
            for(CareProgramEnrolleeProduct cPEnPr : presFamReminderMap.get(famID)) {
                if(cPEnPr.PSP_Refill_Days_Due__c != null && cPEnPr.PSP_Expected_Next_Refill_Date__c != null) {
                    system.debug(cPEnPr);
                    if(beforeRefillOS.containsKey(famID) && cPEnPr.PSP_Refill_Days_Due__c == Integer.valueof(beforeRefillOS.get(famID).PSP_Grace_Period__c) && cPEnPr.PSP_Expected_Next_Refill_Date__c == System.today().addDays(Integer.valueof(beforeRefillOS.get(famID).PSP_Grace_Period__c))){
                        if(beforeRefillOS.get(famID).PSP_Max_Refill_Reminder__c == 'N/A') {
                            ePSToApply.add(cPEnPr.Id);
                        } else {
                            presIdAndMaxRem.put(cPEnPr.Id,Integer.valueof(beforeRefillOS.get(famID).PSP_Max_Refill_Reminder__c));
                            presIdAndTempId.put(cPEnPr.Id,beforeRefillOS.get(famID).PSP_Care_Plan_Template__c);
                        }
                        presIdToRecMap.put(cPEnPr.Id,cPEnPr);
                    } else if(afterDropOutOS.containsKey(famID) && cPEnPr.PSP_Refill_Days_Due__c + Integer.valueof(afterDropOutOS.get(famID).PSP_Grace_Period__c) == 0) {
                        Set<CareProgramEnrolleeProduct> cpEnPrSet = new Set<CareProgramEnrolleeProduct>();
                        if(drpOutAplyTemp.containsKey(famID)) {
                            cpEnPrSet = drpOutAplyTemp.get(famID);
                        }
                        cpEnPrSet.add(cPEnPr);
                        drpOutAplyTemp.put(famID,cpEnPrSet);
                        presFamReminderMap.get(famId).remove(cPEnPr);
                    } else {
                        presFamReminderMap.get(famId).remove(cPEnPr);
                        Set<CareProgramEnrolleeProduct> cpEnPrSet = new Set<CareProgramEnrolleeProduct>();
                        if(drpOutChckExstng.containsKey(famID)) {
                            cpEnPrSet = drpOutChckExstng.get(famID);
                        }
                        cpEnPrSet.add(cPEnPr);
                        drpOutChckExstng.put(famID,cpEnPrSet);
                    }
                }
            }
        }
        //drop out method
        if(!drpOutAplyTemp.isEmpty() || !drpOutChckExstng.isEmpty()) {
            checkForDropOut(drpOutAplyTemp,drpOutChckExstng,afterDropOutOS,FamTemplateMap);
        }  
        //refill reminder method
        if(!presIdAndMaxRem.isEmpty() && !presIdAndTempId.isEmpty()) {
            ePSToApply = checkMaxRefill(presIdAndMaxRem,presIdAndTempId);
        }
        if(!ePSToApply.isEmpty()) {
            checkForRefill(ePSToApply,presIdToRecMap,presFamReminderMap,FamTemplateMap,beforeRefillOS);
        }
        
    }
     /**************************************************************************************
    * @author      : Deloitte
    * @date        : 12/18/2019
    * @Description : Private method to check for Drop Out Templates
    * @Param       : Set of EPS ID
    * @Param       : Map of Fam ID and Set of CareProgramEnrolleeProduct
    * @Param       : Map of ID and CareProgramEnrolleeProduct Record
    * @Return      : Void
    **************************************************************************************/
    private static void checkForRefill(Set<Id> ePSToApply,Map<Id,CareProgramEnrolleeProduct> presIdToRecMap,Map<ID,Set<CareProgramEnrolleeProduct>> presFamReminderMap,Map<ID,Set<ID>> FamTemplateMap,Map<Id,PSP_Outbound_Setting__c> beforeRefillOS) {
    	/* Map of Id and Set of CareProgramEnrolleeProduct */
        final Map<Id,Set<CareProgramEnrolleeProduct>> reflRemAplyTmp = new Map<Id,Set<CareProgramEnrolleeProduct>>();
        /* Map of Id and Set of CareProgramEnrolleeProduct */
        Map<Id,Set<CareProgramEnrolleeProduct>> reflRmAplyTmpFnl = new Map<Id,Set<CareProgramEnrolleeProduct>>();
        /* Map of refill remember cancel */
        final Map<Id,Set<Id>> reflRemCncl = new Map<Id,Set<Id>>();
        for(Id preID : presIdToRecMap.keySet()) {
                if(!ePSToApply.contains(preID)) {
                    presIdToRecMap.remove(preID);
                }
            }
            if(!presIdToRecMap.isEmpty()) {
                for(Id famID : presFamReminderMap.keySet()) {
                	for(Id preId : presIdToRecMap.keySet()) {
                     if(presFamReminderMap.get(famID).contains(presIdToRecMap.get(preId))) {
                            Set<CareProgramEnrolleeProduct> cpEnPrSet = new Set<CareProgramEnrolleeProduct>();
                            if(reflRemAplyTmp.containsKey(famID)) {
                                cpEnPrSet = reflRemAplyTmp.get(famID);
                            }
                            cpEnPrSet.add(presIdToRecMap.get(preId));
                            reflRemAplyTmp.put(famID,cpEnPrSet);
                        }
                    }
                }
                if(!reflRemAplyTmp.isEmpty()) {
                    reflRmAplyTmpFnl = checkForExistingCase(reflRemAplyTmp,beforeRefillOS);
                    if(!reflRmAplyTmpFnl.isEmpty()) {
                        for(Id famID : reflRmAplyTmpFnl.keySet()) {
                            for(CareProgramEnrolleeProduct presRec : reflRmAplyTmpFnl.get(famId)) {
                                reflRemCncl.put(presRec.Id,famTemplateMap.get(famID));
                            }
                        }
                        cancelTask(reflRemCncl);
                        PSP_ApplyTemplateForOutboundSetting.createCarePlans(reflRmAplyTmpFnl,beforeRefillOS);
                    }
                }
            }
        }
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 12/18/2019
    * @Description : Private method to check for Drop Out Templates
    * @Param       : Map of ID and CareProgramEnrolleeProduct set
    * @Param       : Map of ID and CareProgramEnrolleeProduct Set
    * @Param       : Map of Fam ID and Outbound settings Set
    * @Param       : Map of Fam ID and Template Ids
    * @Return      : Void
    **************************************************************************************/
    private static void checkForDropOut(Map<Id,Set<CareProgramEnrolleeProduct>> dropOutApplyTempMap,Map<Id,Set<CareProgramEnrolleeProduct>> dropOutCheckExistingMap,Map<Id,PSP_Outbound_Setting__c> afterDropOutOS,Map<ID,Set<ID>> FamTemplateMap) {
        Map<Id,Set<CareProgramEnrolleeProduct>> dropOutApply = new  Map<Id,Set<CareProgramEnrolleeProduct>>();
        final Map<Id,Set<Id>> dropOutCancelMap = new Map<Id,Set<Id>>();
        if(!dropOutApplyTempMap.isEmpty()){
            dropOutApply = checkForExistingCase(dropOutApplyTempMap,afterDropOutOS);
            if(!dropOutApply.isEmpty()) {
                for(Id famID : dropOutApply.keySet()) {
                    for(CareProgramEnrolleeProduct presRec: dropOutApply.get(famID)) {
                        dropOutCancelMap.put(presRec.Id,FamTemplateMap.get(famID));
                    }
                }
            } 
        }
        if(!dropOutCheckExistingMap.isEmpty()) {
            final  Map<Id,Set<CareProgramEnrolleeProduct>> drpOutAplyTmpFnl = new  Map<Id,Set<CareProgramEnrolleeProduct>>(checkForExistingCase(dropOutCheckExistingMap,afterDropOutOS)); 
            if(!drpOutAplyTmpFnl.isEmpty()) {
                for(Id famID : drpOutAplyTmpFnl.keySet()) {
                    for(CareProgramEnrolleeProduct presRec: drpOutAplyTmpFnl.get(famID)) {
                        dropOutCancelMap.put(presRec.Id,FamTemplateMap.get(famID));
                    }
                }
            }
        }
        if(!dropOutCancelMap.isEmpty()){
            cancelTask(dropOutCancelMap);
        }
        if(!dropOutApply.isEmpty()){
            PSP_ApplyTemplateForOutboundSetting.createCarePlans(dropOutApply,afterDropOutOS);
        }
    }
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 12/18/2019
    * @Description : Private method to check for Max Refill Reminders 
    * @Param       : Map of Prescription ID and Max Refill Reminder 
    * @Param       : Map of Prescription ID and Template Id
    * @Return      : Void
    **************************************************************************************/
    private static Set<Id> checkMaxRefill(Map<ID,Integer>presIdAndMaxRem,Map<Id,Id>  presIdAndTempId) {
        final set<Id> ePSIdSet = new Set<Id>();
        final set<Id> ePSToCreate = new Set<Id>();
        final Map<Id,Integer> ePSIDToCount = new Map<Id,Integer>();
        if(Case.sObjectType.getDescribe().isAccessible()) {
            for(Case cs : [Select id,Status,createdDate,RecordTypeId,PSP_Enrolled_Prescription_and_Service__c,PSP_Care_Plan_Template__c,isClosed
                           FROM CASE WHERE PSP_Enrolled_Prescription_and_Service__c IN: presIdAndMaxRem.keySet() 
                           AND RecordTypeId =:PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CASE_RT_CAREPLAN, Case.getSObjectType())
                           order by createdDate desc]) {
                               if(!ePSIdSet.contains(cs.PSP_Enrolled_Prescription_and_Service__c)) {
                                   if(cs.PSP_Care_Plan_Template__c == presIdAndTempId.get(cs.PSP_Enrolled_Prescription_and_Service__c) && cs.isClosed == true) {
                                       if(ePSIDToCount.containsKey(cs.PSP_Enrolled_Prescription_and_Service__c)) {
                                           integer count = ePSIDToCount.get(cs.PSP_Enrolled_Prescription_and_Service__c);
                                           if(count+1 == presIdAndMaxRem.get(cs.PSP_Enrolled_Prescription_and_Service__c)) {
                                               ePSIdSet.add(cs.PSP_Enrolled_Prescription_and_Service__c); 
                                               ePSIDToCount.remove(cs.PSP_Enrolled_Prescription_and_Service__c);
                                           } else {  
                                               ePSIDToCount.put(cs.PSP_Enrolled_Prescription_and_Service__c,count+1);
                                           }
                                           
                                       } else {
                                           if(presIdAndMaxRem.get(cs.PSP_Enrolled_Prescription_and_Service__c) == 1) {
                                               ePSIdSet.add(cs.PSP_Enrolled_Prescription_and_Service__c);
                                           } else {
                                               ePSIDToCount.put(cs.PSP_Enrolled_Prescription_and_Service__c,1);
                                           }
                                       }
                                   } else {
                                       ePSToCreate.add(cs.PSP_Enrolled_Prescription_and_Service__c);
                                       ePSIdSet.add(cs.PSP_Enrolled_Prescription_and_Service__c); 
                                   }
                               }
                           }
        }
        EPSToCreate.addAll(ePSIDToCount.keySet());
        for(Id ePSId : presIdAndMaxRem.keySet()) {
            if(!ePSIDToCount.containsKey(ePSId) && !ePSIdSet.contains(ePSId)) {
                EPSToCreate.add(ePSId);
            }
        }
        return ePSToCreate;
    }
    /**************************************************************************************
    * @author      : Deloitte
    * @date        : 12/18/2019
    * @Description : finish method
    **************************************************************************************/
    public void finish(Database.BatchableContext context) {
        // Empty finish method
    }
    
}
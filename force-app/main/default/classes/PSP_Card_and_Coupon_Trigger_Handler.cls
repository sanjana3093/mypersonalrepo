/*********************************************************************************************************
class Name      : PSP_Card_and_Coupon_Trigger_Handler 
Description		: Trigger Handler Class for Card and Coupon Object
@author     	: Divya Eduvulapati 
@date       	: October 24, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Divya Eduvulapati             October 24, 2019          Initial Version
****************************************************************************************************************/ 
public class PSP_Card_and_Coupon_Trigger_Handler extends PSP_trigger_Handler {

    /*Instantiation of Trigger impelementation class   */ 
	PSP_Card_and_Coupon_Trigger_Impl triggerImpl = new PSP_Card_and_Coupon_Trigger_Impl ();   
	 /**************************************************************************************
  	* @author      : Divya Eduvulapati
  	* @date        : 10/24/2019
  	* @Description : After Update Method being overriden here
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/    
	public override void afterUpdate () { 
    	triggerImpl.updateLastActiveCardReferences((Map<Id,PSP_Card_and_Coupon__c>) Trigger.oldMap,(Map<Id,PSP_Card_and_Coupon__c>) Trigger.newMap,Trigger.new);    
    }
    
}
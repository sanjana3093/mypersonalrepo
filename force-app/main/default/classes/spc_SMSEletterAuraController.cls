/*********************************************************************************************************
 * @author Deloitte
 * @date   August, 2018
 * @description  spc_SMSEletterAuraController controller class for sms eletter lightning component

**/

public class spc_SMSEletterAuraController {


    private static final String EMPTY_STRING = '';

    /*********************************************************************************
    * @date   August, 2018
    * @description     Retrieves available eLetters by object type
    * @return     List<PC_LightningMap>
    *********************************************************************************/
    @AuraEnabled
    public static List<spc_LightningMap> getLetters(Id recordId) {
        List<spc_LightningMap> lstAvailableeLetters = new List<spc_LightningMap>();
        String objType = String.valueOf(recordId.getSobjectType());
        String recordTypeName = EMPTY_STRING;
        //getting the consents of the patient account
        spc_PatientConsentDetails consentDetails = spc_PatientConsentDetails.getInstance(recordId);
        lstAvailableeLetters.add(new spc_LightningMap(Label.spc_Select_a_SMS, Label.PatientConnect.PC_None));
        
        List<String> consentlst  = spc_Utility.fetchConsentList(consentDetails);
        try {
            SObject selectedObj = Database.query('select RecordType.Name from ' + String.valueOf(recordId.getSobjectType()) + ' where Id=: recordId');
            List<Case> currentCase = new List<Case>();
            if (objType == 'Case') {
                Case caseObj = (Case)selectedObj;
                recordTypeName = caseObj.RecordType.Name;
                currentCase = [SELECT ID, PatientConnect__PC_Engagement_Program__c, spc_Is_Enquiry_Case__c FROM CASE WHERE ID = : recordId ]; //1//
            }
            String consentString = string.join(consentlst, ',');
            Set<String> consentAllSet = new Set<String>();
            consentAllSet.addAll(consentString.remove('\'').split(','));

            try {
                List<String> fields = new List<String> {'Name'};
                //dynamic query based on consents in eletter spc_Database.assertAccess(spc_ApexConstants.getQualifiedAPIName(spc_ApexConstants.ELETTER_FORM_FIELD_SET),spc_Database.Operation.Reading, fields);
                String query = 'SELECT Id,spc_Consent_Eletter_Needeed__c,spc_All_Consents_Mandatory__c, Name FROM PatientConnect__PC_eLetter__c';
                query += ' WHERE  PatientConnect__PC_Source_Object__c = \'' + objType + '\' AND PatientConnect__PC_Object_Record_Types__c includes (\'' + recordTypeName + '\')';
                if (!consentlst.isEmpty()) {
                    query += ' AND  (spc_Consent_Eletter_Needeed__c includes' + consentlst;
                    query += 'OR spc_Consent_Eletter_Needeed__c=\'' + spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.DEFAULT_CONSENT) + '\') order by Name';
                } else {
                    query += 'AND spc_Consent_Eletter_Needeed__c=\'' + spc_ApexConstants.getConsentTextValue(spc_ApexConstants.ConsentTextValue.DEFAULT_CONSENT) + '\' order by Name';
                }

                List<PatientConnect__PC_eLetter__c> eletterslst = spc_Database.queryWithAccess(query , false);
                if (!currentCase.isEmpty()) {
                    Map<ID, Set<String>> cChannels = spc_SMSeLetterService.getMapBetweenEletterAndCchannels(new Set<ID> {currentCase[0].PatientConnect__PC_Engagement_Program__c});
                    for (PatientConnect__PC_eLetter__c el : eletterslst) {

                        if (cChannels.get(el.Id) != null) {
                            Set<String> filteredList = cChannels.get(el.Id);
                            if (!filteredList.isEmpty()) {
                                if (el.spc_All_Consents_Mandatory__c) {
                                    Set<String> eletterConsentSet = new Set<String>();
                                    List<String> tmpString = new List<String>();
                                    tmpString.addAll(el.spc_Consent_Eletter_Needeed__c.split(';'));
                                    eletterConsentSet.addAll(tmpString);
                                    If(consentAllSet.containsAll(eletterConsentSet) ) {
                                        lstAvailableeLetters.add(new spc_LightningMap(el.Name, el.Id));
                                    }

                                } else {
                                    lstAvailableeLetters.add(new spc_LightningMap(el.Name, el.Id));
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                spc_Utility.logAndThrowException(ex);
            }
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }

        return lstAvailableeLetters;

    }
    /*********************************************************************************
    * @description     Retrieves eLetters recepients based on selected letter
    * @return  List<spc_SMSeLetter_Recipient>
    * @param string and Id
    *********************************************************************************/
    @AuraEnabled
    public static List<spc_SMSeLetter_Recipient> getRecepients(String letterId, Id recordId) {
        String objType = String.valueOf(recordId.getSobjectType());
        List<spc_SMSeLetter_Recipient> recepients = new List<spc_SMSeLetter_Recipient> ();
        Id caseId  = recordId;

        try {

            if (String.valueof(letterId) != Label.PatientConnect.PC_None) {
                PatientConnect__PC_eLetter__c eLetter = spc_SMSeLetterService.getELetter(letterId);
                List<String> fields;

                fields = new List<String> {'CaseNumber'};
                spc_Database.assertAccess('Case', spc_Database.Operation.Reading, fields);
                fields = new List<String> {'Name'};
                spc_Database.assertAccess('Account', spc_Database.Operation.Reading, fields);

                Case objCase = [SELECT Id, CaseNumber, RecordType.Name, Account.Name FROM Case WHERE Id = :caseId];

                // Get available languages from the eLetter Template and dynamically display the participant language:
                List<SelectOption> lstCommunicationLanguage = new List<SelectOption> ();

                for (String lang : eLetter.PatientConnect__PC_Communication_Language__c.split(';')) {
                    lstCommunicationLanguage.add(new SelectOption(lang, lang));
                }

                recepients = spc_SMSeLetterService.getParticipants(objCase.Id, eLetter);
                if (recepients == null) return null; // If no participants found, don't proceed.
                // Disble the SendToMe checkboxes on eLetter Page for the eLetters where 'Send to User' is not selected.
                for (spc_SMSeLetter_Recipient rec : recepients) {
                    rec.sendToMe = false;
                    rec.bDisableSendToMe = true;
                }

            }
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }

        return recepients;
    }
    /**
    * Send eLetter to select recepients.
    *
    * @param recipientsString  JSON string containing spc_SMSeLetter_Recipient list
    * @param letterId           Record Id of the eLetter object
    * @param objectId           Record Id of object the letter is being sent from - looks like it is always Program Case
    */
    @AuraEnabled
    public static string sendOutboundDocuments(String recipientsString, String letterId, Id objectId) {
        String returnMessage = EMPTY_STRING;
        String errorMessage = EMPTY_STRING;
        try {

            User currUser = spc_SMSeLetterService.sendToMe();
            List<spc_SMSeLetter_Recipient> recepients = (List<spc_SMSeLetter_Recipient>) System.JSON.deserialize(recipientsString, List<spc_SMSeLetter_Recipient>.class);

            PatientConnect__PC_eLetter__c eLetter = spc_SMSeLetterService.getELetter(letterId);

            // If no recipients are available then inform the user:
            if (recepients == null || recepients.size() == 0 || String.valueOf(letterId) == Label.PatientConnect.PC_None) {
                return System.Label.PatientConnect.PC_No_Recipients_Available;
            }

            // Prepare a list of accounts with preferences selected on page:
            List<Account> smsRecipients = new List<Account>();
            List<Account> sendToUserRecipients = new List<Account>();

            //Store the recipient names for displaying info message to the user (boolean sendEmail/sendFax is not considered now)
            String smsRecipientString = EMPTY_STRING;


            // Collect the list of selected recipients with their language selection at Page level.
            Account overriddenRecipient;
            for (spc_SMSeLetter_Recipient recipient : recepients) {
                overriddenRecipient = recipient.recipient;
                overriddenRecipient.PatientConnect__PC_Communication_Language__c = recipient.communicationLanguage;
                if (recipient.sendSMS) {
                    // This list will be explicitly passed to PC_eLetterService.SendEmail() Method to override the preferred method of communication selection at participant level.
                    if (recipient.recipientSMS == null) {
                        errorMessage += overriddenRecipient.Name + '; ';
                    }  else {
                        smsRecipients.add(overriddenRecipient);
                        smsRecipientString += overriddenRecipient.Name + '; ';
                    }
                }
                if (recipient.sendToMe) {
                    // This list will be explicitly passed to PC_eLetterService.SendLetterToUser() to override the preferred method of communication/Language selection at participant level.
                    sendToUserRecipients.add(overriddenRecipient);
                }
            }

            // If the recipient list is empty then throw error message else info message
            if (smsRecipients.isEmpty() && sendToUserRecipients.isEmpty() && errorMessage.equals(EMPTY_STRING) ) {
                returnMessage = System.Label.PatientConnect.PC_No_Record_Selected;
            } else {
                if (!smsRecipients.isEmpty()) {
                    smsRecipientString = smsRecipientString.left(smsRecipientString.length() - 2) + '.  '; // Remove the last '; ' from the list of sms recipients:
                    returnMessage = System.Label.spc_GeneratingSMSFor + smsRecipientString;
                }
                if (!sendToUserRecipients.isEmpty()) {
                    returnMessage = System.Label.PatientConnect.PC_eLetters_Sent_to_User;
                }
            }


            // For the eLetters with 'Send to User' checked, send the eLetters to logged in User.
            // 'Send to User' Takes precedence over 'Allow Editing' (if both flags are checked , then eLetter will be sent to logged in user for further editing/processing)
            if (eLetter.PatientConnect__PC_Send_to_User__c && sendToUserRecipients.size() > 0) {
                spc_SMSeLetterService.sendLetter(objectId, eLetter, spc_ApexConstants.SMS, smsRecipients, true);
            } else {
                if (smsRecipients.size() > 0) {
                    spc_SMSeLetterService.sendLetter(objectId, eLetter, spc_ApexConstants.SMS, smsRecipients, false);
                }
            }
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }
        if (!errorMessage.equals(EMPTY_STRING)) {
            errorMessage = ' ' + errorMessage.left(errorMessage.length() - 2) + ' ';
            returnMessage += System.Label.spc_InvalidPhoneNumberPrefix + errorMessage + System.Label.spc_InvalidPhoneNumberPostfix;
        }

        return returnMessage;
    }
}
/**
* @author Deloitte
* @description This is the test class for spc_CaseStatusIndicatorsctrl
*/
@isTest
private class spc_CaseStatusIndicatorsctrlTest {
  private static Case objCase;
  private static void setupTestdata() {
    List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();

    Account acc = spc_Test_Setup.createPatient('New Patient');
    acc.spc_HIPAA_Consent_Received__c = 'Yes';
    acc.spc_Patient_Mrkt_and_Srvc_consent__c = 'Yes';
    acc.spc_Patient_Services_Consent_Received__c = 'Yes';
    acc.spc_Text_Consent__c = 'Yes';
    acc.spc_Patient_Marketing_Consent_Date__c = Date.valueOf('2014-06-07');
    acc.spc_Patient_HIPAA_Consent_Date__c = Date.valueOf('2014-06-07');
    acc.spc_Patient_Text_Consent_Date__c = Date.valueOf('2014-06-07');
    acc.spc_Patient_Service_Consent_Date__c = Date.valueOf('2014-06-07');
    spc_Database.ins(acc);

    Account phyAcc = spc_Test_Setup.createTestAccount('PC_Physician');
    phyAcc.Name = 'Acc_Name';
    spc_Database.ins(phyAcc);

    spc_Database.ins(apexConstants);
    Account patientAcc1 = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());

    Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
    spc_Database.ins(manAcc);

    PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('Engagement Program SageRx', manAcc.Id);
    engPrgm.PatientConnect__PC_Program_Code__c = 'BREX';
    insert engPrgm;

    objCase = spc_Test_Setup.newCase(acc.Id, 'PC_Program');
    objCase.Status = 'Enrolled';
    objCase.PatientConnect__PC_Engagement_Program__c = engPrgm.Id;
    objCase.spc_WC__c = 'In Progress';
    objCase.PatientConnect__PC_Status_Indicator_3__c = 'Not applicable';
    objCase.PatientConnect__PC_Status_Indicator_4__c = 'Action Required';
    objCase.PatientConnect__PC_Status_Indicator_5__c = 'Complete';
    objCase.PatientConnect__PC_Physician__c = phyAcc.Id;
    spc_Database.ins(objCase) ;
  }

  /**
  * @Name          testspc_CaseStatusIndicatorsctrl
  * @Description   test method
  * @Author        Nikunj Vadi
  * @CreatedDate   2018-08-09
  * @Return        void
  */
  private static testMethod void testspc_CaseStatusIndicatorsctrl() {
    setupTestdata();
    Test.StartTest();

    spc_CaseStatusIndicatorIconController CI = new spc_CaseStatusIndicatorIconController();
    spc_CaseStatusIndicatorIconController.getStatusIndicatorIcons(objCase.Id);
    System.assertNotEquals(objCase, null);
    Test.StopTest();
  }
}
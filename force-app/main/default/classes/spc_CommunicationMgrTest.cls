/**********************************************************************************
* @author Deloitte
* @date Aug 13, 2018
*
* @description This is the test class for spc_CommunicationMgr
*/

@isTest
public class spc_CommunicationMgrTest {
    @testSetup static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

    }
    @isTest
    public static void testEnvelope() {
        Test.startTest();
        List<String> envelop = new List<String>();
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        spc_Database.ins(manAcc);
        PatientConnect__PC_Engagement_Program__c engPrgm = spc_Test_Setup.createEngagementProgram('Engagement Program SageRx', manAcc.Id);
        engPrgm.PatientConnect__PC_Program_Code__c = 'SAGE';
        insert engPrgm;
        Case programCase = new Case();
        programCase.PatientConnect__PC_Engagement_Program__c = engPrgm.ID;
        insert programCase;
        String json = '{"programCaseId":"' + programCase.id + '","flowName":"test","engagementProgramId":"' + engPrgm.id + '"}';
        envelop.add(json);
        Map<Id, Case> mapProgramCase = new Map<Id, Case>();
        mapProgramCase.put(programCase.Id, programCase);
        String rtn;
        spc_CommunicationMgr.execute(envelop);
        rtn = new spc_CommunicationMgr.RecipientHandler().getFAXNumber('12345667');
        rtn = new spc_CommunicationMgr.RecipientHandler().getContactId('12345667');
        rtn = new spc_CommunicationMgr.RecipientHandler().getEmailDomain('test@test.com');
        rtn = new spc_CommunicationMgr.RecipientHandler().getEmailAddress('test@tesst.com');
        Test.stopTest();

    }
}
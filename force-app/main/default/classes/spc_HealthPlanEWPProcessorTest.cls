/**
* @author Deloitte
* @date 17/07/2018
*
* @description This is the Test Class for for Health Plan Processor
*/

@isTest
public class spc_HealthPlanEWPProcessorTest {
    @testSetup static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

    }
    private static final String engagementProgramName = 'HPEWP';
    @isTest static void run() {
        User adminUser = spc_Test_Setup.createUser(spc_Test_Setup.SYS_ADMIN_PROFILE, 'userpctest@testpc.com', 'UserName');
        spc_Database.ins(adminUser);

        System.runAs(adminUser) {
            Test.StartTest();
            Account man = spc_Test_Setup.createTestAccount('Manufacturer');
            List<Account> manufacturers =  new List<Account>();
            manufacturers.add(man);
            manufacturers = (List<Account>) spc_Database.ins(manufacturers);
            Account manufacturer = manufacturers[0];

            Account  patient = spc_Test_Setup.createPatient('testpatient');
            patient = (Account)spc_Database.ups(patient);

            PatientConnect__PC_Engagement_Program__c engagementProgram = spc_Test_Setup.createEngagementProgram(engagementProgramName, manufacturer.Id);
            engagementProgram.PatientConnect__PC_Program_Code__c = engagementProgramName;
            engagementProgram = (PatientConnect__PC_Engagement_Program__c) spc_Database.ups(engagementProgram);

            Case enrollmentCase = spc_Test_Setup.newCase(patient.id, 'PC_Enrollment');
            enrollmentCase.PatientConnect__PC_Engagement_Program__c = engagementProgram.Id;
            enrollmentCase = (Case) spc_Database.ups(enrollmentCase);

            Case programCase =  spc_Test_Setup.newCase(patient.id, 'PC_Program');
            programCase.PatientConnect__PC_Engagement_Program__c = engagementProgram.Id;
            programCase = (Case) spc_Database.ups(programCase);

            enrollmentCase.PatientConnect__PC_Program__c = programCase.Id;
            enrollmentCase.AccountId = patient.Id;
            enrollmentCase = (Case) spc_Database.ups(enrollmentCase);
            Account pay = spc_Test_Setup.createTestAccount('Payer');
            List<Account> payers =  new List<Account>();
            payers.add(pay);
            payers = (List<Account>) spc_Database.ups(payers);

            List<Account> patients = new List<Account>();
            patients.add(patient);
            PatientConnect__PC_Health_Plan__c hp = new PatientConnect__PC_Health_Plan__c();
            PatientConnect__PC_Health_Plan__c hp2 = new PatientConnect__PC_Health_Plan__c();

            String pObjectName = spc_ApexConstants.spc_HEALTH_PLAN;
            String pDeveloperName =  'Private Health Plan';

            RecordType rt = [Select Id, developerName, Name From RecordType Where SobjectType = :pObjectName LIMIT 1];
            hp.RecordTypeId = rt.Id;
            hp.PatientConnect__PC_Payer__c = payers[0].Id;
            hp.spc_Policy_Holder_Phone__c = '12345678909';
            hp.PatientConnect__PC_Cardholder_Relationship_to_Patient__c = 'SELF';
            hp.PatientConnect__PC_Cardholder_s_Date_of_Birth__c = Date.Today();
            hp.PatientConnect__PC_Effective_Date__c = System.today();
            hp.PatientConnect__PC_Expiration_Date__c = System.today() + 1;
            hp2.RecordTypeId = rt.Id;
            hp2.PatientConnect__PC_Payer__c = payers[0].Id;
            hp2.spc_Policy_Holder_Phone__c = '2345678909';
            hp2.PatientConnect__PC_Effective_Date__c = System.today();
            hp2.PatientConnect__PC_Cardholder_Relationship_to_Patient__c = 'FATHER';

            List<Object> values = new List<Object>();
            spc_HealthPlanEWPLightningController.healthPlanPageWrapper h =
                new spc_HealthPlanEWPLightningController.healthPlanPageWrapper(hp, new List<String>());

            h.recordName = rt.Name;
            List<Object> values2 = new List<Object>();
            spc_HealthPlanEWPLightningController.healthPlanPageWrapper h2 =
                new spc_HealthPlanEWPLightningController.healthPlanPageWrapper(hp2, new List<String>());

            h2.recordName = rt.Name;
            Object h_map = JSON.deserializeUntyped(JSON.serialize(h));
            values.add(h_map);
            Object h_map2 = JSON.deserializeUntyped(JSON.serialize(h2));
            values2.add(h_map2);
            //creating pageState as desired by the processor
            Map<String, Object> pageState = new Map<String, Object>();
            pageState.put('lstHealthPlan', values);
            spc_HealthPlanEWPProcessor.processEnrollment(enrollmentCase, pageState);
            Map<String, Object> pageState1 = new Map<String, Object>();
            pageState1.put('lstHealthPlan', values2);

            spc_HealthPlanEWPProcessor.processEnrollment(enrollmentCase, pageState1);
            String aId = patient.Id;

            // this query is to ensure that all the data has been related properly
            List<PatientConnect__PC_Health_Plan__c> updatedItems = [SELECT Id from PatientConnect__PC_Health_Plan__c WHERE PatientConnect__Patient__c = :aId LIMIT 1];
            if (updatedItems != null && updatedItems.size() > 0) {
                system.assert(true, 'success');
            } else {
                system.assert(false, 'failed');
            }
            Test.stopTest();
        }
    }
}
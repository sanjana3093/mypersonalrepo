@isTest
public class CCL_FinishedProduct_Controller_Test {
   @testSetup
    public static void createTestData() {

       final String profileName = CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE;
        final User sysAdminUser = CCL_TestDataFactory.createTestUser(profileName);
        insert sysAdminUser;

        CCL_Therapy__c commercialTherapy = CCL_TestDataFactory.createCommercialTherapy();
        Account orderingHospital= CCL_TestDataFactory.createShipToLocation();
        insert orderingHospital;

        CCL_Order__c order = new CCL_Order__c();
        order.Name='Novartis Batch ID';
        order.CCL_First_Name__c='First Name';
        order.CCL_Last_Name__c='Last Name';
        order.CCL_Middle_Name__c='N';
        order.CCL_Suffix__c='Sr.';
        order.RecordTypeId=CCL_StaticConstants_MRC.ORDER_RECORDTYPE_CLINICAL;
        order.CCL_Date_of_DOB__c='1';
        order.CCL_Month_of_DOB__c='JAN';
        order.CCL_Year_of_DOB__c='1990';
        order.CCL_Hospital_Patient_ID__c='1234';
        order.CCL_Patient_Weight__c=90;
        order.CCL_Patient_Age_At_Order__c=30;
        order.CCL_Treatment_Protocol_Subject_ID__c='CTL198088';
        order.CCL_Protocol_Center_Number__c='1234';
        order.CCL_Protocol_Subject_Number__c='123';
        order.CCL_Consent_for_Additional_Research__c=TRUE;
        order.CCL_PRF_Ordering_Status__c='PRF_Submitted';
        order.CCL_Hospital_Purchase_Order_Number__c='34834349';
        order.CCL_Physician_Attestation__c= TRUE;
        order.CCL_Patient_Eligibility__c= TRUE;
        order.CCL_VA_Patient__c= TRUE;
        order.CCL_Planned_Apheresis_Pick_up_Date_Time__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        order.CCL_Manufacturing_Slot_ID__c='123567';
        order.CCL_Value_Stream__c=89;
        order.CCL_Estimated_FP_Delivery_Date__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        order.CCL_Scheduler_Missing__c=TRUE;
        order.CCL_Returning_Patient__c=TRUE;
        order.CCL_Estimated_Manufacturing_Start_Date__c=date.newInstance(2014, 9, 15);
        order.CCL_Therapy__c=commercialTherapy.id;
        order.CCL_Ordering_Hospital__c=orderingHospital.id;
        insert order;


        CCL_Shipment__c shipment = new CCL_Shipment__c();
        shipment.Name = 'Test';
        shipment.CCL_Number_of_Bags_Shipped__c = 2;
        //shipment.CCL_Expiry_Date_of_Shipped_Bags__c = Date.newInstance(2020, 8, 13);
        shipment.CCL_Product_Acceptance_Status__c ='Accepted';
        //CCL_TestDataFactory.createTestFinishedProductShipment(order.Id, therapy.Id);
        if(shipment!=null) {
            insert shipment;

        if(order!=null ) {
        CCL_Finished_Product__c fp = new CCL_Finished_Product__c();
            fp.Name = 'Test FP';
            fp.CCL_Order__c = order.Id;
            fp.CCL_Shipment__c= shipment.Id;
            fp.CCL_Actual_Batch_Release_Date__c= Date.newInstance(2020, 8, 13);
            fp.CCL_Actual_Infusion_Date_Time__c = Date.newInstance(2020, 9, 13);
            fp.CCL_Manufacturing_Date__c=Date.newInstance(2020, 8, 13);
            //fp.CCL_Infusion_to_Time_Zone__c = 'EST';
            fp.CCL_Expiry_Date__c = Date.newInstance(2020, 8, 13);
            fp.CCL_Infusion_Details__c = 'Infusion reporter';
            fp.CCL_Batch_Status__c = 'Approved';
            fp.CCL_Product_Acceptance_Status__c = 'Product Delivery Accepted';
            fp.CCL_Shipment__c=shipment.id;
            insert fp;

            ContentVersion contentVersion = new ContentVersion(
            Title = 'test',
            PathOnClient = 'test.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
            );
            insert contentVersion;
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

            ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = fp.CCL_Shipment__c;
            cdl.ContentDocumentId = documents[0].Id;
            cdl.shareType = 'V';
            insert cdl;

        }

        }



    }

    static testMethod void getFinishedProductDataTest() {
          CCL_Finished_Product__c finishedProduct = [select Id, Name,CCL_Actual_Batch_Release_Date__c, CCL_Manufacturing_Date__c,CCL_Expiry_Date__c,CCL_Manufacturing_Date_Text__c,
                                               CCL_Actual_Batch_Release_Date_Text__c,
                                               CCL_Actual_Infusion_Date_Time_Text__c,CCL_Infusion_to_Time_Zone__c, CCL_Infusion_Details__c, CCL_Batch_Status__c,CCL_Product_Acceptance_Status__c,
                                                CCL_Shipment__r.CCL_Number_of_Bags_Shipped__c,
                                                CCL_Shipment__r.CCL_Expiry_Date_of_Shipped_Bags_Text__c,
                                                CCL_Shipment__r.CCL_Product_Acceptance_Status__c,
                                                CCL_Expiration_Date_Text__c from CCL_Finished_Product__c where Name=:'Test FP'];
    Test.startTest();
               CCL_FinishedProduct_Controller.getFinishedProductData(finishedProduct.Id);
    Test.stopTest();
    System.assertEquals('equal', 'equal', 'msg');
    }
    static testMethod void getFormattedDateTest() {

        Date value= Date.newInstance(2020, 8, 13);
        CCL_FinishedProduct_Controller.getFormattedDate(value);
    System.assertEquals('equal', 'equal', 'msg');
    }
    static testMethod void checkUserPermissionTest() {
        CCL_FinishedProduct_Controller.checkUserPermission();
    System.assertEquals('equal', 'equal', 'msg');

    }

    static testMethod void checkUserPermissionPrimaryInfusionFlow() {
        CCL_FinishedProduct_Controller.checkUserPermissionPrimaryInfusion();
        System.assertEquals('equal', 'equal', 'msg');

    }

    static testMethod void checkUserName() {
        User infusionreporter = [SELECT FirstName, Id FROM User WHERE FirstName ='test FirstName' LIMIT 1];
        CCL_FinishedProduct_Controller.getUserName(infusionreporter.Id);
        System.assertEquals('test FirstName', infusionreporter.FirstName, 'Mismatch in User Name');

    }


    static testMethod void getUploadedFileDetails() {

          CCL_Finished_Product__c finishedProduct = [select Id, Name,CCL_Actual_Batch_Release_Date__c, CCL_Manufacturing_Date__c,CCL_Expiry_Date__c,CCL_Manufacturing_Date_Text__c,
                                               CCL_Actual_Batch_Release_Date_Text__c,
                                               CCL_Actual_Infusion_Date_Time_Text__c,CCL_Infusion_to_Time_Zone__c, CCL_Infusion_Details__c, CCL_Batch_Status__c,CCL_Product_Acceptance_Status__c,
                                                CCL_Shipment__r.CCL_Number_of_Bags_Shipped__c,
                                                CCL_Shipment__r.CCL_Expiry_Date_of_Shipped_Bags_Text__c,
                                                CCL_Shipment__r.CCL_Product_Acceptance_Status__c,
                                                CCL_Expiration_Date_Text__c from CCL_Finished_Product__c where Name=:'Test FP'];
    Test.startTest();
               CCL_FinishedProduct_Controller.getUploadedFileDetails(finishedProduct.Id);
    Test.stopTest();
    System.assertEquals('equal', 'equal', 'msg');

    }
	 static testMethod void testGetHeader(){
     CCL_Finished_Product__c finishedProduct = [select Id, CCL_Order__c,CCL_Order__r.CCL_Therapy__c, CCL_Order__r.CCL_Ordering_Hospital__c from CCL_Finished_Product__c where Name=:'Test FP'];
    Test.startTest();
               CCL_FinishedProduct_Controller.getShipmentHeader();
         CCL_FinishedProduct_Controller.getTherapyType(finishedProduct.Id,'Shipment');
         CCL_FinishedProduct_Controller.getTherapyType(finishedProduct.CCL_Order__C,'Order');
         CCL_FinishedProduct_Controller.fetchHospitalOptIn(finishedProduct.CCL_Order__r.CCL_Therapy__c,finishedProduct.CCL_Order__r.CCL_Ordering_Hospital__c);
    Test.stopTest();
         System.assertEquals('Test FP', finishedProduct.Name, 'msg'); 
		 }


}
public with sharing class ContactController {

    @AuraEnabled(cacheable=true)
    public static List<Product2> getContactList() {
        return [SELECT Id,Name,RecordTypeId,PSP_Franchise__c,PSP_Portfolio__c,IsActive,PSP_Family__r.Name,
                (select Id,Name from Families__r),
    ( SELECT ID from ProductCareProgramProducts)
    FROM Product2 where RecordType.Name='Product']; 
    }
}
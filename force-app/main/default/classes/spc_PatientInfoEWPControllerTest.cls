/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for PrescriptionSiteOfCareProcessor
    *  @date            06/18/2018
    *  @version         1.0
    *
*********************************************************************************************************/
@isTest
public class spc_PatientInfoEWPControllerTest {
    @testSetup static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);

    }
    /*********************************************************************************
    Method Name    : testGetPicklistEntryMap
    Description    : Test pocklist entry map
    Return Type    : void
    Parameter      : None
    *********************************************************************************/
    @isTest
    public static void testGetPicklistEntryMap() {
        Test.startTest();
        Map<String, List<spc_PatientInfoEWPController.PicklistEntryWrapper>> pickLists = spc_PatientInfoEWPController.getPicklistEntryMap();
        System.assertEquals(True, pickLists.size() > 0);
        Test.stopTest();

    }
    /*********************************************************************************
    Method Name    : testGetPicklistEntryMap
    Description    : Test pocklist entry map
    Return Type    : void
    Parameter      : None
    *********************************************************************************/
    @isTest
    public static void testGetFieldLabels() {
        Test.startTest();
        spc_PatientInfoEWPController.FieldLabels fieldLabels = spc_PatientInfoEWPController.getFieldLabels();
        System.assertEquals(True, fieldLabels != null );
        Test.stopTest();

    }
    /*********************************************************************************
    Method Name    : testGetPicklistEntryMap
    Description    : Test pocklist entry map
    Return Type    : void
    Parameter      : None
    *********************************************************************************/
    @isTest
    public static void testGetExistingAddress() {
        Test.startTest();
        Account patientAccount = new Account();
        patientAccount.PatientConnect__PC_First_Name__c = 'Ron';
        patientAccount.PatientConnect__PC_Last_Name__c = 'Bwon';
        patientAccount.Name = 'Bwon, Ron';
        spc_Database.ins(patientAccount);
        list<spc_PatientInfoEWPController.AddressRecord> existingAddress = spc_PatientInfoEWPController.getExistingAddress(patientAccount.Id);
        //System.assertEquals(True, existingAddress != null );
        Test.stopTest();

    }
    @isTest
    public static void testGetAccountOrApplicant() {
        Test.StartTest();
        Account patient = spc_Test_Setup.createTestAccount('PC_Patient');
        patient.PatientConnect__PC_Email__c = 'sr@sr.com';
        patient.Phone = '7121231413';
        patient.spc_Mobile_Phone__c = '7121231413';
        insert patient;

        PatientConnect__PC_Address__c  patAddress = spc_Test_Setup.createAddress(patient.Id);
        patAddress = (PatientConnect__PC_Address__c)spc_Database.ins(patAddress, false);

        Account manufacturer = spc_Test_Setup.createTestAccount('Manufacturer');
        manufacturer.PatientConnect__PC_Email__c = 'sr1@sr.com';
        manufacturer.Phone = '7121231413';
        manufacturer.spc_Mobile_Phone__c = '7121231413';
        insert manufacturer;
        PatientConnect__PC_Engagement_Program__c  engagement = spc_Test_Setup.createEngagementProgram('programA', manufacturer.Id);
        engagement = (PatientConnect__PC_Engagement_Program__c)spc_Database.ins(engagement, false);

        Case enrollment = spc_Test_Setup.newCase(patient.id, 'PC_Enrollment');
        insert enrollment;

        PatientConnect__PC_Applicant__c applicant = new PatientConnect__PC_Applicant__c( PatientConnect__PC_Last_Name__c = 'lastName',
                PatientConnect__PC_First_Name__c = 'Test First Name',
                PatientConnect__PC_Source__c = true);
        spc_Database.ins(applicant, false);

        enrollment.PatientConnect__PC_ActiveApplicant__c = applicant.Id;
        enrollment = (Case)spc_Database.ups(enrollment);

        Map<String, List<spc_PatientInfoEWPController.PicklistEntryWrapper>> pickList;
        spc_PatientInfoEWPController.FieldLabels fieldList;
        spc_PatientInfoEWPController.AccountRecord acc;
        spc_PatientInfoEWPController.AccountRecord app;
        List<spc_PatientInfoEWPController.AddressRecord> addr;
        pickList = spc_PatientInfoEWPController.getPicklistEntryMap();
        fieldList = spc_PatientInfoEWPController.getFieldLabels();
        acc =  spc_PatientInfoEWPController.getAccountorApplicant(patient.Id, enrollment.id, applicant.Id, null, new List<String>(), '', '');
        spc_PatientInfoEWPController.AccountRecord acc2 = spc_PatientInfoEWPController.getAccountorApplicant(null, enrollment.id, applicant.Id, null, new List<String>(), '', '');
            spc_PatientInfoEWPController.getFieldSetFields('Case','PatientConnect__EnrollPatient_PhysicianInfo');
            spc_PatientInfoEWPController.getDependentOptionsImpl();
            spc_PatientInfoEWPController.getPickListValueNone();
        Test.stopTest();
    }
}
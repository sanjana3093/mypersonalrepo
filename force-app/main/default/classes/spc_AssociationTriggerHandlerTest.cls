/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for spc_AssociationTriggerHandler
    *  @date            06/18/2018
    *  @version         1.0
*********************************************************************************************************/
@isTest
public class spc_AssociationTriggerHandlerTest {
    @testSetup
    static void setup() {
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        Account account = spc_Test_Setup.createTestAccount('Payer');
        insert account;
        system.assertNotEquals(account, NULL);
        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');
        insert programCase;
        system.assertNotEquals(programCase, NULL);


    }

    public static testmethod void testEexcution() {
    case oCase=[select id from case];
        Account oAcc = [select id from Account];
        PatientConnect__PC_Association__c oAsso = new PatientConnect__PC_Association__c();
        oAsso.PatientConnect__PC_Role__c = 'Payer';
        oAsso.PatientConnect__PC_Program__c = oCase.id;
        oAsso.PatientConnect__PC_Account__c = oAcc.id;
        oAsso.PatientConnect__PC_AssociationStatus__c = 'active';
        insert oAsso;
        system.assertNotEquals(oAsso, NULL);

    }

    public static testmethod void testEexcution1() {
        Test.startTest();
        Account manufacturerAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId());
        insert manufacturerAcc;
        PatientConnect__PC_Engagement_Program__c engProgram = new PatientConnect__PC_Engagement_Program__c (Name = 'Brexanolone Engagement Program', PatientConnect__PC_Manufacturer__c = manufacturerAcc.Id, PatientConnect__PC_Program_Code__c = 'BREX');
        insert engProgram;
        Account patientAcc = spc_Test_Setup.createPatient('Test Patient');
        insert patientAcc;
        Case prgmCase = spc_Test_Setup.createCase('Program', Schema.SObjectType.Case.getRecordTypeInfosByName().get('Program').getRecordTypeId(), engProgram.Id, patientAcc.Id);
        insert prgmCase;

        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
        insert hcoAcc;

        PatientConnect__PC_Address__c addr = spc_Test_Setup.createAddress(hcoAcc.Id);
        addr.spc_Fax__c = '12345';
        insert addr;
        PatientConnect__PC_Association__c socAssoc = new PatientConnect__PC_Association__c(PatientConnect__PC_Account__c = hcoAcc.Id, PatientConnect__PC_Program__c = prgmCase.Id, PatientConnect__PC_Role__c = 'HCO', PatientConnect__PC_EndDate__c = System.today() + 10, spc_Address__c = addr.Id);
        insert socAssoc;
        PatientConnect__PC_Interaction__c intr = spc_Test_Setup.createInteraction(prgmCase.Id);
        intr.PatientConnect__PC_Participant__c = hcoAcc.Id;
        insert intr;
        socAssoc.PatientConnect__PC_AssociationStatus__c = 'Inactive';
        update socAssoc;
        PatientConnect__PC_Association__c socAssoc1 = new PatientConnect__PC_Association__c(PatientConnect__PC_Account__c = hcoAcc.Id, PatientConnect__PC_Program__c = prgmCase.Id, PatientConnect__PC_Role__c = 'HCO', PatientConnect__PC_EndDate__c = System.today() + 10, spc_Address__c = addr.Id);
        insert socAssoc1;
        socAssoc1.PatientConnect__PC_AssociationStatus__c = 'Active';
        update socAssoc1;
        socAssoc1.spc_Address__c = addr.Id;
        socAssoc1.spc_Fax__c = null;
        update socAssoc1;
        socAssoc1.PatientConnect__PC_AssociationStatus__c = 'Inactive';
        update socAssoc1;
        Test.stopTest();

    }
}
/*********************************************************************************************************
class Name      : PSP_Product_And_Service_Trigger_Handler 
Description		: Trigger Handler Class for Product And Services Object
@author     	: Saurabh Tripathi 
@date       	: July 10, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi            July 10, 2019          Initial Version
Divya Eduvulapati			November 18,2019	   version1
****************************************************************************************************************/ 
public class PSP_Product_And_Service_Trigger_Handler extends PSP_trigger_Handler {
	/*Instantiation of Trigger impelementation class   */ 
	PSP_Prod_And_Service_Triggr_Implement triggerImpl = new PSP_Prod_And_Service_Triggr_Implement ();   
	 /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 07/10/2019
  	* @Description : Before Insert Method being overriden here
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/    
	public override void beforeInsert () {
    	triggerImpl.checkDuplicate (Trigger.New,(Map<Id,Product2>)Trigger.oldMap);        
    }
     /**************************************************************************************
  	* @author      : Saurabh Tripathi
  	* @date        : 07/10/2019
  	* @Description : Before Update Method being overriden here
  	* @Param       : Null
  	* @Return      : Void
  	***************************************************************************************/
    public override void beforeUpdate () {
        //List of Products 
        final List<Product2> prodList= new List<Product2>();
        for (Product2 newProd : (List<Product2>) Trigger.new) {
            final Product2 oldProduct= (Product2)Trigger.oldMap.get (newProd.Id);
         
            if ((String.isNotBlank (newProd.PSP_ID__c) && !newProd.PSP_ID__c.equals (oldProduct.PSP_ID__c)) || !newProd.Name.equals(oldProduct.Name) 
                || (String.isNotBlank (newProd.StockKeepingUnit) && !newProd.StockKeepingUnit.equals (oldProduct.StockKeepingUnit)) ) {
                 prodList.add(newProd);
            } 
        }
		triggerImpl.checkDuplicate (prodList,(Map<Id,Product2>)Trigger.oldMap);        
    }
    
}
/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for spc_ConfirmTextConsentController
    *  @date            07/18/2018
    *  @version         1.0
*********************************************************************************************************/
@IsTest
public class spc_ConfirmTextConsentControllerTest {

  @testSetup static void setupTestdata() {
    List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
    spc_Database.ins(apexConstants);
  }

  @IsTest static void testconfirmTextConsent() {
    Test.startTest();
    String patientName;
    String patientMobileNumber = '1234567890';
    String testMobileNumber;
    String templateName;
    String programCaseId;

    Account physicianAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
    if (null != physicianAcc) {
      physicianAcc.spc_HIPAA_Consent_Received__c = 'Yes';
      physicianAcc.spc_Patient_Services_Consent_Received__c = 'Yes';
      physicianAcc.spc_Text_Consent__c = 'Yes';
      physicianAcc.PatientConnect__PC_External_ID__c = '1234';
      physicianAcc.spc_Services_Text_Consent__c = 'No';
      physicianAcc.spc_Patient_HIPAA_Consent_Date__c = System.today();
      physicianAcc.spc_Services_Text_Consent_Date__c = System.today();
      physicianAcc.spc_Patient_Text_Consent_Date__c = System.today();
      insert physicianAcc;
      system.assertNotEquals(physicianAcc, NULL);
      Account manf = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId());
      insert manf;
      system.assertNotEquals(manf, null);
      PatientConnect__PC_Engagement_Program__c eg = spc_Test_Setup.createEngagementProgram('testEP', manf.id);
      insert eg;
      Case programCaseRec = spc_Test_Setup.createCases(new List<Account> {physicianAcc}, 1, 'PC_Program').get(0);
      if (null != programCaseRec) {
        programCaseRec.Type = 'Program';
        programCaseRec.PatientConnect__PC_Status_Indicator_1__c = '';
        programCaseRec.PatientConnect__PC_Engagement_Program__c = eg.Id;
        insert programCaseRec;
        system.assertNotEquals(programCaseRec, NULL);
      }

      spc_ConfirmTextConsentController.confirmTextConsent(programCaseRec.id);
      spc_ConfirmTextConsentController.confirmTextConsent(programCaseRec.id);

    }
    Test.stopTest();
  }
}
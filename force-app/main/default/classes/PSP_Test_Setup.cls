/********************************************************************************************************
*  @author          Deloitte
*  @description     This class is used for storing all common methods which will be used by the Test Methods
*  @date            July 10, 2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi           July 10, 2019          Initial Version
****************************************************************************************************************/

public  with Sharing class PSP_Test_Setup {
    private PSP_Test_Setup () {
        //Private constructor to prevent instantiation
    }
    /*********************************************************************************
Method Name    : setDataforApexTestConstants
Author         : Pratik Raj
Description    : set Data for Apex Test Constants
Return Type    : Case
*********************************************************************************/
    public static  List<PSP_ApexConstantsSetting__c> setDataforApexConstants() {
        final List<sObject> lstObj = Test.loadData(PSP_ApexConstantsSetting__c.sObjectType, 'PSP_ApexConstantsSetting_Data');
        Database.insert(lstObj, false);
        return new List<PSP_ApexConstantsSetting__c>();
    }
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating a CareProgram object
@return           CareProgram
*********************************************************************************/
    public static CareProgram createTestCareProgram (String name,Id parentProgram) {
        final CareProgram  cProgram = new CareProgram ();
        cProgram.Name = name;
        cProgram.ParentProgramId = parentProgram;
        cProgram.Status=PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.PICKLIST_VAL_STATUS);
        //cProgram.Status= 'Active';
        cProgram.StartDate = System.today () + 10;
        cProgram.PSP_Program_Sector__c = 'Public';
        return cProgram;
    }
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating a CareProgram object
@return           card and Coupon
*********************************************************************************/
    public static PSP_Card_and_Coupon__c createCard () {
        return new PSP_Card_and_Coupon__c (Name='1234567891234566');
    } 
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating a CareProgram object
@return           contact
*********************************************************************************/
    public static Contact createContact () {
        final Contact  con = new Contact (HealthCloudGA__PreferredName__c='Test Con',LastName='Lcon');
        con.HealthCloudGA__SourceSystemId__c='sktest';
        con.PSP_Specialty__c='ADMINISTRATIVE';
        return con;
    } 
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating a CareProgram object
@return           contact
*********************************************************************************/
    public static CareProgramEnrollee createProgramEnrollee () {
        final CareProgramEnrollee  cpe = new CareProgramEnrollee (Name='Test Program Enrollee');
        return cpe;
    } 
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating a CareProgram object
@return           purchase transaction
*********************************************************************************/
    public static List<PSP_Purchase_Transaction__c> createPurchasetransaction () {
        return new List<PSP_Purchase_Transaction__c> {new PSP_Purchase_Transaction__c(Name='Test',PSP_EAN__c='123456789',PSP_Pharmacy_External_ID__c='skmsT5dd',PSP_Patient_Reference_ID__c='PA-0000000156',PSP_Card_Number__c = '12346',PSP_HCP_Reference_Id__c='sktest' )};
            } 
    
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating a CareProgram Enroll Card object
@return           CareProgram Enroll Card object
*********************************************************************************/
    public static CareProgramEnrollmentCard createTestCareProgramEnrollCard (String name,String status,String cardNumber, Id enrollId,
                                                                             String assignee,String cardType, Id careProgramId) {
                                                                                 final CareProgramEnrollmentCard  cEnrollCrdProg = new CareProgramEnrollmentCard ();
                                                                                 cEnrollCrdProg.Name = name;
                                                                                 cEnrollCrdProg.status = status;
                                                                                 cEnrollCrdProg.CardNumber = cardNumber;
                                                                                 cEnrollCrdProg.CareProgramEnrolleeId = enrollId;
                                                                                 return cEnrollCrdProg;
                                                                             } 
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating a Care Program Product object
@return           Care Program Product
*********************************************************************************/
    public static Product2 createProduct (String name) {
        final Product2  product = new Product2 ();
        product.Name = name;
        product.IsActive = true;
        product.RecordTypeId = PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.PRODUCT_RT_PRODUCT, Product2.getSObjectType());
        return product;
    }
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating a Care Program Product object
@return           Care Program Product
*********************************************************************************/
    public static CareProgramProduct createTestCareProgramProduct (CareProgram cProgram,Product2 product) {
        final CareProgramProduct  careProProduct = new CareProgramProduct ();
        careProProduct.Name = cProgram.Name + '-'+product.Name;
        careProProduct.CareProgramId = cProgram.Id;
        careProProduct.ProductId = product.Id;
        return careProProduct;
    } 
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating a Care Program Enrollee object
@return           Care Program Enrollee object
*********************************************************************************/
    public static CareProgramEnrollee createTestCareProgramEnrollment (String name,Id careProgramId) {
        final CareProgramEnrollee  cProgEnrollee = new CareProgramEnrollee ();
        cProgEnrollee.Name = name;
        cProgEnrollee.CareProgramId = careProgramId;
        cProgEnrollee.UserId = UserInfo.getUserId ();
        return cProgEnrollee;
    } 
    
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating a CareProgramEnrolleeProduct object
@return           CareProgramEnrolleeProduct
*********************************************************************************/
    public static CareProgramEnrolleeProduct createTestCareProgramEnrolleeProduct (String name,Id careProgramProId,Id cPrgmEnroleeId) {
        final CareProgramEnrolleeProduct  newProgram = new CareProgramEnrolleeProduct ();
        newProgram.CareProgramEnrolleeId = cPrgmEnroleeId;
        newProgram.CareProgramProductId = careProgramProId;
        newProgram.Name=name;
        return newProgram;
    }
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for creating a Case object
@return           Case
*********************************************************************************/
    public static Case createTestAdverseEventCase (String name,Id parentProgram) {
        final Case  newCase = new Case ();
        newCase.PSP_Care_Program_Enrollee_Product__c = parentProgram;
        newCase.Status = PSP_ApexConstants.STATUS_NEW;
        newCase.Subject =  PSP_ApexConstants.ADVERSE_EVENT;
        newCase.Priority = 'Normal';
        newCase.Origin='Phone';
        newCase.Description= PSP_ApexConstants.ADVERSE_EVENT;
        return newCase;
    }
    
    
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for business account
@return           Account
*********************************************************************************/
    public static Account createTestBusinessAccount(String name,String acctype) {
        final Account  newAccount = new Account ();
        newAccount.Name=name;
        newAccount.RecordTypeId=PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.ACCOUNT_RT_BUSINESS, Account.getSObjectType());
        newAccount.PSP_Account_Type__c=acctype;
        return newAccount;
    }
    /*********************************************************************************
@author           Deloitte
@Description      This method is used for business account
@return           Account
*********************************************************************************/
    public static Account getPersonAccount(String name) {
        
        Id rAccountRoleRecordTypeId = Schema.SObjectType.HealthCloudGA__ReciprocalRole__c.getRecordTypeInfosByName().get('Account Role').getRecordTypeId();
        List<HealthCloudGA__ReciprocalRole__c> reciRole = new List<HealthCloudGA__ReciprocalRole__c>();
        HealthCloudGA__ReciprocalRole__c rr9 = new HealthCloudGA__ReciprocalRole__c();
        rr9.HealthCloudGA__InverseRole__c = 'Other';
        rr9.Name = 'Other';
        rr9.RecordTypeId = rAccountRoleRecordTypeId ;
        reciRole.add(rr9);
        
        insert reciRole; 
        final Account  newAccount = new Account ();
        newAccount.lastname=name;
        newAccount.RecordTypeId=PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.ACCOUNT_RT_PATIENT, Account.getSObjectType());
        newAccount.PersonBirthdate=System.today();
        //newAccount.PSP_Account_Type__c=Acctype;
        return newAccount;
    }
    /**********************************************************************************
@author           Deloitte
@Description      This method is used for creating a CareProgram object with Record
@return           CareProgram
*************************************************************************************/
    public static CareProgram createTestCareProgramWithRecordType(String name, Id parentProgram) {
        final CareProgram cProgram = new CareProgram();
        cProgram.Name = name;
        cProgram.ParentProgramId = parentProgram;
        cProgram.Status = PSP_ApexConstants.STATUS_ACTIVE;
        cProgram.StartDate = System.today() + 10;
        cProgram.PSP_Program_Sector__c = 'Public';
        cProgram.RecordTypeId = Schema.SObjectType.CareProgram.getRecordTypeInfosByName().get('Care Program').getRecordTypeId();
        return cProgram;
    }
}
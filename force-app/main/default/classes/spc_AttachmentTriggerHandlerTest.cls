/*********************************************************************************
* @author Deloitte
* @date July 4,2018
* @description This is the test class for spc_AttachmentTriggerHandler
*********************************************************************************/
@isTest
public class spc_AttachmentTriggerHandlerTest {
    @testSetup
    static void setup() {
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
    }

    public static testmethod void createDocumentFromFax() {
        ZPAPER__zStack__c zpaper = new  ZPAPER__zStack__c();
        zpaper.ZPAPER__newFax__c = true;
        insert zpaper;
        Attachment newAttachment = new Attachment();
        newAttachment.ParentId = zpaper.id;
        newAttachment.Name = 'Test attachment';
        newAttachment.Body = Blob.valueOf(Label.spc_attachment_body);
        newAttachment.ContentType = 'application/pdf';
        insert newAttachment;
        system.assertNotEquals(newAttachment, NULL);
    }

    public static testmethod void createDocumentFromFaxDocuSign() {
        Account manf2 = spc_Test_Setup.createTestAccount('Manufacturer');
        manf2.name = 'manufacture2';
        insert manf2;
        system.assertNotEquals(manf2, NULL);

        PatientConnect__PC_Engagement_Program__c eg2 = spc_Test_Setup.createEngagementProgram('test EP 2', manf2.id);
        insert eg2;
        system.assertNotEquals(eg2, NULL);
        PatientConnect__PC_Document__c doc = spc_Test_Setup.createDocument('Email - Outbound', 'completed', eg2.id);
        insert doc;
        system.assertNotEquals(doc, NULL);
        dsfs__DocuSign_Status__c docuSign = new  dsfs__DocuSign_Status__c();
        docuSign.dsfs__Subject__c = 'ABC';
        docuSign.spc_Inbound_Document__c = doc.Id;
        insert docuSign;
        Attachment newAttachment = new Attachment();
        newAttachment.ParentId = docuSign.id;
        newAttachment.Name = 'Test attachment';
        newAttachment.Body = Blob.valueOf(Label.spc_attachment_body);
        newAttachment.ContentType = 'application/pdf';
        insert newAttachment;
        system.assertNotEquals(newAttachment, NULL);
    }


}

/**
* @author Deloitte
* @date 08/17/2018
*
* @description This is the Test Class for spc_eLetterEmailTemplateSender
*/

@isTest
public class spc_eLetterEmailTemplateSenderTest {
    public static final String MOCK_SENDER = 'spc_Test_MockELetterSender';

    @testSetup static void setup() {
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User objUser = new User(Alias = 'test', email = 'TestSysAdmin@test123.com', emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', country = 'United States', profileid = p.Id,
                                timezonesidkey = 'America/Los_Angeles', username = System.currentTimeMillis() + 'TestSysAdmin@test123.com');

        spc_Database.ins(objUser);
        System.runAs(objUser) {
            //Create an Email Template
            EmailTemplate validEmailTemplate = new EmailTemplate();
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = 'test';
            validEmailTemplate.DeveloperName = 'test';
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();
            insert validEmailTemplate;
            // create a record for the custom setting
            PatientConnect__PC_System_Settings__c cSetting = new PatientConnect__PC_System_Settings__c();
            cSetting.PatientConnect__PC_eLetter_Sender_Class__c = MOCK_SENDER;
            spc_Database.ins(cSetting);
            List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
            spc_Database.ins(apexConstants);
            spc_ApexConstantsSetting__c cs = spc_ApexConstantsSetting__c.getInstance('ELETTER_SENDER_CLASS');
            update cs;
        }

    }

    @isTest static void testSendLetter() {
        test.startTest();
        // Create a Manufacturer account
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;
        // Create a Engagement Program
        PatientConnect__PC_Engagement_Program__c program = spc_Test_Setup.createEngagementProgram('ABC', manAcc.ID);
        insert program;
        //Create Physician account
        Account account = spc_Test_Setup.createTestAccount('PC_Physician');
        account.Name = 'Physician Name';
        account.PatientConnect__PC_Specialist_Type__c = 'Cardiologist';
        account.Type = 'Specialist';
        account.PatientConnect__PC_Sub_Type__c = 'Other';
        account.PatientConnect__PC_Email__c = 'test@test.com';
        account.spc_Patient_Mrkt_and_Srvc_consent__c = System.Label.spc_No;
        insert account;
        //Create program case
        Case programCase = spc_Test_Setup.newCase(account.Id, spc_ApexConstants.CASE_PROGRAM_RECORD_TYPE);
        programCase.PatientConnect__PC_Engagement_Program__c = program.ID;
        insert programCase;
        PatientConnect__PC_Association__c association = createAssociation(account, programCase, 'Treating Physician', Date.today().addDays(1));
        //Create eLetter
        PatientConnect__PC_eLetter__c eletter = spc_Test_Setup.createEeletter('Program', 'Email Template');
        insert eletter;
        
        Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
        attach.parentId=eletter.id;
        insert attach;
		
        // instantiate spc_eltterService sendLetter method
        spc_eLetterService.sendLetter(programCase.Id, eLetter, 'Email', new List<Account> {account}, FALSE);
        List<PatientConnect__PC_Document__c> checkDocument = [SELECT Id FROM PatientConnect__PC_Document__c];
        // check if the document is created
        system.assertEquals(checkDocument.size(), 1);
        spc_eLetterService.sendLetter(programCase.Id, eLetter, 'Email', new List<Account> {account}, TRUE);
        test.stopTest();
    }

    private static PatientConnect__PC_Association__c createAssociation(Account account, Case caseObj, String roleName, Date endDate) {
        PatientConnect__PC_Association__c association = new PatientConnect__PC_Association__c (PatientConnect__PC_Account__c = account.Id, PatientConnect__PC_Program__c = caseObj.Id, PatientConnect__PC_Role__c = roleName, PatientConnect__PC_EndDate__c = endDate);
        return (PatientConnect__PC_Association__c)spc_Database.ins(association);
    }

}
@isTest
public class spc_DocumentLogTriggerHandlerTest {
    
    @isTest
    static void testDocumentLog_1() {
        
        Test.startTest();        
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User objUser = new User(Alias = 'test', email = 'TestSysAdmin@test123.com',emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                                localesidkey = 'en_US',country = 'United States', profileid = p.Id,
                                timezonesidkey = 'America/Los_Angeles', username = System.currentTimeMillis()+'TestSysAdmin@test123.com');
        
        spc_Database.ins(objUser);
        
        Account patientAcc = spc_Test_Setup.createPatient('Patient Name');
        insert patientAcc;
        
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;
        
        Case prgCase1 = spc_Test_Setup.newCase(patientAcc.Id, 'PC_Program');
        insert prgCase1;
        
        Account patientAcc1 = spc_Test_Setup.createPatient('Patient Test');
        insert patientAcc1;
        
        Case prgCase2 = spc_Test_Setup.newCase(patientAcc1.Id, 'PC_Program');
        insert prgCase2;
        
        Case inqCase2 = spc_Test_Setup.newCase(patientAcc.Id, 'spc_Inquiry');
        insert inqCase2;
        
        Case inqCase = spc_Test_Setup.newCase(patientAcc1.Id, 'spc_Inquiry');
        insert inqCase;
        
        PatientConnect__PC_Engagement_Program__c engProgram = spc_Test_Setup.createEngagementProgram('Test EP', manAcc.Id);
        
        PatientConnect__PC_Document__c docNew = spc_Test_Setup.createDocument(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_FAX_INBOUND), 'Sent', engProgram.Id);
        insert docNew;
        
        //hco account
        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
        insert hcoAcc; 
        system.assertNotEquals(hcoAcc,NULL);
        
        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(prgCase1.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        
        insert interaction;
        
        PatientConnect__PC_Document_Log__c docLog = new PatientConnect__PC_Document_Log__c();
        
        docLog.PatientConnect__PC_Program__c = prgCase1.Id;
        docLog.PatientConnect__PC_Document__c = docNew.Id;
        docLog.spc_Inquiry__c = inqCase2.Id;
        docLog.PatientConnect__PC_Interaction__c = Interaction.Id;
        
        insert docLog;
        
        docLog.PatientConnect__PC_Program__c = prgCase2.Id;
        update docLog; 
        
        docLog.spc_Inquiry__c = inqCase.Id;
        update docLog;
        
        PatientConnect__PC_Document_Log__c docLog1 = new PatientConnect__PC_Document_Log__c();
        
        docLog1.PatientConnect__PC_Document__c = docNew.Id;
        docLog1.spc_Inquiry__c = inqCase2.Id;
        
        insert docLog1;
        
        docLog1.PatientConnect__PC_Program__c = null;
        docLog1.spc_Inquiry__c = inqCase.Id;
        update docLog1;
        Test.stopTest();
    }
    
    @isTest
    static void testDocumentLog_2() {
        Test.startTest();
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        
        Account patientAcc = spc_Test_Setup.createPatient('Patient Name');
        insert patientAcc;
        
        Account manAcc = spc_Test_Setup.createTestAccount('Manufacturer');
        insert manAcc;
        
        PatientConnect__PC_Engagement_Program__c engProgram = spc_Test_Setup.createEngagementProgram('Test EP', manAcc.Id);
        
        Account patientAcc1 = spc_Test_Setup.createPatient('Patient Test');
        insert patientAcc1;
        
        Case prgCase1 = spc_Test_Setup.newCase(patientAcc.Id, 'PC_Program');
        insert prgCase1;
        
        Case prgCase2 = spc_Test_Setup.newCase(patientAcc1.Id, 'PC_Program');
        insert prgCase2;
        
        Case inqCase2 = spc_Test_Setup.newCase(patientAcc.Id, 'spc_Inquiry');
        insert inqCase2;
        
        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
        insert hcoAcc;
        
        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(prgCase1.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        
        insert interaction;
        
        //Create interaction
        PatientConnect__PC_Interaction__c interaction1 = new PatientConnect__PC_Interaction__c();
        interaction1 = spc_Test_Setup.createInteraction(prgCase2.Id);
        interaction1.PatientConnect__PC_Participant__c = hcoAcc.Id;
        insert interaction1;
        
        PatientConnect__PC_Document__c docNew = spc_Test_Setup.createDocument(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_FAX_INBOUND), 'Sent', engProgram.Id);
        insert docNew;
        
        PatientConnect__PC_Document_Log__c docLog = new PatientConnect__PC_Document_Log__c();
        docLog.PatientConnect__PC_Program__c = prgCase1.Id;
        docLog.PatientConnect__PC_Document__c = docNew.Id;
        docLog.spc_Inquiry__c = inqCase2.Id;
        docLog.PatientConnect__PC_Interaction__c = Interaction.Id;
        insert docLog;
        
        PatientConnect__PC_Program_Coverage__c pcc=new PatientConnect__PC_Program_Coverage__c();
        pcc.PatientConnect__PC_Program__c=prgCase1.id;
        insert pcc;
        
        PatientConnect__PC_Program_Coverage__c pcc1=new PatientConnect__PC_Program_Coverage__c();
        pcc1.PatientConnect__PC_Program__c=prgCase2.id;
        insert pcc1;
        system.assertNotEquals(pcc1,NULL);
        
        PatientConnect__PC_Document_Log__c docLog2 = new PatientConnect__PC_Document_Log__c();
        docLog2.PatientConnect__PC_Document__c = docNew.Id;
        docLog2.PatientConnect__PC_Coverage__c = pcc.Id;
        insert docLog2;
        
        docLog2.PatientConnect__PC_Program__c = null;
        docLog2.PatientConnect__PC_Coverage__c = pcc1.Id;
        update docLog2;
        
        PatientConnect__PC_Document__c docNew1 = spc_Test_Setup.createDocument(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_EMAIL_INBOUND), 'Sent', engProgram.Id);
        insert docNew1;
        
        PatientConnect__PC_Document__c docNew2 = spc_Test_Setup.createDocument(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_FAX_OUTBOUND), 'Sent', engProgram.Id);
        insert docNew2;
        
        PatientConnect__PC_Document__c docNew3 = spc_Test_Setup.createDocument(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_MAIL_OUTBOUND), spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_STATUS_READYTOSEND), engProgram.Id);
        insert docNew3;
        
        PatientConnect__PC_Document_Log__c docLog3 = new PatientConnect__PC_Document_Log__c();
        
        docLog3.PatientConnect__PC_Document__c = docNew2.Id;
        docLog3.PatientConnect__PC_Program__c = prgCase1.Id;
        docLog3.PatientConnect__PC_Interaction__c = interaction.Id;
        insert docLog3;
        system.assertNotEquals(docLog3,NULL);
        
        docLog3.PatientConnect__PC_Program__c = null;
        docLog3.PatientConnect__PC_Interaction__c = Interaction1.Id;
        update docLog3;
        
        PatientConnect__PC_Document_Log__c docLog4 = new PatientConnect__PC_Document_Log__c();
        
        docLog4.PatientConnect__PC_Document__c = docNew3.Id;
        docLog4.PatientConnect__PC_Program__c = prgCase1.Id;
        docLog4.spc_Inquiry__c = inqCase2.Id;
        docLog4.PatientConnect__PC_Interaction__c = interaction.Id;
        insert docLog4;
        
        Test.stopTest();
    }
    
}
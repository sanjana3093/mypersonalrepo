global class GetRecordTypeIds {
    /*transient global String accRecordType {get; set;}
    transient global String addrRecordType {get; set;}*/
    public string physicianRecordTypeId {get;set;}
    public string hcoRecordTypeId {get;set;}
    public string pharmacyRecordTypeId {get;set;}
    public string payerRecordTypeId {get;set;}
    public spc_VeevaWidgetParameters__c veevaValues {get;set;}
	public GetRecordTypeIds (ApexPages.StandardController stdController)
    {
     	physicianRecordTypeId = spc_ApexConstants.getRTId(spc_ApexConstants.RecordTypeName.ACCOUNT_RTDEV_PHYSICIAN, Account.SobjectType);
        hcoRecordTypeId = spc_ApexConstants.getRTId(spc_ApexConstants.RecordTypeName.ACCOUNT_RTDEV_HCO, Account.SobjectType);
        pharmacyRecordTypeId = spc_ApexConstants.getRTId(spc_ApexConstants.RecordTypeName.ACCOUNT_RTDEV_PHARMACY, Account.SobjectType);
        payerRecordTypeId = spc_ApexConstants.getRTId(spc_ApexConstants.RecordTypeName.ACCOUNT_RTDEV_PAYER, Account.SobjectType);
        
		//Check if Custom setting is configured 
		if(spc_VeevaWidgetParameters__c.getInstance('Veeva') != null){
			veevaValues = spc_VeevaWidgetParameters__c.getInstance('Veeva');
			//Parsing the base64 encoded password
            Blob passwordBlob = EncodingUtil.base64Decode(veevaValues.password__c);
            if(null != passwordBlob){
                veevaValues.password__c = passwordBlob.toString();
            }
		}else{
			veevaValues = new spc_VeevaWidgetParameters__c();
		}
        
    }
  /*  global String getaccRecordType(){
        accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('PC_HCP').getRecordTypeId();
        return accRecordType;
    }*/
    
    
	
	@RemoteAction
	global static String getAddressId(String VeevaId) {
		List<PatientConnect__PC_Address__c> addrId;
		try {
			addrId = [Select Id from PatientConnect__PC_Address__c where Veeva_ID__c=:VeevaId];
		} catch(Exception e) {
			 System.debug(e);
		}
		if(addrId.size() > 0) {
		  return addrId[0].Id;
		} else {
			  return 'NOID';
		  }
	}
		
	}	

public class CCL_CapacityAPIMockHttpCallout implements HttpCalloutMock {
    // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"access_token":"hIz3KZvYf6a0mgFHB4HnXJSEIuHh6Q4esTIZxW8WPq5wShpz8yFsYj","Therapy":"KYMRIAH - Pediatric ALL US","Subject_ID":"","Pickup_Site_ID":"0200001122","DropOff_Site_ID":"0200001122","NVS_ID":"","Status":"OK","Error_Code":"","Error_Message":"","QualifiedSlots":[{"APHPickup_Date":"31-08-2020","ValueStream":"21","FP_Delivery_Date":"20-09-2020","Manufacturing_Plant":"US33","Day_0_Date":"10-09-2020","SlotID":"xxxncnc1234"},{"APHPickup_Date":"01-09-2020","ValueStream":"21","FP_Delivery_Date":"21-09-2020","Manufacturing_Plant":"US33","Day_0_Date":"11-09-2020","SlotID":"xxxncnc1235"},{"APHPickup_Date":"01-09-2020","ValueStream":"21","FP_Delivery_Date":"22-09-2020","Manufacturing_Plant":"US33","Day_0_Date":"12-09-2020","SlotID":"xxxncnc1236"}]}');
        res.setStatusCode(200);
        res.setStatus('OK');
        return res;
    }
}
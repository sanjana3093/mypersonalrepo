/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for spc_PhysicianEWPProcessor
    *  @date            08/07/2018
    *  @version         1.0
    *
*********************************************************************************************************/
@isTest
public class spc_PhysicianEWPProcessorTest {
    @testSetup static void setup() {
        List<spc_ApexConstantsSetting__c>  apexConstants =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstants);
    }
    @isTest static void testProcessEnrollment() {
        test.startTest();
        Id ID_PATIENT_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PATIENT)).getRecordTypeId();
        //Create Patient Account
        Account account = spc_Test_Setup.createAccount(ID_PATIENT_RECORDTYPE);
        Id ID_PHYSICIAN_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_PHYSICIAN)).getRecordTypeId();
        Account physicianAccount = spc_Test_Setup.createAccount(ID_PHYSICIAN_RECORDTYPE);
        if (null != account) {
            insert account;
        }
        if (null != physicianAccount) {
            insert physicianAccount;
        }
        //Create Enrollment Case
        Case enrollmentCase = spc_Test_Setup.newCase(account.id, 'PC_Enrollment');
        //Create Program Case
        Case programCase = spc_Test_Setup.newCase(account.id, 'PC_Program');
        if (null != programCase) {
            insert programCase;
            enrollmentCase.PatientConnect__PC_Program__c = programCase.id;
            if (null != enrollmentCase) {
                insert enrollmentCase;
            }
        }
        Map<String, Object> pageState = new Map<String, Object>();
        Map<String, Object> persistentPhysicianInfo = new Map<String, Object>();

        persistentPhysicianInfo.put('Id', physicianAccount.id);

        pageState.put('selectedResult', persistentPhysicianInfo);

        spc_PhysicianEWPProcessor.processEnrollment(enrollmentCase, pageState);

        Map<String, Object> pageState2 = new Map<String, Object>();
        Map<String, Object> persistentPhysicianInfo2 = new Map<String, Object>();

        persistentPhysicianInfo2.put('patient', 'random');

        pageState2.put('selectedResult', persistentPhysicianInfo2);

        spc_PhysicianEWPProcessor.processEnrollment(enrollmentCase, pageState2);
        Case enrollmentCase2 = spc_Test_Setup.newCase(account.id, 'PC_Enrollment');
        if (null != enrollmentCase2) {
            insert enrollmentCase2;
        }
        try {
            spc_PhysicianEWPProcessor.processEnrollment(enrollmentCase2, pageState2);

        } catch (Exception ex) {
            System.assert(ex != null);
        }
        try {

            spc_PhysicianEWPProcessor.processEnrollment(enrollmentCase, null);
        } catch (Exception ex) {
            System.assert(ex != null);
        }
        System.assert(persistentPhysicianInfo.size() > 0);
        test.stopTest();
    }
}
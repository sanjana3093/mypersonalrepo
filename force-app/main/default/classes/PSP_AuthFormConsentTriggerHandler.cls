/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the trigger handler class for auth Form Consent 
*  @date            09/18/2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
deloitte            Nov 21, 2019     Initial Version
****************************************************************************************************************/
public class PSP_AuthFormConsentTriggerHandler extends PSP_trigger_Handler {

    /* trigger implementation */    
	final PSP_AuthFormConsentTriggerImpl triggerImpl = new PSP_AuthFormConsentTriggerImpl ();
    
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Before Insert method 
    *  @date            Nov 21, 2019
    *  @version         1.0
    *********************************************************************************************************/
    public override void beforeInsert () {
        final List<AuthorizationFormConsent> consentLst = new List<AuthorizationFormConsent>();
       
        for (AuthorizationFormConsent newConsent :  (List<AuthorizationFormConsent>)Trigger.new) { 
            if( String.isNotBlank(newConsent.ConsentGiverId)) {
                consentLst.add(newConsent);
                            }
        }
        if(!consentLst.isEmpty()) {
            triggerImpl.validateParty(consentLst);
        }
    }
    
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     Before Update method of Case
    *  @date            Nov 21, 2019
    *  @version         1.0
    *********************************************************************************************************/
    public override void beforeUpdate () {
        final List<AuthorizationFormConsent> consentLst = new List<AuthorizationFormConsent>();
        for (AuthorizationFormConsent newConsent :  (List<AuthorizationFormConsent>)Trigger.new) { 
            if( String.isNotBlank(newConsent.ConsentGiverId)) {
                consentLst.add(newConsent);
            }
        }
        if(!consentLst.isEmpty()) {
            triggerImpl.validateParty(consentLst);
        }
    }
}
/********************************************************************************************************
*  @author          Deloitte
*  @date            13/08/2018
*  @description   This is the class Zpaper
*  @version         1.0
*********************************************************************************************************/
public class spc_zPaperData {

  //List of Template Data Records
  List<spc_Template_Data__c> lstTD = new List<spc_Template_Data__c>() ;

  //List of Cases to update
  List<Case> CaselstUpdate = new List<Case>();

  //List of Case IDS
  List<ID> CaseIDs = new List<ID>();

  Map<ID, spc_Template_Data__c> CaseTDMap = new Map<ID, spc_Template_Data__c>();

  //Health Plan Type = Primary
  String Primary = spc_ApexConstants.getHelthPlanType(spc_ApexConstants.HealthPlanType.HEALTH_PLANTYPE_PRIMARY);
  //Health Plan Type = Secondary
  String Secondary = spc_ApexConstants.getHelthPlanType(spc_ApexConstants.HealthPlanType.HEALTH_PLANTYPE_SECONDARY);

  //Health Plan Type = Tertiary
  String Tertiary = spc_ApexConstants.getHelthPlanType(spc_ApexConstants.HealthPlanType.HEALTH_PLANTYPE_TERTIARY);

  //List Of HealthPlan Types
  List<String> HealthTypes = new List<String> {Primary, Secondary, Tertiary};

  //List Of BIBVCase Types
  List<String> BIBVTypes = new List<String>();

  //Contructor List of cases as parameter
  public spc_zPaperData(List<Case> LstCase) {

    //Create Map of Case and template Data
    CaseIds = createMap(LstCase);

    //Poulate Accounts on Template Data
    relatedAccounts(CaseIDs);

    //Populate interaction field
    relatedInteraction(CaseIDs);

    //Related Program Cases
    relatedCases(CaseIDs);

    //Related Program Coverage
    relatedProgramCoverage(CaseIDs);

    //DML Operations
    dMLOperation();

  }

  /********************************************************************************************************
  *  @author          Deloitte
  *  @date            13/08/2018
  *  @description   Create Map of CaseID to template Data record
  * @Param         List<Case> LstCase
  * @Return        List<ID>
  *  @version         1.0
  *********************************************************************************************************/
  public List<ID> createMap(List<Case> LstCase) {


    For(Case CR : LstCase) {
      CaseIDs.add(CR.ID);
      if (CR.spc_Template_Data__c != null) {
        CAseTDMap.put(CR.ID, CR.spc_Template_Data__r);

      } else {
        CaseTDMap.put(CR.ID, new spc_Template_Data__c());

        //Add in a list to update
        CaselstUpdate.add(CR);
      }
    }
    return CaseIDs;
  }

  /********************************************************************************************************
  *  @author          Deloitte
  *  @date            31/07/2018
  *  @description   Related Accounts
  * @Param         List<ID> CaseIDs
  * @Return        void
  *  @version         1.0
  *********************************************************************************************************/
  public void relatedAccounts(List<ID> CaseIDs) {
    //Specialty Pharmacy Role
    String sphRole = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_SPHARMACY);
    String externalCompoundingPhRole = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_EXT_COMP_PHARMACY);
    //HCO  Role
    String fdvrole = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.FREE_DRUG_VENDOR ) ;
    String hcoRole = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_HCO);
    String phyrole = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_TREATING_PHYSICIAN ) ;
    String referringPhyrole = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_REFERRING_PHYSICIAN ) ;
    String designatedCaregiverRole = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_DESIGNATED_CAREGIVER ) ;
    //List Of Roles
    List<String> Roles = new List<String> {sphRole, hcoRole, fdvrole, externalCompoundingPhRole, phyrole, referringPhyrole, designatedCaregiverRole};

    For(PatientConnect__PC_Association__c PA : [Select Id, PatientConnect__PC_Role__c, spc_Address__c, PatientConnect__PC_AssociationStatus__c, PatientConnect__PC_Account__c, PatientConnect__PC_Program__c
        From PatientConnect__PC_Association__c
        Where PatientConnect__PC_Role__c IN :Roles AND PatientConnect__PC_Program__c IN :CaseIDs]) {


      If(PA.PatientConnect__PC_Role__c != null) {
        //Populate speciality pharmacy in template data object
        If(PA.PatientConnect__PC_Role__c == sphRole && PA.PatientConnect__PC_Account__c != null
           && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {
          //TD.spc_Specialty_Pharmacy__c = PA.PatientConnect__PC_Account__c;
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_SPP_Association__c  = PA.id;
        } else if (PA.PatientConnect__PC_Role__c == sphRole && PA.PatientConnect__PC_Account__c != null
                   && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.ASSOCIATION_STATUS_INACTIVE) {
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_SPP_Association__c  = null;
        }
        If(PA.PatientConnect__PC_Role__c == externalCompoundingPhRole && PA.PatientConnect__PC_Account__c != null
           && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_ECP_Association__c  = PA.id;
        } else if (PA.PatientConnect__PC_Role__c == externalCompoundingPhRole && PA.PatientConnect__PC_Account__c != null
                   && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.ASSOCIATION_STATUS_INACTIVE) {
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_ECP_Association__c  = null;
        }

        //Populate HCO in template data object
        //Populate caregiver details
        If(PA.PatientConnect__PC_Role__c == designatedCaregiverRole && PA.PatientConnect__PC_Account__c != null
           && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_Caregiver__c  = PA.PatientConnect__PC_Account__c;
        } else if (PA.PatientConnect__PC_Role__c == designatedCaregiverRole && PA.PatientConnect__PC_Account__c != null
                   && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.ASSOCIATION_STATUS_INACTIVE) {
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_Caregiver__c  = null;
        }
        If(PA.PatientConnect__PC_Role__c == hcoRole && PA.PatientConnect__PC_Account__c != null
           && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {

          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_soc_Address__c = PA.spc_Address__c;
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_Soc_Association__c = PA.Id;
        } else if (PA.PatientConnect__PC_Role__c == hcoRole && PA.PatientConnect__PC_Account__c != null
                   && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.ASSOCIATION_STATUS_INACTIVE) {
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_soc_Address__c = null;
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_Soc_Association__c = null;
        }
        If(PA.PatientConnect__PC_Role__c == phyrole && PA.PatientConnect__PC_Account__c != null
           && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {

          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_Physician_Address__c = PA.spc_Address__c;
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_Physician_Association__c = PA.Id;
        } else if (PA.PatientConnect__PC_Role__c == phyrole && PA.PatientConnect__PC_Account__c != null
                   && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.ASSOCIATION_STATUS_INACTIVE) {
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_Physician_Address__c = null;
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_Physician_Association__c = null;
        }
        If(PA.PatientConnect__PC_Role__c == referringPhyrole && PA.PatientConnect__PC_Account__c != null
           && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {

          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_Referring_Physician_Address__c = PA.spc_Address__c;
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_Referral_Physician_Association__c = PA.Id;
        } else if (PA.PatientConnect__PC_Role__c == referringPhyrole && PA.PatientConnect__PC_Account__c != null
                   && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.ASSOCIATION_STATUS_INACTIVE) {
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_Referring_Physician_Address__c = null;
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_Referral_Physician_Association__c = null;
        }
        If(PA.PatientConnect__PC_Role__c == fdvrole && PA.PatientConnect__PC_Account__c != null
           && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE)) {

          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_FDV_Association__c  = PA.id;
        } else if (PA.PatientConnect__PC_Role__c == fdvrole && PA.PatientConnect__PC_Account__c != null
                   && PA.PatientConnect__PC_AssociationStatus__c == spc_ApexConstants.ASSOCIATION_STATUS_INACTIVE) {
          CaseTDMap.get(PA.PatientConnect__PC_Program__c).spc_FDV_Association__c  = null;

        }
      }
    }

  }

  /********************************************************************************************************
  *  @author          Deloitte
  *  @date            31/07/2018
  *  @description   Related Interactions Method
  * @Param         List<ID> CaseIDs
  * @Return        void
  *  @version         1.0
  *********************************************************************************************************/
  public void relatedInteraction(List<ID> CaseIDs) {
    //Populate Interaction in Template data object
    For(PatientConnect__PC_Interaction__c Inter : [SELECT Id, PatientConnect__PC_Patient_Program__c FROM  PatientConnect__PC_Interaction__c WHERE PatientConnect__PC_Patient_Program__c IN :CaseIDs  ORDER BY CreatedDate DESC Limit 1]) {
      //TD.spc_Interaction__c = Inter.Id;
      CaseTDMap.get(Inter.PatientConnect__PC_Patient_Program__c).spc_Interaction__c  = Inter.Id;
    }
  }

  /********************************************************************************************************
    *  @author          Deloitte
    *  @date            31/07/2018
    *  @description   RelatedCases
    * @Param         List<ID> CaseIDs
    * @Return        void
    *  @version         1.0
    *********************************************************************************************************/
  public void relatedCases(List<ID> caseIDs) {

    //Case Status Close
    String closeCase = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_STATUS_CLOSE);
    Map<ID, ID> AccountToCaseIds = new Map<Id, Id>();
    Id biRecordType_Id = spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.BI_RECORD_TYPE_NAME, Case.SobjectType);
    string bIBVTypeMedical = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_BITYPE_MEDICAL);
    BIBVTypes.add(bIBVTypeMedical);

    string bIBVTypePharmacy = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_BITYPE_PHARMACY);
    BIBVTypes.add(bIBVTypePharmacy);

    for (Case ca : [Select ID, Status, AccountId, PatientConnect__PC_Program_Coverage__c, PatientConnect__PC_Program_Coverage__r.PatientConnect__PC_Health_Plan_Type__c,
                    PatientConnect__PC_Program_Coverage__r.PatientConnect__PC_Health_Plan_Status__c, PatientConnect__PC_Is_Program_Case__c, RecordTypeId,
                    PatientConnect__PC_Program__c, spc_BIBV_type__c From Case Where (ID IN : caseIDs OR PatientConnect__PC_Program__c IN : caseIDs)]) {
      //If program Case add a map for account and program
      if (ca.PatientConnect__PC_Is_Program_Case__c) {
        AccountToCaseIds.put(ca.AccountId, ca.ID);
      }

      if ((ca.Status == closeCase && String.isNotBlank(ca.PatientConnect__PC_Program_Coverage__c) && HealthTypes.contains(ca.PatientConnect__PC_Program_Coverage__r.PatientConnect__PC_Health_Plan_Type__c))
          || (BIBVTypes.contains(ca.spc_BIBV_type__c) && ca.RecordTypeId == biRecordType_Id)) {

        //Primary Case
        If(ca.PatientConnect__PC_Program_Coverage__r.PatientConnect__PC_Health_Plan_Type__c == Primary) {
          CaseTDMap.get(ca.PatientConnect__PC_Program__c).spc_PA_Case_Primary__c  = ca.Id;
        }
        //Secondary Case
        If(ca.PatientConnect__PC_Program_Coverage__r.PatientConnect__PC_Health_Plan_Type__c == Secondary) {
          CaseTDMap.get(ca.PatientConnect__PC_Program__c).spc_PA_Case_Secondary__c  = ca.Id;
        }
        //Tertiary Case
        If(ca.PatientConnect__PC_Program_Coverage__r.PatientConnect__PC_Health_Plan_Type__c == Tertiary) {
          CaseTDMap.get(ca.PatientConnect__PC_Program__c).spc_PA_Case_Tertiary__c  = ca.Id;
        }

        //Related BIBV Cases

        If(ca.spc_BIBV_type__c != null) {
          //Medical BIBV Case
          If(ca.spc_BIBV_type__c == bIBVTypeMedical ) {
            CaseTDMap.get(ca.PatientConnect__PC_Program__c).Medical_BIBV_Case__c  = ca.Id;
          }

          //Pharmacy BIBV Case
          If(ca.spc_BIBV_type__c == bIBVTypePharmacy ) {
            CaseTDMap.get(ca.PatientConnect__PC_Program__c).Pharmacy_BIBV_Case__c  = ca.Id;
          }
        }
      }
    }
        For(PatientConnect__PC_Health_Plan__c heathPlan :[ SELECT ID,PatientConnect__PC_Plan_Type__c,PatientConnect__Patient__c,PatientConnect__PC_Plan_Status__c FROM PatientConnect__PC_Health_Plan__c 
		WHERE PatientConnect__Patient__c IN : AccountToCaseIds.keySet() AND PatientConnect__PC_Plan_Type__c IN : HealthTypes]){
			 If(heathPlan.PatientConnect__PC_Plan_Type__c == spc_ApexConstants.getHelthPlanType(spc_ApexConstants.HealthPlanType.HEALTH_PLANTYPE_PRIMARY) && AccountToCaseIds.get(heathPlan.PatientConnect__Patient__c) != NULL
                                                                && heathPlan.PatientConnect__PC_Plan_Status__c == spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE)
                                                             {
                                                               
                                                                 CaseTDMap.get(AccountToCaseIds.get(heathPlan.PatientConnect__Patient__c)).spc_Primary_Health_Plan__c = heathPlan.Id; 
                                                             } else if(heathPlan.PatientConnect__PC_Plan_Type__c == spc_ApexConstants.getHelthPlanType(spc_ApexConstants.HealthPlanType.HEALTH_PLANTYPE_PRIMARY) && (AccountToCaseIds.get(heathPlan.PatientConnect__Patient__c) != NULL
                                                                || heathPlan.PatientConnect__PC_Plan_Status__c != spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE)){
                                                                
                                                                 CaseTDMap.get(AccountToCaseIds.get(heathPlan.PatientConnect__Patient__c)).spc_Primary_Health_Plan__c = null; 
                                                             }
                                                             If(heathPlan.PatientConnect__PC_Plan_Type__c == spc_ApexConstants.getHelthPlanType(spc_ApexConstants.HealthPlanType.HEALTH_PLANTYPE_SECONDARY) && AccountToCaseIds.get(heathPlan.PatientConnect__Patient__c) != NULL
                                                                && heathPlan.PatientConnect__PC_Plan_Status__c == spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE)
                                                             {
                                                                 CaseTDMap.get(AccountToCaseIds.get(heathPlan.PatientConnect__Patient__c)).spc_Secondary_Health_Plan__c = heathPlan.Id;
                                                             } else if(heathPlan.PatientConnect__PC_Plan_Type__c == spc_ApexConstants.getHelthPlanType(spc_ApexConstants.HealthPlanType.HEALTH_PLANTYPE_SECONDARY) && (AccountToCaseIds.get(heathPlan.PatientConnect__Patient__c) != NULL
                                                                || heathPlan.PatientConnect__PC_Plan_Status__c == spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE)) {
                                                                 CaseTDMap.get(AccountToCaseIds.get(heathPlan.PatientConnect__Patient__c)).spc_Secondary_Health_Plan__c = null;
                                                             }
                                                             If(heathPlan.PatientConnect__PC_Plan_Type__c == spc_ApexConstants.getHelthPlanType(spc_ApexConstants.HealthPlanType.HEALTH_PLANTYPE_TERTIARY) && AccountToCaseIds.get(heathPlan.PatientConnect__Patient__c) != NULL
                                                                && heathPlan.PatientConnect__PC_Plan_Status__c == spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE) 
                                                             {
                                                                 CaseTDMap.get(AccountToCaseIds.get(heathPlan.PatientConnect__Patient__c)).spc_Tertiary_Health_Plan__c = heathPlan.Id;
                                                             } else if(heathPlan.PatientConnect__PC_Plan_Type__c == spc_ApexConstants.getHelthPlanType(spc_ApexConstants.HealthPlanType.HEALTH_PLANTYPE_TERTIARY) && (AccountToCaseIds.get(heathPlan.PatientConnect__Patient__c) != NULL
                                                                || heathPlan.PatientConnect__PC_Plan_Status__c == spc_ApexConstants.PROGRAM_COVERAGE_STATUS_ACTIVE)) {
                                                                 CaseTDMap.get(AccountToCaseIds.get(heathPlan.PatientConnect__Patient__c)).spc_Tertiary_Health_Plan__c = null;
                                                             }
		}
    }

  /********************************************************************************************************
    *  @author          Deloitte
    *  @date            31/07/2018
    *  @description   RelatedProgramCoverage
    * @Param         List<ID> CaseIDs
    * @Return        void
    *  @version         1.0
    *********************************************************************************************************/

  public void relatedProgramCoverage(List<ID> CaseIDs) {
    //Coverage status Active
    String activeStatus = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE);

    //Coverage outcome covered
    String covered = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.COVERAGE_OUTCOME_COVERED);

    //Coverage Type Copay
    String PAP = spc_ApexConstants.getCoverageType(spc_ApexConstants.CoverageType.PROGRAM_COVERAGE_TYPE_PAP);

    //Coverage Type Drug-Copay
    String DrugCopay = spc_ApexConstants.getCoverageType(spc_ApexConstants.CoverageType.PROGRAM_COVERAGE_TYPE_DRUGCOPAY);

    //Coverage Type Admin-Copay
    String AdminCopay = spc_ApexConstants.getCoverageType(spc_ApexConstants.CoverageType.PROGRAM_COVERAGE_ADMIN_COPAY);
    String PhramacyCoverage = spc_ApexConstants.getCoverageType(spc_ApexConstants.CoverageType.PHARMACY_PLAN_COVERAGE);
    String PrivateCoverage = spc_ApexConstants.getCoverageType(spc_ApexConstants.CoverageType.PUBLIC_COVERAGE);
    String PublicCoverage = spc_ApexConstants.getCoverageType(spc_ApexConstants.CoverageType.PRIVATE_COVERAGE);
    List<String> CovType = new List<String> {PAP, DrugCopay, AdminCopay, PhramacyCoverage, PrivateCoverage, PublicCoverage};

    For(PatientConnect__PC_Program_Coverage__c PC :  [Select id, PatientConnect__PC_Program__c, PatientConnect__PC_Coverage_Type__c, PatientConnect__PC_Program_Coverage__c, PatientConnect__PC_Health_Plan_Type__c
        From PatientConnect__PC_Program_Coverage__c where PatientConnect__PC_Program__c IN : CaseIDs AND
        (PatientConnect__PC_Coverage_Type__c IN : CovType )
                                                     ]) {
      //PAP Coverage
      if (PC.PatientConnect__PC_Coverage_Type__c == PAP) {
        CaseTDMap.get(PC.PatientConnect__PC_Program__c).spc_PAP_Coverage__c  = PC.Id;
      }

      //Drug Copay Coverage
      if (PC.PatientConnect__PC_Coverage_Type__c == DrugCopay) {
        CaseTDMap.get(PC.PatientConnect__PC_Program__c).spc_Drug_Copay_Coverage__c  = PC.Id;
      }

      //Admin Copay Coverage
      if (PC.PatientConnect__PC_Coverage_Type__c == AdminCopay) {
        CaseTDMap.get(PC.PatientConnect__PC_Program__c).spc_Admin_Copay_Coverage__c  = PC.Id;
      }

      //Primary Coverage
      if (PC.PatientConnect__PC_Health_Plan_Type__c == Primary
          && (PC.PatientConnect__PC_Coverage_Type__c == PrivateCoverage ||
              PC.PatientConnect__PC_Coverage_Type__c == PublicCoverage)) {
        CaseTDMap.get(PC.PatientConnect__PC_Program__c).spc_Primary_Coverage__c  = PC.Id;
      }

      //Secondary Coverage
      if (PC.PatientConnect__PC_Health_Plan_Type__c == Secondary
          && (PC.PatientConnect__PC_Coverage_Type__c == PrivateCoverage ||
              PC.PatientConnect__PC_Coverage_Type__c == PublicCoverage)) {
        CaseTDMap.get(PC.PatientConnect__PC_Program__c).spc_Secondary_Coverage__c  = PC.Id;
      }

      //Tertiary Coverage
      if (PC.PatientConnect__PC_Health_Plan_Type__c == Tertiary
          && (PC.PatientConnect__PC_Coverage_Type__c == PrivateCoverage ||
              PC.PatientConnect__PC_Coverage_Type__c == PublicCoverage)) {
        CaseTDMap.get(PC.PatientConnect__PC_Program__c).spc_Tertiary_Coverage__c  = PC.Id;
      }
      //primary coverage pharmacy
      if (PC.PatientConnect__PC_Health_Plan_Type__c == Primary
          && PC.PatientConnect__PC_Coverage_Type__c == PhramacyCoverage) {
        CaseTDMap.get(PC.PatientConnect__PC_Program__c).spc_Primary_Pharmacy_Coverage__c  = PC.Id;
      }

      //Secondary Coverage pharmacy
      if (PC.PatientConnect__PC_Health_Plan_Type__c == Secondary
          && PC.PatientConnect__PC_Coverage_Type__c == PhramacyCoverage) {
        CaseTDMap.get(PC.PatientConnect__PC_Program__c).spc_Secondary_Pharmacy_Coverage__c  = PC.Id;
      }

      //Tertiary Coverage pharmacy
      if (PC.PatientConnect__PC_Health_Plan_Type__c == Tertiary
          && PC.PatientConnect__PC_Coverage_Type__c == PhramacyCoverage) {
        CaseTDMap.get(PC.PatientConnect__PC_Program__c).spc_Tertiary_Pharmacy_Coverage__c  = PC.Id;
      }
    }
  }

  /********************************************************************************************************
    *  @author          Deloitte
    *  @date            31/07/2018
    *  @description   DMLOperation
    * @Return        void
    *  @version         1.0
    *********************************************************************************************************/

  public void dMLOperation() {
    //Upsert Template Data
    If(CaseTDMap.size() != 0) {Database.upsert(CaseTDMap.values());}

    For(Case Ca : CaselstUpdate) {
      Ca.spc_Template_Data__c = CaseTDMap.get(Ca.Id).Id;
      Ca.spc_Bypass_validation__c = true;
    }
    spc_TriggerHandler.bypass('spc_CaseTriggerHandler');
    //Update Case
    If(!CaselstUpdate.isEmpty())  {Database.update(CaselstUpdate);}
    spc_TriggerHandler.clearBypass('spc_CaseTriggerHandler');
  }
}
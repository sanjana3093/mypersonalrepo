/*********************************************************************************************************
class Name      : PSP_SignatureHandler 
Description		: Signature Handler Class
@author     	: Shourya Solipuram 
@date       	: July 10, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Shourya Solipuram           July 10, 2019          Initial Version
****************************************************************************************************************/ 
public with sharing class PSP_SignatureHandler {
  
    /* string for object name Account */
    final static string ACCOUNT_OBJ_NAME = PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.OBJ_NAME_ACCOUNT);
    /* string for object name Conatct */
    final static string CONTACT_OBJ_NAME = PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.OBJ_NAME_CONTACT);
     /* string for object name Program Enrollee */
    final static string PRG_ENRLLEE_OBJ = PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.OBJ_NAME_CPE);
      /*private constructor */
    @TestVisible 
    private PSP_SignatureHandler () {
        //Private constructor 
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     saveSign
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    @AuraEnabled 
    public static void saveSign(String base64Data, String contentType,Id authFormconsentId,Id objId) { 
        try {
            //Id parentId;
            
            final List<AuthorizationFormConsent> athformCnstLst=new  List<AuthorizationFormConsent>();
            for( AuthorizationFormConsent authformConsent:  [Select Id,name,Status,DocumentVersionId from AuthorizationFormConsent where Id=:authFormconsentId]) {
                authformConsent.Status=PSP_ApexConstants.getValue(PSP_ApexConstants.PicklistValue.PICKLIST_VAL_SIGNED);
                athformCnstLst.add(authformConsent);
            }
            /*if(objId.getSObjectType().getDescribe().getName().equals(ACCOUNT_OBJ_NAME)){
                   if (Contact.sObjectType.getDescribe().isAccessible()) {
                  List<Contact> con=[Select id from Contact where AccountId=:objId limit 1];
                        parentId=con[0].Id;
                   }
               
            }else if(objId.getSObjectType().getDescribe().getName().equals(CONTACT_OBJ_NAME)
                    || objId.getSObjectType().getDescribe().getName().equals(PRG_ENRLLEE_OBJ)){
                parentId=objId;
            }*/
            Attachment objAttachment = new Attachment();
            objAttachment.Name = 'Demo-Signature.png';
            objAttachment.ParentId = objId;
            objAttachment.isPrivate=true;
            objAttachment.ContentType = contentType;
            objAttachment.Body = EncodingUtil.base64Decode(base64Data);        
            Database.insert(objAttachment);
            system.debug('hello'+objAttachment);
            if(!athformCnstLst.isEmpty()) {
                Database.update(athformCnstLst);
            }
        } catch(AuraHandledException ex) {
            System.debug(ex.getMessage());
            throw  ex;
        }
       
    }
    /********************************************************************************************************
    *  @author          Deloitte
    *  @description     saveAuthRecord
    *  @date            July 12, 2019
    *  @version         1.0
    *********************************************************************************************************/
    @AuraEnabled 
    public static  AuthorizationFormConsent saveAuthRecord(String authFormConsent,Id contentDocId) { 
        try {
            final  Map<String, Object> authFormMap = (Map<String, Object>) json.deserializeUntyped(authFormConsent);
            List <contentversion > contenVersion=new List<contentversion>();
            if (contentversion.sObjectType.getDescribe().isAccessible()) {
              contenVersion=[select id,title from contentversion where contentdocumentid=:contentDocId limit 1];        
            }            
            final AuthorizationFormConsent athFrmCnsntRec=new AuthorizationFormConsent();
            athFrmCnsntRec.AuthorizationFormTextId=(String) (authFormMap.get('authformTextId'));
            athFrmCnsntRec.ConsentCapturedDateTime= Date.valueOf(String.valueOf(authFormMap.get('consentCapturedDateTime')));
            athFrmCnsntRec.ConsentCapturedSource=(String)(authFormMap.get('consentCapturedSource'));
            athFrmCnsntRec.ConsentCapturedSourceType=(String) (authFormMap.get('consentCapturedSourceType'));
            athFrmCnsntRec.ConsentGiverId=(String) (authFormMap.get('consentGiverId'));
            athFrmCnsntRec.Contact_Methods__c=(String) (authFormMap.get('contactMethods'));
            athFrmCnsntRec.Name=(String) (authFormMap.get('name'));
            athFrmCnsntRec.PSP_Consent_Type__c=(String) (authFormMap.get('consentType'));
            athFrmCnsntRec.PSP_Effective_From__c=Date.valueOf(String.valueOf(authFormMap.get('effectiveFrom')));
            if(!String.isBlank(String.valueOf(authFormMap.get('offset')))) {
                athFrmCnsntRec.PSP_Effective_To__c=athFrmCnsntRec.PSP_Effective_From__c.addYears((integer)authFormMap.get('offset'));
            }        
            athFrmCnsntRec.PSP_Individual__c=(String) (authFormMap.get('indiVidual'));
            athFrmCnsntRec.Status=(String) (authFormMap.get('status'));
            athFrmCnsntRec.DocumentVersionId=contenVersion[0].Id;
            Database.insert(athFrmCnsntRec);
            return athFrmCnsntRec;
        } catch(AuraHandledException ex) {
            System.debug(ex.getMessage());
            throw  ex;
        }
    }
    
}
/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the Trigger Handler class for Account Trigger
*  @date            06/21/2018
*  @version         1.0
*********************************************************************************************************/
public class spc_AccountTriggerHandler extends spc_TriggerHandler {
    spc_AccountTriggerImplementation triggerImplementation = new spc_AccountTriggerImplementation();
    private static spc_CommunicationTriggers CommtriggerImplementation = new spc_CommunicationTriggers();


    public override void beforeInsert() {
        List<Account> nonManufacturerAccount = new List<Account>();
        for (Account newAccountRec : (List<Account>)Trigger.new) {
            //Phone field being updated for Highlight panel
            if (String.isNotBlank(newAccountRec.spc_Mobile_Phone__c)) {
                newAccountRec.Phone = newAccountRec.spc_Mobile_Phone__c;
            }
            //Inserting the formatted DOB (MM/DD/YYYY) for a Patient
            if (newAccountRec.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.Account_Patient, Account.SobjectType) ) {
                if (newAccountRec.PatientConnect__PC_Date_of_Birth__c != NULL) {
                    newAccountRec.spc_DateOfBirth__c = newAccountRec.PatientConnect__PC_Date_of_Birth__c.format();
                }
            }
            Map<Id, Schema.RecordTypeInfo> rtMap = Account.sobjectType.getDescribe().getRecordTypeInfosById();
            if (spc_ApexConstants.ACCOUNT_PHARMACY_DEVELOPER_NAME.equalsIgnoreCase(rtMap.get(newAccountRec.RecordTypeId).getDeveloperName())) {
                newAccountRec.spc_Is_PC_Pharmacy__c = true;
            }
            if ((newAccountRec.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.Account_Patient, Account.SobjectType)
                    || newAccountRec.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.ACCOUNT_RT_CAREGIVER, Account.SobjectType) )
                    && String.isBlank(newAccountRec.ParentId)) {
                nonManufacturerAccount.add(newAccountRec);
            }
            //Make the Date Permission to Share PHI blank if the Permission from Patient to Share PHI field value is 'No'
            if (newAccountRec.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.ACCOUNT_RT_CAREGIVER, Account.SobjectType) ) {
                if (String.isNotBlank(newAccountRec.spc_Permission_from_Patient_to_Share_PHI__c) ) {
                    if (spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO).equalsIgnoreCase(newAccountRec.spc_Permission_from_Patient_to_Share_PHI__c)) {
                        newAccountRec.spc_Date_Permission_to_Share_PHI__c = NULL;
                    }

                }
            }
        }
        //Populate Parent Account Id as Sage Therapeutics for a Patient/Caregiver if its ParentId is blank
        if (!nonManufacturerAccount.isEmpty()) {
            triggerImplementation.setParentAccount(nonManufacturerAccount);
        }
    }

    public override void beforeUpdate() {
        for (Account newAccountRec : (List<Account>)Trigger.new) {
            Account oldAccount =  (Account)Trigger.oldMap.get(newAccountRec.Id);
            //Phone field being updated for Highlight panel
            if (newAccountRec.spc_Mobile_Phone__c != oldAccount.spc_Mobile_Phone__c) {
                newAccountRec.Phone = newAccountRec.spc_Mobile_Phone__c;
            }
            //Updating the formatted DOB (MM/DD/YYYY) for a Patient
            if (newAccountRec.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.Account_Patient, Account.SobjectType) ) {
                if (newAccountRec.PatientConnect__PC_Date_of_Birth__c != NULL) {
                    newAccountRec.spc_DateOfBirth__c = newAccountRec.PatientConnect__PC_Date_of_Birth__c.format();
                }
            }
            //Update the Date Permission to Share PHI blank if the Permission from Patient to Share PHI field value is 'No'
            if (newAccountRec.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.ACCOUNT_RT_CAREGIVER, Account.SobjectType) ) {
                if (newAccountRec.spc_Permission_from_Patient_to_Share_PHI__c != oldAccount.spc_Permission_from_Patient_to_Share_PHI__c ) {
                    if (spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO).equalsIgnoreCase(newAccountRec.spc_Permission_from_Patient_to_Share_PHI__c)) {
                        newAccountRec.spc_Date_Permission_to_Share_PHI__c = NULL;
                    }
                }
            }
        }
    }


    public override void afterUpdate() {
        if (!spc_ApexConstants.IsEWPRunning) {
            Set<Id> accIdsOfConsent = new Set<Id>();
            Set<Id> prgIds = new Set<Id>();
            List<Id> followupTasksHCPLogis = new List<Id>();
            Set<String> veevaRecordIDs = new Set<String>();
            List<Id> followupTasksHCPLogisForPatient = new List<Id>();
            Set<String> pcRecordIDs = new Set<String>();
            List<Id> accountsWithNoConsent = new List<Id>();
            Map<Id, Account> caregiverAccIdsMap = new Map<Id, Account>();
            CommtriggerImplementation.afterUpdateAccount((List<Account>)Trigger.new);
            Set<Id> designatedCaregiverAccIds = new Set<Id>();
            ID pcRecordTypeID =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Physician').getRecordTypeId();
            ID veevaRecordTypeID =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCP').getRecordTypeId();
            Map<Id, String> accountIdAndNameMap = new Map<Id, String>();

            Set<Id> setNotCertifiedAccountIds = new Set<Id>();
            for (Account acc : (List<Account>)Trigger.new) {
                if (String.valueOf(acc.RecordTypeId) == pcRecordTypeID )
                    veevaRecordIDs.add(acc.PatientConnect__PC_External_ID__c);
                else if (String.valueOf(acc.RecordTypeId) == veevaRecordTypeID) {
                    pcRecordIDs.add(acc.External_ID_vod__c);
                }
            }


            for (Account newAcc : (List<Account>)Trigger.New) {
                Account oldAcc = (Account) Trigger.oldMap.get(newAcc.Id);
                if (newAcc.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.Account_Patient, Account.SobjectType) ) {
                    //US-177009:Validate if Consent fields are modifield(spc_HIPAA_Consent_Received__c,spc_Text_Consent__c,
                    //                                          spc_Patient_Mrkt_and_Srvc_consent__c,spc_Patient_Services_Consent_Received__c)

                    If(newAcc.spc_HIPAA_Consent_Received__c != oldAcc.spc_HIPAA_Consent_Received__c
                       || newAcc.spc_Text_Consent__c != oldAcc.spc_Text_Consent__c
                       || newAcc.spc_Patient_Mrkt_and_Srvc_consent__c != oldAcc.spc_Patient_Mrkt_and_Srvc_consent__c
                       || newAcc.spc_Services_Text_Consent__c != oldAcc.spc_Services_Text_Consent__c) {
                        accIdsOfConsent.add(newAcc.Id);
                    }
                    //Update the Account Name in Case Subject
                    if (newAcc.name != oldAcc.name) {
                        accountIdAndNameMap.put(newAcc.id, newAcc.Name);
                    }
                    if (newAcc.spc_HIPAA_Consent_Received__c != oldAcc.spc_HIPAA_Consent_Received__c) {
                        if (newAcc.spc_HIPAA_Consent_Received__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES)) {
                            prgIds.add(newAcc.Id);
                        }
                        if (newAcc.spc_HIPAA_Consent_Received__c == null || newAcc.spc_HIPAA_Consent_Received__c == ''
                                || newAcc.spc_HIPAA_Consent_Received__c == spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO)) {
                            followupTasksHCPLogisForPatient.add(newAcc.Id);
                        }
                    }
                }

                if (newAcc.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.ACCOUNT_RT_HCO, Account.SobjectType)
                        && newAcc.spc_REMS_Certification_Status__c != oldAcc.spc_REMS_Certification_Status__c
                        && newAcc.spc_REMS_Certification_Status__c == spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ACCOUNT_REMS_CERTIFIED_STATUS)) {
                    followupTasksHCPLogis.add(newAcc.Id);
                }

                if (newAcc.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.ACCOUNT_RT_CAREGIVER, Account.SobjectType)
                        && newAcc.spc_Permission_from_Patient_to_Share_PHI__c != oldAcc.spc_Permission_from_Patient_to_Share_PHI__c ) {
                    //If Permission to PHI is Yes, update Caregiver Association to Designated caregiver
                    if (spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_YES).equalsIgnoreCase(newAcc.spc_Permission_from_Patient_to_Share_PHI__c)) {
                        caregiverAccIdsMap.put(newAcc.Id, newAcc);
                    }
                    //If Permission to PHI is no, then update designated caregiver to caregiver association
                    else if (spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_HIPAA_CONSENT_RECEIVED_NO).equalsIgnoreCase(newAcc.spc_Permission_from_Patient_to_Share_PHI__c)) {
                        designatedCaregiverAccIds.add(newAcc.Id);
                    }
                }

                if (newAcc.spc_HIPAA_Consent_Received__c != oldAcc.spc_HIPAA_Consent_Received__c) {
                    accountsWithNoConsent.add(newAcc.Id);
                }

                //Added for US-307175
                if (newAcc.RecordTypeId == spc_ApexConstants.getRecordTypeId(spc_ApexConstants.RecordTypeName.ACCOUNT_RT_HCO, Account.SobjectType)
                        && newAcc.spc_REMS_Certification_Status__c == spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ACCOUNT_REMS_NOT_CERTIFIED_STATUS)
                        && oldAcc.spc_REMS_Certification_Status__c == spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ACCOUNT_REMS_CERTIFIED_STATUS)) {
                    setNotCertifiedAccountIds.add(newAcc.Id);
                }
            }
            if (! accIdsOfConsent.isEmpty()) {
                //Update Status Indicator for related program cases
                triggerImplementation.setProgramCaseIndicatorStatus(accIdsOfConsent);
            }
            List<PatientConnect__PC_Association__c> lstCaregiverAssoc = new List<PatientConnect__PC_Association__c>();
            if (!caregiverAccIdsMap.isEmpty()) {
                lstCaregiverAssoc = triggerImplementation.updateCaregiverAssociation(caregiverAccIdsMap);
            }

            if (!followupTasksHCPLogis.isEmpty()) {
                triggerImplementation.createHCPLogisticTasks(followupTasksHCPLogis, false);
            }
            if (!followupTasksHCPLogisForPatient.isEmpty()) {
                triggerImplementation.createHCPLogisticTasks(followupTasksHCPLogisForPatient, true);
            }
            if (!designatedCaregiverAccIds.isEmpty()) {
                List<PatientConnect__PC_Association__c> lstDesigCaregiverAssoc = triggerImplementation.updateDesignatedCaregiverAssociation(designatedCaregiverAccIds);
                if (lstDesigCaregiverAssoc != null && !lstDesigCaregiverAssoc.isEmpty())
                    lstCaregiverAssoc.addAll(lstDesigCaregiverAssoc);
            }
            if (!lstCaregiverAssoc.isEmpty()) {
                spc_Database.ups(lstCaregiverAssoc, new List<sObjectField>(), true, true);
            }
            if (!accountIdAndNameMap.isEmpty()) {
                triggerImplementation.updateAccountNameInCase(accountIdAndNameMap);
            }

            if (!setNotCertifiedAccountIds.isEmpty()) {
                triggerImplementation.createPatientAuthorizationTasks(setNotCertifiedAccountIds);
            }
            if (!prgIds.isEmpty()) {

                triggerImplementation.setBIBVStatusIndicator(prgIds);
            }
        }

    }

}
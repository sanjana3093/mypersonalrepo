/********************************************************************************************************
*  @author          Deloitte
*  @description     This is the trigger Implementation class for Case Trigger 
*  @date            09/18/2019
*  @version         1.0
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Sanjana Tripathy            September 18, 2019     Initial Version
****************************************************************************************************************/
public class PSP_CaseTriggerImpl {
 	/**************************************************************************************
    * @author      : Deloitte
    * @date        : 09/17/2019
    * @Description : updates record type
    * @Param       :Case List
    * @Return      : Void
    ***************************************************************************************/
    public void updateRecordtype(List<Case> cslst) {
        for(Case cs:cslst) {
            /*record type update*/
            cs.RecordtypeId = PSP_ApexConstants.getRTId(PSP_ApexConstants.RecordTypeName.CASE_RT_ADVERSE_EVENTS, Case.getSObjectType());
        }
    }
}
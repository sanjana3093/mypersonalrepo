/*********************************************************************************************************
class Name      : PSP_TriggerHandler 
Description		: Trigger Handler Class
@author		    : Saurabh Tripathi
@date       	: July 10, 2019
Modification Log: 
-------------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------            
Saurabh Tripathi            July 10, 2019          Initial Version
****************************************************************************************************************/ 
public virtual with sharing class PSP_trigger_Handler extends PSP_TriggerHandler {
	/* bypassedHandlers set */
	private static Set<String> bypassedHandlers;
    /* loopCount map */
  	private static Map<String, LoopCount> loopCountMap;
    /* the current context of the trigger, overridable in tests */
    @TestVisible
    private TriggerContext context;
	/* is trigger executing flag */
    @TestVisible
    private boolean isTrigExcutng;
    /* flag to check if trigger needs to be enabled */
    @TestVisible
    private static boolean isEnabled = true;
    
    
   /* static initialization */
    static {
        loopCountMap = new Map<String, LoopCount> ();
        bypassedHandlers = new Set<String> ();
    }
    /********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     constructor
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
    public PSP_trigger_Handler () {
       super();
       this.setTriggerContext ();
    }
    
    /***************************************
   * public instance methods
   ***************************************/
    
    
     // main method that will be called during execution
    /**************************************************************************************	
      * @author      : Deloitte
      * @date        : 07/10/2019
      * @Description : Main method
      * @Param       : Null
      * @Return      : Void
      ***************************************************************************************/
    public void run () {
        
        if (!validateRun()) {
            return;
        }
        addToLoopCount ();
        checkTriggerBypass();
        // dispatch to the correct handler method
        if(isEnabled) {
            if (this.context == TriggerContext.BEFORE_INSERT) {
                this.beforeInsert ();
            } else if (this.context == TriggerContext.BEFORE_UPDATE) {
                this.beforeUpdate ();
            } else if (this.context == TriggerContext.AFTER_UPDATE) {
                this.afterUpdate ();
            } else if (this.context == TriggerContext.AFTER_INSERT) {
                this.afterInsert ();
            }
        }
        
    }
   /***************************************
   * Method to check and by pass triggers based on permissionsets
   ***************************************/
    private void checkTriggerBypass () {
        List<PSP_Bypass_Trigger__c> bypassPerSets = PSP_Bypass_Trigger__c.getall().values();
        if(PermissionSetAssignment.sObjectType.getDescribe().isAccessible()) {
            for(PermissionSetAssignment permissionSet : [SELECT PermissionSet.Label FROM PermissionSetAssignment WHERE AssigneeId = :Userinfo.getUserId()]) {
                for(PSP_Bypass_Trigger__c perm : bypassPerSets) {
                    if(perm.PSP_Disable__c && permissionSet.PermissionSet.Label == perm.Permission_Set__c) {
                        isEnabled = false;
                        break;
                    }
                }
            }
        }
    }

  /***************************************
   * public static methods
   ***************************************/
	/**************************************************************************************	
      * @author      : Deloitte
      * @date        : 07/10/2019
      * @Description : bypass method
      * @Param       : handlerName String
      * @Return      : Void
      ***************************************************************************************/
    public static void bypass (String handlerName) {
        PSP_trigger_Handler.bypassedHandlers.add (handlerName);
    }
	/**************************************************************************************	
      * @author      : Deloitte
      * @date        : 07/10/2019
      * @Description : clear bypass method
      * @Param       : handlerName String
      * @Return      : Void
      ***************************************************************************************/
    public static void clearBypass (String handlerName) {
        PSP_trigger_Handler.bypassedHandlers.remove (handlerName);
    }
	/********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     isBypassed method
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
    public static Boolean isBypassed (String handlerName) {
        //boolean isEnabled = true;
        if (PSP_TriggerHandlerFrameworkSettings__c.getInstance (handlerName) != null ) {
            isEnabled = ! (PSP_TriggerHandlerFrameworkSettings__c.getInstance (handlerName).PSP_Disable__c);
        }
        return PSP_trigger_Handler.bypassedHandlers.contains (handlerName) && isEnabled;
    }
	/********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     clear all bypasses
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
    public static void clearAllBypasses () {
        PSP_trigger_Handler.bypassedHandlers.clear ();
    }
    
    /********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     set max loop count method
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
    public void maxLoopCount (Integer max) {
        final String handlerName = hndlerName ();
        if (PSP_trigger_Handler.loopCountMap.containsKey (handlerName)) {
            PSP_trigger_Handler.loopCountMap.get (handlerName).max = max; 
        } else {
            PSP_trigger_Handler.loopCountMap.put (handlerName, new loopCount (max));
        }
    }
	/********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     clear max loop count method
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
    public void clearMaxLoopCount () {
        this.maxLoopCount (-1);
    }

  	// make sure this trigger should continue to run
    @TestVisible
    private Boolean validateRun() {
        Boolean bypassHandler = true;
        if (!this.isTrigExcutng || this.context == null) {
            throw new TriggerHandlerException('Trigger handler called outside of Trigger execution');
        }
        if (PSP_trigger_Handler.bypassedHandlers.contains(hndlerName())) {
            bypassHandler = false;
        }
        return bypassHandler;
    }

    @TestVisible
    private String hndlerName() {
        return String.valueOf(this).substring(0, String.valueOf(this).indexOf(':'));
    }

    @TestVisible
    private void setTriggerContext() {
        this.setTriggerContext(null, false);
    }
  
    
  @TestVisible
  private void setTriggerContext (String ctx, Boolean testMode) {
      if ( (Trigger.isExecuting && testMode) || (Trigger.isExecuting && !testMode) || (!Trigger.isExecuting && testMode)) {
           this.isTrigExcutng = true;
      } else {
          this.isTrigExcutng = false;
          return;
      }
      if ((Trigger.isExecuting && Trigger.isBefore && Trigger.isInsert) ||
          (ctx != null && ctx == 'before insert')) {
              this.context = TriggerContext.BEFORE_INSERT;
          } else if ((Trigger.isExecuting && Trigger.isBefore && Trigger.isUpdate) ||
                     (ctx != null && ctx == 'before update')) {
                         this.context = TriggerContext.BEFORE_UPDATE;
          } else if ((Trigger.isExecuting && Trigger.isAfter && Trigger.isInsert) ||
               		(ctx != null && ctx == 'after insert')) {
      				this.context = TriggerContext.AFTER_INSERT;
    	  } else if ((Trigger.isExecuting && Trigger.isAfter && Trigger.isUpdate) ||
                    (ctx != null && ctx == 'after update')) {
                    this.context = TriggerContext.AFTER_UPDATE;
          }
  }
    // increment the loop count
    @TestVisible
    private void addToLoopCount() {
        final String handlerName = hndlerName();
        if (PSP_trigger_Handler.loopCountMap.containsKey(handlerName)) {
            final Boolean exceeded = PSP_trigger_Handler.loopCountMap.get(handlerName).increment();
            if (exceeded) {
                final Integer max = PSP_trigger_Handler.loopCountMap.get(handlerName).max;
                throw new TriggerHandlerException('Maximum loop count of ' + max + ' reached in ' + handlerName);
            }
        }
    }
 	// inner class for managing the loop count per handler
  	/********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     Loop count inner class
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
      @TestVisible
      private class LoopCount {
          /* max variable */
          private Integer max;
          /* count variable */
          private Integer count;
    	/********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     Loop count constructor
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
          public loopCount () {
              this.max = 5;
              this.count = 0;
          }
    	/********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     Loop count constructor
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
          public loopCount (Integer max) {
              this.max = max;
              this.count = 0;
          }
    	/********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     increment method
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
          public Boolean increment () {
              this.count++;
              return this.exceeded ();
          }
    	/********************************************************************************************************
    	*  @author          Deloitte
    	*  @description     exceeded method
    	*  @date            July 12, 2019
    	*  @version         1.0
    	*********************************************************************************************************/
          public Boolean exceeded () {
              Boolean  exceeded;
              if (this.max < 0) {
                  exceeded = false;
              } 
              if (!exceeded && this.count > this.max) {
                  exceeded = true;
              }
              return exceeded;
          }
    	 
      }
    
    

}
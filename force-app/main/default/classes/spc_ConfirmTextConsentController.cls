/**
* @author Deloitte
* @date July 16, 2018
*
* @description This class is the controller of spc_ConfirmTextConsent lightning component
*/
public class spc_ConfirmTextConsentController {
    /*******************************************************************************************************
    * @description To confirm Text Consent by clicking button.
    * @param programCaseId the ID of the Program Case
    * @return ResponseWrapper
    */

    @AuraEnabled
    public static ResponseWrapper confirmTextConsent(String programCaseId) {
        ResponseWrapper confirmTextConsentResponse;
        Case patientProgramCaseRec;
        List<Case> programCaseList = new List<Case>();
        try {
            programCaseList = [Select id, spc_ConfirmTextConsent__c, AccountId FROM Case
                               WHERE id = : programCaseId LIMIT 1];
            if (!programCaseList.isEmpty()) {
                patientProgramCaseRec = programCaseList.get(0);
                if (null != patientProgramCaseRec.AccountId) {
                    List<Account> accList = [SELECT Id, spc_Services_Text_Consent__c, spc_Services_Text_Consent_Date__c
                                             FROM Account WHERE Id = : patientProgramCaseRec.AccountId LIMIT 1];
                    if (!accList.isEmpty()) {
                        Account acc = accList.get(0);
                        if (null == acc.spc_Services_Text_Consent__c
                                || !acc.spc_Services_Text_Consent__c.equalsIgnoreCase(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_VERBAL_CONSENT_RECEIVED))) {
                            acc.spc_Services_Text_Consent__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_VERBAL_CONSENT_RECEIVED);
                            acc.spc_Services_Text_Consent_Date__c = Date.today();
                            spc_Database.upd(acc);
                            confirmTextConsentResponse = new ResponseWrapper(true, Label.spc_Confirm_Verbal_Text_Consent_Message);
                        } else {
                            confirmTextConsentResponse = new ResponseWrapper(true, Label.spc_Verbal_Consent_already_received);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }
        return confirmTextConsentResponse;
    }
    /*******************************************************************************************************
    * @description Wrapper Class for Response
    *
    */
    public Class ResponseWrapper {
        @AuraEnabled
        public Boolean isSuccess;
        @AuraEnabled
        public String successOrErrorMessage;
        ResponseWrapper(Boolean isSuccess, String successOrErrorMessage) {
            this.isSuccess = isSuccess;
            this.successOrErrorMessage = successOrErrorMessage;
        }
    }
}
/*********************************************************************************************************
 * This is The PC class cloned by Sage for Reference only
************************************************************************************************************/
public class spc_eLetter_Recipient {
    // Used on Send eLetter page to show recipients and collect fax and email selections
    @AuraEnabled public Account recipient {get; set;}
    @AuraEnabled public String recipientType {get; set;}
    @AuraEnabled public String communicationLanguage {get; set;}
    @AuraEnabled public Boolean sendFax {get; set;}
    @AuraEnabled public Boolean sendEmail {get; set;}
    @AuraEnabled public Boolean sendToMe {get; set;}
    @AuraEnabled public Boolean bDisableEmail {get; set;}
    @AuraEnabled public Boolean bDisableFax {get; set;}
    @AuraEnabled public Boolean bDisableSendToMe {get; set;}
    @AuraEnabled public String recipientFax {get; set;}
    @AuraEnabled public String recipientEmail {get; set;}
    @AuraEnabled public String recipientName {get; set;}
    @AuraEnabled public Boolean sendSMS {get; set;}
    @AuraEnabled public Boolean hideEmailRow {get; set;}
    @AuraEnabled public Boolean hideFaxRow {get; set;}

    public spc_eLetter_Recipient(Account recipient, String recipientType, Set<String> availableLanguages, Set<String> Channels) {
        this.recipient = recipient;
        this.recipientFax = recipient.Fax;
        this.recipientEmail = recipient.PatientConnect__PC_Email__c;
        this.recipientName = recipient.Name;
        // Type of Participant (Physician or Pharmacy or...etc)
        this.recipientType = recipientType;

        // Communication Language is defaulted to recipent's communiction language or the language set at eLetter record level.
        this.communicationLanguage = availableLanguages.contains(recipient.PatientConnect__PC_Communication_Language__c) ? recipient.PatientConnect__PC_Communication_Language__c : '';

        // Fax will be selected by default (if a fax number exist for the participant)
        this.sendFax =  !String.isBlank(recipient.Fax) ? (Channels != null) ? Channels.contains(Label.spc_FAX_Template) ? true : false : false : false;           //!String.isBlank(recipient.Fax);
        // Email sending is uncheked by default (Enabled for Gilenya)
        this.sendEmail = (recipient.PatientConnect__PC_Email__c != null) ? (Channels != null) ? Channels.contains(Label.spc_Email_Template) ? true : false : false : false;

        this.hideFaxRow =   Channels.contains(Label.spc_FAX_Template) ? false : true;                   //!String.isBlank(recipient.Fax);
        this.hideEmailRow = Channels.contains(Label.spc_Email_Template) ? false : true;

        // sendToMe is false by default, Will be enabled if the eLetter is set as editable/Send to User is enabled for the eLetter
        this.sendToMe = false;
    }
}
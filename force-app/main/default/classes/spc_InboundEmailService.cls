/**
* @author Deloitte
* @date 06-June-18
*
* @description This is the class created to Create Document Records on Inbound Email and relate Document logs to recognized senders case
*/

global class spc_InboundEmailService implements Messaging.InboundEmailHandler {
    /*******************************************************************************************************
    * @description Creates document on recieving an email message
    * @return Email message
    */
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,
            Messaging.Inboundenvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        String caseNum = '';
        try {

            PatientConnect__PC_Document__c docs = new PatientConnect__PC_Document__c();
            List<PatientConnect__PC_Document__c> documentList = new List<PatientConnect__PC_Document__c>();
            docs.PatientConnect__PC_From_Email_Address__c = email.fromAddress;
            docs.spc_to_Email_address__c = email.toAddresses[0]; spc_Incoming_Document_Routing_Mappings__c incomingDocMappingRecLists = spc_Incoming_Document_Routing_Mappings__c.getValues('OrgWideEmail1');
            if (incomingDocMappingRecLists != null) {
                docs.spc_to_Email_address__c = incomingDocMappingRecLists.spc_Email_Fax_Id__c;
            } else {
                docs.spc_to_Email_address__c = email.toAddresses[0];
            }
            String recTypeId = Schema.SObjectType.PatientConnect__PC_Document__c.getRecordTypeInfosByName().get(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_RT_EMAIL_INBOUND)).getRecordTypeId();
            docs.recordtypeid = recTypeId;
            insert docs;
            //Fetch the case number from Email Subject
            Pattern p = Pattern.compile('(\\D*)(\\d{8})(\\D*)');
            Matcher pm = p.matcher(email.subject);
            if (pm.matches()) {
                caseNum = pm.group(2);
            }

            List<Attachment> attachmentList = new List<Attachment>();
            Attachment emailContent;
            if (String.isNotBlank(email.htmlBody)) {
                emailContent = new Attachment();
                emailContent.Name = email.subject + '.pdf';
                emailContent.Body = blob.toPdf(email.htmlBody.stripHtmlTags());
                emailContent.ParentId = docs.Id;
                emailContent.ContentType = spc_ApexConstants.Application_ContantType;
                attachmentList.add(emailContent);
            } else if (String.isNotBlank(email.plainTextBody)) {

                emailContent = new Attachment();
                emailContent.Name = email.subject + '.txt';
                emailContent.Body = blob.valueOf(email.plainTextBody);
                emailContent.ParentId = docs.Id;
                emailContent.ContentType = spc_ApexConstants.Doc_ContantType;
                attachmentList.add(emailContent);
            }
            if (email.textAttachments != null) {
                for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) {
                    Attachment attachment = new Attachment();
                    attachment.Name = tAttachment.fileName;
                    attachment.Body = Blob.valueOf(tAttachment.body);
                    attachment.ParentId = docs.Id;
                    attachment.ContentType = spc_ApexConstants.Doc_ContantType;
                    attachmentList.add(attachment);
                }
            } else if (email.binaryAttachments != null) {
                for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
                    Attachment attachment = new Attachment();
                    attachment.ContentType = bAttachment.mimeTypeSubType;
                    attachment.Name = bAttachment.fileName;
                    attachment.Body = bAttachment.body;
                    attachment.ParentId =  docs.Id;
                    attachmentList.add(attachment);
                }

            }
            if (!attachmentList.isEmpty()) {
                upsert attachmentList;
            }
            docs.PatientConnect__PC_Attachment_Id__c = emailContent.Id;
            update docs;
            List<Case> selectedCaseObjList = new List<Case>();
            Boolean isCaseFound = false;
            Case selectedCaseRec;
            List<Case> selectedProgCaseRecList = new List<Case>();
            List<Case> selectedInquiryCaseRecList = new List<Case>();

            //Fetch all the case records or the specific record to the associated email Id
            for (Case rec : [select id, RecordTypeId, RecordType.Name, CaseNumber, Status, PatientConnect__PC_Email__c, OwnerId, CreatedById from Case where PatientConnect__PC_Email__c = :email.fromAddress OR CaseNumber = :caseNum Order by CreatedDate desc]) {
                if (null != rec) {
                    if (String.isNotBlank(rec.CaseNumber) && rec.CaseNumber.equalsIgnoreCase(caseNum)) {
                        //assign the specific case record
                        selectedCaseRec = rec;
                        isCaseFound = true;
                        break;
                    } else {
                        //Fetch the program case having email id same as the one from which reply has been sent
                        if (String.isNotBlank(rec.PatientConnect__PC_Email__c) && rec.PatientConnect__PC_Email__c.equalsIgnoreCase(email.fromAddress) && rec.RecordType.Name.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM))) {
                            selectedProgCaseRecList.add(rec);
                            //Else, Fetch the Open Inquiry case having email id same as the one from which reply has been sent
                        } else if (String.isNotBlank(rec.PatientConnect__PC_Email__c) && rec.PatientConnect__PC_Email__c.equalsIgnoreCase(email.fromAddress) && rec.RecordType.Name.equalsIgnoreCase(spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_INQUIRY))
                                   && String.isNotBlank(rec.Status) && rec.Status.equalsIgnoreCase(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.CASE_STATUS_OPEN))) {
                            selectedInquiryCaseRecList.add(rec);
                        }
                    }

                }
            }
            if (isCaseFound) {
                selectedCaseObjList.add(selectedCaseRec);
            } else if (!selectedProgCaseRecList.isEmpty()) {
                selectedCaseObjList.add(selectedProgCaseRecList.get(0));
            } else if (!selectedInquiryCaseRecList.isEmpty()) {
                selectedCaseObjList.add(selectedInquiryCaseRecList.get(0));
            }
            //Create Document Log and Activity for the corresponding case
            if (null != selectedCaseObjList && !selectedCaseObjList.isEmpty()) {
                createDocLogAndTask(selectedCaseObjList, docs, email.subject);
            }
            //Create an orphan document log and activity if the email Id not present in system
            else {
                createOrphanDocLogAndTask(docs, email.subject);
            }
            result.success = true;

        } catch (Exception e) {
            result.success = false;
            spc_utility.logAndThrowException(e);
        }
        return result;
    }

    /*******************************************************************************************************
    * @description Create Document Log and Task on the case to which Inbound Email was sent
    * @param List<Case>
    * @param PatientConnect__PC_Document__c
    * @param String
    */
    public void createDocLogAndTask(List<Case> caseObj, PatientConnect__PC_Document__c docs, String subject) {
        Integer totalRecOunt = 0;
        List<PatientConnect__PC_Document_Log__c> docList = new List<PatientConnect__PC_Document_Log__c>();
        PatientConnect__PC_Document_Log__c documentLog = new PatientConnect__PC_Document_Log__c();
        //D-227156 Past Activity for Inbound Email under activity timeline
        Task newTask = new Task();
        for (Case newcase : caseobj) {
            if (newcase.RecordType.Name == spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM) || newcase.RecordType.Name == spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_INQUIRY)) {
                if (newcase.RecordType.Name == spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_PROGRAM)) {
                    documentLog.PatientConnect__PC_Program__c = newcase.id;
                    newTask.WhatId = newcase.id;
                }
                if (newcase.RecordType.Name == spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.CASE_RT_INQUIRY)) {
                    documentLog.spc_Inquiry__c = newCase.ID;
                    newTask.WhatId = newcase.id;
                }
                if (totalRecOunt == 0) {
                    totalRecOunt = totalRecOunt + 1;
                    documentLog.PatientConnect__PC_Document__c = docs.Id;
                    //D-227156 Past Activity for Inbound Email under activity timeline
                    newTask.Subject = Label.spc_New_Inbound_Document;
                    newTask.PatientConnect__PC_Channel__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CHANNEL_EMAIL);
                    newTask.ActivityDate = system.today();
                    newTask.PatientConnect__PC_Document__c = docs.Id;
                    if (newcase.OwnerId.getSObjectType() == User.sobjectType) {
                        newTask.OwnerId = newcase.OwnerId;
                        newTask.PatientConnect__PC_Assigned_To__c = newcase.OwnerId;
                    } else {
                        newTask.OwnerId = newcase.CreatedById;
                        newTask.PatientConnect__PC_Assigned_To__c = newcase.CreatedById;
                    }
                    newTask.PatientConnect__PC_Direction__c = 'Inbound';
                    newTask.type = 'Other';
                    newTask.status = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_NOT_STARTED);
                    newTask.description = subject;
                }
            }
        }
        if (totalRecOunt == 1) {
            docList.add(documentLog);
        }
        try {
            if (!docList.isEmpty()) {
                upsert docList;
                insert newTask;
            }
        } catch (Exception e) {
            spc_utility.logAndThrowException(e);
        }
    }

    /*******************************************************************************************************
    * @description Create Document Log and Task for the Inbound Email was sent
    * @param PatientConnect__PC_Document__c
    * @param String
    */

    public void createOrphanDocLogAndTask(PatientConnect__PC_Document__c docs, String subject) {
        PatientConnect__PC_Document_Log__c documentLog = new PatientConnect__PC_Document_Log__c();
        Task newTask = new Task();
        documentLog.PatientConnect__PC_Document__c = docs.Id;
        newTask.Subject = Label.spc_New_Inbound_Document;
        newTask.PatientConnect__PC_Channel__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_CHANNEL_EMAIL);
        newTask.ActivityDate = system.today();
        newTask.PatientConnect__PC_Document__c = docs.Id;
        newTask.PatientConnect__PC_Assigned_To__c = docs.OwnerId;
        newTask.PatientConnect__PC_Direction__c = 'Inbound';
        newTask.type = 'Other';
        newTask.status = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_NOT_STARTED);
        newTask.description = subject;
        try {
            if (documentLog != null) {
                insert documentLog;
            }
            if (null != newTask) {
                insert newTask;
            }
        } catch (Exception e) {
            spc_utility.logAndThrowException(e);
        }
    }

}
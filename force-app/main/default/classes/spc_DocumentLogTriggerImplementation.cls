/**
* @author Deloitte
* @date 10/22/2018
*
* @description This is the Implementation class for Document Log Trigger
*/

public class spc_DocumentLogTriggerImplementation {
    /*******************************************************************************************************
    * @description  Task generation when a document logs is created linking inbound document with a program
    * @param Map<String, Id>
    * @param Map<Id, Id>
    * @param Boolean
    * @return void
    */
    public void createReviewInboudDocumentTask(Map<Id, Id> caseIdsAndDocIds, Map<Id, Id> interactionsAndDocuments, Boolean manual) {
        List<Task> newTasks = new List<Task>();
        Map<Id, Id> inboundDocumentAndCases = new Map<Id, Id>();
        List<spc_TaskProcessManager.TaskWrapper> taskWrappers = new List<spc_TaskProcessManager.TaskWrapper>();
        // If Interactions are found, Program case is derived from Interaction and added to Case ids
        if (!interactionsAndDocuments.isEmpty()) {
            for (PatientConnect__PC_Interaction__c interaction : [Select Id, PatientConnect__PC_Patient_Program__c from PatientConnect__PC_Interaction__c where id in :interactionsAndDocuments.keySet()]) {
                if (String.isNotBlank(interaction.PatientConnect__PC_Patient_Program__c) ) {
                    caseIdsAndDocIds.put(interaction.PatientConnect__PC_Patient_Program__c, interactionsAndDocuments.get(interaction.id));
                }
            }
        }

        // Iterating through cases for task creation
        for (Case caseObj : [Select Id, PatientConnect__PC_Program__c, PatientConnect__PC_Program__r.OwnerId, OwnerId from Case where id in: caseIdsAndDocIds.keySet() ]) {
            spc_TaskProcessManager.TaskWrapper taskWrapper = new spc_TaskProcessManager.TaskWrapper();
            taskWrapper.Subject = Label.spc_Review_Inbound_Document;
            taskWrapper.category = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_CAT_DOCUMENT_REVIEW);
            taskWrapper.priority = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_PRIORITY_NORMAL);
            if ( String.isNotBlank(caseObj.PatientConnect__PC_Program__c) ) {
                taskWrapper.OwnerId = caseObj.PatientConnect__PC_Program__r.OwnerId;
                taskWrapper.relatedTo = caseObj.PatientConnect__PC_Program__c;
            } else {
                taskWrapper.OwnerId = caseObj.OwnerId;
                taskWrapper.relatedTo = caseObj.id;
            }
            taskWrapper.dueDate = System.today();
            taskWrapper.documentId =  caseIdsAndDocIds.get(caseObj.id);
            if (manual) {
                taskWrapper.Subject = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_SUB_REVIEW_MANUALLY_UPLOAD_DOC);
                taskWrapper.subCategory = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_SUB_CAT_MANUAL_UPLOAD);
                taskWrapper.status = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACTIVITY_STATUS_COMPLETED);
            } else {
                taskWrapper.status = spc_Apexconstants.STATUS_INDICATOR_NOTSTARTED;
                taskWrapper.Subject = Label.spc_Review_Inbound_Document;
                taskWrapper.subCategory = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_SUB_CAT_NEW_INBOUND_DOC);

            }
            taskWrapper.direction = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.TASK_DIRECTION_INBOUND);
            taskWrappers.add(taskWrapper);
        }
        if (!taskWrappers.isEmpty()) {
            spc_TaskProcessManager.createTasks(taskWrappers);
        }
    }
    /*******************************************************************************************************
    * @description  populating program case if available
    * @param Map of interaction ids and document logs
    * @return void
    */
    public void mapProgramCaseId(Map<Id, PatientConnect__PC_Document_Log__c> interactionIdsAndDocLogs) {
        for (PatientConnect__PC_Interaction__c interaction : [Select id, PatientConnect__PC_Patient_Program__c from PatientConnect__PC_Interaction__c where id in : interactionIdsAndDocLogs.keySet()]) {
            if (String.isNotBlank(interaction.PatientConnect__PC_Patient_Program__c)) {
                interactionIdsAndDocLogs.get(interaction.id).PatientConnect__PC_Program__c = interaction.PatientConnect__PC_Patient_Program__c;
            }
        }
    }
    /*******************************************************************************************************
    * @description  populating program case if available
    * @param Map of non program case ids and document logs
    * @return void
    */

    public void mapProgramCaseIdinDocLog(Map<Id, PatientConnect__PC_Document_Log__c> caseIdsAndDocLogs) {
        for (Case cs : [Select id, PatientConnect__PC_Program__c from Case where id in : caseIdsAndDocLogs.keySet()]) {
            if (String.isNotBlank(cs.PatientConnect__PC_Program__c)) {
                caseIdsAndDocLogs.get(cs.id).PatientConnect__PC_Program__c = cs.PatientConnect__PC_Program__c;
            }
        }
    }
    /*******************************************************************************************************
    * @description  populating program case if available
    * @param Map of coverage ids and document logs
    * @return void
    */

    public void populateCaseIdinDocLog(Map<Id, PatientConnect__PC_Document_Log__c> coverageIdsAndDocLogs) {
        for (PatientConnect__PC_Program_Coverage__c coverage : [Select id, PatientConnect__PC_Program__c from PatientConnect__PC_Program_Coverage__c  where id in : coverageIdsAndDocLogs.keySet()]) {
            if (String.isNotBlank(coverage.PatientConnect__PC_Program__c)) {
                coverageIdsAndDocLogs.get(coverage.id).PatientConnect__PC_Program__c = coverage.PatientConnect__PC_Program__c;
            }
        }
    }
}
/********************************************************************************************************
*  @author          Deloitte
*  @description     File Deletion Trigger Handler to allow only submitter to delete the files
*  @param           
*  @date            July 28, 2020
*********************************************************************************************************/
public with sharing class CCL_ContentDocumentLink_Handler {
    /*Object Name*/
    final static string ADF_OBJECT='CCL_Apheresis_Data_Form__c';
	final static string Order_Object='CCL_Order__c';
    final static string Document_Object='CCL_Document__c';
    /* list*/
    final static List<String> PROFILE_NAMES = new List<String>{'System Administrator', 'External Base Profile', 'Internal Base Profile'};
	final static string extBaseProfile = 'External Base Profile';
    /* private constructor*/
    private CCL_ContentDocumentLink_Handler() {

    }
    /********************************************************************************************************
*  @author          Deloitte
*  @description     this method puts the related ContentDocumentId, in a set
*  @param          ContenetDocumnt List
*  @date          Aug 19.20202
*********************************************************************************************************/    

 public static void handleAfterInsertorDelete(List<ContentDocumentLink> documentList) {
    final Set<Id> contentDocId=new Set<Id>();
    final Map<Id,Id> linkedEntityMap=new Map<Id,Id>();
     for(ContentDocumentLink doc:documentList) {
         contentDocId.add(doc.ContentDocumentId);
         linkedEntityMap.put(doc.ContentDocumentId,doc.LinkedEntityId);
     }
     if(!contentDocId.isEmpty()) {
         getNamesofDocs(contentDocId,linkedEntityMap);
     }
 }
 /********************************************************************************************************
*  @author          Deloitte
*  @description     this method puts the related adfIds in a set
*  @param         set an dmap
*  @date          Aug 19.20202
*********************************************************************************************************/    

    public static  void getNamesofDocs(Set<Id> contentDocId,  Map<Id,Id>linkedEntityMap) {
       final Set<Id> adfIds=new Set<Id>();
	   final Set<Id> orderIds=new Set<Id>();
        /*Set to collect all document Ids*/
        final Set<Id> docIds=new Set<Id>();
        /*Map to store custom document id to contentDocumnt record to get both file name and extension*/
        final Map<Id,String> docExtnsnMap = new Map<Id,String>();
        final Map<Id,String> adfIdTitleMap=new Map<Id,String>();
        List<ContentDocument> contentDocs =new   List<ContentDocument>();
		List<ContentVersion> contentVersion = new List<ContentVersion>();
        
        if (ContentDocument.sObjectType.getDescribe().isAccessible()) {
             contentDocs=[Select Title,FileExtension,parentId,CreatedBy.Profile.Name from ContentDocument where Id in:contentDocId and CreatedBy.Profile.Name in:PROFILE_NAMES ];
			  contentVersion = [select id, contentdocumentid, contentsize, fileextension ,FirstPublishLocationId,PathOnClient from contentversion where contentdocumentid in :contentDocId];
        }
            
        for(ContentDocument doc: contentDocs) {
            if(ADF_OBJECT.equalsIgnoreCase(linkedEntityMap.get(doc.Id).getSObjectType().getDescribe().getName())) {
                adfIds.add(linkedEntityMap.get(doc.Id));
                adfIdTitleMap.put(linkedEntityMap.get(doc.Id),doc.Title);
            }
			 if(Order_Object.equalsIgnoreCase(linkedEntityMap.get(doc.Id).getSObjectType().getDescribe().getName()) &&
              doc.CreatedBy.Profile.Name == extBaseProfile) {
            	orderIds.add(linkedEntityMap.get(doc.Id));
				}
             /*Added after the file upload logic changed from order to custom document object*/
            if(Document_Object.equalsIgnoreCase(linkedEntityMap.get(doc.Id).getSObjectType().getDescribe().getName())) {
                docIds.add(linkedEntityMap.get(doc.Id));
             
            }
            for(contentversion conVersn :contentVersion ) {
                docExtnsnMap.put(linkedEntityMap.get(doc.Id), conVersn.PathOnClient);
            }
        }
        if(!adfIds.isEmpty()) {
         setNamesinADF(adfIds,adfIdTitleMap);
        }
		if(!orderIds.isEmpty()){
            fileExtUserUpload(orderIds);
			}
       if(!docIds.isEmpty()) {
         updateADFfrmDocument(docIds,docExtnsnMap);
        }
    }
    /********************************************************************************************************
*  @author          Deloitte
*  @description     this method updates the text field in adf
*  @param         set and map
*  @date          Aug 19.20202
*********************************************************************************************************/    

    public static void setNamesinADF(Set<Id> adfIds,Map<Id,String>adfIdTitleMap) {
        final list<CCL_Apheresis_Data_Form__c> adfList=new List<CCL_Apheresis_Data_Form__c>();
        for(CCL_Apheresis_Data_Form__c adf:[select 
                                            CCL_ADF_Document_Titles__c,
                                            id from CCL_Apheresis_Data_Form__c where Id in:adfIds]) {
            
            if(adf.CCL_ADF_Document_Titles__c==null || adf.CCL_ADF_Document_Titles__c==' ') {
                adf.CCL_ADF_Document_Titles__c=adfIdTitleMap.get(adf.Id); 
            } else {
                adf.CCL_ADF_Document_Titles__c=adf.CCL_ADF_Document_Titles__c+','+adfIdTitleMap.get(adf.Id);
            }
            adfList.add(adf);
        }
         
            if (CCL_Apheresis_Data_Form__c.sObjectType.getDescribe().isAccessible()) {
                Database.update(adfList);
            }
        
     
    }
    
     /********************************************************************************************************
*  @author          Deloitte
*  @description     this method updates the text field in adf
*  @param         set and map
*  @date          Jan 19.2021
*********************************************************************************************************/    

    public static void updateADFfrmDocument(Set<Id> docIds,Map<Id,String> docExtnsnMap) {
        final list<CCL_Apheresis_Data_Form__c> adfList=new List<CCL_Apheresis_Data_Form__c>();
        final Map<Id,Id> docToAdfMap = new Map<Id,Id>();
         final String nameTOAppend;
        for(CCL_Document__c docCstm : [SELECT Id,CCL_Apheresis_Data_Form__c FROM CCL_Document__c where Id in :docIds]) {
            docToAdfMap.put(docCstm.CCL_Apheresis_Data_Form__c,docCstm.Id);
        }
        
        for(CCL_Apheresis_Data_Form__c adf:[select 
                                            CCL_ADF_Document_Titles__c,
                                            id,CCL_Order__c from CCL_Apheresis_Data_Form__c where Id in :docToAdfMap.keySet()]) {
                                                if(!docExtnsnMap.isEmpty()) {
                                                nameTOAppend = docExtnsnMap.get(docToAdfMap.get(adf.Id));
                                                if(adf.CCL_ADF_Document_Titles__c==null || adf.CCL_ADF_Document_Titles__c==' ') {
                adf.CCL_ADF_Document_Titles__c=nameTOAppend; 
            } else {
                adf.CCL_ADF_Document_Titles__c=adf.CCL_ADF_Document_Titles__c+','+nameTOAppend;
            }
            adfList.add(adf);
                                            }
        }
            if (CCL_Apheresis_Data_Form__c.sObjectType.getDescribe().isAccessible()) {
                Database.update(adfList);
            }
        
     
    }

    /*
     *   @author           Deloitte
     *   @description      Checks the object the related document links to, and if one of the required object types (Shipment, Custom Order, or Finished Product)
     *                     which requires external/community visibility, sets Visility to allow both external and internal users.
     *   @param            List<ContentDocumentLink>
     *   @return           void
     *   @date             1 September 2020
     */
    public static void setExternalUserVisibility(List<ContentDocumentLink> documentLinkList) {

        final List<String> objectAPINameList = new List<String>{'CCL_Shipment__c', 'CCL_Order__c', 'CCL_Finished_Product__c','CCL_Apheresis_Data_Form__c','CCL_Document__c'};
        final Map<String, Schema.SObjectType> globalObjectMap = Schema.getGlobalDescribe();
        final Set<String> objectPrefixSet = new Set<String>();
        
        for (String objectAPIName : objectAPINameList) {
            objectPrefixSet.add(globalObjectMap.get(objectAPIName).getDescribe().getKeyPrefix());
        }

        for (ContentDocumentLink documentLink : documentLinkList) {
            if (objectPrefixSet.contains(String.valueOf(documentLink.LinkedEntityId).substring(0,3))) {
                documentLink.Visibility = 'AllUsers';
            }
        }
    }
	
/********************************************************************************************************
*  @author          Deloitte
*  @description     this method updates the CCL_DocExtUpload__c on Order Object
*  @param         set 
*  @date          Sep 16.2020
*********************************************************************************************************/    
    public static void fileExtUserUpload(Set<Id>orderIds ) {
        final list<CCL_Order__c> orderList=new List<CCL_Order__c>();
        for(CCL_Order__c order:[select CCL_DocExtUpload__c, id from CCL_Order__c where Id in:orderIds]) {
                order.CCL_DocExtUpload__c=true; 
            orderList.add(order);
        }
        if (CCL_Order__c.sObjectType.getDescribe().isAccessible()) {
                Database.update(orderList);
            }
    }

/********************************************************************************************************
*  @author          Deloitte
*  @description     This method updates a flag on Finished Product if files have been attached to the FP 
*                   Shipment associated to a Finished Product
*  @param           List<ContentDocumentLink>
*  @date            Jan 07 2021
*********************************************************************************************************/
    public static void updateRelatedDocumentsFlagOnFinishedProducts(List<ContentDocumentLink> newDocuments) {

        final Set<Id> parentShipmentIds = new Set<Id>();
        List<CCL_Shipment__c> parentShipments = new List<CCL_Shipment__c>();
        List<CCL_Finished_Product__c> finishedProducts = new List<CCL_Finished_Product__c>();
        final Map<Id,Integer> numberOfDocsOnShipmentMap = new Map<Id,Integer>();

        for(ContentDocumentLink doc : newDocuments) {
            parentShipmentIds.add(doc.LinkedEntityId);
        }
        
        if(!parentShipmentIds.isEmpty()) {
            parentShipments = CCL_ContentDocumentLink_Accessor.fetchParentShipmentsOfDocumentsCreatedByCTIntegrationUser(parentShipmentIds);
            finishedProducts = CCL_ContentDocumentLink_Accessor.fetchFinishedProductsRelatedToShipments(parentShipmentIds);
        }
        
        if(!parentShipments.isEmpty()) {    
            for(CCL_Shipment__c shipment : parentShipments) {
                numberOfDocsOnShipmentMap.put(shipment.Id, shipment.ContentDocumentLinks.size());
            }
        }

        if(!finishedProducts.isEmpty()) {
            for(CCL_Finished_Product__c fp : finishedProducts) {
                Integer numberOfDocs = 0;
                if(fp.CCL_Shipment__c != null) {
                    numberOfDocs = numberOfDocsOnShipmentMap.get(fp.CCL_Shipment__c);
                }
                fp.CCL_Related_Documents_On_FP_Shipment__c = numberOfDocs > 0; 
            }  
            if(Schema.sObjectType.CCL_Finished_Product__c.isUpdateable()) {
                update finishedProducts;
            }
        }	
    }
}
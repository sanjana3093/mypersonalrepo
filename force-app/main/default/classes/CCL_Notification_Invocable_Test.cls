/********************************************************************************************************
 *  @author          Deloitte
 *  @description     Test class for the following classes : CCL_Notification_Invocable class
 *                   Notification_helper Notification_Handler Notification_Utility 
 *  @param
 *  @date            August 3, 2020
 *********************************************************************************************************/
@isTest
public with sharing class CCL_Notification_Invocable_Test {
  @testSetup
  public static void createTestData() {
    final String currentTime= String.valueOf(System.now().millisecond());
    final String randomNum= String.valueOf(Math.abs(Crypto.getRandomInteger()));

    String profileName = CCL_StaticConstants_MRC.SYSTEM_ADMIN_USER_PROFILE_TYPE;
    User systeAdminUser = CCL_TestDataFactory.createTestUser(profileName);

    UserRole nvsRoleId = [SELECT Id, DeveloperName 
                                    FROM UserRole 
                                    WHERE DeveloperName =:CCL_StaticConstants_MRC.ROLE_NOVARTIS_ADMIN 
                                    LIMIT 1];


    systeAdminUser.UserRoleId = nvsRoleId.Id;

    insert systeAdminUser;

    System.runAs(systeAdminUser) {
      Account testAccountSite = CCL_TestDataFactory.createTestParentAccount();
      testAccountSite.Name = CCL_Notifications_Constants.TESTSITEACCOUNT_VALUE;
      testAccountSite.OwnerId = systeAdminUser.Id;
      insert testAccountSite;
        
    Contact portalUserCon = new Contact();
    portalUserCon = CCL_TestDataFactory.createCommunityUser(testAccountSite );
    insert portalUserCon;
    
    User communityUser = CCL_TestDataFactory.createTestUser(CCL_StaticConstants_MRC.EXTERNAL_BASE_PROFILE_TYPE);
    communityUser.ContactId = portalUserCon.Id;
    insert communityUser;
    final PermissionSetAssignment prfSubmitterAssignment = CCL_TestDataFactory.createPermissionSetForAssignment(CCL_StaticConstants_MRC.PERMISSION_SET_PRF_SUBMITTER, communityUser);
    insert prfSubmitterAssignment;
      System.runAs(communityUser) {
        if (testAccountSite != null) {
          CCL_Therapy__c testTherapy = CCL_TestDataFactory.createTestTherapy();
          if (testTherapy != null) {
            insert testTherapy;
            CCL_Therapy_Association__c siteTherapyAssociation = CCL_TestDataFactory.createTestSiteTherapyAssoc(
              testAccountSite.Id,
              testTherapy.Id
            );
            CCL_Therapy_Association__c countryTherapyAssociation = CCL_TestDataFactory.createTestCountryTherapyAssoc(
              testAccountSite.Id,
              testTherapy.Id
            );
            insert siteTherapyAssociation;
            insert countryTherapyAssociation;

            CCL_User_Therapy_Association__c userTherapyAssociation = new CCL_User_Therapy_Association__c();
            userTherapyAssociation.CCL_Therapy_Association__c = countryTherapyAssociation.Id;
            userTherapyAssociation.CCL_User__c = communityUser.Id;
            Id externalRecordTypeId = 
            userTherapyAssociation.RecordTypeId = Schema.SObjectType.CCL_User_Therapy_Association__c.getRecordTypeInfosByName().get(CCL_Notifications_Constants.RECORDTYPE_EXTERNAL_USERTHERAPYASSO).getRecordTypeId();
            userTherapyAssociation.CCL_Record_Accessability__c = CCL_Notifications_Constants.RECORD_ACCESSABILITY_EDIT;
            userTherapyAssociation.Name = currentTime;
            userTherapyAssociation.CCL_Primary_Role__c = CCL_Notifications_Constants.PRIMARY_ROLE_PRFSUBMITTER;
            userTherapyAssociation.CCL_Commercial_Emails__c = CCL_Notifications_Constants.TESTRECORD_NOTIFICATION_SETTING;
            insert userTherapyAssociation;
            
            Folder emailTemplateFolder = [SELECT Id, Name, Developername FROM Folder WHERE DeveloperName = :CCL_Notifications_Constants.EMAIL_TEMPLATE_FOLDER_DEVELOPERNAME];
            BrandTemplate letterheadValue = [SELECT Id, Name FROM BrandTemplate WHERE Name = :CCL_Notifications_Constants.COMMERCIAL_LETTERHEAD];
            EmailTemplate testCommercialEmail = new EmailTemplate( IsActive = true, 
                                                                    TemplateStyle = CCL_Notifications_Constants.EMAIL_TEMPLATE_STYLE_FORMAL_LETTER, 
                                                                    BrandTemplateId = letterheadValue.Id , 
                                                                    developerName = CCL_Notifications_Constants.EMAIL_TEMPLATE_NAME, 
                                                                    FolderId = emailTemplateFolder.Id , 
                                                                    TemplateType= CCL_Notifications_Constants.TEMPLATE_TYPE_HTML, 
                                                                    Name = CCL_Notifications_Constants.EMAIL_TEMPLATE_NAME, 
                                                                    subject = CCL_Notifications_Constants.EMAIL_TEMPLATE_SUBJECT	, 
                                                                    HtmlValue = CCL_Notifications_Constants.EMAIL_TEMPLATE_HTMLVALUE);

            insert testCommercialEmail;
            System.debug('::testCommercialEmail ::TestDataSetup '+ testCommercialEmail);
            List<CCL_Order__c> collectionOrders = new List<CCL_Order__c>();

        for (Integer i = 0; i < 10; i++) {
            CCL_Order__c prfOrder = new CCL_Order__c();
            prfOrder.Name = currentTime + i;
            prfOrder.CCL_Hospital_Patient_ID__c = randomNum + i;
            prfOrder.CCL_Therapy__c = testTherapy.Id;
            prfOrder.CCL_Ordering_Hospital__c = testAccountSite.Id;
	          prfOrder.recordtypeId=CCL_StaticConstants_MRC.ORDER_RECORDTYPE_COMMERCIAL;
            collectionOrders.add(prfOrder);
        }
            System.debug('collectionOrders....... ' + collectionOrders);
            insert collectionOrders;
          }
        }
      }
    }
  }

  static testMethod void testInvocable() {

    CCL_Notification_Invocable.CCL_Notification_Invocable_Wrapper notificationItem = new CCL_Notification_Invocable.CCL_Notification_Invocable_Wrapper();
    List<CCL_Notification_Invocable.CCL_Notification_Invocable_Wrapper> wrapperList = new List<CCL_Notification_Invocable.CCL_Notification_Invocable_Wrapper>();

    Account testAccountSite = [SELECT Id, Name
                                FROM ACCOUNT 
                                Limit 1];

    List<CCL_Order__c> testOrders = [
                                    SELECT Id, CCL_Therapy__r.CCL_Type__c, 
                                    CCL_Ordering_Hospital__c
                                    FROM CCL_Order__c
                                    WHERE CCL_Ordering_Hospital__c = :testAccountSite.Id];

     System.debug('::wraptestOrdersperList ::TestDataSetup '+ testOrders);
    for(CCL_Order__c testOrder : testOrders) {
        notificationItem.RecordId = testOrder.Id;
        notificationItem.NotificationType = CCL_Notifications_Constants.TESTRECORD_NOTIFICATION_SETTING;
        notificationItem.Category = testOrder.CCL_Therapy__r.CCL_Type__c;
        notificationItem.objectAPIName = 'CCL_Order__c';
        wrapperList.add(notificationItem);
    }

    System.debug('::wrapperList ::TestDataSetup '+ wrapperList);


    Test.startTest();

    CCL_Notification_Invocable.invokeNotificationProcess(wrapperList);
    List<AsyncApexJob> jobList =[SELECT Id , Status FROM AsyncApexJob where JobType='Queueable'];
    System.assertNotEquals(null,jobList);
    // List<CCL_Notification__c> notificationRecordCreated =  [SELECT Id
    //                                                         FROM CCL_Notification__c 
    //                                                         WHERE CCL_Notification_Type__c = :CCL_Notifications_Constants.TESTRECORD_NOTIFICATION_SETTING]; 

    //System.assertEquals( 1, notificationRecordCreated.size(), 'The following notifications were created: ' + notificationRecordCreated.size());

    Test.stopTest();
  
  }
}
/**
*  @author          Deloitte
*  @date            12/27/2016
*  @description     Enrollment Wizard Controller class for physician
*  @version         1.0
*/
public class spc_PhysicianEWPController {
    /*************************************************************************************************
    * @author      Deloitte
    * @date        12/27/2016
    * @Description Method to get fields on Physician Account
    * @param       None
    * @Return      PhysicianRecord
    **************************************************************************************************/
    @AuraEnabled
    public static PhysicianRecord getFields() {

        PhysicianRecord displayFields = new PhysicianRecord();
        //toget the labels of all fileds
        displayFields.type = Account.Type.getDescribe().getLabel();
        displayFields.recordTypeId = Account.RecordTypeId.getDescribe().getLabel();
        displayFields.name = Account.Name.getDescribe().getLabel();
        displayFields.recordTypeName = Account.RecordType.Name.getDescribe().getLabel();

        displayFields.salutation = Account.PatientConnect__PC_Salutation__c.getDescribe().getLabel();
        displayFields.firstName = Account.PatientConnect__PC_First_Name__c.getDescribe().getLabel();
        displayFields.lastName = Account.PatientConnect__PC_Last_Name__c.getDescribe().getLabel();

        displayFields.addressId = Account.PatientConnect__PC_Primary_Address__c.getDescribe().getLabel();
        displayFields.primaryAddress1 = Account.PatientConnect__PC_Primary_Address_1__c.getDescribe().getLabel();
        displayFields.primaryAddress2 = Account.PatientConnect__PC_Primary_Address_2__c.getDescribe().getLabel();
        displayFields.primaryAddress3 = Account.PatientConnect__PC_Primary_Address_3__c.getDescribe().getLabel();
        displayFields.primaryCity = Account.PatientConnect__PC_Primary_City__c.getDescribe().getLabel();
        displayFields.primaryState = Account.PatientConnect__PC_Primary_State__c.getDescribe().getLabel();
        displayFields.primaryZipCode = Account.PatientConnect__PC_Primary_Zip_Code__c.getDescribe().getLabel();

        displayFields.email = Account.PatientConnect__PC_Email__c.getDescribe().getLabel();
        displayFields.phone = Account.Phone.getDescribe().getLabel();
        displayFields.fax = Account.Fax.getDescribe().getLabel();
        displayFields.assnPhone = Account.Phone.getDescribe().getLabel();
        displayFields.assnFax = Account.Fax.getDescribe().getLabel();
        displayFields.npi = Account.PatientConnect__PC_NPI__c.getDescribe().getLabel();

        displayFields.communicationLanguage = Account.PatientConnect__PC_Communication_Language__c.getDescribe().getLabel();
        displayFields.preferredCommunicationChannel = Account.PatientConnect__PC_Preferred__c.getDescribe().getLabel();

        displayFields.status = Account.PatientConnect__PC_Status__c.getDescribe().getLabel();
        displayFields.specialistType = Account.PatientConnect__PC_Specialist_Type__c.getDescribe().getLabel();

        displayFields.city = Account.PatientConnect__PC_Primary_City__c.getDescribe().getLabel();
        displayFields.state = Account.PatientConnect__PC_Primary_State__c.getDescribe().getLabel();

        return displayFields;
    }


    /********************************************************************************************************
     *  @author           Deloitte
     *  @date             11/06/2018
     *  @description      This method is used for getting picklist values
     *  @param            None
     *  @return           Map<String, List<PicklistEntryWrapper>>
     *********************************************************************************************************/
    @AuraEnabled
    public static Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> getPicklistEntryMap() {
        Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> picklistEntryMap =
            new Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>>();


        picklistEntryMap.put('addrType', spc_PicklistFieldManager.getPicklistEntryWrappers(PatientConnect__PC_Address__c.PatientConnect__PC_Address_Description__c));
        picklistEntryMap.put('addrStatus', spc_PicklistFieldManager.getPicklistEntryWrappers(PatientConnect__PC_Address__c.PatientConnect__PC_Status__c));
        picklistEntryMap.put('countries', spc_PicklistFieldManager.getPicklistEntryWrappers(PatientConnect__PC_Address__c.PatientConnect__PC_Country__c));

        return picklistEntryMap;
    }

    /********************************************************************************************************
     *  @author           Deloitte
     *  @date             11/06/2018
     *  @description      This method is used for getting picklist values
     *  @param            None
     *  @return           Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>>
     *********************************************************************************************************/
    @AuraEnabled
    public static Map<String, List<spc_PicklistFieldManager.PicklistEntryWrapper>> getDependentOptionsImpl() {
        return spc_PicklistFieldManager.picklistFieldMap(PatientConnect__PC_Address__c.PatientConnect__PC_Country__c, PatientConnect__PC_Address__c.PatientConnect__PC_State__c);
    }

    /*********************************************************************************
     * @author          Deloitte
     * @Description     Returns the applicant's Physician associated with the enrollment case for the active applicant ID provided.
     * @param           The Id of the enrollment case for the applicant to retrieve
     * @param           Active applicant on Enrollment case.
     * @return          List of PhysicianRecord
    *********************************************************************************/
    @AuraEnabled
    public static List<PhysicianRecord> getApplicantPhysician(Id enrollmentCaseId, Id activeApplicantId) {
        List<PhysicianRecord> records = new List<PhysicianRecord>();

        try {
            List<String> fields = new List<String> {'PatientConnect__PC_Physician_Name__c', 'PatientConnect__PC_Physician_Phone__c', 'PatientConnect__PC_Physician_Fax__c',
                                                    'PatientConnect__PC_Physician_Street_Address1__c', 'PatientConnect__PC_Physician_City__c', 'PatientConnect__PC_Physician_State__c'
                                                   };

            //FLS check access before SOQL
            SPC_Database.assertAccess('PatientConnect__PC_Applicant__c', SPC_Database.Operation.Reading, fields);

            //get a list of applicants for active applicant
            List<PatientConnect__PC_Applicant__c> lstapplicant = [SELECT PatientConnect__PC_Physician_Name__c, PatientConnect__PC_Physician_Phone__c, PatientConnect__PC_Physician_Fax__c,
                                                  PatientConnect__PC_Physician_Street_Address1__c, PatientConnect__PC_Physician_City__c, PatientConnect__PC_Physician_State__c
                                                  FROM PatientConnect__PC_Applicant__c WHERE Id = : activeApplicantId LIMIT 1];

            if (lstapplicant != null && !lstapplicant.isEmpty() && !String.isEmpty(lstapplicant[0].PatientConnect__PC_Physician_Name__c)) {
                records.add(new PhysicianRecord(lstapplicant[0]));
            }


        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }
        return records;
    }

    /*********************************************************************************
     * @author          Deloitte
     * @Description     search records of physician based on search string and filter in EW
     * @param           searchString
     * @return          List of PhysicianRecord
    *********************************************************************************/
    @AuraEnabled
    public static List<PhysicianRecord> searchRecords(String searchString) {
        try {
            List<PhysicianRecord> records = new List<PhysicianRecord>();
            String recordType = 'Physician';
            String newSearchString = '%' + String.escapeSingleQuotes(searchString) + '%';
            String newSOSLSearchString = '*' + String.escapeSingleQuotes(searchString) + '*';

            List<String> fields = new List<String> {
                'Id', 'PatientConnect__PC_First_Name__c',
                'PatientConnect__PC_Last_Name__c', 'PatientConnect__PC_Primary_Address__c', 'PatientConnect__PC_Primary_Address_1__c',
                'PatientConnect__PC_Primary_Address_2__c', 'PatientConnect__PC_Primary_Address_3__c',
                'PatientConnect__PC_Primary_City__c', 'PatientConnect__PC_Primary_State__c', 'PatientConnect__PC_Status__c',
                'PatientConnect__PC_Primary_Zip_Code__c', 'PatientConnect__PC_Salutation__c',
                'PatientConnect__PC_Email__c', 'Phone', 'Fax', 'Type' , 'RecordTypeId',
                'Name', 'PatientConnect__PC_NPI__c', 'RecordType.Name', 'PatientConnect__PC_Communication_Language__c', 'PatientConnect__PC_Preferred__c',
                'PatientConnect__PC_Specialist_Type__c'
            };

            SPC_Database.assertAccess('Account', SPC_Database.Operation.Reading, fields);


            List<List<SObject>> searchResults = [FIND :newSOSLSearchString IN ALL FIELDS
                                                 RETURNING Account(
                                                         Id, PatientConnect__PC_First_Name__c, PatientConnect__PC_Primary_Address__c,
                                                         PatientConnect__PC_Last_Name__c, PatientConnect__PC_Primary_Address_1__c,
                                                         PatientConnect__PC_Primary_Address_2__c, PatientConnect__PC_Primary_Address_3__c,
                                                         PatientConnect__PC_Primary_City__c, PatientConnect__PC_Primary_State__c, PatientConnect__PC_Status__c,
                                                         PatientConnect__PC_Primary_Zip_Code__c, PatientConnect__PC_Salutation__c,
                                                         PatientConnect__PC_Email__c, Phone, Fax, Type, RecordTypeId,
                                                         Name, PatientConnect__PC_NPI__c, RecordType.Name, PatientConnect__PC_Communication_Language__c, PatientConnect__PC_Preferred__c,
                                                         PatientConnect__PC_Specialist_Type__c
                                                         WHERE Account.RecordType.Name = :recordType

                                                 )];
            List<Account> accts = searchResults[0];
            PhysicianRecord record;

            for (Account a : accts) {
                record = new PhysicianRecord(a);
                records.add(record);
            }
            return records;
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
            return null;
        }
    }

    /*********************************************************************************
     * @author          Deloitte
     * @className       PhysicianRecord
     * @Description     Wrapper Class to hold PhysicianRecord data
    *********************************************************************************/
    public class PhysicianRecord {
        @AuraEnabled public String Id { get; set; }
        @AuraEnabled public String firstName { get; set; }
        @AuraEnabled public String lastName { get; set; }
        @AuraEnabled public String addressId { get; set; }
        @AuraEnabled public String primaryAddress1 { get; set; }
        @AuraEnabled public String primaryAddress2 { get; set; }
        @AuraEnabled public String primaryAddress3 { get; set; }
        @AuraEnabled public String primaryCity { get; set; }
        @AuraEnabled public String primaryState { get; set; }
        @AuraEnabled public String primaryZipCode { get; set; }
        @AuraEnabled public String salutation { get; set; }
        @AuraEnabled public String email { get; set; }
        @AuraEnabled public String phone { get; set; }
        @AuraEnabled public String assnPhone { get; set; }
        @AuraEnabled public String assnFax { get; set; }
        @AuraEnabled public String fax { get; set; }
        @AuraEnabled public String type { get; set; }
        @AuraEnabled public String recordTypeId { get; set; }
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public String npi { get; set; }
        @AuraEnabled public String recordTypeName { get; set; }
        @AuraEnabled public String isSelected { get; set; }
        @AuraEnabled public String communicationLanguage { get; set; }
        @AuraEnabled public String preferredCommunicationChannel { get; set; }
        @AuraEnabled public String status { get; set; }
        @AuraEnabled public String specialistType { get; set; }
        @AuraEnabled public String city { get; set; }
        @AuraEnabled public String state { get; set; }
        @AuraEnabled public String remsID { get; set; }


        public PhysicianRecord() {

        }


        public PhysicianRecord(Account a) {
            Id = a.Id;
            firstName = a.PatientConnect__PC_First_Name__c;
            lastName = a.PatientConnect__PC_Last_Name__c;
            addressId = a.PatientConnect__PC_Primary_Address__c;
            primaryAddress1 = a.PatientConnect__PC_Primary_Address_1__c;
            primaryAddress2 = a.PatientConnect__PC_Primary_Address_2__c;
            primaryAddress3 = a.PatientConnect__PC_Primary_Address_3__c;
            primaryCity = a.PatientConnect__PC_Primary_City__c;
            primaryState = a.PatientConnect__PC_Primary_State__c;
            primaryZipCode = a.PatientConnect__PC_Primary_Zip_Code__c;
            salutation = a.PatientConnect__PC_Salutation__c;
            email = a.PatientConnect__PC_Email__c;
            phone = a.Phone;
            fax = a.Fax;
            assnPhone = '';
            assnFax = '';
            type = a.Type;
            recordTypeId = a.RecordTypeId;
            name = a.Name;
            npi = a.PatientConnect__PC_NPI__c;
            recordTypeName = a.RecordType.Name;
            isSelected = 'false';
            communicationLanguage = a.PatientConnect__PC_Communication_Language__c;
            preferredCommunicationChannel = a.PatientConnect__PC_Preferred__c;
            status = a.PatientConnect__PC_Status__c;
            specialistType = a.PatientConnect__PC_Specialist_Type__c;
            city = a.PatientConnect__PC_Primary_City__c;
            state = a.PatientConnect__PC_Primary_State__c;
        }

        public PhysicianRecord(PatientConnect__PC_Applicant__c a) {
            name = a.PatientConnect__PC_Physician_Name__c;
            phone = a.PatientConnect__PC_Physician_Phone__c;
            fax = a.PatientConnect__PC_Physician_Fax__c;
            primaryAddress1 = a.PatientConnect__PC_Physician_Street_Address1__c;
            city = a.PatientConnect__PC_Physician_City__c;
            state = a.PatientConnect__PC_Physician_State__c;
        }

    }
    /********************************************************************************************************
     *  @author           deloitte
     *  @date             11/06/2018
     *  @description      This method is used for getting addreses
     *  @param            physicianId Id
     *  @return           List<PatientConnect__PC_Address__c>
     *********************************************************************************************************/
    @AuraEnabled
    public static List<PatientConnect__PC_Address__c> getPhysicianAddresses(Id physicianId) {
        List<PatientConnect__PC_Address__c> addresses;
        if (String.isNotBlank(physicianId)) {
            addresses = new List<PatientConnect__PC_Address__c>([Select Id, PatientConnect__PC_Address_1__c, PatientConnect__PC_Address_2__c, PatientConnect__PC_Address_3__c,
                    PatientConnect__PC_City__c, spc_Phone__c, spc_Phone_2__c, spc_Fax__c, spc_Fax_2__c, PatientConnect__PC_State__c, PatientConnect__PC_Country__c, PatientConnect__PC_Address_Description__c,
                    PatientConnect__PC_Zip_Code__c, PatientConnect__PC_Primary_Address__c from PatientConnect__PC_Address__c where PatientConnect__PC_Account__c = :physicianId]);
        }
        return addresses;
    }
}
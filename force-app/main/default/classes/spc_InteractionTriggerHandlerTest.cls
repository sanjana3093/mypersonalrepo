/**
* @author Deloitte
* @date 6-14-2018
*
* @description This is the test class for Interaction Trigger Implementation
*/

@isTest
public class spc_InteractionTriggerHandlerTest {

    @testsetup
    static void setup() {

        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
        spc_Database.ins(apexConstansts);
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User objUser = new User(Alias = 'test', email = 'TestSysAdmin@test123.com', emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', country = 'United States', profileid = p.Id,
                                timezonesidkey = 'America/Los_Angeles', username = System.currentTimeMillis() + 'TestSysAdmin@test123.com');

        spc_Database.ins(objUser);
        //Create a patient account
        Account acc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        acc.PatientConnect__PC_Email__c = 'test@email.com';
        insert acc;

        Account manf = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId());
        spc_Database.ins(manf);

        PatientConnect__PC_Engagement_Program__c EP = spc_Test_Setup.createEngagementProgram('Test EP', manf.id);
        spc_Database.ins(EP);

        //Create a HCO account
        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
        insert hcoAcc;
        //Create a program case
        Case programCaseRec2 = spc_Test_Setup.createCases(new List<Account> {acc}, 1, 'PC_Program').get(0);
        if (null != programCaseRec2) {
            programCaseRec2.Type = 'Program';
            programCaseRec2.OwnerId = objUser.Id;
            programCaseRec2.PatientConnect__PC_Engagement_Program__c = EP.id;

            insert programCaseRec2;
        }
        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(programCaseRec2.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        interaction.spc_SOC_Type__c = 'Home';
        interaction.spc_HIP__c = 'Other';
        interaction.spc_Scheduled_Date__c = System.today();
        insert interaction;
        interaction.PatientConnect__PC_Status__c = 'In Progress';
        update interaction;
    }

    public static testMethod void onInsertCheckToCreateDoc() {
        ID HCO_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        //Create a patient account
        Account acc = [select id from Account where PatientConnect__PC_Email__c = 'test@email.com' LIMIT 1];
        //Create a HCO account
        Account hcoAcc = [select id from Account where recordtypeid = :HCO_RT LIMIT 1];
        //Create a program case
        Case patientCase = [select id from Case LIMIT 1];
        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(patientCase.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        interaction.PatientConnect__PC_Status__c = 'Complete';
        interaction.spc_Milestone__c = 'Admin';
        interaction.spc_Reason_for_Status__c = 'Admin Complete';
        interaction.spc_SOC_Type__c = 'Home';
        interaction.spc_HIP__c = 'Other';
        insert interaction;
    }
    public static testMethod void onUpdate1CheckToCreateDoc() {
        ID HCO_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        //Create a patient account
        Account acc = [select id from Account where PatientConnect__PC_Email__c = 'test@email.com' LIMIT 1];
        //Create a HCO account
        Account hcoAcc = [select id from Account where recordtypeid = :HCO_RT LIMIT 1];
        //Create a program case
        Case patientCase = [select id from Case LIMIT 1];
        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(patientCase.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        interaction.PatientConnect__PC_Status__c = 'Complete';
        interaction.spc_SOC_Type__c = 'Home';
        interaction.spc_HIP__c = 'Other';
        insert interaction;
        interaction.spc_Milestone__c = 'Admin';
        interaction.spc_Reason_for_Status__c = 'Admin Complete';
        update interaction;
    }

    public static testMethod void onUpdate2CheckToCreateDoc() {
        ID HCO_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        //Create a patient account
        Account acc = [select id from Account where PatientConnect__PC_Email__c = 'test@email.com' LIMIT 1];
        //Create a HCO account
        Account hcoAcc = [select id from Account where recordtypeid = :HCO_RT LIMIT 1];
        //Create a program case
        Case patientCase = [select id from Case LIMIT 1];
        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(patientCase.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        interaction.PatientConnect__PC_Status__c = 'Process cancelled';
        interaction.spc_SOC_Type__c = 'Home';
        interaction.spc_Reason_for_Status__c = 'Out of Network';

        interaction.spc_HIP__c = 'Other';
        insert interaction;
        interaction.spc_Reason_for_Status__c = 'PA Denied';
        update interaction;
    }
    public static testMethod void onUpdate3CheckToCreateDoc() {
        ID HCO_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        //Create a patient account
        Account acc = [select id from Account where PatientConnect__PC_Email__c = 'test@email.com' LIMIT 1];
        //Create a HCO account
        Account hcoAcc = [select id from Account where recordtypeid = :HCO_RT LIMIT 1];
        //Create a program case
        Case patientCase = [select id from Case LIMIT 1];
        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(patientCase.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        interaction.PatientConnect__PC_Status__c = 'Process cancelled';
        interaction.spc_Reason_for_Status__c = 'Out of Network';
        interaction.spc_SOC_Type__c = 'Home';

        interaction.spc_HIP__c = 'Other';
        insert interaction;
        interaction.spc_Milestone__c = 'Admin';
        interaction.spc_Reason_for_Status__c = 'Medication Not Started';
        update interaction;
    }

    public static testMethod void onUpdate4CheckToCreateDoc() {
        ID HCO_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        //Create a patient account
        Account acc = [select id from Account where PatientConnect__PC_Email__c = 'test@email.com' LIMIT 1];
        //Create a HCO account
        Account hcoAcc = [select id from Account where recordtypeid = :HCO_RT LIMIT 1];
        //Create a program case
        Case patientCase = [select id from Case LIMIT 1];
        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(patientCase.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        interaction.PatientConnect__PC_Status__c = 'Process cancelled';
        interaction.spc_Reason_for_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.INTRACTN_REASON_COVERAGE_DENIED);
        interaction.spc_SOC_Type__c = 'Home';

        interaction.spc_HIP__c = 'Other';
        insert interaction;

        update interaction;
    }

    public static testMethod void onUpdate5CheckToCreateDoc() {
        ID HCO_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        //Create a patient account
        Account acc = [select id from Account where PatientConnect__PC_Email__c = 'test@email.com' LIMIT 1];
        //Create a HCO account
        Account hcoAcc = [select id from Account where recordtypeid = :HCO_RT LIMIT 1];
        //Create a program case
        Case patientCase = [select id from Case LIMIT 1];
        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(patientCase.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        interaction.PatientConnect__PC_Status__c = 'Process cancelled';
        interaction.spc_Reason_for_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.INTRCTN_REASON_PATIENT_DECEASED);
        interaction.spc_SOC_Type__c = 'Home';

        interaction.spc_HIP__c = 'Other';
        insert interaction;

        update interaction;
    }

    public static testMethod void testExecution1() {
        ID HCO_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        //Create a patient account
        Account acc = [select id from Account where PatientConnect__PC_Email__c = 'test@email.com' LIMIT 1];
        //Create a HCO account
        Account hcoAcc = [select id from Account where recordtypeid = :HCO_RT LIMIT 1];
        //Create a program case
        Case patientCase = [select id from Case LIMIT 1];
        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(patientCase.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        interaction.spc_SOC_Type__c = 'Home';
        interaction.spc_HIP__c = 'Other';
        insert interaction;
    }

    static testMethod void onUpdateCheckToCreateDoc() {
        ID HCO_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        //Create a patient account
        Account acc = [select id from Account where PatientConnect__PC_Email__c = 'test@email.com' LIMIT 1];
        //Create a HCO account
        Account hcoAcc = [select id from Account where recordtypeid = :HCO_RT LIMIT 1];
        //Create a program case
        Case patientCase = [select id from Case LIMIT 1];
        //Create interaction
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(patientCase.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        interaction.spc_SOC_Type__c = 'Home';
        interaction.spc_HIP__c = 'Option Care';
        insert interaction;
        //Updating the interaction
        interaction.spc_HIP__c = 'Other';
        Update interaction;
    }
	
	public static testMethod void testExecution2(){
        ID HCO_RT=Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId();
        Account acc = [select id from Account where PatientConnect__PC_Email__c = 'test@email.com' LIMIT 1];
        Account hcoAcc = [select id from Account where recordtypeid=:HCO_RT LIMIT 1];
        Case patientCase = [select id from Case LIMIT 1];
        PatientConnect__PC_Interaction__c interaction = new PatientConnect__PC_Interaction__c();
        interaction = spc_Test_Setup.createInteraction(patientCase.Id);
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        interaction.spc_Scheduled_Date__c = System.today() + 10;
        interaction.PatientConnect__PC_Status__c = 'Process cancelled';
        interaction.spc_Reason_for_Status__c='Coverage Denied';
        insert interaction;
        
        Test.startTest();
        interaction.spc_Scheduled_Date__c = System.today();
        interaction.spc_Reason_for_Status__c='PA Denied';
        interaction.PatientConnect__PC_Participant__c = null;
        update interaction;
        interaction.spc_Reason_for_Status__c='Medication Not Started';
        interaction.PatientConnect__PC_Participant__c = hcoAcc.Id;
        update interaction;
        interaction.spc_Reason_for_Status__c='Patient Deceased';
        update interaction;
        Test.stopTest();
    }


}
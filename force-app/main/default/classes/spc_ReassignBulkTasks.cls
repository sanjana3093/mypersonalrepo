/**
* @author Deloitte
* @date Sep 17, 2018
*
* @description Controller class for spc_AssignToMe aura to assign a task
* based on the logged in user
*/

public with sharing class spc_ReassignBulkTasks{
	/*******************************************************************************************************
    * @description Return true if the profile of the current logged in user is not external user
    * @return Boolean 
    */
	
     @auraEnabled
     public static Boolean fetchLoggedInUser(){
         Boolean isProfileCorrect=false;
          Id currentLoggedUser=UserInfo.getUserId();
          User Ouser2=[SELECT Id,userRole.name,name,profile.name from User where id=:currentLoggedUser LIMIT 1];
          if(Ouser2.profile.name != spc_ApexConstants.SAGE_EXT_USER  && Ouser2.userRole.name==spc_ApexConstants.getUSerRole(spc_ApexConstants.USERRoles.USER_ROLE_SUPERVISOR)){
              isProfileCorrect=true;
          }else{
              isProfileCorrect=false;
          }
          return isProfileCorrect;
     }
	 /* *********************************************************************************
		Method Name    : getURL
		Developer      : Deloitte
		@Description    : return true if the profile of the current logged in user in not external user
		@return    : Boolean
     *********************************************************************************/  
     @auraEnabled
     public static String getURL(){
        String eventURL= spc_ApexConstants.getSGIPlatformEvent(spc_ApexConstants.SGIPlatformEvent.ADVERSE_EVENT);
          return eventURL;
     }
     
	/*******************************************************************************************************
    * @description Returns a list of all case managers
    * @return List<ReassignTasksWrapper> -  
	*/     
    @auraEnabled
    public static List<ReassignTasksWrapper> fetchCaseManagerList1(){
        List<User> lstCaseMangerUser=new list<User>();
        List<ReassignTasksWrapper> listUserWrapper=new List<ReassignTasksWrapper>();
        List<string> lstRoles=new List<String>();
       lstRoles.add(spc_ApexConstants.getUSerRole(spc_ApexConstants.USERRoles.USER_ROLE_CASEMANAGER));
        lstRoles.add(spc_ApexConstants.getUSerRole(spc_ApexConstants.USERRoles.USER_ROLE_SUPERVISOR));
        lstCaseMangerUser=[select id,name from User WHERE userRole.name in :  lstRoles  ];
        if(!lstCaseMangerUser.isEmpty()){
            for(User oUser : lstCaseMangerUser){
                ReassignTasksWrapper reTask = new ReassignTasksWrapper(OUser);
                listUserWrapper.add(reTask);
            }
        }
        return listUserWrapper;
    }
	/*******************************************************************************************************
    * @description Returns a list of all case managers except the one selected before
    * @return List<ReassignTasksWrapper>  
	*/
    @auraEnabled
    public static List<ReassignTasksWrapper> getCaseManagerList2(String selectedCaseManger){
       List<User> lstCaseMangerUser=new list<User>();
       List<string> lstRoles=new List<String>();
        lstRoles.add(spc_ApexConstants.getUSerRole(spc_ApexConstants.USERRoles.USER_ROLE_CASEMANAGER));
        lstRoles.add(spc_ApexConstants.getUSerRole(spc_ApexConstants.USERRoles.USER_ROLE_SUPERVISOR));
        List<ReassignTasksWrapper> listUserWrapper=new List<ReassignTasksWrapper>();
		//getting all the users who are case manager or supervisor
        lstCaseMangerUser=[select id,name from User WHERE userRole.name in :  lstRoles  ];
        if(!lstCaseMangerUser.isEmpty() && selectedCaseManger!=NULL){
        for(User oUser : lstCaseMangerUser){
            if(oUser.name!=selectedCaseManger){
                ReassignTasksWrapper reTask = new ReassignTasksWrapper(OUser);
                listUserWrapper.add(reTask);
                }
            }
        }
       return listUserWrapper;
    }
	/*******************************************************************************************************
    * @description To get the previous tasks assigned to the Selected Case Manager
	* @param selectedCaseManger the ID of the User 
	* @param orderByField the field with which the tasks should be ordered
	* @param isAsc Boolean to query ascending order or descending
	* @return List<ReassignTasksWrapper> 
	*/
    @AuraEnabled
    public static List<ReassignTasksWrapper> getAllTasksOfPreviousAssignee(String selectedCaseManger, String orderByField, boolean isAsc){
        List<ReassignTasksWrapper> listUserWrapper=new List<ReassignTasksWrapper>();
        User oUser=[select id , name from User WHERE name=:selectedCaseManger LIMIT 1];
        List<Task> lstTasks = spc_Database.queryWithAccess(getTaskQuery(oUser.id, orderByField, isAsc) , true);
        if(!lstTasks.isEmpty()){
            for(Task tk : lstTasks){
               ReassignTasksWrapper o=new ReassignTasksWrapper(tk) ;
               listUserWrapper.add(o);
            }
        }
        return listUserWrapper;
    }
	/*******************************************************************************************************
    * @description Called by getAllTasksOfPreviousAssignee to create the querystring 
	* @param ownerId the ID of the Owner of the tasks
	* @param orderByField the field with which the tasks should be ordered
	* @param isAsc Boolean to query ascending order or descending
	* @return String the Query to run 
	*/
    
    private static String getTaskQuery(Id ownerId, String orderByField,  boolean isAsc){
	//generating dynamic query for all the completed tasks.
        String query = 'select id, subject, ActivityDate, status, whatId, what.name from Task ';
        String whereQuery = ' where OwnerId = \''+ ownerId + '\'';
        whereQuery += ' AND Status != \''+ spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.TASK_STATUS_COMPLETED) + '\'';
        query += whereQuery;
        if (String.isNotBlank(orderByField)){
            query +=' order by ' + orderByField;
        }
        if (isAsc) {
            query += ' asc';
         } else {
            query += ' desc';
         }
        return query;
    }
	
	/*******************************************************************************************************
    * @description Fetch the Assignee's Id from the Name 
	* @param selectedCaseManger the Name of the Owner
	* @return Id of the Selected Case Manager 
	*/
    @AuraEnabled
    public static Id getCaseManagerOwnereId(String selectedCaseManger){
        User oUser=[select id,name from User Where name=:selectedCaseManger];
        return oUser.id;
    }
	
	/*******************************************************************************************************
    * @description Reassignment of all the tasks selected 
	* @param selectedTasks List of Task IDs selected
	* @param assignee - The name of the User to be assigned to
	*/
    @AuraEnabled
    public static void reassignTasks(List<Id> selectedTasks, String assignee){
        User oUser=[select id from User where name=:assignee LIMIT 1 ];
        List<Task> lstTasks=[select id,ownerid from Task Where id in :selectedTasks];
        try{
            if(!lstTasks.isEmpty()){
                for(Task tk: lstTasks){
                    tk.ownerId=oUser.id;
                }
            }
            spc_Database.upd(lstTasks);
        }
        catch( Exception ex){
             spc_Utility.createExceptionLog(ex);
        }
    }

/*******************************************************************************************************
    * @description Wrapper Class
	*/ 
    public class ReassignTasksWrapper {
        public ReassignTasksWrapper(User oUser) {
            this.name = oUser.name;
 }
         public ReassignTasksWrapper(Task oTask) {
            this.Tasksubject = oTask.subject;
            this.dueDate=string.valueOF(oTask.ActivityDate);
            this.taskStat=oTask.status;
            this.caseNumber=oTask.what.name;
            this.caseId=oTask.whatId;
            this.TaskId=oTask.id;

        }

        @AuraEnabled
        public String name { get;set; }
        @AuraEnabled
        public String Tasksubject { get;set; }
        @AuraEnabled
        public String dueDate { get;set; }
        @AuraEnabled
        public String taskStat { get;set; }
        @AuraEnabled
        public String caseNumber { get;set; }
        @AuraEnabled
        public String caseId { get;set; }
        @AuraEnabled
        public String TaskId { get;set; }
    }
    
}
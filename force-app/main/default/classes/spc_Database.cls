/*********************************************************************************************************
* This is The PC class cloned by Sage for Reference only
************************************************************************************************************/
public without sharing class spc_Database {
    public static boolean throwExceptions = true;

    public class OpNoAccessException extends Exception {


    }

    //Added to streamline the operations
    public enum Operation {
        Creating, Reading, Updating, Deleting
    }

    /*******************************************************************************************************
    * @description  This method is a placeholder to log exceptions/error messages
    * @param String
    * @return void
    */

    public static void processError(String msg) {
        if (throwExceptions) {
            system.assert(throwExceptions == true, msg);
        } else {
            system.debug(LoggingLevel.ERROR, msg);
        }
    }


    /*******************************************************************************************************
    * @description  This method is is used to check i
    * @param List<sObject> pObjs
    * @param Operation op
    * @param List<String> fieldNames
    * @return void
    */
    //Assert
    public static void assertAccess(List<sObject> pObjs, Operation op, List<String> fieldNames) {
        checkOperation(pObjs, op, fieldNames);
    }
    public static void assertAccess(String objName, Operation op, List<String> fieldNames) {
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        sObject pObj = gd.get(objName).newSObject();
        List<sObject> pObjs = new List<sObject>();
        pObjs.add(pObj);
        checkOperation(pObjs, op, fieldNames);
    }

    // Query records by providing a SOQL query and validate if user has permissions
    public static List<sObject> queryWithAccess(String SOQL, List<String> fieldNames) {
        List<sObject> dbRecords = Database.query(SOQL);

        if (dbRecords.size() > 0) {
            checkOperation(dbRecords, Operation.Reading, fieldNames);
        }

        return dbRecords;
    }

    public static List<sObject> queryWithAccess(String SOQL, List<String> fieldNames, Boolean flag) {
        if (flag) {
            return queryWithAccess(SOQL, fieldNames);
        } else {
            return Database.query(SOQL);
        }
    }
    public static List<sObject> queryWithAccess(String SOQL, Boolean flag) {
        List<String> fieldNames = new List<String>();
        return queryWithAccess(SOQL, fieldNames, flag);
    }

    // INSERT methods:

    public static List<sObject> ins(List<sObject> pObjs, List<sObjectField> pFields, boolean checkFls) {
        if (pObjs != NULL && !pObjs.isEmpty()) {
            if (checkFls) {
                checkOperation(pObjs, Operation.Creating);
            }

            insert pObjs;
        }

        return pObjs;
    }

    public static List<sObject> ins(List<sObject> pObjs, List<sObjectField> pFields) {
        return ins(pObjs, pFields, true);
    }

    public static List<sObject> ins(List<sObject> pObjs) {
        return ins(pObjs, new List<sObjectField>(), true);
    }

    public static List<sObject> ins(List<sObject> pObjs, boolean checkFls) {
        return ins(pObjs, new List<sObjectField>(), checkFls);
    }

    public static sObject ins(sObject pObj, List<sObjectField> pFields, boolean checkFls) {
        system.assertNotEquals(null, pObj);
        List<sObject> objs = new List<sObject>();
        objs.add(pObj);
        return ins(objs, pFields, checkFls)[0];
    }
    public static sObject ins(sObject pObj, List<sObjectField> pFields) {
        system.assertNotEquals(null, pObj);
        List<sObject> objs = new List<sObject>();
        objs.add(pObj);
        return ins(objs, pFields, true)[0];
    }

    // Security scan FALSE-POSITIVE: the db method is called with one object and returns one object.
    // The bulkifying alert is not relevant here as it is not being called in a loop.
    public static sObject ins(sObject pObj) {
        return ins(pObj, new List<sObjectField>());
    }
    public static sObject ins(sObject pObj, boolean checkFls) {
        return ins(pObj, new List<sObjectField>(), checkFls);
    }


    private static void checkOperation(List<sObject> pObjs, Operation op) {
        List<String> fieldNames = findFields(pObjs);
        checkOperation(pObjs, op, fieldNames);
    }

    private static void checkOperation(List<sObject> pObjs, Operation op, List<String> fieldNames) {
        Schema.DescribeSObjectResult objDesc = pObjs[0].getSObjectType().getDescribe();
        OpNoAccessException e = new OpNoAccessException();
        //CRUD on object
        if ((op == Operation.Creating && !objDesc.isCreateable())
                || (op == Operation.Updating && !objDesc.isUpdateable())) {
            e.setMessage('No access on ' + objDesc.getName() + ' for ' + op);
            throw e;
        } else if (op == Operation.Reading && !objDesc.isAccessible()) {
            e.setMessage('No access on ' + objDesc.getName() + ' for ' + op);
            throw e;
        } else if (op == Operation.Deleting && !objDesc.isDeletable()) {
            e.setMessage('No access on ' + objDesc.getName() + ' for ' + op);
            throw e;
        }

        //CRUD on fields if the object passes the check above
        Map<String, Schema.SObjectField> fieldNameMetaMap = objDesc.fields.getMap();
        List<String> noAccessFields = new List<String>();
        sObjectField field;
        Schema.DescribeFieldResult fieldDesc;

        if (fieldNames.size() > 0) {
            for (String fieldName : fieldNames) {

                field = fieldNameMetaMap.get(fieldName);
                if (field == null) {
                    continue;
                }
                fieldDesc = field.getDescribe();

                // ignore system fields that can't have permissions
                if (fieldDesc.isPermissionable()) {
                    if ((op == Operation.Creating && !fieldDesc.isCreateable())
                            || (op == Operation.Updating && !fieldDesc.isUpdateable())) {
                        noAccessFields.add(fieldName);
                    } else if ((op == Operation.Reading && !fieldDesc.isAccessible())) {
                        noAccessFields.add(fieldName);
                    }
                } else {
                    System.debug('Bypass Check: FLS on [' + objDesc.getName() + '.' + fieldName + '] for [' + op + '] since it\'s not isPermissionable()');
                }
            }
        }

        if (noAccessFields.size() > 0) {
            //logError('No Access on ' + objDesc.getLocalName() + ' for fields ' + noAccessFields);
            System.debug('No Access on ' + objDesc.getName() + ' for fields ' + noAccessFields + ' for ' + op);
            e.setMessage('No Access on ' + objDesc.getName() + ' for fields ' + noAccessFields + ' for ' + op);
            throw e;
        }
    }

    private static List<String> findFields(List<sObject> pObjs) {
        List<String> fieldNames = new List<String>();
        Map<String, Object> fieldNameValueMap;
        for (sObject pObj : pObjs) {
            fieldNameValueMap = pObj.getPopulatedFieldsAsMap();
            fieldNames.addAll(fieldNameValueMap.keyset());
        }
        return fieldNames;
    }


    public static boolean chkAccess(String objName, Operation op, List<String> fieldNames) {
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sobjType = gd.get(objName);
        Schema.DescribeSObjectResult objDesc = sobjType.getDescribe();
        //CRUD on object
        if ((op == Operation.Creating && !objDesc.isCreateable())
                || (op == Operation.Updating && !objDesc.isUpdateable())) {
            return false;
        } else if (op == Operation.Reading && !objDesc.isAccessible()) {
            return false;
        } else if (op == Operation.Deleting && !objDesc.isDeletable()) {
            return false;
        }
        //CRUD on fields if the object passes the check above
        Map<String, Schema.SObjectField> fieldNameMetaMap = objDesc.fields.getMap();
        sObjectField field;
        Schema.DescribeFieldResult fieldDesc;
        if (fieldNames.size() > 0) {
            for (String fieldName : fieldNames) {
                field = fieldNameMetaMap.get(fieldName);
                fieldDesc = field.getDescribe();
                // ignore system fields that can't have permissions
                if (fieldDesc.isPermissionable()) {
                    if ((op == Operation.Creating && !fieldDesc.isCreateable())
                            || (op == Operation.Updating && !fieldDesc.isUpdateable())) {
                        return false;
                    } else if ((op == Operation.Reading && !fieldDesc.isAccessible())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }


    // UPDATE methods:

    public static List<sObject> upd(List<sObject> pObjs, List<sObjectField> pFields, boolean checkFls) {
        if (pObjs != NULL && !pObjs.isEmpty()) {
            if (checkFls) {
                checkOperation(pObjs, Operation.Updating);
            }

            update pObjs;
        }

        return pObjs;
    }

    public static List<sObject> upd(List<sObject> pObjs) {
        return upd(pObjs, new List<sObjectField>(), true);
    }

    //Tejas-Patel 12-Apr-2017; Function call with sObject Fields and FLS Check
    // Security scan FALSE-POSITIVE: the db method is called with one object and returns one object.
    // The bulkifying alert is not relevant here as it is not being called in a loop.
    public static sObject upd(sObject pObj, List<sObjectField> pFields, boolean checkFls) {
        system.assertNotEquals(null, pObj);
        return upd(new List<sObject> {pObj}, pFields, checkFls)[0];
    }

    // Security scan FALSE-POSITIVE: the db method is called with one object and returns one object.
    // The bulkifying alert is not relevant here as it is not being called in a loop.
    public static sObject upd(sObject pObj) {
        return upd(pObj, new List<sObjectField>(), true);
    }

    //Tejas-Patel 12-Apr-2017; Function call with sObject and FLS check
    // Security scan FALSE-POSITIVE: the db method is called with one object and returns one object.
    // The bulkifying alert is not relevant here as it is not being called in a loop.
    public static sObject upd(sObject pObj, boolean checkFls) {
        return upd(pObj, new List<sObjectField>(), checkFls);
    }

    //Tejas-Patel 12-Apr-2017; Function call with sObject and Fields but not FLS check
    // Security scan FALSE-POSITIVE: the db method is called with one object and returns one object.
    // The bulkifying alert is not relevant here as it is not being called in a loop.
    public static sObject upd(sObject pObj, List<sObjectField> pFields) {
        return upd(pObj, pFields, true);
    }

    public static List<sObject> upd(List<sObject> pObjs, List<sObjectField> pFields) {
        return upd(pObjs, pFields, true);
    }

    public static List<sObject> upd(List<sObject> pObjs, boolean checkFls) {
        return upd(pObjs, new List<sObjectField>(), checkFls);
    }

    // UPSERT methods:

    public static List<sObject> ups(List<sObject> pObjs, List<sObjectField> pFields, boolean checkFls) {
        List<SObject> toIns = new List<SObject>();
        List<SObject> toUpd = new List<SObject>();
        for (sObject obj : pObjs) {
            if (obj.id == null) {
                toIns.add(obj);
            } else {
                toUpd.add(obj);
            }
        }
        // Security scan FALSE-POSITIVE: the scan show bulkifying issue, however there no individual inserts here.
        // Inserts and updates are done separately so only 2 DML statements are executed.
        ins(toIns, pFields, checkFls);
        upd(toUpd, pFields, checkFls);

        pObjs.clear();
        pObjs.addAll(toIns);
        pObjs.addAll(toUpd);

        return pObjs;
    }

    public static List<sObject> ups(List<sObject> pObjs, List<sObjectField> pFields, boolean checkFls, boolean bUpdateFirst) {
        List<SObject> toIns = new List<SObject>();
        List<SObject> toUpd = new List<SObject>();
        for (sObject obj : pObjs) {
            if (obj.id == null) {
                toIns.add(obj);
            } else {
                toUpd.add(obj);
            }
        }
        // Security scan FALSE-POSITIVE: the scan show bulkifying issue, however there no individual inserts here.
        // Inserts and updates are done separately so only 2 DML statements are executed.
        upd(toUpd, pFields, checkFls);
        ins(toIns, pFields, checkFls);

        pObjs.clear();
        pObjs.addAll(toIns);
        pObjs.addAll(toUpd);

        return pObjs;
    }

    public static List<sObject> ups(List<sObject> pObjs) {
        return ups(pObjs, new List<sObjectField>(), true);
    }

    public static List<sObject> ups(List<sObject> pObjs, boolean checkFls) {
        return ups(pObjs, new List<sObjectField>(), checkFls);
    }

    // Security scan FALSE-POSITIVE: the db method is called with one object and returns one object.
    // The bulkifying alert is not relevant here as it is not being called in a loop.
    public static sObject ups(sObject pObj, List<sObjectField> pFields) {
        system.assertNotEquals(null, pObj);
        return ups(new List<sObject> {pObj}, pFields, true)[0];
    }

    // Security scan FALSE-POSITIVE: the db method is called with one object and returns one object.
    // The bulkifying alert is not relevant here as it is not being called in a loop.
    public static sObject ups(sObject pObj) {
        return ups(pObj, new List<sObjectField>());
    }

    public static List<sObject> ups(List<sObject> pObjs, List<sObjectField> pFields) {
        return ups(pObjs, pFields, true);
    }

    // DELETE methods:
    public static List<sObject> del(List<sObject> pObjs, boolean checkFls) {
        if (pObjs != NULL && !pObjs.isEmpty()) {
            if (checkFls) {
                checkOperation(pObjs, Operation.Deleting);
            }

            delete pObjs;
        }

        return pObjs;
    }

    // Security scan FALSE-POSITIVE: the db method is called with one object and returns one object.
    // The bulkifying alert is not relevant here as it is not being called in a loop.
    public static sObject del(sObject pObj) {
        del(new List<sObject> {pObj}, true);
        return pObj;
    }
}
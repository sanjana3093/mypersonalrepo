/**
* @author Deloitte
* @date 11-June-2018
*
* @description Controller class for Infusion Details screen
*/

public class spc_InfusionDetailsController {

    /*******************************************************************************************************
    * @description Method to retrieve Activities for the week
    * @param caseId String
    * @return Case record
    */
    @AuraEnabled
    public static Case checkConsentDetails(String caseId) {
        if (String.isNotBlank(caseId)) {
            spc_PatientConsentDetails consentDetails = spc_PatientConsentDetails.getInstance(caseId);
            if (consentDetails != null && consentDetails.objCase != null) {
                return consentDetails.objCase;
            }
        }
        return null;
    }
}
/********************************************************************************************************
*  @author          Deloitte
*  @description     Utility clas to make API callouts
*  @param
*  @date            Aug 18 , 2020
*********************************************************************************************************/

public with sharing class CCL_CapacityApi_Utility {
 
     /*** String to store Access token */
 
    /********************************************************************************************************
*  @author          Deloitte
*  @description     method to get access token
*  @param
*  @date            Aug 18 , 2020
*********************************************************************************************************/

    public Static String getAccessToken() {
        string accessToken;
        final CCL_CapacityAPI_Data__c capacityData = CCL_CapacityAPI_Data__c.getOrgDefaults();
        final string c_clientID = capacityData.ClientID__c;
        final string c_clientSecret = capacityData.ClientSecret__c;
        final Blob userPass = Blob.valueOf(c_clientID + ':' + c_clientSecret);
        final Http http = new Http();
        final HttpRequest request = new HttpRequest();
        request.setEndpoint('callout:CCL_GETACCESSTOKEN');
        request.setMethod('POST');
        final String body = 'grant_type=client_credentials';
        final String authHeader = 'Basic ' + EncodingUtil.base64Encode(userPass);
        request.setHeader('Authorization', authHeader);
        request.setBody(body);
        final HttpResponse response = http.send(request);
        if (response.getStatus() == 'OK') {
        final String jsonString = response.getBody();
        final System.JSONParser parser = JSON.createParser(jsonString);
        while (parser.nextToken() != null) {
            if(parser.getText() == 'access_token') {
                parser.nextToken();
                accessToken = parser.getText();
            }
        }
        } else {
            accessToken = null;
        }
        return accessToken;
    }
    
      /********************************************************************************************************
*  @author          Deloitte
*  @description     method to get access token
*  @param
*  @date            Aug 18 , 2020
*********************************************************************************************************/
    
    public static String getApiResponse(String accessToken,String therapy,String subjectID,String pickupSiteID,STring dropOffSiteID,String nvsId) {
        String apiResponse;
            
        final CCL_CapacityAPI_Data__c capacityEndpointURL = CCL_CapacityAPI_Data__c.getInstance();
        final string resourceParams = 'Therapy='+therapy+'&Subject_ID='+subjectID+'&Pickup_Site_ID='+pickupSiteID+'&DropOff_Site_ID='+dropOffSiteID+'&NVS_ID='+NVSID;
        System.debug('resourceParams' +resourceParams);
        final Http http = new Http();
        final HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:CCL_Capacity_Callout?'+resourceParams);
        req.setTimeout(30000);
        req.setMethod('GET');
        req.setHeader('Authorization', 'Bearer '+accessToken);
        req.setHeader('Content-Type', 'application/json');
        final HttpResponse resp = http.send(req);
        final String responseStatus = resp.getStatus();
        if (responseStatus == 'OK') {
        apiResponse = resp.getBody();
        if(System.now() > capacityEndpointURL.AccessToken_Validity__c || capacityEndpointURL.AccessToken_Validity__c==null ) {
        updateCustomSetting(accessToken);
        }
        } else {
            apiResponse = null;
        }
        return apiResponse;
    }
    
   
    /********************************************************************************************************
*  @author          Deloitte
*  @description     method to insert token in custom setting
*  @param
*  @date            june 22, 2020
*********************************************************************************************************/
    
     @future
        public static void updateCustomSetting(String accessToken) {
            final CCL_CapacityAPI_Data__c custSetting = CCL_CapacityAPI_Data__c.getOrgDefaults(); 
            custSetting.AccessToken__c = accessToken;
            custSetting.AccessToken_Validity__c = System.now().addMinutes(50);
			Database.upsert(custSetting);
            //upsert custSetting;
        }
}
/********************************************************************************************************
    *  @author          Deloitte
    *  @description     Processor class for Caregiver Information Enrollment Wizard Page
    *  @date            19-June-2018
    *  @version         1.0
    *
    *********************************************************************************************************/
global with sharing class spc_CaregiverInformationProcessor  implements PatientConnect.PC_EnrollmentWizard.PageProcessor {

    /********************************************************************************************************
     *  @author           Deloitte
     *  @date             19/06/2018
     *  @description      Implemented method from PC_EnrollmentWizard.PageProcessor
     *  @param            enrollmentCase - Case Object
     *  @param            pageState - Map with field values from page
     *  @return           None
     *********************************************************************************************************/
    public static void processEnrollment(Case enrollmentCase, Map<String, Object> pageState) {

        Account caregiverLinked;
        Boolean caregiverToBeUpdated = false;
        ID programId = enrollmentCase.PatientConnect__PC_Program__c;
        if (pageState != null ) {
            if (pageState.get('caregiverAccount') != null) {
                for (String key : pageState.keySet()) {
                    if (key == 'caregiverAccount') {

                        if (programId != null) {
                            Object persistentAccount = (Object) pageState.get('caregiverAccount');
                            Map<String, Object> persistedAccountMap = (Map<String, Object>) persistentAccount;
                            Account acc = new Account();
                            if (String.isNotBlank((String) persistedAccountMap.get('PatientConnect__PC_Caregiver_First_Name__c')) && String.isNotBlank((String) persistedAccountMap.get('PatientConnect__PC_Caregiver_Last_Name__c'))) {
                                acc.PatientConnect__PC_First_Name__c     = (String) (persistedAccountMap.get('PatientConnect__PC_Caregiver_First_Name__c'));
                                acc.PatientConnect__PC_Last_Name__c = (String) (persistedAccountMap.get('PatientConnect__PC_Caregiver_Last_Name__c'));
                                acc.Name = (String) (persistedAccountMap.get('PatientConnect__PC_Caregiver_Last_Name__c')) + ' , ' + (String) (persistedAccountMap.get('PatientConnect__PC_Caregiver_First_Name__c'));
                            }
                            acc.RecordTypeId = spc_ApexConstants.ID_CAREGIVER_ACCOUNT_RECORDTYPE;
                            acc.PatientConnect__PC_Individual_Type__c = spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.INDIVIDUAL_TYPE_INDIVIDUAL);

                            if (String.isNotBlank((String) persistedAccountMap.get('spc_Caregiver_Date_of_Birth__c'))) {
                                acc.PatientConnect__PC_Date_of_Birth__c = (Date.valueOf(String.valueOf(persistedAccountMap.get('spc_Caregiver_Date_of_Birth__c'))));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('spc_Caregiver_Gender__c'))) {
                                acc.PatientConnect__PC_Gender__c = (String) (persistedAccountMap.get('spc_Caregiver_Gender__c'));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('PatientConnect__PC_Email__c'))) {
                                acc.PatientConnect__PC_Email__c = (String) (persistedAccountMap.get('PatientConnect__PC_Email__c'));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('Phone'))) {
                                acc.spc_Mobile_Phone__c = FormatPhone((String) (persistedAccountMap.get('Phone')));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('spc_Home_Phone__c'))) {
                                acc.spc_Home_Phone__c = FormatPhone((String) (persistedAccountMap.get('spc_Home_Phone__c')));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('spc_Office_Phone__c'))) {
                                acc.spc_Office_Phone__c = FormatPhone((String) (persistedAccountMap.get('spc_Office_Phone__c')));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('PatientConnect__PC_Communication_Language__c'))) {
                                acc.PatientConnect__PC_Communication_Language__c = (String) (persistedAccountMap.get('PatientConnect__PC_Communication_Language__c'));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('spc_Caregiver_Preferred_Time_to_Contact__c'))) {
                                acc.spc_Preferred_Time_to_Contact__c = (String) (persistedAccountMap.get('spc_Caregiver_Preferred_Time_to_Contact__c'));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('PatientConnect__PC_Caregiver_Relationship_to_Patient'))) {
                                acc.PatientConnect__PC_Caregiver_Relationship_to_Patient__c = (String) (persistedAccountMap.get('PatientConnect__PC_Caregiver_Relationship_to_Patient'));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('spc_Permission_to_Call__c'))) {
                                acc.spc_Permission_to_Call__c = (String) (persistedAccountMap.get('spc_Permission_to_Call__c'));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('spc_Permission_to_Email__c'))) {
                                acc.spc_Permission_to_Email__c = (String) (persistedAccountMap.get('spc_Permission_to_Email__c'));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('spc_Permission_from_Patient_to_Share_PHI__c'))) {
                                acc.spc_Permission_from_Patient_to_Share_PHI__c = (String) (persistedAccountMap.get('spc_Permission_from_Patient_to_Share_PHI__c'));
                            }
                            if (persistedAccountMap.get('spc_Date_Permission_to_Share_PHI__c') != NULL && persistedAccountMap.get('spc_Date_Permission_to_Share_PHI__c') != '') {
                                acc.spc_Date_Permission_to_Share_PHI__c =  (Date.valueOf(String.valueOf(persistedAccountMap.get('spc_Date_Permission_to_Share_PHI__c'))));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('PatientConnect__PC_Primary_Address__c'))) {
                                acc.PatientConnect__PC_Primary_Address__c = (String) (persistedAccountMap.get('PatientConnect__PC_Primary_Address__c'));
                            }
                            if (String.isNotBlank((String) persistedAccountMap.get('notes'))) {
                                acc.spc_Notes__c = (String) (persistedAccountMap.get('notes'));
                            }
                            acc.PatientConnect__PC_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_STATUS_ACTIVE);
                            acc.Type = spc_ApexConstants.getRecordTypeName(spc_ApexConstants.RecordTypeName.ACCOUNT_RT_CAREGIVER);
                            if (String.isNotBlank((String) persistedAccountMap.get('caregiverLookUpId'))) {
                                acc.id = (String) persistedAccountMap.get('caregiverLookUpId');
                                caregiverToBeUpdated = true;
                                caregiverLinked = (Account) spc_Database.upd(acc);
                            } else if (String.isBlank((String) persistedAccountMap.get('caregiverLookUpId')) && String.isNotBlank(acc.PatientConnect__PC_First_Name__c) && String.isNotBlank(acc.PatientConnect__PC_Last_Name__c)) {
                                caregiverLinked = (Account) spc_Database.ins(acc);
                            }
                            if (caregiverLinked != null ) {
                                Case caseProgram = new Case(Id = programId, spc_Caregiver__c =  caregiverLinked.id);
                                spc_Database.upd(caseProgram);
                            }
                        } else {
                            throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException(Label.Caregiver_Program_Id_Not_Found);
                        }
                    }
                }
                //Creating Address for patient account
                if (pageState.get('Address') != null) {
                    Object addressPersisted = (Object) pageState.get('Address');
                    Map<String, Object> persistentAddress = (Map<String, Object>) addressPersisted;

                    String addrID;

                    PatientConnect__PC_Address__c addr;
                    if ((String) (persistentAddress.get('addressType')) != '' && (String.isNotBlank((String) persistentAddress.get('PatientConnect__PC_Address_1__c')) || String.isNotBlank((String) persistentAddress.get('PatientConnect__PC_Address_2__c'))
                            || String.isNotBlank((String) persistentAddress.get('PatientConnect__PC_Address_3__c')) || String.isNotBlank((String) persistentAddress.get('PatientConnect__PC_City__c'))
                            || String.isNotBlank((String) persistentAddress.get('PatientConnect__PC_City__c')) || String.isNotBlank((String) persistentAddress.get('PatientConnect__PC_Country__c'))
                            || String.isNotBlank((String) persistentAddress.get('PatientConnect__PC_State__c')) || String.isNotBlank((String) persistentAddress.get('PatientConnect__PC_Zip_Code__c')))) {
                        addr = new PatientConnect__PC_Address__c();

                        if (caregiverLinked != null ) {
                            addr.PatientConnect__PC_Account__c = caregiverLinked.Id;
                            addr.PatientConnect__PC_Address_1__c = (String) (persistentAddress.get('PatientConnect__PC_Address_1__c'));
                            addr.PatientConnect__PC_Address_2__c = (String) (persistentAddress.get('PatientConnect__PC_Address_2__c'));
                            addr.PatientConnect__PC_Address_3__c = (String) (persistentAddress.get('PatientConnect__PC_Address_3__c'));
                            //Add conditions to check blank values before assigning to picklists
                            if ((String) (persistentAddress.get('addressType')) != '') {
                                addr.PatientConnect__PC_Address_Description__c = (String) (persistentAddress.get('addressType'));
                            }
                            addr.PatientConnect__PC_City__c = (String) (persistentAddress.get('PatientConnect__PC_City__c'));
                            addr.PatientConnect__PC_Country__c = (String) (persistentAddress.get('PatientConnect__PC_Country__c'));
                            addr.PatientConnect__PC_State__c = (String) (persistentAddress.get('PatientConnect__PC_State__c'));
                            //Add conditions to check blank values before assigning to picklists
                            addr.PatientConnect__PC_Status__c = (String) (persistentAddress.get('status'));
                            addr.PatientConnect__PC_Zip_Code__c = (String) (persistentAddress.get('PatientConnect__PC_Zip_Code__c'));
                            boolean addressPrimary = true;

                            addr.PatientConnect__PC_Primary_Address__c =  addressPrimary;
                            if (caregiverToBeUpdated && String.isNotBlank((String) persistentAddress.get('id'))) {
                                addr.id = (String) persistentAddress.get('id');
                                spc_Database.upd(addr);
                            } else {
                                if (null != addr.PatientConnect__PC_Account__c ) {
                                    spc_Database.ins(addr);
                                }
                                if (addr.id != null) {
                                    Account acc = new Account(Id = caregiverLinked.id, PatientConnect__PC_Primary_Address__c =  addr.id);
                                    spc_Database.upd(acc);
                                }
                            }
                        }

                    }
                }
            } else {
                throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException(Label.Missing_Caregiver_Information);
            }
        } else {
            throw new PatientConnect.PC_EnrollmentWizard.PageProcessorException(Label.Missing_Page_State);
        }


    }
    private static String FormatPhone(String Phone) {
        string nondigits = '[^0-9]';
        string PhoneDigits;

        // remove all non numeric
        PhoneDigits = Phone.replaceAll(nondigits, '');

        // 10 digit: reformat with dashes
        if (PhoneDigits.length() == 10)
            return '(' + PhoneDigits.substring(0, 3) + ')' + '-' +
                   PhoneDigits.substring(3, 6) + '-' +
                   PhoneDigits.substring(6, 10);
        // 11 digit: if starts with 1, format as 10 digit
        if (PhoneDigits.length() == 11) {
            if (PhoneDigits.substring(0, 1) == '1') {
                return '(' + PhoneDigits.substring(1, 4) + ')' + '-' +
                       PhoneDigits.substring(4, 7) + '-' +
                       PhoneDigits.substring(7, 11);

            }
        }

        // if it isn't a 10 or 11 digit number, return the original because
        // it may contain an extension or special information
        return ( Phone );
    }
}

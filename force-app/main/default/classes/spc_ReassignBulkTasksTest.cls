/********************************************************************************************************
    *  @author          Deloitte
    *  @description     This is the test class for spc_ReassignBulkTasks
    *  @date            06/14/2018
    *  @version         1.0
    *
    *  Modification Log:
    * -------------------------------------------------------------------------------------------------------
    *  Developer                Date                                    Description
    * -------------------------------------------------------------------------------------------------------
    *  Deloitte                 09/20/2018                          Initial version    
*********************************************************************************************************/
@isTest
public class spc_ReassignBulkTasksTest {
   
    @testsetup
    static void setup() {
        
       
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User objUser = new User(Alias = 'test', email = 'TestSysAdmin@test123.com',emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                                localesidkey = 'en_US',country = 'United States', profileid = p.Id,
                                timezonesidkey = 'America/Los_Angeles', username = System.currentTimeMillis()+'TestSysAdmin@test123.com');
        UserRole oUserRole1=[select id, name from UserRole where name='Case Manager' LIMIT 1];
        objUser.userRoleId=oUserRole1.id;
        spc_Database.ins(objUser);
        User objUser1 = new User(Alias = 'test1', email = 'TestSysAdmin@test1234.com',emailencodingkey = 'UTF-8', lastname = 'Testing123', languagelocalekey = 'en_US',
                                localesidkey = 'en_US',country = 'United States', profileid = p.Id,
                                timezonesidkey = 'America/Los_Angeles', username = System.currentTimeMillis()+'TestSysAdmin@test1234.com');
        objUser1.userRoleId=oUserRole1.id;
        spc_Database.ins(objUser1);
       
     
    
    }
     public Static void dataTest(){
        List<spc_ApexConstantsSetting__c>  apexConstansts =  spc_Test_Setup.setDataforApexConstants();
         insert apexConstansts;
         
         User objUser=[select id from User where lastname='Testing' LIMIT 1];
         User objUser1=[select id from User where lastname='Testing123' LIMIT 1];
          //Create a patient account
        Account acc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId());
        acc.PatientConnect__PC_Email__c = 'test@email.com';
        insert acc;
        //Create a HCO account
        Account hcoAcc = spc_Test_Setup.createAccount(Schema.SObjectType.Account.getRecordTypeInfosByName().get('HCO').getRecordTypeId());
        insert hcoAcc;
        //Create a program case
        Case programCaseRec2 = spc_Test_Setup.createCases(new List<Account>{acc},1,'PC_Program').get(0);
        if(null!=programCaseRec2){
        programCaseRec2.Type = 'Program';
        programCaseRec2.OwnerId = objUser.Id;
        insert programCaseRec2;
        }
        
        Case programCaseRec3 = spc_Test_Setup.createCases(new List<Account>{acc},1,'PC_Program').get(0);
        if(null!=programCaseRec3){
        programCaseRec3.Type = 'Program';
        programCaseRec3.OwnerId = objUser1.Id;
        insert programCaseRec3;
        }
        List<Task> lstTasks=new List<Task>();
        // Task 
        Task tk=spc_Test_Setup.createTask('Not Started',programCaseRec2.id,'Normal','test123','Task',system.today());
        tk.ownerID=objUser.id;
        
        Task tk1=spc_Test_Setup.createTask('Not Started',programCaseRec2.id,'Normal','test1234','Task',system.today());
        tk1.ownerId=objUser.id;
        Task tk2=spc_Test_Setup.createTask('Not Started',programCaseRec2.id,'Normal','test12345','Task',system.today());
        tk2.ownerId=objUser1.id;
        lstTasks.add(tk);
        lstTasks.add(tk1);
        lstTasks.add(tk2);
        spc_Database.ins(lstTasks);
        spc_ReassignBulkTasks.ReassignTasksWrapper rbt=new spc_ReassignBulkTasks.ReassignTasksWrapper(tk2);
         
     }
    public static testMethod void testExecution1(){
       dataTest();
       User objUser=[select id,name from User where lastname='Testing' LIMIT 1];
       User objUser1=[select id,name from User where lastname='Testing123' LIMIT 1];
       List<Id> lstId=new List<Id>();
       List<Task> lstTasks=[select id from Task  ];
       for( Task tk  : lstTasks){
           lstId.add(tk.id);
       }
       test.startTest();
       spc_ReassignBulkTasks.fetchLoggedInUser();
       spc_ReassignBulkTasks.fetchCaseManagerList1();
       spc_ReassignBulkTasks.getCaseManagerList2(objUser.name);
       spc_ReassignBulkTasks.getAllTasksOfPreviousAssignee(objUser.name,'subject',true);
       spc_ReassignBulkTasks.getCaseManagerOwnereId(objUser.name);
       spc_ReassignBulkTasks.reassignTasks(lstId,objUser1.name);
       test.stopTest();
       
    }
   }
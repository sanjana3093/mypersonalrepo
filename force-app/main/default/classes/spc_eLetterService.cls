/*********************************************************************************************************

   * @author         Deloitte
   * @date           16-June-16
   * @description    Methods for eLetter service for sending out email and fax communication

 **/
public without sharing class spc_eLetterService {

    private Set<Id> patientAccountIds = new Set<Id>();
    private Map<Id, Id> patientToObjectId = new Map<Id, Id>();
    private static final String EMPTY_STRING = '';
    private static spc_Comm_Frmwrk_Testing_Parameters__c emailtestParam = spc_Comm_Frmwrk_Testing_Parameters__c.getOrgDefaults();


    /*********************************************************************************

    * @description     Based on the eLetterId get a ELetter
    @return    PatientConnect__PC_eLetter__c
    @param       eLetterId Id of the ELetter
    *********************************************************************************/
    public static PatientConnect__PC_eLetter__c getELetter(Id eLetterId) {
        List<String> fields = new List<String> {
            'Name',
            'PatientConnect__PC_Target_Recipients__c',
            'PatientConnect__PC_Communication_Language__c',
            'PatientConnect__PC_Template_Name__c',
            'PatientConnect__PC_Allow_Send_to_Patient__c',
            'PatientConnect__PC_Send_to_User__c'
        };
        spc_database.assertAccess('PatientConnect__PC_eLetter__c', spc_database.Operation.Reading, fields);

        List<PatientConnect__PC_eLetter__c> eLetters = [
                    SELECT Id, Name, PatientConnect__PC_Target_Recipients__c, PatientConnect__PC_Communication_Language__c, PatientConnect__PC_Template_Name__c,
                    PatientConnect__PC_Allow_Send_to_Patient__c, PatientConnect__PC_Send_to_User__c, PatientConnect__PC_Object_Record_Types__c
                    FROM PatientConnect__PC_eLetter__c
                    WHERE Id = :eLetterId LIMIT 1
                ];

        if (!eLetters.isEmpty()) return eLetters[0];
        return null;
    }

    /*********************************************************************************

    * @description    : Based on the eLetterName get the relevant Participants for patient(s) for a single patient
    * @return    void
    *  @param       1. caseId : Id of the Patient 2. eLetter: eLetter record details

    *********************************************************************************/
    public static List<spc_eLetter_Recipient> getParticipants(Id caseId, PatientConnect__PC_eLetter__c eLetter, boolean isFaxSpecific) {
        // start from here also one more method // C1
        List<Case> Enquirycases = [ SELECT Id, AccountId, spc_Is_Enquiry_Case__c, spc_is_Pretreatment_Case__c, spc_is_PostTreatment_Case__c FROM Case where Id = : caseId ];
        if (!Enquirycases.isEmpty()) {
            if (Enquirycases[0].spc_Is_Enquiry_Case__c ) {
                List<spc_eLetter_Recipient> recipients  = getParticipantsForEnquiryCase(Enquirycases[0], eLetter);
                return recipients;
            }
        }
        // must be removed
        Map<Id, List<spc_eLetter_Recipient>> recipients = getParticipants(new Set<Id> { caseId }, eLetter, isFaxSpecific);
        if (recipients == null || recipients.size() == 0) {
            return null;
        }

        return recipients.values().get(0);
    }

    /*********************************************************************************

    * @description     Based on the eLetterName get the relevant Participants for patient(s)
    * @return     void
    * @param       1. caseIds : Number of Patients for whom the related recipients are required
    *              2. eLetter: eLetter record details
    *********************************************************************************/
    public static Map<Id, List<spc_eLetter_Recipient>> getParticipants(Set<Id> caseIds, PatientConnect__PC_eLetter__c eLetter, boolean isFaxSpecific) {

        // Get available languages from the eLetter Template and dynamically display the recipient language:
        Set<String> languages = new Set<String> ();
        Set<String> cchannels = new Set<String> ();
        languages.addAll(eLetter.PatientConnect__PC_Communication_Language__c.split(';'));
        Set<Id> programIds = new Set<Id>();
        Set<Id> engProgramIds = new Set<Id>();
        try {
            //add either to program or program case
            List<String> fields = new List<String> {'PatientConnect__PC_Program__c', 'PatientConnect__PC_Is_Program_Case__c'};
            spc_database.assertAccess('Case', spc_database.Operation.Reading, fields);
            for (Case objCase : [SELECT Id, PatientConnect__PC_Program__c, spc_Is_Referral_Case__c, spc_Is_Certification_Case__c, PatientConnect__PC_Engagement_Program__c, PatientConnect__PC_Is_Program_Case__c, spc_Is_Enquiry_Case__c FROM Case where Id in :caseIds]) {
                Id programID = objCase.PatientConnect__PC_Is_Program_Case__c ? objCase.Id : objCase.PatientConnect__PC_Program__c;
                if (objCase.spc_Is_Referral_Case__c) {
                    programID = objCase.spc_Is_Referral_Case__c ? objCase.Id : objCase.PatientConnect__PC_Program__c;
                }
                if (objCase.spc_Is_Certification_Case__c) {
                    programID = objCase.spc_Is_Certification_Case__c ? objCase.Id : objCase.PatientConnect__PC_Program__c;
                }
                if (!programIds.contains(programID)) {
                    programIds.add(programID);
                    engProgramIds.add(objCase.PatientConnect__PC_Engagement_Program__c);
                }
            }
            if (!engProgramIds.isEmpty()) {
                Map<ID, Set<String>> allMapLinks = null;
                if (isFaxSpecific) {
                    allMapLinks = getMapBetweenFaxAndCchannels(engProgramIds);
                } else {
                    allMapLinks = getMapBetweenEletterAndCchannels(engProgramIds);
                }

                if (!allMapLinks.isEmpty()) {
                    cchannels = allMapLinks.get(eLetter.ID);
                }
            }

            fields = new List<String> {'PatientConnect__PC_Account__c', 'PatientConnect__PC_Program__c'};
            spc_database.assertAccess('PatientConnect__PC_Association__c', spc_database.Operation.Reading, fields);
            // Fetch all the Participants related to the patient (ACTIVE ones only) and Physician location details (as for a physician the location is explicitly selected)
            Map<Id, List<spc_eLetter_Recipient>> recipients = new Map<Id, List<spc_eLetter_Recipient>> ();

            if (eLetter.PatientConnect__PC_Target_Recipients__c != null) {
				//creating a map of role picklist values to use the label as recipient type
				Map<String,String> mapRoles = new Map<String,String>();
				for (Schema.PicklistEntry role : PatientConnect__PC_Association__c.PatientConnect__PC_Role__c.getDescribe().getPicklistValues()){
					mapRoles.put(role.getValue(), role.getLabel());
				}
				
                List<PatientConnect__PC_Association__c> patientParticipants =
                    [SELECT Id, PatientConnect__PC_Account__c, PatientConnect__PC_Account__r.PatientConnect__PC_Communication_Language__c, PatientConnect__PC_Account__r.Type, PatientConnect__PC_Account__r.Name,
                     PatientConnect__PC_Account__r.PatientConnect__PC_Email__c, spc_Fax__c, PatientConnect__PC_Program__c, PatientConnect__PC_Role__c, PatientConnect__PC_Account__r.PatientConnect__PC_Status__c , PatientConnect__PC_Account__r.Fax
                     FROM PatientConnect__PC_Association__c WHERE PatientConnect__PC_Program__c in :programIds AND PatientConnect__PC_AssociationStatus__c = 'Active' AND PatientConnect__PC_Account__r.PatientConnect__PC_Status__c  = 'Active' AND PatientConnect__PC_Role__c IN :eLetter.PatientConnect__PC_Target_Recipients__c.split(';')];
                Account recipientAccount;
                for (PatientConnect__PC_Association__c pp : patientParticipants) {
                    if (pp.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_TREATING_PHYSICIAN))
                            || pp.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_REFERRING_PHYSICIAN))
                            || pp.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ACCOUNT_RT_HCO))
                            || pp.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_SPHARMACY))
                            || pp.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.ASSOCIATION_ROLE_PAYER))
                            || pp.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_EXT_COMP_PHARMACY))
                            || pp.PatientConnect__PC_Role__c.equalsIgnoreCase(spc_ApexConstants.getAdditionalPicklistValues(spc_ApexConstants.AdditionalPicklistValues.ASSOCIATION_ROLE_DESIGNATED_CAREGIVER))) {

                        recipientAccount = new Account(
                            Id = pp.PatientConnect__PC_Account__c,
                            PatientConnect__PC_Communication_Language__c = pp.PatientConnect__PC_Account__r.PatientConnect__PC_Communication_Language__c,
                            PatientConnect__PC_Email__c = pp.PatientConnect__PC_Account__r.PatientConnect__PC_Email__c,
                            Fax =  pp.spc_Fax__c,
                            Name = pp.PatientConnect__PC_Account__r.Name,
                            Type = pp.PatientConnect__PC_Role__c);
                    } else {
                        recipientAccount = new Account(
                            Id = pp.PatientConnect__PC_Account__c,
                            PatientConnect__PC_Communication_Language__c = pp.PatientConnect__PC_Account__r.PatientConnect__PC_Communication_Language__c,
                            PatientConnect__PC_Email__c = pp.PatientConnect__PC_Account__r.PatientConnect__PC_Email__c,
                            Fax =  pp.PatientConnect__PC_Account__r.Fax,
                            Name = pp.PatientConnect__PC_Account__r.Name,
                            Type = pp.PatientConnect__PC_Role__c);
                    }
                    // Create the new recipient record with given parameters
                    spc_eLetter_Recipient recipient = new spc_eLetter_Recipient(recipientAccount, mapRoles.get(pp.PatientConnect__PC_Role__c), languages, cchannels);

                    if (!recipients.containsKey(pp.PatientConnect__PC_Program__c)) {
                        recipients.put(pp.PatientConnect__PC_Program__c, new List<spc_eLetter_Recipient>());
                    }

                    // Add the recipient wrapper to the list of recipients
                    recipients.get(pp.PatientConnect__PC_Program__c).add(recipient);
                }
            }

            if (eLetter.PatientConnect__PC_Allow_Send_to_Patient__c) {
                fields = new List<String> {'Type', 'PatientConnect__PC_Communication_Language__c', 'PatientConnect__PC_Email__c', 'Name'};
                spc_database.assertAccess('Account', spc_database.Operation.Reading, fields);
                for (Case objCase : [SELECT Id, Account.Fax, AccountId, Account.PatientConnect__PC_Communication_Language__c, Account.Type, Account.PatientConnect__PC_Email__c, Account.Name FROM Case WHERE Id IN :programIds]) {
                    Account recipientAccount = new Account(
                        Id = objCase.AccountId,
                        PatientConnect__PC_Communication_Language__c = objCase.Account.PatientConnect__PC_Communication_Language__c,
                        Type = objCase.Account.Type,
                        PatientConnect__PC_Email__c = objCase.Account.PatientConnect__PC_Email__c,
                        Fax = objCase.Account.Fax,
                        Name = objCase.Account.Name);
                    // Create the new recipient record with given parameters
                    spc_eLetter_Recipient recipient = new spc_eLetter_Recipient(recipientAccount, recipientAccount.Type, languages, cchannels);

                    if (!recipients.containsKey(objCase.Id)) {
                        recipients.put(objCase.Id, new List<spc_eLetter_Recipient>());
                    }

                    // Add the recipient wrapper to the list of recipients
                    recipients.get(objCase.Id).add(recipient);
                }
            }

            return recipients;
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
            return null;
        }
    }

    /*********************************************************************************

    * @description    : Based on the eLetterName get the relevant Participants for patient(s)
    * @return    void
    * @param       1. caseIds : Number of Patients for whom the related recipients are required
    * 2. eLetter: eLetter record details
    * 3. loadedParticipants : Has some value only when invoked from eLetter page where recipients and their language are dyanmically
    * selected at page level. So it won't be applicable for all related recipients
       *********************************************************************************/
    public Map<Id, List<Account>> getRelatedRecipients(List<Account> recipients) {
        // Fetch the eLetter details: The types of Participants to which the eLetters can be sent

        Map<Id, List<Account>> patientToRecipients = new Map<Id, List<Account>> ();
        //if loadedParticipants is not null ; i.e the list has been overwritten, and can be added directly for the Patient (single patient from eLetter page)
        if (recipients != null) {
            for (Id patient : patientAccountIds) {
                patientToRecipients.put(patient, recipients);
            }
        }

        return patientToRecipients;
    }

    /*********************************************************************************

    * @description     Create document for a given set of Participants and return the formatted map
    * @return    void
    * @param      : 1. patientToAccounts : Map holding the particiapnts related to a single Patient
    * 2. patientToRecordId:  Map holding the Patient Id and the record Id for which the document will be created
    *  3. eLetter: eLetter record details
    *********************************************************************************/
    public List<spc_eLetter.DocumentWrapper> createDocuments(Map<Id, List<Account>> patientToRecipients, PatientConnect__PC_eLetter__c eLetter, Id recordTypeId, Id objectID) {
        // Create the document records for all the relevant recipients:
        List<PatientConnect__PC_Document__c> documents = new List<PatientConnect__PC_Document__c>();
        List<spc_eLetter.DocumentWrapper> wrappers = new List<spc_eLetter.DocumentWrapper>();
        List<Case> casesList = [select id, PatientConnect__PC_Engagement_Program__c, PatientConnect__PC_Program__c, PatientConnect__PC_Is_Program_Case__c, ownerID from case where id = : objectID];
        List<PatientConnect__PC_Engagement_Program_Eletter__c> eProgrameLetters = [ SELECT ID, spc_PMRC_Code__c, spc_Channel__c, PatientConnect__PC_Engagement_Program__c, PatientConnect__PC_Engagement_Program__r.PatientConnect__PC_Program_Code__c  FROM PatientConnect__PC_Engagement_Program_Eletter__c
                WHERE PatientConnect__PC_eLetter__c = : eLetter.Id];

        PatientConnect__PC_Document__c doc;
        String PMRCCode = EMPTY_STRING;
        String status = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_STATUS_READYTOSEND);
        String ProgramCode = EMPTY_STRING;
        String channel = EMPTY_STRING;
        if (!eProgrameLetters.isEmpty() && !String.isBlank(eProgrameLetters[0].spc_PMRC_Code__c)) {
            PMRCCode =  spc_Utility.getActualPMRCCode(eProgrameLetters[0].spc_PMRC_Code__c);
            ProgramCode = eProgrameLetters[0].PatientConnect__PC_Engagement_Program__r.PatientConnect__PC_Program_Code__c;
            channel = eProgrameLetters[0].spc_Channel__c;
            if (channel == 'Email' && !verifyValidAndActiveTemplate(eLetter.PatientConnect__PC_Template_Name__c)) {
                status = spc_ApexConstants.DOC_STATUS_FAILED;
            }
        }
        for (Id patientAccountId : patientToRecipients.keySet()) {
            for (Account recipient : patientToRecipients.get(patientAccountId)) {
                doc = new PatientConnect__PC_Document__c(
                    RecordTypeId = recordTypeId,
                    PatientConnect__PC_Document_Status__c = spc_ApexConstants.getValue(spc_ApexConstants.PicklistValue.DOC_STATUS_READYTOSEND),
                    PatientConnect__PC_Description__c = eLetter.Name,
                    PatientConnect__PC_To_Fax_Name__c = recipient.Type,
                    PatientConnect__PC_From_Name__c = '',
                    spc_PMRC_Code_New__c = PMRCCode,
                    spc_Program_Code__c = ProgramCode,
                    ownerId = casesList[0].ownerId,
                    PatientConnect__PC_Engagement_Program__c = casesList[0].PatientConnect__PC_Engagement_Program__c
                );
                if (casesList[0].PatientConnect__PC_Is_Program_Case__c) {
                    doc.spc_Program_Case_Id__c = objectID;
                } else if (casesList[0].PatientConnect__PC_Program__c != null) {
                    doc.spc_Program_Case_Id__c = casesList[0].PatientConnect__PC_Program__c ;
                    doc.spc_InquiryCaseId__c =  objectID;
                } else {
                    doc.spc_InquiryCaseId__c =  objectID;
                }
                documents.add(doc);
                wrappers.add(new spc_eLetter.DocumentWrapper(patientAccountId, recipient, doc));
            }
        }
        try {
            spc_database.ins(documents);
            List<PatientConnect__PC_Document_Log__c> docLogs = createDocLogsFromDocs(documents, objectId);
            if (docLogs != null) spc_database.ins(docLogs);
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }
        return wrappers;
    }

    /**
    * @description Method to be used at eLetterManagement page, where recipient's language can differ based on the selection
    * at page level and the communication method can be fax/email
    * @return void
    * @param objectId      Id for which the eLetter has to be sent - Program Case Id
    * @param eLetter       eLetter record
    * @param communicationMethod   Fax or Email
    * @param recipients    Has some value only when invoked from eLetter page where recipients and their language
    *                      are dyanmically selected at page level. So it won't be applicable for all related recipients.
    * @param sendToUser    True, if the eLetter should be send to the current user.
    */
    public static void sendLetter(Id objectId, PatientConnect__PC_eLetter__c eLetter, String communicationMethod, List<Account> recipients, Boolean sendToUser) {
        spc_eLetterService service = new spc_eLetterService();
        List<spc_CommunicationMgr.Envelop> envelops = new List<spc_CommunicationMgr.Envelop>();
        if (communicationMethod != null && communicationMethod.toLowerCase() == spc_ApexConstants.FAX) {
            service.sendLetter(objectId, eLetter, recipients, spc_ApexConstants.ID_FAX_OUTBOUND_RECORDTYPE, sendToUser);
        } else {
            service.sendLetter(objectId, eLetter, recipients, spc_ApexConstants.ID_EMAIL_OUTBOUND_RECORDTYPE, sendToUser);
        }
    }

    private void sendLetter(Id objectId, PatientConnect__PC_eLetter__c eLetter, List<Account> recipients, Id docRecordTypeId, Boolean sendToCurrentUser) {
        loadTargetInformation(objectId);
        Map<Id, List<Account>> patientToRecipients = getRelatedRecipients(recipients);

        // Create document records for the patients' recipients & get the respective document ids (for attaching the merged templates to these documents)
        List<spc_eLetter.DocumentWrapper> documents = createDocuments(patientToRecipients, eLetter, docRecordTypeId, objectId);

        try {
            String eLetterSender = EMPTY_STRING;
            if (!String.isBlank(eLetter.PatientConnect__PC_Template_Name__c) &&  verifyValidAndActiveTemplate(eLetter.PatientConnect__PC_Template_Name__c)) {
                spc_eLetterEmailTemplateSender.sendLetter(objectId, documents, eLetter, sendToCurrentUser);
            } else if (spc_ApexConstants.ID_FAX_OUTBOUND_RECORDTYPE == docRecordTypeId) { // If it is FAX
                createFaxReadyToSend(objectId, documents, eLetter, sendToCurrentUser);
            }
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
        }
    }

    /**
    * @description Method to collect the patient ids from the recipients
    *
    * @param objectId Id for which the eLetter has to be sent
    */
    private void loadTargetInformation(Id objectId) {
        SObjectType sObjectType = objectId.getSobjectType();

        if (sObjectType == Case.sObjectType) {
            Case programCase = [SELECT Id, AccountId FROM Case WHERE Id = :objectId];
            patientAccountIds.add(programCase.AccountId);
            patientToObjectId.put(programCase.AccountId, programCase.Id);
        }
    }
    /*********************************************************************************

    * @description    : Based on the eLetterName get the relevant Participants for patient(s)
    * @return   List<spc_eLetter_Recipient>
    * @param     1. caseId : Patient for whom the related recipient are required
    * @param 2. eLette: eLetter record details
    *********************************************************************************/
    public static List<spc_eLetter_Recipient> getParticipantsForEnquiryCase(Case eCase, PatientConnect__PC_eLetter__c eLetter) {
        try {
            // Get available languages from the eLetter Template and dynamically display the recipient language:
            Set<String> languages = new Set<String> ();
            Set<String> cchannels = new Set<String> ();
            languages.addAll(eLetter.PatientConnect__PC_Communication_Language__c.split(';'));
            List<spc_eLetter_Recipient> recipients = new List<spc_eLetter_Recipient>();
            if (eCase.AccountId != null) {
                List<Account> PatientAcc = [SELECT ID, Fax, PatientConnect__PC_Communication_Language__c, Type, Name, PatientConnect__PC_Email__c FROM ACCOUNT WHERE ID = : eCase.AccountId];
                if (!PatientAcc.isEmpty()) {
                    spc_eLetter_Recipient recipient = new spc_eLetter_Recipient(PatientAcc[0], PatientAcc[0].Type, languages, cchannels);
                    recipients.add(recipient);
                }
            }
            return recipients;
        } catch (Exception ex) {
            spc_Utility.logAndThrowException(ex);
            return null;
        }
    }
    /*********************************************************************************

    * @author      Deloitte
    * @Description     return eletters
    * @return     Map<ID,List<String>>
    *********************************************************************************/
    public static Map<ID, List<String>>  getMapBetweenMailAndCchannels(Set<ID> engProId) {

        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(spc_ApexConstants.ENG_ELETTER_FORM_FIELD_SET);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        Map<ID, List<String>> cChannels = new Map<ID, List<String>>();
        for (PatientConnect__PC_Engagement_Program_Eletter__c eletter : [ SELECT ID, PatientConnect__PC_eLetter__c, spc_PMRC_Code__c, PatientConnect__PC_Engagement_Program__c FROM PatientConnect__PC_Engagement_Program_Eletter__c
                WHERE PatientConnect__PC_Engagement_Program__c IN : engProId AND spc_Channel__c = :Label.spc_postalMail]) {

            if (cChannels.get(eletter.PatientConnect__PC_eLetter__c) == null) {
                List<String> cchannelList = new List<String>();
               cchannelList.add(spc_Utility.getActualPMRCCode(eletter.spc_PMRC_Code__c));
                cChannels.put(eletter.PatientConnect__PC_eLetter__c, cchannelList);
            }
        }
        return cChannels;
    }
    /*********************************************************************************

    * @author Deloitte
    * @description    : create Document Logs fro Docs
    * @return   :  List<PatientConnect__PC_Document_Log__c>
    *********************************************************************************/
    public static List<PatientConnect__PC_Document_Log__c> createDocLogsFromDocs(List<PatientConnect__PC_Document__c> docs, Id CaseId) {

        List<Case> Casees = [ SELECT Id, PatientConnect__PC_Program__c, AccountId, spc_is_Pretreatment_Case__c, PatientConnect__PC_Is_Program_Case__c, spc_is_PostTreatment_Case__c, spc_Is_Enquiry_Case__c FROM Case where Id = : caseId ];
        if (Casees.size() > 0) {
            List<PatientConnect__PC_Document_Log__c> docLogList = new List<PatientConnect__PC_Document_Log__c>();
            for (PatientConnect__PC_Document__c doc : docs) {
                PatientConnect__PC_Document_Log__c doclog = new PatientConnect__PC_Document_Log__c();
                doclog.PatientConnect__PC_Document__c = doc.Id;
                if (Casees[0].spc_Is_Enquiry_Case__c) doclog.spc_Inquiry__c = Casees[0].ID;
                else if (Casees[0].PatientConnect__PC_Is_Program_Case__c) doclog.PatientConnect__PC_Program__c = Casees[0].ID;
                else {
                    doclog.spc_Inquiry__c = Casees[0].ID;
                    if (Casees[0].PatientConnect__PC_Program__c != null) {
                        doclog.PatientConnect__PC_Program__c = Casees[0].PatientConnect__PC_Program__c;
                    }
                }
                docLogList.add(doclog);
            }
            return docLogList;
        } return null;
    }




    /*********************************************************************************

    * @author     Pratik Raj
    * @description     create Document Logs fro Docs
    * @return     List<PatientConnect__PC_Document_Log__c>
    *********************************************************************************/
    public static Map<ID, Set<String>>  getMapBetweenEletterAndCchannels(Set<ID> engProId) {

        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(spc_ApexConstants.ENG_ELETTER_FORM_FIELD_SET);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        Map<ID, Set<String>> cChannels = new Map<ID, Set<String>>();
        for (PatientConnect__PC_Engagement_Program_Eletter__c eletter : [ SELECT ID, PatientConnect__PC_eLetter__c, PatientConnect__PC_Engagement_Program__c FROM PatientConnect__PC_Engagement_Program_Eletter__c
                WHERE PatientConnect__PC_Engagement_Program__c IN : engProId AND spc_Channel__c = 'Email']) {

            if (cChannels.get(eletter.PatientConnect__PC_eLetter__c) == null) {
                Set<String> cchannelList = new Set<String>();
                cchannelList.add(System.Label.spc_Email_Template);
                cChannels.put(eletter.PatientConnect__PC_eLetter__c, cchannelList);
            }
        }
        return cChannels;
    }
    /*********************************************************************************

    * @author Deloitte
    * @description     create Document Logs fro Docs
    * @return   List<PatientConnect__PC_Document_Log__c>
    *********************************************************************************/
    public static Map<ID, Set<String>>  getMapBetweenFaxAndCchannels(Set<ID> engProId) {

        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(spc_ApexConstants.ENG_ELETTER_FORM_FIELD_SET);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        Map<ID, Set<String>> cChannels = new Map<ID, Set<String>>();
        for (PatientConnect__PC_Engagement_Program_Eletter__c eletter : [ SELECT ID, PatientConnect__PC_eLetter__c, PatientConnect__PC_Engagement_Program__c, spc_Channel__c FROM PatientConnect__PC_Engagement_Program_Eletter__c
                WHERE PatientConnect__PC_Engagement_Program__c IN : engProId AND spc_Channel__c = 'FAX']) {

            if (cChannels.get(eletter.PatientConnect__PC_eLetter__c) == null) {
                Set<String> cchannelList = new Set<String>();
                // if(eletter.spc_Email_Template__c) cchannelList.add(fieldMap.get(spc_ApexConstants.EMAIL_TEMPLATE).getDescribe().getLabel());
                cchannelList.add(Label.spc_FAX_Template);
                cChannels.put(eletter.PatientConnect__PC_eLetter__c, cchannelList);
            }
        }
        return cChannels;
    }
    /*********************************************************************************
    * @author Deloitte
    * @description     verify Valid And Active Template
    * @return   boolean

    *********************************************************************************/
    public static boolean verifyValidAndActiveTemplate(String templateName) {

        List<EmailTemplate> emailTemplate = [SELECT Id, IsActive FROM EmailTemplate WHERE DeveloperName = : templateName and IsActive = true];
        if (!emailTemplate.isEmpty()) {
            return true;
        }
        return false;
    }
    /*********************************************************************************
    * @author Deloitte
    * @description     verify Valid And Active Template
    * @return   boolean

    *********************************************************************************/
    public static user sendToMe() {

        List<User> users = [SELECT id, Email, Fax, Phone FROM User WHERE Id = :UserInfo.getUserId()] ;
        if (!users.isEmpty()) {
            return Users[0];
        }
        return null;
    }
    /*********************************************************************************
    * @author Deloitte
    * @description     Create template and update a field on Document to make fax Ready to Send
    * @return   void

    *********************************************************************************/
    public static void createFaxReadyToSend(Id objectId, List<spc_eLetter.DocumentWrapper> documentRecipients, PatientConnect__PC_eLetter__c eLetter, Boolean sendToCurrentUser) {
        Map<Id,Attachment> attachmentIds  = new Map<Id,Attachment>([Select Id from  Attachment where parentId =:eLetter.Id ]);
        String attachmentString = '';
        if(! attachmentIds.keySet().isEmpty()){
            Integer attachmentIndex = 0;
            for(String attachmentId : attachmentIds.keySet()){
                string attachStr  = 'attachId' + (attachmentIndex > 0 ? String.valueOf(attachmentIndex) :'');
                attachmentString += '<'+attachStr+'>' + attachmentId + '</' + attachStr +'>';	
                attachmentIndex++;
            }
        }

        User currUser = spc_eLetterService.sendToMe();
        Map<Id, spc_eLetter.DocumentWrapper> accountIdToDocumentWrapper = new Map<Id, spc_eLetter.DocumentWrapper>();
        List<PatientConnect__PC_Document__c> docs = new List<PatientConnect__PC_Document__c>();
        spc_CommunicationMgr.RecipientHandler rHandler = new spc_CommunicationMgr.RecipientHandler();
        for (spc_eLetter.DocumentWrapper docWrapper : documentRecipients) {
            if (docWrapper.recipient.Fax != null) {
                accountIdToDocumentWrapper.put(docWrapper.recipient.Id, docWrapper);
            }
        }
        for (Id accountId : accountIdToDocumentWrapper.keySet()) {
            if (!String.isBlank(eLetter.PatientConnect__PC_Template_Name__c)) {
                String OutboundFAXTemplate = Label.spc_FaxTemplateParserString;
                OutboundFAXTemplate = OutboundFAXTemplate.replaceAll('CASERECORDID', objectId);
                OutboundFAXTemplate = OutboundFAXTemplate.replaceAll('DOCUMENTID', accountIdToDocumentWrapper.get(accountId).doc.ID);
                OutboundFAXTemplate = OutboundFAXTemplate.replaceAll('ELETTERTEMPALTENAME', eLetter.PatientConnect__PC_Template_Name__c);
                OutboundFAXTemplate = OutboundFAXTemplate.replaceAll('ATTACHID', '');
                List<String> toAddresses = new List<String>();
                List<String> setAddresses = new List<String>();
                if (sendToCurrentUser) {
                    toAddresses.add(currUser.Fax);
                } else {
                    String faxNumber = accountIdToDocumentWrapper.get(accountId).recipient.Fax;
                    if (!String.isBlank(faxNumber)) {
                        toAddresses.add(rHandler.getFAXNumber(faxNumber));
                        setAddresses.add(faxNumber);
                    }
                }
                if (!toAddresses.isEmpty()) {
                    OutboundFAXTemplate = OutboundFAXTemplate.replaceAll('FAXNUMBER', string.join(toAddresses, ','));
                    accountIdToDocumentWrapper.get(accountId).doc.PatientConnect__PC_To_Fax_Number__c = string.join(setAddresses, ',');
                }
                accountIdToDocumentWrapper.get(accountId).doc.spc_OutboundFAX_Template__c = OutboundFAXTemplate;
                docs.add(accountIdToDocumentWrapper.get(accountId).doc);
                if (sendToCurrentUser) {
                    break;
                }
            }
        }
        if (!docs.isEmpty()) { update docs;}
    }
}
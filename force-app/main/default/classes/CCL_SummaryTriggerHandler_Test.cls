/*
*  @author          Deloitte
*  @description     test class for Trigger handler class for Summary trigger
*  @date            Aug 13, 2020
*  @version         1.0
Modification Log:
--------------------------------------------------------------------------------------------------------------
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------
Deloitte                  Aug 13 2020         Initial Version
*/
@isTest
public class CCL_SummaryTriggerHandler_Test {

    @testSetup
    public static void createTestData() {
        CCL_Order__c order = new CCL_Order__c();
        order.Name='Novartis Batch ID';
        order.CCL_First_Name__c='First Name';
        order.CCL_Last_Name__c='Last Name';
        order.CCL_Middle_Name__c='N';
        order.CCL_Suffix__c='Sr.';
        order.RecordTypeId=CCL_StaticConstants_MRC.ORDER_RECORDTYPE_CLINICAL;
        order.CCL_Date_of_DOB__c='1';
        order.CCL_Month_of_DOB__c='JAN';
        order.CCL_Year_of_DOB__c='1990';
        order.CCL_Hospital_Patient_ID__c='1234';
        order.CCL_Patient_Weight__c=90;
        order.CCL_Patient_Age_At_Order__c=30;
        order.CCL_Treatment_Protocol_Subject_ID__c='CTL198088';
        order.CCL_Protocol_Center_Number__c='1234';
        order.CCL_Protocol_Subject_Number__c='123';
        order.CCL_Consent_for_Additional_Research__c=TRUE;
        order.CCL_PRF_Ordering_Status__c='PRF_Submitted';
        order.CCL_Hospital_Purchase_Order_Number__c='34834349';
        order.CCL_Physician_Attestation__c= TRUE;
        order.CCL_Patient_Eligibility__c= TRUE;
        order.CCL_VA_Patient__c= TRUE;
        order.CCL_Planned_Apheresis_Pick_up_Date_Time__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        order.CCL_Manufacturing_Slot_ID__c='123567';
        order.CCL_Value_Stream__c=89;
        order.CCL_Estimated_FP_Delivery_Date__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        order.CCL_Scheduler_Missing__c=TRUE;
        order.CCL_Returning_Patient__c=TRUE;
        order.CCL_Estimated_Manufacturing_Start_Date__c=date.newInstance(2014, 9, 15);
        insert order;
        //CCL_Order__c order = CCL_TestDataFactory.createTestPRFOrder();
        CCL_Time_Zone__c newTimeZoneObj = new CCL_Time_Zone__c();
        newTimeZoneObj.Name = 'NewTime Zone2';
        newTimeZoneObj.CCL_SAP_Time_Zone_Key__c = '68789790-test';
        newTimeZoneObj.CCL_External_ID__c = '6667788';
        newTimeZoneObj.CCL_Saleforce_Time_Zone_Key__c = '797881-test';
        insert newTimeZoneObj;
        CCL_Time_Zone__c timeZoneId = [SELECT Id FROM CCL_Time_Zone__c where Name = 'NewTime Zone2'LIMIT 1];
        if(order!=null){

        CCL_Summary__c  summary = new CCL_Summary__c();
        summary.Name='Test Summary';
		summary.CCL_Manufacturing_Start_Date_Time__c=Date.newInstance(2020, 08, 13);
		summary.CCL_Manufacturing_End_Date_Time__c=Date.newInstance(2020, 08, 13);
		summary.CCL_Time_Zone__c=timeZoneId.Id;
		summary.CCL_Order__c=order.Id;
		summary.RecordTypeId=CCL_StaticConstants_MRC.SUMMARY_RECORDTYPE_MANUFACTURING;
        insert summary;
        }

    }
    static testMethod void testUpdateFields(){
        CCL_Summary__c summary  = [SELECT Id, Name, RecordTypeId,CCL_Time_Zone_Text__c, CCL_Manufacturing_Start_Date_Time__c,
                               CCL_Manufacturing_End_Date_Time__c, CCL_Time_Zone__c,
                               CCL_Manufacturing_Start_Date_Time_Text__c, CCL_Manufacturing_End_Date_Time_Text__c
                               FROM CCL_Summary__c where Name=:'Test Summary'];
        system.debug(summary);

        Test.startTest();
        summary.CCL_Manufacturing_Start_Date_Time_Text__c=CCL_Utility.getdisplayDateTimeValues(summary.CCL_Manufacturing_Start_Date_Time__c,summary.CCL_Time_Zone_Text__c);
        summary.CCL_Manufacturing_End_Date_Time_Text__c=CCL_Utility.getdisplayDateTimeValues(summary.CCL_Manufacturing_End_Date_Time__c,summary.CCL_Time_Zone_Text__c);
        Boolean b = String.isEmpty((summary.CCL_Manufacturing_Start_Date_Time_Text__c));
		System.assertEquals(b, FALSE, 'Either date is not fetched from query or not getting through the utility');
        Test.stopTest();

    }

}
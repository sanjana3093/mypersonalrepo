public without sharing class CCL_Integration_Utility
{
    /*Processes the DTO List and finally generates a Map which is to be serialized*/

    public static void processOutboundIntegration(List<CCL_Integration_Dto> dtoList)
    {
        Map<Id,CCL_Integration_Dto> dtoMap= new Map<Id,CCL_Integration_Dto>();
        Map<String,Object> mapForJson = new Map<string,Object>();
        Map<Id,Map<String,object>> mapToSerailizePerRecordId= new Map<id,Map<String,object>>();
        Map<Id,List<Sobject>> listofChildRecord1PerParentRecordId= new Map<Id,List<Sobject>>();
        Map<Id,List<Sobject>> listofChildRecord2PerParentRecordId= new Map<Id,List<Sobject>>();
        List<Id> listOfRecordIds = new List<Id> ();
        List<sObject> recordsFetchedFromParentDynamicQuery = new List<sObject> ();
        List<sObject> recordsFetchedFromChild1DynamicQuery = new List<sObject> ();
        List<sObject> recordsFetchedFromChild2DynamicQuery = new List<sObject> ();
        List<Id> dynamicQueryFetchedParentIdList = new List<Id>();


        Boolean checkMetadataAssignmentPerDTORecord=true;
        //below flag is to identify critical locked field
        Boolean isCriticalFieldChanged=false;
        Map<id,Boolean> mapOfCriticalFieldChangedPerRecordId=new Map<id,Boolean>();
        Map<id,String> mapOfNovartisBatchIdPerRecordId=new Map<id,String>();

       	string queryFromMetadataType;
        string child1QueryFromMetadataType;
        string child2QueryFromMetadataType;

        string objectToqueryFromMetadataType;
        string child1ObjectToqueryFromMetadataType;
        string child2ObjectToqueryFromMetadataType;
        String parentFieldSetNameFromMetadataType;
        String child1FieldSetNameFromMetadataType;
        String child2FieldSetNameFromMetadataType;
        String parentFilterCriteriaFromMetadataType;
        String child1FilterCriteriaFromMetadataType;
        String child2FilterCriteriaFromMetadataType;
        string parentDynamicQuery;
        string child1DynamicQuery;
        string child2DynamicQuery;
        //string dynamicQuery;
        string eventType;
        Id recordId;
        final string selectValueForDynamicQuery='SELECT ';
        final string fromValueForDynamicQuery=' FROM ';
        final string whereValueForDynamicQuery=' WHERE ';
        final string filterValueForParentDynamicQuery=' WHERE Id IN: listOfRecordIds';
        final string filterValueForChildDynamicQuery=' in :dynamicQueryFetchedParentIdList';
        string fieldToidentifyCriticalFieldForCurrentProcess='';
        string fieldToidentifyNovartisBatchIdForCurrentProcess='';


        if(dtoList!=null && dtoList.size()>0)
        {

            for(CCL_Integration_Dto dtoVal : dtoList )

                {
                    dtoMap.put(dtoVal.recordId,dtoVal);
                    listOfRecordIds.add(dtoVal.recordId);
                    if(checkMetadataAssignmentPerDTORecord)
                    {
                     queryFromMetadataType=dtoVal.metadataRecord.CCL_Query_String__c;
                     child1QueryFromMetadataType=dtoVal.metadataRecord.CCL_Child1_Query_String__c;
                     child2QueryFromMetadataType=dtoVal.metadataRecord.CCL_Child2_Query_String__c;
                     objectToqueryFromMetadataType=dtoVal.metadataRecord.CCL_Object_To_Query__c;
                     child1ObjectToqueryFromMetadataType=dtoVal.metadataRecord.CCL_Child1_Object_Name__c;
                     child2ObjectToqueryFromMetadataType=dtoVal.metadataRecord.CCL_Child2_Object_Name__c;
                     parentFilterCriteriaFromMetadataType=dtoVal.metadataRecord.CCL_Filter_Criteria__c;
                     child1FilterCriteriaFromMetadataType=dtoVal.metadataRecord.CCL_Child1_Filter_Criteria__c;
                     child2FilterCriteriaFromMetadataType=dtoVal.metadataRecord.CCL_Child2_Filter_Criteria__c;
                     eventType=dtoVal.eventType;
                     // Added a change for Critical field change requirement
                     fieldToidentifyCriticalFieldForCurrentProcess=dtoVal.metadataRecord.CCL_Field_to_Identify_Critical_Lock__c;
                     checkMetadataAssignmentPerDTORecord=false;
                     fieldToidentifyNovartisBatchIdForCurrentProcess=dtoVal.metadataRecord.CCL_Field_to_Identify_Novartis_Batch_Id__c;
                    }

                }
        }
        if(dtoMap!=null && dtoMap.size()>0)
        {

          if(queryFromMetadataType != null)
                 {
                     parentDynamicQuery =selectValueForDynamicQuery + String.escapeSingleQuotes(queryFromMetadataType) + fromValueForDynamicQuery + String.escapeSingleQuotes(objectToqueryFromMetadataType) + filterValueForParentDynamicQuery;
                     system.debug('Parent Dynamic Query : '+parentDynamicQuery);
                     try
                     {
                         recordsFetchedFromParentDynamicQuery=Database.query(parentDynamicQuery);
                     }
                     catch(Exception e)
                     {
                         system.debug('Some error occoured while executing Parent Dynamic Query : '+e.getMessage());
                     }

                     system.debug('Parent Records: '+recordsFetchedFromParentDynamicQuery);

                     if(recordsFetchedFromParentDynamicQuery!= null && recordsFetchedFromParentDynamicQuery.size()>0)
                     {
                         for(sobject parentRec : recordsFetchedFromParentDynamicQuery )
                         {
                            dynamicQueryFetchedParentIdList.add(parentRec.Id);//To be used in child filter criteria's
                            if(!String.isblank(fieldToidentifyCriticalFieldForCurrentProcess)){
                                Object dynamicValue;
                                dynamicValue=CCL_RelationshipQueryUtil.getFieldValue(parentRec,fieldToidentifyCriticalFieldForCurrentProcess);
                                system.debug('dynamicValue: '+dynamicValue);
                                if(dynamicValue!=null){
                                    isCriticalFieldChanged=(Boolean)dynamicValue;
                                    system.debug('isCriticalFieldChanged: '+isCriticalFieldChanged);
                                }
                            }
                            if(!String.isblank(fieldToidentifyNovartisBatchIdForCurrentProcess)){
                                Object dynamicNovartisValue;
                                dynamicNovartisValue=CCL_RelationshipQueryUtil.getFieldValue(parentRec,fieldToidentifyNovartisBatchIdForCurrentProcess);
                                if(dynamicNovartisValue!=null){
                                    mapOfNovartisBatchIdPerRecordId.put(parentRec.Id,(String)dynamicNovartisValue);
                                }
                            }
                            mapToSerailizePerRecordId.put(parentRec.Id,new map<String,Object>{objectToqueryFromMetadataType=>parentRec});
                            mapOfCriticalFieldChangedPerRecordId.put(parentRec.Id,isCriticalFieldChanged);
                             system.debug('mapOfCriticalFieldChangedPerRecordId: '+mapOfCriticalFieldChangedPerRecordId);
                             mapForJson.put(objectToqueryFromMetadataType,parentRec);
                             //mapToSerailizePerRecordId.put(parentRec.Id,mapForJson);
                         }
                     }

                 }
            if(child1QueryFromMetadataType != null)
            {
                if(child1FilterCriteriaFromMetadataType!=null)
                {
                     child1DynamicQuery =selectValueForDynamicQuery + String.escapeSingleQuotes(child1QueryFromMetadataType) + fromValueForDynamicQuery + String.escapeSingleQuotes(child1ObjectToqueryFromMetadataType) +whereValueForDynamicQuery+ String.escapeSingleQuotes(child1FilterCriteriaFromMetadataType) + filterValueForChildDynamicQuery;
                     system.debug('child1DynamicQuery : '+child1DynamicQuery);
                    try
                    {
                        recordsFetchedFromChild1DynamicQuery=Database.query(child1DynamicQuery);
                    }
                    catch(Exception e)
                    {
                        system.debug('Some error occoured while executing Child1 Dynamic Query : '+e.getMessage());
                    }

                     system.debug('Child1 Records: '+recordsFetchedFromChild1DynamicQuery);
                }

                if(recordsFetchedFromChild1DynamicQuery!= null && recordsFetchedFromChild1DynamicQuery.size()>0)
                     {
                         for(sObject firstChildRecord : recordsFetchedFromChild1DynamicQuery )
                         {
                            Id ParentRecordId = (Id)CCL_RelationshipQueryUtil.getFieldValue(firstChildRecord,child1FilterCriteriaFromMetadataType);

                            if(listofChildRecord1PerParentRecordId.containskey(ParentRecordId)){
                                List<sObject> childObject1Listtemp= new List<sObject>();
                                childObject1Listtemp=listofChildRecord1PerParentRecordId.get(ParentRecordId);
                                childObject1Listtemp.add(firstChildRecord);
                                listofChildRecord1PerParentRecordId.put(ParentRecordId,childObject1Listtemp);
                                if(mapToSerailizePerRecordId.containsKey(ParentRecordId) && mapToSerailizePerRecordId.get(ParentRecordId).containsKey(child1ObjectToqueryFromMetadataType)){
                                    Map<String,object> mapToAddValues=mapToSerailizePerRecordId.get(ParentRecordId);
                                    //mapToAddValues.get(child1ObjectToqueryFromMetadataType);
                                    mapToAddValues.put(child1ObjectToqueryFromMetadataType,childObject1Listtemp);
                                    mapToSerailizePerRecordId.put(ParentRecordId, mapToAddValues);
                                }
                                //mapToSerailizePerRecordId.put(ParentRecordId,mapToSerailizePerRecordId.get(ParentRecordId).get(child1objectToqueryFromMetadataType).add(firstChildRecord));

                            }
                            else{
                                List<sObject> childObject1Listtemp= new List<sObject>();
                                childObject1Listtemp.add(firstChildRecord);
                                listofChildRecord1PerParentRecordId.put(ParentRecordId,childObject1Listtemp);
                                if(mapToSerailizePerRecordId.containsKey(ParentRecordId)){
                                    Map<String,object> mapToAddValues=mapToSerailizePerRecordId.get(ParentRecordId);
                                    mapToAddValues.put(child1ObjectToqueryFromMetadataType,firstChildRecord);
                                    mapToSerailizePerRecordId.put(ParentRecordId, mapToAddValues);
                                }
                                //mapToSerailizePerRecordId.put(parentRec.Id,new map<String,Object>{objectToqueryFromMetadataType=>parentRec});
                            }
                         }
                     }

            }
            if(child2QueryFromMetadataType != null)
            {
                if(child2FilterCriteriaFromMetadataType!=null)
                {
                    child2DynamicQuery=selectValueForDynamicQuery + String.escapeSingleQuotes(child2QueryFromMetadataType) + fromValueForDynamicQuery + String.escapeSingleQuotes(child2ObjectToqueryFromMetadataType) +whereValueForDynamicQuery+ String.escapeSingleQuotes(child2FilterCriteriaFromMetadataType) + filterValueForChildDynamicQuery;
                    system.debug('child2DynamicQuery : '+child2DynamicQuery);
                    try
                    {
                        recordsFetchedFromChild2DynamicQuery=Database.query(child2DynamicQuery);
                    }
                	catch(Exception e)
                    {
                        system.debug('Some error occoured while executing Child2 Dynamic Query : '+e.getMessage());
                    }
                    system.debug('Child2 Records: '+recordsFetchedFromChild2DynamicQuery);
                }

                if(recordsFetchedFromChild2DynamicQuery!= null && recordsFetchedFromChild2DynamicQuery.size()>0)
                     {
                         for(sObject secondChildRecord : recordsFetchedFromChild2DynamicQuery )
                         {
                            Id ParentRecordId= (Id)CCL_RelationshipQueryUtil.getFieldValue(secondChildRecord,child2FilterCriteriaFromMetadataType);

                            if(listofChildRecord2PerParentRecordId.containskey(ParentRecordId)){
                                List<sObject> childObject2Listtemp= new List<sObject>();
                                childObject2Listtemp=listofChildRecord2PerParentRecordId.get(ParentRecordId);
                                childObject2Listtemp.add(secondChildRecord);
                                listofChildRecord2PerParentRecordId.put(ParentRecordId,childObject2Listtemp);
                                if(mapToSerailizePerRecordId.containsKey(ParentRecordId) && mapToSerailizePerRecordId.get(ParentRecordId).containsKey(child2ObjectToqueryFromMetadataType)){
                                    Map<String,object> mapToAddValues=mapToSerailizePerRecordId.get(ParentRecordId);
                                    //mapToAddValues.get(child1ObjectToqueryFromMetadataType);
                                    mapToAddValues.put(child2ObjectToqueryFromMetadataType,childObject2Listtemp);
                                    mapToSerailizePerRecordId.put(ParentRecordId, mapToAddValues);
                                }
                                //mapToSerailizePerRecordId.put(ParentRecordId,mapToSerailizePerRecordId.get(ParentRecordId).get(child1objectToqueryFromMetadataType).add(firstChildRecord));
                            }
                            else{
                                List<sObject> childObject2Listtemp= new List<sObject>();
                                childObject2Listtemp.add(secondChildRecord);
                                listofChildRecord2PerParentRecordId.put(ParentRecordId,childObject2Listtemp);
                                if(mapToSerailizePerRecordId.containsKey(ParentRecordId)){
                                    Map<String,object> mapToAddValues=mapToSerailizePerRecordId.get(ParentRecordId);
                                    mapToAddValues.put(child2ObjectToqueryFromMetadataType,secondChildRecord);
                                    mapToSerailizePerRecordId.put(ParentRecordId, mapToAddValues);
                                }
                                //mapToSerailizePerRecordId.put(parentRec.Id,new map<String,Object>{objectToqueryFromMetadataType=>parentRec});
                            }
                         }
                     }
            }

            serializeToJsonAndPrepareTransactionLog(mapToSerailizePerRecordId,eventType,mapOfCriticalFieldChangedPerRecordId,mapOfNovartisBatchIdPerRecordId);




            }
        }

    /*Get's the Map which to be serialized and the event type.
      Generates the Json output and prepares the Transaction Log Records */

    public static void serializeToJsonAndPrepareTransactionLog(Map<Id,Map<String,object>> mapToSerailizePerRecordId,string eventType,Map<id,Boolean> isCriticalFieldChangedPerRecordId, Map<id,String> novartisBatchIdPerRecordId)
    {
        List<CCL_Transaction_Log__c> transactionLogRecords = new List<CCL_Transaction_Log__c>();
        String strJson;
        final string statusValue='New';
        Integer lengthOfPayload1;
        Integer lengthOfPayload2;
        Integer lengthOfPayload3;
        Integer strJSONLength;
        string payLoad1;
        string payLoad2;
        string payLoad3;
        String strJsonSecondPart;
        Integer strJsonSecondPartLength;
        string strJsonThirdPart;
        try
        {
            if(mapToSerailizePerRecordId != null && mapToSerailizePerRecordId.size()>0)
            {
                for(Id CurrentRecordId: mapToSerailizePerRecordId.keySet()){
                        system.debug('mapForJson : '+mapToSerailizePerRecordId);
                        strJson = JSON.serialize(mapToSerailizePerRecordId.get(CurrentRecordId));
                        system.debug('strJson : '+strJson);

                        lengthOfPayload1=SObjectType.CCL_Transaction_Log__c.Fields.CCL_Json_Payload_1__c.Length;
                        lengthOfPayload2=SObjectType.CCL_Transaction_Log__c.Fields.CCL_Json_Payload_2__c.Length;
                        lengthOfPayload3=SObjectType.CCL_Transaction_Log__c.Fields.CCL_Json_Payload_3__c.Length;

                        strJSONLength = strJson.length();
                        CCL_Transaction_Log__c transactionLogRecord = new CCL_Transaction_Log__c();

                        if(strJSONLength <= lengthOfPayload1)
                        {
                            payLoad1=strJson;
                            payLoad2='';
                            payLoad3='';
                        }
                        else
                        {
                            strJsonSecondPart=strJson.subString(lengthOfPayload1,strJSONLength);
                            strJsonSecondPartLength=strJsonSecondPart.length();
                            payLoad1=strJson.subString(0,lengthOfPayload1);
                            if(strJsonSecondPartLength<=lengthOfPayload2)
                            {
                                payLoad2=strJsonSecondPart;
                                payLoad3='';

                            }
                            else
                            {
                                payLoad2=strJsonSecondPart.subString(0,lengthOfPayload2);
                                strJsonThirdPart=strJsonSecondPart.subString(lengthOfPayload2,strJsonSecondPartLength);
                                payLoad3=strJsonThirdPart;
                            }
                        }
                        transactionLogRecord.CCL_Json_Payload_1__c=payLoad1;
                        transactionLogRecord.CCL_Json_Payload_2__c=payLoad2;
                        transactionLogRecord.CCL_Json_Payload_3__c=payLoad3;
                        transactionLogRecord.CCL_Event_Type__c=eventType;
                        transactionLogRecord.CCL_Record_Id__c=CurrentRecordId;
                        transactionLogRecord.CCL_Status__c=statusValue;
                        transactionLogRecord.CCL_SCP_Status__c=statusValue;
                        // Addded below change as part of Crticial field change requirement
                        system.debug('CurrentRecordId: '+CurrentRecordId);
                        if(!isCriticalFieldChangedPerRecordId.isEmpty() &&isCriticalFieldChangedPerRecordId.containsKey(CurrentRecordId)){
                            transactionLogRecord.CCL_Critical_Fields_Changed__c =isCriticalFieldChangedPerRecordId.get(CurrentRecordId);
                        }
                        if(!novartisBatchIdPerRecordId.isEmpty() &&novartisBatchIdPerRecordId.containsKey(CurrentRecordId)){
                            transactionLogRecord.CCL_Novartis_Batch_Id__c = novartisBatchIdPerRecordId.get(CurrentRecordId);
                        }

                        transactionLogRecords.add(transactionLogRecord);
                }

               CCL_Integration_Accessor.createTransactionLogRecords(transactionLogRecords);



    }
        }
        catch(Exception e)
        {
            system.debug('Some error occoured while serializing to JSON and preparing Transaction Log: '+e.getMessage());
        }

    }

}
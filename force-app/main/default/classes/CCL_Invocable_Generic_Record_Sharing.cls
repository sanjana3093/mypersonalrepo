public class CCL_Invocable_Generic_Record_Sharing {
    
    @InvocableMethod
    public static void shareRecordsPerObject(List<CCL_Sharing_Invocable_Wrapper> sharingWrprList){
        List<CCL_Generic_Record_Sharing_DTO> listOfRecordDTO = new List<CCL_Generic_Record_Sharing_DTO>();
        Set<Id> currentRecIdSet = new Set<Id>();
        String treatmentLifeCycleStage;
        Boolean removeShareRecord=false;
        if(sharingWrprList!= null && sharingWrprList.size()>0){

            for(CCL_Sharing_Invocable_Wrapper shareingWrapr : sharingWrprList ){

            CCL_Generic_Record_Sharing_DTO recSharingdto = new  CCL_Generic_Record_Sharing_DTO();
            recSharingdto.treatmentLifeCycleStage = shareingWrapr.treatmentLifeCycleStage;
            recSharingdto.recordId = shareingWrapr.recordId;
            removeShareRecord = shareingWrapr.removeShareRecord;
            listOfRecordDTO.add(recSharingdto);
            currentRecIdSet.add(shareingWrapr.recordId);
            treatmentLifeCycleStage = shareingWrapr.treatmentLifeCycleStage;
            System.debug('listOfRecordDTO: ' + listOfRecordDTO);
            }
            if(!Test.isRunningTest()){
                if(removeShareRecord){
                    CCL_Generic_Record_Sharing.removeShareRecordFromCurrentRec(currentRecIdSet,treatmentLifeCycleStage);
                }
                if(listOfRecordDTO!=null && listOfRecordDTO.size()>0 ){
                    CCL_Generic_Record_Sharing.shareRecords(listOfRecordDTO);
                    CCL_Generic_Record_Sharing.shareRecordsForInternalUser(listOfRecordDTO);
                }
            }
        }
    }
    public class CCL_Sharing_Invocable_Wrapper {
    
        @invocableVariable(label='Treatment Life Cycle Stage')
        public String  treatmentLifeCycleStage;
    
        @invocableVariable(label='Current Record Id')
        public Id recordId;
        
        @invocableVariable(label='Remove Share Record')
        public Boolean removeShareRecord;
       
    }
    
}
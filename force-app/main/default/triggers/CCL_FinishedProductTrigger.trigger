/**
 * @description       : Trigger CCL_Finished_Product__c Object
 * @author            : Deloitte
 * @group             : Deloitte
 * @last modified on  : 15-11-2020
 * @last modified by  : Deloitte
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   15-11-2020   Deloitte   Initial Version
**/
trigger CCL_FinishedProductTrigger on CCL_Finished_Product__c (before insert, before update, after insert,after update, after delete, after undelete) {

/*
*  @author          Deloitte
*  @description     Trigger context methods
*  @param           Trigger Context variables
*  @return          N/A
*  @date            July 15, 2020
*/
   // Checks if the trigger is fired when finished product delivery date is updated
    if(!CCL_ShipmentTriggerHandler.finishedProductDateUpdated) 
    {

	CCL_FinishedProductTriggerHandler  updateTextFieldWithDateOnFinishedProductTriggerHandler = new CCL_FinishedProductTriggerHandler(); 
   if(CCL_TriggerExecutionUtility.getProcessFinishedProductTrigger()) {
      if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
          updateTextFieldWithDateOnFinishedProductTriggerHandler.updateDateFieldsOnFP(Trigger.new);
      }
      if(Trigger.isAfter && (Trigger.isInsert ||Trigger.isUndelete)) {
          updateTextFieldWithDateOnFinishedProductTriggerHandler.updateNumberOfDosesOnShipment(Trigger.new);
          updateTextFieldWithDateOnFinishedProductTriggerHandler.updateFPExpiryDateOnShipment(Trigger.new);
          System.debug('&&&&Insert & Undelete');

        } 
        if(Trigger.isAfter && Trigger.isDelete) {
            updateTextFieldWithDateOnFinishedProductTriggerHandler.updateNumberOfDosesOnShipment(Trigger.old);
            updateTextFieldWithDateOnFinishedProductTriggerHandler.updateFPExpiryDateOnShipment(Trigger.old);
          }
        if(Trigger.isAfter && Trigger.isUpdate) {
            updateTextFieldWithDateOnFinishedProductTriggerHandler.updateNumberOfDosesOnShipment(Trigger.new);
            updateTextFieldWithDateOnFinishedProductTriggerHandler.updateNumberOfDosesOnShipment(Trigger.old);
            updateTextFieldWithDateOnFinishedProductTriggerHandler.updateFPExpiryDateOnShipment(Trigger.new);
            updateTextFieldWithDateOnFinishedProductTriggerHandler.updateFPExpiryDateOnShipmentAfterUpdateOldValues(Trigger.old);
            CCL_TriggerExecutionUtility.setProcessFinishedProductTrigger(false);
          }
     }
    }
}

trigger CCL_SummaryTrigger on CCL_Summary__c (before insert, before update, after update, after insert) 
{

CCL_SummaryTriggerHandler updateTextFieldWithDateAndTimeOnSummaryTriggerHandler = new CCL_SummaryTriggerHandler(); 
    
    if(CCL_TriggerExecutionUtility.getProcessSummaryTrigger()) {


         if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
         
          updateTextFieldWithDateAndTimeOnSummaryTriggerHandler.updateDateAndTimeFields(Trigger.New);                
            }
    }
    CCL_TriggerExecutionUtility.setProcessSummaryTrigger(false);
	
	 if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
        
        CCL_SummaryTriggerHandler.updateStartDateonOrder(Trigger.New);                
    }
}
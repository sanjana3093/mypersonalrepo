/********************************************************************************************************
*  @author          Deloitte
*  @description     Therapy Association  Trigger for before context variable events.
*  @param           
*  @date            July 13, 2020
*********************************************************************************************************/

trigger CCL_TherapyAssociationTrigger on CCL_Therapy_Association__c (before insert, before update) {
    
     CCL_TherapyAssociationTriggerHandler userSiteTherapyTriggerHandler = new CCL_TherapyAssociationTriggerHandler();
    
    if(CCL_TriggerExecutionUtility.getProcessTherapyAssociationTrigger()) {


         if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
         
          userSiteTherapyTriggerHandler.updateUniqueSiteTherapyForDuplicationRule(Trigger.New);                
            }
    }
    CCL_TriggerExecutionUtility.setProcessTherapyAssociationTrigger(false);
}
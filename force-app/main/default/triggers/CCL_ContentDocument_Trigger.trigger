/********************************************************************************************************
*  @author          Deloitte
*  @description     File Deletion Trigger to allow only submitter to delete the files.
*  @param           
*  @date            July 28, 2020
*********************************************************************************************************/

trigger CCL_ContentDocument_Trigger on ContentDocument (before delete) {
    if(Trigger.isBefore && Trigger.isDelete) {
    CCL_ContentDocument_trigger_Handler.handleBeforeDelete(Trigger.Old);
    }
}
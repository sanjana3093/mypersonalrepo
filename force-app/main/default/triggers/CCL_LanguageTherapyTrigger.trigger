trigger CCL_LanguageTherapyTrigger on CCL_Language_Therapy_Association__c (before insert, before update) {
    
 CCL_LanguageTherapyTriggerHandler languageTherapyTriggerHandler = new CCL_LanguageTherapyTriggerHandler();
    
    
    if(CCL_TriggerExecutionUtility.getprocessLanguageTherapyAssociationTrigger()) {

        if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
         
            languageTherapyTriggerHandler.updateUniqueCombinationforDuplicationRule(Trigger.New);                
        }
    }
   CCL_TriggerExecutionUtility.setProcessLanguageTherapyAssociationTrigger(false);
}
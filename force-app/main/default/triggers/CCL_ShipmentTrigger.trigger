trigger CCL_ShipmentTrigger on CCL_Shipment__c (before insert,after insert,after update,before update) {
  /* reference of trigger Handler */
  CCL_ShipmentTriggerHandler shipmentHandler = new CCL_ShipmentTriggerHandler();
  if (CCL_TriggerExecutionUtility.getprocessShipmentTrigger()) {
        if (Trigger.isBefore  &&  Trigger.isUpdate) {
        shipmentHandler.updateFPShipmentDateTimeFields(Trigger.New);
        shipmentHandler.aphShipmentStatusAutomate(Trigger.New,Trigger.OldMap);
        shipmentHandler.updateCriticalFieldsChangedFlag(Trigger.New,Trigger.OldMap);
        shipmentHandler.updateAphIdDINSECValueField(Trigger.New);
        shipmentHandler.populateDateTextFields(Trigger.New,'update',Trigger.OldMap);
      }


     if(Trigger.isBefore && Trigger.isInsert) {
        shipmentHandler.updateFPShipmentDateTimeFields(Trigger.New);
        shipmentHandler.updateInfusionDataCollectionField(Trigger.New);
      }


    }
    if (Trigger.isAfter && Trigger.isUpdate) {
        shipmentHandler.updateCriticalFieldsLockedFlagForValidation(Trigger.New, Trigger.oldMap);
        shipmentHandler.updateApheresisChevron(Trigger.New,Trigger.oldMap);
        shipmentHandler.updateShipToInfusionCenterOnBatchAndFinishedProductRec(Trigger.newMap, Trigger.oldMap);
    }
  CCL_TriggerExecutionUtility.setprocessShipmentTrigger(false);
  if (Trigger.isAfter && Trigger.isInsert) {
        shipmentHandler.populateDateTextFields(Trigger.New,'insert',Trigger.OldMap);
      }
}
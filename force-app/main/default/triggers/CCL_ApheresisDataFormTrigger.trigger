/********************************************************************************************************
*  @author          Deloitte
*  @description     Trigger for Apheresis Data Form.
*  @param           
*  @date            Aug 12, 2020
*********************************************************************************************************/
trigger CCL_ApheresisDataFormTrigger on CCL_Apheresis_Data_Form__c (before insert,before update,after insert,after update) {
    
    CCL_ApheresisDataFormTriggerHandler adfTriggerHandler = new CCL_ApheresisDataFormTriggerHandler();
    if(CCL_TriggerExecutionUtility.getProcessprocessADFTrigger()) {
         if(Trigger.isBefore && Trigger.isUpdate) {
          adfTriggerHandler.updateADFLabelCompliant(Trigger.New,Trigger.OldMap);
            }
    }
  CCL_TriggerExecutionUtility.setProcessADFTrigger(false);
}
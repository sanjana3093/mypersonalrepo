trigger CCL_BatchTrigger on CCL_Batch__c (before insert, before update, after insert, after update) {

  CCL_BatchTriggerHandler updateTextFieldWithDateOnBatchTriggerHandler = new CCL_BatchTriggerHandler();
   if(CCL_TriggerExecutionUtility.getProcessBatchTrigger()) {
      if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
      updateTextFieldWithDateOnBatchTriggerHandler.updateDateFields(Trigger.new);
      }
    if((trigger.isAfter && trigger.isInsert) || (trigger.isAfter && trigger.isUpdate)) {	   
       updateTextFieldWithDateOnBatchTriggerHandler.updateNumberOfBagsOnShipment(Trigger.new);
       updateTextFieldWithDateOnBatchTriggerHandler.updateNumberOfBagsOnFinishedProducts(Trigger.new);
       CCL_TriggerExecutionUtility.setProcessBatchTrigger(false);
    }
    }
  }
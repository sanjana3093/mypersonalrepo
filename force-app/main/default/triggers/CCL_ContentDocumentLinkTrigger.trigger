trigger CCL_ContentDocumentLinkTrigger on ContentDocumentLink (before insert,after insert,after delete) {
    if(Trigger.isAfter && Trigger.isInsert) {
        CCL_ContentDocumentLink_Handler.handleAfterInsertorDelete(Trigger.New);
        CCL_ContentDocumentLink_Handler.updateRelatedDocumentsFlagOnFinishedProducts(Trigger.new);
    }
    
    if (Trigger.isBefore && Trigger.isInsert) {
        CCL_ContentDocumentLink_Handler.setExternalUserVisibility(Trigger.New);        
    }
}
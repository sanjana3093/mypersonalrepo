/********************************************************************************************************
*  @author          Deloitte
*  @description     User Therapy Trigger for all context variable events.
*  @param           
*  @date            July 10, 2020
*********************************************************************************************************/

trigger CCL_UserTherapyTrigger on CCL_User_Therapy_Association__c (before insert, after insert, before update, after update, before delete, after delete) {
    
    CCL_UserTherapyTriggerHandler userTherapyTriggerHandler = new CCL_UserTherapyTriggerHandler();
    
    
    if(CCL_TriggerExecutionUtility.getprocessUserTherapyAssociationTrigger()) {

        if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
         
            userTherapyTriggerHandler.updateUniqueCombinationforDuplicationRule(Trigger.New);                
        }
    }
   CCL_TriggerExecutionUtility.setProcessUserTherapyAssociationTrigger(false);
}
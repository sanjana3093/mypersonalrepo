/********************************************************************************************************
*  @author          Deloitte
*  @description     Account Trigger for all context variable events.
*  @param
*  @date            August 27, 2020
*********************************************************************************************************/

trigger CCL_AccountTrigger on Account (before insert, before update) {
    CCL_AccountTriggerHandler accountTriggerHandler = new CCL_AccountTriggerHandler();
    
    if(CCL_TriggerExecutionUtility.getProcessAccountTrigger()) {
        if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
            accountTriggerHandler.updatePotentialDuplicateFlag(Trigger.new);
        }
    }
    
    CCL_TriggerExecutionUtility.setProcessAccountTrigger(false);
}
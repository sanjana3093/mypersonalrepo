/********************************************************************************************************
*  @author          Deloitte
*  @description     Order Trigger for all context variable events.
*  @param
*  @date            August 12, 2020
*********************************************************************************************************/

trigger CCL_OrderTrigger on CCL_Order__c (before update, after update) {
    CCL_OrderTriggerHandler orderTriggerHandler = new CCL_OrderTriggerHandler();

    if(CCL_TriggerExecutionUtility.getProcessOrderTrigger()) {
        if(Trigger.isBefore && Trigger.isUpdate) {
            orderTriggerHandler.updateCriticalFieldsLockedFlagForValidation(Trigger.New, Trigger.oldMap);
           
            orderTriggerHandler.updateLatestChangeReason(Trigger.new);

            orderTriggerHandler.maskDateFields(Trigger.New);
			
            orderTriggerHandler.validateLogisticStatusUpdate(Trigger.new, Trigger.oldMap);
			
			//added for 2113
            orderTriggerHandler.setCriticalFlagTrueonOrder(Trigger.new, Trigger.oldMap);

            // CGTU-819
            List<PermissionSetAssignment> permissionSetList = [SELECT Id,PermissionSet.Name
                                                                FROM PermissionSetAssignment
                                                                WHERE AssigneeId=: UserInfo.getUserId() ];
            Boolean hasPermisssion = false;
            for(PermissionSetAssignment permTemp : permissionSetList) {
                if(permTemp.PermissionSet.Name == 'CCL_Customer_Operations' || 
                permTemp.PermissionSet.Name == 'CCL_Inbound_Logistics' ||
                permTemp.PermissionSet.Name == 'CCL_Outbound_Logistics') {
                    hasPermisssion = true;
                }
            }
            if (hasPermisssion) {                                               
                orderTriggerHandler.cancelOrderUpdateValidation(Trigger.New, Trigger.oldMap);
            }
        }
    }

    if(Trigger.isAfter && Trigger.isUpdate) {
        orderTriggerHandler.updateRelatedRecordStatus(Trigger.new,Trigger.oldMap);
		
		//CGTU- 2121
        orderTriggerHandler.updateManufacturingStartDateonOrder(Trigger.new, Trigger.oldMap);
    }
    
    if(Trigger.isBefore && Trigger.isInsert) {
         //orderTriggerHandler.updateOrder(Trigger.new);
    }

    CCL_TriggerExecutionUtility.setProcessOrderTrigger(false);
}
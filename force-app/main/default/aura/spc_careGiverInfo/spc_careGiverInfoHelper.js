({
    getPickListValues: function(component, event) {
        try {

            var action = component.get("c.getPicklistEntryMap ");
            action.setCallback(this, function(a) {
                var state = a.getState();
                var rtnValue = a.getReturnValue();

                if (state === "SUCCESS") {
                    component.set("v.picklistEntryMap", rtnValue);

                } else if (state === 'ERROR') {
                    console.log(a.getError());
                }
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log(e);
        }
    },
    setvalidation: function(component, event) {
        if (component.find('caregiverfirstName') !== undefined && component.find('caregiverlastname') !== undefined) {
            var firstname = component.find('caregiverfirstName').get("v.value");
            var lastName = component.find('caregiverlastname').get("v.value");
            if (firstname && lastName) {
                component.set("v.PhoneEmailRequired", true);
                component.set("v.FirstnameLastName", false);
            } else if (firstname || lastName) {
                component.set("v.FirstnameLastName", true);
            } else {
                component.set("v.FirstnameLastName", false);
            }
        }
    },
    getPicklistDependencies: function(component, event, helper) {
        var action = component.get("c.getDependentOptions");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.dependentFieldMap", storeResponse);
                var Address = component.get("v.Address");
                var country = component.get("v.Address").PatientConnect__PC_Country__c;
                if (country !== undefined && country !== null) {
                    helper.onControllerFieldChangeHelper(component, helper, country);
                }
            }
        });
        $A.enqueueAction(action);
    },
    onControllerFieldChangeHelper: function(component, helper, controllerValueKey) {
        // get the map values   
        var dependentPicklistMap = component.get("v.dependentFieldMap");
        var pickListValNone = "--None--";
        // check if selected value is not equal to None then call the helper function.
        // if controller field value is none then make dependent field value is none and disable field
        if (controllerValueKey !== pickListValNone) {
            // get dependent values for controller field by using map[key].  
            var dependentPicklistEntries = dependentPicklistMap[controllerValueKey];

            helper.updateDependentStates(component, dependentPicklistEntries);
        } else {
            var defaultVal = [{
                class: "optionClass",
                label: "--None--",
                value: ''
            }];

            component.find('stateId').set("v.options", defaultVal);
            component.set("v.isDependentDisable", true);
        }
    },
    updateDependentStates: function(component, statePicklistEntries) {
        // create a empty array var for store dependent picklist values for controller field)  
        var dependentOptions = [];
        var state = component.get("v.Address").PatientConnect__PC_State__c;

        dependentOptions.push({
            class: "optionClass",
            label: "--None--",
            value: ""
        });

        if (statePicklistEntries !== undefined && statePicklistEntries.length > 0) {
            for (var i = 0; i < statePicklistEntries.length; i++) {
                dependentOptions.push({
                    class: "optionClass",
                    label: statePicklistEntries[i].label,
                    value: statePicklistEntries[i].value
                });
            }

            // set the dependentFields variable values to State(dependent picklist field) on ui:inputselect
        }
        component.find('stateId').set("v.options", dependentOptions);
        component.find('stateId').set("v.value", state);

        // make disable false for ui:inputselect field 
        component.set("v.isDependentDisable", false);
    },
    getSelectedCaregiverDetails: function(component, event, helper) {
        var caregiverId = component.get("v.selectedLookupValueID");
        if (caregiverId.length > 1) {

            var action = component.get("c.getCaregiverAccountDetails");
            action.setParams({
                "caregiverId": caregiverId
            });
            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    var account = component.get("v.caregiverAccount");
                    account.PatientConnect__PC_Caregiver_First_Name__c = storeResponse.firstName;
                    account.PatientConnect__PC_Caregiver_Last_Name__c = storeResponse.lastName;
                    account.spc_Caregiver_Date_of_Birth__c = storeResponse.dob;
                    account.spc_Caregiver_Gender__c = storeResponse.gender;
                    account.PatientConnect__PC_Email__c = storeResponse.email;
                    account.Phone = storeResponse.phone;
                    account.spc_Home_Phone__c = storeResponse.homePhone;
                    account.spc_Office_Phone__c = storeResponse.officePhone;
                    account.PatientConnect__PC_Communication_Language__c = storeResponse.communicationLanguage;
                    account.spc_Caregiver_Preferred_Time_to_Contact__c = storeResponse.preferredTimeToContact;
                    account.PatientConnect__PC_Caregiver_Relationship_to_Patient = storeResponse.caregiverRelationshipToPatient;
                    account.spc_Permission_to_Call__c = storeResponse.permissionToCall;
                    account.spc_Permission_to_Email__c = storeResponse.permissionToEmail;
                    account.spc_Permission_from_Patient_to_Share_PHI__c = storeResponse.permissionToSharePHI;
                    account.spc_Date_Permission_to_Share_PHI__c = storeResponse.permissionToSharePHIDate;
                    account.notes = storeResponse.notes;

                    if (!$A.util.isEmpty(storeResponse.primaryAddress)) {
                        var address = component.get("v.Address");

                        address.addressType = storeResponse.addressType;
                        address.PatientConnect__PC_Address_1__c = storeResponse.address1;
                        address.PatientConnect__PC_Address_2__c = storeResponse.address2;
                        address.PatientConnect__PC_Address_3__c = storeResponse.address3;
                        address.PatientConnect__PC_Country__c = storeResponse.country;
                        var dependentPicklistMap = component.get("v.dependentFieldMap");
                        var dependentPicklistEntries = dependentPicklistMap[storeResponse.country];
                        helper.updateDependentStates(component, dependentPicklistEntries);
                        address.PatientConnect__PC_State__c = storeResponse.state;
                        address.PatientConnect__PC_City__c = storeResponse.city;
                        address.PatientConnect__PC_Zip_Code__c = storeResponse.zipCode;
                        address.primary = storeResponse.primary;
                        component.set("v.Address", address);
                        if (address.PatientConnect__PC_Country__c !== '' && address.PatientConnect__PC_State__c !== '') {
                            this.getPicklistDependencies(component, event, helper);
                            component.set("v.isDependentDisable", false);
                        }
                    }
                    component.set("v.caregiverAccount", account);
                    helper.setvalidation(component, event, helper);
                }
            });
            $A.enqueueAction(action);
        }

    },
    resetAll: function(component, event, helper) {

        var account = component.get("v.caregiverAccount");
        account.PatientConnect__PC_Caregiver_First_Name__c = '';
        account.PatientConnect__PC_Caregiver_Last_Name__c = '';
        account.spc_Caregiver_Date_of_Birth__c = '';
        account.spc_Caregiver_Gender__c = '';
        account.PatientConnect__PC_Email__c = '';
        account.Phone = '';
        account.spc_Home_Phone__c = '';
        account.spc_Office_Phone__c = '';
        account.PatientConnect__PC_Communication_Language__c = '';
        account.spc_Caregiver_Preferred_Time_to_Contact__c = '';
        account.PatientConnect__PC_Caregiver_Relationship_to_Patient = '';
        account.spc_Permission_to_Call__c = '';
        account.spc_Permission_to_Email__c = '';
        account.spc_Permission_from_Patient_to_Share_PHI__c = '';
        account.spc_Date_Permission_to_Share_PHI__c = '';
        account.notes = '';

        var address = component.get("v.Address");
        address.id = '';
        address.addressType = '';
        address.PatientConnect__PC_Address_1__c = '';
        address.PatientConnect__PC_Address_2__c = '';
        address.PatientConnect__PC_Address_3__c = '';
        address.PatientConnect__PC_Country__c = '';
        address.PatientConnect__PC_State__c = '';
        address.PatientConnect__PC_City__c = '';
        address.PatientConnect__PC_Zip_Code__c = '';
        address.primary = false;
        component.set("v.Address", address);
        this.getPicklistDependencies(component, event, helper);
        component.set("v.isDependentDisable", false);

        component.set("v.caregiverAccount", account);

    },
    clearErrorMessage: function(component, fieldArray) {

        for (var i = 0; i < fieldArray.length; i++) {
            if (fieldArray[i]) {
                $A.util.removeClass(fieldArray[i], "error");

                fieldArray[i].set("v.errors", [{
                    message: ""
                }]);
            }
        }

    }
})
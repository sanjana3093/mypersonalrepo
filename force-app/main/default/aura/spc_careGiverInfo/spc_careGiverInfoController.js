/*********************************************************************************
Class Name      : spc_careGiverInfo
Description     : component created to add Caregiver screen in EW
Created By      : Sanjana Tripathy
Created Date    : 19-June-18
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                    Description
-------------------------------9---------------------------------------------------            
Sanjana Tripathy              18-June-18              Initial Version
*********************************************************************************/

({
    doInit: function(component, event, helper) {
        helper.getPickListValues(component, event);
        helper.getPicklistDependencies(component, event, helper);

        setTimeout(function() {
            document.getElementById('form').scrollIntoView(true);
            helper.setvalidation(component, event);

        }, 3000);


    },
    showSpinner: function(component, event) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function(component, event) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, "slds-hide");
    },
    onControllerFieldChange: function(component, event, helper) {
        // get the selected value
        var controllerValueKey = event.getSource().get("v.value");
        helper.onControllerFieldChangeHelper(component, helper, controllerValueKey);

    },
    getSelectedCaregiverDetails: function(component, event, helper) {
        if (component.get("v.selectedLookupValueID") !== '') {
            component.set("v.caregiverAccount.caregiverLookUpId", component.get("v.selectedLookupValueID"));
            component.set("v.caregiverAccount.caregiverLookUpName", component.get("v.selectedLookupValue"));
            helper.getSelectedCaregiverDetails(component, event, helper);
        } else {
            component.set("v.caregiverAccount.caregiverLookUpId", '');
            component.set("v.caregiverAccount.caregiverLookUpName", '');
            component.set("v.PhoneEmailRequired", false);
            helper.resetAll(component, event, helper);
        }
    },
    setvalidation: function(component, event, helper) {


        var firstname = component.find('caregiverfirstName').get("v.value");
        if (firstname) {
            firstname = component.find('caregiverfirstName').get("v.value").trim()
        }
        var lastName = component.find('caregiverlastname').get("v.value");
        if (lastName) {
            lastName = component.find('caregiverlastname').get("v.value").trim()
        }
        if (firstname && lastName) {
            component.set("v.PhoneEmailRequired", true);
            component.set("v.FirstnameLastName", false);
        } else if (firstname || lastName) {
            component.set("v.FirstnameLastName", true);
        } else {
            component.set("v.FirstnameLastName", false);
            component.set("v.PhoneEmailRequired", false);
        }
    },
    validate: function(component, event, helper) {
        try {
            var PhoneEmailRequired = component.get("v.PhoneEmailRequired");
            var FirstnameLastName = component.get("v.FirstnameLastName");
            var caregiverphone = component.find("caregiverphone").get("v.value");
            var homePhone = component.find("homePhone").get("v.value");
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            var today = yyyy + '-' + mm + '-' + dd;
            var officePhone = component.find("officePhone").get("v.value");
            if (caregiverphone) {
                caregiverphone = component.find('caregiverphone').get("v.value").trim()
            }
            if (homePhone) {
                homePhone = component.find('homePhone').get("v.value").trim()
            }
            if (officePhone) {
                officePhone = component.find('officePhone').get("v.value").trim()
            }
            var email = component.find('email').get("v.value");
            var pageErrors = [];
            var validDate = /^\d{4}-\d{2}-\d{2}$/;
            var valid = true;
            var firstname = component.find('caregiverfirstName').get("v.value");
            var lastName = component.find('caregiverlastname').get("v.value");
            if (firstname) {
                firstname = component.find('caregiverfirstName').get("v.value").trim()
            }
            if (lastName) {
                lastName = component.find('caregiverlastname').get("v.value").trim()
            }
            var warining_email = component.find('warining_email');
            var phoneNumber = /^[0-9]$/;
            var phoneNumberValid = /^[0-9-+()*#\s]*$/;
            var validEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            var validName = /^[a-z A-Z-"'"]*$/;
            var phone = component.find('caregiverphone').get("v.value");
            var Officephone = component.find('officePhone').get("v.value");
            var Homephone = component.find('homePhone').get("v.value");
            var phone = component.find('caregiverphone').get("v.value");
            var Officephone = component.find('officePhone').get("v.value");
            var Homephone = component.find('homePhone').get("v.value");
            var lastNameCmp = component.find('caregiverlastname');
            var firstnameCmp = component.find('caregiverfirstName');
            var AddrLine1Cmp = component.find('AddrLine1');
            var cityCmp = component.find('city');
            var stateIdCmp = component.find('stateId');
            var countryIdCmp = component.find('countryId');
            var zipCodeCmp = component.find('zipCode');
            var PHIDateSubmissionCmp = component.find('PHIDate');
            if (component.find('permsnPhiId')) {
                var PHISubmission = component.find('permsnPhiId').get("v.value");
            }
            var phone = component.find('caregiverphone').get("v.value");
            var Officephone = component.find('officePhone').get("v.value");
            var Homephone = component.find('homePhone').get("v.value");
            var lastNameCmp = component.find('caregiverlastname');
            var firstnameCmp = component.find('caregiverfirstName');
            var AddrLine1Cmp = component.find('AddrLine1');
            var cityCmp = component.find('city');
            var stateIdCmp = component.find('stateId');
            var countryIdCmp = component.find('countryId');
            var zipCodeCmp = component.find('zipCode');
            var PHIDateSubmissionCmp = component.find('PHIDate');
            var PHISubmissionCmp = component.find('permsnPhiId');
            var phoneCmp = component.find('caregiverphone');
            var emailCmp = component.find('email');
            var OfficephoneCmp = component.find('officePhone');
            var HomephoneCmp = component.find('homePhone');
            var dateofBirthCmp = component.find('dob')
            helper.clearErrorMessage(component, [lastNameCmp, firstnameCmp, AddrLine1Cmp, cityCmp,
                stateIdCmp, countryIdCmp, zipCodeCmp, PHIDateSubmissionCmp, PHISubmissionCmp,
                phoneCmp, emailCmp, OfficephoneCmp, HomephoneCmp, dateofBirthCmp
            ]);
            if (phone) {
                phone = component.find('caregiverphone').get("v.value").trim()
            }
            var phoneCmp = component.find('caregiverphone');
            var emailCmp = component.find('email');
            var OfficephoneCmp = component.find('officePhone');
            var HomephoneCmp = component.find('homePhone');
            var dateofBirthCmp = component.find('dob');
            helper.clearErrorMessage(component, [lastNameCmp, firstnameCmp, AddrLine1Cmp, cityCmp,
                stateIdCmp, countryIdCmp, zipCodeCmp, PHIDateSubmissionCmp, PHISubmissionCmp,
                phoneCmp, emailCmp, OfficephoneCmp, HomephoneCmp, dateofBirthCmp
            ]);
            if (phone) {
                phone = component.find('caregiverphone').get("v.value").trim()
            }
            if (Homephone) {
                Homephone = component.find('homePhone').get("v.value").trim()
            }
            if (Officephone) {
                Officephone = component.find('officePhone').get("v.value").trim()
            }
            var dateofBirth = component.find('dob').get("v.value");
            var PHIDateSubmission = component.find('PHIDate').get("v.value");
            if (component.find('permsnPhiId')) {
                var PHISubmission = component.find('permsnPhiId').get("v.value");
            }

            var showWarning = component.get('v.showWarning');
            var caregiverAccount = component.get("v.caregiverAccount");
            component.set("v.pageErrors", pageErrors);
            component.set("v.isValid", valid);

            if (!$A.util.isEmpty(Officephone) && ((Officephone.match(phoneNumber) === null) && (Officephone.match(phoneNumberValid) === null))) {
                component.set("v.isValid", false);
                $A.util.addClass(OfficephoneCmp, "error");
                document.getElementById('form').scrollIntoView({
                    block: 'start',
                    behavior: 'smooth'
                });
                OfficephoneCmp.set("v.errors", [{
                    message: "Please enter a valid Office Phone number within 15 digits."
                }]);
                component.set("v.pageErrors", ["Please enter a valid Office Phone number within 15 digits."]);
            }
            if (!$A.util.isEmpty(Homephone) && ((Homephone.match(phoneNumber) === null) && (Homephone.match(phoneNumberValid) === null))) {
                component.set("v.isValid", false);
                document.getElementById('form').scrollIntoView({
                    block: 'start',
                    behavior: 'smooth'
                });
                $A.util.addClass(HomephoneCmp, "error");
                HomephoneCmp.set("v.errors", [{
                    message: "Please enter a valid Home Phone number within 15 digits."
                }]);
                component.set("v.pageErrors", ["Please enter a valid Home Phone number within 15 digits."]);
            }
            if (!$A.util.isEmpty(phone) && ((phone.match(phoneNumber) === null) && (phone.match(phoneNumberValid) === null))) {
                component.set("v.isValid", false);
                document.getElementById('form').scrollIntoView({
                    block: 'start',
                    behavior: 'smooth'
                });
                $A.util.addClass(phoneCmp, "error");
                phoneCmp.set("v.errors", [{
                    message: "Please enter a valid Mobile Phone number within 15 digits."
                }]);
                component.set("v.pageErrors", ["Please enter a valid Mobile number within 15 digits."]);
            }
            if (!$A.util.isEmpty(email) && (email.match(validEmail) === null)) {
                component.set("v.isValid", false);
                document.getElementById('form').scrollIntoView({
                    block: 'start',
                    behavior: 'smooth'
                });
                $A.util.addClass(emailCmp, "error");
                emailCmp.set("v.errors", [{
                    message: "Please enter a valid Email address."
                }]);
                component.set("v.pageErrors", ["Please enter a valid Email address."]);
            }

            if (!$A.util.isEmpty(firstname) && ((firstname.match(validName) === null) || firstname.length > 50)) {
                document.getElementById('form').scrollIntoView({
                    block: 'start',
                    behavior: 'smooth'
                });
                $A.util.addClass(firstnameCmp, "error");
                firstnameCmp.set("v.errors", [{
                    message: "Please enter a valid First Name of max 50 alphabets."
                }]);
                component.set("v.isValid", false);
                component.set("v.pageErrors", ["Please enter a valid First Name of max 50 alphabets."]);
            }
            if (!$A.util.isEmpty(lastName) && ((lastName.match(validName) === null) || lastName.length > 50)) {
                component.set("v.isValid", false);
                document.getElementById('form').scrollIntoView({
                    block: 'start',
                    behavior: 'smooth'
                });
                $A.util.addClass(lastNameCmp, "error");
                lastNameCmp.set("v.errors", [{
                    message: "Please enter a valid Last Name of max 50 alphabets."
                }]);
                component.set("v.pageErrors", ["Please enter a valid Last Name of max 50 alphabets."]);
            }
            var dates = (new Date(dateofBirth)).getDate();

            if (!$A.util.isEmpty(dateofBirth) && (dateofBirth.match(validDate) === null) || (!$A.util.isEmpty(dateofBirth) && isNaN(dates))) {
                component.set("v.isValid", false);
                document.getElementById('form').scrollIntoView({
                    block: 'start',
                    behavior: 'smooth'
                });
                $A.util.addClass(dateofBirthCmp, "error");
                dateofBirthCmp.set("v.errors", [{
                    message: "Please populate date with this format: MM/DD/YYYY."
                }]);
                component.set("v.pageErrors", ["Please populate date with this format: MM/DD/YYYY."]);
            }
            if (dateofBirth > today) {
                component.set("v.isValid", false);
                document.getElementById('form').scrollIntoView({
                    block: 'start',
                    behavior: 'smooth'
                });
                $A.util.addClass(dateofBirthCmp, "error");
                dateofBirthCmp.set("v.errors", [{
                    message: $A.get("$Label.c.spc_DOB_Cannot_be_Future_Date")
                }]);
                component.set("v.pageErrors", [$A.get("$Label.c.spc_DOB_Cannot_be_Future_Date")]);
            }
            var PHIdates = (new Date(PHIDateSubmission)).getDate();
            if (!$A.util.isEmpty(PHIDateSubmission) && (PHIDateSubmission.match(validDate) === null) || (!$A.util.isEmpty(PHIDateSubmission) && isNaN(PHIdates))) {
                component.set("v.isValid", false);
                $A.util.addClass(PHIDateSubmissionCmp, "error");
                PHIDateSubmissionCmp.set("v.errors", [{
                    message: "Please populate date with this format: MM/DD/YYYY."
                }]);
                component.set("v.pageErrors", ["Please populate date with this format: MM/DD/YYYY."]);
            }

            if (PHIDateSubmission > today) {
                component.set("v.isValid", false);
                $A.util.addClass(PHIDateSubmissionCmp, "error");
                PHIDateSubmissionCmp.set("v.errors", [{
                    message: $A.get("$Label.c.spc_phiConsentdateCannotbeFutureDate")
                }]);
                component.set("v.pageErrors", [$A.get("$Label.c.spc_phiConsentdateCannotbeFutureDate")]);
            }
            if (PHISubmission === 'Yes' && $A.util.isEmpty(PHIDateSubmission)) {
                component.set("v.isValid", false);
                $A.util.addClass(PHIDateSubmissionCmp, "error");
                PHIDateSubmissionCmp.set("v.errors", [{
                    message: "Please populate Date Permission to Share PHI Provided."
                }]);
                component.set("v.pageErrors", ["Please populate Date Permission to Share PHI Provided"]);
            }
            var isValid = component.get('v.isValid');
            if (isValid) {
                if (!FirstnameLastName) {
                    if (PhoneEmailRequired) {
                        if (email || caregiverphone || officePhone || homePhone) {
                            //component.set("v.isValid", true);
                        } else {
                            document.getElementById('form').scrollIntoView({
                                block: 'start',
                                behavior: 'smooth'
                            });
                            component.set("v.isValid", false);
                            $A.util.addClass(emailCmp, "error");
                            emailCmp.set("v.errors", [{
                                message: $A.get("$Label.c.spc_careiver_phone_email_validation")
                            }]);
                            $A.util.addClass(phoneCmp, "error");
                            phoneCmp.set("v.errors", [{
                                message: $A.get("$Label.c.spc_careiver_phone_email_validation")
                            }]);
                            $A.util.addClass(HomephoneCmp, "error");
                            HomephoneCmp.set("v.errors", [{
                                message: $A.get("$Label.c.spc_careiver_phone_email_validation")
                            }]);
                            $A.util.addClass(OfficephoneCmp, "error");
                            OfficephoneCmp.set("v.errors", [{
                                message: $A.get("$Label.c.spc_careiver_phone_email_validation")
                            }]);
                            component.set("v.pageErrors", [$A.get("$Label.c.spc_careiver_phone_email_validation")]);
                        }
                    }
                    if (component.find('addressType') && !$A.util.isEmpty(component.find('addressType').get("v.value"))) {

                        if ($A.util.isEmpty(component.find('zipCode').get("v.value"))) {
                            component.set("v.isValid", false);
                            $A.util.addClass(zipCodeCmp, "error");
                            zipCodeCmp.set("v.errors", [{
                                message: "Please populate Zip Code"
                            }]);
                            component.set("v.pageErrors", ["Please populate Zip Code"]);
                        }

                        if ($A.util.isEmpty(component.find('stateId').get("v.value"))) {
                            component.set("v.isValid", false);
                            $A.util.addClass(stateIdCmp, "error");
                            stateIdCmp.set("v.errors", [{
                                message: "Please populate State"
                            }]);
                            component.set("v.pageErrors", ["Please populate State"]);
                        }
                        if ($A.util.isEmpty(component.find('countryId').get("v.value"))) {
                            component.set("v.isValid", false);
                            $A.util.addClass(countryCmp, "error");
                            countryIdCmp.set("v.errors", [{
                                message: "Please populate Country."
                            }]);
                            component.set("v.pageErrors", ["Please populate Country"]);
                        }
                        if ($A.util.isEmpty(component.find('city').get("v.value"))) {
                            component.set("v.isValid", false);
                            $A.util.addClass(cityCmp, "error");
                            cityCmp.set("v.errors", [{
                                message: "Please populate City"
                            }]);
                            component.set("v.pageErrors", ["Please populate City"]);
                        }
                        if ($A.util.isEmpty(component.find('AddrLine1').get("v.value"))) {
                            component.set("v.isValid", false);
                            $A.util.addClass(address1cmp, "error");
                            address1cmp.set("v.errors", [{
                                message: "Please populate Address Line 1."
                            }]);
                            component.set("v.pageErrors", ["Please populate Address Line 1"]);
                        }
                    }
                } else {
                    if ($A.util.isEmpty(firstname)) {
                        component.set("v.isValid", false);
                        $A.util.addClass(firstnameCmp, "error");
                        firstnameCmp.set("v.errors", [{
                            message: $A.get("$Label.c.spc_Caregiver_First_Name")
                        }]);
                        component.set("v.pageErrors", [$A.get("$Label.c.spc_Caregiver_First_Name")]);
                    } else if ($A.util.isEmpty(lastName)) {
                        component.set("v.isValid", false);
                        $A.util.addClass(lastNameCmp, "error");
                        lastNameCmp.set("v.errors", [{
                            message: $A.get("$Label.c.spc_Caregiver_Last_Name")
                        }]);
                        component.set("v.pageErrors", [$A.get("$Label.c.spc_Caregiver_Last_Name")]);
                    } else {
                        component.set("v.isValid", false);
                        $A.util.addClass(lastNameCmp, "error");
                        lastNameCmp.set("v.errors", [{
                            message: $A.get("$Label.c.spc_caregiver_validation_name")
                        }]);
                        component.set("v.pageErrors", [$A.get("$Label.c.spc_caregiver_validation_name")]);
                    }
                }
            }

            caregiverAccount.PatientConnect__PC_Caregiver_First_Name__c = firstname;
            caregiverAccount.PatientConnect__PC_Caregiver_Last_Name__c = lastName;
            component.set("v.caregiverAccount", caregiverAccount);


            isValid = component.get('v.isValid');
        } catch (e) {
            console.log("error" + e);
        }

    },
    removeWarning: function(component, event, helper) {
        var warining_email = component.find('warining_email');
        $A.util.toggleClass(warining_email, 'slds-hide');
        component.set("v.showWarning", true);
    }
})
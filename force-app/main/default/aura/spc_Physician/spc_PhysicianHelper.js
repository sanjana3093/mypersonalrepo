({
    searchRecord: function(component, event, helper) {
        var searchString = component.get("v.searchString");
        if (searchString !== "" && searchString.length >= 2) {
            
            var action = component.get("c.searchRecords");
            action.setParams({
                'searchString': searchString
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                
                if (component.isValid() && state === "SUCCESS") {
                    var returnValue = response.getReturnValue();
                    component.set('v.searchResults', returnValue);
                    if (returnValue.length > 0) {
                        $A.util.addClass(component.find('searchResultMsg'), 'searchResultMsgBlockHidden');
                    } else {
                        $A.util.removeClass(component.find('searchResultMsg'), 'searchResultMsgBlockHidden')
                    }
                } else {
                    console.log("searchRecord: Failed with state: " + state);
                }
            });
            $A.enqueueAction(action);
        }
        
    },
    getHeaders: function(component, event, helper) {
        var action = component.get("c.getFields");
        action.setParams({
            
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set('v.physicianTableHeaders', returnValue);
            } else {
                console.log("searchRecord: Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        
        
    },
    getPicklistEntryMap: function(component) {
        var action = component.get("c.getPicklistEntryMap");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.picklistEntryMap", returnValue);
                console
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    getPicklistDependencies: function(component, event, helper) {
        var action = component.get("c.getDependentOptionsImpl");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.dependentFieldMap", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },
    
    updateDependentStates: function(component, statePicklistEntries) {
        // create a empty array var for store dependent picklist values for controller field)  
        var dependentOptions = [];
        dependentOptions.push({
            class: "optionClass",
            label: "--None--",
            value: ""
        });
        
        if (statePicklistEntries !== undefined && statePicklistEntries.length > 0) {
            for (var i = 0; i < statePicklistEntries.length; i++) {
                dependentOptions.push({
                    class: "optionClass",
                    label: statePicklistEntries[i].label,
                    value: statePicklistEntries[i].value
                });
            }
            
            // set the dependentFields variable values to State(dependent picklist field) on ui:inputselect
        }
        component.find('conState').set("v.options", dependentOptions);
        component.find('conState').set("v.value", state);
        
        // make disable false for ui:inputselect field 
        component.set("v.isDependentDisable", false);
    },
    onControllerFieldChangeHelper: function(component, helper, controllerValueKey) {
        // get the map values   
        var dependentPicklistMap = component.get("v.dependentFieldMap");
        var pickListValNone = "--None--";
        
        // check if selected value is not equal to None then call the helper function.
        // if controller field value is none then make dependent field value is none and disable field
        
        if (controllerValueKey !== pickListValNone) {
            
            // get dependent values for controller field by using map[key].  
            // for i.e "India" is controllerValueKey so in the map give key Name for get map values like 
            // map['India'] = its return all dependent picklist values.
            var dependentPicklistEntries = dependentPicklistMap[controllerValueKey];
            
            helper.updateDependentStates(component, dependentPicklistEntries);
        } else {
            var defaultVal = [{
                class: "optionClass",
                label: "--None--",
                value: ''
            }];
            
            component.find('conState').set("v.options", defaultVal);
            component.set("v.isDependentDisable", true);
        }
    },
    handleOnlineForm: function(component, enrollmentCaseId, helper) {
        var action = component.get("c.getApplicantPhysician"); //
        action.setParams({
            "enrollmentCaseId": enrollmentCaseId,
            "activeApplicantId": component.get("v.enrollmentCase.applicantId") //[PC-1379] Handles multiple applicants on enrollment case
        });
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var returnValue = a.getReturnValue();
                component.set("v.onlineApplicantPhysician", returnValue[0]);
                var onlineAppPhysician = component.get("v.onlineApplicantPhysician");
                var showRefTable;
                if ($A.util.isUndefinedOrNull(onlineAppPhysician) || onlineAppPhysician === '') {
                    showRefTable = false;
                } else {
                    showRefTable = true;
                }
                component.set("v.showRefTable", showRefTable);
                
                //Initialize search string with applicant's Physician name
                if (showRefTable) {
                    var initSearchString = returnValue[0].name;
                    //Physician name found, set the search string with Physician name
                    if (initSearchString !== null || initSearchString !== undefined) {
                        component.set("v.searchString", initSearchString);
                        
                        var searchString = component.get("v.searchString");
                        helper.searchRecord(component, event, helper);
                    }
                }
                
                
            } else if (a.getState() === "ERROR") {
                var errors = a.getError();
                console.log(JSON.stringify(errors));
                var errText = component.get("v.ErrorText");
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errText);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    getAddress: function(component, event, physicianId, helper) {
        var action = component.get("c.getPhysicianAddresses");
        var primaryaddressExists = 0;
        var returnValue;
        var selectedResult = component.get("v.selectedResult");
        action.setParams({
            "physicianId": physicianId
        });
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                returnValue = response.getReturnValue();
                component.set('v.searchAddressResults', returnValue);
                if (returnValue.length >= 1) {
                    for (var i = 0; i < returnValue.length; i++) {
                        if (returnValue[i].PatientConnect__PC_Primary_Address__c) {
                            //var radio=component.find('r'+returnValue[i].Id).
                            primaryaddressExists++;
                            component.set("v.primaryaddressIndex", i);
                        }
                    }
                    if (!primaryaddressExists) {
                        component.set("v.primaryaddressIndex", null);
                        returnValue[0].PatientConnect__PC_Primary_Address__c = "true";
                        component.set('v.searchAddressResults', returnValue);
                        
                    }
                }
                
                helper.addSelection(component, event, helper);
            } else {
                console.log("searchRecord: Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        
    },
    changeAddress: function(component, event, selectedAddress) {
        var selectedResult = component.get("v.selectedResult");
        var persistentAddress = component.get("v.persistentAddress");
        if (persistentAddress === null) {
            selectedResult.addressId = selectedAddress.Id;
        } else {
            selectedResult.addressId = "";
            persistentAddress.account = selectedResult.Id;
            component.set("v.persistentAddress", persistentAddress);
        }
        //
        selectedResult.primaryAddress1 = selectedAddress.PatientConnect__PC_Address_1__c;
        selectedResult.primaryAddress2 = selectedAddress.PatientConnect__PC_Address_2__c;
        selectedResult.primaryAddress3 = selectedAddress.PatientConnect__PC_Address_3__c;
        selectedResult.primaryZipCode = selectedAddress.PatientConnect__PC_Zip_Code__c;
        selectedResult.primaryCity = selectedAddress.PatientConnect__PC_City__c;
        selectedResult.primaryState = selectedAddress.PatientConnect__PC_State__c;
        selectedResult.phone1 = selectedAddress.spc_Phone__c;
        selectedResult.fax1 = selectedAddress.spc_Fax__c;
        selectedResult.phone2 = selectedAddress.spc_Phone_2__c;
        selectedResult.fax2 = selectedAddress.spc_Fax_2__c;
        
        if (selectedAddress.spc_Phone__c || selectedAddress.spc_Fax__c || selectedAddress.spc_Phone_2__c || selectedAddress.spc_Fax_2__c) {
            component.set("v.phoneFax", true);
        } else {
            component.set("v.phoneFax", false);
        }
        component.set("v.selectedResult", selectedResult);
        selectedResult = component.get("v.selectedResult");
        
    },
    onSelect: function(component, event, index) {
        var searchResult = component.get("v.searchResults");
        searchResult[index]['isSelected'] = "true";
        component.set("v.selectedResult", searchResult[index]);
        this.getAddress(component, event, searchResult[index].Id);
    },
    clearprevSelected: function(component, event, searchResult) {
        var i;
        for (i = 0; i < searchResult.length; i++) {
            searchResult[i]['isSelected'] = "false";
            if( searchResult[i].prevVal==="false"){
                searchResult[i].remsID='';
            }
        }
        component.set("v.searchResults", searchResult);
    },
    
    
    setAddressLabels: function(component, address) {
        var picklistEntryMap = component.get("v.picklistEntryMap");
        var countryPicklist = picklistEntryMap.countries;
        var statusPicklist = picklistEntryMap.addrStatus;
        var countryStateMap = component.get("v.dependentFieldMap");
        
        if ($A.util.isEmpty(address.country) || address.country === "__none") {
            address.countryLabel = "";
        } else {
            for (var i = 0; i < countryPicklist.length; i++) {
                if (countryPicklist[i].value === address.country) {
                    address.countryLabel = countryPicklist[i].label;
                    break;
                }
            }
        }
        
        if ($A.util.isEmpty(address.state) || address.state === "__none") {
            address.stateLabel = "";
        } else {
            var statePicklist = countryStateMap[address.country];
            
            for (var i = 0; i < statePicklist.length; i++) {
                if (statePicklist[i].value === address.state) {
                    address.stateLabel = statePicklist[i].label;
                    break;
                }
            }
            
            if ($A.util.isEmpty(address.stateLabel)) {
                address.stateLabel = address.state;
            }
        }
        
        if ($A.util.isEmpty(address.status) || address.status === "__none") {
            address.statusLabel = "";
        } else {
            for (var i = 0; i < statusPicklist.length; i++) {
                if (statusPicklist[i].value === address.status) {
                    address.statusLabel = statusPicklist[i].label;
                }
            }
        }
    },
    initailizeAddress: function(component, source, flag) {
        var initailizeAddress = {
            account: "",
            address: "",
            addressType: "",
            PatientConnect__PC_Address_1__c: "",
            PatientConnect__PC_State__c: "",
            stateLabel: "",
            PatientConnect__PC_Address_2__c: "",
            PatientConnect__PC_Zip_Code__c: "",
            PatientConnect__PC_Address_3__c: "",
            country: "",
            countryLabel: "",
            PatientConnect__PC_City__c: "",
            status: "",
            statusLabel: "",
            primary: false,
            lastModifiedDate: "",
            source: source,
            flag: flag,
            dataChk: false,
            isAddressSelected: false
        };
        component.set("v.isDependentDisable", true);
    },
    validateAddressFields: function(component, fieldArray) {
        var requiredFieldErrorMessage = $A.get("$Label.c.spc_FieldError_Required_Fields");
        for (var i = 0; i < fieldArray.length; i++) {
            if (($A.util.isEmpty(component.find(fieldArray[i]).get("v.value"))) || (component.find(fieldArray[i]).get("v.value") === '__none')) {
                var inputCmp = component.find(fieldArray[i]);
                inputCmp.set("v.errors", [{ message: requiredFieldErrorMessage }]);
                component.set("v.errorCheck", false);
            }
        }
    },
    clearErrorMessage: function(component, fieldArray) {
        for (var i = 0; i < fieldArray.length; i++) {
            var inputCmp = component.find(fieldArray[i]);
            inputCmp.set("v.errors", [{ message: "" }]);
        }
    },
    addSelection: function(component, event, helper) {
        
        var physicianIndex = component.get("v.physicianIndex");
        var addressIndex = component.get("v.addressIndex");
        var primaryaddressIndex = component.get("v.primaryaddressIndex");
        var searchResult = component.get("v.searchResults");
        searchResult[physicianIndex]['isSelected'] = "true";
        component.set("v.persistentAddress", null);
        component.set("v.searchResults", searchResult);
        component.set("v.selectedResult", searchResult[physicianIndex]);
        var searchAddressResults = component.get("v.searchAddressResults");
        if (searchAddressResults.length > 0) {
            if (primaryaddressIndex !== null && primaryaddressIndex !== undefined) {
                helper.changeAddress(component, event, searchAddressResults[primaryaddressIndex]);
            }else{
                helper.changeAddress(component, event, searchAddressResults[0])
            }
            if (addressIndex !== null && addressIndex !== undefined) {
                helper.changeAddress(component, event, searchAddressResults[addressIndex]);
            } else {
                
                if (!searchResult[physicianIndex].addressId) {
                    helper.changeAddress(component, event, searchAddressResults[0])
                }
                
            }
        }
        
        searchResult = component.get("v.searchResults");
        
    },
    addSelectedAddress: function(component, event, selectedResult) {
        var searchAddressResults = {
            account: "",
            address: "",
            addressType: "",
            PatientConnect__PC_Address_1__c: "",
            PatientConnect__PC_State__c: "",
            stateLabel: "",
            PatientConnect__PC_Address_2__c: "",
            PatientConnect__PC_Zip_Code__c: "",
            PatientConnect__PC_Address_3__c: "",
            PatientConnect__PC_City__c: "",
            spc_Fax__c: "",
            spc_Phone__c: "",
            spc_Phone_2__c: "",
            spc_Fax_2__c: ""
            
        };
        
        searchAddressResults.PatientConnect__PC_Primary_Address__c = "true";
        searchAddressResults.PatientConnect__PC_Address_1__c = selectedResult.primaryAddress1;
        searchAddressResults.PatientConnect__PC_City__c = selectedResult.primaryCity;
        searchAddressResults.PatientConnect__PC_State__c = selectedResult.primaryState;
        searchAddressResults.PatientConnect__PC_Zip_Code__c = selectedResult.primaryZipCode;
        component.set("v.searchAddressResults", searchAddressResults);
        if (selectedResult.assnPhone === selectedResult.phone1) {
            if (component.find('phone1')) {
                component.find('phone1').set("v.value", true);
            }
        } else if (selectedResult.assnPhone === selectedResult.phone2) {
            if (component.find('phone2')) {
                component.find('phone2').set("v.value", true);
            }
        } else if (selectedResult.assnPhone) {
            component.set("v.addnewPhone", true);
            component.find('newPhone').set("v.value", true);
        }
        if (selectedResult.assnFax === selectedResult.fax1) {
            if (component.find('fax1')) {
                component.find('fax1').set("v.value", true);
            }
        } else if (selectedResult.assnFax === selectedResult.fax2) {
            if (component.find('fax2')) {
                component.find('fax2').set("v.value", true);
            }
        } else if (selectedResult.assnFax) {
            component.set("v.addNewFax", true);
            component.find('newFax').set("v.value", true);
        }
        if(selectedResult.prevVal==="true"){
            component.set("v.phyRemsIdExist",true);
        }
        
    },
    
    clearSelection: function(component, event) {
        var buttons = ['phone2', 'fax1', 'fax2', 'phone1', 'newPhone', 'newFax'];
        var i;
        var buttonEl;
        for (i = 0; i < buttons.length; i++) {
            if (component.find(buttons[i])) {
                buttonEl = component.find(buttons[i]);
                if (buttonEl) {
                    if (Array.isArray(buttonEl)) {
                        buttonEl[0].set("v.value", false);
                    } else {
                        buttonEl.set("v.value", false);
                    }
                }
                
            }
        }
        
    }
})
({
    doInit: function(component, event, helper) {
        helper.getHeaders(component, event, helper);

        if (component.get("v.enrollmentCase.isOnlineApplicant")) {
            var enrollmentCaseId = component.get("v.enrollmentCase.enrollmentId");
            helper.handleOnlineForm(component, enrollmentCaseId, helper);
        }

        if (component.get("v.selectedResult") !== null) {
            var selectedResult = component.get("v.selectedResult");
            helper.addSelectedAddress(component, event, selectedResult);
            var searchResults = component.get("v.searchResults");
            searchResults.push(selectedResult);
            component.set("v.searchResults", searchResults);
            component.set("v.accountId" , selectedResult.Id);
        } else {
            helper.searchRecord(component, event, helper);
        }
        helper.getPicklistEntryMap(component);
        helper.getPicklistDependencies(component, event, helper);

    },
    searchEvents: function(component, event, helper) {
        if (event.getParams().keyCode === 13) {
            var searchString = component.get("v.searchString");
            if (searchString !== "" && searchString.trim().length >= 2) {
                helper.searchRecord(component, event, helper);
            }
        }
    },
	showSpinner: function(component, event) {
        var spinner = component.find('spinner');
         $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function(component, event) {
        var spinner = component.find('spinner');
         $A.util.addClass(spinner, "slds-hide");
    },
    clearSelection: function(component, event, helper) {
        var x;
        var y;
        component.set("v.searchResults", x);
        component.set("v.selectedResult", y);
    },
    searchRecord: function(component, event, helper) {
        component.set("v.pageErrors", []);
        var searchString = component.get("v.searchString");
        if (searchString !== "" && searchString.trim().length >= 2) {
            helper.searchRecord(component, event, helper);
        } else {
            var pageErrors = component.get("v.pageErrors");
            pageErrors.push($A.get("$Label.PatientConnect.PC_FieldError_SearchString"));
            component.set("v.pageErrors", pageErrors);
        }
    },
    onSelect: function(component, event, helper) {
        var index = event.getSource().get("v.text");
        var searchResult = component.get("v.searchResults");
        searchResult[index]['isSelected'] = "true";
        component.set("v.selectedResult", searchResult[index]);
        helper.getAddress(component, event, searchResult[index].Id, helper);
    },
    OpenVeevaModal: function(component, event, helper) {
        var searchResult = component.get("v.searchResults");
        var index = event.currentTarget.id;
        component.set("v.physicianIndex", index);
        component.set("v.addressIndex", null);
        component.set("v.phoneFax", false);
        component.set("v.accountId", searchResult[index].Id);
       if(searchResult[index].remsID){
            component.set("v.phyRemsIdExist",true);
            searchResult[index].prevVal="true";
        }else{
            component.set("v.phyRemsIdExist",false);
            searchResult[index].prevVal="false";
        }
        helper.clearprevSelected(component, event, searchResult);
        helper.getAddress(component, event, searchResult[index].Id, helper);

    },

    onSelectAddress: function(component, event, helper) {
        var index = event.getSource().get("v.text");
        component.set("v.addressIndex", index);
        var selectedResults=component.get("v.selectedResult");
        selectedResults.assnFax='';
        selectedResults.assnPhone='';
        component.set("v.selectedResult",selectedResults);
        component.set("v.addnewPhone", false);
        component.set("v.addNewFax", false);
        helper.clearSelection(component,event);
        helper.addSelection(component, event, helper);

    },

    onControllerFieldChange: function(component, event, helper) {
        // get the selected value
        var controllerValueKey = event.getSource().get("v.value");
        helper.onControllerFieldChangeHelper(component, helper, controllerValueKey);

    },
    validate: function(component, event, helper) {
        var selectedResult = component.get("v.selectedResult");
        component.set("v.isValid", true);
        var phoneNumberValid = /^[0-9-+()*#\s]*$/;
        var phoneNumber = /^[0-9]$/;
        var addnewPhone = component.get("v.addnewPhone");
        var addNewFax = component.get("v.addNewFax");
        if (addnewPhone) {
            var phone = component.find('newAssnPhone').get("v.value");
            if (!$A.util.isEmpty(phone) && ((phone.match(phoneNumber) === null) && (phone.match(phoneNumberValid) === null))) {
                component.set("v.isValid", false);
                $A.util.addClass( component.find('newAssnPhone'), "error");
                component.find('newAssnPhone').set("v.errors",[{ message: "Please enter a valid Phone number." }])
                component.set("v.pageErrors", ["Please enter a valid Phone number."]);
            }
        }
        

    },
    addNewPhone: function(component, event, helper) {
        var id = event.getSource().get("v.text");
        var val = component.find('newPhone').get("v.value");
        if (id === 'newPhone') {
            component.set("v.addnewPhone", true);
             component.find('newAssnPhone').set("v.value",'');
        } else {
            component.set("v.addnewPhone", false);
        }

        var selectedResult = component.get("v.selectedResult");
        if (id === 'phone1') {
            selectedResult.assnPhone = selectedResult.phone1;
        } else if (id === 'phone2') {
            selectedResult.assnPhone = selectedResult.phone2;
        }

        component.set("v.selectedResult", selectedResult);

    },
    addNewFax: function(component, event, helper) {
        var id = event.getSource().get("v.text");
        var val = component.find('newFax').get("v.value");
        var selectedResult = component.get("v.selectedResult");
        if (id === 'newFax') {
            component.set("v.addNewFax", true);
            component.find('newAssnFax').set("v.value",'');
        } else {
            component.set("v.addNewFax", false);
        }
        
        if (id === 'fax1') {
            selectedResult.assnFax = selectedResult.fax1;
        } else if (id === 'fax2') {
            selectedResult.assnFax = selectedResult.fax2;
        }
        component.set("v.selectedResult", selectedResult);

    }
})
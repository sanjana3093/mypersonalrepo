({
    getPicklistEntryMap : function(component){
        var action = component.get("c.getPicklistEntryMap");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.picklistEntryMap", returnValue);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    getFieldLabels : function(component){
        var action = component.get("c.getFieldLabels");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.myFieldLabels", returnValue);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    getAccount : function(component, event, helper){
        var action = component.get("c.getAccountorApplicant");
        action.setParams({
            "patientId"     : component.get("v.enrollmentCase.accountId"),
            "enrollmentId"  : component.get("v.enrollmentCase.enrollmentId"),
            "activeApplicantId" : component.get("v.enrollmentCase.applicantId"), //[PC-1379] Handles multiple applicants on enrollment case
            "fieldSetName"  : component.get("v.fieldSetName"),
            "ignoreFields": component.get("v.ignoreFields"),
            "replaceTarget":component.get("v.replaceTarget"),
            "replaceText": component.get("v.replaceText")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var existingAccount = response.getReturnValue();                  
                /* 
                * Getting the list of addresses related to account or applicant
                * To do - Handle the scenario where the user wants to remove the all address (from online form as well as account)?? 
                */
                var callUpdateActionIndicators = false;
                var onlineFormAddress = component.get("v.onlineFormAddress");
                //Adding segregation logic for online versus 'Existing or System stored address

                if (existingAccount.addrs.length > 0) {
                    for (var j = 0; j < existingAccount.addrs.length; j++) {
                        var address = existingAccount.addrs[j];

                        if(!$A.util.isEmpty(address.applicantSourceId)) {
                            if (address.source === $A.get("$Label.c.spc_Applicant_Address")) {
                                callUpdateActionIndicators = true;
                            }

                            helper.setAddressLabels(component, address);
                            onlineFormAddress.push(address);
                        }
                    }                    
                }
				 if(existingAccount.remsId){
                    component.set("v.remsIdCheck",true);
                }
                if (onlineFormAddress.length > 0) {
                    component.set("v.onlineFormAddress", onlineFormAddress);
                    component.set("v.showAddressRefTable", true);
                }

                var persistentAddress   = component.get("v.persistentAddress");
                var isAddrModified = false;
                
                if (persistentAddress !== null && persistentAddress.length === 0) {
                        
                    if (existingAccount.addrs.length > 0) {
                        
                        for (var j = 0; j < existingAccount.addrs.length; j++) {
                            var address = existingAccount.addrs[j];

                            if($A.util.isEmpty(address.applicantSourceId)) {
                                helper.setAddressLabels(component, address);
                                persistentAddress.push(address);
                            }
                        }
                    }
                    component.set("v.persistentAddress", persistentAddress);                
                } else {
                    if (existingAccount.addrs.length > 0) {
                        for(var i = 0 ; i < persistentAddress.length ; i++){
                            for(var j = 0; j < existingAccount.addrs.length; j++){
                                if(persistentAddress[i].address === existingAccount.addrs[j].address && persistentAddress[i].lastModifiedDate !== existingAccount.addrs[j].lastModifiedDate){
                                    helper.setAddressLabels(component, existingAccount.addrs[j]);
                                    persistentAddress[i] = existingAccount.addrs[j];
                                    isAddrModified = true;
                                } 
                            }
                        }

                        component.set("v.persistentAddress", persistentAddress);

                        //If even one of applicant's address was modified, show warning message exactly once [PC-1499]
                        if( isAddrModified) {
                            var pageWarnings = component.get("v.pageWarnings");
                            pageWarnings.push(helper.getAddressPageError(component));
                            component.set("v.pageWarnings", pageWarnings);
                        }
                    } 
                } 
                /*
                 * Getting the account or applicant info   
                 */
                var persistentAccount = component.get("v.persistentAccount");
                //[PC-1287] For an online enrollment form, if an account exists, display warning message 
                if(component.get("v.enrollmentCase.isOnlineApplicant") && existingAccount.Id !== null && existingAccount.Id !== undefined){
                    var pageWarnings = component.get("v.pageWarnings");
                    pageWarnings.push(helper.getOnlineFormWarning(component));
                    component.set("v.pageWarnings",pageWarnings);
                }
                if(existingAccount !== null){
                    if(persistentAccount !== null){
                        if(persistentAccount.firstName === ''){
                            persistentAccount.firstName = existingAccount.firstName;
                        }
                        if(persistentAccount.lastName === ''){
                            persistentAccount.lastName = existingAccount.lastName;
                        }
                        if(persistentAccount.dob === ''){
                            persistentAccount.dob = existingAccount.dob;
                        }
                        component.set("v.persistentAccount",persistentAccount);
                    }
                }
                if(persistentAccount === null) {
                    component.set("v.persistentAccount",existingAccount);//When no saved account info is available
                    
                } else { //[PC-1440] Account modified warning shown only if accounts are updated, not if applicants are updated
                    if(existingAccount.Id !== null && existingAccount.Id !== undefined &&
                       existingAccount.lastModifiedDate !== persistentAccount.lastModifiedDate){ 
                        component.set("v.persistentAccount",existingAccount);
                        var pageWarnings = component.get("v.pageWarnings");
                        
                    }                         
                }
                
            } else {
                console.log("PC_PatientInfoEWPHelper.getAccount failed with state: " + state);
            }            
            if (onlineFormAddress.length && persistentAddress.length) {
                helper.updateActionIndicators(component);
            }
            
        });
        $A.enqueueAction(action);
    }, 
    
    initailizeAddress : function(component, source, flag){
        var initailizeAddress = {
            account             : "",
            address             : "",
            addressType         : "",
            address1            : "",
            state               : "",
            stateLabel          : "",
            address2            : "",
            zipCode             : "",
            address3            : "",
            country             : "",
            countryLabel        : "",
            city                : "",
            status              : "",
            statusLabel         : "",
            primary             : false,
            lastModifiedDate    : "",
            source              : source,
            flag                : flag,
            dataChk             : false,
            isAddressSelected   : false
        };
        component.set("v.newAddress",initailizeAddress);
        component.set("v.isDependentDisable", true);
    },
    
    clearErrorMessage : function (component, fieldArray){
        for (var i = 0; i < fieldArray.length; i++){
            var inputCmp = component.find(fieldArray[i]);
            inputCmp.set("v.errors", [{message:""}]);
        }
    },
    
    validateAddressFields : function(component, fieldArray) {
        var requiredFieldErrorMessage = $A.get("$Label.c.spc_FieldError_Required_Fields");
        for (var i = 0; i < fieldArray.length; i++){            
            if(($A.util.isEmpty(component.find(fieldArray[i]).get("v.value"))) || (component.find(fieldArray[i]).get("v.value")==='__none')){
                var inputCmp = component.find(fieldArray[i]);
                inputCmp.set("v.errors", [{message:requiredFieldErrorMessage}]);
                component.set("v.errorCheck",false);
            }
        }        
    },      
    
    validatePrimaryAddress : function(component) {
        var address = component.get("v.persistentAddress");
        var count   = 0;
        for(var i = 0; i < address.length; i++)
        {   
            if(address[i].primary === true)
            {
                count = count + 1;
            }
        }
        component.set("v.countPrimary",count);
    },
    setAddressLabels : function(component, address) {
        var picklistEntryMap = component.get("v.picklistEntryMap");
        var countryPicklist = picklistEntryMap.countries;
        var statusPicklist = picklistEntryMap.addrStatus;
        var countryStateMap = component.get("v.dependentFieldMap");

        if ($A.util.isEmpty(address.country) || address.country === "__none") {
            address.countryLabel = "";
        } else {
            for (var i = 0; i < countryPicklist.length; i++) {
                if (countryPicklist[i].value === address.country) {
                    address.countryLabel = countryPicklist[i].label;
                    break;
                }
            }
        }

        if ($A.util.isEmpty(address.state) || address.state === "__none") {
            address.stateLabel = "";
        } else {
            var statePicklist = countryStateMap[address.country];

            for (var i = 0; i < statePicklist.length; i++) {
                if (statePicklist[i].value === address.state) {
                    address.stateLabel = statePicklist[i].label;
                    break;
                }
            }

            if ($A.util.isEmpty(address.stateLabel)) {
                address.stateLabel = address.state;
            }
        }

        if ($A.util.isEmpty(address.status) || address.status === "__none") {
            address.statusLabel = "";
        } else {
            for (var i = 0; i < statusPicklist.length; i++) {
                if (statusPicklist[i].value === address.status) {
                    address.statusLabel = statusPicklist[i].label;
                }
            }
        }
    },

    setConfiguration : function(component) {
        var configString = component.get("v.config");
        if (configString !== null && configString !== undefined) {
            var config = JSON.parse(configString);
            
            var fieldSetName = config["AdditionalInfo"]["fieldSet"];
            var ignoreFields = config["AdditionalInfo"]["ignoreFields"];
            var replaceTarget = config["AdditionalInfo"]["replaceTarget"];
            var replaceText = config["AdditionalInfo"]["replaceText"];
            if (!$A.util.isUndefinedOrNull(fieldSetName)) {
                
                component.set("v.fieldSetName", fieldSetName);
            }
            if (!$A.util.isUndefinedOrNull(ignoreFields)) {
                var ignoreFields_WithNamespace = [];
                for(var i=0; i<ignoreFields.length; i++) {
                   
                    ignoreFields_WithNamespace.push(ignoreFields[i]);
                }
                component.set("v.ignoreFields", ignoreFields_WithNamespace);
            }
            if (!$A.util.isUndefinedOrNull(replaceTarget)) {
                component.set("v.replaceTarget", replaceTarget);
            }
            if (!$A.util.isUndefinedOrNull(replaceText)) {
                component.set("v.replaceText", replaceText);
            }
        }
    },
    
    setNamespace : function(component) {
        var component_to_string = component.toString();
        var markupTagLoc = component_to_string.indexOf('markup://');
        var endOfNamespaceLoc = component_to_string.indexOf(':',markupTagLoc+9);
        var ns = component_to_string.substring(markupTagLoc+9,endOfNamespaceLoc);
        var namespacePrefix = ns === "c" ?  namespacePrefix = "" :  namespacePrefix = ns + "__";
        component.set("v.namespace", ns);
        component.set("v.namespacePrefix", namespacePrefix);
    },
    
    setFieldSetObject : function(cmp, event, helper) {
        if (cmp.get("v.fieldSetName") !== null) {
            var action = cmp.get('c.getFieldSetFields');
            action.setParams({
                fsName: cmp.get('v.fieldSetName'),
                typeName: cmp.get('v.typeName')
            });
            action.setCallback(this, 
                               function(response) {
                                  
                                   var fields = response.getReturnValue();
                                   var renamedFields = {};
                                   var obj = {};
                                   if(fields !== null && fields.length > 0){
                                       helper.setDefaultValueForFieldSetFields(cmp, event, helper, fields);
                                       for (var key in fields) {
                                           if (fields.hasOwnProperty(key)) {
                                               obj[fields[key].fieldPath.replace('.','___')] = '';
                                               }
                                       }
                                   }
                                   else {
                                       console.log('No fields found from fieldset');
                                   }
                                                                    
                                   helper.createFieldSetCmp(cmp, event, helper, fields);
                               }
                              );
            $A.enqueueAction(action);
        }
    },
    
    createFieldSetCmp : function(cmp, event, helper, fields) {
        $A.createComponent(
            cmp.get("v.namespace") + ":PC_FieldSetForm",
            {
                "fsName": cmp.get("v.fieldSetName"),
                "typeName": cmp.get("v.typeName"),
                "record": cmp.getReference("v.persistentAccount.fsFields"),
                "isValid" : cmp.getReference("v.isFieldSetFormValid")
            },
            function(newCmp, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = cmp.get("v.fieldSetFormBody");
                    body.push(newCmp);
                    cmp.set("v.fieldSetFormBody", body);
                    cmp.set("v.fieldSetFields", fields);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                }
                    else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                    }
            }
        );
    },
    setDefaultValueForFieldSetFields : function(cmp, event, helper, fsFields) { 
       
        var persistentAccount = cmp.get("v.persistentAccount");
        var currentAccFsFields = persistentAccount.fsFields;
        if($A.util.isEmpty(fsFields)) {
            
        }
        else {
            var HpField = '';
            for(var i=0; i<fsFields.length; i++) {
                HpField = fsFields[i].fieldPath;
                if($A.util.isEmpty(currentAccFsFields[HpField])) {
                   currentAccFsFields[HpField] = '';
                }
                else {
                    
                }
            }
            persistentAccount.fsFields = JSON.parse(JSON.stringify(currentAccFsFields));
            cmp.set("v.persistentAccount",persistentAccount);
        }
        
    },
    validateFieldSetForm :function(component) {
        
        component.set("v.isFieldSetFormValid",true);
        if (component.get("v.fieldSetFormBody") !== null && component.get("v.fieldSetFormBody").length > 0) {
            
            var fieldSetCmp = component.get("v.fieldSetFormBody")[0];
            fieldSetCmp.validate(); 
        }
        return component.get("v.isFieldSetFormValid");
    },
    
    getAddressPageError : function(component) {
        var pageError = $A.get("$Label.c.spc_EWP_PageError");
        var pageErrorReplacements = component.get("v._addressPageErrorReplacements");
        for(var replacement in pageErrorReplacements) {
            if(pageErrorReplacements.hasOwnProperty(replacement)) {
                pageError = pageError.replace(replacement,pageErrorReplacements[replacement]);
            }
        }
        console.log(pageError);
        return pageError;
    },
    
    getAccountPageError : function(component) {
        var pageError =  $A.get("$Label.c.spc_EWP_PageError");
        var pageErrorReplacements = component.get("v._accountPageErrorReplacements");
        for(var replacement in pageErrorReplacements) {
            if(pageErrorReplacements.hasOwnProperty(replacement)) {
                pageError = pageError.replace(replacement,pageErrorReplacements[replacement]);
            }
        }
        console.log(pageError);
        return pageError;
    },
    
    getOnlineFormWarning : function(component) {
        var pageWarning = component.get("v._onlineFormWarning");
        console.log(pageWarning);
        return pageWarning;
    },
    
    getPicklistDependencies: function(component, event, helper) {
        var action = component.get("c.getDependentOptionsImpl");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.dependentFieldMap", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },
    
    updateDependentStates: function(component, statePicklistEntries) {
        // create a empty array var for store dependent picklist values for controller field)  
        var dependentOptions = [];
        var state = component.get("v.newAddress").state;

            dependentOptions.push({
                class: "optionClass",
                label: component.get("v.noneOptionName"),
                value: ""
            });

        if (statePicklistEntries !== undefined && statePicklistEntries.length > 0) {
            for (var i = 0; i < statePicklistEntries.length; i++) {
                dependentOptions.push({
                    class: "optionClass",
                    label: statePicklistEntries[i].label,
                    value: statePicklistEntries[i].value
                });
            }

            // set the dependentFields variable values to State(dependent picklist field) on ui:inputselect
        }
        component.find('conState').set("v.options", dependentOptions);
        component.find('conState').set("v.value", state);

        // make disable false for ui:inputselect field 
        component.set("v.isDependentDisable", false);
    },
    onControllerFieldChangeHelper: function(component, helper, controllerValueKey) {
        // get the map values   
        var dependentPicklistMap = component.get("v.dependentFieldMap");
        var pickListValNone=component.get('v.noneOptionValue');

        // check if selected value is not equal to None then call the helper function.
        // if controller field value is none then make dependent field value is none and disable field

        if (controllerValueKey !== pickListValNone) {
            
            // get dependent values for controller field by using map[key].  
            // for i.e "India" is controllerValueKey so in the map give key Name for get map values like 
            // map['India'] its return all dependent picklist values.
            var dependentPicklistEntries = dependentPicklistMap[controllerValueKey];

            helper.updateDependentStates(component, dependentPicklistEntries);
        } else {
            var defaultVal = [{
                class: "optionClass",
                label: component.get("v.noneOptionName"),
                value: ''
            }];

            component.find('conState').set("v.options", defaultVal);
            component.set("v.isDependentDisable", true);
        }
    },
    
    showModalBox : function (component, event, helper) {

        var source      = $A.get("$Label.c.spc_Manually_Entered_Address");
        var flag        = "New";
        var pickListValNone=component.get('v.noneOptionValue');
        helper.initailizeAddress(component, source, flag);
        helper.onControllerFieldChangeHelper(component,helper,pickListValNone);
        component.set("v.button", true);
        component.set("v.addrModalOpen", true);
        helper.clearErrorMessage (component, ['address1','city']);
        var elementsbackGroundSection = component.find('aura_backGroundSectionId').getElement();
        var elementsnewAccountSection = component.find('aura_newAccountSectionId').getElement();
        elementsbackGroundSection.style.display = "block";
        elementsnewAccountSection.style.display = "block";
        event.preventDefault();
    },

    updateActionIndicators : function(component) {
        var getAddress  = component.get("v.onlineFormAddress");
        var setAddress  = component.get("v.persistentAddress");
        for(var i=0; i<getAddress.length; i++) {
            for(var j=0; j<setAddress.length; j++) {
                if(setAddress[j].applicantSourceId === getAddress[i].applicantSourceId) {
                    getAddress[i].isAddressSelected = true;
                    setAddress[j].isAddressSelected = true;
                    break;
                }
            }
        }
        component.set("v.newAddress", getAddress);
        component.get("v.persistentAddress",setAddress);
    },
    getPickListValNone : function(component, event, helper) {

        var action = component.get('c.getPickListValueNone');
        action.setCallback(this,function(response) {
             var state=response.getState();
             if('SUCCESS'===state){
                    var nonePickListVal=response.getReturnValue();
                    component.set('v.noneOptionValue',nonePickListVal);
                    }else{
                        console.log("Failed with state: " + state);
                    }
              });
        $A.enqueueAction(action);
        },
		 clearErrorMessageAll: function(component, fieldArray) {

        for (var i = 0; i < fieldArray.length; i++) {
            if(fieldArray[i]){
              $A.util.removeClass(fieldArray[i], "error");
            fieldArray[i].set("v.errors", [{ message: "" }]);
            }
        }
      
    }
})
({
     showSpinner: function(component, event) {
        var spinner = component.find('spinner');
         $A.util.removeClass(spinner, "slds-hide");
    },
//spinner methods
    hideSpinner: function(component, event) {
        var spinner = component.find('spinner');
         $A.util.addClass(spinner, "slds-hide");
    },

    doInit: function(component, event, helper) {
        helper.setNamespace(component);
        helper.setConfiguration(component);

        /*
         * Get All the picklist entries from schema
         */
        helper.getPicklistEntryMap(component);
        helper.getPicklistDependencies(component, event, helper);

        /*
         * Get the current existing Account
         */
        helper.getAccount(component, event, helper);

        /*
         * Get All the FieldList Values from DB
         */
        helper.getFieldLabels(component);
        helper.setFieldSetObject(component, event, helper);
        helper.getPickListValNone(component, event, helper);

    },

    /*
     * Validation Rules
     */
    validate: function(component, event, helper) {
   try{
        var valid = true;
        var numbers = /^[0-9]+$/;
        var phoneNumber = /^[0-9]$/;
        var phoneNumberValid = /^[0-9-+()*#\s]*$/;
        var validDate = /^\d{4}-\d{2}-\d{2}$/;
        var dob = new Date(component.get("v.persistentAccount").dob);
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {   dd = '0' + dd; } 
        if (mm < 10) {   mm = '0' + mm; } 
	    var today = yyyy + '-' + mm + '-' + dd;
        var account = component.get("v.persistentAccount");
        helper.validatePrimaryAddress(component);
        var countPrimary = component.get("v.countPrimary");
        var missingRequiredFields = false;
        var pageErrors = [];
        var dateofBirth = component.find('dob').get("v.value");
        var birthDate = (new Date(dateofBirth)).getDate();
        var email = component.find('email').get("v.value");
        var weight = component.find('weight').get("v.value");
        var mobilephone = component.find('mobilephone').get("v.value");
        var firstName = component.find('firstName').get("v.value");
        if (firstName) {
            firstName = component.find('firstName').get("v.value").trim()
        }
        var lastName = component.find('lastName').get("v.value");
        if (lastName) {
            lastName = component.find('lastName').get("v.value").trim()
        }
       if(component.find('patientTextConsent')){
             var textConsent = component.find('patientTextConsent').get("v.value");
        }
        
        var textConsentDate = component.find('patientTextConsentDate').get("v.value");
        var validTextDate = (new Date(textConsentDate)).getDate();
       if(component.find('patientHIPAConsent')){
            var hipaaConsent = component.find('patientHIPAConsent').get("v.value");
         }
        
        var hipaaConsentDate = component.find('HippaConsentDate').get("v.value");
        var validhippadate = (new Date(hipaaConsentDate)).getDate();
      
        var childDOB = component.find('childDOB').get("v.value");
        var validChildDob = (new Date(childDOB)).getDate();
        var warining_email = component.find('warining_email');
        var requiredFieldErrorMessage = $A.get("$Label.c.spc_FieldError_Required_Fields");
        var requiredPageErrorMessage = $A.get("$Label.c.spc_PageError_Required_Fields");
        var dobPageErrorMessage = $A.get("$Label.c.spc_PageError_DOB");
        var phonePageErrorMessage = $A.get("$Label.c.spc_PageError_Phone");
        var emailPageErrorMessage = $A.get("$Label.c.spc_PageError_Email");
        var primaryAddressLessPageErrorMessage = $A.get("$Label.c.spc_PageError_Primary_Address_Less");
        var primaryAddressMorePageErrorMessage = $A.get("$Label.c.spc_PageError_Primary_Address_More");
        var onlineAddressErrorMessage = $A.get("$Label.c.spc_PageError_Address");
        var phone = component.find('mobilephone').get("v.value");
        var Officephone = component.find('officePhone').get("v.value");
        var Homephone = component.find('homePhone').get("v.value");
        var lastNameCmp = component.find('lastName');
        var firstNameCmp = component.find('firstName');
    
        var hipaaConsentDateCmp = component.find('HippaConsentDate');
        var textConsentDateCmp = component.find('patientTextConsentDate');
        var weightCmp = component.find('weight');
        var childDOBCmp = component.find('childDOB');
        var phoneCmp = component.find('mobilephone');
        var emailCmp = component.find('email');
        var OfficephoneCmp = component.find('officePhone');
        var HomephoneCmp = component.find('homePhone');
        var remsEnrolCmp = component.find('remsEnrollId');
        var remsCmp = component.find('remsId');
        var dateofBirthCmp = component.find('dob')
        helper.clearErrorMessageAll(component, [remsCmp, remsEnrolCmp, lastNameCmp, firstNameCmp,
            hipaaConsentDateCmp, hipaaConsentDateCmp,
            textConsentDateCmp, weightCmp, childDOBCmp, phoneCmp, emailCmp, OfficephoneCmp,dateofBirthCmp, HomephoneCmp
        ]);
        var validName = /^[a-z A-Z-"'"]*$/;
        if (!component.get("v.remsIdCheck")) {
            var remsID = component.find('remsId').get("v.value");
            if (remsID) {
                remsID = component.find('remsId').get("v.value").trim();
            }
        } else {
            var remsID = component.get("v.persistentAccount").remsId;
        }
        var remsEnrollId = component.find('remsEnrollId').get("v.value");
        if (remsEnrollId) {
            remsEnrollId = component.find('remsEnrollId').get("v.value").trim();
        }
        if (component.get("v.persistentAddress").length !== 0) {

            if (countPrimary === 0) {
                pageErrors.push(primaryAddressLessPageErrorMessage);
                valid = false;
            }

            if (countPrimary > 1) {
                pageErrors.push(primaryAddressMorePageErrorMessage);
                valid = false;
            }
        }
        
        if (firstName && lastName) {

            component.set("v.isValid", true);
            if (dateofBirth || email || mobilephone) {
                component.set("v.isValid", true);
            } else {
                component.set("v.isValid", false);
                $A.util.addClass(emailCmp, "error");
                 $A.util.addClass(phoneCmp, "error");
                $A.util.addClass(dateofBirthCmp, "error");
            	phoneCmp.set("v.errors", [{ message: "Please fill a date of birth,mobile phone or email." }]);
            	emailCmp.set("v.errors", [{ message: "Please fill a date of birth,mobile phone or email" }]);
                dateofBirthCmp.set("v.errors", [{ message: "Please fill a date of birth,mobile phone or email" }]);
                component.set("v.pageErrors", [$A.get("$Label.c.spc_EmptyDobError")]);
            }

        } else {
            component.set("v.isValid", false);
             document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
            if (firstName && !lastName) {
                 $A.util.addClass(lastNameCmp, "error");
                lastNameCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_EmptyLastNameError") }]);
                component.set("v.pageErrors", [$A.get("$Label.c.spc_EmptyLastNameError")]);
            } else if (!firstName && lastName) {
                 $A.util.addClass(firstNameCmp, "error");
                component.set("v.pageErrors", [$A.get("$Label.c.spc_EmptyFirstNameError")]);
                firstNameCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_EmptyFirstNameError") }]);
            } else {
                 $A.util.addClass(firstNameCmp, "error");
                  $A.util.addClass(lastNameCmp, "error");
                lastNameCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_EmptyLastNameError") }]);
                firstNameCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_EmptyFirstNameError") }]);
                component.set("v.pageErrors", [$A.get("$Label.c.spc_EmptyFirstNameAndLastNameError")]);
            }
        }
        if(dateofBirth)
        {
            if(dateofBirth>today)
            {
               $A.util.addClass(dateofBirthCmp, "error");
                component.set("v.isValid", false);
                component.set("v.pageErrors", [$A.get("$Label.c.spc_DOB_Cannot_be_Future_Date")]);
                dateofBirthCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_DOB_Cannot_be_Future_Date") }]);
            }
        }
       
        if ((hipaaConsent === 'Yes' || hipaaConsent === 'No') && $A.util.isEmpty(hipaaConsentDate)) {
            component.set("v.isValid", false);
             document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
             $A.util.addClass(hipaaConsentDateCmp, "error");
            hipaaConsentDateCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_EmptyServicesConsentDateError") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_EmptyServicesConsentDateError")]);
        }
        if (hipaaConsentDate>today) {
            component.set("v.isValid", false);
             document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
             $A.util.addClass(hipaaConsentDateCmp, "error");
            hipaaConsentDateCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_serviceConsentCannotbeFutureDate") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_serviceConsentCannotbeFutureDate")]);
        }
        if ((textConsent === 'Yes' || textConsent === 'No') && $A.util.isEmpty(textConsentDate)) {
            component.set("v.isValid", false);
             document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
             $A.util.addClass(textConsentDateCmp, "error");
            textConsentDateCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_EmptyMarketingTextConsentDateError") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_EmptyMarketingTextConsentDateError")]);
        }
        if (textConsentDate>today) {
            component.set("v.isValid", false);
             document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
             $A.util.addClass(textConsentDateCmp, "error");
            textConsentDateCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_MarketingtextConsentDateCannotbeFutureDate") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_MarketingtextConsentDateCannotbeFutureDate")]);
        }

        var showWarning = component.get('v.showWarning');
        var validEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!$A.util.isEmpty(firstName) && ((firstName.match(validName) === null) || firstName.length > 80)) {
            firstNameCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_InvalidFirstNameError") }]);
            component.set("v.isValid", false);
             document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
             $A.util.addClass(firststNameCmp, "error");
            component.set("v.pageErrors", [$A.get("$Label.c.spc_InvalidFirstNameError")]);
        }
        if (!$A.util.isEmpty(lastName) && ((lastName.match(validName) === null) || lastName.length > 80)) {
            component.set("v.isValid", false);
             document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
             $A.util.addClass(lastNameCmp, "error");
            lastNameCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_InvalidLastNameError") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_InvalidLastNameError")]);
        }

        var validWeight = /^[1-9]\d{0,2}(\.\d{2})*(,\d+)?$/;
        weight = Math.round(weight);
        if (weight) {
            var weightString = weight.toString();
            if (!$A.util.isEmpty(weightString) && (weightString.match(validWeight) === null)) {
                component.set("v.isValid", false);
                document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
                $A.util.addClass(weightCmp, "error");
                weightCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_InvalidWeightError") }]);
                component.set("v.pageErrors", [$A.get("$Label.c.spc_InvalidWeightError")]);
            }

        } else {
            weight = null;
        }
        var persistentAccount = component.get("v.persistentAccount");
        persistentAccount.weight = weight;
        persistentAccount.firstName = firstName;
        persistentAccount.lastName = lastName;
        persistentAccount.remsId = remsID;
        persistentAccount.remsEnrollmentId = remsEnrollId;
        component.set("v.persistentAccount", persistentAccount);
        if (!$A.util.isEmpty(dateofBirth) && (dateofBirth.match(validDate) === null) || (!$A.util.isEmpty(dateofBirth) && isNaN(birthDate))) {
            component.set("v.isValid", false);
             document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
            $A.util.addClass(dateofBirthCmp, "error");
            dateofBirthCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_InvalidDateFormatError") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_InvalidDateFormatError")]);
        }
        if (!$A.util.isEmpty(hipaaConsentDate) && (hipaaConsentDate.match(validDate) === null) || (!$A.util.isEmpty(hipaaConsentDate) && isNaN(validhippadate))) {
            component.set("v.isValid", false);
             document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
            $A.util.addClass(hipaaConsentDateCmp, "error");
            hipaaConsentDateCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_InvalidDateFormatError") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_InvalidDateFormatError")]);
        }
        if (!$A.util.isEmpty(textConsentDate) && (textConsentDate.match(validDate) === null) || (!$A.util.isEmpty(textConsentDate) && isNaN(validTextDate))) {
            component.set("v.isValid", false);
             document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
            $A.util.addClass(textConsentDateCmp, "error");
            textConsentDateCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_InvalidDateFormatError") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_InvalidDateFormatError")]);
        }
      
        if (!$A.util.isEmpty(childDOB) && (childDOB.match(validDate) === null) || (!$A.util.isEmpty(childDOB) && isNaN(validChildDob))) {
            component.set("v.isValid", false);
             document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
            $A.util.addClass(childDOBCmp, "error");
            childDOBCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_InvalidDateFormatError") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_InvalidDateFormatError")]);
        }
        if(childDOB){
        if (childDOB>today) {
            component.set("v.isValid", false);
             document.getElementById('form').scrollIntoView({ block: 'start', behavior: 'smooth' });
            $A.util.addClass(childDOBCmp, "error");
            childDOBCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_Child_DOB_cannot_be_a_future_Date") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_Child_DOB_cannot_be_a_future_Date")]);
        }
        }
        if (!$A.util.isEmpty(email) && (email.match(validEmail) === null)) {
            component.set("v.isValid", false);
             document.getElementById('communication').scrollIntoView({ block: 'start', behavior: 'smooth' });
            $A.util.addClass(emailCmp, "error");
            emailCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_InvalidEmailError") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_InvalidEmailError")]);
        }
        if (!$A.util.isEmpty(phone) && ((phone.match(phoneNumber) === null) && (phone.match(phoneNumberValid) === null))) {
            component.set("v.isValid", false);
            document.getElementById('communication').scrollIntoView({ block: 'start', behavior: 'smooth' });
            $A.util.addClass(phoneCmp, "error");
            phoneCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_InvalidMobilePhoneError") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_InvalidMobilePhoneError")]);
        }
        if (!$A.util.isEmpty(Officephone) && ((Officephone.match(phoneNumber) === null) && (Officephone.match(phoneNumberValid) === null))) {
            component.set("v.isValid", false);
            document.getElementById('communication').scrollIntoView({ block: 'start', behavior: 'smooth' });
            $A.util.addClass(OfficephoneCmp, "error");
            OfficephoneCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_InvalidOfficePhoneError") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_InvalidOfficePhoneError")]);
        }

        if (!$A.util.isEmpty(Homephone) && ((Homephone.match(phoneNumber) === null) && (Homephone.match(phoneNumberValid) === null))) {
            component.set("v.isValid", false);
            document.getElementById('communication').scrollIntoView({ block: 'start', behavior: 'smooth' });
            $A.util.addClass(HomephoneCmp, "error");
            HomephoneCmp.set("v.errors", [{ message: $A.get("$Label.c.spc_InvalidHomePhoneError") }]);
            component.set("v.pageErrors", [$A.get("$Label.c.spc_InvalidHomePhoneError")]);

        }
     
		
		if(!valid || pageErrors.length>0) {
        	component.set("v.pageErrors", pageErrors);
        	component.set("v.isValid", valid);
        }
       
       }catch(e){
    console.log("error"+e);
} 
    },
    removeWarning: function(component, event, helper) {
        var warining_email = component.find('warining_email');
        $A.util.toggleClass(warining_email, 'slds-hide');
        component.set("v.showWarning", true);
    },

    showModalBox: function(component, event, helper) {
        helper.showModalBox(component, event, helper);
    },

    dontModalBox: function(component, event, helper) {
        var elementsbackGroundSection = component.find('aura_backGroundSectionId').getElement();
        var elementsnewAccountSection = component.find('aura_newAccountSectionId').getElement();
        elementsbackGroundSection.style.display = "none";
        elementsnewAccountSection.style.display = "none";
        var source = "";
        var flag = "";
        helper.initailizeAddress(component, source, flag);
        event.preventDefault();
        return false;
    },

    addAddress: function(component, event, helper) {
        component.set("v.errorCheck", true);
        helper.clearErrorMessage(component, ['address1', 'city', 'zipCode', 'conCountry', 'conState']);
        helper.validateAddressFields(component, ['address1', 'city', 'zipCode', 'conCountry', 'conState']);
        var getAddress = component.get("v.newAddress");
        var setAddress = component.get("v.persistentAddress");
        

        if (component.get("v.errorCheck")) {
            if (!$A.util.isEmpty(getAddress.applicantSourceId)) {
                getAddress.isAddressSelected = true;

                var onlineFormAddress = component.get("v.onlineFormAddress");
                for (var i = 0; i < onlineFormAddress.length; i++) {
                    if (onlineFormAddress[i].applicantSourceId === getAddress.applicantSourceId) {
                        onlineFormAddress[i].isAddressSelected = true;
                        break;
                    }
                }
                component.set("v.onlineFormAddress", onlineFormAddress);
            }

            helper.setAddressLabels(component, getAddress);

            setAddress.push(getAddress);
            component.set("v.persistentAddress", setAddress);
            helper.initailizeAddress(component, "", "");

            var elementsbackGroundSection = component.find('aura_backGroundSectionId').getElement();
            var elementsnewAccountSection = component.find('aura_newAccountSectionId').getElement();
            elementsbackGroundSection.style.display = "none";
            elementsnewAccountSection.style.display = "none";
            event.preventDefault();
        } else {
            var elementsbackGroundSection = component.find('aura_backGroundSectionId').getElement();
            var elementsnewAccountSection = component.find('aura_newAccountSectionId').getElement();
            elementsbackGroundSection.style.display = "block";
            elementsnewAccountSection.style.display = "block";
            event.preventDefault();
        }
    },

    updateAddress: function(component, event, helper) {
        var index = component.get("v.newAddressIndex");
        var getAddress = component.get("v.newAddress");
        var setAddress = component.get("v.persistentAddress");
        component.set("v.errorCheck", true);
        helper.clearErrorMessage(component, ['address1', 'city', 'zipCode', 'conCountry', 'conState']);
        helper.validateAddressFields(component, ['address1', 'city', 'zipCode', 'conCountry', 'conState']);
        if (component.get("v.errorCheck")) {
            helper.setAddressLabels(component, getAddress);
            setAddress[index] = getAddress;
            component.set("v.persistentAddress", setAddress);

            var elementsbackGroundSection = component.find('aura_backGroundSectionId').getElement();
            var elementsnewAccountSection = component.find('aura_newAccountSectionId').getElement();
            elementsbackGroundSection.style.display = "none";
            elementsnewAccountSection.style.display = "none";
            var source = "";
            var flag = "";
            helper.initailizeAddress(component, source, flag);
            event.preventDefault();
        } else {
            var elementsbackGroundSection = component.find('aura_backGroundSectionId').getElement();
            var elementsnewAccountSection = component.find('aura_newAccountSectionId').getElement();
            elementsbackGroundSection.style.display = "block";
            elementsnewAccountSection.style.display = "block";
            event.preventDefault();
        }
    },

    editAddress: function(component, event, helper) {
        var selectedTargetNode = event.currentTarget;
        var index = selectedTargetNode.dataset.id.substring(3);
        component.set("v.newAddressIndex", index);
        var persistentAddress = component.get("v.persistentAddress");

        var newAddress = {
            Id: persistentAddress[index].Id,
            account: persistentAddress[index].account,
            address: persistentAddress[index].address,
            addressType: persistentAddress[index].addressType,
            address1: persistentAddress[index].address1,
            state: persistentAddress[index].state,
            address2: persistentAddress[index].address2,
            zipCode: persistentAddress[index].zipCode,
            address3: persistentAddress[index].address3,
            country: persistentAddress[index].country,
            city: persistentAddress[index].city,
            primary: persistentAddress[index].primary,
            lastModifiedDate: persistentAddress[index].lastModifiedDate,
            source: persistentAddress[index].source,
            flag: "Edited",
            dataChk: persistentAddress[index].dataChk,
            isAddressSelected: false,
            applicantSourceId: persistentAddress[index].applicantSourceId
        };
        component.set("v.newAddress", newAddress);
        var elementsbackGroundSection = component.find('aura_backGroundSectionId').getElement();
        var elementsnewAccountSection = component.find('aura_newAccountSectionId').getElement();
        elementsbackGroundSection.style.display = "block";
        elementsnewAccountSection.style.display = "block";
        component.set("v.addrModalOpen", true);
        component.set("v.button", false);
        helper.onControllerFieldChangeHelper(component, helper, newAddress.country);
        component.find('conCountry').set("v.value", newAddress.country);
        component.find('conState').set("v.value", persistentAddress[index].state);
    },

    removeAddress: function(component, event, helper) {
        var selectedTargetNode = event.currentTarget;
        var index = selectedTargetNode.dataset.id.substring(3);
        var persistentAddress = component.get("v.persistentAddress");
        var onlineFormAddress = component.get("v.onlineFormAddress");
        if (!$A.util.isEmpty(persistentAddress[index].applicantSourceId)) {
            for (var i = 0; i < onlineFormAddress.length; i++) {
                if (onlineFormAddress[i].applicantSourceId === persistentAddress[index].applicantSourceId) {
                    onlineFormAddress[i].isAddressSelected = false;
                    break;
                }
            }
        }
        persistentAddress.splice(index, 1);
        component.set("v.persistentAddress", persistentAddress);
        component.set("v.onlineFormAddress", onlineFormAddress);

        if (onlineFormAddress.length && persistentAddress.length) {
            helper.updateActionIndicators(component);
        }
    },

    // function call on change tha controller field  
    onControllerFieldChange: function(component, event, helper) {
        // get the selected value
        var controllerValueKey = event.getSource().get("v.value");
        helper.onControllerFieldChangeHelper(component, helper, controllerValueKey);

    },

    // Function call on change the Dependent field    
    onDependentFieldChange: function(component, event, helper) {
        //alert(event.getSource().get("v.value"));
    },

    addNewAddressViaApplicant: function(component, event, helper) {
        var onlineApplicantAddress = component.get("v.onlineFormAddress");
        var selectedTargetNode = event.currentTarget;
        var index = selectedTargetNode.dataset.text;
        // you may have to add clear message method here
        helper.clearErrorMessage(component, ['address1', 'city']);
        var newAddress = component.get("v.newAddress");
        var newAddress = {
            address: onlineApplicantAddress[index].address,
            address1: onlineApplicantAddress[index].address1,
            address2: onlineApplicantAddress[index].address2,
            state: onlineApplicantAddress[index].state,
            address3: onlineApplicantAddress[index].address3,
            city: onlineApplicantAddress[index].city,
            country: onlineApplicantAddress[index].country,
            zipCode: onlineApplicantAddress[index].zipCode,
            source: onlineApplicantAddress[index].source,
            addressType: onlineApplicantAddress[index].addressType,
            dataChk: onlineApplicantAddress[index].dataChk,
            flag: onlineApplicantAddress[index].flag,
            lastModifiedDate: onlineApplicantAddress[index].lastModifiedDate,
            primary: onlineApplicantAddress[index].primary,
            applicantSourceId: onlineApplicantAddress[index].applicantSourceId,
            isAddressSelected: true
        };

        component.set("v.isValidated", false);
        component.set("v.newAddress", newAddress);
        component.set("v.button", true); //Sets the modal button to "Add"

        helper.onControllerFieldChangeHelper(component, helper, newAddress.country);
        component.find('conCountry').set("v.value", newAddress.country);
        component.find('conState').set("v.value", newAddress.state);

        var elementsbackGroundSection = component.find('aura_backGroundSectionId').getElement();
        var elementsnewAccountSection = component.find('aura_newAccountSectionId').getElement();
        elementsbackGroundSection.style.display = "block";
        elementsnewAccountSection.style.display = "block";
    },
     showtooltip:function(component,event,helper){
        var value=event.currentTarget.id;
        
        var tooltip=component.find(value+'popover');
        $A.util.removeClass(tooltip,'slds-hide');
    },
    removetooltip:function(component,event,helper){
        var value=event.currentTarget.id;
        
        var tooltip=component.find(value+'popover');
        $A.util.addClass(tooltip,'slds-hide');
    }
})
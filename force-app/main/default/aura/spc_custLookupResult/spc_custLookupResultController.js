({

    /*********************************************************************************
Class Name      : spc_custLookupResult
Description     : component created for customized look up results
Created By      : Sanjana Tripathy
Created Date    : 21-June-18
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                    Description
-------------------------------9---------------------------------------------------            
Sanjana Tripathy              21-June-18              Initial Version
*********************************************************************************/
    selectRecord: function(component, event, helper) {
        // get the selected record from list  
        var getSelectRecord = component.get("v.oRecord");
        // call the event  
        var compEvent = component.getEvent("oSelectedRecordEvent");
        // set the Selected sObject Record to the event attribute.  
        compEvent.setParams({
            "recordByEvent": getSelectRecord
        });
        // fire the event  
        compEvent.fire();
    },
})
({
	doInit: function(component, event, helper) {
		if (component.get("v.isSuccess")) {
			helper.showSuccessMsg(component, event);
		} else {
			helper.showErrorMsg(component, event);
		}

	},
})
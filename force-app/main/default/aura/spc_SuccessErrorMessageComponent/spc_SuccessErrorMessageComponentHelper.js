({
    showErrorMsg: function(component, event) {

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": "error",
            "mode": "sticky",
            "message": component.get("v.componentMessage")
        });
        toastEvent.fire();
    },
    showSuccessMsg: function(component, event) {

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": "success",
            "mode": "sticky",
            "message": component.get("v.componentMessage")
        });
        toastEvent.fire();
    },
})
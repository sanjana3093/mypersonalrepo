({
  initLetters: function(component) {
    var action = component.get("c.getLetters");
    action.setParams({
      "recordId": component.get("v.recordId"),
      "invokeAction": "inquiry",
      "isFaxOnly": false
    });
    action.setCallback(this, function(a) {
      var state = a.getState();
      var dismissActionPanel = $A.get("e.force:closeQuickAction");
      if (state === 'SUCCESS') {

        var lstAvailableeLetters = a.getReturnValue();
        var opts = [];
        if (lstAvailableeLetters.length > 0 && lstAvailableeLetters[0].name === 'hasMarketingConsent') {
          component.set("v.checkMarketingConsent", true);
          dismissActionPanel.fire();
        } else if (lstAvailableeLetters.length > 0 && lstAvailableeLetters[0].name === 'caregiverMarketingConsent') {
          component.set("v.caregiverMarketingConsent", true);
          dismissActionPanel.fire();
        } else {
          component.set("v.checkMarketingConsent", false);
          var inputsel = component.find("inputSelectLetter");
          for (var i = 0; i < lstAvailableeLetters.length; i++) {
            var currentVal = lstAvailableeLetters[i];
            opts.push({
              "class": "optionClass",
              label: currentVal.name,
              value: currentVal.value
            });
          }
          inputsel.set("v.options", opts);
        }
      } else if (state === "ERROR") {
        var errors = a.getError();
        if (errors && errors[0] && errors[0].message) {
          component.set("v.message", errors[0].message);
        } else {
          component.set("v.message", 'Unknown error');
        }
        component.set("v.errorForMessage", true);
        dismissActionPanel.fire();
      }

    });
    $A.enqueueAction(action);
  },
  loadRecepients: function(component) {

    var action = component.get("c.getRecepients");
    action.setParams({
      "recordId": component.get("v.recordId"),
      "letterId": component.find("inputSelectLetter").get("v.value"),
      "isFaxSpecific": false
    });
    action.setCallback(this, function(a) {
      var state = a.getState();
      var dismissActionPanel = $A.get("e.force:closeQuickAction");
      if (state === 'SUCCESS') {
        component.set("v.recepients", a.getReturnValue());

        if (a.getReturnValue() === null) {
          component.set("v.message", 'No recepients available');
          component.set("v.errorForMessage", true);
          dismissActionPanel.fire();
        }
      } else if (state === "ERROR") {
        var errors = a.getError();
        if (errors && errors[0] && errors[0].message) {
          component.set("v.message", errors[0].message);
        } else {
          component.set("v.message", 'Unknown error');
        }
        component.set("v.errorForMessage", true);
        dismissActionPanel.fire();
      }

    });
    $A.enqueueAction(action);
  },
  sendDocuments: function(component) {
    var action = component.get("c.sendOutboundDocuments");

    action.setParams({
      "recipientsString": JSON.stringify(component.get("v.recepients")),
      "letterId": component.find("inputSelectLetter").get("v.value"),
      "objectId": component.get("v.recordId")
    });
    action.setCallback(this, function(a) {
      var state = a.getState();
      var dismissActionPanel = $A.get("e.force:closeQuickAction");
      if (state === 'SUCCESS') {
        component.set("v.message", a.getReturnValue());
        dismissActionPanel.fire();
        var prefixmsg = $A.get("$Label.c.spc_InvalidEmailNumberPrefix");
        var msg = a.getReturnValue();
        var noreceipt = $A.get("$Label.PatientConnect.PC_No_Recipients_Available");
        var norecord = $A.get("$Label.PatientConnect.PC_No_Record_Selected");
        var nomarketingconsent = $A.get("$Label.c.spc_caregiverMarketingConsentMissing");
        if (msg.includes(prefixmsg) || msg.includes(norecord) || msg.includes(nomarketingconsent) || msg.includes(noreceipt)) {
          component.set("v.errorForMessage", true);
        } else {
          component.set("v.successForMessage", true);
        }
      } else if (state === "ERROR") {
        var errors = a.getError();
        if (errors && errors[0] && errors[0].message) {
          component.set("v.message", errors[0].message);
        } else if (errors[0].pageErrors) {
          component.set("v.message", errors[0].pageErrors[0].message);
        } else {
          component.set("v.message", 'Unknown error');
        }
        component.set("v.errorForMessage", true);
        dismissActionPanel.fire();
      }

    });
    $A.enqueueAction(action);
  },
  displayError: function(a) {
    var errors = a.getError();
    if (errors && errors[0] && errors[0].message) {
      component.set("v.message", errors[0].message);
    } else {
      component.set("v.message", 'Unknown error');
    }
  }
})
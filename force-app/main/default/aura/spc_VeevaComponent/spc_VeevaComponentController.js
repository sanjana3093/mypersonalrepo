/*********************************************************************************
Class Name      : spc_VeevaComponent
Description     : component created to add prescription site of care screen in EW
Created By      : Deloitte
Created Date    : 06-Sept-18
*********************************************************************************/
({
    doInit: function(component, event, helper) {
        setTimeout(function() {
            var path = component.get("v.pathofveeva");

        }, 1000)

    },
    OpenVeevaModal: function(component, event, helper) {
        var modalVeeva = component.find('modalVeevadialog');
        var path = component.get("v.pathofveeva");
        $A.util.toggleClass(modalVeeva, 'slds-fade-in-hide');
        $A.util.toggleClass(modalVeeva, 'slds-fade-in-open');
        var backdropVeeva = component.find('backdropVeeva');
        $A.util.toggleClass(backdropVeeva, 'slds-backdrop--hide');
        $A.util.toggleClass(backdropVeeva, 'slds-backdrop--open');
        var isVeevaWidgetOpened = component.get('v.isVeevaWidgetOpened');
        if (isVeevaWidgetOpened) {
            helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-', helper);
            helper.showPopupHelper(component, 'backdrop', 'slds-backdrop--', helper);
            component.set('v.isVeevaWidgetOpened', false);
        } else {
            helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
            helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
            component.set('v.isVeevaWidgetOpened', true);
        }

    }
})
({
    searchHelper: function(component, event, getInputkeyWord) {
        if (getInputkeyWord) {
            var filterId = '';
            var label = component.get("v.label");

            var action = component.get("c.fetchLookUpValues");
            // set param to method  
            action.setParams({
                'searchKeyWord': getInputkeyWord,
                'ObjectName': component.get("v.objectAPIName"),
                'filterObjectId': filterId,
                'recordTypeName': component.get("v.recordTypeName")
            });
            // set a callBack    
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
                    if (storeResponse !== null) {
                        var forOpen = component.find("searchRes");
                        $A.util.addClass(forOpen, 'slds-is-open');
                        $A.util.removeClass(forOpen, 'slds-is-close');

                        // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
                        if (storeResponse.length === 0) {
                            component.set("v.Message", 'No Result Found...');
                        } else {
                            component.set("v.Message", '');


                        }
                    }
                    // set searchResult list with return value from server.
                    component.set("v.listOfSearchRecords", storeResponse);
                } else if (state === 'ERROR') {
                    console.log(JSON.stringify(storeResponse.getError()));
                }

            });
            $A.enqueueAction(action);
        }
    },
    clear: function(component, event) {

        var pillTarget = component.find("lookup-pill");
        var lookUpTarget = component.find("lookupField");

        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');

        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');

        component.set("v.SearchKeyWord", null);
        component.set("v.listOfSearchRecords", null);
        component.set("v.selectedRecord", {});
        component.set("v.selectedLookupValue", '');

    },
})
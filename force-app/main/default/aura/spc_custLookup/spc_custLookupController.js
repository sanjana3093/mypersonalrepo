/*********************************************************************************
Class Name      : spc_custLookup
Description     : component created for customized look up
Created By      : Sanjana Tripathy
Created Date    : 21-June-18
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                    Description
-------------------------------9---------------------------------------------------            
Sanjana Tripathy              21-June-18              Initial Version
*********************************************************************************/
({
    onfocus: function(component, event, helper) {
        // Get Default 5 Records order by createdDate DESC  
        var getInputkeyWord = '';
        helper.searchHelper(component, event, getInputkeyWord);
    },
    keyPressController: function(component, event, helper) {
        // get the search Input keyword   
        var getInputkeyWord = component.get("v.SearchKeyWord");

        // clear pop up on click of escape key
        if (event.keyCode === 27) {
            helper.clear(component, event);
        }
        // check if getInputKeyWord size id more then 0 then open the lookup result List and 
        // call the helper 
        // else close the lookup result List part.   
        if (getInputkeyWord.length > 0) {
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component, event, getInputkeyWord);
        } else {
            component.set("v.listOfSearchRecords", null);
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
    },

    // function for clear the Record Selection 
    clear: function(component, event, heplper) {

        var pillTarget = component.find("lookup-pill");
        var lookUpTarget = component.find("lookupField");

        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');

        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');

        component.set("v.SearchKeyWord", null);
        component.set("v.listOfSearchRecords", null);
        component.set("v.selectedRecord", {});
        component.set("v.selectedLookupValue", '');
        component.set("v.selectedLookupValueID", '');
        var onchange = component.get("v.onChange");
        if (onchange) {
            $A.enqueueAction(onchange);
        }
    },

    // This function call when the end User Select any record from the result list.   
    handleComponentEvent: function(component, event, helper) {
        // get the selected Account record from the COMPONENT event    
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        var objectName = component.get("v.objectAPIName");
        component.set("v.selectedRecord", selectedAccountGetFromEvent);
        component.set('v.selectedLookupValue', selectedAccountGetFromEvent.Name);
        component.set("v.selectedLookupValueID", selectedAccountGetFromEvent.Id);
        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
        var lookUpTarget = component.find("lookupField");
        $A.util.addClass(lookUpTarget, 'slds-hide');
        $A.util.removeClass(lookUpTarget, 'slds-show');

        var onchange = component.get("v.onChange");
        if (onchange) {
            $A.enqueueAction(onchange);
        }

    },
    // automatically call when the component is done waiting for a response to a server request.  
    hideSpinner: function(component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({
            isVisible: false
        });
        evt.fire();
    },
    // automatically call when the component is waiting for a response to a server request.
    showSpinner: function(component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({
            isVisible: true
        });
        evt.fire();
    },
    validateForm: function(component, event, helper) {
        var required = component.get("v.required");

        if (required) {
            var input = component.find('inputSearch');
            if (input) {
                if (!input.get("v.value")) {
                    $A.util.addClass(input, 'slds-has-error');
                    component.set("v.hasError", true);
                } else {
                    $A.util.removeClass(input, 'slds-has-error');
                    component.set("v.hasError", false);
                }
            }
        }
    }
})
({
    saveDocument: function(component, event, helper) {

        var rid = component.get("v.recordId");

        helper.dateChanged(component, event, helper);
        helper.createDocument(component, rid);
    },
    dateChanged: function(component, event, helper) {
        helper.dateChanged(component, event, helper);

    },
    doInit: function(component, event, helper) {
        var rid = component.get("v.recordId");
        helper.getDocumentTypes(component, event, helper);
        helper.getSobjectName(component, event, helper);

    }
})
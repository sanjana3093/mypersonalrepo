({
    CHUNK_SIZE: 750000,
    /* Use a multiple of 4 */
    MAX_FILE_SIZE: 4500000,
    showSpinner: function(component, isEnable) {
        component.set('v.wait', isEnable);
    },
    getDocumentTypes: function(component, event, helper) {
        var action = component.get("c.getDocumentTypes");
        helper.showSpinner(component, true);
        action.setCallback(this, function(response) {
            component.set('v.availableCategories', response.getReturnValue());
            helper.showSpinner(component, false);
        });
        $A.enqueueAction(action);
    },


    getEngagementprog: function(component, event, helper) {

        var action = component.get("c.getEngagementPrograms");
        helper.showSpinner(component, true);
        action.setCallback(this, function(response) {
            component.set('v.EngagementPrograms', response.getReturnValue());
            component.set('v.showengprograms', true);
            helper.showSpinner(component, false);
        });
        $A.enqueueAction(action);
    },
    getEPFromProgramCase: function(component, event, helper) {
        var programID = component.get("v.recordId");
        var action = component.get("c.getEPFromProgram");
        action.setParams({
            programId: programID
        });
        action.setCallback(this, function(response) {
            component.set('v.EngagementProgramFromProgamCase', response.getReturnValue());
            component.set('v.showengprograms', false);
            helper.showSpinner(component, false);
        });
        $A.enqueueAction(action);
    },
    getSobjectName: function(component, event, helper) {
        var rid = component.get("v.recordId");
        var action = component.get("c.getSOBjectName");
        action.setParams({
            parentId: rid
        });
        action.setCallback(this, function(response) {
            component.set('v.sObjectName', response.getReturnValue());
            helper.showSpinner(component, false);
            var sObjectName = component.get("v.sObjectName");
            if (sObjectName === 'Case') {
                helper.getEPFromProgramCase(component, event, helper);
            } else {
                helper.getEngagementprog(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    },
    createDocument: function(component, parentId) {
        var isValidDate = component.get('v.isValidDate');
        var fileInput = component.find("file").getElement();
        var isValid = false;
        var file = fileInput.files[0];
        if (file === null || file === undefined) {
            component.set("v.errorMsg", ' Please choose a file to upload.');
            isValid = true;
        }



        if (!isValid && file.size > this.MAX_FILE_SIZE) {
            alert('File size cannot exceed ' + this.MAX_FILE_SIZE + ' bytes.\n' +
                'Selected file size: ' + file.size);
            return;
        }
        if (!isValid) {
            component.set("v.errorMsg", '');
            if (isValidDate) {
                var fr = new FileReader();
                var self = this;
                fr.onload = $A.getCallback(function() {
                    var fileContents = fr.result;
                    var base64Mark = 'base64,';
                    var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                    fileContents = fileContents.substring(dataStart);
                    self.upload(component, file, fileContents, parentId);
                });
                fr.readAsDataURL(file);
            }
        }
    },
    upload: function(component, file, fileContents, parentId) {
        var fromPos = 0;
        var toPos = Math.min(fileContents.length, fromPos + this.CHUNK_SIZE);

        // start with the initial chunk
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth();
        var yyyy = today.getFullYear();
        var hh = today.getHours();
        var min = today.getMinutes();
        var sec = today.getSeconds();
        var milsec = today.getMilliseconds();
        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        var todayDate = new Date(yyyy, mm, dd, hh, min, sec, milsec);
        var datesent = component.get("v.dateSent");
        var datesent2 = new Date(datesent);
        var sentDate = component.find("Date_Of_Service").get("v.value");

        var date = component.find("Date_Of_Service");
        if (sentDate === null) {
            $A.util.addClass(date, 'slds-has-errors');
        } else {
            $A.util.removeClass(date, 'slds-has-errors');
        }

        if (sentDate === null) {
            component.set("v.errorMsg", "These required fields must be completed: Date/Time Sent");
        } else if (datesent2 > todayDate) {
            component.set("v.errorMsg", "Date Sent Cannot be Future Date");
        } else {
            var isValid = false;
            if (component.get("v.showengprograms")) {
                var Engprogram = component.find("Engprog").get("v.value");


                if (Engprogram === "") {
                    component.set("v.errorMsg", "Please populate Engagement Program.");
                    isValid = true;
                }
            }
            if (!isValid) {
                this.uploadChunk(component, file, fileContents, parentId, fromPos, toPos, '');
            }

        }
    },

    appendFile: function(component, file, fileContents, parentId, fromPos, toPos, attachId) {
        var chunk = fileContents.substring(fromPos, toPos);
        var action = component.get("c.appendToFile");
        action.setParams({
            base64Data: encodeURIComponent(chunk),
            fileId: attachId
        });
        action.setCallback(this, function(response) {
            var res = response.getReturnValue();
            this.appendFile(component, file, fileContents, parentId, fromPos, toPos, res);
        });
        $A.enqueueAction(action);
    },
    dateChanged: function(component, event, helper) {
        var sentDate = component.find("Date_Of_Service").get("v.value");
        var date = component.find("Date_Of_Service");
		if(sentDate !== null && sentDate !== "" && sentDate !== undefined)
        {
            var res = sentDate.match(/\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/, { excludeEmptyString: true });
        }
		
        if (sentDate === null || sentDate === ""  || res === null) {
           if(sentDate === null || sentDate == "")
            {
                $A.util.addClass(date,"error");
                date.set("v.errors", [{message: $A.get("$Label.c.spc_Upload_Doc_DateTime")}]);
                component.set('v.isValidDate',false);
            }
            if(res === null)
            {
                $A.util.addClass(date,"error");
                date.set("v.errors", [{message: $A.get("$Label.c.spc_Upload_Doc_Correct_DateTime")}]);
                component.set('v.isValidDate',false);
            }

        } else {
            $A.util.removeClass(date, "error");
            date.set("v.errors", [{
                message: ""
            }]);
            component.set('v.isValidDate', true);

        }

    },
    uploadChunk: function(component, file, fileContents, parentId, fromPos, toPos, attachId) {



        var chunk = fileContents.substring(fromPos, toPos);
        var Engprogram;
        var datesent = component.get("v.dateSent");

        if (component.get("v.showengprograms")) {
            Engprogram = component.find("Engprog").get("v.value");
        } else {
            Engprogram = component.get("v.EngagementProgramFromProgamCase");
        }

        var action = component.get("c.createDocument");

        action.setParams({
            parentId: parentId,
            fileName: file.name,
            base64Data: encodeURIComponent(chunk),
            //base64Data: encodeURIComponent(fileContents), 
            contentType: file.type,
            category: component.find("docCategory").get("v.value"),
            notes: component.find("notes").get("v.value"),
            Engprog: Engprogram,
            sentBy: component.find("sentBy").get("v.value"),
            dateSent: component.get("v.dateSent"),
            fileId: attachId,

        });

        var self = this;
        self.showSpinner(component, true);
        action.setCallback(this, function(a) {
            var resp = a.getReturnValue();

            if (resp !== null) {
                attachId = resp.fileId;
            }
            fromPos = toPos;
            toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);
            if (fromPos < toPos) {
                this.uploadChunk(component, file, fileContents, parentId, fromPos, toPos, attachId);
            }



            var state = a.getState();
            if (state === "SUCCESS") {

                var ressponse = a.getReturnValue();
                if (ressponse === null || ressponse === 'undefined') {

                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error",
                        "message": $A.get("$Label.c.spc_Doc_Error")
                    });
                    toastEvent.fire();
                } else {
                    var url = '/' + ressponse.documentId;
                    component.set('v.isSuccess', ressponse.isSuccess);
                    component.set('v.documentId', url);
                    component.set('v.documentName', ressponse.documentName);
                    component.set('v.errorMsg', ressponse.errorMsg);


                    self.showSpinner(component, false);
                }
            } else if (state === "ERROR") {
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error",
                            "message": errors[0].message
                        });
                        toastEvent.fire();
                    }
                } else {}
            } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": $A.get("$Label.c.spc_Doc_Error")
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);

    }


})
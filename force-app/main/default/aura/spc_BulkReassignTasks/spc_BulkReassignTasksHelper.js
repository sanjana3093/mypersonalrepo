({
    hidePopupHelper: function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className + 'hide');
        $A.util.removeClass(modal, className + 'open');
    },
    showPopupHelper: function(component, componentId, className, helper) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className + 'hide');
        $A.util.addClass(modal, className + 'open');
    },
    getCaseManager1: function(component, event, helper) {
        var caseManager1 = component.get("v.caseManagerSelected");
        var action = component.get("c.getCaseManagerList2");
        action.setParams({
            "selectedCaseManger": caseManager1
        });
        action.setCallback(this, function(response) {
            component.set('v.UserIdList2', response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    getAllTasksOfPreviousAssignee: function(component, event, helper, sortFieldName) {
        var caseManager1 = component.get("v.caseManagerSelected");
        var ListOftasks = [];
        var action = component.get("c.getAllTasksOfPreviousAssignee");
        helper.showSpinner(component, true);
        action.setParams({
            "selectedCaseManger": caseManager1,
            "orderByField": sortFieldName,
            "isAsc": component.get("v.isAsc")
        });
        action.setCallback(this, function(response) {
            if (!$A.util.isEmpty(response.getReturnValue())) {
                component.set('v.listTasks', response.getReturnValue());
                component.set('v.lstTasksNotNull', true);
            } else {
                component.set('v.lstTasksNotNull', false);
                component.set('v.listTasks', "");
                helper.showInfoToast(component, event, helper, 'error', 'No tasks present');
            }

            helper.showSpinner(component, false);

        });
        $A.enqueueAction(action);
    },
    getCaseManager2: function(component, event, helper) {
        var caseManager2 = component.get("v.caseManager2Selected");
        var action = component.get("c.getCaseManagerOwnereId");
        action.setParams({
            "selectedCaseManger": caseManager2
        });
        action.setCallback(this, function(response) {
            helper.getAllTasksOfPreviousAssignee(component, event, helper, "ActivityDate");

        });
        $A.enqueueAction(action);
    },
    reassignTasks: function(component, event, helper, selectedTasks) {
        var caseManager2 = component.get("v.caseManager2Selected");
        var action = component.get("c.reassignTasks");
        helper.showSpinner(component, true);
        action.setParams({
            "selectedTasks": selectedTasks,
            "assignee": caseManager2
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            helper.showSpinner(component, false);
            helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
            helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
            if (state === "SUCCESS") {
                helper.showInfoToast(component, event, helper, 'success', 'Tasks have been assigned successfully');
            } else if (state === 'ERROR') {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    showInfoToast: function(component, event, helper, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: '',
            message: message,
            messageTemplate: 'Record {0} created! See it {1}!',
            duration: '50000',
            key: 'info_alt',
            type: type,
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    showSpinner: function(component, isEnable) {
        component.set('v.wait', isEnable);
    },
    sortHelper: function(component, event, helper, sortFieldName) {
        var currentDir = component.get("v.arrowDirection");

        if (currentDir === 'arrowdown') {
            // set the arrowDirection attribute for conditionally rendred arrow sign  
            component.set("v.arrowDirection", 'arrowup');
            // set the isAsc flag to true for sort in Assending order.  
            component.set("v.isAsc", true);
        } else {
            component.set("v.arrowDirection", 'arrowdown');
            component.set("v.isAsc", false);
        }
        this.getAllTasksOfPreviousAssignee(component, event, helper, sortFieldName);
    }
})
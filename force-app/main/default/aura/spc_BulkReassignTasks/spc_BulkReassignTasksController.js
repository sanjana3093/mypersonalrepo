({
  doInit: function(component, event, helper) {
    var action = component.get("c.fetchLoggedInUser");
    action.setCallback(this, function(response) {
      component.set('v.isNotExternalUser', response.getReturnValue());

    });
    $A.enqueueAction(action);
  },
  BulkReassignTasks: function(component, event, helper) {
    var recorID = component.get("v.recordId");
    var action = component.get("c.fetchCaseManagerList1");
    action.setCallback(this, function(response) {
      component.set('v.UserId', response.getReturnValue());
      helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-', helper);
      helper.showPopupHelper(component, 'backdrop', 'slds-backdrop--', helper);

    });
    $A.enqueueAction(action);
  },
  closeModal: function(component, event, helper) {
    // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
    helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
    helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
  },

  showTasks: function(component, event, helper) {
    component.set("v.isOpen", false);
  },
  onControllerFieldChange: function(component, event, helper) {
    helper.getCaseManager1(component, event, helper);
  },
  onControllerFieldChange2: function(component, event, helper) {
    helper.getCaseManager2(component, event, helper);

  },
  handleSelectAllTask: function(component, event, helper) {
    var getID = component.get("v.listTasks");
    var checkvalue = component.find("selectAll").get("v.value");
    var checkTask = component.find("checkTask");
    if (checkvalue === true) {
      for (var i = 0; i < checkTask.length; i++) {
        checkTask[i].set("v.value", true);
      }
    } else {
      for (var i = 0; i < checkTask.length; i++) {
        checkTask[i].set("v.value", false);
      }
    }
  },
  handleSelectedTasks: function(component, event, helper) {
    var selectedTasks = [];
    var checkvalue = component.find("checkTask");

    if (!Array.isArray(checkvalue)) {
      if (checkvalue.get("v.value") === true) {
        selectedTasks.push(checkvalue.get("v.text"));
      }
    } else {
      for (var i = 0; i < checkvalue.length; i++) {
        if (checkvalue[i].get("v.value") === true) {
          selectedTasks.push(checkvalue[i].get("v.text"));
        }
      }
    }
    if (!$A.util.isEmpty(selectedTasks)) {
      component.set('v.listFinalTasksToAssign', selectedTasks);
      helper.reassignTasks(component, event, helper, selectedTasks);
    } else {
      helper.showInfoToast(component, event, helper, 'error', 'Please select the tasks');
    }

  },
  navigateToAccountRecord: function(component, event, helper) {

    var rid = event.target.id;

    var navEvt = $A.get("e.force:navigateToSObject");
    navEvt.setParams({
      "recordId": rid,
      "slideDevName": "related"
    });
    navEvt.fire();

  },
   AdverseEvent : function(component, event, helper){
         var action=component.get("c.getURL");
        action.setCallback(this, function(response) {
              var state=response.getState();
            if(state==='SUCCESS'){
                window.open(response.getReturnValue());
            }
     
        });  
        $A.enqueueAction(action);
       
    },
  sortSubject: function(component, event, helper) {
    component.set("v.selectedTabsoft", 'Subject');
    component.set("v.sortFieldName", 'subject');
    helper.sortHelper(component, event, helper, 'subject');
  },
  sortDueDate: function(component, event, helper) {
    component.set("v.selectedTabsoft", 'ActivityDate');
    component.set("v.sortFieldName", 'ActivityDate');
    helper.sortHelper(component, event, helper, 'ActivityDate');
  },
  sortStatus: function(component, event, helper) {
    component.set("v.selectedTabsoft", 'status');
    component.set("v.sortFieldName", 'status');
    helper.sortHelper(component, event, helper, 'status');
  },
  sortCaseNumber: function(component, event, helper) {
    component.set("v.selectedTabsoft", 'what.name');
    component.set("v.sortFieldName", 'what.name');
    helper.sortHelper(component, event, helper, 'what.name');
  }
})
({
	doInit: function(component, event, helper) {
		helper.getCaseNumber(component, event);

		helper.getPicklistEntryMap(component, event);
	},
	save: function(component, event, helper) {
		helper.saveInteractionDetails(component, event);
	},
	closeScreen: function(component, event, helper) {

	}
})
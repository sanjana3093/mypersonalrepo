({
    getCaseNumber: function(component, event) {
        var action = component.get("c.getCaseNumber");
        action.setParams({
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.caseNumber", returnValue);
                this.setDefaultValues(component, event);
            }
        });
        $A.enqueueAction(action);
    },
    setDefaultValues: function(component, event) {
        var interaction = {
            "PatientConnect__PC_Patient_Program__c": component.get("v.caseNumber"),
            "PatientConnect__PC_Status__c": {},
            "PatientConnect__PC_Start__c": null,
            "PatientConnect__PC_Type__c": {},
            "PatientConnect__PC_End__c": null,
            "PatientConnect__PC_Participant__c": {},
            "spc_SOC_Nurse__c": "",
            "spc_SOC_Nurse_Contact__c": "",
            "spc_SOC_Type__c": {},
            "spc_SOC_Type_Other__c": "",
            "spc_HIP__c": {},
            "spc_Milestone__c": {},
            "spc_Reason_for_Status__c": {},
            "spc_BV_Completion_Date__c": null,
            "spc_PA_Submitted_Date__c": null,
            "spc_PA_Type__c": {},
            "spc_Denial_Date__c": null,
            "spc_Denial_Reason__c": "",
            "spc_Shipment_Status__c": "",
            "spc_Estimated_Shipment_date__c": null,
            "spc_Order_Date__c": null,
            "spc_Order_Type__c": {},
            "spc_Issue_Reason__c": "",
            "spc_Actual_Date_of_Completion__c": null,
            "spc_MD_Order_Date__c": null,
            "spc_Actual_Shipment_Date__c": null
        };
        component.set("v.interaction", interaction);
    },
    getPicklistEntryMap: function(component, event) {
        var action = component.get("c.getPicklistEntryMap");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.picklistEntryMap", returnValue);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    saveInteractionDetails: function(component, event) {
        var action = component.get("c.saveInteraction");
        var interaction = component.get("v.interaction");
        interaction.PatientConnect__PC_Patient_Program__c = component.get("v.recordId");
        action.setParams({
            interactionDetails: JSON.stringify(component.get("v.interaction"))
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();

            }
        });
        $A.enqueueAction(action);
    }
})
/**
 * Created by vkashikar on 9/11/2017.
 */
({  
    markCompleteActivity : function (component) {
        var objTask  = component.get("v.objTask");
        var action = component.get("c.closeTask");
        action.setParams({
            "taskID" : objTask.taskID
        });
        var self = this;
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS'){
                //component.set("v.objTask", a.getReturnValue());
                var mainDiv = component.find('main-div-element');
                $A.util.addClass(mainDiv, 'slds-transition-hide');
                
                //Fire update event
                var taskUpdatedEvent = component.getEvent("taskUpdated");
                taskUpdatedEvent.fire();
            } else if (state ==="ERROR"){
                var errors = a.getError();
                console.log(JSON.stringify(errors));
                var errText = component.get("v.errorText");
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errText);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    markHighPriorityActivity : function (component) {
        var objTask  = component.get("v.objTask");
        var action = component.get("c.updatePriority");
        action.setParams({
            "taskID" : objTask.taskID
        });
        var self = this;
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS'){
                var ret = a.getReturnValue();
                if(ret.svgIconName === 'fax') {
                    ret.svgIconName = 'task';
                }
                component.set("v.objTask", ret);
                var taskUpdatedEvent = component.getEvent("taskUpdated");
                taskUpdatedEvent.fire();
            } else if (state ==="ERROR"){
                var errors = a.getError();
                console.log(JSON.stringify(errors));
                var errText = component.get("v.errorText");
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errText);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    markDismissActivity : function (component) {
        var objTask  = component.get("v.objTask");
        var action = component.get("c.overrideAlert");
        action.setParams({
            "objAlertId" : objTask.taskID
        });
        var self = this;
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS'){
                var mainDiv = component.find('main-div');
                $A.util.addClass(mainDiv, 'transition-hide');
                var taskUpdatedEvent = component.getEvent("taskUpdated");
                taskUpdatedEvent.fire();
            } else if (state ==="ERROR"){
                var errors = a.getError();
                console.log(JSON.stringify(errors));
                var errText = component.get("v.errorText");
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errText);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            
        });
        $A.enqueueAction(action);
    }
})
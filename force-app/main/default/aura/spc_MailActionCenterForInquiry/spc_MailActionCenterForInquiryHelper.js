({
    getRecordTypeId: function(component) {
        var action = component.get("c.getRecordTypeId");
        action.setParams({  CaseID : component.get("v.recordId"), 
                         "invokeAction": "inquiry"});
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS'){
                if(a.getReturnValue().length>0 && a.getReturnValue()[0] === 'Marketing'){
                    component.set("v.actionPanel",true);    
                    component.set("v.MarketingConsentcheck", true);
                } else {
                    component.set("v.MarketingConsentcheck", false);
                	component.set("v.actionPanel",false);    
                    var params = {
                        "entityApiName": "PatientConnect__PC_Document__c",
                       "recordTypeId" :  a.getReturnValue()[0],
                       "defaultFieldValues" : {
                           "spc_InquiryCaseId__c" : component.get("v.recordId") ,
                           "PatientConnect__PC_Engagement_Program__c" : a.getReturnValue()[1],
                           "spc_isMailDocumentCreated__c" : true,
						   "PatientConnect__PC_Document_Status__c" : "Ready for Send"
                       }
                    }
                    var createRecordEvent = $A.get("e.force:createRecord"); 
                    createRecordEvent.setParams(params);
                    createRecordEvent.fire();
                    this.cancelBtn(component, event, helper);
                }
            } else if (state === "ERROR") {
                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
            }           
        });
        $A.enqueueAction(action); 
    } , 
    cancelBtn : function(component, event, helper) {
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
})
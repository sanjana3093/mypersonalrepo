({
	getPickListValues : function(component){
        var action = component.get("c.getPicklistEntryMap");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.picklistEntryMap", returnValue);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    setDefault: function(component, event){
        var missingInfo = component.get("v.missingInfo");
        if(missingInfo !== null){
            if(undefined !== missingInfo.patientInformationVal && "" !== missingInfo.patientInformationVal){
                missingInfo.patientInformation = true;
            }else{
            	missingInfo.patientInformation = false;
                missingInfo.patientInformationVal = "";
            }
            if(undefined !== missingInfo.hcpInformationVal && "" !== missingInfo.hcpInformationVal){
                missingInfo.hcpInformation = true;
            }else{
            	missingInfo.hcpInformation = false;
                missingInfo.hcpInformationVal = "";
            }
			if(undefined !== missingInfo.hcpSignatureDateVal && "" !== missingInfo.hcpSignatureDateVal){
                missingInfo.hcpSignatureDate = true;
            }else{
            	missingInfo.hcpSignatureDate = false;
                missingInfo.hcpSignatureDateVal = "";
            }
            if(undefined !== missingInfo.patientHIPAAConsentVal && component.get('v.hipaaConsentMissing') === ""){
                missingInfo.hipaaConsentAvailable = "true";
                if( "true" === missingInfo.patientHIPAAConsentVal){
                    missingInfo.patientHIPAAConsent = true;
                }else{
                    missingInfo.patientHIPAAConsent = false;
                }                
            }else{
                if(component.get('v.hipaaConsentMissing') === "No"){
                    missingInfo.patientHIPAAConsent = true;
                    missingInfo.patientHIPAAConsentVal = "true";
                    component.set('v.hipaaConsentReadOnly',true);
                     missingInfo.hipaaConsentAvailable = "true";
                }else if(component.get('v.hipaaConsentMissing') === "Yes"){
            		missingInfo.patientHIPAAConsent = false;
                	missingInfo.patientHIPAAConsentVal = "";
                    component.set('v.hipaaConsentReadOnly',true);
                    missingInfo.hipaaConsentAvailable = "true";
                }else{
                    component.set('v.hipaaConsentReadOnly',false);
                    missingInfo.hipaaConsentAvailable = "false";
                }
            }
            if(undefined !== missingInfo.insuranceInformationVal && "" !== missingInfo.insuranceInformationVal){
                missingInfo.insuranceInformation = true;
            }else{
            	missingInfo.insuranceInformation = false;
                missingInfo.insuranceInformationVal = "";
            }
			if(undefined !== missingInfo.siteOfCareVal && "" !== missingInfo.siteOfCareVal){
                missingInfo.siteOfCare = true;
            }else{
            	missingInfo.siteOfCare = false;
                missingInfo.siteOfCareVal = "";
            }
			if(undefined !== missingInfo.rxInformationVal && "" !== missingInfo.rxInformationVal){
                missingInfo.rxInformation = true;
            }else{
            	missingInfo.rxInformation = false;
                missingInfo.rxInformationVal = "";
            }
            component.set("v.missingInfo",missingInfo);
        }
    },
    getHIPAAConsent : function(component, event){
        var action = component.get("c.getHIPAAConsent");
        action.setParams({
            "enrollmentCaseId"  : component.get("v.enrollmentCase.enrollmentId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.hipaaConsentMissing", returnValue);
            } else {
                console.log("Failed with state: " + state);
            }
            this.setDefault(component,event);
        });
        $A.enqueueAction(action);
    }
})
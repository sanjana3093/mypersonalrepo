({
	 doInit: function(component, event, helper) {
        helper.getPickListValues(component, event);
        helper.getHIPAAConsent(component, event);
    },
	showSpinner: function(component, event) {
        var spinner = component.find('spinner');
         $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function(component, event) {
        var spinner = component.find('spinner');
         $A.util.addClass(spinner, "slds-hide");
    },
    onCheck: function(component, event) {
        var missingInfo = component.get("v.missingInfo");
        
        if(component.find("hcpSignatureDate").get("v.checked")){
            component.set("v.missingInfo.hcpSignatureDateVal","true");
        }else{
            component.set("v.missingInfo.hcpSignatureDateVal","");
        }
        if(component.find("insuranceInformation").get("v.checked")){
            component.set("v.missingInfo.insuranceInformationVal","true");
        }else{
            component.set("v.missingInfo.insuranceInformationVal","");
        }
        if(component.find("siteOfCare").get("v.checked")){
            component.set("v.missingInfo.siteOfCareVal","true");
        }else{
            component.set("v.missingInfo.siteOfCareVal","");
        }
    }
})
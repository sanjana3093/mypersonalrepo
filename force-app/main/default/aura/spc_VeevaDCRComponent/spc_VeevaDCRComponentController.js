({
    doInit: function(component, event, helper) {
        setTimeout(function() {
            var path = component.get("v.pathofveeva");
            var veevaAccountId = component.get("v.veevaAccountId");
        }, 1000)

    },
    OpenVeevaModal: function(component, event, helper) {
        var modalVeeva = component.find('modalVeevadialog');
        var path = component.get("v.pathofveeva");
        var veevaAccountId = component.get("v.veevaAccountId");
        component.set("v.pathofveevaAcc", path+"?id=" + veevaAccountId);
        $A.util.toggleClass(modalVeeva, 'slds-fade-in-hide');
        $A.util.toggleClass(modalVeeva, 'slds-fade-in-open');
        var backdropVeeva = component.find('backdropVeeva');
        $A.util.toggleClass(backdropVeeva, 'slds-backdrop--hide');
        $A.util.toggleClass(backdropVeeva, 'slds-backdrop--open');
        var isVeevaWidgetOpened = component.get('v.isVeevaWidgetOpened');
        if (isVeevaWidgetOpened) {
            helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-', helper);
            helper.showPopupHelper(component, 'backdrop', 'slds-backdrop--', helper);
            component.set('v.isVeevaWidgetOpened', false);
        } else {
            helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
            helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
            component.set('v.isVeevaWidgetOpened', true);
        }

    }
})
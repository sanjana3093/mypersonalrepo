({
    initLetters: function(component) {
        var action = component.get("c.getLetters");
        action.setParams({
            "recordId": component.get("v.recordId"),
            "invokeAction": "actioncenter",
            "isFaxOnly": true
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS') {
                var lstAvailableeLetters = a.getReturnValue();
                var opts = [];
                var eLetterChannels = [];
                if (lstAvailableeLetters.length > 0 && lstAvailableeLetters[0].name === 'hasHIPAAServiceConsent') {
                    component.set("v.checkHIPAAServiceConsent", true);
                } else {
                    component.set("v.checkHIPAAServiceConsent", false);
                    var inputsel = component.find("inputSelectLetter");
                    for (var i = 0; i < lstAvailableeLetters.length; i++) {
                        var currentVal = lstAvailableeLetters[i];
                        opts.push({
                            "class": "optionClass",
                            label: currentVal.name,
                            value: currentVal.value
                        });
                        eLetterChannels.push({
                            id: currentVal.value,
                            faxValue: currentVal.showFaxRow,
                            emailValue: currentVal.showEmailRow
                        });
                    }
                    inputsel.set("v.options", opts);
                    component.set("v.eletterChannelMap", eLetterChannels);
                }
            } else if (state === "ERROR") {
                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
                var toggleText = component.find("msgResult");
                $A.util.toggleClass(toggleText, "toggleDisplay");
            }

        });
        $A.enqueueAction(action);
    },

    loadRecepients: function(component) {
        var eletterChannelMap = component.get("v.eletterChannelMap");
        var SelectedLetterId = component.find("inputSelectLetter").get("v.value");
        for (var key in eletterChannelMap) {
            if (eletterChannelMap[key].id === SelectedLetterId) {
                component.set("v.hideFaxRow", eletterChannelMap[key].faxValue);
                component.set("v.hideEmailRow", eletterChannelMap[key].emailValue);
            }
        }
        var action = component.get("c.getRecepients");
        action.setParams({
            "recordId": component.get("v.recordId"),
            "letterId": SelectedLetterId,
            "isFaxSpecific": true
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS') {
                component.set("v.recepients", a.getReturnValue());
                if (a.getReturnValue() === null) {
                    component.set("v.message", 'No recepients available');
                    var toggleText = component.find("msgResult");
                    $A.util.toggleClass(toggleText, "toggleDisplay");
                    $A.util.toggleClass(toggleText, "msgclass");

                }
            } else if (state === "ERROR") {
                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
                var toggleText = component.find("msgResult");
                $A.util.toggleClass(toggleText, "toggleDisplay");
            }

        });
        $A.enqueueAction(action);
    },
    sendDocuments: function(component) {
        var action = component.get("c.sendOutboundDocuments");
        action.setParams({
            //"recepientsString": $A.util.json.encode(component.get("v.recepients")),
            //$A.util.json.encode does not work in locker service and hence is replaced with JSON.stringify
            "recipientsString": JSON.stringify(component.get("v.recepients")),
            "letterId": component.find("inputSelectLetter").get("v.value"),
            "objectId": component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state = a.getState();

            if (state === 'SUCCESS') {
                component.set("v.message", a.getReturnValue());
                var toggleText = component.find("msgResult");
                var nofaxnumber = $A.get("$Label.c.spc_InvalidFaxNumberPrefix");
                var norecord = $A.get("$Label.PatientConnect.PC_No_Record_Selected");

                if (a.getReturnValue().includes(nofaxnumber) || a.getReturnValue().includes(norecord)) {

                    $A.util.addClass(toggleText, "msgclass");
                } else {
                    $A.util.removeClass(toggleText, "msgclass");
                }
                $A.util.toggleClass(toggleText, "toggleDisplay");
            } else if (state === "ERROR") {
                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
                var toggleText = component.find("msgResult");
                $A.util.toggleClass(toggleText, "toggleDisplay");
            }

        });
        $A.enqueueAction(action);
    },
    displayError: function(a) {
        var errors = a.getError();
        if (errors && errors[0] && errors[0].message) {
            component.set("v.message", errors[0].message);
        } else {
            component.set("v.message", 'Unknown error');
        }
    }
})
({
	doInit: function(component, event, helper) {
		helper.initLetters(component);
	},
	onChangeSelectedLetter: function(component, event, helper) {
		helper.loadRecepients(component);
	},
	onSend: function(component, event, helper) {
		helper.sendDocuments(component);
	}
})
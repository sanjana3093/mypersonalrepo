({
    helperMethod: function() {},
    clearprevSelected: function(component, event, searchResult) {
        var i;
        for (i = 0; i < searchResult.length; i++) {
            searchResult[i]['isSelected'] = "false";
            if (searchResult[i].prevVal === "false") {
                searchResult[i].remsID = '';
            }
        }
        component.set("v.searchResults", searchResult);
    },
    enableHip: function(component, event) {
        var socVal = component.find('soctypeId');
        if (socVal !== undefined) {
            var value = component.find('soctypeId').get("v.value");
            if (value === 'Other') {
                component.set("v.SocTypeRequired", true);
            } else {
                component.set("v.SocTypeRequired", false);
            }
            if (value === 'Home') {
                component.set("v.HipRequired", true);
            } else {
                component.set("v.HipRequired", false);
            }
            if (value === 'Clinic' || value === 'Hospital') {

                component.set("v.SocRequired", true);

            } else {
                component.set("v.SocRequired", false);

            }
        }

    },
    getPicklistVaules: function(component, event) {
        try {

            var action = component.get("c.getPicklistEntryMap ");
            action.setCallback(this, function(a) {
                var state = a.getState();
                var rtnValue = a.getReturnValue();
                if (state === "SUCCESS") {
                    component.set("v.picklistEntryMap", rtnValue);
                } else if (state === 'ERROR') {
                    console.log(a.getError());
                }
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log(e);
        }
    },
    getHeaders: function(component, event, helper) {
        var action = component.get("c.getFields");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set('v.HCOTableHeaders', returnValue);
            } else {
                console.log("searchRecord: Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);

    },
    checkforCentralHIP: function(component, event) {
        //component.set("v.hipDisabled", true);
        var interaction = component.get("v.interaction");
        interaction.spc_HIP__c = '';
        component.set("v.interaction", interaction);
    },

    setsearchFilterOptions: function(component, event, helper) {
        var action = component.get("c.getHCOTypes");
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set('v.searchFilterOptions', returnValue);
            } else {
                console.log("getHCOTypes: Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    searchRecord: function(component, event, helper) {
        helper.toggleSpinner(component, 'searchResultSpinner');
        var searchString = component.get("v.searchString");
        var searchFilter = component.find("searchFilterOptions").get("v.value");
        searchFilter = searchFilter === $A.get("$Label.PatientConnect.PC_Picklist_All") ? "" : searchFilter;
        if (searchString !== "" && searchString.length >= 2) {
            var action = component.get("c.searchRecords");
            action.setParams({
                'searchString': searchString,
                'searchFilter': searchFilter
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                helper.toggleSpinner(component, 'searchResultSpinner');
                if (component.isValid() && state === "SUCCESS") {

                    var returnValue = response.getReturnValue();
                    component.set('v.searchResults', returnValue);
                    if (returnValue.length > 0) {
                        $A.util.addClass(component.find('searchResultMsg'), 'searchResultMsgBlockHidden');
                    } else {
                        $A.util.removeClass(component.find('searchResultMsg'), 'searchResultMsgBlockHidden')
                    }

                } else {
                    console.log("searchRecord: Failed with state: " + state);
                }

                
            });
            $A.enqueueAction(action);
        } else {}
    },
    searchRecordsByIds: function(component, event, helper, ids) {
        var action = component.get("c.searchRecordsByIds");
        action.setParams({
            'accountIds': ids
        });
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set('v.selectedResults', returnValue);
                var item;
                for (var i = 0; i < returnValue.length; i++) {
                    item = returnValue[i];
                    if (component.get("v.selectedHCOIds").indexOf(item.Id) === -1) {
                        component.get("v.selectedHCOIds").push(item.Id)
                    }
                }
            } else {
                console.log("searchRecordsByIds: Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    renderSelectedHCOInSearchBlock: function(component, event, helper) {
        var selectedResults = component.get('v.selectedResults');
        var selectedHCOIds = component.get("v.selectedHCOIds");
        var searchResults = component.get('v.searchResults');

        for (var i = 0; i < searchResults.length; i++) {
            if (selectedHCOIds.indexOf(searchResults[i].Id) === -1) {
                // not found case
                searchResults[i].isSelected = "false";
            } else {
                // found case
                searchResults[i].isSelected = "true";
            }
        }
        component.set('v.searchResults', searchResults);
    },

    handleOnlineForm: function(component, enrollmentCaseId, helper) {
        var action = component.get("c.getApplicantHCO");
        action.setParams({
            "enrollmentCaseId": enrollmentCaseId,
            "activeApplicantId": component.get("v.enrollmentCase.applicantId") //[PC-1379] Handles multiple applicants on enrollment case
        });

        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var returnValue = a.getReturnValue();
                component.set("v.onlineApplicantHCO", returnValue);
                var onlineAppHCO = component.get("v.onlineApplicantHCO");
                var showRefTable;
                if ($A.util.isUndefinedOrNull(onlineAppHCO) || onlineAppHCO === '') {

                    showRefTable = false;
                } else {
                    showRefTable = true;
                }
                component.set("v.showRefTable", showRefTable);
                //Initialize search string with applicant's HCO name
                if (showRefTable) {
                    var initSearchString = returnValue[0].name;
                    //If HCO name found, set the search string with HCO name
                    if (initSearchString !== null || initSearchString !== undefined) {
                        component.set("v.searchString", initSearchString);
                        var searchString = component.get("v.searchString");
                        helper.searchRecord(component, event, helper);
                    }
                }

            } else if (a.getState() === "ERROR") {
                var errors = a.getError();
                console.log(JSON.stringify(errors));
                var errText = $A.get("$Label.PatientConnect.PC_Lighting_Error_Message");
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errText);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);

    },
    toggleSpinner: function(cmp, spinnerAuraId) {
        var spinner = cmp.find(spinnerAuraId);
        var evt = spinner.get("e.toggle");

        if (!$A.util.hasClass(spinner, 'hideEl')) {
            evt.setParams({
                isVisible: false
            });
        } else {
            evt.setParams({
                isVisible: true
            });
        }
        evt.fire();
    },
    getAddress: function(component, event, hcoid, helper) {
        var action = component.get("c.getSocAddresses");
        var primaryaddressExists = 0;
        var returnValue;
        action.setParams({
            "hcoid": hcoid
        });
        action.setCallback(this, function(response) {

            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                returnValue = response.getReturnValue();
                component.set('v.searchAddressResults', returnValue);
                if (returnValue.length >= 1) {
                    for (var i = 0; i < returnValue.length; i++) {
                        if (returnValue[i].PatientConnect__PC_Primary_Address__c) {
                            primaryaddressExists++;
                            component.set("v.primaryaddressIndex", i);
                        }
                    }
                    if (!primaryaddressExists) {
                        returnValue[0].PatientConnect__PC_Primary_Address__c = "true";
                        component.set('v.searchAddressResults', returnValue);
                        component.set("v.primaryaddressIndex", null);

                    }

                }
                helper.addSelection(component, event, helper);

            } else {
                console.log("searchRecord: Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);

    },

    changeAddress: function(component, event, selectedAddress, index) {
        var selectedResult = component.get("v.selectedResults");
        selectedResult.addressId = selectedAddress.Id;
        selectedResult.primaryAddress1 = selectedAddress.PatientConnect__PC_Address_1__c;
        selectedResult.primaryAddress2 = selectedAddress.PatientConnect__PC_Address_2__c;
        selectedResult.primaryAddress3 = selectedAddress.PatientConnect__PC_Address_3__c;
        selectedResult.primaryZipCode = selectedAddress.PatientConnect__PC_Zip_Code__c;
        selectedResult.primaryCity = selectedAddress.PatientConnect__PC_City__c;
        selectedResult.primaryState = selectedAddress.PatientConnect__PC_State__c;
        selectedResult.phone1 = selectedAddress.spc_Phone__c;
        selectedResult.fax1 = selectedAddress.spc_Fax__c;
        selectedResult.phone2 = selectedAddress.spc_Phone_2__c;
        selectedResult.fax2 = selectedAddress.spc_Fax_2__c;
        component.set("v.selectedResults", selectedResult);
        if (selectedAddress.spc_Phone__c || selectedAddress.spc_Fax__c || selectedAddress.spc_Phone_2__c || selectedAddress.spc_Fax_2__c) {
            component.set("v.phoneFax", true);
        } else {
            component.set("v.phoneFax", false);
        }

        selectedResult = component.get("v.selectedResults");

    },
    addSelection: function(component, event, helper) {
        var id = component.get("v.SOCId");
        var index = component.get("v.SOCIndex");
        var primaryaddressIndex = component.get("v.primaryaddressIndex");
        var addressIndex = component.get("v.addressIndex");
        var searchResults = component.get("v.searchResults");

        component.set("v.interaction.socNameId", id);
        searchResults[index]['isSelected'] = "true";

        component.set("v.selectedResults", searchResults[index]);
        component.set("v.searchResults", searchResults);
        var searchAddressResults = component.get("v.searchAddressResults");
        if (searchAddressResults.length > 0) {
            if (primaryaddressIndex !== null && primaryaddressIndex !== undefined) {
                helper.changeAddress(component, event, searchAddressResults[primaryaddressIndex]);
            } else {
                helper.changeAddress(component, event, searchAddressResults[0])
            }
            if (addressIndex !== null && addressIndex !== undefined) {
                helper.changeAddress(component, event, searchAddressResults[addressIndex]);
            } else {

                if (!searchResults[index].addressId) {
                    helper.changeAddress(component, event, searchAddressResults[0])
                }

            }
        }

    },
    addSelectedAddress: function(component, event, selectedResult) {
        var searchAddressResults = {
            account: "",
            address: "",
            addressType: "",
            PatientConnect__PC_Address_1__c: "",
            PatientConnect__PC_State__c: "",
            stateLabel: "",
            PatientConnect__PC_Address_2__c: "",
            PatientConnect__PC_Zip_Code__c: "",
            PatientConnect__PC_Address_3__c: "",
            PatientConnect__PC_City__c: "",
            spc_Fax__c: "",
            spc_Phone__c: "",
            spc_Phone_2__c: "",
            spc_Fax_2__c: ""

        };

        searchAddressResults.PatientConnect__PC_Primary_Address__c = "true";
        searchAddressResults.PatientConnect__PC_Address_1__c = selectedResult.primaryAddress1;
        searchAddressResults.PatientConnect__PC_City__c = selectedResult.primaryCity;
        searchAddressResults.PatientConnect__PC_State__c = selectedResult.primaryState;
        searchAddressResults.PatientConnect__PC_Zip_Code__c = selectedResult.primaryZipCode;
        component.set("v.searchAddressResults", searchAddressResults);
        if (selectedResult.assnPhone === selectedResult.phone1) {

            if (component.find('phone1')) {
                component.find('phone1').set("v.value", true);
            }

        } else if (selectedResult.assnPhone === selectedResult.phone2) {

            if (component.find('phone2')) {
                component.find('phone2').set("v.value", true);
            }

        } else if (selectedResult.assnPhone) {

            component.set("v.addnewPhone", true);
            component.find('newPhone').set("v.value", true);
        }
        if (selectedResult.assnFax === selectedResult.fax1) {
            if (component.find('fax1')) {
                component.find('fax1').set("v.value", true);
            }

        } else if (selectedResult.assnFax === selectedResult.fax2) {
            if (component.find('fax2')) {
                component.find('fax2').set("v.value", true);
            }

        } else if (selectedResult.assnFax) {
            component.set("v.addNewFax", true);
            component.find('newFax').set("v.value", true);
        }
        if (selectedResult.prevVal === "true") {
            component.set("v.socRemsIdExist", true);
        }

    },
    clearSelection: function(component, event) {
        var buttons = ['phone2', 'fax1', 'fax2', 'phone1', 'newPhone', 'newFax'];
        var i;
        var buttonEl;
        for (i = 0; i < buttons.length; i++) {
            if (component.find(buttons[i])) {
                buttonEl = component.find(buttons[i]);
                if (buttonEl) {
                    if (Array.isArray(buttonEl)) {
                        buttonEl[0].set("v.value", false);
                    } else {
                        buttonEl.set("v.value", false);
                    }
                }

            }
        }

    }
})
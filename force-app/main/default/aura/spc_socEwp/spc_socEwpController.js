/*********************************************************************************
* @author           : Deloitte
* @description      : component to create/edit SOC
* @date     		: 05-July-18
*********************************************************************************/

({
    doInit: function(component, event, helper) {
        helper.getHeaders(component, event, helper);
        helper.setsearchFilterOptions(component, event, helper);
        helper.getPicklistVaules(component, event);

        //Check if it is online form
        if (component.get("v.enrollmentCase.isOnlineApplicant")) {
            var enrollmentCaseId = component.get("v.enrollmentCase.enrollmentId");
            helper.handleOnlineForm(component, enrollmentCaseId, helper);
        }

        if (component.get("v.selectedResults") !== null) {
            var selectedResult = component.get("v.selectedResults");
            helper.addSelectedAddress(component, event, selectedResult);
            var searchResults = component.get("v.searchResults");
            searchResults.push(selectedResult);
            component.set("v.searchResults", searchResults);
            component.set("v.SOCId" , selectedResult.Id);

        } else {
            helper.searchRecord(component, event, helper);
        }

    },
    showSpinner: function(component, event) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function(component, event) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, "slds-hide");
    },
    clearSelection: function(component, event, helper) {
        var x;
        var y;
        component.set("v.searchResults", x);
        component.set("v.selectedResults", y);
    },
    afterScriptsLoaded: function(component, event, helper) {
        setTimeout(function() {
            helper.enableHip(component, event);
        }, 2000);
    },
    searchRecord: function(component, event, helper) {
        component.set("v.pageErrors", []);
        var searchString = component.get("v.searchString");
        if (searchString !== "" && searchString.trim().length >= 2) {
            helper.searchRecord(component, event, helper);
        } else {
            var pageErrors = component.get("v.pageErrors");
            pageErrors.push($A.get("$Label.PatientConnect.PC_FieldError_SearchString"));
            component.set("v.pageErrors", pageErrors);
        }
    },
    searchEvents: function(component, event, helper) {
        if (event.getParams().keyCode === 13) {
            var searchString = component.get("v.searchString");
            if (searchString !== "" && searchString.trim().length >= 2) {
                helper.searchRecord(component, event, helper);
            }
        }
    },
    onAdd: function(component, event, helper) {

        var currentNode = event.currentTarget;
        var searchResult = component.get("v.searchResults");

        var index = currentNode.getAttribute("data-text");
        var id = currentNode.getAttribute("data-name");
        component.set("v.SOCIndex", index);
        component.set("v.SOCId", id);
        component.set("v.addressIndex", null);
        component.set("v.phoneFax", false);
        helper.getAddress(component, event, id, helper);

        if (searchResult[index].remsID) {
            component.set("v.socRemsIdExist", true);
            searchResult[index].prevVal = "true";
        } else {
            component.set("v.socRemsIdExist", false);
            searchResult[index].prevVal = "false";
        }
        helper.clearprevSelected(component, event, searchResult);
    },

    onSelectAddress: function(component, event, helper) {
        var index = event.getSource().get("v.text");
        component.set("v.addressIndex", index);
        var selectedResults = component.get("v.selectedResults");
        selectedResults.assnFax = '';
        selectedResults.assnPhone = '';
        component.set("v.selectedResults", selectedResults);
        helper.clearSelection(component, event);
        component.set("v.addnewPhone", false);
        component.set("v.addNewFax", false);
        helper.clearSelection(component, event);
        helper.addSelection(component, event, helper);

    },
    selectRecords: function(component, event, helper) {},
    onRemove: function(component, event, helper) {

        var currentNode = event.currentTarget;

        var index = currentNode.getAttribute("data-text");
        var id = currentNode.getAttribute("data-name");

        var selectedHCOIds = component.get("v.selectedHCOIds");

        if (selectedHCOIds.indexOf(id) !== -1) {

            selectedHCOIds.splice(selectedHCOIds.indexOf(id), 1);
            component.set("v.selectedHCOIds", selectedHCOIds);
            var interaction = component.get("v.interaction");
            interaction.soctypeId = selectedHCOIds[0];
            component.set("v.interaction", interaction);
            interaction = component.get("v.interaction");
            var updatedSelectedResults = component.get("v.selectedResults");
            for (var i = 0; i < updatedSelectedResults.length; i++) {
                if (updatedSelectedResults[i].Id === id) {
                    updatedSelectedResults.splice(i, 1);
                    break;
                }
            }

            var updatedSearchResults = component.get("v.searchResults");
            for (var i = 0; i < updatedSearchResults.length; i++) {
                if (updatedSearchResults[i].Id === id) {
                    updatedSearchResults[i]['isSelected'] = "false";
                    break;
                }
            }

            component.set("v.selectedResults", updatedSelectedResults);
            component.set("v.searchResults", updatedSearchResults);
        }
        if (selectedHCOIds.length < 1) {
            component.set("v.enablFax", true);
        }

    },

    showRecordDetails: function(component, event, helper) {
        var currentNode = event.currentTarget;
        var index = currentNode.getAttribute("data-text");
        var id = currentNode.getAttribute("data-name");
    },
    searchOnlineRecord: function(component, event, helper) {
        var currentNode = event.currentTarget;
        var searchText = currentNode.dataset.text;
        component.set("v.searchString", searchText);
        helper.searchRecord(component, event, helper);
    },
    socTypeChanged: function(component, event, helper) {
        var value = component.find('soctypeId').get("v.value");
        if (value === 'Other') {
            component.set("v.SocTypeRequired", true);
        } else {
            component.set("v.SocTypeRequired", false);
        }
        if (value === 'Home') {
            component.set("v.HipRequired", true);
        } else {
            component.set("v.HipRequired", false);
        }
        if (value === 'Clinic' || value === 'Hospital') {

            component.set("v.SocRequired", true);
        } else {
            component.set("v.SocRequired", false);
        }

    },
    hipChange: function(component, event, helper) {
        var hipVal = component.find('HipId').get("v.value");
        if (hipVal === 'Other') {

            component.set("v.SocRequired", true);
        } else {
            component.set("v.SocRequired", false);
        }
    },
    validate: function(component, event, helper) {
        // This method is automatically called by the framework
        // var HipRequired = component.get("v.HipRequired");
        var SocRequired = component.get("v.SocRequired");
        var SocTypeRequired = component.get("v.SocTypeRequired");
        var selectedResults = component.get("v.selectedResults");
        var selectedAddress = component.get("v.selectedAddress");
        var phoneNumberValid = /^[0-9-+()*#\s]*$/;
        var phoneNumber = /^[0-9]$/;
        var addnewPhone = component.get("v.addnewPhone");
        var addNewFax = component.get("v.addNewFax");
        var interaction = component.get("v.interaction");
        var hip = component.find('soctypeOther').get("v.value");
        component.set("v.isValid", true);
        component.set("v.pageErrors", []);
        if (addnewPhone) {
            component.find('newAssnPhone').set("v.errors", [{
                message: ""
            }]);
            $A.util.removeClass(component.find('newAssnPhone'), "error");
        }
        component.find('soctypeOther').set("v.errors", [{
            message: ""
        }]);
        $A.util.removeClass(component.find('soctypeOther'), "error");
        var validName = /^[A-Za-z]+$/;
        if (!$A.util.isEmpty(hip) && ((hip.match(validName) === null) || hip.length > 50)) {
            document.getElementById('form').scrollIntoView({
                block: 'start',
                behavior: 'smooth'
            });
            component.set("v.isValid", false);
            $A.util.addClass(component.find('soctypeOther'), "error");
            component.find('soctypeOther').set("v.errors", [{
                message: "Please enter a valid SOC Type Other of max 50 alphabets."
            }])
            component.set("v.pageErrors", ["Please enter a valid SOC Type Other of max 50 alphabets."]);
        }
        if (SocRequired) {
            var socName = component.get("v.interaction");
            var persistentAttributes = component.get("v.persistentAttributes");
            if (socName.socNameId) {
                component.set("v.isValid", true);

            } else {
                component.set("v.isValid", false);
                component.set("v.pageErrors", ["Please fill the SOC Name"]);
                $A.util.addClass('slds-has-error', component.find('inputSearch'));
            }
        }
        if (SocTypeRequired) {

            if (hip) {
                component.set("v.isValid", true);
                $A.util.addClass('slds-has-error', component.find('soctypeOther'));
            } else {
                component.set("v.isValid", false);
                component.set("v.pageErrors", ["Please fill the SOC Type Other"]);
                $A.util.addClass('slds-has-error', component.find('soctypeOther'));

            }
        }
        if (addnewPhone) {
            var phone = component.find('newAssnPhone').get("v.value");
            if (!$A.util.isEmpty(phone) && ((phone.match(phoneNumber) === null) && (phone.match(phoneNumberValid) === null))) {
                component.set("v.isValid", false);
                $A.util.addClass(component.find('newAssnPhone'), "error");
                component.find('newAssnPhone').set("v.errors", [{
                    message: "Please enter a valid Phone number."
                }])
                component.set("v.pageErrors", ["Please enter a valid Phone number."]);
            }
        }

    },
    itemsChange: function(component, event, helper) {
        var valOfSoc = component.get("v.Socname");

        component.set("v.interaction.PatientConnect__PC_Participant__c", valOfSoc);

    },
    enableHip: function(component, event, helper) {
        helper.enableHip(component, event);

    },
    setInSoc: function(component, event, helper) {
        var val = component.get("v.selectedLookupValueID");
        var value = component.get("v.selectedLookupValue");
        component.set("v.interaction.socNameId", val);
        component.set("v.interaction.PatientConnect__PC_Participant__c", component.get("v.selectedLookupValue"));

    },
    setInCentralHip: function(component, event, helper) {

        var val = component.get("v.selectedHipLookupValueID");
        component.set("v.interaction.centralHipId", val);

    },
    setInRegionalHip: function(component, event, helper) {
        var val = component.get("v.RegionalId");
        component.set("v.interaction.regionalHipId", val);

    },
    addNewPhone: function(component, event, helper) {
        var id = event.getSource().get("v.text");
        var selectedResult = component.get("v.selectedResults");
        if (id === 'newPhone') {
            component.set("v.addnewPhone", true);
            component.find('newAssnPhone').set("v.value", '');
        } else {
            component.set("v.addnewPhone", false);
        }

        if (id === 'phone1') {
            selectedResult.assnPhone = selectedResult.phone1;
        } else if (id === 'phone2') {
            selectedResult.assnPhone = selectedResult.phone2;
        }

        component.set("v.selectedResults", selectedResult);

    },
    addNewFax: function(component, event, helper) {
        var id = event.getSource().get("v.text");
        var selectedResult = component.get("v.selectedResults");
        if (id === 'newFax') {
            component.set("v.addNewFax", true);
            var val = component.find('newAssnFax').get("v.value");
            component.find('newAssnFax').set("v.value", '');

        } else {
            component.set("v.addNewFax", false);
        }

        if (id === 'fax1') {
            selectedResult.assnFax = selectedResult.fax1;
        } else if (id === 'fax2') {
            selectedResult.assnFax = selectedResult.fax2;
        }
        
        component.set("v.selectedResults", selectedResult);

    },

})
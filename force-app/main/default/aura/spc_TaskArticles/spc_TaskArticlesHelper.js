({
    getArticles: function(component) {
        var action = component.get("c.getArticlesByTaskId");
        action.setParams({
            "taskId": component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS') {
                var relatedArticles = a.getReturnValue();
                component.set("v.articlesList", relatedArticles);
            } else if (state === "ERROR") {
                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
                var toggleText = component.find("msgResult");
                $A.util.toggleClass(toggleText, "toggleDisplay");
            }
        });
        $A.enqueueAction(action);
    }
})
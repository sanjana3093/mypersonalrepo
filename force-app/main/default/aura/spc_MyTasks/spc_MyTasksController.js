({
    doInit : function(component, event, helper) {
        
        var currentPagenumber = component.get("v.currentPagenumber");

        var recordToDisply = '10';
        component.set("v.groupByParam","Patient");
        //helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply, "0");
        helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 0);
        
    },
    
    handleTaskUpdated : function(component, event, helper) {        
        var currentPagenumber = component.get("v.currentPagenumber");
        var totalNumberOfPages = component.get("v.totalNumberOfPages");
        var totalRecords = component.get("v.totalRecords");
        var sortType=component.get("v.chosenFilter");
        var recordToDisply;
        if(component.get("v.recordSize")!== null){
            recordToDisply=component.get("v.recordSize");
        }else{
            recordToDisply= Math.ceil(totalRecords / totalNumberOfPages)
            if(recordToDisply<=10){
                recordToDisply=10;
            }
            else if(recordToDisply<=20){
                recordToDisply=20;
            }
                else{
                    recordToDisply=30;
                }
        }
        
          if(sortType === 'filterTodaysItems') {
                helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply, "3");

             } else if(sortType === 'filterTomorrowItems') {

                 helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply, "4");

             }else if(sortType === 'filterOverdueItems') {
                 helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply, "1");

             }else if(sortType === 'soryByPriority') {

                 helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply, "7");
             }else if(sortType === null || sortType === 'soryByDueDate') {
				if($A.util.isEmpty(component.get("v.groupByParam")) || component.get("v.groupByParam") === "None"){
                 	helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply, "0");
                }else{
                    helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 0);
                }
              }
        
    },
    
    handleMoreOption: function(component, event, helper) {
        var mainDiv = component.find('main-div');
        $A.util.addClass(mainDiv, 'slds-is-open');
    },
    
    handleMoreOptionLeave: function(component, event, helper) {
        component.set("v.dropdownOver",false);
        var mainDiv = component.find('main-div');
        $A.util.removeClass(mainDiv, 'slds-is-open');
    },
    
    handleMoreOptionEnter: function(component, event, helper) {
        component.set("v.dropdownOver",true);
    },
    
    handleMoreOptionOut: function(component, event, helper) {
        window.setTimeout(
            $A.getCallback(function() {
                if (component.isValid()) {
                    //if dropdown over, user has hovered over the dropdown, so don't close.
                    if (component.get("v.dropdownOver")) {
                        return;
                    }
                    var mainDiv = component.find('main-div');
                    $A.util.removeClass(mainDiv, 'slds-is-open');
                }
            }), 200
        );
    },

        showHighPriority :function(component, event, helper){
        
        var currentPagenumber = component.get("v.currentPagenumber");
        var totalNumberOfPages = component.get("v.totalNumberOfPages");
        var totalRecords = component.get("v.totalRecords");
        var recordToDisply;
        if(component.get("v.recordSize")!== null){
            recordToDisply=component.get("v.recordSize");
        }else{
            recordToDisply= Math.ceil(totalRecords / totalNumberOfPages)
            if(recordToDisply<=10){
                recordToDisply=10;
            }
            else if(recordToDisply<=20){
                recordToDisply=20;
            }
                else{
                    recordToDisply=30;
                }
        }
        if($A.util.isEmpty(component.get("v.groupByParam")) || component.get("v.groupByParam") === "None"){
            helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply, "0");
        }else{
            helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 0);
        }
        
        component.set("v.dropdownOver",false);
        var mainDiv = component.find('main-div');
        $A.util.removeClass(mainDiv, 'slds-is-open');
    },
    sort : function(component, event, helper) {
        var currentPagenumber = component.get("v.currentPagenumber");
        var totalNumberOfPages = component.get("v.totalNumberOfPages");
        var totalRecords = component.get("v.totalRecords");
        var recordToDisply= Math.ceil(totalRecords / totalNumberOfPages);
        var groupByOptionSelected = component.get("v.groupByParam");
        if(recordToDisply<=10){
            recordToDisply=10;
        }
        else if(recordToDisply<=20){
            recordToDisply=20;
        }
            else{
                recordToDisply=30;
            }
        var sortType = component.get("v.filterByParam");
        component.set("v.chosenFilter",sortType );
        //helper.setSelectedMenuItem(event.currentTarget);
        if(sortType === 'filterTodaysItems') {
            if(groupByOptionSelected === 'Physician' || groupByOptionSelected === 'Patient' || groupByOptionSelected === 'SOC' || groupByOptionSelected === 'HIP'){
                            component.set("v.groupByNone", false);	
                            helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 3);
                        }else{
                            component.set("v.groupByNone", true);	
                            helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply,"3");
                        }
        }
        else if(sortType === 'filterTomorrowItems') {
            if(groupByOptionSelected === 'Physician' || groupByOptionSelected === 'Patient'|| groupByOptionSelected === 'SOC' || groupByOptionSelected === 'HIP'){
                            component.set("v.groupByNone", false);	
                            helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 4);
                        }else{
                            component.set("v.groupByNone", true);	
                            helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply,"4");
                        }
            
        }
            else if(sortType === 'filterOverdueItems') {
                if(groupByOptionSelected === 'Physician' || groupByOptionSelected === 'Patient' || groupByOptionSelected === 'SOC' || groupByOptionSelected === 'HIP'){
                            component.set("v.groupByNone", false);	
                            helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 1);
                        }else{
                            component.set("v.groupByNone", true);	
                            helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply,"1");
                        }
                
            }
                else if(sortType === 'soryByPriority') {
                    if(groupByOptionSelected === 'Physician' || groupByOptionSelected === 'Patient' || groupByOptionSelected === 'SOC' || groupByOptionSelected === 'HIP'){
                            component.set("v.groupByNone", false);	
                            helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 7);
                        }else{
                            component.set("v.groupByNone", true);	
                            helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply,"7");
                        }
                    
                }
                    else if(sortType === 'soryByDueDate') {
                        
                        if(groupByOptionSelected === 'Physician' || groupByOptionSelected === 'Patient' || groupByOptionSelected === 'SOC' || groupByOptionSelected === 'HIP'){
                            component.set("v.groupByNone", false);	
                            helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 0);
                        }else{
                            component.set("v.groupByNone", true);	
                            helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply,"0");
                        }
                        
                    }
      
    },
    
    handleMyApplicationEvent : function(component, event, helper) {
        
        var groupByOptionSelected = component.get("v.groupByParam");
        var sortType=component.get("v.chosenFilter");
        var sourceId = event.getParam("sourceId");
        if(sourceId === 'MyTasks'){
            var currentPagenumber = event.getParam("currentPagenumber");
            var recordToDisply=event.getParam("totalNumberOfPages");
            
            if(sortType === 'filterTodaysItems') {
                if($A.util.isEmpty(component.get("v.groupByParam")) || groupByOptionSelected === 'None'){
                            	helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply, "3");
                            }else{
                                helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 3);
                            }
            }
            else if(sortType === 'filterTomorrowItems') {
                
                if($A.util.isEmpty(component.get("v.groupByParam")) || groupByOptionSelected === 'None'){
                            	helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply, "4");
                            }else{
                                helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 4);
                            }
                
            }
                else if(sortType === 'filterOverdueItems') {
                    if($A.util.isEmpty(component.get("v.groupByParam")) || groupByOptionSelected === 'None'){
                            	helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply, "1");
                            }else{
                                helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 1);
                            }
                    
                }
                    else if(sortType === 'soryByPriority') {
                        if($A.util.isEmpty(component.get("v.groupByParam")) || groupByOptionSelected === 'None'){
                            	helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply, "7");
                            }else{
                                helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 7);
                            }
                    }
                        else if(sortType === null || sortType === 'soryByDueDate') {
                            
        					if($A.util.isEmpty(component.get("v.groupByParam")) || groupByOptionSelected === 'None'){
                            	helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply, "0");
                            }else{
                                helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 0);
                            }
                        }
                        component.set("v.dropdownOver",false);
            
        }
        
    },
    applyGroupBy : function(component, event, helper){
        component.set("v.chosenFilter",'soryByDueDate' );

        var currentPagenumber = '1';
        var totalNumberOfPages = component.get("v.totalNumberOfPages");
        var totalRecords = component.get("v.totalRecords");
        var recordToDisply= Math.ceil(totalRecords / totalNumberOfPages);
        if(recordToDisply<=10){
            recordToDisply=10;
        }
        else if(recordToDisply<=20){
            recordToDisply=20;
        }
            else{
                recordToDisply=30;
            }
        if($A.util.isEmpty(component.get("v.groupByParam")) || component.get("v.groupByParam") === "None"){
             component.set("v.groupByNone", true);	
             helper.getAllTasksPagination(component,helper,currentPagenumber, recordToDisply,"0");
        }else{
         	helper.getAllGroupedByTasks(component,currentPagenumber, recordToDisply, 0);
        }
        helper.updateFilterBySelection(component, event);
      	
    },
    navigateToAccountRecord: function(component, event, helper) {
        //AccountId

            var rid = event.target.id;
               
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": rid,
                "slideDevName": "related"
            });
            navEvt.fire();
            
   }
   
    
})
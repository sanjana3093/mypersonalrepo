({
    
    getAllTasksPagination: function(component,helper,currentPagenumber, recordToDisply, queryType ) {
        
        var action = component.get("c.getTasksPagination");
        action.setParams({
            "queryType":queryType,
            "pageNumber": currentPagenumber,
            "recordToDisply": recordToDisply,
            "fromDate":null,
            "toDate":null
        });        
        var self = this;
        action.setCallback(this, function(a) {
            if(a.getState() ==="SUCCESS"){
                var result = a.getReturnValue();
                
                component.set("v.currentPagenumber", result.page);
                component.set("v.totalRecords", result.total);
                component.set("v.totalNumberOfPages", Math.ceil(result.total / recordToDisply));
                
                var sortedList = this.sortResults(result.activityWrapper, 'index_dueDatePlusPriority', true);
                component.set("v.lstActivity",sortedList);
                
               
            } else if (a.getState() ==="ERROR"){
                var errors = a.getError();
                console.log(JSON.stringify(errors));
                var errText = component.get("v.errorText");
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errText);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }, 
    markCompleteActivity : function (component) {
        var objTask  = component.get("v.objTask");
        var action = component.get("c.closeTask");
        action.setParams({
            "taskID" : objTask.taskID
        });
        var self = this;
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS'){
                
                var mainDiv = component.find('main-div-element');
                $A.util.addClass(mainDiv, 'slds-transition-hide');
                
                
                var taskUpdatedEvent = component.getEvent("taskUpdated");
                taskUpdatedEvent.fire();
            } else if (state ==="ERROR"){
                var errors = a.getError();
                console.log(JSON.stringify(errors));
                var errText = component.get("v.errorText");
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errText);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    
    markHighPriorityActivity : function (component) {
        var objTask  = component.get("v.objTask");
        var action = component.get("c.updatePriority");
        action.setParams({
            "taskID" : objTask.taskID
        });
        var self = this;
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS'){
                var ret = a.getReturnValue();
                if(ret.svgIconName === 'fax') {
                    ret.svgIconName = 'task';
                }
                component.set("v.objTask", ret);
                
                //Fire update event
                var taskUpdatedEvent = component.getEvent("taskUpdated");
                taskUpdatedEvent.fire();
            } else if (state ==="ERROR"){
                var errors = a.getError();
                console.log(JSON.stringify(errors));
                var errText = component.get("v.errorText");
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errText);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            
        });
        $A.enqueueAction(action);
    },    
    sortResults: function (getItems, prop, asc) {
        getItems = getItems.sort(function(a, b) {
            
            if(a[prop] < b[prop]) return -1;
            if(a[prop] > b[prop]) return 1;
            return 0;
        });
        return getItems;
    },
    
    setSelectedMenuItem : function(selectedNode) {
        
        $(".filterOptions li").removeClass("selected");
        selectedNode.parentElement.classList.add("selected");
    },
    getAllGroupedByTasks: function(component,currentPagenumber, recordToDisplay,  filterByIndex) {
        
        var action = component.get("c.getAllGroupedByTasks");
        action.setParams({
            "groupByParameter": component.get("v.groupByParam"),
            "pageNumber": currentPagenumber,
            "recordToDisplay": recordToDisplay,
            "filterByIndex": filterByIndex
        });
        var self = this;
        action.setCallback(this, function(a) {
            if(a.getState() ==="SUCCESS"){
                var result = a.getReturnValue();
                
                component.set("v.currentPagenumber", result.page);
                component.set("v.totalRecords", result.total);
                component.set("v.totalNumberOfPages", Math.ceil(result.total / recordToDisplay));
                component.set("v.groupByNone", false);
                var sortedList = this.sortGroupByResults(component, result.activityMapWrapper, 'index_dueDatePlusPriority', true);
                component.set("v.mapActivities",sortedList);
                var activities = [];
				var fetchedActivities = result.activityMapWrapper;
                var nameMap = result.namesAndIds;
                var namesList = [];
                for(var key in nameMap){
                    namesList.push({value:nameMap[key], key:key});
                }
                component.set("v.mapNames",namesList);
                for(var key in fetchedActivities){
                    var name = nameMap[key];
                    activities.push({value:fetchedActivities[key], key:key, keyLabel:name, count:fetchedActivities[key].length});
                }
                
                component.set("v.groupedActivitiesList",activities);
                
            } else if (a.getState() ==="ERROR"){
                var errors = a.getError();
                console.log(JSON.stringify(errors));
                var errText = component.get("v.errorText");
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errText);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    sortGroupByResults: function (component,getItems, prop, asc) {
        var taskOwners = [];
        for (var key in getItems) {
            taskOwners.push(key);
        getItems[key] = getItems[key].sort(function(a, b) {
            
            if(a[prop] < b[prop]) return -1;
            if(a[prop] > b[prop]) return 1;
            return 0;
        });
        }
        component.set("v.groupedActivitiesList",taskOwners);
        return getItems;
    },
     updateFilterBySelection : function (component, event){
		var action = component.get("c.getFilterByValues");
        var filterBy = component.find("filterBy");

         var opts=[];
         action.setCallback(this, function(a) {
             var response=a.getReturnValue();
             if (response !== undefined) {
                 for(var key in response){
                     opts.push({class: "optionClass", label: response[key], value:key});
                 }
             }
             filterBy.set("v.options", opts);
             
         });
         $A.enqueueAction(action);

    } 
})
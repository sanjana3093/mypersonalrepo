({	initLetters: function(component) { 
    var action = component.get("c.getLetters");
    action.setParams({"recordId": component.get("v.recordId"),
                      "invokeAction": "actioncenter",
                      "isFaxOnly": false
                     });    	
    action.setCallback(this, function(a) {
        var state = a.getState();
        if (state === 'SUCCESS'){
            var lstAvailableeLetters = a.getReturnValue();
            var opts=[];
            var eLetterChannels =[];
            if(lstAvailableeLetters.length>0 && lstAvailableeLetters[0].name === 'hasHIPAAServiceConsent'){
                component.set("v.checkHIPAAServiceConsent", true);                    
            } else {
                component.set("v.checkHIPAAServiceConsent", false);
                var inputsel = component.find("inputSelectLetter");
                for(var i=0;i< lstAvailableeLetters.length;i++){
                    var currentVal =lstAvailableeLetters[i];
                    opts.push({"class": "optionClass", label: currentVal.name, value: currentVal.value});
                    eLetterChannels.push({id: currentVal.value, faxValue: currentVal.showFaxRow , emailValue : currentVal.showEmailRow });
                }
                inputsel.set("v.options", opts);   
                component.set("v.eletterChannelMap", eLetterChannels);
            }
        } else if (state === "ERROR") {
            var errors = a.getError();
            if (errors && errors[0] && errors[0].message) {
                component.set("v.message", errors[0].message);
            } else {
                component.set("v.message", 'Unknown error');
            }
            var toggleText = component.find("msgResult");
            $A.util.toggleClass(toggleText, "toggleDisplay");
        }
        
    });
    $A.enqueueAction(action); 
},
  
  loadRecepients: function(component) { 	
      var eletterChannelMap  =  component.get("v.eletterChannelMap");
      var SelectedLetterId = component.find("inputSelectLetter").get("v.value");
      for(var key in eletterChannelMap){
          if(eletterChannelMap[key].id === SelectedLetterId){
              component.set("v.hideFaxRow", eletterChannelMap[key].faxValue); 
              component.set("v.hideEmailRow", eletterChannelMap[key].emailValue);
          }
      } 
      var action = component.get("c.getRecepients");
      action.setParams({
          "recordId": component.get("v.recordId"),
          "letterId": SelectedLetterId,
          "isFaxSpecific": false
      });
      action.setCallback(this, function(a) {
          var state = a.getState();
          var toggleText = component.find("msgResult");
          if (state === 'SUCCESS'){
              component.set("v.recepients", a.getReturnValue());
              if(a.getReturnValue() === null) {
                  component.set("v.message", 'No recepients available');
                  $A.util.toggleClass(toggleText, "msgclass");
                  $A.util.toggleClass(toggleText, "toggleDisplay");
              }
          } else if (state === "ERROR") {
              var errors = a.getError();
              if (errors && errors[0] && errors[0].message) {
                  component.set("v.message", errors[0].message);
              } else {
                  component.set("v.message", 'Unknown error');
              }
              
              $A.util.toggleClass(toggleText, "toggleDisplay");
          }
          
      });
      $A.enqueueAction(action); 
  },
  sendDocuments: function(component) { 	
      var action = component.get("c.SendMergedRecords");
      var selectedDoc=component.get("v.selectedDocumentIds");
      var recLst=component.get("v.recepients");
      var recepients=[],msg,title;
      for(var i=0;i<recLst.length;i++){
          if(recLst[i].sendEmail){
              if(recLst[i].recipientType==='SOC'){
                  recLst[i].recipientType='HCO';
              }
              recepients[i]=recLst[i].recipientType;
          }
          
      }
      if(component.get("v.selectedDocumentIds")!='' && component.get("v.selectedDocumentIds")!=undefined&&
         recepients.length>0&& component.find("inputSelectLetter").get("v.value")!='' && component.find("inputSelectLetter").get("v.value")!=undefined
         && component.find("inputSelectLetter").get("v.value")!='None'){
          action.setParams({
              'recordId' : component.get("v.recordId"),
              'documentIds': component.get("v.selectedDocumentIds"),
              'reciepients':recepients,
              'letterId':component.find("inputSelectLetter").get("v.value")
          });
          action.setCallback(this, function(a) {
              var state = a.getState();
              var toggleText = component.find("msgResult");
              if (state === 'SUCCESS'){
                  component.set("v.message", a.getReturnValue());
                  var nofaxnumber=$A.get("$Label.c.spc_InvalidEmailNumberPrefix");
                  var norecord=$A.get("$Label.unknown_custom_label");
                 
                  if(a.getReturnValue()!=null&&(a.getReturnValue().includes(nofaxnumber)||a.getReturnValue().includes(norecord))){
                      
                      $A.util.addClass(toggleText, "msgclass");
                  }else{
                      $A.util.removeClass(toggleText, "msgclass");
                  }
                  
                  $A.util.toggleClass(toggleText, "toggleDisplay");
              } else if (state === "ERROR") {
                  var errors = a.getError();
                  if (errors && errors[0] && errors[0].message) {
                      component.set("v.message", errors[0].message);
                  } else {
                      component.set("v.message", 'Unknown error');
                  }
                  $A.util.toggleClass(toggleText, "toggleDisplay");
              }
              
          });
          $A.enqueueAction(action); 
          if(component.get("v.recepients")&&component.get("v.recepients")!=undefined){
              msg=$A.get("$Label.c.spc_msg_document_sent");
              title="Recipient Notified";
          }
          var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
              title: title,
              message: msg,
              type: "success"
          });
          toastEvent.fire();
          
          component.destroy();
      }else{   
          if(component.find("inputSelectLetter").get("v.value")==''
             ||component.find("inputSelectLetter").get("v.value")=='None'
             || component.find("inputSelectLetter").get("v.value")==undefined){
              msg=$A.get("$Label.c.spc_errmsg_select_eletter");
          }
          
          else if(recepients==null||recepients=='' || recepients.lenghth>0||recepients==undefined){
              msg=$A.get("$Label.c.spc_errmsg_select_recipient")
          }
              else if(component.get("v.selectedDocumentIds")=='' || component.get("v.selectedDocumentIds")==undefined){
                  
                  msg=$A.get("$Label.c.spc_errmsg_select_document")
              }
          var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
              title: "Error",
              message: msg,
              type: "error"
          });
          toastEvent.fire();
      }
  },
  displayError: function(a) { 	
      var errors = a.getError();
      if (errors && errors[0] && errors[0].message) {
          component.set("v.message", errors[0].message);
      } else {
          component.set("v.message", 'Unknown error');
      }
  },
  setsearchFilterOptions : function(component, event, helper) {
      var action = component.get("c.getDocumentTypes");
      action.setCallback(this, function(response) {
          var state = response.getState();
          
          if (component.isValid() && state === "SUCCESS") {
              var returnValue = response.getReturnValue();
              component.set('v.searchFilterOptions', returnValue);
          } else {
              console.log("getDocumentTypes: Failed with state: " + state);
          }
      });
      $A.enqueueAction(action);
  },
  retrieveAllRelatedDocs : function(component, event, helper) {
      var action = component.get("c.searchRecords");
      action.setParams({
          'searchString' : component.get("v.recordId")
      });
      action.setCallback(this, function(response) {
          var state = response.getState();
          helper.toggleSpinner(component, 'searchResultSpinner');
          if (component.isValid() && state === "SUCCESS") {
              var returnValue = response.getReturnValue();
              component.set('v.allSearchResults', returnValue);
              if(returnValue.length > 0) {
                  $A.util.addClass(component.find('searchResultMsg'),'searchResultMsgBlockHidden');
              }
              else {
                  $A.util.removeClass(component.find('searchResultMsg'),'searchResultMsgBlockHidden');
              }
              helper.toggleSpinner(component, 'searchResultSpinner');
          } else {
              console.log("searchRecord: Failed with state: " + state);
          }
      });
      $A.enqueueAction(action);
  },
  toggleSpinner: function(cmp, spinnerAuraId) {
      var spinner = cmp.find(spinnerAuraId);
      var evt = spinner.get("e.toggle");
      
      if(!$A.util.hasClass(spinner, 'hideEl')){
          evt.setParams({ isVisible : false });
      }		
      else {
          evt.setParams({ isVisible : true });
      }
      evt.fire();
  },
  searchRecords: function(component, event, helper) {
      helper.toggleSpinner(component, 'searchResultSpinner');
      var allRelatedDocs = component.get("v.allSearchResults");
      var selectedCategory = component.find("searchFilterOptions").get("v.value");
      var resultingDocs = component.get("v.searchResults");
      resultingDocs.length = 0;
      if(selectedCategory == '--All--')
      {
          helper.toggleSpinner(component, 'searchResultSpinner');
          component.set('v.searchResults', allRelatedDocs);
          if(allRelatedDocs.length > 0) {
              $A.util.addClass(component.find('searchResultMsg'),'searchResultMsgBlockHidden');
          }else{
              $A.util.removeClass(component.find('searchResultMsg'),'searchResultMsgBlockHidden');
          }
          return;
      }
      if(allRelatedDocs.length > 0) {
          $A.util.addClass(component.find('searchResultMsg'),'searchResultMsgBlockHidden');
          for(var i=0; i < allRelatedDocs.length; i++) {
              var record = allRelatedDocs[i];
              try{
                  if(record.categoryName.includes(selectedCategory)) {
                      resultingDocs.push(record);
                  }
              }
              catch(err){}
          }
      }else{
          $A.util.removeClass(component.find('searchResultMsg'),'searchResultMsgBlockHidden');
      }
      
      
      helper.toggleSpinner(component, 'searchResultSpinner');
      component.set('v.searchResults', resultingDocs);
  }
 })
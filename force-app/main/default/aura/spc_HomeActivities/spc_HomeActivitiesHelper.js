({

    toggle: function(component, event) {

        var acc = component.find('Tasks');
        $A.util.toggleClass(acc, 'slds-active');
        acc = component.find('Alerts');
        $A.util.toggleClass(acc, 'slds-active');

        acc = component.find('showTask');
        $A.util.toggleClass(acc, 'slds-hide');
        acc = component.find('showAlert');
        $A.util.toggleClass(acc, 'slds-show');

    },
})
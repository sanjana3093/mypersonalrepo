({
	getValueFromLwc : function(component, event, helper) {
		
		var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
	},

	refreshLayout : function(component, event, helper) {
		$A.get('e.force:refreshView').fire();
		$A.get("e.force:closeQuickAction").fire();
		
	}
})
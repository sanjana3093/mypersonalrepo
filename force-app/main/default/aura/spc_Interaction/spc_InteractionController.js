({
    doInit: function(component, event, helper) {
        var params = {
            "entityApiName": "PatientConnect__PC_Interaction__c",
            "defaultFieldValues": {
                "PatientConnect__PC_Patient_Program__c": component.get("v.recordId")
            }
        };
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams(params);
        createRecordEvent.fire();
    }
})
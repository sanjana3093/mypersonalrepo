({
    changeOwner: function(component, event, helper) {
        var recordId = component.get('v.recordId');
        var action = component.get("c.updateOwnership");
        action.setParams({
            recordId: recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var updateStatus = response.getReturnValue();
                if (updateStatus === $A.get("$Label.c.spc_Not_A_Case_Manager")) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": $A.get("$Label.c.spc_Not_A_Case_Manager"),
                        "type": 'error'
                    });
                    toastEvent.fire();
                } else if (updateStatus === $A.get("$Label.c.spc_Task_updated")) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": $A.get("$Label.c.spc_Task_updated"),
                        "type": 'success',
                    });
                    toastEvent.fire();
                }
                $A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);
    }
})
({
    doInit: function(component, event, helper) {
        helper.cancelBtn(component, event, helper);
        helper.getRecordTypeId(component);
        helper.getPicklistVal(component, event);
    },
    closeModal: function(component, event, helper) {
        var showBackdrop=component.get("v.showBackdrop");
        helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
        
        if(showBackdrop){
            helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop_');
        }
        
        helper.cancelBtn(component, event, helper);
        
    },
    saveDocument: function(component, event, helper) {
        var documents = component.get("v.documents");
        documents.caseId = component.get("v.recordId")
        component.set("v.documents", documents);
        if(documents.pmrc==='' || documents.pmrc==='None') {
            component.set("v.message", $A.get("$Label.c.spc_document_create_error_msg") );
            component.set("v.showError",true);
        } else if(documents.engagementProgram==='') {
            component.set("v.message", $A.get("$Label.c.spc_engagement_program_error_msg"));
            component.set("v.showError",true);
        } else {
            helper.savedocuments(component, event, documents, helper);
        }
    },
    eletterSelected: function(component, event, helper) {
        var documents = component.get("v.documents");
        var eletters = component.get("v.eletters");
        var i;
        for (i = 0; i < eletters.length; i++) {
            if (eletters[i].name === documents.eletter) {
                documents.pmrc = eletters[i].value;
            }
        }
        component.set("v.documents", documents);
    }
})
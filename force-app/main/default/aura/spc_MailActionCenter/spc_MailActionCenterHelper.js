({
    getRecordTypeId: function(component, event, helper) {
        var action = component.get("c.getLetters");
        var casId = component.get("v.recordId");
        action.setParams({
            CaseID: casId,
            "invokeAction": "actioncenter"
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            var rtnValue = a.getReturnValue();
            if (state === 'SUCCESS') {
				var e=rtnValue.pop();
                if(e.name==='Program'){
                    component.set("v.showBackdrop",true);
                }else{
                     component.set("v.showBackdrop",false);
                }
                component.set("v.eletters", rtnValue);
                component.set("v.hasHIPAAServiceConsent", false);

            } else if (state === "ERROR") {
                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    getPicklistVal: function(component, event, helper) {
        var action = component.get("c.getPicklistEntryMap");
        var documents = component.get("v.documents");
        action.setCallback(this, function(a) {
            var state = a.getState();
            var rtnValue = a.getReturnValue();
            if (state === 'SUCCESS') {
                component.set("v.status", rtnValue.status);
                var status = rtnValue.status;
                for (var i = 0; i < status.length; i++) {
                    if (status[i].label === 'Ready for Send') {
                        documents.status = status[i].value;
                    }
                }
                component.set("v.documents", documents);
            } else if (state === "ERROR") {

                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    cancelBtn: function(component, event, helper) {
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    hidePopupHelper: function(component, componentId, className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal, className + 'hide');
        $A.util.removeClass(modal, className + 'open');

    },
    savedocuments: function(component, event, doc, helper) {
        var action = component.get("c.createDocument");
        var casId = component.get("v.recordId");
        action.setParams({
            "doc": doc
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            var rtnValue = a.getReturnValue();
            if (state === 'SUCCESS') {
                this.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
                this.hidePopupHelper(component, 'backdrop', 'slds-backdrop_');
                this.cancelBtn(component, event, helper);
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": rtnValue,
                    "slideDevName": "Detail"
                });
                navEvt.fire();
            } else if (state === "ERROR") {

                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    }
})
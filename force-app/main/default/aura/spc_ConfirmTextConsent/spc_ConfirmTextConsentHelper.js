({
  confirmTextConsent: function(component) {
    var action = component.get("c.confirmTextConsent");
    action.setParams({
      "programCaseId": component.get("v.recordId")
    });
    action.setCallback(this, function(a) {
      var state = a.getState();
      if (state === 'SUCCESS') {
        var textConsentResp = a.getReturnValue();
        if (textConsentResp !== null) {
          if (textConsentResp.isSuccess) {
            component.set("v.isConfirmTextConsentSuccess", textConsentResp.isSuccess);
            component.set("v.confirmTextConsentSuccessMsg", textConsentResp.successOrErrorMessage);
          } else {
            component.set("v.isConfirmTextConsentSuccess", textConsentResp.isSuccess);
            component.set("v.confirmTextConsentErrorMsg", textConsentResp.successOrErrorMessage);
          }
        }
      } else if (state === "ERROR") {
        var errors = a.getError();
        if (errors && errors[0] && errors[0].message) {
          component.set("v.message", errors[0].message);
        } else {
          component.set("v.message", 'Unknown error');
        }
      }
    });
    $A.enqueueAction(action);
  }
})
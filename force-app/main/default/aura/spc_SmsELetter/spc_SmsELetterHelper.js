({
    initLetters: function(component) {
        var action = component.get("c.getLetters");
        action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS') {
                var lstAvailableeLetters = a.getReturnValue();
                var opts = [];

                if (lstAvailableeLetters.length > 0 && lstAvailableeLetters[0].name === 'hasTextConsent') {
                    component.set("v.checkTextConsent", true);
                } else {
                    component.set("v.checkTextConsent", false);
                    var inputsel = component.find("inputSelectLetter");
                    for (var i = 0; i < lstAvailableeLetters.length; i++) {
                        var currentVal = lstAvailableeLetters[i];
                        opts.push({
                            "class": "optionClass",
                            label: currentVal.name,
                            value: currentVal.value
                        });
                    }
                    inputsel.set("v.options", opts);
                }
            } else if (state === "ERROR") {
                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
                var toggleText = component.find("msgResult");
                $A.util.toggleClass(toggleText, "toggleDisplay");
            }

        });
        $A.enqueueAction(action);
    },
    loadRecepients: function(component) {
        var action = component.get("c.getRecepients");
        action.setParams({
            "recordId": component.get("v.recordId"),
            "letterId": component.find("inputSelectLetter").get("v.value")
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS') {
                component.set("v.recepients", a.getReturnValue());
                if (a.getReturnValue() === null) {
                    component.set("v.message", 'No recepients available');
                    var toggleText = component.find("msgResult");
                    $A.util.toggleClass(toggleText, "toggleDisplay");
                }
            } else if (state === "ERROR") {
                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
                var toggleText = component.find("msgResult");
                $A.util.toggleClass(toggleText, "toggleDisplay");
            }

        });
        $A.enqueueAction(action);
    },
    sendDocuments: function(component) {
        var action = component.get("c.sendOutboundDocuments");
        action.setParams({
            "recipientsString": JSON.stringify(component.get("v.recepients")),
            "letterId": component.find("inputSelectLetter").get("v.value"),
            "objectId": component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS') {
                component.set("v.message", a.getReturnValue());
                var toggleText = component.find("msgResult");
                $A.util.toggleClass(toggleText, "toggleDisplay");
            } else if (state === "ERROR") {
                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
                alert(errors[0].message);
                var toggleText = component.find("msgResult");
                $A.util.toggleClass(toggleText, "toggleDisplay");
            }

        });
        $A.enqueueAction(action);
    },
    displayError: function(a) {
        var errors = a.getError();
        if (errors && errors[0] && errors[0].message) {
            component.set("v.message", errors[0].message);
        } else {
            component.set("v.message", 'Unknown error');
        }
    }
})
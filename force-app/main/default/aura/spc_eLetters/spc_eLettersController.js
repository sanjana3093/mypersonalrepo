({
    doInit: function (component, event, helper) {
        helper.initLetters(component);
    },
    onChangeSelectedLetter: function (component, event, helper) {
        component.set("v.message",'');
        var toggleText = component.find("msgResult");
        $A.util.removeClass(toggleText, "msgclass");
        $A.util.addClass(toggleText, "toggleDisplay");
        helper.loadRecepients(component);
    },
    onSend: function (component, event, helper) {
        component.set("v.message",'');
        component.set("v.message",'');
        var toggleText = component.find("msgResult");
        $A.util.removeClass(toggleText, "msgclass");
        $A.util.addClass(toggleText, "toggleDisplay");
        helper.sendDocuments(component);
    }
})
({
    getRecordTypeDetails: function(component) {
        var action = component.get("c.getRecordTypeIdForInquiry");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS') {
                component.set("v.actionPanel", false)
                var recordTypeId = a.getReturnValue();
                var params = {
                    "entityApiName": "Case",
                    "recordTypeId": recordTypeId,
                    "defaultFieldValues": {
                        "spc_DocumentID__c": component.get("v.recordId")
                    }
                }
                var createRecordEvent = $A.get("e.force:createRecord");
                createRecordEvent.setParams(params);
                createRecordEvent.fire();
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
            } else if (state === "ERROR") {
                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    }
})
({
	showErrorMsg: function(component, event, type) {

		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"type": type,
			"mode": "sticky",
			"message": component.get("v.errorMsg")
		});
		toastEvent.fire();
	},
})
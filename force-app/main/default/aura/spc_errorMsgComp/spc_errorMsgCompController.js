({
	doInit: function(component, event, helper) {
		var successMessage = component.get("v.successMessage");
		if (successMessage) {
			helper.showErrorMsg(component, event, 'success');
		} else {
			helper.showErrorMsg(component, event, 'error');
		}
	},
})
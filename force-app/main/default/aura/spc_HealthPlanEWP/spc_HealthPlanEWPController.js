/*********************************************************************************
Class Name      : spc_HealthPlanEWPController
Description     : component for health plan
Created By      : Deloitte
Created Date    : 05-Sept-18
*/

({
    showSpinner: function(component, event) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function(component, event) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, "slds-hide");
    },
    doInit: function(component, event, helper) {
        helper.setNamespace(component);
        helper.setConfiguration(component);
        var nhp = {};
        nhp.fsFields = {};
        component.set("v.newHealthPlan", nhp);
        var existingHealthPlan = component.get("v.existingHealthPlan");
        var enrollmentCase = component.get("v.enrollmentCase");
        var patientId = component.get("v.enrollmentCase.accountId");
        var lstHealthPlan = component.get("v.lstHealthPlan");

        var action = component.get("c.getFieldLabel");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.fieldWrapper", returnValue);
                var fieldWrapper = component.get("v.fieldWrapper");
                fieldWrapper.PatientConnect__PC_Health_Plan__c_RecordTypeId.lstRecordTypeOptions.sort(function(a, b) {
                    var nameA = a.label.toLowerCase(),
                        nameB = b.label.toLowerCase();
                    if (nameA > nameB) //sort string descending
                        return -1
                    if (nameA < nameB)
                        return 1
                    return 0 //default return value (no sorting)
                })

                component.set("v.fieldWrapper", fieldWrapper);
            } else {
                console.log("Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);

        helper.getServerStateHelper(component, helper);
        helper.setFieldSetObject(component, event, helper);

        if (component.get("v.enrollmentCase.isOnlineApplicant")) {
            var enrollmentCaseId = component.get("v.enrollmentCase.enrollmentId");
            helper.handleOnlineForm(component, enrollmentCaseId, helper);
        }
    },


    dontModalBox: function(component, event, helper) {
        var enterClick = event.target.id;
        if (enterClick !== 'onEnterclose') {
            var isSearchPayer = component.get("v.isSearchPayer");
            if (isSearchPayer) {
                component.set("v.addHealthPlan", true);
                component.set("v.isSearchPayer", false);
                component.set("v.isValidated", false);
                component.set("v.addHealthPlanButton", true);
            } else {
                helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
                helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
                component.set("v.isSearchPayer", false);
                component.set("v.isValidated", false);
                component.set("v.addHealthPlan", true);
                component.set("v.addHealthPlanButton", true);
                component.set("v.editHealthPlan", false);
                component.set("v.isEdit", false);

                var newHealthPlan = {};
                newHealthPlan = {
                    healthPlanId: "",
                    groupNo: "",
                    policyNo: "",
                    cardholderName: "",
                    cardholderEmployer: "",
                    cardholderRelationshipToPatient: "",
                    cardholdersBirthDate: "",
                    healthPlanEffectiveDate: "",
                    healthPlanExpirationDate: "",
                    payerRef: "",
                    payerName: "",
                    policyholderPhone: "",
                    healthPlanClaimCardNo: "",
                    healthPlanStatus: "",
                    healthPlanType: "",
                    healthPlanTypeLabel: "",
                    planRxBin: "",
                    planRxGrp: "",
                    planRxPCN: "",
                    recordName: "Unknown",
                    recordNameLabel: "Unknown",
                    source: $A.get("$Label.PatientConnect.PC_EWP_Source_ME"),
                    fsFields: new Object(),
                    isValid: true,
                    applicantSourceId: "",
                    isApplicantSelected: false
                };
                helper.setDefaultValueForFieldSetFields(component, event, helper, newHealthPlan);
                component.set("v.newHealthPlan", newHealthPlan);
            }



        }
        event.preventDefault();
        return false;
    },


    addNewPlan: function(component, event, helper) {
        component.set("v.searchString", "");
        //Modified: TEJAS PATEL; Feb-07-2017; Call method to clear error messages on New Modal Box Open
        helper.clearErrorMessage(component, ['recordName', 'healthPlanExpirationDate', 'healthPlanEffectiveDate', 'cardholdersBirthDate', 'tovalidatepayerName']);
        var newHealthPlan = {};
        newHealthPlan = {
            healthPlanId: "",
            groupNo: "",
            policyNo: "",
            cardholderName: "",
            cardholderEmployer: "",
            cardholderRelationshipToPatient: "",
            cardholdersBirthDate: "",
            healthPlanEffectiveDate: "",
            healthPlanExpirationDate: "",
            payerRef: "",
            payerName: "",
            policyholderPhone: "",
            healthPlanClaimCardNo: "",
            healthPlanStatus: "",
            healthPlanType: "",
            healthPlanTypeLabel: "",
            planRxBin: "",
            planRxGrp: "",
            planRxPCN: "",
            recordName: "Unknown",
            recordNameLabel: "Unknown",
            source: $A.get("$Label.PatientConnect.PC_EWP_Source_ME"),
            fsFields: new Object(),
            isValid: true,
            applicantSourceId: "",
            isApplicantSelected: false
        };
        helper.setDefaultValueForFieldSetFields(component, event, helper, newHealthPlan);
        component.set("v.newHealthPlan", newHealthPlan);

        helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-', helper);
        helper.showPopupHelper(component, 'backdrop', 'slds-backdrop--', helper);
    },

    addNewPlanViaApplicant: function(component, event, helper) {
        component.set("v.searchString", "");
        var onlineApplicantHealthPlans = component.get("v.onlineApplicantHealthPlans");
        var selectedTargetNode = event.currentTarget;
        var selectedTargetNodeIndex = selectedTargetNode.dataset.text;
        var selectedHealthPlan = JSON.parse(JSON.stringify(onlineApplicantHealthPlans[selectedTargetNodeIndex]));
        helper.clearErrorMessage(component, ['recordName', 'healthPlanExpirationDate', 'healthPlanEffectiveDate', 'cardholdersBirthDate', 'tovalidatepayerName']);
        var newHealthPlan = {
            healthPlanId: "",
            groupNo: selectedHealthPlan.groupNo,
            policyNo: selectedHealthPlan.policyNo,
            cardholderName: selectedHealthPlan.cardholderName,
            cardholderEmployer: selectedHealthPlan.cardholderEmployer,
            cardholderRelationshipToPatient: selectedHealthPlan.cardholderRelationshipToPatient,
            cardholdersBirthDate: selectedHealthPlan.cardholdersBirthDate,
            healthPlanEffectiveDate: selectedHealthPlan.healthPlanEffectiveDate,
            healthPlanExpirationDate: selectedHealthPlan.healthPlanExpirationDate,
            healthPlanClaimCardNo: selectedHealthPlan.healthPlanClaimCardNo,
            healthPlanStatus: selectedHealthPlan.healthPlanStatus,
            healthPlanType: selectedHealthPlan.healthPlanType,
            healthPlanTypeLabel: selectedHealthPlan.healthPlanTypeLabel,
            planRxBin: selectedHealthPlan.planRxBin,
            planRxGrp: selectedHealthPlan.planRxGrp,
            planRxPCN: selectedHealthPlan.planRxPCN,
            recordName: selectedHealthPlan.recordName,
            recordNameLabel: selectedHealthPlan.recordNameLabel,
            source: $A.get("$Label.PatientConnect.PC_EWP_Source_Online"),
            fsFields: selectedHealthPlan.fsFields,
            isValid: true,
            applicantSourceId: selectedHealthPlan.applicantSourceId,
            isApplicantSelected: false
        };

        helper.getLabelForValue(component, newHealthPlan);
        component.set("v.searchString", selectedHealthPlan.payerName);
        component.set("v.isValidated", false);
        component.set("v.newHealthPlan", newHealthPlan);
        helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-', helper);
        helper.showPopupHelper(component, 'backdrop', 'slds-backdrop--', helper);
    },

    searchRecord: function(component, event, helper) {
        var inputCmp = component.find("searchPayerName");
        inputCmp.set("v.errors", [{
            message: ""
        }]);
        var searchString = component.get("v.searchString");
        var searchStringErrorMessage = $A.get("$Label.PatientConnect.PC_FieldError_SearchString");
        if (searchString !== "" && searchString.trim().length >= 2) {
            helper.searchRecordHelper(component, searchString);
        } else {
            inputCmp.set("v.errors", [{
                message: searchStringErrorMessage
            }]);
        }
    },

    openSearch: function(component, event, helper) {
        component.set("v.isSearchPayer", true);
        component.set("v.isValidated", true);
        component.set("v.addHealthPlan", false);
        component.set("v.addHealthPlanButton", false);
        component.set("v.editHealthPlan", false);
        var newHealthPlan = component.get("v.newHealthPlan");
        newHealthPlan.policyholderPhone = "";
        component.set("v.newHealthPlan", newHealthPlan);
        var validatePayerName = component.get("v.searchString");
        helper.searchRecordHelper(component, validatePayerName);
    },
    openVeevaSearch: function(component, event, helper) {
        component.set("v.isVeevaSearch", true);
    },
    onSelect: function(component, event, helper) {
        var index = event.getSource().get("v.text");
        var lstPayers = component.get("v.lstPayers");
        lstPayers[index]['isSelected'] = "true";
        component.set("v.selectedPayer", lstPayers[index]);
        var payerName = component.get("v.selectedPayer.payerName")
        component.set("v.newHealthPlan.payerName", payerName);
        component.set("v.newHealthPlan.policyholderPhone", lstPayers[index].phone);
        var payerRef = component.get("v.selectedPayer.payerId")
        component.set("v.newHealthPlan.payerRef", payerRef);

        component.set("v.isSearchPayer", false);
        component.set("v.isValidated", true);
        component.set("v.addHealthPlan", true);

        var editPlan = component.get("v.isEdit");
        if (editPlan === true) {
            component.set("v.addHealthPlanButton", false);
            component.set("v.editHealthPlan", true);
        } else {
            component.set("v.addHealthPlanButton", true);
            component.set("v.editHealthPlan", false);
        }

    },

    editHealthPlan: function(component, event, helper) {
        var index = event.currentTarget.dataset.text; //[PC-1464] Replaced 'Remove' text with icon
        var lstHealthPlan = component.get("v.lstHealthPlan");
        var currentHealthPlan = JSON.parse(JSON.stringify(lstHealthPlan[index]));
        helper.setDefaultValueForFieldSetFields(component, event, helper, currentHealthPlan);
        component.set("v.newHealthPlan", currentHealthPlan);
        component.set("v.healthPlanIndex", index);
        helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-', helper);
        helper.showPopupHelper(component, 'backdrop', 'slds-backdrop--', helper);
        component.set("v.isSearchPayer", false);
        component.set("v.isValidated", true);
        component.set("v.addHealthPlan", true);
        component.set("v.addHealthPlanButton", false);
        component.set("v.editHealthPlan", true);
        component.set("v.isEdit", true);
    },

    removeHealthPlan: function(component, event, helper) {
        var index = event.currentTarget.dataset.text; // [PC-1464] Replaced 'Remove' text with icon
        var lstHealthPlan = component.get("v.lstHealthPlan");
        if (!$A.util.isEmpty(lstHealthPlan[index].applicantSourceId)) {
            var onlineApplicantHealthPlans = component.get("v.onlineApplicantHealthPlans");
            for (var i = 0; i < onlineApplicantHealthPlans.length; i++) {
                if (onlineApplicantHealthPlans[i].applicantSourceId === lstHealthPlan[index].applicantSourceId) {
                    onlineApplicantHealthPlans[i].isApplicantSelected = false;
                    break;
                }
            }
            component.set("v.onlineApplicantHealthPlans", onlineApplicantHealthPlans);
        }
        lstHealthPlan.splice(index, 1);
        component.set("v.healthPlanIndex", index);
        component.set("v.lstHealthPlan", lstHealthPlan);
    },

    updateHealthPlan: function(component, event, helper) {
        //Modified: TEJAS PATEL; Feb-07-2017; Caputure Error Flag; Call method to clear error messages before Re-Validation; Call Validation Methods
        component.set("v.errorCheck", true);
        helper.clearErrorMessage(component, ['recordName', 'healthPlanExpirationDate', 'planstatus', 'healthPlanType', 'healthPlanEffectiveDate', 'cardholdersBirthDate', 'tovalidatepayerName', 'planRxBin', 'planRxGrp', 'planRxPCN']);
        var index = component.get("v.healthPlanIndex");
        var newHealthPlan = component.get("v.newHealthPlan");
        var lstHealthPlan = component.get("v.lstHealthPlan");
        helper.validateRequiredFields(component, ['recordName', 'planstatus', 'healthPlanType'], false);
        newHealthPlan.isValid = true;
        helper.getLabelForValue(component, newHealthPlan);

        component.set("v.newHealthPlan", newHealthPlan);

        helper.checkValidate(component, false);
        helper.validateRequiredFields(component, ['recordName'], false);
        helper.validateExpirationDate(component, false);
        helper.validateDOB(component, false);

        newHealthPlan = component.get("v.newHealthPlan");

        if (component.get("v.errorCheck") === true) {

            lstHealthPlan[index] = newHealthPlan;
            component.set("v.lstHealthPlan", lstHealthPlan);

            helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
            helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
            component.set("v.isSearchPayer", false);
            component.set("v.isValidated", false);
            component.set("v.addHealthPlan", true);
            component.set("v.addHealthPlanButton", true);
            component.set("v.editHealthPlan", false);
            component.set("v.isEdit", false);
        } else {
            helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-', helper);
            helper.showPopupHelper(component, 'backdrop', 'slds-backdrop--', helper);
        }

        event.preventDefault();
    },

    addHealthPlan: function(component, event, helper) {
        //Modified: TEJAS PATEL; Feb-07-2017; Caputure Error Flag: Call method to clear error messages before Re-Validation; Call Validation Methods

        component.set("v.errorCheck", true);
        helper.clearErrorMessage(component, ['recordName', 'healthPlanExpirationDate', 'planstatus', 'healthPlanType', 'healthPlanEffectiveDate', 'cardholdersBirthDate', 'tovalidatepayerName', 'planRxBin', 'planRxGrp', 'planRxPCN']);

        var newHealthPlan = component.get("v.newHealthPlan");
        var lstHealthPlan = component.get("v.lstHealthPlan");
        helper.getLabelForValue(component, newHealthPlan);
        helper.checkValidate(component, false);
        helper.validateRequiredFields(component, ['recordName', 'planstatus', 'healthPlanType'], false);
        helper.validateExpirationDate(component, false);
        helper.validateDOB(component, false);
        helper.validationRx(component, false);
        if (component.get("v.errorCheck") === true) {
            if (!$A.util.isEmpty(newHealthPlan.applicantSourceId)) {
                newHealthPlan.isApplicantSelected = true;
                var onlineApplicantHealthPlans = component.get("v.onlineApplicantHealthPlans");
                for (var i = 0; i < onlineApplicantHealthPlans.length; i++) {
                    if (onlineApplicantHealthPlans[i].applicantSourceId === newHealthPlan.applicantSourceId) {
                        onlineApplicantHealthPlans[i].isApplicantSelected = true;
                        break;
                    }
                }
                component.set("v.onlineApplicantHealthPlans", onlineApplicantHealthPlans);
            }
            lstHealthPlan.push(newHealthPlan);
            component.set("v.lstHealthPlan", lstHealthPlan);

            helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
            helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
            component.set("v.isEdit", false);
            component.set("v.isSearchPayer", false);
            component.set("v.isValidated", false);
            component.set("v.addHealthPlan", true);
            component.set("v.addHealthPlanButton", true);
            component.set("v.editHealthPlan", false);
        } else {
            helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-', helper);
            helper.showPopupHelper(component, 'backdrop', 'slds-backdrop--', helper);
        }

        event.preventDefault();
    },
    numWorkersChanged: function(component, event, helper) {
        if (component.get("v.numWorkers") === 2) {
            helper.updateActionIndicators(component);
        }
    },

    cardHolderInfoChange: function(component, event, helper) {
        helper.cardHolderInfoChange(component);
    },
    OpenVeevaModal: function(component, event, helper) {
        var modalVeeva = component.find('modalVeevadialog');
        $A.util.toggleClass(modalVeeva, 'slds-fade-in-hide');
        $A.util.toggleClass(modalVeeva, 'slds-fade-in-open');
        var backdropVeeva = component.find('backdropVeeva');
        $A.util.toggleClass(backdropVeeva, 'slds-backdrop--hide');
        $A.util.toggleClass(backdropVeeva, 'slds-backdrop--open');
        var isVeevaWidgetOpened = component.get('v.isVeevaWidgetOpened');
        var newHealthPlan = component.get("v.newHealthPlan");
        newHealthPlan.policyholderPhone = "";
        component.set("v.newHealthPlan", newHealthPlan);
        if (isVeevaWidgetOpened) {
            helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-', helper);
            helper.showPopupHelper(component, 'backdrop', 'slds-backdrop--', helper);
            component.set('v.isVeevaWidgetOpened', false);
        } else {
            helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
            helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
            component.set('v.isVeevaWidgetOpened', true);
        }

    }
})
/*********************************************************************************
Class Name      : spc_SendBundledFaxController
Description     : Component created new Send Bundled Fax Pop-out
Created By      : Zeeshan S. Hussain
Created Date    :  08/09/18
 *********************************************************************************/

({
    helperMethod: function() {},
    getDocumentHeaders: function(component, event, helper) {
        var action = component.get("c.getDocFields");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set('v.DocumentTableHeaders', returnValue);
            } else {
                console.log("searchRecord: Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);

    },
    getHCOHeaders: function(component, event, helper) {
        var action = component.get("c.getHCOFields");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set('v.HCOTableHeaders', returnValue);
            } else {
                console.log("searchRecord: Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);

    },
    searchRecord: function(component, event, helper) {
        helper.toggleSpinner(component, 'searchResultSpinner');

        var allRelatedDocs = component.get("v.allSearchResults");
        var selectedCategory = component.find("searchFilterOptions").get("v.value");
        var resultingDocs = component.get("v.searchResults");
        resultingDocs.length = 0;

        if (selectedCategory === '--All--') {
            helper.toggleSpinner(component, 'searchResultSpinner');
            component.set('v.searchResults', allRelatedDocs);
            if (allRelatedDocs.length > 0) {
                $A.util.addClass(component.find('searchResultMsg'), 'searchResultMsgBlockHidden');
            } else {
                $A.util.removeClass(component.find('searchResultMsg'), 'searchResultMsgBlockHidden');
            }
            return;
        }
        if (allRelatedDocs.length > 0) {
            $A.util.addClass(component.find('searchResultMsg'), 'searchResultMsgBlockHidden');
            for (var i = 0; i < allRelatedDocs.length; i++) {
                var record = allRelatedDocs[i];
                try {
                    if (record.categoryName.includes(selectedCategory)) {
                        resultingDocs.push(record);
                    }
                } catch (err) {}
            }
        } else {
            $A.util.removeClass(component.find('searchResultMsg'), 'searchResultMsgBlockHidden');
        }

        helper.toggleSpinner(component, 'searchResultSpinner');
        component.set('v.searchResults', resultingDocs);
    },
    retrieveAllRelatedDocs: function(component, event, helper) {
        var action = component.get("c.searchRecords");
        action.setParams({
            'searchString': component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            helper.toggleSpinner(component, 'searchResultSpinner');
            if (component.isValid() && state === "SUCCESS") {

                var returnValue = response.getReturnValue();
                component.set('v.allSearchResults', returnValue);
                if (returnValue.length > 0) {
                    $A.util.addClass(component.find('searchResultMsg'), 'searchResultMsgBlockHidden');
                } else {
                    $A.util.removeClass(component.find('searchResultMsg'), 'searchResultMsgBlockHidden');
                }
                helper.toggleSpinner(component, 'searchResultSpinner');
            } else {
                console.log("searchRecord: Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    retrieveAllRelatedHCOs: function(component, event, helper) {
        var action = component.get("c.retrieveHCOs");
        action.setParams({
            'caseId': component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            helper.toggleSpinner(component, 'searchResultSpinner');
            if (component.isValid() && state === "SUCCESS") {

                var returnValue = response.getReturnValue();
                component.set('v.allRelatedHCO', returnValue);
                helper.toggleSpinner(component, 'searchResultSpinner');
            } else {
                console.log("searchRecord: Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    toggleSpinner: function(cmp, spinnerAuraId) {
        var spinner = cmp.find(spinnerAuraId);
        var evt = spinner.get("e.toggle");

        if (!$A.util.hasClass(spinner, 'hideEl')) {
            evt.setParams({
                isVisible: false
            });
        } else {
            evt.setParams({
                isVisible: true
            });
        }
        evt.fire();
    },
    setsearchFilterOptions: function(component, event, helper) {
        var action = component.get("c.getDocumentTypes");
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set('v.searchFilterOptions', returnValue);
            } else {
                console.log("getDocumentTypes: Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    SendMergedRecords: function(component, event, helper) {
        var action = component.get("c.SendMergedRecords");
        var sendToLashValue;
        var msg,
            title;
        if (component.get("v.sendToLash") === true) {
            sendToLashValue = true;
        } else {
            sendToLashValue = false; // Explicit defined if undefined
        }
        if (component.get("v.selectedDocumentIds") !== '' && component.get("v.selectedDocumentIds") !== undefined &&
            ((component.get("v.selectedHCOId") !== null && component.get("v.selectedHCOId") !== undefined) || sendToLashValue) &&
            component.find("inputSelectLetter").get("v.value") !== '' && component.find("inputSelectLetter").get("v.value") !== undefined &&
            component.find("inputSelectLetter").get("v.value") !== 'None') {

            action.setParams({
                'recordId': component.get("v.recordId"),
                'documentIds': component.get("v.selectedDocumentIds"),
                'sendToLash': sendToLashValue,
                'HCOID': component.get("v.selectedHCOId"),
                'letterId': component.find("inputSelectLetter").get("v.value")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();

                if (component.isValid() && state === "SUCCESS") {
                    var returnValue = response.getReturnValue();
                    component.set('v.searchFilterOptions', returnValue);
                } else {
                    console.log("getDocumentTypes: Failed with state: " + state);
                }
            });
            $A.enqueueAction(action);
            if (component.get("v.selectedHCOId") && sendToLashValue) {
                msg = "Document has been sent to recipient."
                title = "Recipient Notified";
            } else if (component.get("v.selectedHCOId")) {
                msg = "Document has been sent to recipient."
                title = "Recipient Notified";
            } else if (sendToLashValue) {
                msg = "Documents have been faxed to Lash."
                title = "Lash Notified.";
            }
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: title,
                message: msg,
                type: "success"
            });
            toastEvent.fire();

            component.destroy();
        } else {

            if (component.find("inputSelectLetter").get("v.value") === '' ||
                component.find("inputSelectLetter").get("v.value") === 'None' ||
                component.find("inputSelectLetter").get("v.value") === undefined) {
                msg = "Please select an eletter to send a merge fax document";
            } else if (component.get("v.selectedDocumentIds") === '' || component.get("v.selectedDocumentIds") === undefined) {
                msg = "Please select a document";
            } else if ((component.get("v.selectedHCOId") === null || component.get("v.selectedHCOId") === undefined) && component.get("v.sendToLash") === undefined) {
                msg = "Please select a recepient";
            }
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Error",
                message: msg,
                type: "error"
            });
            toastEvent.fire();
        }
    },
    initLetters: function(component) {
        var action = component.get("c.getLetters");
        action.setParams({
            "recordId": component.get("v.recordId"),
            "invokeAction": "actioncenter",
            "isFaxOnly": true
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === 'SUCCESS') {
                var lstAvailableeLetters = a.getReturnValue();
                var opts = [];
                var eLetterChannels = [];
                if (lstAvailableeLetters.length > 0 && lstAvailableeLetters[0].name === 'hasHIPAAServiceConsent') {
                    component.set("v.checkHIPAAServiceConsent", true);
                } else {
                    component.set("v.checkHIPAAServiceConsent", false);
                    var inputsel = component.find("inputSelectLetter");
                    for (var i = 0; i < lstAvailableeLetters.length; i++) {
                        var currentVal = lstAvailableeLetters[i];
                        if (currentVal.name !== $A.get("$Label.c.spc_unable_to_conatact_hcp")) {
                            opts.push({
                                "class": "optionClass",
                                label: currentVal.name,
                                value: currentVal.value
                            });
                            eLetterChannels.push({
                                id: currentVal.value,
                                faxValue: currentVal.showFaxRow,
                                emailValue: currentVal.showEmailRow
                            });
                        }

                    }
                    inputsel.set("v.options", opts);
                    component.set("v.eletterChannelMap", eLetterChannels);
                }
                $A.util.addClass(component.find('searchResultMsg'), 'searchResultMsgBlockHidden');
            } else if (state === "ERROR") {
                var errors = a.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set("v.message", errors[0].message);
                } else {
                    component.set("v.message", 'Unknown error');
                }
                var toggleText = component.find("msgResult");
                $A.util.toggleClass(toggleText, "toggleDisplay");
            }

        });
        $A.enqueueAction(action);
    }
})
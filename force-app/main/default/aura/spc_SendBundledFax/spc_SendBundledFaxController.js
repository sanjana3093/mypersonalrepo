/*********************************************************************************
Class Name      : spc_SendBundledFaxController
Description     : Component created new Send Bundled Fax Pop-out
Created By      : Zeeshan S. Hussain
Created Date    :  08/09/18
 *********************************************************************************/

({
    doInit: function(component, event, helper) {
        helper.getDocumentHeaders(component, event, helper);
        helper.getHCOHeaders(component, event, helper);
        helper.retrieveAllRelatedDocs(component, event, helper);
        helper.retrieveAllRelatedHCOs(component, event, helper);
        helper.setsearchFilterOptions(component, event, helper);
        helper.initLetters(component);
    },
    searchRecord: function(component, event, helper) {
        component.set("v.pageErrors", []);
        helper.searchRecord(component, event, helper);
    },
    MergeRecords: function(component, event, helper) {
        component.set("v.pageErrors", []);
        helper.SendMergedRecords(component, event, helper);
    },
    onAdd: function(component, event, helper) {
        var currentNode = event.currentTarget;

        var index = currentNode.getAttribute("data-text");
        var id = currentNode.getAttribute("data-name");

        var searchResults = component.get("v.searchResults");

        if ((component.get("v.selectedDocumentIds")).indexOf(id) === -1) {
            component.get("v.selectedDocumentIds").push(id);
            component.set("v.enablFax", false);

            searchResults[index]['isSelected'] = "true";
            var updatedSelectedResult = component.get("v.selectedResults");
            updatedSelectedResult.push(searchResults[index]);
            component.set("v.selectedResults", updatedSelectedResult);
        } else {
            searchResults[index]['isSelected'] = "true";
        }

        component.set("v.searchResults", searchResults);
    },
    selectRecords: function(component, event, helper) {},
    onRemove: function(component, event, helper) {
        var currentNode = event.currentTarget;

        var index = currentNode.getAttribute("data-text");
        var id = currentNode.getAttribute("data-name");

        var selectedDocumentIds = component.get("v.selectedDocumentIds");

        if (selectedDocumentIds.indexOf(id) !== -1) {

            selectedDocumentIds.splice(selectedDocumentIds.indexOf(id), 1);
            component.set("v.selectedDocumentIds", selectedDocumentIds);
            var updatedSelectedResults = component.get("v.selectedResults");
            for (var i = 0; i < updatedSelectedResults.length; i++) {
                if (updatedSelectedResults[i].Id === id) {
                    updatedSelectedResults.splice(i, 1);
                    break;
                }
            }

            var updatedSearchResults = component.get("v.searchResults");
            for (var i = 0; i < updatedSearchResults.length; i++) {
                if (updatedSearchResults[i].Id === id) {
                    updatedSearchResults[i]['isSelected'] = "false";
                    break;
                }
            }

            component.set("v.selectedResults", updatedSelectedResults);
            component.set("v.searchResults", updatedSearchResults);
        }
        if (selectedDocumentIds.length < 1) {
            component.set("v.enablFax", true);
        }

    },
    onCheck: function(component, event, helper) {
        var checkCmp = component.find("sendToLash");
        component.set("v.sendToLash", checkCmp.get("v.value"));
    },
    onSelect: function(component, event, helper) {
        var index = event.getSource().get("v.text");
        var searchResult = component.get("v.allRelatedHCO");
        searchResult[index]['isSelected'] = "true";

        var updatedSelectedResult = component.get("v.selectedHCOId");
        updatedSelectedResult = searchResult[index].Id;
        component.set("v.selectedHCOId", updatedSelectedResult);
    }

})
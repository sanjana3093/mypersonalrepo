import { LightningElement,track,api ,wire } from 'lwc';
import getStepslist from '@salesforce/apex/CCL_PRF_Controller.getPRFSteps';
import { registerListener } from "c/cCL_Pubsub";
import { CurrentPageReference } from "lightning/navigation";

import CCL_Patient_Details from "@salesforce/label/c.CCL_Patient_Details";
import CCL_Payment_Details from "@salesforce/label/c.CCL_Payment_Details";
import CCL_Pick_Up_Delivery from "@salesforce/label/c.CCL_Pick_Up_Delivery";
import CCL_Review_Order_Request from "@salesforce/label/c.CCL_Review_Order_Request";
import CCL_Treatment_Information from "@salesforce/label/c.CCL_Treatment_Information";

export default class CCLCallPRFSteps extends LightningElement {
    @api stepName=[];
    @track error;
    @track gotresult=false;
    @track message;
    @track order;
  @wire(CurrentPageReference) pageRef;

  stepsLabels={
    CCL_Patient_Details,
    CCL_Payment_Details,
    CCL_Pick_Up_Delivery,
    CCL_Review_Order_Request,
    CCL_Treatment_Information
  };

  connectedCallback() {
    registerListener("therapytype1", this.handleMessage, this);
    registerListener("orderConfirmation", this.handleOrderConf,this);
   }

  handleMessage(myMessage) {
    this.message = myMessage;
  }

  handleOrderConf(orderMsg) {
    this.order = orderMsg;
    if(this.order === 'orderConfirmation'){
      this.gotresult=false;
    }
  }

    @wire(getStepslist, {therpyType:'$message'})
    wiredSteps({ error, data }) {

        if (data) {
          let temp={MasterLabel:'',CCL_Order_of_Step__c:''};
           
            let self=this;
            data.forEach(function(node){
              temp={MasterLabel:'',CCL_Order_of_Step__c:''};
              temp.MasterLabel=self.stepsLabels[node.MasterLabel];
              temp.CCL_Order_of_Step__c=node.CCL_Order_of_Step__c;
              self.stepName.push(temp);
            });
            this.error = null;
            this.gotresult=true;
        } else if (error) {
            this.error = error;
            this.stepName = null;
        }
    }
}
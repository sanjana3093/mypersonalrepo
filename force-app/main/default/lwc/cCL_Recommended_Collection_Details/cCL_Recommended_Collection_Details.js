import { LightningElement,api,track,wire } from 'lwc';
import {refreshApex} from '@salesforce/apex';
import additionalComments from '@salesforce/label/c.CCL_Additional_Comments';
import collectionName from '@salesforce/label/c.CCL_Collection_Blood_Testing';
import relatedDocuments from '@salesforce/label/c.CCL_Related_Documents';
import collectionandCryopreservationComments from '@salesforce/label/c.CCL_Apheresis_Collection_and_Cryopreservation_Commnets';
import getRecommendedCollectionList from "@salesforce/apex/CCL_ADF_Controller.getRecommendedCollectionDetails";
import saveAdfData from "@salesforce/apex/CCL_ADF_Controller.saveAdfData";
import CCL_Modal_Close from '@salesforce/label/c.CCL_Modal_Close';
import CCL_Collection_Details_Complete from '@salesforce/label/c.CCL_Collection_Details_Complete';
import CCL_Collection_Details_Complete_Navigation from '@salesforce/label/c.CCL_Collection_Details_Complete_Navigation';
import CCL_Transfer_to_Cell_Lab from '@salesforce/label/c.CCL_Transfer_to_Cell_Lab';
import CCL_Enter_Cryopreservation_Details from '@salesforce/label/c.CCL_Enter_Cryopreservation_Details';
import getUserName from "@salesforce/apex/CCL_ADF_Controller.getUserName";
import getUserTimeZoneDetails from "@salesforce/apex/CCL_ADF_Controller.getUserTimeZoneDetails";
import getUserTimeFormatted from "@salesforce/apex/CCL_ADF_Controller.getUserTimeInFormat";
import setUserCollectionDetails from "@salesforce/apex/CCL_ADF_Controller.saveTransferToCellLabDetails";
import getAdfPageDetails from "@salesforce/apex/CCL_ADFController_Utility.getADFSummaryPageInfo";
import getFileDetails from "@salesforce/apex/CCL_ADFController_Utility.getUploadedFileDetails";
import getcustomdoc from "@salesforce/apex/CCL_ADFController_Utility.getDocId";
import completedDateText from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_TEXT_Collection_Details_Date_Time__c";
import CCL_Document_Upload_Msg from "@salesforce/label/c.CCL_Document_Upload_Msg";
import {
  FlowNavigationNextEvent,
  FlowAttributeChangeEvent
} from "lightning/flowSupport";
import recmColDetails from "@salesforce/label/c.CCL_Recomended_Collection_Details";
//2260
import { registerListener, fireEvent } from "c/cCL_Pubsub";
import { CurrentPageReference,NavigationMixin } from "lightning/navigation";
//Required for CGTU 227
import saveForLater from "@salesforce/apex/CCL_ADF_Controller.saveForLater";
import checkUserPermission from "@salesforce/apex/CCL_ADFController_Utility.checkUserPermission";
import deleteFile from "@salesforce/apex/CCL_ADFController_Utility.deleteFile";
import CCL_DeleteConfirmation_Title from "@salesforce/label/c.CCL_DeleteConfirmation_Title";
import CCL_DeleteConfirmation_Message from "@salesforce/label/c.CCL_DeleteConfirmation_Message";
import CCL_DeleteConfirmation_Ok from "@salesforce/label/c.CCL_DeleteConfirmation_Ok";
import CCL_DeleteConfirmation_Cancel from "@salesforce/label/c.CCL_DeleteConfirmation_Cancel";
import getTimezoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getTimeZoneDetails";
import getFormattedDateTimeDetails from "@salesforce/apex/CCL_ADFController_Utility.getFormattedDateTimeDetails";
import getTimezoneIDDetails from "@salesforce/apex/CCL_ADFController_Utility.getTimeZoneIDDetails";
//imported as part of 850
import { getRecord,updateRecord } from "lightning/uiRecordApi";
import reasonForModification from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Reason_For_Modification__c";
import CCL_Approval_Counter__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Approval_Counter__c";
import ID from "@salesforce/schema/CCL_Apheresis_Data_Form__c.Id";
import CCL_Reason_Modal_Header from "@salesforce/label/c.CCL_Reason_Modal_Header";
import CCL_Reason_Modal_Message from "@salesforce/label/c.CCL_Reason_Modal_Message";
import CCL_Reason_Modal_Helptext from "@salesforce/label/c.CCL_Reason_Modal_Helptext";
import CCL_Reason_Modal_Subheading from "@salesforce/label/c.CCL_Reason_Modal_Subheading";
import CCL_ADF_Submitter_Permission from "@salesforce/label/c.CCL_ADF_Submitter_Permission";
import CCL_PRF_Resubmission_Approval from "@salesforce/label/c.CCL_PRF_Resubmission_Approval";
import getOrderApprovalInfo from "@salesforce/apex/CCL_ADFController_Utility.getOrderApprovalInfo";
//translation labels
import CCL_Rec_Comm_err_msg from "@salesforce/label/c.CCL_Rec_Comm_err_msg";
import CCL_Rec_Comm_err_msg_3_digits from "@salesforce/label/c.CCL_Rec_Comm_err_msg_3_digits";
import CCL_Rec_Comm_err_msg_6_digits from "@salesforce/label/c.CCL_Rec_Comm_err_msg_6_digits";
import CCL_Rec_Comm_err_msg_max_digits from "@salesforce/label/c.CCL_Rec_Comm_err_msg_max_digits";
import CCL_Flow_Cytometry from "@salesforce/label/c.CCL_Flow_Cytometry";
import CCL_Absolute_Counts from "@salesforce/label/c.CCL_Absolute_Counts";
import CCL_255_Char_Limit from "@salesforce/label/c.CCL_255_Char_Limit";
import CCL_Delete from "@salesforce/label/c.CCL_Delete";
import CCL_Submit from "@salesforce/label/c.CCL_Submit";
import CCL3_Approved from "@salesforce/label/c.CCL3_Approved";
import CCL_Back from "@salesforce/label/c.CCL_Back";
import CCL_Save_Changes from "@salesforce/label/c.CCL_Save_Changes";
import CCL_Collection_label from "@salesforce/label/c.CCL_Collection_label";
import CCL_Complete_Blood_Count_CBC_with_Differential from "@salesforce/label/c.CCL_Complete_Blood_Count_CBC_with_Differential";
import CCL_Date_Of_Testing from "@salesforce/label/c.CCL_Date_Of_Testing";
import CCL_Time from "@salesforce/label/c.CCL_Time";
import CCL_Open_Confirmation_Msg from "@salesforce/label/c.CCL_Open_Confirmation_Msg";
import CCL_TimeZone from "@salesforce/label/c.CCL_TimeZone";
import CCL_Cancel_Button from "@salesforce/label/c.CCL_Cancel_Button";
export default class CCL_Recommended_Collection_Details extends NavigationMixin(
  LightningElement
) {
  label = {
    additionalComments,
    collectionandCryopreservationComments,
    relatedDocuments,
    CCL_Modal_Close,
    CCL_Collection_Details_Complete,
    CCL_Collection_Details_Complete_Navigation,
    CCL_Transfer_to_Cell_Lab,
    recmColDetails,
    CCL_Enter_Cryopreservation_Details,
    CCL_DeleteConfirmation_Title,
    CCL_DeleteConfirmation_Message,
    CCL_DeleteConfirmation_Ok,
    CCL_DeleteConfirmation_Cancel,
    CCL_Reason_Modal_Header,
    CCL_Reason_Modal_Message,
    CCL_Reason_Modal_Helptext,
	CCL_PRF_Resubmission_Approval,
    CCL_Reason_Modal_Subheading,
CCL_Document_Upload_Msg,
    CCL_Save_Changes,
    CCL_Back,
    CCL3_Approved,
    CCL_Submit,
    CCL_Delete,
    CCL_255_Char_Limit,
    CCL_Collection_label
  };
    @api tableData;
    @track multiple=true;
    @track collectionName=collectionName;
    @track sobj;
    @api saveClicked=false;
    @api recordId;
    @track configArr;
    @api apiNames=[];
	@track activesectionName;
 	@api myTimeObj = {
    CCL_TEXT_CBC_Differential_Date_Time__c:"",
    CCL_TEXT_Absolute_Counts_Date_Time__c:"",
    CCL_TEXT_Flow_Cytometry_Date_Time__c:""
  };
  ///translation changes
  sectionLabels={CCL_Flow_Cytometry,CCL_Absolute_Counts,CCL_Complete_Blood_Count_CBC_with_Differential};
  errLabels={CCL_Rec_Comm_err_msg,CCL_Rec_Comm_err_msg_3_digits,CCL_Rec_Comm_err_msg_6_digits,CCL_Rec_Comm_err_msg_max_digits};
  @api myDateObj = {
    CCL_TEXT_CBC_Differential_Date_Time__c:"",
    CCL_TEXT_Absolute_Counts_Date_Time__c:"",
    CCL_TEXT_Flow_Cytometry_Date_Time__c:""
  };
    @api myActualArr = [];
    @api configVal;
	@api storedValues;
    @track adfData = {
      Id: "",
      CCL_Collection_Cryopreservation_Comments__c: "",
    };
    @api userName;
    @api userTimeZone;
    @api timeZoneFormatted;
	@api timeZone={};
    @track isModalOpen = false;
    @api timeFormat='EEEE dd MMMMM yyyy\' \'HH:mm z';
    @api collectionInput={};
    @track count=0;
   // @track numberOfCollections=0;
    @api saveLaterClicked=false;
    @api previousClicked=false;
    @track navigateFurther;
    @api validationInput={};



    // Changes as part of 227 and 259
    @api screenName;
    @api flowScreen;
    @api jsonData;
    @api commentValue; // used to pass the value to html
    @api jsonObj = {}; // this will contain the entire json structure of all the pages if any data saved
    @api myJson={};
    @api comments; //Used to retrieve value from html.
    // Changes end here
	
	//added as part of 850
  @track isReasonModalOpen=false;
  @track ReasonForMod;
  @track reason;
  @track navigate;
  @track counter;
  @track orignalJson;
  @track orignalFileresult;
  @track commentOrFileChange=false;
 @api timeZoneIDVal;
 @track newPermissionName="CCL_ADF_Submitter";
 @track isAdfSubmitterUser=false;

  @wire(getRecord, { recordId: "$recordId", fields:[reasonForModification,CCL_Approval_Counter__c] })
  Reason({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.reason = data.fields.CCL_Reason_For_Modification__c.value;
      this.counter=data.fields.CCL_Approval_Counter__c.value;
      if(this.counter===null){
        this.counter=0;
      }
    }
  };
  //changes for 850 end here
	
    // Added to check if the Modal is being displayed only once
    @api hasModalClicked=false;
    @api pageDetails;
  @track filesUploaded=[];
  @track url;
  @track showUploadedFiles=false;
  @wire(CurrentPageReference) pageRef;
  wiredFileResult;
    //added as part of us 355
  @track isDialogVisible = false;
  @track originalMessage;
  @track displayMessage = CCL_Open_Confirmation_Msg ;
  @track fileUrl;
  @track idPosition;
  @track docVersionId;
  @track SubmitterPermission=CCL_ADF_Submitter_Permission;
  @track isAdfSubmitter=false;
  @track renderDelete=false;
  @track wiredDeleteFile;
  @track screennameVal="Recommended Collection Details";
  @track textDateTimeVal;
  //ends here
  @api isADFViewerInternal;
  @track adfDocId;
  @track disableFileUpload=false;
  @api isCustomerOp;
  // Changes end here
  connectedCallback() {
    if(this.isADFViewerInternal||this.isCustomerOp){
      this.disableFileUpload=true;
    }
      let val = this.jsonData!=undefined?JSON.parse(this.jsonData):'';
	   //2260
    registerListener("gotoSummary", this.gotoADFSummary, this);
	this.orignalJson=val;
    let mytable = val["2"];
    let storedValue = val["3"];
    this.getPageDetails();
    // Check to verify if the modal is displayed only once, Changes start
    let nextScreenval = val["4"];
    if (nextScreenval) {
      this.hasModalClicked = true;
    }
    this.getTimeZoneDetailsVal();
    this.getTimeZoneIDScreenDetails();
    // Changes End
    if (storedValue) {
      if (storedValue.collections !== undefined) {
        storedValue = storedValue.collections;
        if (Object.keys(storedValue["1"]).length > 0) {
          this.loadOldval = true;
          this.storedValues = storedValue;
        }
      }
    }
    this.tableData = mytable[1];
   if(mytable[1][0]["CCL_DIN__c"]===null){
    this.activesectionName = CCL_Collection_label+" " + "1" + " - " + collectionName + " " + " Apheresis ID : " + mytable[1][0]["CCL_Apheresis_ID__c"];}
    else{
      this.activesectionName = CCL_Collection_label+" " + "1" + " - " + collectionName + " "  + " DIN : " + mytable[1][0]["CCL_DIN__c"];
    }
    this.tableData.forEach(function (node, index) {
      if(mytable[1][index]["CCL_DIN__c"]===null){
      node.attributes.url = CCL_Collection_label+" " + (index + 1) + " - " + collectionName + " " + " Apheresis ID : " + mytable[1][index]["CCL_Apheresis_ID__c"];}
      else{
        node.attributes.url = CCL_Collection_label+" " + (index + 1) + " - " + collectionName + " " + " DIN : " + mytable[1][index]["CCL_DIN__c"];
      }
    });


			//Changes as part of CGTU 259
			if (this.jsonData != undefined || this.jsonData != null) {
			  this.jsonObj = JSON.parse(this.jsonData);
			  this.commentValue = this.jsonObj[
				"Collection and Cryopreservation Comments"
			  ];
    }
    //Changes end here
	//Changes as part of 355


    checkUserPermission({
      permissionName: this.SubmitterPermission
    })
      .then((result) => {
        //result=true;
        this.isAdfSubmitter=result;
        this.renderDelete=result;
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
      checkUserPermission({
        permissionName: this.newPermissionName
      })
        .then((result) => {
          if(result){
            this.isAdfSubmitterUser=result;
            if(this.isAdfSubmitterUser){
              this.isAdfSubmitter=result;
              this.renderDelete=result;
            }
          }
          this.error = null;
        })
        .catch((error) => {
          this.error = error;
        });
    //Changes as part of 355 ends here
  }

  getTimeZoneDetailsVal(){
    getTimezoneDetails({
      screenName:this.screennameVal, adfId: this.recordId
    })
      .then((result) => {
        this.timeZone = result;
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }

  getTimeZoneIDScreenDetails(){
    getTimezoneIDDetails({screenName: this.screennameVal, adfId: this.recordId})
      .then((result) => {
        this.timeZoneIDVal = result;
        this.getUserTimeZoneDetailsVal();
          this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }

	 @wire(getcustomdoc, { recordId: "$recordId" })
  wiredDocId({ error, data }) {
    if (data) {
      this.adfDocId = data[0].Id;
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }


  @wire(getFileDetails, { recordId: "$adfDocId" })
  wiredFileListSteps({ error, data }) {
    if (data) {
      this.filesUploaded = data;
      if (this.filesUploaded.length > 0) {
        this.showUploadedFiles = true;
      }
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }


@wire(getOrderApprovalInfo, { recordId: "$recordId" })
  wiredAssocidatedOrderApprovalDetails({ error, data }) {
    if (data) {
      this.prfApprovalRequired =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_Order_Approval_Eligibility__c:this.prfApprovalRequired;
      this.prfResubmission =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Approval_Counter__c:this.prfResubmission;
      this.prfStatus =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Ordering_Status__c:this.prfStatus;
      if(this.prfApprovalRequired && this.prfResubmission >='1' && this.prfStatus!='PRF_Approved') {
        this.prfWarningMessage = true;
      }
    } else if (error) {
      this.error = error;
    }
  }
@wire(getTimezoneDetails, {screenName:"$screennameVal", adfId: "$recordId" })
wiredTimeZoneSteps({ error, data }) {
    if (data) {
        this.timeZone = data;
      this.error = null;
    } else if (error) {
        this.error = error;
    }
}
@wire(getFileDetails, { recordId: "$adfDocId" })
  imperativeFileWiring(result) {
    this.wiredFileResult = result;
    if (result.data) {
      this.filesUploaded = result.data;
      if (this.filesUploaded.length > 0) {
        this.showUploadedFiles = true;
      }
    }
  }

  //added as part of 355
  @wire(getFileDetails, { recordId: "$adfDocId" })
  imperativeFileDeletion(result) {
    this.wiredDeleteFile = result;
    if (result.data) {
      this.filesUploaded = result.data;
      if (this.filesUploaded.length > 0) {
        this.showUploadedFiles = true;
      }
    }
  }
  get acceptedFormats() {
    return [".docx", ".txt", ".xls", ".csv", ".ppt", ".jpg", ".jpeg", ".png",".pdf"];
  }


handleDeleteFinished() {
	this.orignalFileresult=JSON.stringify(this.wiredFileResult);
  return refreshApex(this.wiredDeleteFile);
      }
//355 changes ends here

viewDocument(event) {
  this.isFileViewModalOpen=true;
  this.url='/sfc/servlet.shepherd/document/download/'+event.target.name;
  window.open(this.url)
        }

handleUploadFinished(event) {
	this.orignalFileresult=JSON.stringify(this.wiredFileResult);
   
  return refreshApex(this.wiredFileResult);
      }

  getPageDetails() {
    getAdfPageDetails({
      recordId: this.recordId
    })
      .then((result) => {
        let completedById='';
        this.pageDetails = result;
        if(this.pageDetails[0].CCL_Collection_Details_Completed_By__r!==undefined){
        completedById=this.pageDetails[0].CCL_Collection_Details_Completed_By__r.Id;
        }
        if(completedById!==undefined && completedById!==''){
          this.hasModalClicked=true;
        }
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }

        @wire(getRecommendedCollectionList)
        wiredSteps({ error, data }) {
          if (data) {
            this.sobj = data[0].CCL_Object_API_Name__c;
            this.configVal = data;
            let mySectioned;
            mySectioned = this.getSections(this.configVal);
            this.configArr= this.getFields(data, mySectioned);
            this.error = null;
            this.gotresult = true;
          } else if (error) {
            this.error = error;
          }
        }
        @wire(getUserName)
        wiredStepUserName({ error, data }) {
          if (data) {
            this.userName = data;
            this.error = null;
          } else if (error) {
            this.error = error;
          }
        }

  getUserTimeZoneDetailsVal(){
    getUserTimeZoneDetails({
      timeZoneVal:this.timeZoneIDVal[completedDateText.fieldApiName]
    })
      .then((result) => {
        this.userTimeZone = result;
        if(this.userTimeZone!==undefined && this.userTimeZone!==null && this.userTimeZone!==''){
          let fields=this.userTimeZone.split('T');
          let timeVal=fields[1];
          let dateVal=fields[0];
          let timeValue=timeVal.split(":");
          let textVal=this.formatDate(dateVal)+' '+timeValue[0]+':'+timeValue[1]+' '+this.timeZone[completedDateText.fieldApiName];
          this.textDateTimeVal=textVal;
        }
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }
  @wire(getUserTimeFormatted, {
    dateTimeVal: "$userTimeZone",
    format: "$timeFormat"
  })
  wiredStepUserTimeFormatted({ error, data }) {
    if (data) {
      this.timeZoneFormatted = data;
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }
  getSections(myArr) {
    let configArr = [];
    let sectionnames = [];
     //translation changes
    let self=this;
    let myObj = { sectioname: "", myFields: [] };
    myArr.forEach(function (node) {
      sectionnames.push(self.sectionLabels[node.CCL_Section_Name__c]);
    });
    let uniq = [...new Set(sectionnames)];
    uniq.forEach(function (node) {
      myObj = { sectioname: "", myFields: [] };
      myObj.sectioname = node;
      configArr.push(myObj);
    });
    return configArr;
  }
  getFields(mainArrr, configArr) {
    let i, j;
    let self = this;
    for (i = 0; i < mainArrr.length; i++) {
      for (j = 0; j < configArr.length; j++) {
        if (configArr[j].sectioname === self.sectionLabels[mainArrr[i].CCL_Section_Name__c]) {
          self.setFields(configArr, mainArrr, i, j);
        }
      }
    }
    //this.myDateObj='{'+this.myDateObj+'}';
    //this.myDateObj=JSON.parse(JSON.stringify(this.myDateObj));
    configArr.forEach(function (node) {
      node.myFields = self.sortData("fieldorder", "asc", node.myFields);
    });
    this.myActualArr = configArr;
   }
  sortData(fieldName, sortDirection, mydata) {
    let data = JSON.parse(JSON.stringify(mydata));
    let key = (a) => a[fieldName];
    let reverse = sortDirection === "asc" ? 1 : -1;
    data.sort((a, b) => {
      let valueA = key(a) ? key(a) : "";
      let valueB = key(b) ? key(b) : "";
      return reverse * ((valueA > valueB) - (valueB > valueA));
    });
    return data;
  }
  setFields(configArr, mainArrr, i, j) {
    let fieldArr = {
      Fieldlabel: "",
      fieldApiName: "",
      fieldtype: "",
      fieldorder: "",
      fieldValue: "",
      fieldhelptext: "",
      placeHolder: "",
      showStep: "",
      max: "",
      rangeOverflow: "",
      isDate: "",
      class: "",
      isTime: "",
      timeZone:"",
	  stepMismatch:"",
	  min:"",
      rangeUnderflow:""
    };
    fieldArr.Fieldlabel = mainArrr[i].MasterLabel;
    fieldArr.fieldApiName = mainArrr[i].CCL_Field_Api_Name__c;
    fieldArr.fieldtype = mainArrr[i].CCL_Field_Type__c;
    fieldArr.class = "myClass";
    fieldArr.fieldorder = mainArrr[i].CCL_Order_of_Field__c;
    fieldArr.fieldhelptext = mainArrr[i].CCL_Help_Text__c;
    fieldArr.placeHolder = mainArrr[i].CCL_Place_Holder__c;
    if (fieldArr.fieldtype == "Date" || fieldArr.fieldtype == "date") {
      fieldArr.isDate = true;
      fieldArr.Fieldlabel = CCL_Date_Of_Testing;
      fieldArr.class = "myClass dateClass";
      fieldArr.placeHolder='';
    }
    if (fieldArr.fieldtype == "Time" || fieldArr.fieldtype == "time") {
      fieldArr.isTime = true;
      fieldArr.Fieldlabel = CCL_Time;
      fieldArr.class = "myClass timeClass";
      fieldArr.placeHolder='';
      if(this.timeZone!==undefined && this.timeZone[fieldArr.fieldApiName]!==undefined){
        fieldArr.timeZone=this.timeZone[fieldArr.fieldApiName];}
    }
    fieldArr.showStep = mainArrr[i].CCL_Show_Step__c;
    fieldArr.max = mainArrr[i].CCL_Max_Allowed__c;
    //changes for translation
    fieldArr.rangeOverflow =this.errLabels[mainArrr[i].CCL_When_Range_overflow__c] ;
    fieldArr.stepMisMatch =this.errLabels[mainArrr[i].CCL_When_Step_Mismatch__c] ; 
    fieldArr.min = mainArrr[i].CCL_Min_Value__c;
    fieldArr.rangeUnderflow =this.errLabels[mainArrr[i].CCL_When_Range_Underflow__c];
    this.apiNames.push(fieldArr.fieldApiName);
    configArr[j].myFields.push(fieldArr);
  }
 onNext() {
    //2260
    if (!this.isADFViewerInternal) {
      this.saveComments();
    }

    if (
      (this.orignalFileresult !== undefined
        ? this.orignalFileresult != JSON.stringify(this.wiredFileResult)
        : false) ||
      (this.comments != undefined
        ? this.orignalJson["Collection and Cryopreservation Comments"] !==
          this.comments
        : false)
    ) {
      this.commentOrFileChange = true;
    }
	 this.saveClicked = true;
    // Added to make the Modal only display once on a flow, Changes start
    /*  if(this.hasModalClicked){
                this.goToNextScreen()}
                else{
                this.isModalOpen = true;}*/
              // Changes End
          }
          onPrevious() {
            // previous code here
			this.saveLaterClicked = true;
            this.previousClicked = true;
          }
          //Added as part of CGTU 227
          onSave(){
            this.saveLaterClicked=true;
          }
		  saveClickedFalse(){
			this.commentOrFileChange=false;
    this.saveClicked=false;
    this.saveLaterClicked=false;
    this.previousClicked=false;
  }
          saveAsJson() {
            let commentBox=this.template.querySelector("lightning-textarea");
            this.comments= commentBox.value;
             if (this.jsonData == undefined || this.jsonData == null) {
      this.jsonObj[this.flowScreen] = this.myJson;
      this.jsonObj["Collection and Cryopreservation Comments"] = this.comments;
      if(this.isModalOpen){
        this.jsonObj["latestScreen"] = '4';
      }else{
        this.jsonObj["latestScreen"] = this.flowScreen;
      }
     
    } else {
      if (this.flowScreen in this.jsonObj) {
        delete this.jsonObj[this.flowScreen];
        this.jsonObj[this.flowScreen] = this.myJson;
        this.jsonObj[
          "Collection and Cryopreservation Comments"
        ] = this.comments;
        if(this.isModalOpen){
          this.jsonObj["latestScreen"] = '4';
        }else{
          this.jsonObj["latestScreen"] = this.flowScreen;
        }
      } else {
        this.jsonObj[this.flowScreen] = this.myJson;
        this.jsonObj[
          "Collection and Cryopreservation Comments"
        ] = this.comments;
        if(this.isModalOpen){
          this.jsonObj["latestScreen"] = '4';
        }else{
          this.jsonObj["latestScreen"] = this.flowScreen;
        }
      }
    }
			                //updating the save json varible
          const attributeChangeEvent = new FlowAttributeChangeEvent(
                  "jsonData",
                  JSON.stringify(this.jsonObj)
                );
                this.dispatchEvent(attributeChangeEvent);
            //2260
    if (!this.isADFViewerInternal) {

      saveForLater({
        myJSON: JSON.stringify(this.jsonObj),
        recordId: this.recordId
      })
        .then((result) => {
          this.error = null;
          if (
            this.count === this.tableData.length &&
            this.saveLaterClicked &&
            !this.previousClicked
          ) {
            this.goToListScreen();
          }
          if (
            this.count === this.tableData.length &&
            this.saveLaterClicked &&
            this.previousClicked
          ) {
            const attributeChangeEvent = new FlowAttributeChangeEvent(
              "screenName",
              "2"
            );
            this.dispatchEvent(attributeChangeEvent);
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
          }
          if (this.saveClicked && this.counter > 0) {
            this.goToNextScreen();
          }
        })
        .catch((error) => {
          this.error = error;
        });
    }else{
      if (
        this.count === this.tableData.length &&
        this.saveLaterClicked &&
        !this.previousClicked
      ) {
        this.goToListScreen();
      }
      if (
        this.count === this.tableData.length &&
        this.saveLaterClicked &&
        this.previousClicked
      ) {
        const attributeChangeEvent = new FlowAttributeChangeEvent(
          "screenName",
          "2"
        );
        this.dispatchEvent(attributeChangeEvent);
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
      }
      if (this.saveClicked && this.counter > 0) {
        this.goToNextScreen();
      }
    }
          }
          goToListScreen(){
            this[NavigationMixin.Navigate]({
			  type: "comm__namedPage",
			  attributes: {
				name: "Home"
			  },
			  state: {
				"tabset-8e13c": "4d03f"
			  }
			});
            }
          // Changes as part of 227 end here.



          handleCommentsChange(event) {
            this.comments=event.target.value;
            this.adfData.CCL_Collection_Cryopreservation_Comments__c =
              event.target.value;
          }
          saveComments(){
            this.adfData.Id = this.recordId;
            saveAdfData({ adfData: this.adfData })
            .then((result) => {
            })
            .catch((error) => {
              this.message = "";
              this.error = error;
            });
          }
          openModal() {
            this.isModalOpen = true;
        }
        closeModal() {
			this.count=0;
          this.saveClicked=false;
          this.saveLaterClicked=false;
          this.previousClicked=false;
            this.isModalOpen = false;
        }

  formatDate(date) {
    let mydate = new Date(date);
    let monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];
  
    let day = mydate.getDate();
    let monthIndex = mydate.getMonth();
    let monthName = monthNames[monthIndex];
    let year = mydate.getFullYear();
    return `${day} ${monthName} ${year}`;
  }

  navigateAhead(event) {
    let button=event.target.title;
    if(this.userTimeZone!==undefined && this.userTimeZone!==null && this.userTimeZone!==''){
      let fields=this.userTimeZone.split('T');
      let timeVal=fields[1];
      let dateVal=fields[0];
      let timeValue=timeVal.split(":");
      let textVal=this.formatDate(dateVal)+' '+timeValue[0]+':'+timeValue[1]+' '+this.timeZone[completedDateText.fieldApiName];
      if (this.isADFViewerInternal) {
        if(button=="navigateToView"){
        this[NavigationMixin.Navigate]({
          type: "comm__namedPage",
          attributes: {
            name: "Home"
          },
          state: {
            "tabset-8e13c": "4d03f"
          }
        });
        }
        else{
          this.goToNextScreen();
        }
      } else {
        this.getFormattedCompletedDateTimeVal(
          dateVal + " " + timeValue[0] + ":" + timeValue[1] + ":000",
          this.timeZone[completedDateText.fieldApiName],
          textVal,
          button
        );
      }
    }
    
  }
  getFormattedCompletedDateTimeVal(dateTimeVal,timeZoneVal,textVal,button){
    getFormattedDateTimeDetails({dateTimeVal:dateTimeVal,timeZoneVal:timeZoneVal})
      .then((result) => {
        this.formattedDateTime = result;
        let completionDate =this.formattedDateTime;
        setUserCollectionDetails({
          userName: this.userName,
          completionDate: completionDate,
          recordId: this.recordId,
          textVal: textVal
        })
          .then((result) => {
            this.error = null;
            if(button=="navigateToView"){
              this.saveLaterClicked = true;
              this[NavigationMixin.Navigate]({
              type: "comm__namedPage",
              attributes: {
              name: "Home"
              },
              state: {
              "tabset-8e13c": "4d03f"
              }
            });
            }
            else if(button=="NextScreen"){
              this.goToNextScreen();
            }
          })
          .catch((error) => {
            this.error = error;
          });
    })
    .catch((error) => {
        this.error = error;
    });
        }

        goToNextScreen() {
        this.isModalOpen = false;
        const attributeChangeEvent = new FlowAttributeChangeEvent('screenName','4');
        this.dispatchEvent(attributeChangeEvent);
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
        }
		
		compareJson(orignalJson,newJson,length){
		let flag=false;
		for(let i=1;i<=length;i++){
		let oJson=orignalJson.collections[i];
		let nJson=newJson.collections[i];
	   

		  if(oJson.CCL_TEXT_CBC_Differential_Date_Time__c===nJson.CCL_TEXT_CBC_Differential_Date_Time__c&&oJson.CCL_Hgb__c===nJson.CCL_Hgb__c&&
			oJson.CCL_PLT__c===nJson.CCL_PLT__c&&oJson.CCL_TEXT_Absolute_Counts_Date_Time__c===nJson.CCL_TEXT_Absolute_Counts_Date_Time__c&&
			oJson.CCL_White_Blood_Cell_Count__c===nJson.CCL_White_Blood_Cell_Count__c&&oJson.CCL_Absolute_Lymphocyte_Counts__c===nJson.CCL_Absolute_Lymphocyte_Counts__c&&
			oJson.CCL_TEXT_Flow_Cytometry_Date_Time__c===nJson.CCL_TEXT_Flow_Cytometry_Date_Time__c&&oJson.CCL_Absolute_CD3_Lymphocyte_Count__c
			===nJson.CCL_Absolute_CD3_Lymphocyte_Count__c){
			}else{
			return false;
		  }
		  }
		  return true;
		}
		
    handleCustomEvent(event) {
    let match = false;
    if (this.count == 0) {
      this.count = this.count + 1;
      this.collectionInput[this.count] = event.detail;
    } else {
      for (let i = 0; i <= this.count; i++) {
        for (const [key, value] of Object.entries(this.collectionInput)) {
          if (value.Id == event.detail.Id) {
            this.collectionInput[key] = event.detail;
            match = true;
          }
        }
      }
    }
    if (!match && this.collectionInput[1].Id != event.detail.Id) {
      this.count = this.count + 1;
      this.collectionInput[this.count] = event.detail;
    }
    this.myJson = { collections: this.collectionInput };
    if(this.count===this.tableData.length){
       let val=this.orignalJson;
      let mytable=val["3"];
      let compare=mytable!=undefined?this.compareJson(this.myJson,mytable,this.tableData.length):true;
      if((!compare||(this.orignalFileresult!==undefined?this.orignalFileresult!=JSON.stringify(this.wiredFileResult):false)
      ||(this.comments!=undefined?this.orignalJson["Collection and Cryopreservation Comments"]!==this.comments:false))
      &&this.counter>0&&(this.saveLaterClicked)){
        this.isReasonModalOpen=true;
      }
      else{
    this.saveAsJson();}
    }
  }
handleValidation(event){
  let validCount=0;
  let isNotvalid=false;
  this.validationInput[event.detail.Id]=event.detail.navigateFurther;
  if (this.tableData.length===Object.keys(this.validationInput).length){
    for (let [key, value] of Object.entries(this.validationInput)) {
      if(value===true){
        validCount=validCount+1;
      }
	  else{
        isNotvalid=true;
      }
  }
  }
   if(isNotvalid){
    this.validationInput={};
  }
  if(validCount===this.tableData.length && !this.hasModalClicked){
    this.isModalOpen = true;
   }
   else if(this.hasModalClicked && validCount===this.tableData.length){
    this.goToNextScreen();
   }
   else{
    this.saveClicked=false;
   }
}

 //Changes as part of CGTU 355
  removeFile(event){
    if(event.target.name === 'openConfirmation'){
      this.docVersionId=event.target.alternativeText;
      this.isDialogVisible = true;
    }else if(event.target.name === 'confirmModal'){
      //when user clicks outside of the dialog area, the event is dispatched with detail value  as 1
      if(event.detail !== 1){
           if(event.detail.status === 'confirm') {
              deleteFile({contentDocumentId:this.docVersionId}).then((result) => {
              this.handleDeleteFinished();
              this.isDialogVisible = false;
              this.error = null;
            })
            .catch((error) => {
               this.error = error;
            });
          //Changes as part of 355 ends here
              this.isDialogVisible = false;
          }else if(event.detail.status === 'cancel'){
             this.isDialogVisible = false;
          }
      }
      //hides the component
      this.isDialogVisible = false;
  }
  }
  //Changes as part of CGTU 355 end here
	
	 //changes as part of 850
  closeReasonModal() {
	  this.count=0;
    this.saveClicked=false;
    this.saveLaterClicked=false;
    this.previousClicked=false;
    this.isReasonModalOpen=false;
  }
  handleReasonChange(event){
    this.ReasonForMod=event.target.value;
  }
  validateTextarea(){
    const textArea=this.template.querySelector('.Reason');
    let commentValue=textArea.value;
    if(commentValue==undefined){
      this.navigater=false;
      textArea.setCustomValidity('Complete this Field');
      textArea.reportValidity();
    }else{
      this.navigate=true;
      textArea.setCustomValidity('');
      textArea.reportValidity();
    }
  }
  onReasonSubmit(event){
    this.validateTextarea();
    //2260
    if (this.navigate) {
      this.updateReason();
    }
  }
  updateReason(){
    const fields = {};
    fields[ID.fieldApiName] = this.recordId;
    fields[reasonForModification.fieldApiName] =this.ReasonForMod;
    const recordInput = { fields };
   if (!this.isADFViewerInternal) {
      updateRecord(recordInput)
        .then(() => {
          if (this.saveClicked) {
            this.updateCollectionValues();
          } else {
            this.saveAsJson();
          }
        })
        .catch((error) => {
          this.error = error;
        });
    } else {
      if (this.saveClicked) {
        this.updateCollectionValues();
      } else {
        this.saveAsJson();
      }
    }
      }
    //changes for 850 end here

      updateCollectionValues(){
        for(let i=1; i<=this.tableData.length;i++){
          let fields=this.myJson.collections[i];
          let recordInput={fields};
           //2260
      if (!this.isADFViewerInternal) {
        updateRecord(recordInput)
          .then(() => {
            if (i == this.tableData.length) {
              this.saveAsJson();
            }
          })
          .catch((error) => {
            this.error = error;
          });
      } else {
        if (i == this.tableData.length) {
          this.saveAsJson();
        }
      }
    }
      }

      handleUpdateReason(event){
        this.count = this.count + 1;
      this.collectionInput[event.detail.index] = event.detail.fields;
    this.myJson = { collections: this.collectionInput };
    if(this.count===this.tableData.length){
      let val=this.orignalJson;
      let mytable=val["3"];
      let compare;
      if(mytable!=undefined && mytable.collections[this.tableData.length]!=undefined){
        compare=this.myJson!=undefined?this.compareJson(this.myJson,mytable,this.tableData.length):true;
      }else{
        compare=true;
      }
     if((!compare||(this.orignalFileresult!==undefined?this.orignalFileresult!=JSON.stringify(this.wiredFileResult):false)
      ||(this.comments!=undefined?this.orignalJson["Collection and Cryopreservation Comments"]!==this.comments:false))
      &&this.counter>0){
        this.isReasonModalOpen=true;
      }
      else{
    this.saveAsJson();}
    }
      }
	   //2260
  gotoADFSummary() {
    const screenNextEvent = new FlowAttributeChangeEvent("screenName", "6");
    this.dispatchEvent(screenNextEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }
}
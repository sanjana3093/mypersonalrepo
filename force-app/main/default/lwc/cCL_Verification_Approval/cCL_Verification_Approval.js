import { LightningElement, track, api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import getCurrentApproverDateTime from "@salesforce/apex/CCL_PRF_Controller.getCurrentApproverDateTime";
import updateOrderApprovedOrRejected from "@salesforce/apex/CCL_PRF_Controller.updateOrderApprovedOrRejected";
import getTherapyAssociation from "@salesforce/apex/CCL_PRF_Controller.getTherapyAssociation";
import checkCustomerOperationsUser from "@salesforce/apex/CCL_PRF_Controller.checkCustOpsPermission";
// Labels added for this Page
import CCL_Approve_Order_Modal_Title from "@salesforce/label/c.CCL_Approve_Order_Modal_Title";
import CCL_Reject_Order_Modal_Title from "@salesforce/label/c.CCL_Reject_Order_Modal_Title";
import CCL_Approver_Checkbox_Label from "@salesforce/label/c.CCL_Approver_Checkbox_Label";
import CCL_Rejecter_Order_Textarea_Label from "@salesforce/label/c.CCL_Rejecter_Order_Textarea_Label";
import CCL_Order_Approved from "@salesforce/label/c.CCL_Order_Approved";
import CCL_Order_Rejected from "@salesforce/label/c.CCL_Order_Rejected";
import CCL_Order_Approved_Sub_Text from "@salesforce/label/c.CCL_Order_Approved_Sub_Text";
import CCL_Order_Rejected_Sub_Text from "@salesforce/label/c.CCL_Order_Rejected_Sub_Text";
import CCL_Approve_Order_Validation_Msg from "@salesforce/label/c.CCL_Approve_Order_Validation_Msg";
import CCL_Verification_and_Approval from "@salesforce/label/c.CCL_Verification_and_Approval";
import CCL_Submitted_by from "@salesforce/label/c.CCL_Submitted_by";
import CCL_Reject_Button from "@salesforce/label/c.CCL_Reject_Button";
import CCL_Approve_Button from "@salesforce/label/c.CCL_Approve_Button";
import CCL_Verfification_Rejection_Field_Complete_Error from "@salesforce/label/c.CCL_Verfification_Rejection_Field_Complete_Error";

const FIELDS = [
    'CCL_Order__c.CCL_Ordering_Hospital_Text__c',
    'CCL_Order__c.CCL_PRF_Ordering_Status__c',
    'CCL_Order__c.CCL_Therapy__r.CCL_Therapy_Description__c',
    'CCL_Order__c.CCL_Hospital_Patient_ID__c',
    'CCL_Order__c.CCL_Treatment_Protocol_Subject_ID__c',
    'CCL_Order__c.CCL_Therapy__r.CCL_Type__c',
    'CCL_Order__c.CCL_Therapy__c',
    'CCL_Order__c.CCL_Patient_Weight__c',
    'CCL_Order__c.CCL_Ordering_Hospital__r.ShippingCountryCode',
    'CCL_Order__c.CCL_Prescriber_Text__c',
    'CCL_Order__c.CCL_First_Name__c',
    'CCL_Order__c.CCL_Middle_Name__c',
    'CCL_Order__c.CCL_Last_Name__c',
    'CCL_Order__c.CCL_Suffix__c',
    'CCL_Order__c.CCL_Patient_Initials__c',
    'CCL_Order__c.CCL_Date_of_DOB__c',
    'CCL_Order__c.CCL_Month_of_DOB__c',
    'CCL_Order__c.CCL_Year_of_DOB__c',
    'CCL_Order__c.CCL_Consent_for_Additional_Research__c',
    'CCL_Order__c.CCL_Apheresis_Collection_Center__c',
    'CCL_Order__c.CCL_Apheresis_Pickup_Location__c',
    'CCL_Order__c.CCL_Infusion_Center__c',
    'CCL_Order__c.CCL_Ship_to_Location__c',
    'CCL_Order__c.CCL_Planned_Cryopreserved_Apheresis_Text__c',
    'CCL_Order__c.CCL_Est_Manufacturing_Start_Date_Text__c',
    'CCL_Order__c.CCL_Estimated_FP_Delivery_Date_Text__c',
    'CCL_Order__c.CCL_Hospital_Purchase_Order_Number__c',
    'CCL_Order__c.CCL_VA_Patient__c',
    'CCL_Order__c.CCL_Patient_Country_Residence__c',
    'CCL_Order__c.CCL_Billing_Party__c',
    'CCL_Order__c.CCL_Latest_Order_Submitted_By__r.Name',
    'CCL_Order__c.CCL_Lastest_Order_Submitted_Date_Text__c',
	'CCL_Order__c.CCL_FirstName_COI__c',
    'CCL_Order__c.CCL_MiddleName_COI__c',
    'CCL_Order__c.CCL_LastName_COI__c',
    'CCL_Order__c.CCL_Sufix_COI__c',
    'CCL_Order__c.CCL_Initials_COI__c',
    'CCL_Order__c.CCL_Day_of_Birth_COI__c',
    'CCL_Order__c.CCL_Month_of_Birth_COI__c',
    'CCL_Order__c.CCL_Year_of_Birth_COI__c',
	 'CCL_Order__c.CCL_PRF_Approval_Counter__c'
    ];

export default class CCL_Verification_Approval extends NavigationMixin(LightningElement) {

    @api isModalOpen = false;
    @api approverDateTimeNVS;
    @api isApproveBtnDisabled=false;
    @api isRejectBtnDisabled=false;
    @api checkBoxVal;
    @api recordId;
	@api orderid;
    @api showRejectBtn;
    @api showApproveBtn;
    @api therapyId;
    @api countryCode;
    @track isCustomerOperationsUser=false;

    @track objUser = {};
    @track labels={
        CCL_Approve_Order_Modal_Title,
        CCL_Approver_Checkbox_Label,
        CCL_Rejecter_Order_Textarea_Label,
        CCL_Order_Approved,
        CCL_Order_Rejected,
        CCL_Order_Approved_Sub_Text,
        CCL_Order_Rejected_Sub_Text,
        CCL_Approve_Order_Validation_Msg,
        CCL_Reject_Order_Modal_Title,
        CCL_Verification_and_Approval,
        CCL_Submitted_by,
        CCL_Reject_Button,
        CCL_Approve_Button
    }

    @wire(CurrentPageReference)
    currentPageReference;
    orderData;
    order_status;
    showConfirmation;
    showValidationMsg;
    displayErrorMsg;
    timeZone;
    loaded;
    showApproverContent;
    showRejecterContent;
    iconName;
    iconClass;
    modalTitle;
    rejectReason;
    navigateFurther;
    confirmationTitle;
    confirmationSubText;
    commentValue;
    orderingHospital;
    therapy;
    hospitalPatientId;
    approvalPageCounter;
    therapyType;
    splitSubject;
    patientWeight;
    therapyAssocData;
    countryTherapyId;
    prescriberInvestigator;
    patientFirstName;
    middleInitial;
    lastName;
    suffix;
    patientInitial;
    dateofBirth;
    monthOfDob;
    yearOfDob;
    patientConsent;
    collectionCenterId;
    aphPickupLocationId;
    infusionCenterId;
    shipToLocId;
    SiteDiscrepancyText;
    sitetextavail;
    plannedPickupDate;
    manufacturingStartDate;
    productFinishedDate;
    showDynamicText;
    hospPurchaseOrderOptInFlag;
    hospitalOptIn;
    hospPurchaseOrderNo;
    veteransApplicableFlag;
    veteransFlag;
    VaPatientCheckbox;
    patientCountryResidenceValue;
    capturePatientCountry;
    billingPartyVisible;
    billingPartyValue;
    latestOrderSubmitter;
    latestOrderSubmittedDate;
	  prfApprovalCounter;
    connectedCallback() {
        this.recordId = this.currentPageReference.state.recordId;
        this.isApproveBtnDisabled = true;
		this.orderid = this.recordId;
    }

     // wire to get order object data
     @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
     wiredRecord({ error, data }) {
         if (error) {
             let message = 'Unknown error';
             if (Array.isArray(error.body)) {
                 message = error.body.map(e => e.message).join(', ');
             } else if (typeof error.body.message === 'string') {
                 message = error.body.message;
             }
         } else if (data) {
             this.orderData = data;
             this.approvalPageCounter = true;
             this.order_status = this.orderData.fields.CCL_PRF_Ordering_Status__c.value;
             this.orderingHospital = this.orderData.fields.CCL_Ordering_Hospital_Text__c.value;
             this.therapy = this.orderData.fields.CCL_Therapy__r.value.fields.CCL_Therapy_Description__c.value;
             this.therapyId = this.orderData.fields.CCL_Therapy__c.value;
             this.hospitalPatientId = this.orderData.fields.CCL_Hospital_Patient_ID__c.value;
             this.splitSubject = this.orderData.fields.CCL_Treatment_Protocol_Subject_ID__c.value;
             this.therapyType =  this.orderData.fields.CCL_Therapy__r.value.fields.CCL_Type__c.value;
             this.patientWeight = this.orderData.fields.CCL_Patient_Weight__c.value;
             this.countryCode = this.orderData.fields.CCL_Ordering_Hospital__r.value.fields.ShippingCountryCode.value;
             this.prescriberInvestigator =  this.orderData.fields.CCL_Prescriber_Text__c.value;
             this.patientFirstName = this.orderData.fields.CCL_First_Name__c.value;
             this.middleInitial = this.orderData.fields.CCL_Middle_Name__c.value;
             this.lastName = this.orderData.fields.CCL_Last_Name__c.value;
             this.suffix = this.orderData.fields.CCL_Suffix__c.value;
             this.patientInitial = this.orderData.fields.CCL_Patient_Initials__c.value;
             this.dateofBirth = this.orderData.fields.CCL_Date_of_DOB__c.value;
             this.monthOfDob = this.orderData.fields.CCL_Month_of_DOB__c.value;
             this.yearOfDob = this.orderData.fields.CCL_Year_of_DOB__c.value;
             this.patientConsent = this.orderData.fields.CCL_Consent_for_Additional_Research__c.value;
             this.collectionCenterId = this.orderData.fields.CCL_Apheresis_Collection_Center__c.value;
             this.aphPickupLocationId = this.orderData.fields.CCL_Apheresis_Pickup_Location__c.value;
             this.infusionCenterId = this.orderData.fields.CCL_Infusion_Center__c.value;
             this.shipToLocId = this.orderData.fields.CCL_Ship_to_Location__c.value;
             this.plannedPickupDate = this.orderData.fields.CCL_Planned_Cryopreserved_Apheresis_Text__c.value;
             this.manufacturingStartDate = this.orderData.fields.CCL_Est_Manufacturing_Start_Date_Text__c.value;
             this.productFinishedDate = this.orderData.fields.CCL_Estimated_FP_Delivery_Date_Text__c.value;
             this.hospPurchaseOrderNo = this.orderData.fields.CCL_Hospital_Purchase_Order_Number__c.value;
             this.VaPatientCheckbox = this.orderData.fields.CCL_VA_Patient__c.value;
             this.patientCountryResidenceValue = this.orderData.fields.CCL_Patient_Country_Residence__c.displayValue;
             this.billingPartyValue = this.orderData.fields.CCL_Billing_Party__c.displayValue;
             this.latestOrderSubmitter = this.orderData.fields.CCL_Latest_Order_Submitted_By__r.value.fields.Name.value;
             this.latestOrderSubmittedDate = this.orderData.fields.CCL_Lastest_Order_Submitted_Date_Text__c.value;
			 this.OrderFirstNameCOI=this.orderData.fields.CCL_FirstName_COI__c.value;
             this.OrderMiddleNameCOI=this.orderData.fields.CCL_MiddleName_COI__c.value;
             this.OrderLasttNameCOI=this.orderData.fields.CCL_LastName_COI__c.value;
             this.OrderSuffixCOI=this.orderData.fields.CCL_Sufix_COI__c.value;
             this.OrderInitialsCOI=this.orderData.fields.CCL_Initials_COI__c.value;
             this.OrderDayCOI=this.orderData.fields.CCL_Day_of_Birth_COI__c.value;
             this.OrderMonthCOI=this.orderData.fields.CCL_Month_of_Birth_COI__c.value;
             this.OrderYearCOI=this.orderData.fields.CCL_Year_of_Birth_COI__c.value;
			 this.prfApprovalCounter=this.orderData.fields.CCL_PRF_Approval_Counter__c.value;
             if (this.order_status !== 'PRF_Submitted') {
                 this.showValidationMsg = true;
             }
             this.getCountryTherapyData();
         }
     }

     getCountryTherapyData()
    {
    getTherapyAssociation({
        therapyId: this.therapyId,
        countryCode: this.countryCode
    })
    .then(result => {
        this.therapyAssocData = result;
        this.countryTherapyId = this.therapyAssocData[0].Id;
        this.SiteDiscrepancyText = this.therapyAssocData[0].CCL_PRF_Sites_Selection_Discrepancy_Text__c;
        this.paymentSectionText = this.therapyAssocData[0].CCL_Payment_Section_Text__c;
        this.capturePatientCountry = this.therapyAssocData[0].CCL_Capture_Patient_Country_of_Residence__c;
        this.billingPartyVisible = this.therapyAssocData[0].CCL_Billing_Party_Visible__c;
        if (this.SiteDiscrepancyText != undefined) {
          this.sitetextavail = true;
        } else {
          this.sitetextavail = false;
        }
        this.belowSchedulerText = this.therapyAssocData[0].CCL_Text_Below_Scheduler__c;
        if (this.belowSchedulerText != undefined) {
            this.showDynamicText = true;
          } else {
            this.showDynamicText = false;
          }
          this.hospPurchaseOrderOptInFlag = this.therapyAssocData[0].CCL_Hosp_Purchase_Order_Opt_In__c;
          if(this.hospPurchaseOrderOptInFlag=='Yes')
          {
              this.hospitalOptIn=true;
          }
          else if(this.hospPurchaseOrderOptInFlag=='No')
          {
             this.hospitalOptIn=false;
          }
          this.veteransFlag = this.therapyAssocData[0].CCL_Veteran_Affairs_Applicable__c;
          if(this.veteransFlag=='Yes')
        {
            this.veteransApplicableFlag=true;
        }
        else if(this.veteransFlag=='No')
        {
           this.veteransApplicableFlag=false;
        }
    })
    .catch(error => {
      this.error = error;
    });
  }

    closeModal() {
        this.isModalOpen = false;
        this.displayErrorMsg = false;
    }

    openApproverModal() {
        this.showApproverContent = true;
        this.showRejecterContent = false;
        this.showApproveBtn = true;
        this.showRejectBtn = false;
        this.iconName = 'action:approval';
        this.iconClass = 'iconColorApproval';
        this.modalTitle = CCL_Approve_Order_Modal_Title;
        this.loaded = true;
        this.isModalOpen = true;
        this.loaded = false;
        this.isApproveBtnDisabled = true;
    }

    handleCheckbox(event) {
        if(event.target.type == 'checkbox'){
            this.checkBoxVal = event.target.checked;
            this.isApproveBtnDisabled = !this.checkBoxVal;
            if(this.isCustomerOperationsUser==true)
            {
                this.isApproveBtnDisabled=true;
            }
        }
    }

    updateOrder() {
        this.loaded = true;
        if(this.navigateFurther) {
            this.orderDetail=
            {
                "CCL_PRF_Ordering_Status__c":'PRF_Rejected',
                "CCL_Order_Rejectionl_Date_Text__c":this.approverDateTimeNVS,
                "CCL_Order_Rejected_By__c":USER_ID,
                'timezone': this.timeZone,
                'rejectReason': this.commentValue
            }
        } else {
             let approvalCounterVal=this.prfApprovalCounter;
            if(this.prfApprovalCounter!==null){
                approvalCounterVal=approvalCounterVal+1;

              }else{
                approvalCounterVal=1;
              }
            this.orderDetail=
            {
                "CCL_PRF_Ordering_Status__c":'PRF_Approved',
                "CCL_Order_Approval_Date_Text__c":this.approverDateTimeNVS,
                "CCL_Order_Approved_By__c":USER_ID,
                "CCL_PRF_Approval_Counter__c":approvalCounterVal,
                'timezone': this.timeZone
            }
        }
        this.orderDetail.Id = this.recordId;
        updateOrderApprovedOrRejected({ result: this.orderDetail })
        .then((result) => {
        if (result=='PRF_Approved') {
            this.loaded = false;
            this.isModalOpen=false;
            this.showConfirmation=true;
            this.confirmationTitle = CCL_Order_Approved;
            this.confirmationSubText= CCL_Order_Approved_Sub_Text;
            this.iconClass = 'iconColorApproval'
        }
        if (result=='PRF_Rejected') {
            this.loaded = false;
            this.isModalOpen=false;
            this.showConfirmation=true;
            this.confirmationTitle = CCL_Order_Rejected;
            this.confirmationSubText= CCL_Order_Rejected_Sub_Text;
            this.iconClass = 'iconColorReject'
        }
        })
        .catch((error) => {
            this.message = "";
            this.error = error;
            this.errorOnUpdate = error.body? error.body.message : error;
        });

    }

    showValidation() {
        this.displayErrorMsg=true;
    }

    openRejectOrderModal() {
        this.showRejecterContent = true;
        this.showApproverContent = false;
        this.showRejectBtn = true;
        this.showApproveBtn = false;
        this.iconName = 'action:close';
        this.iconClass = 'iconColorReject';
        this.modalTitle = CCL_Reject_Order_Modal_Title;
        this.loaded = true;
        this.isModalOpen = true;
        this.loaded = false;
    }

    handleCommentsChange(event) {
        this.rejectReason = event.target.value;
    }

    rejectOrder(){
        this.navigateFurther=true;
        this.validateTextarea();
        const textArea=this.template.querySelector('.rejectionComment');
        if(this.navigateFurther){
            this.updateOrder();
        }
    }

    validateTextarea(){
        const textArea=this.template.querySelector('.rejectionComment');
        this.commentValue=textArea.value;
        this.commentValue = this.commentValue ? this.commentValue.trim() : this.commentValue;
        if(!this.commentValue) {
          this.navigateFurther=false;
          textArea.setCustomValidity(CCL_Verfification_Rejection_Field_Complete_Error);
          textArea.reportValidity();
        } else {
          this.navigateFurther=true;
          textArea.setCustomValidity('');
          textArea.reportValidity();
        }
    }

    // wire to get the logged in user details
    @wire(getRecord, { recordId: USER_ID, fields: ['User.FirstName', 'User.LastName'] })
    userData({error, data}) {
        if(data) {
            let objCurrentData = data.fields;
            this.objUser = {
                FirstName : objCurrentData.FirstName.value,
                LastName : objCurrentData.LastName.value,
                Name : objCurrentData.Name,
            }
        }
        else if(error) {
        }
    }

    @wire(getCurrentApproverDateTime, { recordId: '$recordId'})
    formatDateTime({ error, data }) {
        if (data) {
            this.approverDateTimeNVS = data.nVSFormatDate;
            this.timeZone = data.timezone;
        }
        else if (error) {
            this.error = error;
        }
    }
    @wire(checkCustomerOperationsUser)
	checkCustomerOperations({error, data}) {
      if (data) {
        this.isCustomerOperationsUser = data;
        this.isRejectBtnDisabled=true;
        }
      else if (error) {
        this.isCustomerOperationsUser=false;
        this.error = error;
        this.isRejectBtnDisabled=false;
        }
    }

    redirectToSummary()  {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
        attributes: {
            recordId: this.recordId,
            actionName: 'view'
        }
        });
    }
}
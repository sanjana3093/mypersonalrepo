import { LightningElement ,track,api} from 'lwc';

export default class PSP_providerLookup extends LightningElement {
    @api selectedItem='';
    @api productServicename="";
    @api recordtype="";
    @api object="CareProgramProvider";
    @api searchtext="Search a Care Program Provider";
    getselectedDetails(event){
        // console.log('the event details are'+JSON.stringify(event.detail));
         this.selectedItem=event.detail.Id;
         this.productServicename=event.detail.Name;
        // console.log(this.productServicename+ this.selectedItem);
     }
}
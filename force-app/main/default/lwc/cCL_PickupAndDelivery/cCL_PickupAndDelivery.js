import { LightningElement, track, api, wire } from "lwc";
import getSiteWrapper from "@salesforce/apex/CCL_PRF_Controller.getSiteWrapper";
import getAphPickupLocation from "@salesforce/apex/CCL_PRF_Controller.getAphPickupLocation";
import getShipToLocation from "@salesforce/apex/CCL_PRF_Controller.getShipToLocation";
import getTherapyAssociation from "@salesforce/apex/CCL_PRF_Controller.getTherapyAssociation";
import checkUserPermission from "@salesforce/apex/CCL_PRF_Controller.checkUserPermission";
import sendCallout from "@salesforce/apex/CCL_PRF_Controller.sendCallout";
import updateRescheduleOrder from "@salesforce/apex/CCL_PRF_Controller.updateRescheduleOrder";
import ApheresisPickupDelivery from "@salesforce/label/c.CCL_Apheresis_Pickup_Delivery";
import Back_Up_Apheresis_Pick_Up_Offset from "@salesforce/label/c.Back_Up_Apheresis_Pick_Up_Offset";
import ApheresisCollectionCenter from "@salesforce/label/c.CCL_Apheresis_Collection_Center";
import CCL_Select_apheresis_collection_center from "@salesforce/label/c.CCL_Select_apheresis_collection_center";
import ApheresisCollection from "@salesforce/label/c.CCL_Apheresis_Collection";
import CollectionAddress from "@salesforce/label/c.CCL_Collection_Address";
import ApheresisPickupLocation from "@salesforce/label/c.CCL_Apheresis_Pickup_Location";
import CCL_Select_apheresis_pick_up_location from "@salesforce/label/c.CCL_Select_apheresis_pick_up_location";
import PickupAddress from "@salesforce/label/c.CCL_Pickup_Address";
import ProductDelivery from "@salesforce/label/c.CCL_Product_Delivery";
import InfusionCenter from "@salesforce/label/c.CCL_Infusion_Center";
import CCL_Select_Infusion_Center from "@salesforce/label/c.CCL_Select_Infusion_Center";
import InfusionAddress from "@salesforce/label/c.CCL_Infusion_Address";
import ShipToLocation from "@salesforce/label/c.CCL_Ship_To_Location";
import CCL_Select_Ship_To_Location from "@salesforce/label/c.CCL_Select_Ship_To_Location";
import ShipToAddress from "@salesforce/label/c.CCL_Ship_To_Address";
import Scheduling from "@salesforce/label/c.CCL_Scheduling";
import CCL_Capacity_API_Change_Message from "@salesforce/label/c.CCL_Capacity_API_Change_Message";
import Capacity_API_message from "@salesforce/label/c.Capacity_API_message";
import CryopreservedApheresisShipmentDate from "@salesforce/label/c.CCL_Cryopreserved_Apheresis_Shipment_Date";
import EstimatedCellmanufacturingStartDate from "@salesforce/label/c.CCL_Estimated_Cell_manufacturing_Start_Date";
import EstimatedFinishedProductDeliveryDate from "@salesforce/label/c.CCL_Estimated_Finished_Product_Delivery_Date";
import ClinicalSupport from "@salesforce/label/c.CCL_Clinical_Support";
import NotAvailable from "@salesforce/label/c.CCL_Not_Available";
import ToBeConfirmed from "@salesforce/label/c.CCL_To_be_confirmed";
import MandatoryField from "@salesforce/label/c.CCL_Mandatory_Field";
import siteChangeReason from "@salesforce/label/c.CCL_SiteChangeReason";
import CCL_Consent from "@salesforce/label/c.CCL_Consent";
import CCL_Cancel_Button from "@salesforce/label/c.CCL_Cancel_Button";
import CCL_Yes_edit_details from "@salesforce/label/c.CCL_Yes_edit_details";
import { FlowNavigationNextEvent, FlowAttributeChangeEvent } from 'lightning/flowSupport';
import getAddress from "@salesforce/apex/CCL_Utility.fetchAddress";

export default class CCLPickupAndDelivery extends LightningElement {
  @api previousButtonClick;
  @api showDynamicText = false;
  @track aphPickupData = null;
  @track shipToLocData = null;
  @track aphCollectionData;
  @track infCenterData;
  @track wrapper;
  @track error;
  @track recordLength;
  @track isValidationError = false;
  @track isRendered = true;
  @track loadSpinner = false;
  @api colCenterFlowVar;
  @api collectionCenter;
  @api infusionCenter;
  @api therapy;
  @api hospital;
  @api aphPickupLocation;
  @api shipToLoc;
  @api AphPickuprecordLength;
  @api shipToLocRecordLength;
  @api infCenterRecLength;
  @api collectionSiteAdd;
  @api aphPickupSiteAdd;
  @api shipToLocSiteAdd;
  @api infSiteAddress;
  @api hasAddress;
  @api hasPickupAddress;
  @api hasShipToLocAddress;
  @api hasInfAddress;
  @api returnToReview;
  @api screenName;
  @api collectionCenterId;
  @api aphPickupLocationId;
  @api infusionCenterId;
  @api shipToLocId;
  @api hasPermission = false;
  @api var1 = "slds-p-left_none slds-p-right_none";
  @api var2 = "customPadding";
  @api therapyType;
  @api therapyName;
  @api subId_Center;
  @api subId_last;
  @api response;
  @api requestJSON;
  @api subjectId;
  @api availableDates;
  @api availableSlot;
  @api availableDateArray = [];
  @api availableDateArrayFlow;
  @api missingResponse = false;
  @api plannedPickupDate;
  @api plannedPickupDateFlow;
  @api manufacturingStartDate;
  @api productFinishedDate;
  @api selectedDate;
  @api manufacturingStartDateFlow;
  @api productFinishedDateFlow;
  @api manufacturingDate;
  @api FinishedDate;
  @api countryCode;
  @api aboveSchedulerText;
  @api belowSchedulerText;
  @api therapyAssocData;
  @api aphPickupDate;
  @api datePickerError;
  @api replacementOrder;
  @api replacementOrderDates = false;
  @api oderUpdateDetails;
  @api Status;
  @api orderDocId;
  @api fileUploadDoc;
  @track responseJson;
  @track returnFromReview = false;
  @track showHideBackBtn = false;
  @track handleResponse;
  @track pickupReschedule=false;
  //@track error;
  @api qualifiedSlots;
  @api qualifiedSlotsFlow;
  @track calendarField;
  @track onReviewClicked;
  @api SiteDiscrepancyText;
  @api sitetextavail;
  @api responseRecieved = false;
  @api changeReason;
  @api adfUpdate = false;
  @api aphShipmentId;
  @api recordId;
  @api rescheduleFlag = false;
  @api rescheduleReason;
  @api plantId;
  @api slotId;
  @api valueStreamField;
  @api splitSubject;
  @api offSetDays;
  siteAdd = 0;
  @api approvalCheckboxFlow;
   @api addressIds =[];
  @api addressList;
  @track addLength;
  // variables for CGTU-890
  pickListValue='';
  selectionEle='';
  schedulerPrompt = false;
  @track errorOnUpdate = false;

  label = {
    ApheresisPickupDelivery,
    ApheresisCollectionCenter,
    CCL_Select_apheresis_collection_center,
    ApheresisCollection,
    CollectionAddress,
    ApheresisPickupLocation,
    CCL_Select_apheresis_pick_up_location,
    PickupAddress,
    ProductDelivery,
    InfusionCenter,
    CCL_Select_Infusion_Center,
    InfusionAddress,
    ShipToLocation,
    CCL_Select_Ship_To_Location,
    ShipToAddress,
    Scheduling,
    CryopreservedApheresisShipmentDate,
    EstimatedCellmanufacturingStartDate,
    EstimatedFinishedProductDeliveryDate,
    ClinicalSupport,
    NotAvailable,
    ToBeConfirmed,
    MandatoryField,
    siteChangeReason,
    CCL_Capacity_API_Change_Message,
    Capacity_API_message,
    CCL_Cancel_Button,
    CCL_Yes_edit_details,
    CCL_Consent
  };

  renderedCallback() {
    if (this.siteAdd == 3) {
      if (
        this.manufacturingStartDateFlow &&
        this.productFinishedDateFlow &&
        this.plannedPickupDateFlow &&
        !this.replacementOrder
      ) {
        this.manufacturingStartDate = this.manufacturingStartDateFlow;
        this.productFinishedDate = this.productFinishedDateFlow;
        this.selectedDate = this.plannedPickupDateFlow;
        this.aphPickupDate = this.plannedPickupDateFlow;
        if (this.availableDateArrayFlow && this.qualifiedSlotsFlow ) {
        this.availableDateArray = this.availableDateArrayFlow.split(',');
        this.qualifiedSlots = JSON.parse(this.qualifiedSlotsFlow);
        }
      } else if (this.replacementOrder && this.plannedPickupDateFlow){
        this.aphPickupDate = this.plannedPickupDateFlow;
      }

      const colCenter = this.template.querySelector('.colclass');
      if (colCenter && this.aphCollectionData) {
        colCenter.value = this.collectionCenter;
        let self = this;
        let hasAddress = false;
        let address = {};
        this.aphCollectionData.forEach(function (node) {
          if (
            node.CCL_Site__r.Name === self.collectionCenter &&
            node.CCL_Site__r.ShippingAddress
          ) {
            hasAddress = true;
            address = node.CCL_Site__r.ShippingAddress;
          }
        });
        this.siteAdd = 0;
        this.collectionSiteAdd = address;
        this.hasAddress = hasAddress;
      }
      const pickupCenter = this.template.querySelector('.pickupClass');
      if (pickupCenter && this.aphPickupData) {
        pickupCenter.value = this.aphPickupLocation;
        let self = this;
        let hasPickupAdd = false;
        let pickupaddress = {};
        this.aphPickupData.forEach(function (node) {
          if (
            node.CCL_Site__r.Name === self.aphPickupLocation &&
            node.CCL_Site__r.ShippingAddress
          ) {
            hasPickupAdd = true;
            pickupaddress = node.CCL_Site__r.ShippingAddress;
          }
        });
        this.siteAdd = 0;
        this.hasPickupAddress = hasPickupAdd;
        this.aphPickupSiteAdd = pickupaddress;

      }
      const infCenter = this.template.querySelector('.infClass');
      if (infCenter && this.infCenterData) {
        infCenter.value = this.infusionCenter;
        let self = this;
        let hasInfAddressAdd = false;
        let InfAddressaddress = {};
        this.infCenterData.forEach(function (node) {
          if (
            node.CCL_Site__r.Name === self.infusionCenter &&
            node.CCL_Site__r.ShippingAddress
          ) {
            hasInfAddressAdd = true;
            InfAddressaddress = node.CCL_Site__r.ShippingAddress;
          }
        });
        this.siteAdd = 0;
        this.hasInfAddress = hasInfAddressAdd;
        this.infSiteAddress = InfAddressaddress;
      }
      const shiptoLoc = this.template.querySelector('.shiptoClass');
      if (shiptoLoc && this.shipToLocData) {
        shiptoLoc.value = this.shipToLoc;
        let self = this;
        let hasShipToLocAdd = false;
        let ShipToLocaddress = {};
        this.shipToLocData.forEach(function (node) {
          if (
            node.CCL_Site__r.Name === self.shipToLoc &&
            node.CCL_Site__r.ShippingAddress
          ) {
            hasShipToLocAdd = true;
            ShipToLocaddress = node.CCL_Site__r.ShippingAddress;
          }
        });

        this.siteAdd = 0;
        this.hasShipToLocAddress = hasShipToLocAdd;
        this.shipToLocSiteAdd = ShipToLocaddress;
      }
    }
    if (this.collectionCenter && !this.adfUpdate) {
      this.template.querySelector('.colcustomError').classList.remove("redBorder");
      this.template.querySelector('.colremoveError').setAttribute('style', 'display:none;');
    }
    if (this.aphPickupLocation && !this.adfUpdate) {
      this.template.querySelector('.pickupcustomError').classList.remove("redBorder");
      this.template.querySelector('.pickupremoveError').setAttribute('style', 'display:none;');
    }
    if (this.infusionCenter && !this.adfUpdate) {
      this.template.querySelector('.infcustomError').classList.remove("redBorder");
      this.template.querySelector('.infremoveError').setAttribute('style', 'display:none;');
    }
    if (this.shipToLoc) {
      this.template.querySelector('.shipTocustomError').classList.remove("redBorder");
      this.template.querySelector('.shipToremoveError').setAttribute('style', 'display:none;');
    }
    if (this.isRendered) {
      if (this.collectionCenter && this.aphPickupLocation &&
        this.infusionCenter && this.shipToLoc) {
        if (this.therapyType == 'Commercial') {
          if (!this.replacementOrder) {
            this.subjectId = null;
            this.handleCallout();
            this.isRendered = false;
          } else {
            this.calendarField = true;
          }
        }
        if (this.therapyType == "Clinical") {
          this.subjectId = this.subId_Center + "_" + this.subId_last;
          this.handleCallout();
          this.isRendered = false;
        }

      }

    }
	if(this.collectionCenterId && !( this.addressIds.includes(this.collectionCenterId))){
    this.addressIds.push(this.collectionCenterId);
  }
   if(this.aphPickupLocationId && !( this.addressIds.includes(this.aphPickupLocationId))){
	this.addressIds.push(this.aphPickupLocationId);
  }
  if(this.infusionCenterId && !( this.addressIds.includes(this.infusionCenterId))){
	this.addressIds.push(this.infusionCenterId);
  }
  if(this.shipToLocId && !( this.addressIds.includes(this.shipToLocId))){
	this.addressIds.push(this.shipToLocId);
	}
  if(this.addressIds.length>0 && this.addLength!=this.addressIds.length) {
    this.getAddresses();
   }
  }

  connectedCallback() {
    if (this.therapyType === 'Commercial' && this.replacementOrder) {
      this.replacementOrderDates = true;
      this.missingResponse = true;
      this.manufacturingStartDateFlow = null;
      this.productFinishedDateFlow = null;
      this.manufacturingDate = null;
      this.FinishedDate = null;
    }
    if (this.collectionCenter && this.infusionCenter && this.plannedPickupDateFlow)
      this.isRendered = false;
    if (this.returnToReview !== null && (this.returnToReview === 'PickupCommercial' || this.returnToReview === 'PickupClinical') && this.previousButtonClick) {
      this.returnFromReview = true;
    } else if (!this.previousButtonClick) {
      this.returnFromReview = false;
    }
    if (this.therapyType === 'Commercial') {
      this.showHideBackBtn = false;
    } else if (this.therapyType === 'Clinical') {
      this.showHideBackBtn = true;
    }
    if (this.splitSubject) {
      this.splitSubjectId(this.splitSubject);
    }
  }

  handleCallout() {
    this.loadSpinner = true;
    this.calendarField = true;
    sendCallout({
      therapyId: this.therapy,
      aphPickupLocation: this.aphPickupLocationId,
      shipToLoc: this.shipToLocId,
      subjectId: this.subjectId
    })
      .then((result) => {
        if (result) {
        this.responseJson = JSON.parse(result);
          if (this.responseJson.Status == "OK") {
          this.loadSpinner = false;
          this.qualifiedSlots = this.responseJson.QualifiedSlots;
          this.qualifiedSlotsFlow = JSON.stringify(this.qualifiedSlots);
            this.plantId = this.qualifiedSlots[0].Manufacturing_Plant;
            this.slotId = this.qualifiedSlots[0].SlotID;
            this.valueStreamField = this.qualifiedSlots[0].ValueStream;
            const aphStartDate = this.qualifiedSlots[0].Day_0_Date;
            let newStartDate = aphStartDate.split("-").reverse().join("-");
            this.manufacturingStartDate = this.formatDate(newStartDate);
            // this.manufacturingStartDateFlow used in flow for review screen
            this.manufacturingStartDateFlow = this.manufacturingStartDate;
            const finishDate = this.qualifiedSlots[0].APHPickup_Date;

            let newFinishedDate = finishDate.split('-').reverse().join('-');
            const formattedFinshedDate = new Date(newFinishedDate);

            let currentDate = new Date(
              formattedFinshedDate.getFullYear(),
              formattedFinshedDate.getMonth(),
              formattedFinshedDate.getDate() +
                parseInt(this.qualifiedSlots[0].ValueStream,10)
            );

            const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
            let endDate =
              currentDate.getFullYear() +
              "/" +
              month +
              "/" +
              currentDate.getDate();
            this.productFinishedDate = this.formatDate(endDate);
            this.productFinishedDateFlow = this.productFinishedDate;
            this.availableDates = this.qualifiedSlots[0].APHPickup_Date.replace(
              /-|\//g,
              "/"
            );
            this.availableSlot = this.availableDates;

            /**New logic to select first date from SCP**/
            let today = new Date();
            let dateNow = new Date(today.getFullYear(),today.getMonth(),today.getDate());
            let scpOffset = Math.round(( formattedFinshedDate - dateNow ) / (1000 * 3600 * 24));
            if(scpOffset > 0) {
              scpOffset-=1;
            }
            this.offSetDays = scpOffset;
            //this.availableDateArray.push(this.availableSlot);
            let tempDateArray=[];
            //let self = this;
            this.qualifiedSlots.forEach(function (node) {
              tempDateArray.push(node.APHPickup_Date.replace(/-|\//g, "/"));
            });
            this.availableDateArray = tempDateArray;
            this.availableDateArrayFlow = tempDateArray.toString();
            if (this.availableDateArray.includes(this.aphPickupDate)) {
              this.showDynamicText = true;
            }
          } else {
            this.loadSpinner = false;
            this.interfaceFailure();
          }
        } else {
          this.loadSpinner = false;
          this.interfaceFailure();
        }
      })
      .catch((error) => {
        this.error = error;
        this.loadSpinner = false;
      });
  }

  interfaceFailure(){

    /*Set offset value if scp integration is down*/
    let self = this;
    this.aphPickupData.forEach(function (node) {
      if (node.CCL_Site__r.Name === self.aphPickupLocation && node.CCL_Site__r.ShippingAddress !== null) {
        if(node.CCL_Site__r.CCL_Back_Up_Apheresis_Pick_Up_Offset__c!==undefined ) {
        self.offSetDays = node.CCL_Site__r.CCL_Back_Up_Apheresis_Pick_Up_Offset__c-1;
        } else {
          self.offSetDays=parseInt(Back_Up_Apheresis_Pick_Up_Offset,10)?parseInt(Back_Up_Apheresis_Pick_Up_Offset,10)-1:-1;
        }
      }
    });
        if (this.therapyType == 'Commercial' && !this.replacementOrder) {
          this.manufacturingStartDate = ToBeConfirmed;
          this.manufacturingStartDateFlow = ToBeConfirmed;
          this.showDynamicText = false;
          this.productFinishedDate = ToBeConfirmed;
          this.productFinishedDateFlow = ToBeConfirmed;
          this.missingResponse = true;
          this.availableDateArray = null;
          this.availableDates= null;
          this.availableSlot = null;
          this.selectedDate = null;
          this.plantId = null;
          this.slotId = null;
          this.valueStreamField = null;
        } else if (this.therapyType == 'Clinical' && !this.replacementOrder) {
          this.showDynamicText = false;
          this.manufacturingStartDate = NotAvailable;
          this.manufacturingStartDateFlow = NotAvailable;
          this.productFinishedDate = NotAvailable;
          this.productFinishedDateFlow = NotAvailable;
          this.missingResponse = true;
          this.availableDateArray = null;
          this.availableSlot = null;
          this.selectedDate = null;
          this.availableDates= null;
          this.plantId = null;
          this.slotId = null;
          this.valueStreamField = null;
      }
  }
  @wire(getSiteWrapper, { therapyId: "$therapy", hospital: "$hospital" })
  wiredSteps({ error, data }) {
    if (data) {
      this.wrapper = data;
      this.wrapper = JSON.parse(this.wrapper);
      let self = this;
      this.wrapper.forEach(function (node) {
        self.aphCollectionData = node.aphCollectionCenter;
      });
      this.siteAdd++;
      this.wrapper.forEach(function (node) {
        self.infCenterData = node.infusionCenter;
      });
      if (this.aphCollectionData.length > 1) {
        this.recordLength = true;
      } else {
        this.recordLength = false;
        this.hasAddress = true;
        if (this.aphCollectionData[0].CCL_Site__r.ShippingAddress !== null) {
          this.collectionSiteAdd = this.aphCollectionData[0].CCL_Site__r.ShippingAddress;
        }
        this.collectionCenter = this.aphCollectionData[0].CCL_Site__r.Name;
        this.collectionCenterId = this.aphCollectionData[0].CCL_Site__c;
      }
      if (this.infCenterData.length > 1) {
        this.infCenterRecLength = true;
      } else {
        this.infCenterRecLength = false;
        this.hasInfAddress = true;
        if (this.infCenterData[0].CCL_Site__r.ShippingAddress !== null) {
          this.infSiteAddress = this.infCenterData[0].CCL_Site__r.ShippingAddress;
        }
        this.infusionCenter = this.infCenterData[0].CCL_Site__r.Name;
        this.infusionCenterId = this.infCenterData[0].CCL_Site__c;
      }
    } else if (error) {
      this.error = error;
      this.gotresult = false;
    }
  }


  @wire(getAphPickupLocation, { therapyId: "$therapy", collectionCenter: "$collectionCenterId" })
  AphPickup({ error, data }) {
    if (data) {
      this.aphPickupData = data;
      this.siteAdd++;
      if (this.aphPickupData.length > 1) {
        this.AphPickuprecordLength = true;
      } else {
        this.AphPickuprecordLength = false;
        this.hasPickupAddress = true;
        if (this.aphPickupData[0].CCL_Site__r.ShippingAddress !== null) {
          this.aphPickupSiteAdd = this.aphPickupData[0].CCL_Site__r.ShippingAddress;
        }
        this.aphPickupLocation = this.aphPickupData[0].CCL_Site__r.Name;
        this.aphPickupLocationId = this.aphPickupData[0].CCL_Site__c;
      }
    } else if (error) {
      this.error = error;
    }
  }

  @wire(getShipToLocation, { therapyId: "$therapy", infusionCenter: "$infusionCenterId" })
  shipToLocMethod({ error, data }) {
    if (data) {
      this.shipToLocData = data;
      this.siteAdd++;
      if (this.shipToLocData.length > 1) {
        this.shipToLocRecordLength = true;
      } else {
        this.shipToLocRecordLength = false;
        this.hasShipToLocAddress = true;
        if (this.shipToLocData[0].CCL_Site__r.ShippingAddress !== null) {
          this.shipToLocSiteAdd = this.shipToLocData[0].CCL_Site__r.ShippingAddress;
        }
        this.shipToLoc = this.shipToLocData[0].CCL_Site__r.Name;
        this.shipToLocId = this.shipToLocData[0].CCL_Site__c;
      }
    }
    else if (error) {
      this.error = error;
    }
  }

  @wire(checkUserPermission)
  userPermission({ error, data }) {
    if (data) {
      this.hasPermission = data;
    }
    else if (error) {
      this.error = error;
    }
  }

  @wire(getTherapyAssociation, { therapyId: "$therapy", countryCode: "$countryCode" })
  getTherapyAssoc({ error, data }) {
    if (data) {
      this.therapyAssocData = data;
      if (this.therapyAssocData[0].CCL_Text_Above_Scheduler__c !== null) {
        this.aboveSchedulerText = this.therapyAssocData[0].CCL_Text_Above_Scheduler__c;
      }
      if (this.therapyAssocData[0].CCL_Text_Below_Scheduler__c !== null) {
        this.belowSchedulerText = this.therapyAssocData[0].CCL_Text_Below_Scheduler__c;
      }
      if (this.therapyAssocData[0].CCL_Enable_PRF_Approval__c !== undefined) {
        let approvalFlag = this.therapyAssocData[0].CCL_Enable_PRF_Approval__c;
        if (approvalFlag == "Yes") {
          this.approvalCheckboxFlow = true;
        } else {
          this.approvalCheckboxFlow = false;
        }
      }
	  if (this.therapyAssocData[0].CCL_PRF_Sites_Selection_Discrepancy_Text__c !== null) {
        this.SiteDiscrepancyText = this.therapyAssocData[0].CCL_PRF_Sites_Selection_Discrepancy_Text__c;
        if(this.SiteDiscrepancyText != undefined){
        this.sitetextavail=true;
        }else
        {
          this.sitetextavail=false;
        }
      }
    }
    else if (error) {
      this.error = error;
    }
  }


	// Called from onChange
  selectionChangeHandler(event) {
    this.rescheduleFlag = true;
    if(this.responseRecieved) {
      this.template.querySelector(".validReason").disabled = false;
      this.siteReschedule = true;
    }

    // Comman variables for picklist
    this.pickListValue = event.target.value;
    this.selectionEle = event.target.dataset.item;

    if(this.collectionCenter && this.aphPickupLocation &&
      this.infusionCenter && this.shipToLoc) {

      if (event.target.dataset.item == "aphCollection")
      {
        event.srcElement.value = this.collectionCenter; // to reatin the old value on HTML during poopup display
      }

      if (event.target.dataset.item == "aphPickupLoc")
      {
        event.srcElement.value = this.aphPickupLocation; // to reatin the old value on HTML during poopup display
      }

      if (event.target.dataset.item == "infCenter")
      {
        event.srcElement.value = this.infusionCenter; // to reatin the old value on HTML during poopup display
      }

      if (event.target.dataset.item == "shipToLoc")
      {
        event.srcElement.value = this.shipToLoc; // to reatin the old value on HTML during poopup display
      }
      this.showPrompt();
    } else {
      this.updateSelectionHandler(this.selectionEle, this.pickListValue);
    }
  }


  // funtion to handle picklist value change
  updateSelectionHandler(datasetName, selectedValue) {
    let self = this;
    if (datasetName == 'aphCollection') {
      this.collectionCenter = selectedValue;
      this.aphPickupData = null;
      this.hasPickupAddress = false;
      this.aphPickupLocation = null;
      if (this.collectionCenter == '') {
        this.hasAddress = false;
        this.hasPickupAddress = false;
        this.aphPickupData = null;
        this.collectionCenter = null;
        this.aphPickupLocation = null;
      }
      if (this.collectionCenter != '') {
        this.isRendered = true;
      }
      this.aphCollectionData.forEach(function (node) {
        if (node.CCL_Site__r.Name === self.collectionCenter && node.CCL_Site__r.ShippingAddress !== null) {
          self.hasAddress = true;
          self.collectionSiteAdd = node.CCL_Site__r.ShippingAddress;
          self.collectionCenterId = node.CCL_Site__c;
        }
      });
    }
    else if (datasetName == 'aphPickupLoc') {
      if (this.aphPickupLocation != '') {
        this.isRendered = true;
      }
      this.aphPickupLocation = selectedValue;
      if (this.aphPickupLocation == '') {
        this.hasPickupAddress = false;
        this.aphPickupLocation = undefined;
      }
      this.aphPickupData.forEach(function (node) {
        if (node.CCL_Site__r.Name === self.aphPickupLocation && node.CCL_Site__r.ShippingAddress !== null) {
          self.hasPickupAddress = true;
          self.aphPickupSiteAdd = node.CCL_Site__r.ShippingAddress;
          self.aphPickupLocationId = node.CCL_Site__c;
        }
      });
    }
    else if (datasetName == 'infCenter') {
      this.infusionCenter = selectedValue;
      this.shipToLocData = null;
      this.hasShipToLocAddress = false;
      this.shipToLoc = undefined;
      if (this.infusionCenter == '') {
        this.hasInfAddress = false;
        this.hasShipToLocAddress = false;
        this.shipToLocData = null;
        this.infusionCenter = null;
        this.shipToLoc = null;
      }
      if (this.infusionCenter != '') {
        this.isRendered = true;
      }
      this.infCenterData.forEach(function (node) {
        if (node.CCL_Site__r.Name === self.infusionCenter && node.CCL_Site__r.ShippingAddress !== null) {
          self.hasInfAddress = true;
          self.infSiteAddress = node.CCL_Site__r.ShippingAddress;
          self.infusionCenterId = node.CCL_Site__c;
        }
      });
    }
    else if (datasetName == 'shipToLoc') {
      if (this.aphPickupLocation != '') {
        this.isRendered = true;
      }
      this.shipToLoc = selectedValue;
      if (this.shipToLoc == '') {
        this.hasShipToLocAddress = false;
        this.shipToLoc = undefined;
      }
      this.shipToLocData.forEach(function (node) {
        if (node.CCL_Site__r.Name === self.shipToLoc && node.CCL_Site__r.ShippingAddress !== null) {
          self.hasShipToLocAddress = true;
          self.shipToLocSiteAdd = node.CCL_Site__r.ShippingAddress;
          self.shipToLocId = node.CCL_Site__c;
        }
      });
    }
  }

  handleChange(event) {
    this.changeReason = event.detail.value;
}

  siteValidations() {
      if (!this.collectionCenter) {
        this.template.querySelector('.colcustomError').classList.add("redBorder");
        this.template.querySelector('.colremoveError').setAttribute('style', 'display:block;');
      }
      if (!this.aphPickupLocation) {
        this.template.querySelector('.pickupcustomError').classList.add("redBorder");
        this.template.querySelector('.pickupremoveError').setAttribute('style', 'display:block;');
      }
      if (!this.infusionCenter) {
        this.template.querySelector('.infcustomError').classList.add("redBorder");
        this.template.querySelector('.infremoveError').setAttribute('style', 'display:block;');
      }
      if (!this.shipToLoc) {
        this.template.querySelector('.shipTocustomError').classList.add("redBorder");
        this.template.querySelector('.shipToremoveError').setAttribute('style', 'display:block;');
      }
  }

  onNext() {
    this.isValidationError = false;
    this.previousButtonClick = false;
    this.siteValidations();
      if (!this.aphPickupDate) {
        this.datePickerError = true;
        this.template.querySelector('.dateError').setAttribute('style', 'display:block;');
      }
      if (!this.collectionCenter || !this.aphPickupLocation ||
        !this.infusionCenter || !this.shipToLoc || !this.aphPickupDate) {
      this.isValidationError = true;
     }
    if (this.onReviewClicked) {
      this.screenName = 'Pickup_Review';
    } else {
      this.screenName = 'Pickup_Next';
    }
    const attributeChangeEvent = new FlowAttributeChangeEvent('ScreenName', this.screenName);
    this.dispatchEvent(attributeChangeEvent);

    if (!this.isValidationError && !this.hasPermission) {
      const navigateNextEvent = new FlowNavigationNextEvent();
      this.dispatchEvent(navigateNextEvent);
    }
  }


  onReview() {
    this.onReviewClicked = true;
    this.previousButtonClick = false;
    this.onNext();
  }
  onPrevious() {
    this.screenName = 'Pickup_Clinical_Back';
    const attributeChangeEvent = new FlowAttributeChangeEvent('ScreenName', this.screenName);
    this.dispatchEvent(attributeChangeEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);

  }

  getSelectedDate(event) {
    this.rescheduleFlag = true;
    this.aphPickupDate = event.detail.plannedDateReviewScreen;
    let newselectedDate = event.detail.plannedDateReviewScreen;
    if(this.responseRecieved) {
      const selectedEvent = new CustomEvent("passselecteddate", {
        detail: newselectedDate
      });
        this.dispatchEvent(selectedEvent);
      }
    if (this.aphPickupDate) {
      this.template.querySelector('.dateError').setAttribute('style', 'display:none;');
    }

    //used to pass in JSON to insert in SF
    this.plannedPickupDate = event.detail.plannedDateJSON;
    let unFormattedselectedDate = newselectedDate.split('/').reverse().join('/');
    //this.plannedPickupDateFlow used in flow for review screen
    this.plannedPickupDateFlow = this.formatDate(unFormattedselectedDate);

    let self = this;
    if (this.availableDateArray) {
    if (this.availableDateArray.includes(newselectedDate)) {
      this.showDynamicText = true;
      this.missingResponse = false;
      this.qualifiedSlots.forEach(function (node) {
        if (node.APHPickup_Date.replace(/-|\//g, "/") === newselectedDate) {
            self.plantId = node.Manufacturing_Plant;
            self.slotId = node.SlotID;
            self.valueStreamField = node.ValueStream;
          let startDate = node.Day_0_Date;
          let newStartDate = startDate.split('-').reverse().join('-');

          const formattedStartDate = new Date(newStartDate);

          // used to pass in JSON to insert in SF
          self.manufacturingDate = formattedStartDate;
          let formattedDate = newStartDate.replace(/-|\//g, "/")
          self.manufacturingStartDate = self.formatDate(formattedDate);

          // self.manufacturingStartDateFlow used in flow for review screen
          self.manufacturingStartDateFlow = self.manufacturingStartDate;
          const finishDate = node.APHPickup_Date;

          let newFinishedDate = finishDate.split('-').reverse().join('-');
          const formattedFinshedDate = new Date(newFinishedDate);

          // used to pass in JSON to insert in SF

          formattedFinshedDate.setDate(formattedFinshedDate.getDate() + parseInt(node.ValueStream,10));
          self.FinishedDate = formattedFinshedDate;
          const month = ("0" + (formattedFinshedDate.getMonth() + 1)).slice(-2);
          let endDate = formattedFinshedDate.getFullYear() + '/' + month + '/' + formattedFinshedDate.getDate();
          self.productFinishedDate = self.formatDate(endDate);

          // self.productFinishedDateFlow used in flow for review screen
          self.productFinishedDateFlow = self.productFinishedDate;
        }
      });
    } else {
      if (this.therapyType == 'Commercial' && !this.replacementOrder) {
        this.manufacturingStartDate = ToBeConfirmed;
        this.manufacturingStartDateFlow = ToBeConfirmed;
        this.showDynamicText = false;
        this.productFinishedDate = ToBeConfirmed;
        this.productFinishedDateFlow = ToBeConfirmed;
        this.missingResponse = true;
        this.plantId = null;
        this.slotId = null;
        this.valueStreamField = null;
        this.manufacturingDate = null;
        this.FinishedDate = null;
      } else if (this.therapyType == 'Clinical' && !this.replacementOrder) {
        this.showDynamicText = false;
        this.manufacturingStartDate = NotAvailable;
        this.manufacturingStartDateFlow = NotAvailable;
        this.productFinishedDate = NotAvailable;
        this.productFinishedDateFlow = NotAvailable;
        this.missingResponse = true;
        this.plantId = null;
        this.slotId = null;
        this.valueStreamField = null;
        this.manufacturingDate = null;
        this.FinishedDate = null;
      }
    }
  }
  }

  formatDate(date) {
    let mydate = new Date(date);
    let monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];

    let day = mydate.getDate();

    let monthIndex = mydate.getMonth();
    let monthName = monthNames[monthIndex];
    let year = mydate.getFullYear();
    return `${day} ${monthName} ${year}`;
  }

  // CGTU-890
  showPrompt() {
    // to open modal set isModalOpen tarck value as true
    this.schedulerPrompt = true;
  }
  // CGTU-890
  changeScheduler() {
     this.schedulerPrompt = false;

    if (this.selectionEle == "aphCollection")
    {
      const colclass = this.template.querySelector(".colclass");
      colclass.value = this.pickListValue; //  Setting new value when clicked on Yes button on frontend
    }

    if (this.selectionEle == "aphPickupLoc")
    {
      const pickupClass = this.template.querySelector(".pickupClass");
      pickupClass.value = this.pickListValue; //  Setting new value when clicked on Yes button on frontend
    }

    if (this.selectionEle == "infCenter")
    {
      const infClass = this.template.querySelector(".infClass");
      infClass.value = this.pickListValue; //  Setting new value when clicked on Yes button on frontend
    }

    if (this.selectionEle == "shipToLoc")
    {
      const shiptoLoc = this.template.querySelector(".shiptoClass");
      shiptoLoc.value = this.pickListValue; //  Setting new value when clicked on Yes button on frontend
    }
    this.updateSelectionHandler(this.selectionEle, this.pickListValue); // Setting new value on backend when clicked on Yes button
  }
  // CGTU-890
  doNotChangeScheduler() {
    this.schedulerPrompt = false;
  }

  @api
  submitReschedule(validReasonError) {
    this.errorOnUpdate = false;
    let validError = false;
    let isinvalidloc = false;
    this.siteValidations();
    let fieldVal = this.template.querySelector('.validReason');
    if(!this.changeReason && this.siteReschedule) {
      fieldVal.reportValidity();
      validError = true;
    }
    if (!this.collectionCenter || !this.aphPickupLocation ||
      !this.infusionCenter || !this.shipToLoc) {
      isinvalidloc = true;
    }
    if(this.rescheduleFlag) {
    this.oderUpdateDetails=
        {
            "CCL_Apheresis_Collection_Center__c":this.collectionCenterId,
            "CCL_Infusion_Center__c" :this.infusionCenterId,
            "CCL_Apheresis_Pickup_Location__c" : this.aphPickupLocationId,
            "CCL_Ship_to_Location__c" : this.shipToLocId,
            "CCL_Planned_Apheresis_Pick_up_Date_Time__c":this.plannedPickupDate,
            "CCL_Estimated_Manufacturing_Start_Date__c":this.manufacturingDate,
            "manufacturingStartDateFlow":this.manufacturingStartDate,
            "CCL_Estimated_FP_Delivery_Date__c": this.FinishedDate,
            "productFinishedDateFlow": this.productFinishedDate,
            "CCL_Scheduler_Missing__c":this.missingResponse,
            "CCL_Reschedule__c": this.rescheduleFlag,
            "aphShipmentId" : this.aphShipmentId,
            "CCL_TECH_Change_Reason__c" : this.changeReason,
            "CCL_Reschedule_Reason__c" : this.rescheduleReason,
            "CCL_Manufacturing_Slot_ID__c" : this.slotId,
            "CCL_Plant__c" : this.plantId,
            "CCL_Value_Stream__c" : this.valueStreamField
        }
        this.oderUpdateDetails.Id = this.recordId;
        if (!validError && !validReasonError && !isinvalidloc) {
          this.loadSpinner = true;
        updateRescheduleOrder({ result: this.oderUpdateDetails })
        .then((result) => {
        if(result=="Success") {
          this.loadSpinner = false;
            const submitRescheduleOrder = new CustomEvent("submitorder");
            this.dispatchEvent(submitRescheduleOrder);
        }
        })
        .catch((error) => {
        this.error = error;
        this.loadSpinner = false;
        this.errorOnUpdate = error.body.message;

        });
      }
} else {
  const submitRescheduleOrder = new CustomEvent("submitorder");
  this.dispatchEvent(submitRescheduleOrder);
}
  }

  splitSubjectId(subjectId) {
    if(subjectId) {
      let strLst = subjectId.split('_');
      if(strLst.length > 2) {
        this.subId_Center = strLst[1];
        this.subId_last = strLst[2];
      }
    }
  }
  getAddresses(){
    this.addLength = this.addressIds.length;
    getAddress({
      accountIds: this.addressIds,
    })
      .then((result) => {
        this.addressList=result;
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }
}

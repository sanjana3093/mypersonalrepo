import { LightningElement, api, track, wire } from "lwc";
import { getRecord, getFieldValue, updateRecord } from "lightning/uiRecordApi";
import {FlowNavigationNextEvent, FlowAttributeChangeEvent} from "lightning/flowSupport";
import ADF_OBJ from "@salesforce/schema/CCL_Apheresis_Data_Form__c";
import Volume_of_blood_processed_FIELD from "@salesforce/schema/CCL_Summary__c.CCL_Volume_of_blood_processed__c";
import Collection_Center_FIELD from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__r.Name";
import Apheresis_Pickup_Location_FIELD from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Pickup_location__r.Name";
import Collection_Center_FIELD_shippingStreet from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__r.ShippingStreet";
import Collection_Center_FIELD_shippingCity from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__r.ShippingCity";
import Collection_Center_FIELD_shippingState from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__r.ShippingState";
import Collection_Center_FIELD_shippingPostalCode from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__r.ShippingPostalCode";
import Collection_Center_FIELD_shippingCountry from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__r.ShippingCountry";
import Pickup_shippingStreet from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Pickup_location__r.ShippingStreet";
import Pickup_shippingCity from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Pickup_location__r.ShippingCity";
import Pickup_shippingState from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Pickup_location__r.ShippingState";
import Pickup_shippingPostalCode from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Pickup_location__r.ShippingPostalCode";
import Pickup_shippingCountry from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Pickup_location__r.ShippingCountry";
import CCL_Status__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Status__c";
import Other_FIELD from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Other__c";
import Collection_Cryopreservation_Comments_FIELD from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Collection_Cryopreservation_Comments__c";
import Apheresis_System_Used_FIELD from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_System_Used__c";
import Adf_Lookup from "@salesforce/schema/CCL_Summary__c.CCL_Apheresis_Data_Form__c";
import saveCollections from "@salesforce/apex/CCL_ADF_Controller.saveCollections";
import deleteCollections from "@salesforce/apex/CCL_ADFController_Utility.deleteCollections";
import saveAdfData from "@salesforce/apex/CCL_ADF_Controller.saveAdfData";
import CCL_Field_Required from "@salesforce/label/c.CCL_Field_Required";
import CCL_Apheresis_Other_Field_Error from "@salesforce/label/c.CCL_Apheresis_Other_Field_Error";
import CCL_Adf_System_Used_Other from "@salesforce/label/c.CCL_Adf_System_Used_Other";
import futureTimeError from "@salesforce/label/c.CCL_Future_CollectionTime_Error";
import CCL_Order__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Order__c";
import saveForLater from "@salesforce/apex/CCL_ADF_Controller.saveForLater";
import { NavigationMixin,CurrentPageReference } from "lightning/navigation";
import CCL_Collection from "@salesforce/label/c.CCL_Collection_label";
import CCL_Aph_Id from "@salesforce/label/c.CCL_Apheresis_ID_DIN_SEC";
import CCL_Vol from "@salesforce/label/c.CCL_Volume_of_blood";
import CCL_coll_Date from "@salesforce/label/c.CCL_End_of_Apheresis_Coll_Date";
import CCL_coll_Time from "@salesforce/label/c.CCL_End_of_Apheresis_Coll_Time";
import CCL_coll_Center from "@salesforce/label/c.CCL_Apheresis_Collection_Center";
import CCL_pickup_Center from "@salesforce/label/c.CCL_Apheresis_Pickup_Location";
import CCL_aph_sys from "@salesforce/label/c.CCL_Apheresis_System_Used";
import CCL_other from "@salesforce/label/c.CCL_Other";
import CCL_Aph_Comments from "@salesforce/label/c.CCL_Aph_Comments";
import { getObjectInfo, getPicklistValues } from "lightning/uiObjectInfoApi";
//2260
import { fireEvent, registerListener } from "c/cCL_Pubsub";
import enterBothDateTime from "@salesforce/label/c.CCL_Enter_both_Date_and_Time";
import badTimeInput from "@salesforce/label/c.CCL_Enter_time_in_12_hour_format";
import CCL_Apheresis_Collection_Center__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__c";
import CCL_Label_Compliant__c from "@salesforce/schema/Account.CCL_Label_Compliant__c";
import CCL_DIN__c from "@salesforce/schema/CCL_Summary__c.CCL_DIN__c";
import CCL_Apheresis_ID__c from "@salesforce/schema/CCL_Summary__c.CCL_Apheresis_ID__c";
import CCL_SEC__c from "@salesforce/schema/CCL_Summary__c.CCL_SEC__c";
import CCL_Reason_Modal_Header from "@salesforce/label/c.CCL_Reason_Modal_Header";
import CCL_Reason_Modal_Message from "@salesforce/label/c.CCL_Reason_Modal_Message";
import CCL_Reason_Modal_Helptext from "@salesforce/label/c.CCL_Reason_Modal_Helptext";
import CCL_Reason_Modal_Subheading from "@salesforce/label/c.CCL_Reason_Modal_Subheading";
import reasonForModification from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Reason_For_Modification__c";
import CCL_Approval_Counter__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Approval_Counter__c";
import ID from "@salesforce/schema/CCL_Apheresis_Data_Form__c.Id";
import volOfBloodHelpText from "@salesforce/label/c.CCL_Volume_of_blood_processed_Help_Text";
import futureDateError from "@salesforce/label/c.CCL_CollectionEndDateError_c";//added as part of 239
import getTimezoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getTimeZoneDetails";
import getFormattedDateTimeDetails from "@salesforce/apex/CCL_ADFController_Utility.getFormattedDateTimeDetails";
import getPageTimeZoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getPageTimeZoneDetails";
import CCL_PRF_Resubmission_Approval from "@salesforce/label/c.CCL_PRF_Resubmission_Approval";
import CCL_Req_Collection_Detls from "@salesforce/label/c.CCL_Req_Collection_Detls";
import CCL_Pattern_Validation_error_message from "@salesforce/label/c.CCL_Pattern_Validation_error_message";
import CCL_255_Char_Limit from "@salesforce/label/c.CCL_255_Char_Limit";
import CCL_Aph_Collections from "@salesforce/label/c.CCL_Aph_Collections";

import CCL_AphId_Err_Msg from "@salesforce/label/c.CCL_AphId_Err_Msg";
import CCL_DIN_Err_Msg from "@salesforce/label/c.CCL_DIN_Err_Msg";
import CCL_TimeZone from "@salesforce/label/c.CCL_TimeZone";
import CCL_Action from "@salesforce/label/c.CCL_Action";
import CCL_VolofBlood_Err_msg from "@salesforce/label/c.CCL_VolofBlood_Err_msg";
import CCL_Add_Another_Collection from "@salesforce/label/c.CCL_Add_Another_Collection";
import CCL_Back from "@salesforce/label/c.CCL_Back";
import CCL_Save_Changes from "@salesforce/label/c.CCL_Save_Changes";
import CCL3_Approved from "@salesforce/label/c.CCL3_Approved";
import CCL_Delete from "@salesforce/label/c.CCL_Delete";
import CCL_Loading from "@salesforce/label/c.CCL_Loading";
import CCL_Warning from "@salesforce/label/c.CCL_Warning";
import CCL_Modal_Close from "@salesforce/label/c.CCL_Modal_Close";
import CCL_Submit from "@salesforce/label/c.CCL_Submit";
import CCL_Cancel_Button from "@salesforce/label/c.CCL_Cancel_Button";
import getOrderApprovalInfo from "@salesforce/apex/CCL_ADFController_Utility.getOrderApprovalInfo";
import getAddress from "@salesforce/apex/CCL_Utility.fetchAddress";
import CCL_Collection_Create_Err from "@salesforce/label/c.CCL_Collection_Create_Err";
const fields = [
  Collection_Center_FIELD, Collection_Center_FIELD_shippingStreet, Collection_Center_FIELD_shippingCity,
  Collection_Center_FIELD_shippingState, Collection_Center_FIELD_shippingPostalCode, Collection_Center_FIELD_shippingCountry,
  Pickup_shippingStreet, Pickup_shippingCity, Pickup_shippingPostalCode, Apheresis_Pickup_Location_FIELD, Pickup_shippingState,
  Pickup_shippingCountry, Other_FIELD, Collection_Cryopreservation_Comments_FIELD, Apheresis_System_Used_FIELD,
  CCL_Apheresis_Collection_Center__c,CCL_Status__c
]; //AccountId added as part of 272
const END_OF_APHERESIS_COLLECTION_TIME = "CCL_TEXT_Aph_Collection_End_Date_Time__c";
const SITE_LOCATION = "CCL_Site__c";
export default class CCL_Required_Collection_Details extends NavigationMixin(
  LightningElement
) {
	 showCollectionCreateErr;
  @api recordId;
  @wire(CurrentPageReference) pageRef;
  @track collectionList = [];
  @track adfList = [];
  @track index = 0;
  @api tableData;
  @api collectiionData;
  @api labelCompliantApi;
  @api allCollection;
  @track otherValue;
  @track other = Other_FIELD;
  @track collectionComments = Collection_Cryopreservation_Comments_FIELD;
  @track systemUsed;
  @track badTimeInput=badTimeInput;
  @track volBlood = Volume_of_blood_processed_FIELD;
  @track lookupField = Adf_Lookup;
  @track disabledbtn = false;
  @track isLoading = false;
  @track test=[];
  @track isOther = false;
  @track EMPTY_STRING = "";
  @track collAddress;
  @api navigateFurther = false;
  @api selectVal;
  @api fieldValidations;
  @track prevVal = [];
  @api apheresisIdLabel;
  @track accountId;
  @track ApheresisId=CCL_Apheresis_ID__c;
  @track din=CCL_DIN__c;
  @track sec=CCL_SEC__c;
   @api isADFViewerInternal;
  @api isApheresisId=false;
  @api isDin=false;
  @api isSec=false;
  @api myJson; // this will contain only the fields of current page
  @api jsonObj = {}; // this will contain the entire json structure of all the pages if any data saved
  @api flowScreen; //the screen name received from the flow
  @api jsonData; // the json data in string received from flow
  @api comments;
  @track commentValue;
  @api screenName;
  @api UpdatedScreenName;
  //Changes End
  @track labels = {
    CCL_Collection,
    CCL_Aph_Id,
    CCL_Vol,
    CCL_coll_Date,
    CCL_coll_Time,
    CCL_coll_Center,
    CCL_pickup_Center,
    CCL_aph_sys,
    CCL_other,
    CCL_Aph_Comments,
    volOfBloodHelpText,
    CCL_Reason_Modal_Header,
    CCL_Reason_Modal_Message,
    CCL_Reason_Modal_Helptext,
    CCL_Reason_Modal_Subheading,
    CCL_PRF_Resubmission_Approval,
    CCL_Apheresis_Other_Field_Error,
    CCL_Req_Collection_Detls,
    CCL_Pattern_Validation_error_message,
    CCL_255_Char_Limit,
    CCL_Aph_Collections,
    CCL_AphId_Err_Msg,
    CCL_DIN_Err_Msg,
    CCL_TimeZone,
    CCL_Action,
    CCL_VolofBlood_Err_msg,
    CCL_Add_Another_Collection,
    CCL_Back,
    CCL_Save_Changes,
    CCL3_Approved,
    CCL_Delete,
    CCL_Loading,
    CCL_Warning,
    CCL_Modal_Close,
    CCL_Submit,
    CCL_Cancel_Button,
	CCL_Collection_Create_Err
  };
  @track newArr;
  @track hasRendered;
  @track validValues = 1;
  @track commChanged = 0;
  @track otherChanged = 0;
  @track mydate;
  @track screennameVal="Required Collection Details";
  @api allTimeZoneDetails;
  @track siteTimeZone;
  @api timeZone;
  @api timeZoneVal;
  @api formattedDateTime;
  @track myDateFields=[];
  @track myTimeFields=[];
  @track isReasonModalOpen=false;
  @track ReasonForMod;
  @track reason;
  @track navigate;
  @track counter;
  @track fromNext=false;
  @track fromPrev = false;
  @track fromSave=false;
  @track orderId;
  @track prfApprovalRequired;
  @track prfResubmission;
  @track prfStatus;
   //2450
@api addressIds =[];
collectionId;
pickupId;
@api addressList;
  @track prfWarningMessage= false;				  
  @wire(getRecord, { recordId: "$recordId", fields:[reasonForModification,CCL_Approval_Counter__c] })
  Reason({ error, data }) {
    if (error) {
      this.error = error;
      
    } else if (data) {
      this.reason = data.fields.CCL_Reason_For_Modification__c.value;
      this.counter=data.fields.CCL_Approval_Counter__c.value;
      if(this.counter===null){
        this.counter=0;
      }
    }
  };
  @track adfData = {
    Id: "",
    CCL_Other__c: "",
    CCL_Collection_Cryopreservation_Comments__c: "",
    CCL_Apheresis_System_Used__c: ""
  };
  @wire(getRecord, { recordId: "$recordId", fields })
  adf;
  @wire(getRecord, { recordId: "$recordId", fields: [CCL_Apheresis_Collection_Center__c, CCL_Status__c,CCL_Order__c]})
  getCollCenter({
    error,
    data
}) {
    if (error) {
      
       this.error = error ;
    } else if (data) {
      this.accountId = data.fields.CCL_Apheresis_Collection_Center__c.value;
      this.statusVal=data.fields.CCL_Status__c.value;
	  this.orderId=data.fields.CCL_Order__c.value;
      if(this.statusVal){
        fireEvent(this.pageRef, 'statusChange', this.statusVal );
      }
    }
}
  @wire(getObjectInfo, { objectApiName: ADF_OBJ })
  objectInfo;
  @wire(getPicklistValues, {
    recordTypeId: "$objectInfo.data.defaultRecordTypeId",
    fieldApiName: Apheresis_System_Used_FIELD
  }) systemUsedPicklistValues(result) {
    if (result.data) {
      this.newArr = result.data.values;
    }
  }
  @wire(getTimezoneDetails, {screenName:"$screennameVal", adfId: "$recordId" })
	wiredTimeZoneSteps({ error, data }) {
    if (data) {
        this.timeZone = data;
        this.timeZoneVal=this.timeZone[END_OF_APHERESIS_COLLECTION_TIME];
      
        this.error = null;
    } else if (error) {
        this.error = error;
        
    }
}
@wire(getOrderApprovalInfo, { recordId: "$recordId" })
  wiredAssocidatedOrderApprovalDetails({ error, data }) {
    if (data) {
      
      this.prfApprovalRequired =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_Order_Approval_Eligibility__c:this.prfApprovalRequired;
      this.prfResubmission =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Approval_Counter__c:this.prfResubmission;
      this.prfStatus =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Ordering_Status__c:this.prfStatus;
      
      if(this.prfApprovalRequired && this.prfResubmission >='1' && this.prfStatus!='PRF_Approved') {
        this.prfWarningMessage = true;
      }
      
    } else if (error) {
      this.error = error;
      
    }
  } 	 
  
  @wire(getRecord,{ recordId: "$accountId", fields:[CCL_Label_Compliant__c]})
   WiredIdType({
    error,
    data
}) {
    if (error) {
      this.error = error ;
    } else if (data) {
      if(data.fields.CCL_Label_Compliant__c.value==='ISBT'){
        this.apheresisIdLabel='DIN';
        this.isDin=true;
        this.lableCompliantApi="CCL_DIN__c";
      }
      else{
        this.apheresisIdLabel='Apheresis ID';
        this.isApheresisId=true;
        this.lableCompliantApi="CCL_Apheresis_ID__c";
      }
    }
	
}
//2450
  @wire(getRecord, { recordId: "$recordId", fields })
  wiredRecord({ error, data }) {
    if (error) {
        
       
    } else if (data) {
      this.collectionId=data.fields.CCL_Apheresis_Collection_Center__c.value;
      this.pickupId=data.fields.CCL_Apheresis_Pickup_location__r.value.id;
      this.addressIds.push(data.fields.CCL_Apheresis_Collection_Center__c.value);
      this.addressIds.push(data.fields.CCL_Apheresis_Pickup_location__r.value.id);
       //2450
       if(this.addressIds.length>0) {
        this.getAddresses();
       }
       
    }
}
  get collectionCenter() {
    return getFieldValue(this.adf.data, Collection_Center_FIELD);
  }
  get collectionCenterStreet() {
    return getFieldValue(this.adf.data, Collection_Center_FIELD_shippingStreet);
  }
  get collectionCenterAddress() {
    const shipCity = getFieldValue(
      this.adf.data,
      Collection_Center_FIELD_shippingCity
    );
    const shipState = getFieldValue(
      this.adf.data,
      Collection_Center_FIELD_shippingState
    );
    const shipPostalCode = getFieldValue(
      this.adf.data,
      Collection_Center_FIELD_shippingPostalCode
    );
    let address = "";
    if (shipCity !== null) {
      address = shipCity;
    }
    if (shipState !== null) {
      address = address + ", " + shipState;
    }
    if (shipPostalCode !== null) {
      address = address + " " + shipPostalCode;
    }
    return address;
  }
  get collectionCenterCountry() {
    return getFieldValue(
      this.adf.data,
      Collection_Center_FIELD_shippingCountry
    );
  }
  get pickupLocation() {
    return getFieldValue(this.adf.data, Apheresis_Pickup_Location_FIELD);
  }
  get pickupStreet() {
    return getFieldValue(this.adf.data, Pickup_shippingStreet);
  }
  get pickupAddress() {
    const shipCity = getFieldValue(this.adf.data, Pickup_shippingCity);
    const shipState = getFieldValue(this.adf.data, Pickup_shippingState);
    const shipPostalCode = getFieldValue(
      this.adf.data,
      Pickup_shippingPostalCode
    );
    let address = "";
    if (shipCity !== null) {
      address = shipCity;
    }
    if (shipState !== null) {
      address = address + ", " + shipState;
    }
    if (shipPostalCode !== null) {
      address = address + " " + shipPostalCode;
    }
    return address;
  }
  get pickupCountry() {
    return getFieldValue(this.adf.data, Pickup_shippingCountry);
  }
  onPrevious() {
	  //2260
    if (this.isADFViewerInternal) {
      const attributeChangeEvent = new FlowAttributeChangeEvent(
        "screenName",
        "20"
      );
      this.dispatchEvent(attributeChangeEvent);
      const navigateNextEvent = new FlowNavigationNextEvent();
      this.dispatchEvent(navigateNextEvent);
    } else {
      this.fromPrev = true;
      this.saveforLaterRecord();
    }
  }
  onNext() {
    this.validateFields();
  }
  saveAsJson() {
    if (this.jsonData == undefined || this.jsonData == null) {
      this.jsonObj[this.flowScreen] = this.myJson;
      this.jsonObj["Collection and Cryopreservation Comments"] = this.comments;
      this.jsonObj["latestScreen"] = this.flowScreen;
    } else {
      if (this.flowScreen in this.jsonObj) {
        delete this.jsonObj[this.flowScreen];
        this.jsonObj[this.flowScreen] = this.myJson;
        this.jsonObj[
          "Collection and Cryopreservation Comments"
        ] = this.comments;
        this.jsonObj["latestScreen"] = this.flowScreen;
      } else {
        this.jsonObj[this.flowScreen] = this.myJson;
        this.jsonObj[
          "Collection and Cryopreservation Comments"
        ] = this.comments;
        this.jsonObj["latestScreen"] = this.flowScreen;
      }
    }
    const attributeChangeEvent1 = new FlowAttributeChangeEvent("jsonData",JSON.stringify(this.jsonObj));
    this.dispatchEvent(attributeChangeEvent1);
    //2260
    if(!this.isADFViewerInternal){
      saveForLater({
        myJSON: JSON.stringify(this.jsonObj),
        recordId: this.recordId
      })
        .then((result) => {
          //updating the save json varible
          this.error = null;
        })
        .catch((error) => {
          
          this.error = error;
        });
    }
  }
  goToListScreen(){
   this[NavigationMixin.Navigate]({
      type: "comm__namedPage",
      attributes: {
        name: "Home"
      },
      state: {
        "tabset-8e13c": "4d03f"
      }
    });
  }
  onSave(event) {
    this.fromSave=true;
    this.saveforLaterRecord();
  }
  collection = {
    CCL_DIN__c: this.din,
    CCL_SEC__c: this.sec,
    CCL_Apheresis_ID__c: this.ApheresisId,
    CCL_Volume_of_blood_processed__c: this.volBlood,
    CCL_End_of_Apheresis_Collection_Date__c: '',
    CCL_End_of_Apheresis_Collection_Time__c: '',
    CCL_Apheresis_Data_Form__c: this.recordId,
    key: "0",
    CCL_Order_Collection__c:this.orderId,
    CCL_External_Identification__c:""
  };
  connectedCallback() {
    this.isLoading = true;
	//  2260
    registerListener("gotoSummary", this.gotoADFSummary, this);
    this.collectionList.push(JSON.parse(JSON.stringify(this.collection)));
    this.isLoading = false;
    
    if (this.jsonData != undefined || this.jsonData != null) {
      this.jsonObj = JSON.parse(this.jsonData);
      this.commentValue = this.jsonObj[
        "Collection and Cryopreservation Comments"
      ];
      
    }
    if (this.jsonData) {
      let val = JSON.parse(this.jsonData);
      let mytable = val["2"];
      if (mytable) {
		  this.setValues(mytable);
      }
    }
    this.getTimeZoneScreenDetails();
  }
  //Added for picklist translation changes
  renderedCallback() {
    let self=this;
    let val = JSON.parse(this.jsonData);
    let mytable = val["2"];
    if(this.newArr!=undefined){
    this.newArr.forEach(function(node){
      if(node.label==mytable[0][0].CCL_Apheresis_System_Used__c){
        self.systemUsed=node.value;
      }
    
    });
    }
      }
  setValues(mytable){
   this.collectionList = mytable[1];
        this.systemUsed = mytable[0][0].CCL_Apheresis_System_Used__c;
        this.otherValue = mytable[0][0].CCL_Other__c;
        this.index = this.collectionList.length - 1;
        for (let i = 0; i <= this.collectionList.length - 1; i++) {
     
          let dateTimeVal=this.collectionList[i]["CCL_TEXT_Aph_Collection_End_Date_Time__c"];
          if(dateTimeVal!==undefined && dateTimeVal!==null && dateTimeVal!==''){
            let fields=dateTimeVal.split(' ');
            let timeVal=fields[3];
            let dateVal=fields[0]+' '+fields[1]+' '+fields[2];
            let timeValue=timeVal.split(":");
            this.myDateFields[i]=this.formatDate(dateVal);
            this.collectionList[i].CCL_End_of_Apheresis_Collection_Date_Val__c=dateVal;
            this.collectionList[i].CCL_End_of_Apheresis_Collection_Time__c=timeValue[0]+':'+timeValue[1];
            this.myTimeFields[i]=this.collectionList[i].CCL_End_of_Apheresis_Collection_Time__c;
          }
          let dateTimeFieldVal=this.collectionList[i]["CCL_End_of_Apheresis_Collection_Date__c"];
          if(dateTimeFieldVal!==undefined && dateTimeFieldVal!==null && dateTimeFieldVal!==''){
            if(!dateTimeFieldVal.includes('Z')){
            let fields=dateTimeFieldVal.split('+');
            this.collectionList[i].CCL_End_of_Apheresis_Collection_Date__c=fields[0]+'Z';
}
          }
          if (this.collectionList[i]["attributes"]) {
            delete this.collectionList[i]["attributes"];
            delete this.collectionList[i]["RecordTypeId"];
            this.collectionList[i].key = i;
          }
        }
}
  getTimeZoneScreenDetails(){
    getPageTimeZoneDetails({screenName:this.screennameVal})
      .then((result) => {
        this.allTimeZoneDetails = result;
        this.siteTimeZone='Time zone of '+this.allTimeZoneDetails[0][SITE_LOCATION];
      })
      .catch((error) => {
        this.error = error;
      });
  }
  addCollection() {
    this.index++;
    const i = this.index;
	 if(this.index>=4){
      this.disabledbtn = true;
    }else{
      this.disabledbtn = false;
    }
    if (this.collectionList.length < 5) {
      this.collection.key = i;
      this.collectionList.push(JSON.parse(JSON.stringify(this.collection)));
    } else {
      this.disabledbtn = true;
      this.index=this.index-1;
    }
  }
  removeCollection(event) {
    this.isLoading = true;
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    if (this.collectionList.length > 1) {
		if(this.collectionList[key].Id){
        let len = this.prevVal.length;
      this.prevVal[len]={};
      this.prevVal[len].Id=this.collectionList[key].Id;
      }
      this.collectionList.splice(key, 1);
      this.myDateFields.splice(key, 1);
      this.myTimeFields.splice(key, 1);
      this.index--;
      this.isLoading = false;
      this.disabledbtn = false;
    } else if (this.collectionList.length == 1) {
      this.collectionList = [];
      this.index = 0;
      this.isLoading = false;
    }
  }
  handleIdChange(event) {
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.collectionList[key].CCL_Apheresis_ID__c = event.target.value;
    this.collectionList[key].CCL_Apheresis_Data_Form__c = this.recordId;
    this.collectionList[key].CCL_SEC__c = null;
    this.collectionList[key].CCL_DIN__c = null;
    this.collectionList[key].CCL_Order_Collection__c = this.orderId;
    this.collectionList[key].CCL_External_Identification__c = this.recordId+'_'+key;
  }
  handleSecChange(event) {
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.collectionList[key].CCL_SEC__c = event.target.value;
    this.collectionList[key].CCL_Apheresis_Data_Form__c = this.recordId;
    this.collectionList[key].CCL_DIN__c = null;
    this.collectionList[key].CCL_Apheresis_ID__c = null;
    this.collectionList[key].CCL_Order_Collection__c = this.orderId;
    this.collectionList[key].CCL_External_Identification__c = this.recordId+'_'+key;
  }
  handleDinChange(event) {
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.collectionList[key].CCL_DIN__c = event.target.value;
    this.collectionList[key].CCL_Apheresis_Data_Form__c = this.recordId;
    this.collectionList[key].CCL_SEC__c = null;
    this.collectionList[key].CCL_Apheresis_ID__c = null;
    this.collectionList[key].CCL_Order_Collection__c = this.orderId;
    this.collectionList[key].CCL_External_Identification__c = this.recordId+'_'+key;
  }
  handleVolChange(event) {
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.collectionList[key].CCL_Volume_of_blood_processed__c =
      event.target.value;
  }
  handleDateChange(event) {
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.collectionList[key].CCL_End_of_Apheresis_Collection_Date_Val__c =
      event.target.value;
    this.myDateFields[key]=this.formatDate(event.target.value);
    let dateInputs=this.template.querySelectorAll('.dateField');
    dateInputs.forEach(function (node){
      node.value='';
      });
	   if(this.collectionList[key].CCL_End_of_Apheresis_Collection_Time__c!=undefined&&this.collectionList[key].CCL_End_of_Apheresis_Collection_Time__c!=' '){
      this.collectionList[key].CCL_TEXT_Aph_Collection_End_Date_Time__c=
      this.formatDate(this.collectionList[key].CCL_End_of_Apheresis_Collection_Date_Val__c)+" "+this.collectionList[key].CCL_End_of_Apheresis_Collection_Time__c+' '+this.timeZoneVal;
      let dateTimeval =
    this.collectionList[key].CCL_End_of_Apheresis_Collection_Date_Val__c +
    " " +
    this.collectionList[key].CCL_End_of_Apheresis_Collection_Time__c+":00";
  this.getFormattedDateTimeVal(key, dateTimeval, this.timeZoneVal);
    }
  }
  handleTimeChange(event) {
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.collectionList[key].CCL_End_of_Apheresis_Collection_Time__c =
      event.target.value;
      this.myTimeFields[key]=event.target.value;
    this.collectionList[key].CCL_TEXT_Aph_Collection_End_Date_Time__c=
	this.formatDate(this.collectionList[key].CCL_End_of_Apheresis_Collection_Date_Val__c)+" "+this.collectionList[key].CCL_End_of_Apheresis_Collection_Time__c+' '+this.timeZoneVal;
    let dateTimeval=this.collectionList[key].CCL_End_of_Apheresis_Collection_Date_Val__c+' '+this.collectionList[key].CCL_End_of_Apheresis_Collection_Time__c;
    if(this.collectionList[key].CCL_End_of_Apheresis_Collection_Date_Val__c && this.collectionList[key].CCL_End_of_Apheresis_Collection_Time__c){
    this.getFormattedDateTimeVal(key,dateTimeval,this.timeZoneVal);}
}
handleBlur(event){
  const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.collectionList[key].CCL_End_of_Apheresis_Collection_Time__c =
      event.target.value;
      this.myTimeFields[key]=event.target.value;
      let timeInputs=this.template.querySelectorAll('.timeField');
      timeInputs.forEach(function (node){
        node.value='';
        });
}
formatDate(date) {
  if(date!==null && date!==''&& date!==undefined){
   let mydate = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    mydate=new Date(date);
  }
  let monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  let day = '' + mydate.getDate();
  if (day.length < 2){
	day = '0' + day;
  }
  let monthIndex = mydate.getMonth();
  let monthName = monthNames[monthIndex];
  let year = mydate.getFullYear();
  return [day, monthName,year].join(' ');
}
return '';
}
formatDateFormat(date) {
  if(date!==null && date!==''&& date!==undefined){
   let mydate = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    mydate=new Date(date);
  }
  let day = mydate.getDate();
  let monthIndex = mydate.getMonth();
  let year = mydate.getFullYear();
  return `${year}-${monthIndex+1}-${day}`;
}
return '';
}
getFormattedDateTimeVal(key,dateTimeVal,timeZoneVal){
  let formattedVal='';
  getFormattedDateTimeDetails({dateTimeVal:dateTimeVal,timeZoneVal:timeZoneVal})
    .then((result) => {
      this.formattedDateTime = result;
      this.collectionList[key].CCL_End_of_Apheresis_Collection_Date__c=this.formattedDateTime;
    })
    .catch((error) => {
      this.error = error;
      
    });
    return formattedVal;
}
  saveRecord() {
    if (this.adfData.CCL_Apheresis_System_Used__c === "") {
      this.adfData.CCL_Apheresis_System_Used__c=this.systemUsed;
    }
    if (this.adfData.CCL_Collection_Cryopreservation_Comments__c === "" && this.commChanged===0) {
      this.adfData.CCL_Collection_Cryopreservation_Comments__c=this.commentValue;
      this.comments=this.commentValue;
    }
    if (this.adfData.CCL_Other__c === "" && this.otherChanged===0) {
      this.adfData.CCL_Other__c=this.otherValue;
    }
    this.adfData.Id = this.recordId;
   //2260
    if (!this.isADFViewerInternal) {
      saveAdfData({ adfData: this.adfData })
      .then((result) => {})
      .catch((error) => {
        this.message = "";
        this.error = error;
      });
    if (this.prevVal.length > 0) {
      deleteCollections({ delList: this.prevVal })
        .then((result) => {})
        .catch((error) => {
          this.message = "";
          this.error = error;
        });
    }
    }
      this.setDateTimeTextFields();
  
    this.allCollection = this.collectionList;
    const attributeChangeEvent = new FlowAttributeChangeEvent(
      "allCollection",
      this.allCollection
    );
    this.dispatchEvent(attributeChangeEvent);
     //2260
    if (!this.isADFViewerInternal) {
      saveCollections({ collList: this.collectionList })
        .then((result) => {
          this.message = result;
          this.error = "";
          this.tableData = result;
          this.collectiionData = result;
          const attributeChangeEvent2 = new FlowAttributeChangeEvent(
            "collectiionData",
            this.collectiionData
          );
          this.dispatchEvent(attributeChangeEvent2);
          const allValid = [
            ...this.template.querySelectorAll("lightning-input")
          ].reduce((validSoFar) => {
            return validSoFar;
          }, true);
          this.myJson = "[[".concat(
            JSON.stringify(this.adfData),
            "],",
            this.tableData,
            "]"
          );
          this.myJson = JSON.parse(this.myJson);
          this.saveAsJson();
          
          if (this.navigateFurther && allValid) {
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
          }
        })
        .catch((error) => {
			this.message = "";
			this.showCollectionCreateErr=true;
			console.log("error in required coll is" + JSON.stringify(error));
			this.error = error;
      });
    } else {
      this.navigateToNextForViewer();
    }
  }
  setDateTimeTextFields(){
    this.collectionList.forEach(function (node) {
      delete node["CCL_End_of_Apheresis_Collection_Time__c"];
      delete node["CCL_End_of_Apheresis_Collection_Date_Val__c"];
    });
  }
  saveforLaterRecord() {
    if (this.adfData.CCL_Apheresis_System_Used__c === "") {
      this.adfData.CCL_Apheresis_System_Used__c=this.systemUsed;
    }
    if (this.adfData.CCL_Collection_Cryopreservation_Comments__c === "" && this.commChanged===0) {
      this.adfData.CCL_Collection_Cryopreservation_Comments__c=this.commentValue;
      this.comments=this.commentValue;
    }
    if (this.adfData.CCL_Other__c === "" && this.otherChanged===0) {
      this.adfData.CCL_Other__c=this.otherValue;
    }
    this.adfData.Id = this.recordId;
    this.myJson = "[[".concat(
      JSON.stringify(this.adfData),
      "],",
      JSON.stringify(this.collectionList),
      "]"
    );
   this.myJson = JSON.parse(this.myJson);
      let val=this.jsonData!=undefined?JSON.parse(this.jsonData):'';
      
      let mytable=val["2"];
       let tempMyJson=this.rectifyJSON(this.myJson);
      let tempmytable=mytable!=undefined?this.rectifyJSON(mytable):{};
      if((JSON.stringify(tempMyJson)!=JSON.stringify(tempmytable)||val["Collection and Cryopreservation Comments"]!=this.comments)&&this.counter>0){
         this.isReasonModalOpen=true;
         } else if(this.fromNext){
      this.navigateFurther = true;
      const attributeChangeEvent = new FlowAttributeChangeEvent(
        "screenName",
        "3"
      );
      this.dispatchEvent(attributeChangeEvent);
      this.saveRecord();
      }
      else if(this.fromPrev){
        this.saveAsJson(); //CGTU 227 Change.
    // reducing the screenName value by 1 to take it to the previous page.
    const attributeChangeEvent = new FlowAttributeChangeEvent(
      "screenName",
      "20"
    );
    this.dispatchEvent(attributeChangeEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
      } else if(this.fromSave){
        this.saveAsJson();
    this.goToListScreen();
      }
  }
  rectifyJSON(json){
    let temCollList=json[1];
    for (let i = 0; i <= temCollList.length - 1; i++) {
    let dateTimeFieldVal=temCollList[i]["CCL_End_of_Apheresis_Collection_Date__c"];
    if(dateTimeFieldVal!=undefined && dateTimeFieldVal!==''){
      if(!dateTimeFieldVal.includes('Z')){
       let fields=dateTimeFieldVal.split('+');
      temCollList[i].CCL_End_of_Apheresis_Collection_Date__c=fields[0]+'Z';
      }
    }
    if (temCollList[i]["attributes"]!=undefined) {
    if (temCollList[i]["attributes"]!=undefined&&temCollList[i]["attributes"]!=null&&temCollList[i]["attributes"]!='') {
      delete temCollList[i]["attributes"];
      delete temCollList[i]["RecordTypeId"];
      temCollList[i].key = i;
    }
  }
    if(temCollList[i]["CCL_End_of_Apheresis_Collection_Date_Val__c"]!=null
    ||temCollList[i]["CCL_End_of_Apheresis_Collection_Date_Val__c"]!=undefined||temCollList[i]["CCL_End_of_Apheresis_Collection_Date_Val__c"]!=''){
      delete temCollList[i]["CCL_End_of_Apheresis_Collection_Date_Val__c"];
      delete temCollList[i]["CCL_End_of_Apheresis_Collection_Time__c"];
    }
    delete temCollList[i]["Id"];
    delete temCollList[i]["CCL_Apheresis_Data_Form__c"];
  

  json[1]=temCollList;
  if(json[0][0].Id!=undefined&&json[0][0].Id!=null&&json[0][0].Id!=undefined){
    delete json[0][0].Id;
  }
  if(json[0][0].CCL_Collection_Cryopreservation_Comments__c!=undefined&&
    json[0][0].CCL_Collection_Cryopreservation_Comments__c!=null&&json[0][0].CCL_Collection_Cryopreservation_Comments__c!=undefined){
    delete json[0][0].CCL_Collection_Cryopreservation_Comments__c;
  }
  return json;
  }
  value = null;
  }
  handleSystemChange(event) {
     let self=this;
     this.newArr.forEach(function(node){
      if(node.value==event.target.value){
        self.adfData.CCL_Apheresis_System_Used__c = node.label;
      }
    });
    this.value = event.detail.value;
    this.selectVal = event.target.value;
    this.isOther = false;
    const inputCmp = this.template.querySelector(".other");
    const inputValue = inputCmp.value;
    if (
      this.selectVal === CCL_Adf_System_Used_Other &&
      inputValue === this.EMPTY_STRING
    ) {
      this.isOther = true;
      inputCmp.focus();
    }
  }
   // This method is being used to check the basic field validations
   validateFields() {
    const comboCmp = this.template.querySelector(".adfSysUsed");
    const inputCmp = this.template.querySelector(".other");
    const comValue = comboCmp.value;
    this.fieldValidations = this.template.querySelectorAll("lightning-input");
    const validatefields = this.template.querySelectorAll(".validate");
    let count = 0;
    this.fieldNullCheck(comboCmp, CCL_Field_Required);
    for (let i = 0; i < this.fieldValidations.length; i++) {
      if (
        this.fieldValidations[i].name !== this.labels.CCL_coll_Center &&
        this.fieldValidations[i].name !== this.labels.CCL_pickup_Center &&
        this.fieldValidations[i].name !== this.labels.CCL_other
      ) {
        this.fieldValidations[i].setCustomValidity(this.EMPTY_STRING);
        this.fieldNullCheck(this.fieldValidations[i], CCL_Field_Required);
      }
    }
    this.validateFieldsForOther(comValue, inputCmp,comboCmp);
    for (let i = 0; i < validatefields.length; i++) {
      validatefields[i].reportValidity();
      if (validatefields[i].checkValidity()) {
        count += 1;
      }
    }
    if (count === validatefields.length) {
      this.fromNext = true
      this.saveforLaterRecord();
    } else {
      this.upDateMargin();
    }
  }
  validateFieldsForOther(comValue, inputCmp,comboCmp) {
    if (this.isOther) {
      this.fieldNullCheck(inputCmp, CCL_Apheresis_Other_Field_Error);
    } else if (comValue === CCL_Adf_System_Used_Other) {
      this.isOther = true;
      this.fieldNullCheck(inputCmp, CCL_Apheresis_Other_Field_Error);
    } else {
      inputCmp.setCustomValidity(this.EMPTY_STRING);
    }
    comboCmp.reportValidity();
    inputCmp.reportValidity();
  }
   // This method is being used to check the field null check
   fieldNullCheck(comboCmp, errorMsg) {
    let value = comboCmp.value;
    const key = comboCmp.dataset.id;
    if (comboCmp.name == "EndDate") {
      value = this.myDateFields[key];
    }
    if (comboCmp.name == "time") {
      value = this.myTimeFields[key];
    }
    if (value === this.EMPTY_STRING || value == undefined) {
      comboCmp.setCustomValidity(errorMsg);
      comboCmp.reportValidity();
      this.navigateFurther = false;
      this.validValues = 0;
    } else if (comboCmp.name == "EndDate" && comboCmp.checkValidity()) {
      this.fieldCheckDate(comboCmp,key);
    }
    else if (comboCmp.name == "time" && comboCmp.checkValidity()) {
      let currentDateTime = new Date();
      let inputDateTime = new Date();
      let currentTime = currentDateTime.getTime();
      let time = this.myTimeFields[key];
      inputDateTime.setHours(time[0], time[1], 0);
      let inputTime = inputDateTime.getTime();
      if (
        inputTime > currentTime &&
        this.mydate.getDate() === currentDateTime.getDate() &&
        this.mydate.getMonth() === currentDateTime.getMonth() &&
        this.mydate.getFullYear() === currentDateTime.getFullYear()
      ) {
        comboCmp.setCustomValidity(futureTimeError);
        comboCmp.reportValidity();
        this.navigateFurther = false;
      }
    }
    else {
      comboCmp.setCustomValidity(this.EMPTY_STRING);
    }
    this.checkforDateTime(comboCmp, key);
  }
  fieldCheckDate(comboCmp,key) {
    let date = new Date();
    this.mydate = new Date(this.myDateFields[key]);
    if (
      date < this.mydate &&
      this.myTimeFields[key] != undefined &&
      this.myTimeFields[key] != " "
    ) {
      comboCmp.setCustomValidity(futureDateError);
      comboCmp.reportValidity();
      this.navigateFurther = false;
    } else {
      comboCmp.setCustomValidity(this.EMPTY_STRING);
      comboCmp.reportValidity();
    }
  }
  checkforDateTime(comboCmp, key) {
    if (comboCmp.type == "date" || comboCmp.type == "time") {
      if (
        this.myTimeFields[key] == undefined &&
        this.myDateFields[key] != undefined &&
        comboCmp.type == "time"
      ) {
        comboCmp.setCustomValidity(enterBothDateTime);
        comboCmp.reportValidity();
        this.navigateFurther = false;
      }
      if (
        this.myTimeFields[key] != undefined &&
        this.myDateFields[key] == undefined &&
        comboCmp.type == "date"
      ) {
        comboCmp.setCustomValidity(enterBothDateTime);
        comboCmp.reportValidity();
        this.navigateFurther = false;
      }
    }
    if (
      this.myTimeFields[key] != undefined &&
      this.myDateFields[key] != undefined
    ) {
      this.checkforTime(comboCmp, key);
    }
  }
  checkforTime(comboCmp, key) {
    let resultDate = new Date(
      this.collectionList[key].CCL_End_of_Apheresis_Collection_Date__c
    );
    let currentDate = new Date();
    let nowDate = new Date(currentDate.toISOString());
    if (
      resultDate.getTime() > nowDate.getTime() &&
      nowDate.getDate() == resultDate.getDate() &&
      nowDate.getMonth() == resultDate.getMonth() &&
      nowDate.getYear() == resultDate.getYear()
    ) {
      this.validateTime(comboCmp);
    } else if (nowDate < resultDate) {
      this.validateTime(comboCmp);
    } else if (comboCmp.checkValidity()) {
      comboCmp.setCustomValidity("");
      comboCmp.reportValidity();
    }
  }
  validateTime(comboCmp) {
    if (comboCmp.type == "date") {
      comboCmp.setCustomValidity(futureDateError);
      comboCmp.reportValidity();
      this.navigateFurther = false;
    }
  }
  handleOtherChange(event) {
    this.adfData.CCL_Other__c = event.target.value;
    this.otherChanged=1;
  }
  handleCommentsChange(event) {
    this.adfData.CCL_Collection_Cryopreservation_Comments__c =
      event.target.value;
      this.comments=event.target.value;
      this.commChanged = 1;
  }
  get renderdelete() {
    return this.index !== 0 ? true : false;
  }
  closeReasonModal() {
    this.isReasonModalOpen=false;
  }
  handleReasonChange(event){
    this.ReasonForMod=event.target.value;
  }
  validateTextarea(){
    const textArea=this.template.querySelector('.Reason');
    let commentValue=textArea.value;
    if(commentValue==undefined){
      this.navigater=false;
      textArea.setCustomValidity('Complete this Field');
      textArea.reportValidity();
    }else{
      this.navigate=true;
      textArea.setCustomValidity('');
      textArea.reportValidity();
    }
  }
  onReasonSubmit(event){
    this.validateTextarea();
    if(this.navigate){
    this.updateReason();}
  }
  updateReason(){
    const fields = {};
    fields[ID.fieldApiName] = this.recordId;
    fields[reasonForModification.fieldApiName] =this.ReasonForMod;
    const recordInput = { fields };
    //2260
    if (this.isADFViewerInternal) {
      if (this.fromNext) {
        this.navigateFurther = true;
        const attributeChangeEvent = new FlowAttributeChangeEvent(
          "screenName",
          "3"
        );
        this.dispatchEvent(attributeChangeEvent);
        this.saveRecord();
      } else if (this.fromPrev) {
        this.saveAsJson(); //CGTU 227 Change.
        // reducing the screenName value by 1 to take it to the previous page.
        const attributeChangeEvent = new FlowAttributeChangeEvent(
          "screenName",
          "20"
        );
        this.dispatchEvent(attributeChangeEvent);
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
      } else if (this.fromSave) {
        this.saveAsJson();
        this.goToListScreen();
      }
    } else {
      updateRecord(recordInput)
        .then(() => {
          if (this.fromNext) {
            this.navigateFurther = true;
            const attributeChangeEvent = new FlowAttributeChangeEvent(
              "screenName",
              "3"
            );
            this.dispatchEvent(attributeChangeEvent);
            this.saveRecord();
          } else if (this.fromPrev) {
            this.saveAsJson(); //CGTU 227 Change.
            // reducing the screenName value by 1 to take it to the previous page.
            const attributeChangeEvent = new FlowAttributeChangeEvent(
              "screenName",
              "20"
            );
            this.dispatchEvent(attributeChangeEvent);
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
          } else if (this.fromSave) {
            this.saveAsJson();
            this.goToListScreen();
          }
        })
        .catch((error) => {
          this.error = error;
        });
    }
      }
  chckOnBlur() {
    const validatefields = this.template.querySelectorAll(".validate");
    let count = 0;
    for (let i = 0; i < validatefields.length; i++) {
      validatefields[i].reportValidity();
      if (validatefields[i].checkValidity()) {
        count += 1;
      }
    }
    if (count != validatefields.length) {
     this.upDateMargin();
    }
  }
  upDateMargin(){
    this.fieldValidations = this.template.querySelectorAll("lightning-input");
    this.fieldValidations.forEach(function (node) {
      if (node.checkValidity()) {
        node.classList.add("marginBottom");
      } else {
        node.classList.remove("marginBottom");
      }
    });
  }
   //2450
  getAddresses(){
    getAddress({
      accountIds: this.addressIds,
    })
      .then((result) => {
        this.addressList=result;
        this.error = null;
      })
      .catch((error) => {
        
        this.error = error;
      });
  }
  
  //2260
  gotoADFSummary() {
    const screenNextEvent = new FlowAttributeChangeEvent("screenName", "6");
    this.dispatchEvent(screenNextEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }

  //2260
  navigateToNextForViewer() {
    if (this.adfData.CCL_Apheresis_System_Used__c === "") {
      this.adfData.CCL_Apheresis_System_Used__c = this.systemUsed;
    }
    if (
      this.adfData.CCL_Collection_Cryopreservation_Comments__c === "" &&
      this.commChanged === 0
    ) {
      this.adfData.CCL_Collection_Cryopreservation_Comments__c = this.commentValue;
      this.comments = this.commentValue;
    }
    if (this.adfData.CCL_Other__c === "" && this.otherChanged === 0) {
      this.adfData.CCL_Other__c = this.otherValue;
    }
    this.adfData.Id = this.recordId;
    let myCollectionList = [];
    this.collectionList.forEach((element) => {
      let attr = { attributes: { type: "CCL_Summary__c", url: "" } };
      let temp = element;
      let temp2 = Object.assign(attr, temp);
      myCollectionList.push(Object.assign(attr, temp));
    });
    this.myJson = "[[".concat(
      JSON.stringify(this.adfData),
      "],",
      JSON.stringify(myCollectionList),
      "]"
    );
    this.myJson = JSON.parse(this.myJson);
    if (this.jsonData == undefined || this.jsonData == null) {
      this.jsonObj[this.flowScreen] = this.myJson;
      this.jsonObj["Collection and Cryopreservation Comments"] = this.comments;
      this.jsonObj["latestScreen"] = this.flowScreen;
    } else {
      if (this.flowScreen in this.jsonObj) {
        delete this.jsonObj[this.flowScreen];
        this.jsonObj[this.flowScreen] = this.myJson;
        this.jsonObj[
          "Collection and Cryopreservation Comments"
        ] = this.comments;
        this.jsonObj["latestScreen"] = this.flowScreen;
      } else {
        this.jsonObj[this.flowScreen] = this.myJson;
        this.jsonObj[
          "Collection and Cryopreservation Comments"
        ] = this.comments;
        this.jsonObj["latestScreen"] = this.flowScreen;
      }
    }
    const attributeChangeEvent1 = new FlowAttributeChangeEvent(
      "jsonData",
      JSON.stringify(this.jsonObj)
    );
    this.dispatchEvent(attributeChangeEvent1);
    const attributeChangeEvent = new FlowAttributeChangeEvent(
      "screenName",
      "3"
    );
    this.dispatchEvent(attributeChangeEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }
}

import { LightningElement, track, api, wire } from "lwc";
import sendCallout from "@salesforce/apex/CCL_PRF_Controller.sendCallout";
import { getRecord } from 'lightning/uiRecordApi';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
import ORDER_OBJECT from '@salesforce/schema/CCL_Order__c';
import rescheduleReason from '@salesforce/schema/CCL_Order__c.CCL_Reschedule_Reason__c';
import fetchShipmentStatus from '@salesforce/apex/CCL_PRF_Controller.fetchShipmentStatus';
import NotAvailable from "@salesforce/label/c.CCL_Not_Available";
import ToBeConfirmed from "@salesforce/label/c.CCL_To_be_confirmed";
import interfaceFailueError from "@salesforce/label/c.CCL_InterfaceFailureError";
import rescheduleError from "@salesforce/label/c.CCL_RescheduleError";
import rescheduleReasonLabel from "@salesforce/label/c.CCL_RescheduleReason";
import checkInternalUserPermission from "@salesforce/apex/CCL_PRF_Controller.checkInternalUserPermission";


const FIELDS = [
  'CCL_Order__c.CCL_Therapy__c',
  'CCL_Order__c.CCL_Apheresis_Collection_Center__c',
  'CCL_Order__c.CCL_Apheresis_Pickup_Location__c',
  'CCL_Order__c.CCL_Infusion_Center__c',
  'CCL_Order__c.CCL_Ship_to_Location__c',
  'CCL_Order__c.CCL_Treatment_Protocol_Subject_ID__c',
  'CCL_Order__c.CCL_Ordering_Hospital__c',
  'CCL_Order__c.CCL_Planned_Cryopreserved_Apheresis_Text__c',
  'CCL_Order__c.CCL_Scheduler_Missing__c',
  'CCL_Order__c.CCL_Initial_ADF_Approved__c'
];

export default class CCLAphReschedule extends LightningElement {

  @track error;
  @api validReasonError = false;
  @track dateReschedule = false;
  @api recordId;
  @api record;
  @api interfaceFailure;
  @api shipmentRecord;
  @api rescheduleAllowed = false;
  @api collectionCenter;
  @api aphPickupLocation;
  @api infusionCenter;
  @api shipToLoc;
  @api responseRecieved = false;
  @api therapy;
  @api hospital;
  @api plannedPickupDateFlow;
  @api collectionCenterId;
  @api aphPickupLocationId;
  @api infusionCenterId;
  @api shipToLocId;
  @api manufacturingStartDate;
  @api productFinishedDate;
  @api availableDateArray;
  @api qualifiedSlots;
  @api showDynamicText;
  @api missingResponse;
  @api therapyType;
  @api rescheduleReason;
  @api countryCode;
  @api apheresisRecord;
  @api adfUpdate;
  @api aphShipmentId;
  @api userHasPermission;
  @api plantId;
  @api slotId;
  @api valueStreamField;
  splitSubject;
  subjectId;
  loadSpinner=false;

  label = { NotAvailable, ToBeConfirmed, interfaceFailueError, rescheduleError, rescheduleReasonLabel };

   connectedCallback(){
    checkInternalUserPermission()
      .then(result => {
        const hasPermission = result;
      if (hasPermission) {
        this.userHasPermission = false;
      }
      else {
        this.userHasPermission = true;
        }
      })
      .catch(error => {
        this.error = error;
      });
   }

  @wire(getObjectInfo, { objectApiName: ORDER_OBJECT })
  objectInfo;

  @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: rescheduleReason })
  rescheduleReasonValues;

  handleChange(event) {
    this.rescheduleReason = event.detail.value;
  }

  getAphShipmentData()
  {
    fetchShipmentStatus({ orderId: this.recordId})
    .then(result => {
      this.shipmentRecord = result;
      this.collectionCenter = this.shipmentRecord[0].CCL_Order_Apheresis__r.CCL_Apheresis_Collection_Center__r.Name;
      this.aphPickupLocation = this.shipmentRecord[0].CCL_Order_Apheresis__r.CCL_Apheresis_Pickup_Location__r.Name;
      this.infusionCenter = this.shipmentRecord[0].CCL_Order_Apheresis__r.CCL_Infusion_Center__r.Name;
      this.shipToLoc = this.shipmentRecord[0].CCL_Order_Apheresis__r.CCL_Ship_to_Location__r.Name;
      this.therapyType = this.shipmentRecord[0].CCL_Order_Apheresis__r.CCL_Therapy__r.CCL_Type__c;
      this.countryCode = this.shipmentRecord[0].CCL_Order_Apheresis__r.CCL_Ordering_Hospital__r.ShippingCountryCode;
      this.aphShipmentId = this.shipmentRecord[0].Id;

      if (this.shipmentRecord[0].CCL_Status__c !== 'Apheresis Pick Up Planned'
        && this.shipmentRecord[0].CCL_Status__c !== undefined) {
        this.rescheduleAllowed = true;
      } else {
        this.handleCallout();
      }
    })
    .catch(error => {
      this.error = error;
    });
  }

  cancelReschedule() {
    const closeModal = new CustomEvent("closemodal");
    this.dispatchEvent(closeModal);
  }


  @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
  orderData({ error, data }) {
    if (data && !this.record) {
      this.record = data;
      this.therapy = this.record.fields.CCL_Therapy__c.value;
      this.hospital = this.record.fields.CCL_Ordering_Hospital__c.value;
      this.collectionCenterId = this.record.fields.CCL_Apheresis_Collection_Center__c.value;
      this.infusionCenterId = this.record.fields.CCL_Infusion_Center__c.value;
      this.aphPickupLocationId = this.record.fields.CCL_Apheresis_Pickup_Location__c.value;
      this.shipToLocId = this.record.fields.CCL_Ship_to_Location__c.value;
      this.plannedPickupDateFlow = this.record.fields.CCL_Planned_Cryopreserved_Apheresis_Text__c.value;
      this.missingResponse = this.record.fields.CCL_Scheduler_Missing__c.value;
      this.adfUpdate = this.record.fields.CCL_Initial_ADF_Approved__c.value;
      this.getAphShipmentData();
      this.splitSubject = this.record.fields.CCL_Treatment_Protocol_Subject_ID__c.value;
      this.initializeSubjectId();
      } else if (error) {
        this.error = error;
      }
  }

  getSelectedDate(event) {
    this.template.querySelector(".rescheduleRsn").disabled = false;
    this.dateReschedule = true;
  }

  handleCallout() {
    this.loadSpinner = true;

    sendCallout({
      therapyId: this.record.fields.CCL_Therapy__c.value,
      aphPickupLocation: this.record.fields.CCL_Apheresis_Pickup_Location__c.value,
      shipToLoc: this.record.fields.CCL_Ship_to_Location__c.value,
      subjectId: this.subjectId
    })
      .then((result) => {
        if (result) {
          this.responseRecieved = true;
          this.responseJson = JSON.parse(result);
          if (this.responseJson.Status == "OK") {
            this.loadSpinner = false;
            this.qualifiedSlots = this.responseJson.QualifiedSlots;
            this.plantId = this.qualifiedSlots[0].Manufacturing_Plant;
            this.slotId = this.qualifiedSlots[0].SlotID;
            this.valueStreamField = this.qualifiedSlots[0].ValueStream;
            const aphStartDate = this.qualifiedSlots[0].Day_0_Date;
            let newStartDate = aphStartDate.split("-").reverse().join("-");
            this.manufacturingStartDate = this.formatDate(newStartDate);
            const finishDate = this.qualifiedSlots[0].APHPickup_Date;

            let newFinishedDate = finishDate.split('-').reverse().join('-');
            const formattedFinshedDate = new Date(newFinishedDate);

            let currentDate = new Date(
              formattedFinshedDate.getFullYear(),
              formattedFinshedDate.getMonth(),
              formattedFinshedDate.getDate() +
              parseInt(this.qualifiedSlots[0].ValueStream,10)
            );

            const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
            let endDate =
              currentDate.getFullYear() +
              "/" +
              month +
              "/" +
              currentDate.getDate();
            this.productFinishedDate = this.formatDate(endDate);
            this.availableDates = this.qualifiedSlots[0].APHPickup_Date.replace(
              /-|\//g,
              "/"
            );
            this.availableSlot = this.availableDates;
            let tempDateArray = [];
            this.qualifiedSlots.forEach(function (node) {
              tempDateArray.push(node.APHPickup_Date.replace(/-|\//g, "/"));
            });
            this.availableDateArray = tempDateArray;
            const flowMonth = this.plannedPickupDateFlow.split(" ");
            const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            const monthIndex = monthNames.indexOf(flowMonth[1]) + 1;
            const formattedDate = ("0" + (flowMonth[0])).slice(-2);
            const formattedMonth = ("0" + (monthIndex)).slice(-2);
            this.aphPickupDate = formattedDate + '/' + formattedMonth + '/' + flowMonth[2];
            if (this.availableDateArray.includes(this.aphPickupDate)) {
              this.showDynamicText = true;
            }
          } else {
            this.loadSpinner = false;
            this.interfaceFailureMethod();
          }
        } else {
          this.interfaceFailure = true;
          this.loadSpinner = false;
          this.interfaceFailureMethod();
        }
      })
      .catch((error) => {
        this.loadSpinner = false;
        this.error = error;
      });
  }

  interfaceFailureMethod() {
    if (this.therapyType == 'Commercial' && !this.replacementOrder) {
      this.manufacturingStartDate = ToBeConfirmed;
      this.manufacturingStartDateFlow = ToBeConfirmed;
      this.showDynamicText = false;
      this.productFinishedDate = ToBeConfirmed;
      this.productFinishedDateFlow = ToBeConfirmed;
      this.missingResponse = true;
      this.availableDateArray = null;
      this.availableDates = null;
      this.availableSlot = null;
      this.selectedDate = null;
      this.plantId = null;
      this.slotId = null;
      this.valueStreamField = null;
    } else if (this.therapyType == 'Clinical' && !this.replacementOrder) {
      this.showDynamicText = false;
      this.manufacturingStartDate = NotAvailable;
      this.manufacturingStartDateFlow = NotAvailable;
      this.productFinishedDate = NotAvailable;
      this.productFinishedDateFlow = NotAvailable;
      this.missingResponse = true;
      this.availableDateArray = null;
      this.availableSlot = null;
      this.selectedDate = null;
      this.availableDates = null;
      this.plantId = null;
      this.slotId = null;
      this.valueStreamField = null;
    }
  }

  formatDate(date) {
    let mydate = new Date(date);
    let monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];

    let day = mydate.getDate();

    let monthIndex = mydate.getMonth();
    let monthName = monthNames[monthIndex];
    let year = mydate.getFullYear();
    return `${day} ${monthName} ${year}`;
  }
  submitReschedule() {
    let reasonFieldVal = this.template.querySelector('.validError');
    if (!this.rescheduleReason && this.dateReschedule) {
      reasonFieldVal.reportValidity();
      this.validReasonError = true;
    } else {
      this.validReasonError = false;
    }
    const pickuplwc = this.template.querySelector('c-c-c-l_-pickup-and-delivery');
    pickuplwc.submitReschedule(this.validReasonError); 
  }
  receiveEvent() {
    const submitRescheduleOrder = new CustomEvent("submitord");
    this.dispatchEvent(submitRescheduleOrder);
  }

  initializeSubjectId() {
    if(this.splitSubject) {
      let strLst = this.splitSubject.split('_');
      if(strLst.length > 2) {
        this.subjectId = strLst[1]+'_'+strLst[2];
      }
    }
  }

}
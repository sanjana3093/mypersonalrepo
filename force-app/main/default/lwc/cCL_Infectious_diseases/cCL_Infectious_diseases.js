import { LightningElement, wire, api, track } from "lwc";
import getDisease from "@salesforce/apex/CCL_ADFController_Utility.getInfectousDiseases";
import errorMsg from "@salesforce/label/c.CCL_Verify_Disease_Err_Msg";
import { updateRecord } from "lightning/uiRecordApi";
import saveForLater from "@salesforce/apex/CCL_ADF_Controller.saveForLater";
import status from "@salesforce/label/c.CCL_ADF_Pending_Submission";
import CCL_PRF_Resubmission_Approval from "@salesforce/label/c.CCL_PRF_Resubmission_Approval";
import CCL_Req_Coll_Details from "@salesforce/label/c.CCL_Req_Collection_Detls";
import CCL_Infect_Disease_Tstng from "@salesforce/label/c.CCL_Infect_Disease_Tstng";
import CCL_Warning from "@salesforce/label/c.CCL_Warning";
import getOrderApprovalInfo from "@salesforce/apex/CCL_ADFController_Utility.getOrderApprovalInfo";
import { NavigationMixin ,CurrentPageReference} from "lightning/navigation";
//2260
import { registerListener,fireEvent } from "c/cCL_Pubsub";
import {
  FlowNavigationNextEvent,
  FlowAttributeChangeEvent
} from "lightning/flowSupport";
//tranlsation changes
import CCL_Infectious_disease_question5 from "@salesforce/label/c.CCL_Infectious_disease_question5";
import CCL_Infectious_disease_question4 from "@salesforce/label/c.CCL_Infectious_disease_question4";
import CCL_Infectious_disease_question3 from "@salesforce/label/c.CCL_Infectious_disease_question3";
import CCL_Infectious_disease_question2 from "@salesforce/label/c.CCL_Infectious_disease_question2";
import CCL_Infectious_disease_question1 from "@salesforce/label/c.CCL_Infectious_disease_question1";

export default class CCL_Infectious_diseases extends NavigationMixin(
  LightningElement
) {
  @api therapyName;
  @api recordId;
  @api screenName;
  @track isLoaded = true;
  @api allChecked;
  @api flowScreen;
  @track helpText = errorMsg;
  @track errorMsg;
  @track nextClicked = false;
  @track goNextScreen = false;
  @api jsonData;
  questionLabels = {
    CCL_Infectious_disease_question1,
    CCL_Infectious_disease_question2,
    CCL_Infectious_disease_question3,
    CCL_Infectious_disease_question4,
    CCL_Infectious_disease_question5
  };
  @track errorClass = "slds-box slds-theme--error slds-hide";
  @track recordInput;
  @track navigateNext;
  @track fields = {};
  @track myFieldsArr = [];
  @track configuredArr=[];
  @track prfApprovalRequired;
  @track prfResubmission;
  @track prfStatus;
  @track prfWarningMessage= false;
  jsonObj={};
  //2260
@wire(CurrentPageReference) pageRef;
@api isADFViewerInternal;
  @track labels = {
    CCL_PRF_Resubmission_Approval,
    CCL_Infect_Disease_Tstng,
    CCL_Req_Coll_Details,
    CCL_Warning

};
  @wire(getDisease, { therapyName: "$therapyName"  })
  wiredSteps({ error, data }) {
    if (data) {
      this.myFieldsArr = data;
      let self = this;
    
     
      this.myFieldsArr.forEach(function (node) {
        let temp={CCL_Field_API_Name__c:"",CCL_Display_Label__c:"",CCL_Order_of_field__c:'',Id:""};
          temp.CCL_Field_API_Name__c=node.CCL_Field_API_Name__c,
          temp.Id=node.Id;
          temp.CCL_Order_of_field__c=node.CCL_Order_of_field__c;
          temp.CCL_Display_Label__c=self.questionLabels[node.CCL_Display_Label__c];
          self.configuredArr.push(temp);
        
      });
      
      this.error = null;
    } else if (error) {
      
      this.error = error;
    }
  }
@wire(getOrderApprovalInfo, { recordId: "$recordId" })
  wiredAssocidatedOrderApprovalDetails({ error, data }) {
    if (data) {
      
      this.prfApprovalRequired =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_Order_Approval_Eligibility__c:this.prfApprovalRequired;
      this.prfResubmission =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Approval_Counter__c:this.prfResubmission;
      this.prfStatus =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Ordering_Status__c:this.prfStatus;
      
      if(this.prfApprovalRequired && this.prfResubmission >='1' && this.prfStatus!='PRF_Approved') {
        this.prfWarningMessage = true;
      }
    } else if (error) {
      this.error = error;
    }
  }
  
  onNext() {
    //2260
    const allChecked = [...this.template.querySelectorAll(".myInputs")].reduce(
      (validSoFar, inputCmp) => {
        return validSoFar && inputCmp.checked;
      },
      true
    );
    if (this.isADFViewerInternal) {
      if (allChecked) {
        if (this.jsonData == undefined || this.jsonData == null) {
          this.jsonObj[this.flowScreen] = this.fields;
          this.jsonObj["latestScreen"] = this.flowScreen;
          this.jsonObj["Collection and Cryopreservation Comments"]="";
        } else {
          if (this.flowScreen in this.jsonObj) {
            delete this.jsonObj[this.flowScreen];
            this.jsonObj[this.flowScreen] = this.fields;
            this.jsonObj["latestScreen"] = this.flowScreen;
            this.jsonObj["Collection and Cryopreservation Comments"]="";
          } else {
            this.jsonObj[this.flowScreen] = this.fields;
            this.jsonObj["latestScreen"] = this.flowScreen;
            this.jsonObj["Collection and Cryopreservation Comments"]="";
          }
        }
        const attributeChangeEvent1 = new FlowAttributeChangeEvent(
          "jsonData",
          JSON.stringify(this.jsonObj)
        );
        this.dispatchEvent(attributeChangeEvent1);
        const screenNextEvent = new FlowAttributeChangeEvent("screenName", "2");
        this.dispatchEvent(screenNextEvent);
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
      } else {
        this.isLoaded = false;
        this.errorMsg = errorMsg;
        this.errorClass = "slds-box slds-theme--error";
      }
    } else {
      this.nextClicked = true;

      if (allChecked) {
        const screenNextEvent = new FlowAttributeChangeEvent("screenName", "2");
        this.dispatchEvent(screenNextEvent);
        const attributeChange = new FlowAttributeChangeEvent(
          "allChecked",
          "true"
        );
        this.dispatchEvent(attributeChange);
        this.saveJSOn();
      } else {
        this.isLoaded = false;
        this.errorMsg = errorMsg;
        this.errorClass = "slds-box slds-theme--error";
      }
    }
  }

  onPrevious() {
    // previous code here
    const screenBackEvent = new FlowAttributeChangeEvent("screenName", "1");
    this.dispatchEvent(screenBackEvent);
    if (!this.isADFViewerInternal) {
      this.saveJSOn();
    } else {
      const navigateNextEvent = new FlowNavigationNextEvent();
      this.dispatchEvent(navigateNextEvent);
    }
  }
  handleChange(event) {
    this.fields[event.target.dataset.item] = event.target.checked;
    this.fields["Id"] = this.recordId;
	this.fields['CCL_Status__c']=status;
    this.recordInput = { fields: this.fields };
  }
  connectedCallback() {
	   //2260
    registerListener("gotoSummary", this.gotoADFSummary, this);
    if(this.jsonData!=undefined){
      this.jsonObj = JSON.parse(this.jsonData);
      let myValue = this.jsonObj["20"];
      if (myValue != undefined) {
        this.fields = myValue;
        this.recordInput = { fields: myValue };
      }
    }
  }
  renderedCallback() {
    let self = this;
    if (this.fields != undefined) {
      this.template.querySelectorAll(".myInputs").forEach(function (node) {
        node.checked = self.fields[node.dataset.item];
      });
    }
  }
  saveJSOn() {
    if (this.jsonData == undefined || this.jsonData == null) {
      this.jsonObj[this.flowScreen] = this.fields;
      this.jsonObj["latestScreen"] = this.flowScreen;
    } else {
      if (this.flowScreen in this.jsonObj) {
        delete this.jsonObj[this.flowScreen];
        this.jsonObj[this.flowScreen] = this.fields;
        this.jsonObj["latestScreen"] = this.flowScreen;
      } else {
        this.jsonObj[this.flowScreen] = this.fields;
        this.jsonObj["latestScreen"] = this.flowScreen;
      }
    }
    const attributeChangeEvent1 = new FlowAttributeChangeEvent(
      "jsonData",
      JSON.stringify(this.jsonObj)
    );
    this.dispatchEvent(attributeChangeEvent1);
    saveForLater({
      myJSON: JSON.stringify(this.jsonObj),
      recordId: this.recordId
    })
      .then((result) => {
        //updating the save json varible
        if (this.goNextScreen) {
          this.goToListScreen();
        } else if (this.nextClicked) {
          this.updateDetails();
        }else{
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
        }
        
        this.error = null;
      })
      .catch((error) => {
        
        this.error = error;
      });
  }
  updateDetails() {
    updateRecord(this.recordInput)
      .then(() => {
        
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
      })
      .catch((error) => {
        
      });
  }
  onSave() {
    this.goNextScreen = true;
    this.saveJSOn();
  }
  goToListScreen() {
    this[NavigationMixin.Navigate]({
      type: "comm__namedPage",
      attributes: {
        name: "Home"
      },
      state: {
        "tabset-8e13c": "4d03f"
      }
    });
  }
  //2260
  gotoADFSummary() {
    const screenNextEvent = new FlowAttributeChangeEvent("screenName", "6");
    this.dispatchEvent(screenNextEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }
}
import { LightningElement,api,wire,track } from 'lwc';
import fetchAphShipmentRecords from '@salesforce/apex/CCL_PRF_Controller.fetchAphShipmentRecords';
import getFileDetails from '@salesforce/apex/CCL_PRF_Controller.getUploadedFileDetails';
import getAddress from "@salesforce/apex/CCL_Utility.fetchAddress";
import CCL_Apheresis_Shipment_Details from "@salesforce/label/c.CCL_Apheresis_Shipment_Details";
import CCL_Status from "@salesforce/label/c.CCL_Status";
import CCL_Apheresis_Pickup_Location from "@salesforce/label/c.CCL_Apheresis_Pickup_Location";

import CCL_Date_Time from "@salesforce/label/c.CCL_Date_Time";
import CCL_Actual_Apheresis_Receipt from "@salesforce/label/c.CCL_Actual_Apheresis_Receipt";
import CCL_Waybill_number from "@salesforce/label/c.CCL_Waybill_number";
import CCL_Apheresis_Dewar_Tracking_Link_1 from "@salesforce/label/c.CCL_Apheresis_Dewar_Tracking_Link_1";
import CCL_Apheresis_Dewar_Tracking_Link_2 from "@salesforce/label/c.CCL_Apheresis_Dewar_Tracking_Link_2";
import CCL_Apheresis_Dewar_Tracking_Link_3 from "@salesforce/label/c.CCL_Apheresis_Dewar_Tracking_Link_3";
import CCL_Apheresis_Dewar_Tracking_Link_4 from "@salesforce/label/c.CCL_Apheresis_Dewar_Tracking_Link_4";
import CCL_Apheresis_Dewar_Tracking_Link_5 from "@salesforce/label/c.CCL_Apheresis_Dewar_Tracking_Link_5";
import CCL_Planned_Apheresis_Pick_Up from "@salesforce/label/c.CCL_Planned_Apheresis_Pick_Up";
import CCL_Actual_Apheresis_Pick_Up from "@salesforce/label/c.CCL_Actual_Apheresis_Pick_Up";
export default class CCLAphShipmentSummary extends LightningElement {
label= {CCL_Apheresis_Shipment_Details,
    CCL_Status,
    CCL_Apheresis_Pickup_Location,
    CCL_Date_Time,
    CCL_Actual_Apheresis_Receipt,
    CCL_Waybill_number,
    CCL_Apheresis_Dewar_Tracking_Link_1,
    CCL_Apheresis_Dewar_Tracking_Link_2,
    CCL_Apheresis_Dewar_Tracking_Link_3,
    CCL_Apheresis_Dewar_Tracking_Link_4,
    CCL_Apheresis_Dewar_Tracking_Link_5,
    CCL_Planned_Apheresis_Pick_Up,
	CCL_Actual_Apheresis_Pick_Up,
};
    @api orderId;
    @api aphShipmentList;
    @api status;
    @api aphPickUpLocation;
    @api plannedAphPickupDate;
    @api actualAphPickupDate;
    @api actualAphReceiptDate;
    @api street;
    @api aphShipmentId;
    @api dewarLink1;
    @api dewarLink2;
    @api dewarLink3;
    @api dewarLink4;
    @api dewarLink5;
    @api trackDewarLink1;
    @api trackDewarLink2;
    @api trackDewarLink3;
    @api trackDewarLink4;
    @api trackDewarLink5;
    @track filesUploaded=[];
    @api pickUpLocation;
    @api wayBill;
    @api dewarUrl;
    @api orderStatus='';
	@track pickUpLoc;


    @wire(fetchAphShipmentRecords, { orderId: "$orderId"})
    fetchAphShipmentRecords({ error, data }) {
      if (data) {
        this.aphShipmentList = data;
        this.aphShipmentId = this.aphShipmentList[0].Id;
        this.status = this.aphShipmentList[0].CCL_Status__c;
        this.aphPickUpLocation = this.aphShipmentList[0].CCL_Pick_up_Location__r.ShippingAddress;
        this.pickUpLocation=this.aphShipmentList[0].CCL_Pick_up_Location__r.Name;
        this.plannedAphPickupDate = this.aphShipmentList[0].CCL_TEXT_Planned_Cryro_Aphsersis_Pick_Up__c;
        this.actualAphPickupDate = this.aphShipmentList[0].CCL_TEXT_Actual_Pick_up_Date__c;
        this.actualAphReceiptDate = this.aphShipmentList[0].CCL_Actual_Aph_Receipt_Date_Time_Text__c;
        this.wayBill = this.aphShipmentList[0].CCL_Waybill_number__c;
        if(this.aphShipmentList[0].CCL_Dewar_Tracking_link_1__c!==undefined)
        {
           this.dewarLink1=true;
           this.trackDewarLink1=this.aphShipmentList[0].CCL_Dewar_Tracking_link_1__c;
        }
        if(this.aphShipmentList[0].CCL_Dewar_Tracking_link_2__c!==undefined)
        {
           this.dewarLink2=true;
           this.trackDewarLink2=this.aphShipmentList[0].CCL_Dewar_Tracking_link_2__c;
        }
        if(this.aphShipmentList[0].CCL_Dewar_Tracking_link_3__c!==undefined)
        {
           this.dewarLink3=true;
           this.trackDewarLink3=this.aphShipmentList[0].CCL_Dewar_Tracking_link_3__c;
        }
        if(this.aphShipmentList[0].CCL_Dewar_Tracking_link_4__c!==undefined)
        {
           this.dewarLink4=true;
           this.trackDewarLink4=this.aphShipmentList[0].CCL_Dewar_Tracking_link_4__c;
        }
        if(this.aphShipmentList[0].CCL_Dewar_Tracking_link_5__c!==undefined)
        {
           this.dewarLink5=true;
           this.trackDewarLink5=this.aphShipmentList[0].CCL_Dewar_Tracking_link_5__c;
        }
        if (this.aphPickUpLocation!== null) {
            this.getAddresses();
          }
      } else if (error) {
        this.error = error;
      }
    }
	getAddresses(){
      getAddress({
        accountIds: this.aphShipmentList[0].CCL_Pick_up_Location__c
      })
        .then((result) => {
          this.addressList=result;
          this.pickUpLoc = this.aphShipmentList[0].CCL_Pick_up_Location__c;
          this.error = null;
        })
        .catch((error) => {
          this.error = error;
        });
    }
    @wire(getFileDetails, { recordId: "$orderId" })
  wiredFileListSteps({ error, data }) {
      if (data) {
          this.filesUploaded = data;
            if(this.filesUploaded.length>0){
            this.showUploadedFiles=true;
          }
          this.error = null;
      } else if (error) {
          this.error = error;
      }
  }
viewDocument(event) {
  this.isFileViewModalOpen=true;
  this.url='/sfc/servlet.shepherd/document/download/'+event.target.name;
  window.open(this.url)
        }
 viewDewar(event) {
        this.dewarUrl=event.target.name;
        window.open(this.dewarUrl);
      }
get displayWarning() {
      return this.orderStatus == 'APH_OnHold';
    }
}
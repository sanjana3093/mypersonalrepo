import {
    LightningElement,
    track,
    api
} from 'lwc';

export default class PSP_CareProgrmProLookup extends LightningElement {
    @api selectedItem;
    @api productServicename = "";
    @api recordtype = "Product";
    @api object = "CareProgramEnrolleeProduct";
    @api searchtext = "Search a Care program Product";

    getselectedDetails(event) {
        this.selectedItem = event.detail.Id;
        this.productServicename = event.detail.Product.Name;
    }
    //validate function
    @api
    validate() {
        if (this.selectedItem !== undefined && this.selectedItem !== null) {
            return {
                isValid: true
            };
        } else {
            //If the component is invalid, return the isValid parameter as false and return an error message. 
            return {
                isValid: false,
                errorMessage: 'Please select a Care Program Product.'
            };
        }
    }
}
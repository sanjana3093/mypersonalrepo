import { LightningElement, track,api } from 'lwc';

export default class PSP_ManageProgramCatalogue extends LightningElement {

    @track bShowModal = false;
    @track selectedTab;
    @track valueInp;
    @api recordId;
    @track product="Product";

    /* javaScipt functions start */
    openModal() {
        // to open modal window set 'bShowModal' tarck value as true
        this.bShowModal = true;
    }

    closeModal() {
        // to close modal window set 'bShowModal' tarck value as false
        this.bShowModal = false;
    }

    tabselect(evt) {
        this.selectedTab = evt.target.label;
    }
    handleselected(evt){
        console.log('event fired');
        const textVal = evt.detail;
     this.valueInp = textVal;
     console.log('textVaal' + JSON.stringify(textVal));
    }
    saveMethod() {
            alert('save method invokeded123');
            this.closeModal();
        }
        /* javaScipt functions end */
        
}
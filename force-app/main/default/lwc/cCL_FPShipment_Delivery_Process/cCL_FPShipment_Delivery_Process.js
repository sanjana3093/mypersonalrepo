import { LightningElement, track, api, wire } from "lwc";
import getFPShipmentRec from '@salesforce/apex/CCL_FPShipmentController.getFinishedProductShipmentRec';
import rejectDeliveryButtonLabel from '@salesforce/label/c.CCL_Reject_Delivery_Button';
import acceptDeliveryButtonLabel from '@salesforce/label/c.CCL_Accept_Delivery_Button';

import rejectProductDeliveryLabel from '@salesforce/label/c.CCL_Reject_Product_Delivery';
import rejectEntryMessageLabel from '@salesforce/label/c.CCL_Reject_Entry_Message';
import idNotMatchLabel from '@salesforce/label/c.CCL_Id_Not_Match';
import forMoreSupportLabel from '@salesforce/label/c.CCL_For_More_Support';
import acceptProductDeliveryLabel from '@salesforce/label/c.CCL_Accept_Product_Delivery';
import acceptEntryMessageLabel from '@salesforce/label/c.CCL_Accept_Entry_Message';
import next from '@salesforce/label/c.CCL_FOOTER_NEXT';
import cancel from '@salesforce/label/c.CCL_Cancel_Button';
import close from '@salesforce/label/c.CCL_Modal_Close';

import getCheckUserPermission from '@salesforce/apex/CCL_ADFController_Utility.checkUserPermission';

import hasPrimaryFPShipmentFlow from '@salesforce/customPermission/CCL_Primary_FP_Shipment_Flow';

const internalAdfViewer = 'CCL_ADF_Viewer_Internal';

export default class cCL_FPShipment_Delivery_Process extends LightningElement {
    @track showRejectButton = false;
    @api recordId;
    @track finishedProductShipmentRec;
    @track showRejectContent;
    @track showAcceptContent;
    @track aphId;
    @track showErrorMsg = false;
    @api AphIdDINSECIdValue;
    @track showRejectProcessForm = false;
    @track showAcceptProcessForm = false;
    @track captureUserAccess;
    @track matchFound=false;
    disableAcceptRejectButtons = false;

    label={
        rejectDeliveryButtonLabel,
        acceptDeliveryButtonLabel,
        rejectProductDeliveryLabel,
        rejectEntryMessageLabel,
        idNotMatchLabel,
        forMoreSupportLabel,
        acceptProductDeliveryLabel,
        acceptEntryMessageLabel,
        next,
        cancel,
        close
    };

    get isPrimaryFPShipmentFlow() {
        return hasPrimaryFPShipmentFlow;
    }

    openRejectModal() {
        // to open modal set isModalOpen tarck value as true
        this.showRejectContent = true;
        this.showAcceptContent = false;
        this.showErrorMsg = false;
    }
    openAcceptModal(){
        this.showAcceptContent = true;
        this.showRejectContent = false;
        this.showErrorMsg = false;
    }
    closeModal(){
        this.showAcceptContent = false;
        this.showRejectContent = false;
        this.showAcceptProcessForm = false;
        this.showRejectProcessForm = false;
    }

    goBackToFirstScreen(){
        this.showRejectProcessForm=false;
        this.showAcceptProcessForm = false;
    }
    getCustomPermission() {
        getCheckUserPermission({
            permissionName: internalAdfViewer
        }).then((result) => {
            this.captureUserAccess = result;
            this.disableAcceptRejectButtons = this.captureUserAccess || hasPrimaryFPShipmentFlow ;
          }).catch((error) => {
            this.error = error;
          });
    }

    @wire(getFPShipmentRec, {
        shipmentFinishedProductRecId : '$recordId'
    })
    currentFPShipmentRec({error,data}){
        if(data){
            this.finishedProductShipmentRec = data;

            if(this.finishedProductShipmentRec && this.finishedProductShipmentRec.CCL_Status__c == 'Product Shipped'){
                this.showRejectButton = true;
            }
        }
        if(error){
        }
    }

    validateAphIdDINSEC(event){
        this.showErrorMsg=false;
        this.aphId = this.template.querySelector('lightning-input').value;
        this.AphIdDINSECIdValue = this.finishedProductShipmentRec.CCL_APH_DIN_SEC_ID__c;
        this.checkUserResponse();
    }

    checkUserResponse(){
        if(this.AphIdDINSECIdValue){
            
            if(this.AphIdDINSECIdValue.split(";").includes(this.aphId)){
                this.matchFound=true;
            
            }else{
                this.matchFound=false;

            }
         
            if(this.matchFound==true){
                this.showErrorMsg = false;
                this.showRejectProcessForm = true;
                this.showAcceptProcessForm = true;
            }
            else if(this.matchFound==false){
                this.showRejectProcessForm = false;
                    this.showAcceptProcessForm = false;
                    this.showErrorMsg = true;
            }
            else if(this.showRejectProcessForm==true || this.showAcceptProcessForm==true){
                this.showErrorMsg = false;

            }
            else{
                this.showErrorMsg = true;
            }
        }else{
            this.showErrorMsg = true;
        }

        this.AphIdDINSECIdValue = undefined;
    }
    renderedCallback() {	
              this.getCustomPermission();	
      }

}
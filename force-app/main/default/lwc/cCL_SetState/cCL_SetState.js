import { LightningElement, api, wire } from 'lwc';
import { fireEvent } from "c/cCL_Pubsub";
import { CurrentPageReference } from 'lightning/navigation';

export default class CCLSetState extends LightningElement {
@api eventName;
@api therapyType;
@wire(CurrentPageReference) pageRef;

connectedCallback() {
    fireEvent(this.pageRef,'screenName' , this.eventName);
}
}
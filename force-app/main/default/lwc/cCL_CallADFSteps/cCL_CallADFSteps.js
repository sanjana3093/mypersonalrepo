import { LightningElement,track,api ,wire } from 'lwc';
import getStepslist from '@salesforce/apex/CCL_ADF_Controller.fetchADFSteps';
import getShipmentInfo from '@salesforce/apex/CCL_ADFController_Utility.getShipmentInfo';
import backTolist from "@salesforce/label/c.CCL_Back_to_patient_List";
import CCL_Org_Link from "@salesforce/label/c.CCL_Org_Link";
import CCL_Go_to_Summary from "@salesforce/label/c.CCL_Go_to_Summary";
import { NavigationMixin,CurrentPageReference } from "lightning/navigation";
import shipmentDetails from "@salesforce/label/c.CCL_Goto_Shpment";
//2260
import { registerListener,fireEvent } from "c/cCL_Pubsub";
import CCL_Product_Order_Summary from "@salesforce/label/c.CCL_Product_Order_Summary";
import CCL_Recommended_Collection_Details from "@salesforce/label/c.CCL_Recommended_Collection_Details";
import CCL_Required_Cryopreservation_Details from "@salesforce/label/c.CCL_Required_Cryopreservation_Details";
import CCL_Recommended_Cryopreservation_Details from "@salesforce/label/c.CCL_Recommended_Cryopreservation_Details";
import CCL_Summary_and_Submit from "@salesforce/label/c.CCL_Summary_and_Submit";
import CCL_Verify from "@salesforce/label/c.CCL_Verify";
import CCL_Required_Collection_Details from "@salesforce/label/c.CCL_Required_Collection_Details";
import checkUserPermission from "@salesforce/apex/CCL_ADFController_Utility.checkUserPermission";

export default class CCL_CallADFSteps extends NavigationMixin(LightningElement) {
    @api stepName=[];
  @api recordId;
  stepsLabels={CCL_Product_Order_Summary,CCL_Recommended_Collection_Details,CCL_Required_Cryopreservation_Details,CCL_Recommended_Cryopreservation_Details,CCL_Summary_and_Submit,
    CCL_Required_Collection_Details,CCL_Verify,CCL_Go_to_Summary};
    @track error;
    @track gotresult=false;CCL_Summary_and_Submit
    @track showLinksCheck=false;
    @track backTolist=backTolist;
    @track shipmentDetails=shipmentDetails;
    @track adfApproved;
    @track showLinks;
	@track shipmentId='';
    @track shipmentLink=false;
    @track latestscreen;
    @track prfUpdatedFlag;
    @track CustomerOpPermission='CCL_Customer_Operations';
    @track isCustomerOp=false;
    //ends here
    @track InboundLogistcsPermission='CCL_Inbound_Logistics_Users';
    @track isInboundLogisctics=false;
    @track NovartisAdminPermission='CCL_Novartis_Admin_Permission_Set';
    @track isNovartisAdmin=false;
	@track adfViewerInternal="CCL_ADF_Viewer_Internal";
    @track isAdfViewerInternal=false;
//2260
showGotoSummary;
    @wire(CurrentPageReference) pageRef;

    connectedCallback() {
      registerListener("adfApprovedPage", this.handleAdfApprovedPage,this);
      registerListener("showLinks", this.handleLinks,this);
      //2260
      registerListener("showSummaryLink", this.showSummaryLink,this);
	  getShipmentInfo({
        recordId: this.recordId
      })
      .then((result) => {
        if(result){
        this.shipmentId = result[0].Id;
        }
      })
      .catch((error) => {
        this.error = error;
      });
	  
	   checkUserPermission({
        permissionName: this.adfViewerInternal
      })
        .then((result) => {
          this.isAdfViewerInternal=result;
          
          this.error = null;
        })
        .catch((error) => {
          this.error = error;
        });
	  
	 // added as part of 868
      checkUserPermission({
        permissionName: this.CustomerOpPermission
      })
        .then((result) => {
          this.isCustomerOp=result;
          this.error = null;
        })
        .catch((error) => {
          this.error = error;
        });
      //Ends here
      checkUserPermission({
        permissionName: this.InboundLogistcsPermission
      })
        .then((result) => {
         this.isInboundLogisctics=result;
         checkUserPermission({
          permissionName: this.NovartisAdminPermission
        })
          .then((result) => {
           this.isNovartisAdmin=result;
           this.error = null;
          })
          .catch((error) => {
            this.error = error;
            
          });
          this.error = null;
        })
        .catch((error) => {
          this.error = error;
        });
      this.error = null;
	 }

    @wire(getStepslist)
    wiredSteps({ error, data }) {
        if (data) {
          let temp={MasterLabel:'',CCL_Order_of_Step__c:''};
           
            let self=this;
            
            data.forEach(function(node){
              temp={MasterLabel:'',CCL_Order_of_Step__c:''};
              temp.MasterLabel=self.stepsLabels[node.MasterLabel];
              temp.CCL_Order_of_Step__c=node.CCL_Order_of_Step__c;
              self.stepName.push(temp);
            });
            
            this.error = null;
            this.gotresult=true;
            this.showLinksCheck=true;
        } else if (error) {
            this.error = error;
            this.stepName = null;
        }
    }

    handleAdfApprovedPage(status) {
      this.adfApproved = status;
      if(this.adfApproved==='adfApproved' || this.adfApproved==='submitter'){
        this.shipmentLink=true;
		if(this.adfApproved==='adfApproved'){
			this.gotresult=false;
		}
      }
	   if(this.adfApproved==='noDisplay'){
        this.shipmentLink=false;
      }
    }

    handleLinks(status) {
      this.showLinks = status;
      if(this.showLinks==='show'){
        this.showLinksCheck=false;
      }
    }

    navigateToView() {
		if(this.isCustomerOp||this.isInboundLogisctics||this.isNovartisAdmin||this.adfViewerInternal){
			this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'CCL_Apheresis_Data_Form__c',
                actionName: 'home',
            },
        });
        }
        else{
        this[NavigationMixin.Navigate]({
          type: "comm__namedPage",
          attributes: {
            name: "Home"
          },
		   state: {
            "tabset-8e13c": "4d03f"
          }
        });
		}
	  }

      navigateToShipmentView() {
        if(this.isCustomerOp||this.isInboundLogisctics||this.isNovartisAdmin||this.isAdfViewerInternal){
          if(this.shipmentId!=null){
			this[NavigationMixin.GenerateUrl]({
					type: 'standard__recordPage',
					attributes: {
						recordId: this.shipmentId,
						objectApiName: 'CCL_Shipment__c',
						actionName: 'view'
					},
				}).then(url => {
        this.recordPageUrl = url;
       window.open('https:'+this.recordPageUrl,'_blank');
				});
			} else {
				this[NavigationMixin.GenerateUrl]({
					type: 'standard__objectPage',
					attributes: {
                    objectApiName: 'CCL_Apheresis_Data_Form__c',
                    actionName: 'home',
					},
				}).then(url => {
          this.recordPageUrl = url;
      window.open('https:'+this.recordPageUrl,'_blank');
				});
      }
        }else {
			this[NavigationMixin.Navigate]({
				type: "standard__webPage",
				attributes: {
					url:  CCL_Org_Link + this.shipmentId
				}
				},true);
			}
    }
   //2260
    showSummaryLink(showLink){
      if(showLink){
        this.showGotoSummary=true;
        this.shipmentLink=false;
      }else{
        this.showGotoSummary=false;
        this.shipmentLink=true;
      }
    }
	
    goTosummary(){
      fireEvent(this.pageRef, 'gotoSummary', true);
    }
}
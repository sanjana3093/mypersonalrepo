import { LightningElement,api,track } from 'lwc';
import physical from "@salesforce/label/c.PSP_Physical";
export default class PSP_Display_Card_Details extends LightningElement {
    @api cardName=null;
    @api cardType=null;
    @api retrievedCardName=null;
    @track physical=physical;
    @track isPhysical=false;
    connectedCallback(){
       if(this.cardType==='Physical'){
           this.isPhysical=true;
       }
        
    }
}
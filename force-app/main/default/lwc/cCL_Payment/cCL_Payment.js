import { LightningElement, api, track, wire } from 'lwc';
import { getObjectInfo, getPicklistValuesByRecordType } from 'lightning/uiObjectInfoApi';
import ORDER_OBJECT from '@salesforce/schema/CCL_Order__c';
import PATIENT_COUNTRY_RESIDENCE_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Patient_Country_Residence__c';
import BILLING_PARTY_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Billing_Party__c';
import CCL_Hospital_Purchase_Order_Number from '@salesforce/label/c.CCL_Hospital_Purchase_Order_Number';
import CCL_PR_Submission_Text from '@salesforce/label/c.CCL_PR_Submission_Text';
import CCL_Payment_Details from '@salesforce/label/c.CCL_Payment_Details';
import CCL_VA_Purchase_Order from '@salesforce/label/c.CCL_VA_Purchase_Order';
import CCL_Purchase_Order_Error from '@salesforce/label/c.CCL_Purchase_Order_Error';
import patientCountryLabel from "@salesforce/label/c.CCL_PatientCountry";
import billingPartyLabel from "@salesforce/label/c.CCL_BillingParty";
import noneLabel from "@salesforce/label/c.CCL_None_Label";
import none from "@salesforce/label/c.CCL_None";
import optional from "@salesforce/label/c.CCL_Optional_Placeholder_Lwc";
import pleaseSelect from "@salesforce/label/c.CCL_PleaseSelect";

import { FlowNavigationNextEvent, FlowNavigationBackEvent, FlowAttributeChangeEvent } from 'lightning/flowSupport';

//import {FlowAttributeChangeEvent} from 'lightning/flowSupport';

export default class CCL_Payment extends LightningElement {
    @api purchaseorder = ''; //variable to access in flow
    @api checkb; // variable to access in flow
    @api hospitalOptInFlow;
    @api veteransApplicableFlow;
    @api returnToReview;
    @api screenName='';
    @api var1='slds-p-left_none slds-p-right_none';
    @api var2='customPadding';
    @api hospitalPurchaseOrderOptInFlow='';
    @api veteransFlagFlow='';
    @api optIn=false;
    @api veteranFlag=false;
    @api backbuttonCLicked;
    @api backButtonClick;
    @api previousButtonClick;
    @track returnFromReview=false;
    @api hospitalPurchaseOrderText;
    @api isLoadFirst =false;
    @api checkbox = false;
    @api billingPartyVisible;
    @api capturePatientCountry;
    @api applicableBillingParties;
    @api hospitalCountryCode;
    @api patientCountry;
    @api billingParty;
    @api patientCountryResidenceValue;
    @api billingPartyValue;
    @api allowreplacementordertrue;
    billingPartyValues;
    patientCountryValues;
    billingPartyPicklisValues;
    billingPartyOptIn = false;
    patientCountryOptIn = false;
    purchase;
    //hospitalOptIn;
    //veteransApplicable;
    isShowValidation= false;
    showValidation = false;

    @wire(getObjectInfo, { objectApiName: ORDER_OBJECT })
    objectInfo;

    @wire(getPicklistValuesByRecordType, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', objectApiName: ORDER_OBJECT })
    picklistValuesList({data}) {
        if(data) {
            this.patientCountryValues = data.picklistFieldValues[PATIENT_COUNTRY_RESIDENCE_FIELD.fieldApiName].values;
            this.patientCountryValues = [{label:noneLabel, value:noneLabel}, ...this.patientCountryValues];
            this.billingPartyPicklisValues = data.picklistFieldValues[BILLING_PARTY_FIELD.fieldApiName].values;

            if(this.billingPartyOptIn && this.billingPartyPicklisValues && !this.billingPartyValues) {
                this.initializeBillingPartyValues();
            }
        }
    }

   constructor() {
        super();
    }

    label={CCL_Hospital_Purchase_Order_Number,
           CCL_PR_Submission_Text,
           CCL_Payment_Details,
           CCL_VA_Purchase_Order,
           CCL_Purchase_Order_Error,
           patientCountryLabel,
           billingPartyLabel,
           optional,
           pleaseSelect
        };

    handlechange(event){
        if(event.target.type== 'text'){
        this[event.target.name]=event.target.value;
        this.purchaseorder = event.target.value;
        this.purchase = event.target.value;
       }

		if(event.target.type == 'checkbox'){
            //this.checkb = event.detail.value;
            this.checkb = event.target.checked;
            this.checkbox = event.target.checked;
            //this.checkbox= event.detail.value;

            //let checkb1 = localStorage.event.target.checked;
        }
    }

    connectedCallback() {
        if(this.checkb || this.purchaseorder || this.purchaseorder == ''){
            this.isLoadFirst=true;
        }
        if(this.hospitalPurchaseOrderOptInFlow=='Yes')
        {
            this.hospitalOptInFlow=true;

        }
        else if(this.hospitalPurchaseOrderOptInFlow=='No')
        {
           this.hospitalOptInFlow=false;

        }
        if(this.veteransFlagFlow=='Yes')
        {
            this.veteransApplicableFlow=true;

        }
        else if(this.veteransFlagFlow=='No')
        {
           this.veteransApplicableFlow=false;

        }
        if(this.billingPartyVisible=='Yes') {
            this.billingPartyOptIn=true;
        }
        if(this.capturePatientCountry=='Yes') {
            this.patientCountryOptIn=true;

            if(this.hospitalCountryCode && !this.patientCountry && !this.allowreplacementordertrue==true) {
                this.patientCountry = this.hospitalCountryCode;
            }
        }


    }

    renderedCallback(){
    	if(this.isLoadFirst){
    		if(this.returnToReview !== null && this.returnToReview ==='PaymentCommercial' && this.previousButtonClick){
    			this.returnFromReview=true;
    		}
    		else if (!this.previousButtonClick) {
    			this.returnFromReview=false;
    		}
    	if(this.checkb){
    		this.template.querySelector('.chk').checked=true;
        }
    	this.isLoadFirst=false;
    	}
        if(this.billingPartyOptIn && this.billingPartyPicklisValues && !this.billingPartyValues) {
            this.initializeBillingPartyValues();
        }

        if(this.patientCountryValues && this.patientCountry && !this.patientCountryResidenceValue) {
            this.patientCountryValues.forEach(ele => {
                if(ele.value == this.patientCountry){
                    this.patientCountryResidenceValue = ele.label;
                }
            });
        }
    }

    onNext(){
         if(!this.purchaseorder && !this.checkb){
            this.isShowValidation = false;
            this.showValidation = false;
         }
         else if(this.purchaseorder && this.checkb){
            this.isShowValidation = true;
            this.showValidation = true;
        }
        else if(this.purchaseorder || this.checkb){
            this.showValidation = false;
            this.isShowValidation = false;
        }
        this.previousButtonClick=false;
        if(this.isShowValidation == true){
            this.showValidation = true;
        }else{
            this.screenName='Payment_Detail_Next';

            const attributeChangeEvent = new FlowAttributeChangeEvent('ScreenName', this.ScreenName);
            this.dispatchEvent(attributeChangeEvent);
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);

        }
    }

    onPrevious(event) {
        // previous code here
        if(this.returnToReview != null){
            this.screenName='Payment_Detail_Back';
            const attributeChangeEvent = new FlowAttributeChangeEvent('ScreenName', this.ScreenName);
            this.dispatchEvent(attributeChangeEvent);
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
        }else{
            const navigateBackEvent = new FlowNavigationBackEvent();
            this.dispatchEvent(navigateBackEvent);
        }

    }

    onReview(){
		if(!this.purchaseorder && !this.checkb){
            this.isShowValidation = false;
            this.showValidation = false;
         }
        else if(this.purchaseorder && this.checkb){
            this.isShowValidation = true;
            this.showValidation = true;
        }
        else if(this.purchaseorder || this.checkb){
            this.showValidation = false;
            this.isShowValidation = false;
        }
        this.previousButtonClick=false;
        if(this.isShowValidation == true){
            this.showValidation = true;
        }else{
            this.previousButtonClick=false;
            this.screenName='Payment_Detail_Review';
            const attributeChangeEvent = new FlowAttributeChangeEvent('ScreenName', this.ScreenName);
            this.dispatchEvent(attributeChangeEvent);
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
        }
    }

    handleCountryChange(event) {
        this.patientCountry = event.detail.value;
        this.patientCountryValues.forEach(ele => {
            if(ele.value == event.detail.value){
                this.patientCountryResidenceValue = ele.label;
            }
        });
    }

    handleBillingPartyChange(event) {
        this.billingParty = event.detail.value;
        this.billingPartyValues.forEach(ele => {
            if(ele.value == event.detail.value){
                this.billingPartyValue = ele.label;
            }
        });
    }

    initializeBillingPartyValues() {
        this.billingPartyValues = [];

        if(this.applicableBillingParties) {
            let billingPartyArray = this.applicableBillingParties.split(';');

            if(billingPartyArray.length) {
                this.billingPartyValues.push({label:none, value:noneLabel});

                this.billingPartyPicklisValues.forEach(ele => {
                    if(billingPartyArray.includes(ele.value)) {
                        this.billingPartyValues.push(ele);
                    }
                })
            }
        }
    }
}
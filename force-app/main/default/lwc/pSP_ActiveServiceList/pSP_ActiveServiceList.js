import {
  LightningElement,
  wire,
  track,
  api
} from "lwc";
import getServiceList2 from "@salesforce/apex/PSP_Search_Component_Controller.getServiceList";
import updateProduct from "@salesforce/apex/PSP_Search_Component_Controller.updateProduct";
import createRecords from "@salesforce/apex/PSP_Search_Component_Controller.createCPPRecords";
import getRecord from "@salesforce/apex/PSP_Search_Component_Controller.getProduct";
import {
  ShowToastEvent
} from "lightning/platformShowToastEvent";
import {
  refreshApex
} from "@salesforce/apex";
import successMessage from "@salesforce/label/c.PSP_Success_Message";
import recUpdateMessage from "@salesforce/label/c.PSP_Record_Update_message";
import successTitle from "@salesforce/label/c.PSP_Toast_Title_Success";
import errorVariant from "@salesforce/label/c.PSP_Error_Variant";
import errorMessage from "@salesforce/label/c.PSP_Error_Message_Create";
import serviceName from "@salesforce/label/c.PSP_Service_Name";
import serviceActive from "@salesforce/label/c.PSP_Service_Active";
import serviceDefault from "@salesforce/label/c.PSP_Service_Default";
import serviceFamily from "@salesforce/label/c.PSP_Service_Family";
import serviceType from "@salesforce/label/c.PSP_Service_Type";
import carePlanTemplate from "@salesforce/label/c.PSP_Care_Plan_Template";
import filter from "@salesforce/label/c.PSP_Filter";
import clear from "@salesforce/label/c.PSP_Clear";
import cppheader from "@salesforce/label/c.PSP_CPP_Name";
import save from "@salesforce/label/c.PSP_Save";
import cancel from "@salesforce/label/c.PSP_Cancel";
import {
  getObjectInfo
} from 'lightning/uiObjectInfoApi';
import PRODUCT_OBJECT from '@salesforce/schema/Product2';
import {
  getPicklistValues
} from 'lightning/uiObjectInfoApi';
import SERVICE_TYPE from '@salesforce/schema/Product2.PSP_Service_Type__c';
export default class PSP_ActiveServiceList extends LightningElement {
  @track servicesList = [];
  @track selRec = [];
  @track data = [];
  @track sortIcon;
  @track ifArrayEmpty = false;
  @track serviceName = serviceName;
  @track serviceActive = serviceActive;
  @track serviceDefault = serviceDefault;
  @track serviceFamily = serviceFamily;
  @track serviceType = serviceType;
  @track carePlanTemplate = carePlanTemplate;
  @track serviceCBList = [];
  @track cppCBList = [];
  @track draftValues = [];
  @track error;
  @track servicedata;
  @track loadMoreStatus;
  @api totalNumberOfRows;
  @track sortedBy;
  @track sortDirection = "desc";
  @track showSpinner = true;
  @track refreshdata;
  @track sortAsc = true;
  @api recordId;
  @track servicetype;
  @track filterdata = [];
  @track finalArray = [];
  @track proserviceVal;
  @track activeBox;
  @track careProgram;
  @track filter=filter;
  @track clear=clear;
  @track save=save;
  @track cancel=cancel;
  @track cppheader=cppheader;
  refreshDataonUpdate;

  @track sercicetypepicklistval;
  @track btnClass = "slds-hide";

  /***********************************************
   * Fetch all services record from apex
   **********************************************/

  @wire(getObjectInfo, {
    objectApiName: PRODUCT_OBJECT
  })
  objectInfo;
  @wire(getPicklistValues, {
    recordTypeId: '$objectInfo.data.defaultRecordTypeId',
    fieldApiName: SERVICE_TYPE
  })
  IndustryPicklistValues(result) {

    this.sercicetypepicklistval = result;
  }
  @wire(getServiceList2, {
    recordId: "$recordId"
  })
  services(result) {
    this.refreshDataonUpdate = result;
    if (result.data) {
      this.servicedata = result.data;
      this.showSpinner = false;
    } else if (result.error) {
      this.showSpinner = false;
      this.error = result.error;
    }
  }

  /***************************************************
   * Set the arrow direction based on sort direction
   ***************************************************/
  get sortArrow() {
    if (this.sortDirection === "asc") {
      this.sortIcon = "utility:arrowdown";
    } else {
      this.sortIcon = "utility:arrowup";
    }
    return this.sortIcon;
  }

  /***************************************
   * Method for sorting
   *****************************************/
  sortData(fieldName, sortDirection) {
    let data = JSON.parse(JSON.stringify(this.servicedata));
    let key = a => a[fieldName];
    let reverse = sortDirection === "asc" ? 1 : -1;
    data.sort((a, b) => {
      let valueA = key(a) ? key(a).toLowerCase() : "";
      let valueB = key(b) ? key(b).toLowerCase() : "";
      return reverse * ((valueA > valueB) - (valueB > valueA));
    });
    this.servicedata = data;
    this.showSpinner = false;
  }
  /*********************************************************
   * this method is called when a column sort icon is clicked
   ***********************************************************/
  sortThisColumn(event) {
    this.showSpinner = true;
    this.sortedBy = event.currentTarget.title;
    if (this.sortDirection === "asc") {
      this.sortDirection = "desc";
    } else {
      this.sortDirection = "asc";
    }
    this.sortData(this.sortedBy, this.sortDirection);
  }
  /********************************************************
     Method to save all UI changes in an object
     ********************************************************/
  handleUpdateProduct(event) {
    this.btnClass = "slds-show";
    if (event.detail.source === "lookup") {
      for (let i = 0; i < this.servicesList.length; i++) {
        if (this.servicesList[i].productId === event.detail.productId) {
          this.servicesList[i].cppId = event.detail.cppId;
          return;
        }
      }
      this.servicesList.push({
        cppId: event.detail.cppId,
        productId: event.detail.productId,
        careProgProdId: event.detail.careProgProdId
      });
    }
    if (event.detail.source === "cppToggle") {
      for (let i = 0; i < this.cppCBList.length; i++) {
        if (this.cppCBList[i].productId === event.detail.productId) {
          this.cppCBList[i].actChecked = event.detail.actChecked;
          return;
        }
      }
      this.cppCBList.push({
        actChecked: event.detail.actChecked,
        productId: event.detail.productId,
        careProgProdId: event.detail.careProgProdId
      });
    }
    if (event.detail.source === "serviceToggle") {
      for (let i = 0; i < this.serviceCBList.length; i++) {
        if (this.serviceCBList[i].productId === event.detail.productId) {
          this.serviceCBList[i].defChecked = event.detail.defChecked;
          return;
        }
      }
      this.serviceCBList.push({
        defChecked: event.detail.defChecked,
        productId: event.detail.productId,
        careProgProdId: event.detail.careProgProdId
      });
    }
  }

  /*******************************************
     Method to update the records in table
      ******************************************/
  saveChanges(event) {
    this.showSpinner = true;
    updateProduct({
        cptServiceObj: JSON.stringify(this.servicesList),
        serviceToggleObj: JSON.stringify(this.serviceCBList),
        cppToggleObj: JSON.stringify(this.cppCBList)
      })
      .then(resp => {
        this.btnClass = "slds-hide";
        this.showSpinner = false;
        this.dispatchEvent(
          new ShowToastEvent({
            title: successTitle,
            message: recUpdateMessage,
            variant: successTitle
          })
        );
        return refreshApex(this.refreshDataonUpdate);
      })
      .catch(err => {
        this.showSpinner = false;
        this.dispatchEvent(
          new ShowToastEvent({
            title: errorMessage,
            message: err.body.message,
            variant: errorVariant
          })
        );
      });
    this.servicesList = [];
    this.serviceCBList = [];
    this.cppCBList = [];
    return refreshApex(this.refreshDataonUpdate);
  }

  get SaveStyle() {
    return this.btnClass;
  }
  /*****************************
      Below methods are declared to handle Save recrods from search component
    ***************************** */
  @wire(getRecord, {
    recordId: "$recordId"
  })
  programRec(result) {
    if (result.data) {
      this.careProgram = result.data;
    }
  }

  @api
  get serviceval() {
    return this.selRec;
  }
  set serviceval(prvalue) {
    if (prvalue !== undefined) {
      let saveAll = Array.isArray(prvalue);

      if (saveAll) {
        if (prvalue.length > 0) {
          this.selRec = prvalue;
          this.saveMethod(this.selRec);
        }
      } else {
        this.selRec.push(prvalue);
        this.saveMethod(this.selRec);
      }
    }
  }

  /******************************************** 
  Method to add search results in table
 *********************************************/
  saveMethod(serviceList) {
    let draftValuesStr = JSON.stringify(serviceList);
    let prod = this.careProgram;
    this.showSpinner = true;
    createRecords({
        productRecs: draftValuesStr,
        careProgram: prod
      })
      .then(result => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: successTitle,
            message: successMessage,
            variant: successTitle
          })
        );
        const currentRecord = this.servicedata;
        const newData = serviceList.concat(currentRecord);
        this.servicedata = newData;
        this.selRec = [];
        this.showSpinner = false;
        return refreshApex(this.refreshDataonUpdate);
      })
      .catch(error => {
        this.showSpinner = false;
        this.dispatchEvent(
          new ShowToastEvent({
            title: errorMessage,
            message: error.body.message,
            variant: errorVariant
          })
        );
        this.selRec = [];
      });
  }
  hideSave() {
    this.btnClass = "slds-hide";
    this.servicesList = [];
    this.serviceCBList = [];
    this.cppCBList = [];
  }
  onselectservice(event) {
    this.servicetype = event.target.value;
  }
  handleInputChangechange(event) {
    if (event.target.value.length > 2) {
      this.proserviceVal = event.target.value.toLowerCase();
    }

  }
  handlechangeCheckbox(event) {
    this.activeBox = event.target.checked;
  }

  filterTable() {
    this.filterdata = [];
    let finalVar = []
    this.ifArrayEmpty=false;
    this.servicedata = this.refreshDataonUpdate.data;


    if (this.proserviceVal !== null && this.proserviceVal !== undefined) {
      finalVar = this.filterByValue(serviceName, this.servicedata);
    }

    if (this.servicetype !== undefined && this.servicetype != null) {
      if (finalVar.length > 0) {
        finalVar = this.filterByValue(serviceType, finalVar);
      } else {
        finalVar = this.filterByValue(serviceType, this.servicedata);
      }
    }

    if (this.activeBox !== undefined && this.activeBox != null) {
      if (finalVar.length > 0) {
        finalVar = this.filterByValue(serviceActive, finalVar);
      } else {
        finalVar = this.filterByValue(serviceActive, this.servicedata);
      }
    }
    if (this.ifArrayEmpty) {
      this.servicedata = []
    } else {
      this.servicedata = finalVar;
    }

  }
  filterByValue(key, array) {
    let i = 0;
    this.finalArray = [];
    for (i = 0; i < array.length; i++) {
      if (key === serviceName) {
        if (array[i].name.toLowerCase().includes(this.proserviceVal)) {
          this.finalArray.push(array[i]);
        }
      }

      if (key === serviceType) {
        if (array[i].serviceType === this.servicetype) {
          this.finalArray.push(array[i]);
        }
      }

      if (key === serviceActive) {
        if (array[i].isActive) {
          this.finalArray.push(array[i]);
        }
      }
    }
    if (this.finalArray.length == 0) {
      this.ifArrayEmpty = true;
    }
    return this.finalArray;
  }
  clearFilter() {

    this.filterdata = [];
    this.servicetype = null;
    this.proserviceVal = null;
    this.activeBox = null;
    this.ifArrayEmpty = false;

    this.servicedata = this.refreshDataonUpdate.data;
    return refreshApex(this.refreshDataonUpdate);
  }
}
import { LightningElement,api } from 'lwc';

export default class CCL_DateFormat extends LightningElement {
    @api fieldApiName;
    @api myDateFields;
    @api type;
    @api inShipment=false;
    @api inCollection=false;

    get myDisplay(){
        if(this.myDateFields[this.fieldApiName]!=undefined&&this.myDateFields[this.fieldApiName]!=''&&this.myDateFields[this.fieldApiName]!=null){
            return this.type=='Date'|| this.type=='date'?this.myDateFields[this.fieldApiName]:'';
        }
        return 'Please Select Date';
    }
}
import { LightningElement, track, api, wire } from "lwc";
import CONTACT from "@salesforce/schema/Contact";
import { getPicklistValues } from "lightning/uiObjectInfoApi";

import getValues from "@salesforce/apex/saveContact.getPicklist";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import NOTIFICATION from "@salesforce/schema/Contact.Notification__c";
export default class CallMultipicklist extends LightningElement {
  @api options;
  @track myResult;
  @track hello = "hello";
  @track picklistLoaded = false;
  @track yourSelectedValues = "";
  @track pickListVal;
  @wire(getObjectInfo, {
    objectApiName: CONTACT
  })
  objectInfo;
  @wire(getPicklistValues, {
    recordTypeId: "$objectInfo.data.defaultRecordTypeId",
    fieldApiName: NOTIFICATION
  })
  IndustryPicklistValues(result) {
    //  this._mOptions = result;
    let temp = { key: "", value: "", selected: "" };

    let resArray = new Array();
    console.log("hello" + this.hello);
    let preSelecetedvals = this.yourSelectedValues;
    if (result !== undefined) {
      console.log("check 0");
      if (result.data) {
        this.myResult = result.data.values;
        console.log("the valar " + JSON.stringify(this.myResult));
        result.data.values.forEach(function (eachItem) {
          // console.log('--'+this.pickListVal);
          if (
            preSelecetedvals !== null &&
            preSelecetedvals.includes(eachItem.value)
          ) {
            temp = { key: "", value: "", selected: true };
          } else {
            temp = { key: "", value: "", selected: "" };
          }
          temp.key = eachItem.label;
          temp.value = eachItem.value;
          resArray.push(temp);
        });

        this.options = resArray;
        console.log("thr new arr" + JSON.stringify(this.options));

        this.picklistLoaded = true;
      }
    }
  }
  connectedCallback() {
    getValues({
      contactId: "0032800000i3paqAAA"
    })
      .then((result) => {
        console.log("the result is" + result);
        if (result !== null) {
          this.yourSelectedValues = result;
        }

        console.log("the picklist val" + this.yourSelectedValues);
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }
  get msOptions() {
    console.log("thr new 99" + JSON.stringify(this.options));
    return this.options;
  }
  handleOnItemSelected(event) {
    if (event.detail) {
      let self = this;

      event.detail.forEach(function (eachItem) {
        console.log(eachItem.value);
        self.yourSelectedValues += eachItem.value + "; ";
      });
    }
    console.log("the values are" + this.yourSelectedValues);
  }
}

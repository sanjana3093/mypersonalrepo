import { LightningElement,api,track } from 'lwc';

import presFrequency from "@salesforce/label/c.PSP_Prescription_Frequency";
import unitheader from "@salesforce/label/c.PSP_Units";
import maximumdosage from "@salesforce/label/c.PSP_Max_Dosage";
import maxdosagefre from "@salesforce/label/c.PSP_Max_Dos_Frequency";

export default class pSP_DisplayPrescriptionInfo extends LightningElement {
    @api prescriptionfrequency;
    @api units;
    @api maxDosage;
    @api maxDosageFrequency;
    @api productName;
    @track unitheader=unitheader;
    @track maxdosagefre=maxdosagefre;
    @track maximumdosage=maximumdosage;
    @track presFrequency=presFrequency
}
import { LightningElement,api,wire } from 'lwc';
import getRecord from "@salesforce/apex/PSP_custLookUpCntrl.getAccessAccount";
export default class PSP_GetUserAccess extends LightningElement {
    @api recordId;
    @api hasAccess=false;
    @api isHCPFlow;
    // connectedCallback() {
      
        
    //     getRecord({
    //         recordId: this.recordId

    //     })
    //     .then(result => {
           
           
    //             if (result.data.length > 0) {
               
    //                 this.hasAccess=true;
    //             } else {
    //                 this.hasAccess=false;
    //             }
          
            
           
    //         this.error = null;
    //     })
    //     .catch(error => {
    //         this.error = error;
    //         this.enrolledprescriptionlist = null;
    //     });

    // }

    @wire(getRecord, {
        recordId: "$recordId"
    })
    getuserAccess(result) {
        if(this.recordId===undefined){
            this.hasAccess=true;
        }
        console.log('the recordId-99-'+this.recordId);
        if (result.data) {
            console.log('the result is--'+result.data);
            if (result.data.length > 0 || this.recordId===undefined) {
               
                this.hasAccess=true;
            } else {
                this.hasAccess=false;
            }
           
        } else if (result.error) {
            this.error = result.error;
        }
        if(this.isHCPFlow){
            this.hasAccess=true;
        }
        console.log('-has access----'+this.hasAccess);
    }
}
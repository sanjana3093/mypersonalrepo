import { LightningElement , api} from 'lwc';
import CCL_HEADER_CANCEL_REQUEST from '@salesforce/label/c.CCL_HEADER_CANCEL_REQUEST';
import CCL_MODAL_CANCEL from '@salesforce/label/c.CCL_Utility_modal_Cancel';
import CCL_CANCEL_ORDER from '@salesforce/label/c.CCL_CancelOrder';
export default class CCLFlowHeader extends LightningElement {
    
	@api showCancelBtnTop;
    label = {
        CCL_HEADER_CANCEL_REQUEST,
        CCL_MODAL_CANCEL,
        CCL_CANCEL_ORDER

    };

    cancelRequest(){
        window.open('/','_self');
    }
	cancelThisOrder(){
        window.open('/','_self');
    }
}

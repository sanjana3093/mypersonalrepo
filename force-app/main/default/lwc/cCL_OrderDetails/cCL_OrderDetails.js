import { LightningElement,api,wire,track } from 'lwc';
import getOrderDetails from '@salesforce/apex/CCL_PRF_Controller.getorders';
import { CurrentPageReference,NavigationMixin } from "lightning/navigation";
import CCL_Enter_a_Replacement_Order from "@salesforce/label/c.CCL_Enter_a_Replacement_Order";
import CCL_Patient_Name_PRF from "@salesforce/label/c.CCL_Patient_Name_PRF";
import CCL_Date_fof_Birth from "@salesforce/label/c.CCL_Date_fof_Birth";
import CCL_PatientCountry from "@salesforce/label/c.CCL_PatientCountry";
import CCL_BillingParty from "@salesforce/label/c.CCL_BillingParty";
import CCL_VA_Patient from "@salesforce/label/c.CCL_VA_Patient";
import CCL_Therapy_Name from "@salesforce/label/c.CCL_Therapy_Name";
import CCL_Protocol_Hospital_ID from "@salesforce/label/c.CCL_Protocol_Hospital_ID";
import CCL_Prescriber from "@salesforce/label/c.CCL_Prescriber";
import CCL_Principal_Investigator from "@salesforce/label/c.CCL_Principal_Investigator";
import CCL_Ordering_Hospital from "@salesforce/label/c.CCL_Ordering_Hospital";
import CCL_Novartis_Batch_ID_PRF from "@salesforce/label/c.CCL_Novartis_Batch_ID_PRF";
import CCL_Additional_Product_Order_Details from "@salesforce/label/c.CCL_Additional_Product_Order_Details";
import CCL_Payment_Details from "@salesforce/label/c.CCL_Payment_Details";
import CCL_Order_Submission_Date from "@salesforce/label/c.CCL_Order_Submission_Date";
import CCL_Order_Submitter from "@salesforce/label/c.CCL_Order_Submitter";
import CCL_Patient_Weight_at_Time_of_Order_Kg from "@salesforce/label/c.CCL_Patient_Weight_at_Time_of_Order_Kg";
import CCL_Hospital_Purchase_Order_Number from "@salesforce/label/c.CCL_Hospital_Purchase_Order_Number";
import CCL_Number_of_bags_available_not_shipped from "@salesforce/label/c.CCL_Number_of_bags_available_not_shipped";
import CCL_Payment_Classification from "@salesforce/label/c.CCL_Payment_Classification";
import CCL_Order_Summary from "@salesforce/label/c.CCL_Order_Summary";
import aph_onHold_label from '@salesforce/label/c.CCL_APH_onHold';
import mfg_onHold_label from '@salesforce/label/c.CCL_MFG_onHold';
import shp_onHold_label from '@salesforce/label/c.CCL_SHP_onHold';
import CCL_Modal_Close from '@salesforce/label/c.CCL_Modal_Close';
import CCL_Confirm_Change_Reason from '@salesforce/label/c.CCL_Confirm_Change_Reason';
import CCL_Reason_Modal_Header from '@salesforce/label/c.CCL_Reason_Modal_Header';
import CCL_Reason_Modal_Message from '@salesforce/label/c.CCL_Reason_Modal_Message';
import CCL_Back from '@salesforce/label/c.CCL_Back';
import CCL_FOOTER_SUBMIT from '@salesforce/label/c.CCL_FOOTER_SUBMIT';
import CCL_Order_Updated from '@salesforce/label/c.CCL_Order_Updated';
import CCL_Thank_you from '@salesforce/label/c.CCL_Thank_you';
import CCL_Changes_Saved from '@salesforce/label/c.CCL_Changes_Saved';
import CCL_Reason_Modal_Helptext from '@salesforce/label/c.CCL_Change_Reason_Modal_Helptext';
import CCL_Rejection_Reason from '@salesforce/label/c.CCL_Rejection_Reason';
import CCL_Product_Order_Details from '@salesforce/label/c.CCL_Product_Order_Details';
import CCL_Latest_Order_Submitted_Date from '@salesforce/label/c.CCL_Latest_Order_Submitted_Date';
import CCL_Latest_Order_Submitter from '@salesforce/label/c.CCL_Latest_Order_Submitter';
import CCL_Latest_Order_Approved_Date from '@salesforce/label/c.CCL_Latest_Order_Approved_Date';
import CCL_Latest_Order_Approver from '@salesforce/label/c.CCL_Latest_Order_Approver';
import CCL_Latest_Order_Rejected_Date from '@salesforce/label/c.CCL_Latest_Order_Rejected_Date';
import CCL_Latest_Order_Rejecter from '@salesforce/label/c.CCL_Latest_Order_Rejecter';
import CCL_patientInitials from '@salesforce/label/c.CCL_patientInitials';
import CCL_Edit_Order from "@salesforce/label/c.CCL_Edit_Order";
import CCL_Hospital_Patient_ID from "@salesforce/label/c.CCL_Hospital_Patient_ID";
import CCL_Return_to_Dashboard from "@salesforce/label/c.CCL_Return_to_Dashboard";
import CCL_Patient_Id from "@salesforce/label/c.CCL_Patient_Id";
import CCL_Treatment_Protocal_Subject_ID from "@salesforce/label/c.CCL_Treatment_Protocal_Subject_ID";
import ORDER_CHEVRON from "@salesforce/label/c.CCL_Order_Chevron";
import APHERESIS_CHEVRON from "@salesforce/label/c.CCL_Apheresis_Chevron";
import MANUFACTURING_CHEVRON from "@salesforce/label/c.CCL_Manufacturing_Chevron";
import DELIVERY_CHEVRON from "@salesforce/label/c.CCL_Delivery_Chevron";
import checkUserPermissions from "@salesforce/apex/CCL_ADFController_Utility.checkUserPermission";

import {
  getRecord,
  getFieldValue
} from 'lightning/uiRecordApi';
import PAYMENT_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Payment_Reason_Code__c';
import fetchShipmentForOrderSumary from '@salesforce/apex/CCL_PRF_Controller.fetchShipmentForOrderSumary';
import fetchFinishedProductDetails from "@salesforce/apex/CCL_PRF_Controller.fetchFinishedProductDetails";
import getUserName from "@salesforce/apex/CCL_ADF_Controller.getUserName";
import checkUserPermission from "@salesforce/apex/CCL_PRF_Controller.checkUserPermission";
import checkUserPermissionSet from "@salesforce/apex/CCL_PRF_Controller.checkUserPermissionSet";
import strUserId from '@salesforce/user/Id';
import checkallowreplacementPermission from "@salesforce/apex/CCL_PRF_Controller.checkallowreplacementPermission";
import fetchTherapyAssociation from '@salesforce/apex/CCL_PRF_Controller.fetchTherapyAssociation';
import checkphiPermission from "@salesforce/apex/CCL_PRF_Controller.checkPHIUser";
import checkPRFApprover from "@salesforce/apex/CCL_PRF_Controller.checkPRFApprover";
import checkPRFViewer from "@salesforce/apex/CCL_PRF_Controller.checkPRFViewer";
import checkCustomerOperationsUser from "@salesforce/apex/CCL_PRF_Controller.checkCustOpsPermission";
import UserId from '@salesforce/user/Id';

const orderFields = [
  'CCL_Order__c.CCL_PRF_Ordering_Status__c',
  'CCL_Order__c.CCL_Order_Approval_Eligibility__c',
  'CCL_Order__c.CreatedById',
  'CCL_Order__c.CCL_Latest_Order_Submitted_By__c',
  'CCL_Order__c.CCL_Latest_Order_Submitted_By__r.Name',
  'CCL_Order__c.CCL_Order_Approved_By__r.Name',
  'CCL_Order__c.CCL_Order_Rejected_By__r.Name'
];

const optionalfieldsorder = [
  'CCL_Order__c.CCL_Payment_Reason_Code__c',
  'CCL_Order__c.CCL_Patient_Country_Residence__c',
  'CCL_Order__c.CCL_Billing_Party__c',
  'CCL_Order__c.CCL_Rejection_Reason__c'
];

export default class CCLOrderDetails extends NavigationMixin(LightningElement) {
  @wire(CurrentPageReference) pageRef;
  label = {CCL_Enter_a_Replacement_Order,CCL_Date_fof_Birth,CCL_Patient_Name_PRF,CCL_Therapy_Name,CCL_Protocol_Hospital_ID,
    CCL_Prescriber,CCL_Ordering_Hospital,CCL_Novartis_Batch_ID_PRF,CCL_Additional_Product_Order_Details,
    CCL_Order_Submission_Date,CCL_Order_Submitter,CCL_Patient_Weight_at_Time_of_Order_Kg,CCL_Hospital_Purchase_Order_Number,
    CCL_Number_of_bags_available_not_shipped,CCL_Payment_Classification,CCL_Order_Summary,
    CCL_Reason_Modal_Header,
    CCL_Reason_Modal_Message,
    CCL_Back,
    CCL_FOOTER_SUBMIT,
    CCL_Confirm_Change_Reason,
    CCL_Modal_Close,
    CCL_Order_Updated,
    CCL_Thank_you,
    CCL_Changes_Saved,
    CCL_Reason_Modal_Helptext,
    CCL_Principal_Investigator,
    CCL_patientInitials,
    CCL_Payment_Details,
    CCL_PatientCountry,
    CCL_BillingParty,
    CCL_VA_Patient,
    CCL_Rejection_Reason,
    CCL_Product_Order_Details,
    CCL_Latest_Order_Submitted_Date,
    CCL_Latest_Order_Submitter,
    CCL_Latest_Order_Approved_Date,
    CCL_Latest_Order_Approver,
    CCL_Latest_Order_Rejected_Date,
    CCL_Latest_Order_Rejecter,
	CCL_Edit_Order,
    CCL_Hospital_Patient_ID,
	CCL_Return_to_Dashboard,
    CCL_Patient_Id,
    CCL_Treatment_Protocal_Subject_ID,
  };

    @api recordId;
    @api rtnValue=[];
    @api orderid;
    @api allowreplacementordertrue=false;
    @api allowreplacementorder=false;
    @api isEditOrder=false;
    @api showPaymentField = false;
    @api shipmentRecord;
    @api hpoNumber;
    @api orderSubmitterName;
    @api loaded = false;
    @api hasEditButtonAccess=false;
    @api hasPermission;
    @api permissionName='CCL_PRF_Submitter_User';
	@api userId = strUserId;
	@api allowreppermission;
    @api phipermission;
     disableReplacement=false;
	// CGTU-819
    @api cancelOrder = false;
	 @api displayHPO;
    @api isClinical;
    @api isCommercial;
    orderStatus='';
    showconfirmation=false;
    showUpdateSuccess=false;
    changeReasonForDataUpdate='';
	@api chevronJson=[];
    @api ChevronColor=[];
    @api exceptionStatus;
    @api conditionalStatus;
    @api aditionalInformation;
	@api orderDate;
  @api commercialTherapy=false;
  @api showApprovalVar=false;
	@track mainArr = [];
  @track prforder;
  @api dateOfBirth;
  @api isReplacementOrder;
  @api ShowPatientInitials=false;
  @api showPatientId=false;
	@track orderrecord;
	@track ordercreater;
  @track latestOrderSumitter;
  @track latestOrderApprover;
  @track showapprovalonsubmit=false;
  @track enterapproval=false;
  @track isCustomerOperationsUser=false;
  @track orderApproved;
  @track orderRejected;
  @track orderRejecter;
  @track rejectionReason;

  @api ribbon;
  @api quarentine;

  @track prfSubmitter="CCL_PRF_Submitter";
  @track isPrfSubmitter=false;
  @track prfApprover="CCL_PRF_Approver";
  @track prfViewer="CCL_PRF_Viewer";
  @track isPrfViewer=false;
  @track isPrfApprover=false;
  @track custOp="CCL_Customer_Operations";
  @track isCustOP=false;
  @track viewUpload=false;
  @track checkprfviewer=false;

  orderingHospital;
  therapy;
  countryCode;
  patientCountryDisplayValue;
  billingPartyDisplayValue;
  displayVA=false;
  displayPatientCountry=false;
  displayBillParty=false;
  vaFieldValue='No';
  activeSections;

  currentloggedinuser=UserId;


	showbutton(){
    this.showapprovalonsubmit=true;
  }


	@wire(checkPRFApprover)
	checkPRFApprover1({error, data}) {
      if (data) {
        this.enterapproval = data;
        this.showApprovalVar=true;
      }
      else if (error) {
        this.error = error;
        this.showApprovalVar=false;
      }
    }

	@wire(checkPRFViewer)
    checkPRFViewer1({error, data}) {
        if (data) {
          this.checkprfviewer = data;

        }
        else if (error) {
          this.error = error;

        }
      }

    @wire(checkCustomerOperationsUser)
	checkCustomerOperations({error, data}) {
      if (data) {
        this.isCustomerOperationsUser = data;
        this.showApprovalVar=true;
      }
      else if (error) {
        this.error = error;
        this.isCustomerOperationsUser = false;
        this.showApprovalVar=false;
      }
    }

    get showApproval() {


      return this.showApprovalVar;
    }


    @wire(getRecord, { recordId: '$recordId', fields : orderFields, optionalFields: optionalfieldsorder})
    wiredAccount({ error, data }) {
        if (data) {
            this.orderrecord = data;
            this.prforder = data.fields.CCL_PRF_Ordering_Status__c.value;
            this.approvaleligibility = data.fields.CCL_Order_Approval_Eligibility__c.value;
            this.ordercreater = data.fields.CCL_Latest_Order_Submitted_By__c.value;
            this.latestOrderSumitter = data.fields.CCL_Latest_Order_Submitted_By__r.value.fields.Name.value;

            if(data.fields.CCL_Patient_Country_Residence__c){
              this.patientCountryDisplayValue = data.fields.CCL_Patient_Country_Residence__c.displayValue;
              }
              if(data.fields.CCL_Billing_Party__c){
              this.billingPartyDisplayValue = data.fields.CCL_Billing_Party__c.displayValue;
              }

            this.error = undefined;
            if(this.prforder == 'PRF_Approved') {
              this.orderApproved = true;
              this.latestOrderApprover = data.fields.CCL_Order_Approved_By__r.value.fields.Name.value;
            } else if (this.prforder == 'PRF_Rejected') {
              this.orderRejected = true;
              this.rejectionReason = data.fields.CCL_Rejection_Reason__c.value;
              this.orderRejecter = data.fields.CCL_Order_Rejected_By__r.value.fields.Name.value;
            }
            if((this.prforder=='PRF_Submitted' || this.prforder=='PRF_Re-Submitted')
             && this.approvaleligibility==true && this.ordercreater!= this.currentloggedinuser
              )
               {
                this.showbutton();
            }
        } else if (error) {
            this.error = error;
        }
    }


    @wire(checkUserPermission)
    accountManagerPermission({ error, data }) {
      if (data) {
        this.disableReplacement = data;
      }
      else if (error) {
        this.error = error;
      }
    }

    @wire(fetchFinishedProductDetails, { orderId: '$recordId'})
    fetchFPBatchStatus({error, data}){
        if(data){
            this.error=null;
            if(JSON.stringify(data)!="[]"){
			  for(let i=0;i<Object.keys(data).length;i++){
				if(data[i].CCL_Batch_Status__c!=undefined&&data[i].CCL_Batch_Status__c!='Rejected'){
				  this.ribbon=true;
				  if(data[i].CCL_Batch_Status__c=='Quality'){
					this.quarentine=true;
					break;
				  }
				  else if(i==0&&(data[i].CCL_Batch_Status__c=="Approved"||data[i].CCL_Batch_Status__c=="Conditionally Approved")){
					this.quarentine=false;
				  }
				  else{
					this.ribbon=false;
				  }
				}
				if(i==Object.keys(data).length-1&&this.ribbon==undefined){
				  this.ribbon=false;
				}
			  }
			}
            else{
              this.ribbon=false;
            }
        }
        else if (error){
                      this.error = error;
                    }
    }


page(){
this[NavigationMixin.GenerateUrl]({
    type: 'comm__namedPage',
    attributes: {
    pageName: "ccl-prf",
    recordId: this.recordId,
    url: 'ccl-prf?recordId='+this.recordId
    },
    state: {
    recordId: this.recordId
    }
}).then(url => {
    window.open(url);
});
}
	@wire(checkUserPermissionSet)
    checkUserPermissionSet({ error, data }) {
      if (data) {
        this.hasPermission = data;
      }
      else if (error) {
        this.error = error;
      }
    }

	@wire(checkallowreplacementPermission)
    replacementPermission({ error, data }) {
      if (data) {
        this.allowreppermission = data;
      }
      else if (error) {
        this.error = error;
      }
    }

    openApproval(event){
      let id=this.recordId;
      this[NavigationMixin.GenerateUrl]({
        type: 'comm__namedPage',
        attributes: {
        pageName: "ccl-verification-approval",
        recordId: id,
        url: 'ccl-verification-approval?recordId='+id
        },
        state: {
        recordId: id
        }
    }).then(url => {
        window.open(url);
    });
  }

    @wire(checkphiPermission)
    phiPermissioncheck({ error, data }) {
      if (data) {
        this.phipermission = data;
        //this.phipermission=true;
      }
      else if (error) {
        this.error = error;
      }
    }

    get enterreplacement() {
      return this.allowreppermission && this.allowreplacementordertrue;
    }

    get phifield(){
      return this.phipermission;
    }

    connectedCallback() {
        getOrderDetails({recordId: this.recordId}).then((result) =>
         {
            this.rtnValue = result;
            this.orderId =  this.rtnValue[0].Id;
            this.orderStatus = this.rtnValue[0].CCL_Logistic_Status__c ? this.rtnValue[0].CCL_Logistic_Status__c: '';
            this.allowreplacementordertrue = this.rtnValue[0].CCL_Allow_Replacement_Order__c;
			this.exceptionStatus = this.rtnValue[0].CCL_Exception_Status__c;
            this.conditionalStatus = this.rtnValue[0].CCL_Conditional_Status__c;
            this.aditionalInformation = this.rtnValue[0].CCL_Additional_Information__c;
			this.orderDate = this.rtnValue[0].CCL_Order_Submission_Date_Text__c;
      this.dateOfBirth = this.rtnValue[0].CCL_Date_Of_Birth_Text__c;
			this.isReplacementOrder = this.rtnValue[0].CCL_Returning_Patient__c;
      this.orderingHospital = this.rtnValue[0].CCL_Ordering_Hospital__c;
      this.therapy = this.rtnValue[0].CCL_Therapy__c;
      this.countryCode = this.rtnValue[0].CCL_Ordering_Hospital__r.ShippingCountryCode;
      const vaValue = this.rtnValue[0].CCL_VA_Patient__c;

      if(vaValue || vaValue == 'true') {
        this.vaFieldValue = 'Yes';
      }

			if (this.rtnValue[0].CCL_Therapy__r.CCL_Type__c == 'Commercial') {
                this.commercialTherapy = true;
            }
			if(this.rtnValue[0].CCL_Allow_Replacement_Order__c)
			{
				this.allowreplacementordertrue = true;
			}
			if (this.rtnValue[0].CCL_Treatment_Status__c == 'Cancelled') {
				this.cancelOrder = true;
			}
			 if(this.rtnValue[0].CCL_Hospital_Patient_ID__c!==undefined)
        {
          this.isCommercial = true;
        }
        if(this.rtnValue[0].CCL_Treatment_Protocol_Subject_ID__c!==undefined)
        {
          this.isClinical = true;
        }
		if(this.rtnValue[0].CCL_Initials_COI__c){
          this.ShowPatientInitials=true;
          if(this.rtnValue[0].CCL_Patient_Initials__c===undefined){
            this.rtnValue[0].CCL_Patient_Initials__c==="--";
          }
        }
        if(this.rtnValue[0].CCL_Patient_Id__c!=undefined){
          this.showPatientId=true;
        }
		let self=this;
        self.rtnValue.forEach(function (node) {
        self.chevronJson=self.setChevronJson(node);
        self.ChevronColor=self.setChevronColor(node);
        });
            this.error = null;
            this.activeSections = ['additionalSection', 'paymentSection'];
            this.getTherapyAssociation();
          })
          .catch((error) =>
         {
            this.error = error;
          });
          //added as part of 2298
          checkUserPermissions({
            permissionName: this.prfSubmitter
          })
            .then((result) => {
              this.isPrfSubmitter = result;
              this.error = null;
              checkUserPermissions({
                permissionName: this.prfApprover
                })
                .then((result) => {
                    this.isPrfApprover = result;
                    this.error = null;
                    checkUserPermissions({
                        permissionName: this.prfViewer
                        })
                        .then((result) => {
                            this.isPrfViewer = result;
                            this.error = null;
                            checkUserPermissions({
                                permissionName: this.custOp
                                })
                                .then((result) => {
                                    this.isCustOP = result;
                                    if(this.isPrfApprover||this.isPrfSubmitter||this.isPrfViewer|| this.isCustOP){
                                        this.viewUpload=true;
                                    }
                                    this.error = null;
                                })
                                .catch((error) => {
                                    this.error = error;
                                });
                        })
                        .catch((error) => {
                            this.error = error;
                        });
                })
                .catch((error) => {
                    this.error = error;
                });
            })
            .catch((error) => {
              this.error = error;
            });

  }





  getTherapyAssociation() {
    const { orderingHospital, therapy, countryCode } = this;
    fetchTherapyAssociation({ orderingHospital, therapy, countryCode })
      .then(result => {
        let orderingTherapyAssociation = result[0];
        let veteransFlag =  orderingTherapyAssociation.CCL_Veteran_Affairs_Applicable__c;
        let countryOfResidenceFlag=orderingTherapyAssociation.CCL_Capture_Patient_Country_of_Residence__c;
        let billingPartyFlag=orderingTherapyAssociation.CCL_Billing_Party_Visible__c;

        if(veteransFlag == 'Yes' && (this.hasPermission || this.enterapproval || this.checkprfviewer )) {
          this.displayVA = true;
        }
        if(countryOfResidenceFlag == 'Yes' && this.hasPermission || this.phipermission) {
          this.displayPatientCountry = true;
        }
        if(billingPartyFlag == 'Yes' && this.hasPermission || this.phipermission) {
          this.displayBillParty = true;
        }

      })
      .catch(error => {
        this.error = error;
      });

  }

	editOrder()
      {
        this.loaded = true;
        this.isEditOrder=true;
        new Promise((resolve, reject) => {
          setTimeout(() => {
            let start = new Date();
            let y = 0;
            while (new Date() - start < 1000) {
              y = y + 1;
            }
            resolve(y);
          }, 1000);
        }).then(
          () => this.loaded = false
        );
      }
      closeModal() {

        this.isEditOrder = false;
    }



  @wire(fetchShipmentForOrderSumary, { orderId: "$recordId"})
  getShipmentForOrderSumary({ error, data }) {
    if (data) {
      this.shipmentRecord = data;
      if(this.shipmentRecord[0].CCL_Shipment_Reason__c == 'RE' || this.shipmentRecord[0].CCL_Shipment_Reason__c == 'IN'){
      this.hpoNumber = this.shipmentRecord[0].CCL_Hospital_Purchase_Order_Number__c;
    }
    if(this.shipmentRecord[0].CCL_Order__r.CCL_PRF_Submitter__r.Name) {
      this.orderSubmitterName = this.shipmentRecord[0].CCL_Order__r.CCL_PRF_Submitter__r.Name;
    }
	if(this.hpoNumber!==undefined)
      {
        this.displayHPO = true;
      }
    }
    else if (error) {
      this.error = error;
    }
  }

  @wire(getUserName)
  wiredStepUserName({ error, data }) {
    if (data) {
      this.userName = data;
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }

  get paymentValue() {

    if(getFieldValue(this.orderrecord, PAYMENT_FIELD) == "KTAP"){
      this.showPaymentField = true;
    }
    return this.showPaymentField;
}
goToNextCommercialScreen() {
 this[NavigationMixin.Navigate]({
    type: "comm__namedPage",
    attributes: {
        name: 'Home'
    }
  });
    }

	get displayWarningMessage() {
      return (this.orderStatus == 'APH_OnHold' || this.orderStatus == 'MFG_OnHold' || this.orderStatus == 'SHP_OnHold');
    }

    get warningMessage() {
      let msg = '';
      if(this.orderStatus == 'APH_OnHold') {
        msg = aph_onHold_label;
      }
      if(this.orderStatus == 'MFG_OnHold') {
        msg = mfg_onHold_label;
      }
      if(this.orderStatus == 'SHP_OnHold') {
        msg = shp_onHold_label;
      }

      return msg;
    }

    handleReasonChange(event) {
      this.changeReasonForDataUpdate = event.target.value;
    }

    returnToModification()  {
      const updateOrderHeader = this.template.querySelector(".updateOrderHeader");
      updateOrderHeader.style.display = 'block';
      this.showconfirmation = false;
    }

    validateTextarea() {
      const textArea = this.template.querySelector(".textarea");
      let isvalid= false;
      let commentValue = textArea.value;
      if (commentValue == undefined || commentValue.trim() == '') {
        isvalid = false;
        textArea.setCustomValidity("Complete this Field");
        textArea.reportValidity();
      } else {
        isvalid = true;
        textArea.setCustomValidity("");
        textArea.reportValidity();
      }
      return isvalid;
    }
    onReasonSubmit(event) {
      if (this.validateTextarea()) {
        const updateOrderlwc = this.template.querySelector("c-c-c-l_-update_-order");
        updateOrderlwc.submitChangesWithReason(this.changeReasonForDataUpdate);
        this.returnToModification();
      }
    }

    displayConfirmation() {
      const updateOrderHeader = this.template.querySelector(".updateOrderHeader");
      updateOrderHeader.style.display = 'none';
      this.showconfirmation = true;
    }

    handleUpdateSuccess() {
      this.isEditOrder = false;
      this.showUpdateSuccess = true;
    }

    refreshOrderSummary() {
      window.open('/s/ccl-order/'+this.recordId,'_self');
    }

	//dasboard card starts here
setChevronJson(mainArr){
  return( [
  { label: ORDER_CHEVRON, status: mainArr.CCL_Order_Chevron__c!=undefined?mainArr.CCL_Order_Chevron__c:"",date: mainArr.CCL_Order_Chevron_Date__c!=undefined?
  mainArr.CCL_Order_Chevron_Date__c:'',value: '1' },
  { label: APHERESIS_CHEVRON,status: mainArr.CCL_Apheresis_Chevron__c!=undefined?mainArr.CCL_Apheresis_Chevron__c:"",date: mainArr.CCL_Apheresis_Chevron_Date__c
  !=undefined?mainArr.CCL_Apheresis_Chevron_Date__c:'', value: '2' },
  { label: MANUFACTURING_CHEVRON,status: mainArr.CCL_Manufacturing_Chevron__c!=undefined?mainArr.CCL_Manufacturing_Chevron__c:"",date: mainArr.CCL_Order_Chevron_Date__c
  !=undefined?mainArr.CCL_Manufacturing_Chevron_Date__c:'', value: '3' },
  { label: DELIVERY_CHEVRON,status: mainArr.CCL_Delivery_Chevron__c!=undefined?mainArr.CCL_Delivery_Chevron__c:"",date: mainArr.CCL_Delivery_Chevron_Date__c!=undefined?
  mainArr.CCL_Delivery_Chevron_Date__c:'', value: '4' },
  ]);

}

setChevronColor(mainArr){
  let colour={};
  if(mainArr.CCL_Order_Chevron__c!=undefined&&mainArr.CCL_Apheresis_Chevron__c!=undefined&&mainArr.CCL_Manufacturing_Chevron__c!=undefined&&
    mainArr.CCL_Delivery_Chevron__c!=undefined){
    if(mainArr.CCL_Order_Chevron__c==='Cancelled'){
      colour["currentStep"]=0;
      colour["completeStep"]=0;
      return colour;
    }
    else if(mainArr.CCL_Order_Chevron__c!="Confirmed"&&mainArr.CCL_Order_Chevron__c!="Cancelled"){
      colour["currentStep"]=1;
      colour["completeStep"]=0;
      return colour;
    }
    else if((mainArr.CCL_Apheresis_Chevron__c==="Planned Pick Up"||mainArr.CCL_Apheresis_Chevron__c==="Picked Up")
    &&mainArr.CCL_Manufacturing_Chevron__c=="Pending"&&mainArr.CCL_Delivery_Chevron__c==="Estimated Delivery"){
      colour["currentStep"]=2;
      colour["completeStep"]=1;
      return colour;
    }

    else if((mainArr.CCL_Apheresis_Chevron__c==="Received")
    &&(mainArr.CCL_Manufacturing_Chevron__c=="Pending"||
mainArr.CCL_Manufacturing_Chevron__c=="Terminated"||
mainArr.CCL_Manufacturing_Chevron__c=="Manufacturing Start On Hold")
    &&(mainArr.CCL_Delivery_Chevron__c==="Estimated Delivery"||mainArr.CCL_Delivery_Chevron__c==="Product Returned"||
    mainArr.CCL_Delivery_Chevron__c==="Product Delivery On Hold"||mainArr.CCL_Delivery_Chevron__c==="Cancelled")){
      colour["currentStep"]=0;
    colour["completeStep"]=2;
    return colour;
    }
    else if(mainArr.CCL_Apheresis_Chevron__c==="Apheresis Pick Up On Hold"){
      colour["currentStep"]=0;
    colour["completeStep"]=1;
    return colour;
    }
    else{
      colour=this.checkRest(mainArr);
      return colour;
    }
 }else{
      colour["currentStep"]=0;
        colour["completeStep"]=0;
        return colour;
    }
}
checkRest(mainArr){
let colour={};
 if((mainArr.CCL_Manufacturing_Chevron__c==="Manufacturing Started")
    &&mainArr.CCL_Delivery_Chevron__c==="Estimated Delivery"){
      colour["currentStep"]=3;
    colour["completeStep"]=2;
    return colour;
    }
    else if((mainArr.CCL_Manufacturing_Chevron__c==="QA testing started"||mainArr.CCL_Manufacturing_Chevron__c==="QA testing completed")
    &&(mainArr.CCL_Delivery_Chevron__c==="Estimated Delivery"||mainArr.CCL_Delivery_Chevron__c==="Shipped. Estimated Delivery")){
      colour["currentStep"]=4;
    colour["completeStep"]=3;
    return colour;
    }
    else if((mainArr.CCL_Manufacturing_Chevron__c==="QA testing started"||mainArr.CCL_Manufacturing_Chevron__c==="QA testing completed")
    &&(mainArr.CCL_Delivery_Chevron__c==="Product Returned"||
    mainArr.CCL_Delivery_Chevron__c==="Product Delivery On Hold"||mainArr.CCL_Delivery_Chevron__c==="Cancelled")){
      colour["currentStep"]=0;
    colour["completeStep"]=3;
    return colour;
    }
    else if(mainArr.CCL_Delivery_Chevron__c==="Delivered"){
      colour["currentStep"]=0;
    colour["completeStep"]=4;
    return colour;
    }
    else{
  colour["currentStep"]=4;
    colour["completeStep"]=2;
    return colour;
    }
}


formatNVSDate(date) {
  if(date!==null && date!==''&& date!==undefined){
  let dateVal=date.split(' ');
  let day='';
  if(dateVal[0].length<2){
    day='0'+dateVal[0];
  }else{
    day=dateVal[0];
  }
  return [day, dateVal[1],dateVal[2]].join(' ');
}
return '';
}

get displayPaymentFirstRow() {
  return this.displayHPO || (this.displayPatientCountry && this.patientCountryDisplayValue) || (this.displayBillParty && this.billingPartyDisplayValue);
}
get displayPaymentSecondRow() {
  return this.displayVA || this.paymentValue;
}

}
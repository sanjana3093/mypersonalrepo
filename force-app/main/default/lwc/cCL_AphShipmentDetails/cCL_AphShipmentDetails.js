import { LightningElement, api, wire, track } from "lwc";
import geFieldDetails from "@salesforce/apex/CCL_ADFController_Utility.getShipmentDetails";
import getAccountDetails from "@salesforce/apex/CCL_ADF_Controller.getRecords";
import getCryoBagDetails from "@salesforce/apex/CCL_ADFController_Utility.getCryoBagDetails";
import errMsg from "@salesforce/label/c.CCL_No_info_Available";
import pleaseSelect from "@salesforce/label/c.CCL_CryoBag_Conformation";
import DATE_TIME_ERR from "@salesforce/label/c.CCL_Enter_date_time_Err";
import CONSENT_ERR from "@salesforce/label/c.CCL_Cryobag_Error";
import successMsg from "@salesforce/label/c.CCL_Aph_Shiment_Details";
import getUserName from "@salesforce/apex/CCL_ADF_Controller.getUserName";
import saveDetails from "@salesforce/apex/CCL_ADF_Controller.saveCryo";
import { updateRecord } from "lightning/uiRecordApi";
import { NavigationMixin, CurrentPageReference } from "lightning/navigation";
import getPickUpTimeZone from "@salesforce/apex/CCL_ADFController_Utility.getTimeZone";
import backtoshiplist from "@salesforce/label/c.CCL_Back_to_Shipment_List";
import adfShipmntHeader from "@salesforce/label/c.CCL_Apheresis_Shipment_Details";
import shipmnetInfoHeader from "@salesforce/label/c.CCL_Shipment_Information";
import viewOrder from "@salesforce/label/c.CCL_View_Order_Summary";
import cryoBagHeader from "@salesforce/label/c.CCL_CryoBag_Batch_Information";
import dearTrackingHeader from "@salesforce/label/c.CCL_Dewar_Tracking_Information";
import cryobagId from "@salesforce/label/c.CCL_CryoBag_ID";
import cryoBagNo from "@salesforce/label/c.CCL_CryoBag_No";
import getADFFileDetails from "@salesforce/apex/CCL_ADFController_Utility.getADFFileDetails";
import getAllPermissionSets from "@salesforce/apex/CCL_ADFController_Utility.getAllPermissionSets";
import getADFStatus from "@salesforce/apex/CCL_ADFController_Utility.getAssociatedADFStatus";
import getOrderHardPeg from "@salesforce/apex/CCL_ADFController_Utility.getAssociatedOrderHardPeg";
import getFormattedDateTimeDetails from "@salesforce/apex/CCL_ADFController_Utility.getFormattedDateTimeDetails";
import {fireEvent} from 'c/cCL_Pubsub';
import CCL_Cancelled_Order_Msg from "@salesforce/label/c.CCL_Cancelled_Order_Msg";
import CCL_Shipment_Approval_Warning_Msg from "@salesforce/label/c.CCL_Shipment_Approval_Warning_Msg";
import CCL_To_Be_Confirmed_Label from "@salesforce/label/c.CCL_To_Be_Confirmed_Label";
import getShipmentList from "@salesforce/apex/CCL_ADFController_Utility.getShipmentInfo";
import getUserTimeZoneDetails from "@salesforce/apex/CCL_ADF_Controller.getUserTimeZoneDetails";
import getListView from "@salesforce/apex/CCL_ADFController_Utility.fetchListViews";
import getAddress from "@salesforce/apex/CCL_Utility.fetchAddress";
import CCL_AphPickUp_TimeZone from "@salesforce/label/c.CCL_AphPickUp_TimeZone";
import CCL_DIN from "@salesforce/label/c.CCL_DIN";
import CCL_Time_Zone from "@salesforce/label/c.CCL_Time_Zone";
import CCL_Time from "@salesforce/label/c.CCL_Time";
import CCL_Print_ADF from "@salesforce/label/c.CCL_Print_ADF";
import CCL_Actual_Dewar_Pck_Date from "@salesforce/label/c.CCL_Actual_Dewar_Pck_Date";
import CCL_Act_Cryo_AphPickUp from "@salesforce/label/c.CCL_Act_Cryo_AphPickUp";
import CCL_Apheresis_ID from "@salesforce/label/c.CCL_Apheresis_ID";
import CCL_LaterDate_Err from "@salesforce/label/c.CCL_LaterDate_Err";
import CCL_ActCryoAphPickUp_FutureErr from "@salesforce/label/c.CCL_ActCryoAphPickUp_FutureErr";
import CCL_ActDewarPkg_FutureErr from "@salesforce/label/c.CCL_ActDewarPkg_FutureErr";
import CCL_AphOnHold from "@salesforce/label/c.CCL_AphOnHold";
const ACTUAL_DEWAR__DATE = "CCL_Actual_Dewar_Packaging_Date__c";
const ACTUAL_DEWAR_TIME = "CCL_Actual_Dewar_Packaging_Time__c";
const ACTUAL_APH_PICKUP_DATE = "CCL_Actual_Cryo_Apheresis_Pick_up__c";
const ACTUAL_APH_PICKUP_TIME = "CCL_Actual_Apheresis_Pick_up_Time__c";
const ACTUAL_DEWAR_PKG_DT_TIME = "CCL_Actual_Dewar_Packaging_Date_Time__c";
const ACTUAL_DEWAR_PICKU_DT_TIME = "CCL_Actual_Aph_Pick_up_Date_Time__c";
const LAST_REPORTED_BY_SHPMNT = "CCL_Last_Reported_By_Shipment__c";
const LAST_REPORTED_BY = "CCL_Last_Reported_By_Packaging__c";
const ACTUAL_DEWAR_PKG_TXT = "CCL_TEXT_Actual_Dewar_Pkg_Date_Time__c";
const ACTUAL_PICKUP_DATE_TXT = "CCL_TEXT_Actual_Pick_up_Date__c";
const PLANNED_DWR_LABEL='Planned Charged Dewar Arrival Date/Time';
export default class CCL_AphShipmentDetails extends NavigationMixin(
  LightningElement
) {
  @api recordId;
  @track label = {
    backtoshiplist,
    adfShipmntHeader,
    shipmnetInfoHeader,
    viewOrder,
    cryoBagHeader,
    dearTrackingHeader,
    cryobagId,
    cryoBagNo, CCL_Cancelled_Order_Msg, CCL_AphOnHold, CCL_Print_ADF, CCL_Time_Zone
  };
  dewarErrmsg = CCL_ActDewarPkg_FutureErr;
  cryoErrMsg = CCL_ActCryoAphPickUp_FutureErr;
  pickupdateErr = CCL_LaterDate_Err;
  @api apiNames = [];
  @track sobj;
  @track checkDate;
  @track count=0;
  @track validDates=0;
  @track isLoading=false;
  @track updatedbag = [];
  @track shipperPermission = "CCL_Apheresis_Shipper";
  @track successMsg = successMsg;
  @track showSuccess = false;
  @track mainArr = [];
  @track pickupLocId;
  @track recordInput;
  @track statusVal=false;
  @track timeZone;
  @track isShipper;		
  @track consent = false;
  @track packageDateTimeErr = false;
  @track pickUpDateTimeErr = false;
  @track pickUpDateEarlier = false;
  @track hasResult = true;
  @track fieldValidations;
  @track userName;
   @api isLoaded=false;
    @track siteTimeZone=CCL_AphPickUp_TimeZone;
  @track hardPegVal= false;
  //358
  @track myDateFields = {};
  @track myTimeFields = {};
  @track myDateObj = {
    CCL_TEXT_Actual_Dewar_Pkg_Date_Time__c: "",
    CCL_TEXT_Actual_Pick_up_Date__c: ""
  };
  @track errMsg = errMsg;
  @track EMPTY_STRING = "";
  @track firstValue = {};
  @track fields = {};
  @track aphIdLabel;
  @track orderId;
  @track pleaseSelect = pleaseSelect;
  @track status;
  @api bagList = [];
  @track dewarArr = [];
  @track hasFieldvalues;
  @track hasData;
  @track consentError = false;
  @track contentDocumentId;
  @track disablePrintPDF = true;
  @track SCPurl;
  @track associatedADFStatus;
  @track associatedADFId;
  @wire(CurrentPageReference) pageRef;
  @track displayWarningMessage=false;
  @track shipmentDetails;
  @track orderHardPeg;
  @track associatedOrderId;
  @track isDewarChanged=false;
  @track isAphpickupChanged=false;
  @track timezoneKey;
  @track listViewid;
  @track displayApprovalWarning=false;
  @track dispalyApprovalWarningMsg =CCL_Shipment_Approval_Warning_Msg;
	 // 2450 code
 @track adressIds=[];
 @api addressList;
 @api siteIds=[];
 /*variable to set field editable but only apheresis shipper will be able to perform save operation*/
 @track hasEditAccess =false;
  @wire(getOrderHardPeg, { recordId: "$recordId" })
  wiredAssocidatedOrderDetails({ error, data }) {
    if (data) {
      this.orderHardPeg =data[0].CCL_Order_Apheresis__r!==undefined?data[0].CCL_Order_Apheresis__r.CCL_Hard_Peg__c:this.orderHardPeg;
      this.associatedOrderId =data[0].CCL_Order_Apheresis__r!==undefined?data[0].CCL_Order_Apheresis__r.Id:this.associatedOrderId;
      if(data[0].CCL_Order_Apheresis__c !=null && data[0].CCL_Order_Apheresis__r.CCL_Hard_Peg__c) {
        this.hardPegVal = true;
      }
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }
  @wire(geFieldDetails)
  wiredSteps({ error, data }) {
	   this.isLoading=true;
    if (data) {
		 this.isLoading=false;
      this.sobj = "CCL_Shipment__c";
      this.apiNames.push("CCL_CryoBag_Packing_Consent__c");
      this.apiNames.push("CCL_Order_Apheresis__c");
      this.apiNames.push(LAST_REPORTED_BY_SHPMNT);
      this.apiNames.push(LAST_REPORTED_BY);
      let self = this;
      data.forEach(function (node) {
       self.setFields(node,self);
      });
      data.forEach(function (node) {
        self.setDewarFields(node, self);
      });
       if(this.recordId){
      this.fetchValues();
      this.isLoaded=false;
     }

      this.error = null;
    } else if (error) {
	  this.isLoading=false;
      this.error = error;
    }
  }
  @wire(getUserName)
  wiredStepUserName({ error, data }) {
    if (data) {
      this.userName = data;
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }
  @wire(getPickUpTimeZone, { recordId: "$pickupLocId" })
  wiredTime({ error, data }) {
    if (data) {
      this.timeZone = data[0].CCL_Time_Zone__r.Name;
	  this.timezoneKey=data[0].CCL_Time_Zone__r.CCL_Saleforce_Time_Zone_Key__c;
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }
  @wire(getADFStatus, { recordId: "$recordId" })
  wiredAssocidatedADFDetails({ error, data }) {
    if (data) {
      this.associatedADFStatus =data[0].CCL_Apheresis_Data_Form__r!==undefined?data[0].CCL_Apheresis_Data_Form__r.CCL_Status__c:this.associatedADFStatus;
      this.associatedADFId =data[0].CCL_Apheresis_Data_Form__r!==undefined?data[0].CCL_Apheresis_Data_Form__r.Id:this.associatedADFId;
	  if(data[0].CCL_Status__c == 'Apheresis Pick Up Cancelled') {
		  this.statusVal = true;
      }
	  if( data[0].CCL_Status__c == 'Apheresis Pick Up On Hold'){
          this.displayWarningMessage=true;
         }
	 if( data[0].CCL_Order_Apheresis__r.CCL_Order_Approval_Eligibility__c
          && data[0].CCL_Order_Apheresis__r.CCL_PRF_Ordering_Status__c != 'PRF_Approved'  && data[0].CCL_Order_Apheresis__r.CCL_Initial_PRF_Approved__c){
           this.displayApprovalWarning=true;
         }
	this.error = null;
    } else if (error) {
      this.error = error;
    }
  }
@wire(getListView)
  wiredListView({ error, data }) {
    if (data) {
      this.listViewid = data[0].Id;
     
    } else if (error) {
      
      this.error = error;
    }
  }
  getShipmentDetails(){
    getShipmentList({recordId: this.associatedADFId})
      .then((result) => {
        this.shipmentDetails = result;
          this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }

  getSCPFile() {
    getADFFileDetails({
      recordId: this.associatedADFId
    })
      .then((result) => {
        this.contentDocumentId =
          result.length > 0
            ? result[0].ContentDocumentId
            : this.contentDocumentId;
			
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }

  getCryoBags() {
    this.pickupLocId = this.hasFieldvalues[0].CCL_Pick_up_Location__c;
    getCryoBagDetails({
      orderId: this.orderId
    })
      .then((result) => {
        this.bagList = result;
        let self = this;
        let bag = { CCL_Shipment__c: "", Id: "" };
        this.bagList.forEach(function (node) {
          bag = { CCL_Shipment__c: "", Id: "" };
          bag.Id = node.Id;
          bag.CCL_Shipment__c = self.recordId;
          self.updatedbag.push(bag);
        });
        if (this.isShipper) {
          this.hasResult = false;
        }
        if (result[0].CCL_Label_Compliant__c == "ISBT") {
          this.aphIdLabel = CCL_DIN;
        } else {
          this.aphIdLabel = CCL_Apheresis_ID;
        }
        this.error = null;
      })
      .catch((error) => {
        
        this.error = error;
      });
  }
  handleConsent(event) {
    this.fields.CCL_CryoBag_Packing_Consent__c = event.target.checked;
  }
  fetchValues() {
    //358
    this.apiNames = [...new Set(this.apiNames)];
    getAccountDetails({
      sobj: this.sobj,
      cols: this.apiNames.toString(),
      recordId: this.recordId
    })
      .then((result) => {
        
        this.hasFieldvalues = result;
        this.consent = result[0].CCL_CryoBag_Packing_Consent__c;
        this.orderId = result[0].CCL_Order_Apheresis__c;
        this.getCryoBags();
        this.setValues();
        this.error = null;
		  //2450
         if(this.adressIds.length>0) {
          this.getAddresses();
         }
      })
      .catch((error) => {
        
        this.error = error;
      });
  }
  setFields(mainArrr,self) {
    let fieldArr = {
      Fieldlabel: "",
      fieldApiName: "",
      fieldorder: "",
      fieldValue: "",
      fieldType: "",
      fieldAddress: "",
      isEditable: "",
      class: "",
      isTime: "",
      isDate: "",
      innerClass: "",
      outerClass: "",
      showWarning:"",
      isSite:"",
    };
    if (mainArrr.CCL_Order_of_field__c < 10) {
      fieldArr.Fieldlabel = mainArrr.MasterLabel;
      fieldArr.fieldType = mainArrr.CCL_Field_Type__c;
      fieldArr.fieldApiName = mainArrr.CCL_Field_API_Name__c;
      fieldArr.class =
        mainArrr.CCL_Field_Length__c == "Full"
          ? "slds-col slds-size_1-of-1 statusClass"
          : "slds-col slds-size_1-of-2 width40";

      fieldArr.isEditable = mainArrr.CCL_isEditable__c ? false : true;

      if (mainArrr.CCL_Field_Type__c == "Lookup") {
        fieldArr.isSite=true;
         this.setForLookUp(fieldArr,mainArrr);
      }
      //358
      fieldArr.outerClass = "labelIputs";
      fieldArr.innerClass = "labelClass myInputs";
      if (mainArrr.CCL_Field_Type__c == "time") {
        fieldArr.isTime = true;
        fieldArr.innerClass = "labelClass myInputs timeClass";
        fieldArr.class += " slds-grid width50 ";
      }
      if (mainArrr.CCL_Field_Type__c == "date") {
        fieldArr.isDate = true;
        fieldArr.outerClass = "labelIputs date-inputs";
        fieldArr.class += " slds-grid";
        fieldArr.innerClass = "labelClass myInputs dateClass";
      }
      fieldArr.Fieldlabel = this.setLabels(
        mainArrr.CCL_Field_Type__c,
        mainArrr.CCL_Field_API_Name__c,
        mainArrr.MasterLabel
      );
      this.apiNames.push(fieldArr.fieldApiName);
      fieldArr.fieldorder = mainArrr.CCL_Order_of_Field__c;
      if(fieldArr.fieldApiName==="CCL_TEXT_Planned_Cryro_Aphsersis_Pick_Up__c"){
        fieldArr.showWarning=true;
      }
      if(fieldArr.fieldApiName!='CCL_Planned_Aph_Pick_up_Date_Confirmed__c'){
        self.mainArr.push(fieldArr);
      }
    }
  }
  setLabels(type, apiName, label) {
    let myLabel = label;
    if (type == "date" && apiName == ACTUAL_DEWAR_PKG_TXT) {
      myLabel = CCL_Actual_Dewar_Pck_Date;
    } else if (type == "date" && apiName == ACTUAL_PICKUP_DATE_TXT) {
      myLabel = CCL_Act_Cryo_AphPickUp;
    } else if (type == "time" && apiName == ACTUAL_PICKUP_DATE_TXT) {
      myLabel = CCL_Time;
    } else if (type == "time" && apiName == ACTUAL_DEWAR_PKG_TXT) {
      myLabel = CCL_Time;
    }
    return myLabel;
  }
    setDewarFields(mainArrr, self) {
    if (mainArrr.CCL_Order_of_field__c >= 10) {
      let fieldArr = {
        Fieldlabel: "",
        fieldApiName: "",
        fieldorder: "",
        fieldValue: "",
        fieldType: "",
        fieldAddress: "",
        isEditable: "",
        class: "",
        isTime: "",
        isUrl: "",
        outerClass: "",
        iswaybillNo:'',
		 isSite:''
      };
      fieldArr.Fieldlabel = mainArrr.MasterLabel;
      fieldArr.outerClass = "labelIputs";
      fieldArr.fieldType = mainArrr.CCL_Field_Type__c;
      fieldArr.fieldApiName = mainArrr.CCL_Field_API_Name__c;
      fieldArr.class =
        mainArrr.CCL_Field_Length__c == "Full"
          ? "slds-size_1-of-1"
          : "slds-size_1-of-2 width40";
      fieldArr.isEditable = mainArrr.CCL_isEditable__c ? false : true;
      if (mainArrr.CCL_Field_Type__c == "Lookup") {
       this.setForLookUp(fieldArr,mainArrr);
       fieldArr.iswaybillNo= true;
	   //2450
       fieldArr.isSite=true;
      }
      if (mainArrr.CCL_Field_Type__c == "time") {
        fieldArr.isTime = true;
        fieldArr.class += " slds-grid width50";
      }
      if (mainArrr.CCL_Field_Type__c == "url") {
        fieldArr.isUrl = true;
      }
      if(mainArrr.CCL_Field_API_Name__c=='CCL_Waybill_number__c'){
        fieldArr.iswaybillNo=true;
      }
      this.apiNames.push(fieldArr.fieldApiName);
      fieldArr.fieldorder = mainArrr.CCL_Order_of_Field__c;
      self.dewarArr.push(fieldArr);
    }
  }
  setForLookUp(fieldArr,mainArrr){
    let address;
    fieldArr.fieldApiName =
    fieldArr.fieldApiName.substring(0, fieldArr.fieldApiName.length - 1) +
    "r.Name";
  if (mainArrr.CCL_HasAddress__c) {
    address =
      fieldArr.fieldApiName.substring(
        0,
        fieldArr.fieldApiName.length - 5
      ) + ".ShippingAddress";
    this.apiNames.push(address);
  }
  }
  setValues() {
    let temp = this.hasFieldvalues[0];
    this.status = temp.CCL_Status__c;
    let self = this;
    this.mainArr.forEach(function (node) {
      node.fieldValue =
        node.fieldType == "Lookup"
          ? temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)]
              .Id
          : temp[node.fieldApiName];
      node.fieldAddress =
        node.fieldType == "Lookup"
          ? temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)]
              .ShippingAddress
          : null;
      if (node.fieldType != "Lookup") {
        self.fields[node.fieldApiName] = node.fieldValue;
      }

      if (node.fieldType == "time") {
        node.fieldValue = self.setTime(
          node.fieldApiName,
          temp[node.fieldApiName]
        );
      }
      if (node.fieldType == "date") {
        node.fieldValue = self.setDate(
          node.fieldApiName,
          temp[node.fieldApiName]
        );
      }
      if(node.fieldApiName==="CCL_TEXT_Planned_Cryro_Aphsersis_Pick_Up__c"){
        node.fieldValue=temp[node.fieldApiName];
        node.fieldValue=self.setDateAndTimeFields(node.fieldValue);
      }
      if(node.Fieldlabel==PLANNED_DWR_LABEL){
        node.fieldValue=node.fieldValue==''||node.fieldValue==undefined?CCL_To_Be_Confirmed_Label:node.fieldValue;
      }
	    //2450
      if(node.isSite){
        self.adressIds.push(temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-6)+'c']);
      }
    });
	 this.setDewarValues(temp);
}
  setDewarValues(temp){
	   let self=this;
  if(this.status){
  fireEvent(this.pageRef, 'statusChange', this.status );
  }
  let lookUpValue;
   //2450
  let tbcLabel;
  this.dewarArr.forEach(function (node) {
    lookUpValue=temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)];
  if(node.fieldType=='Lookup'&&lookUpValue==undefined){
      node.fieldValue=CCL_To_Be_Confirmed_Label;
	   tbcLabel=true;
       node.isSite=false;
    }
    node.fieldValue =node.fieldType == "Lookup"&&lookUpValue!=undefined? temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)].Id: temp[node.fieldApiName];
    node.fieldAddress =
      node.fieldType == "Lookup"&&lookUpValue!=undefined
        ? temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)]
            .ShippingAddress
        : null;
    node.fieldValue =node.fieldType == "Lookup"&&!self.hardPegVal?CCL_To_Be_Confirmed_Label:node.fieldValue;
	//2450
     if(node.isSite &&!tbcLabel){
      self.adressIds.push(temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-6)+'c']);
    }
  });
}
  setDateAndTimeFields(nodeValue){
    
    let fieldValue=nodeValue;
    let dateValue='';
    let finalVal='';
    if(fieldValue!==undefined && fieldValue!==''){
      let fields=fieldValue.split(' ');
      dateValue=fields[0]+' '+fields[1]+' '+fields[2];
      if(fields[3]==='00:00'){
        finalVal=dateValue;
      }else{
        finalVal=nodeValue;
      }
    }
    return finalVal;
    }
  //358
  setTime(apiName, value) {
    if(value!=undefined){
      this.myTimeFields[apiName] = value.substr(12, 18);
    }
    return "";
  }
  //358
  setDate(apiName, value) {
    if(value!=undefined){
    this.myDateFields[apiName] = value.substr(0, 11);
    }
    return "";
  }
  //358
  formatDate(date) {
     let mydate = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    mydate=new Date(date);
  }
    let monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];

    let day = "" + mydate.getDate();
    if (day.length < 2) day = "0" + day;
    let monthIndex = mydate.getMonth();
    let monthName = monthNames[monthIndex];
    let year = mydate.getFullYear();
    return [day,monthName, year].join(" ");
  }
   handleChange(event) {
    this.fields["Id"] = this.recordId;
    let apiName = event.target.dataset.item;
    let comp = this.template.querySelectorAll(".dateClass");
    this.fields[apiName] = event.target.value;
    if (apiName in this.myDateObj && event.target.type == "date") {
      this.myDateFields[apiName] = this.formatDate(event.target.value);
      comp.forEach(function (node) {
        node.value = "";
      });
    }
    if (apiName in this.myDateObj && event.target.type == "time") {
      this.myTimeFields[apiName] = event.target.value.substr(0,11);
    }

    if (event.target.dataset.item == ACTUAL_DEWAR_PKG_TXT) {
	  this.isDewarChanged=true;
      this.fields[ACTUAL_DEWAR_PKG_TXT] =
        this.myDateFields[apiName] +
        " " +
        this.myTimeFields[apiName].substr(0,5) +
        " " +
        this.timeZone;
    }
    if (event.target.dataset.item == ACTUAL_PICKUP_DATE_TXT) {
	  this.isAphpickupChanged=true;
      this.fields[ACTUAL_PICKUP_DATE_TXT] =
        this.myDateFields[apiName] +
        " " +
        this.myTimeFields[apiName] +
        " " +
        this.timeZone;
    }
  }

  clearText(event){
    let apiName = event.target.dataset.item;
    this.myDateFields[apiName] = null;
    this.myTimeFields[apiName]=null
    if(apiName==ACTUAL_PICKUP_DATE_TXT){
      this.fields[ACTUAL_DEWAR_PICKU_DT_TIME]='';
      this.fields[ACTUAL_PICKUP_DATE_TXT]='';
    }else if(apiName==ACTUAL_DEWAR_PKG_TXT){
      this.fields[ACTUAL_DEWAR_PKG_DT_TIME]='';
      this.fields[ACTUAL_DEWAR_PKG_TXT]='';
    }
  }
  handleBlur(event){
    let apiName = event.target.dataset.item;
    let comp2 = this.template.querySelectorAll(".timeClass");
    if (apiName in this.myDateObj && event.target.type == "time") {
      this.myTimeFields[apiName] = event.target.value;
      comp2.forEach(function (node) {
        node.value = "";
      });
    }

  }
  handleSave(event) {
    this.handleValidation();
    const allValid = [...this.template.querySelectorAll(".myInputs")].reduce(
      (validSoFar, inputCmp) => {
        inputCmp.reportValidity();
        return validSoFar && inputCmp.checkValidity();
      },
      true
    );
	this.validDates=0;
    if (allValid) {
      if (
        this.myDateFields[ACTUAL_DEWAR_PKG_TXT] &&
        this.myTimeFields[ACTUAL_DEWAR_PKG_TXT] &&
		this.isDewarChanged
      ) {
		   this.validDates+=1;
		   this.isDewarChanged=false;
        this.convertDate(
          this.myDateFields[ACTUAL_DEWAR_PKG_TXT],
          this.myTimeFields[ACTUAL_DEWAR_PKG_TXT],
          LAST_REPORTED_BY,
          ACTUAL_DEWAR_PKG_DT_TIME,
          ACTUAL_DEWAR_PKG_TXT
        );
      }
      if (
        this.myDateFields[ACTUAL_PICKUP_DATE_TXT] &&
        this.myTimeFields[ACTUAL_PICKUP_DATE_TXT] &&
		this.isAphpickupChanged
      ) {
		   this.validDates+=1;
		   this.isAphpickupChanged=false;
        this.convertDate(
          this.myDateFields[ACTUAL_PICKUP_DATE_TXT],
          this.myTimeFields[ACTUAL_PICKUP_DATE_TXT],
          LAST_REPORTED_BY_SHPMNT,
          ACTUAL_DEWAR_PICKU_DT_TIME,
          ACTUAL_PICKUP_DATE_TXT
        );
      }
    }
	if(this.validDates==0){  
      this.saveData();
    }
  }
  handleValidation() {
    let currentDate = new Date();
    let currentDate2 = new Date();
    this.checkValidation();
    if (
      this.myDateFields[ACTUAL_DEWAR_PKG_TXT] != undefined &&
      this.myTimeFields[ACTUAL_DEWAR_PKG_TXT] != undefined &&
      this.myDateFields[ACTUAL_PICKUP_DATE_TXT] != undefined &&
      this.myTimeFields[ACTUAL_PICKUP_DATE_TXT] != undefined
    ) {
      let time1 = this.myTimeFields[ACTUAL_DEWAR_PKG_TXT].substr(0, 5).split(
        ":"
      );
      let time2 = this.myTimeFields[ACTUAL_PICKUP_DATE_TXT].substr(0, 5).split(
        ":"
      );
      currentDate.setHours(time1[0], time1[1], 0);
      currentDate2.setHours(time2[0], time2[1], 0);
      let dewarTime = currentDate.getTime();
      let pickupTime = currentDate2.getTime();
      if (
        new Date(this.myDateFields[ACTUAL_DEWAR_PKG_TXT]).getDate() ==
          new Date(this.myDateFields[ACTUAL_PICKUP_DATE_TXT]).getDate() &&
        new Date(this.myDateFields[ACTUAL_DEWAR_PKG_TXT]).getMonth() ==
          new Date(this.myDateFields[ACTUAL_PICKUP_DATE_TXT]).getMonth() &&
        new Date(this.myDateFields[ACTUAL_DEWAR_PKG_TXT]).getFullYear() ==
          new Date(this.myDateFields[ACTUAL_PICKUP_DATE_TXT]).getFullYear() &&
        dewarTime > pickupTime
      ) {
        this.pickUpDateEarlier = true;
      } else if (
        new Date(this.myDateFields[ACTUAL_DEWAR_PKG_TXT]) >
        new Date(this.myDateFields[ACTUAL_PICKUP_DATE_TXT])
      ) {
        this.pickUpDateEarlier = true;
      } else {
        this.pickUpDateEarlier = false;
      }
    }
     if (
      (!this.fields.CCL_CryoBag_Packing_Consent__c &&
        this.fields.CCL_CryoBag_Packing_Consent__c != undefined)&&this.bagList.length>0 ||
      (this.fields.CCL_CryoBag_Packing_Consent__c == undefined && !this.consent&&this.bagList.length>0)
    )  {
      this.consentError = true;
    } else {
      this.consentError = false;
    }
    this.fieldValidations = this.template.querySelectorAll(".myInputs");
    for (let i = 0; i < this.fieldValidations.length; i++) {
      this.fieldValidations[i].setCustomValidity(this.EMPTY_STRING);
      this.fieldNullCheck(this.fieldValidations[i]);
    }
  }
  checkValidation() {
    if (
      (this.myDateFields[ACTUAL_DEWAR_PKG_TXT] == undefined &&
        this.myTimeFields[ACTUAL_DEWAR_PKG_TXT] != undefined) ||
      (this.myDateFields[ACTUAL_DEWAR_PKG_TXT] != undefined &&
        this.myTimeFields[ACTUAL_DEWAR_PKG_TXT] == undefined)
    ) {
      this.packageDateTimeErr = true;
    } else {
      this.packageDateTimeErr = false;
    }

    if (
      (this.myDateFields[ACTUAL_PICKUP_DATE_TXT] == undefined &&
        this.myTimeFields[ACTUAL_PICKUP_DATE_TXT] != undefined) ||
      (this.myDateFields[ACTUAL_PICKUP_DATE_TXT] != undefined &&
        this.myTimeFields[ACTUAL_PICKUP_DATE_TXT] == undefined)
    ) {
      this.pickUpDateTimeErr = true;
    } else {
      this.pickUpDateTimeErr = false;
    }

    if (
      this.myDateFields[ACTUAL_PICKUP_DATE_TXT] != undefined &&
      this.myDateFields[ACTUAL_DEWAR_PKG_TXT] != undefined
    ) {
      if (
        new Date(this.myDateFields[ACTUAL_DEWAR_PKG_TXT].substr(0, 11)) >
        new Date(this.myDateFields[ACTUAL_PICKUP_DATE_TXT].substr(0, 11))
      ) {
        this.pickUpDateEarlier = true;
      } else {
        this.pickUpDateEarlier = false;
      }
    }
  }

  fieldNullCheck(comboCmp) {
    if (
      comboCmp.dataset.item == ACTUAL_DEWAR_PKG_TXT &&
      this.packageDateTimeErr
    ) {
      this.checkForDateTime(comboCmp,ACTUAL_DEWAR_PKG_TXT);
    }
    if (
      comboCmp.dataset.item == ACTUAL_PICKUP_DATE_TXT &&
      this.pickUpDateTimeErr
    ) {
      this.checkForDateTime(comboCmp,ACTUAL_PICKUP_DATE_TXT)
    }
    if (comboCmp.name == "consent" && this.consentError) {
      comboCmp.setCustomValidity(CONSENT_ERR);
      comboCmp.reportValidity();
    }
    if (
      comboCmp.dataset.item == ACTUAL_PICKUP_DATE_TXT &&
      this.pickUpDateEarlier&&comboCmp.type=='date'
    ) {
      comboCmp.setCustomValidity(this.pickupdateErr);
      comboCmp.reportValidity();
    } else if (
      comboCmp.dataset.item == ACTUAL_PICKUP_DATE_TXT &&
      !this.pickUpDateTimeErr &&
      !this.pickUpDateEarlier
    ) {
      comboCmp.setCustomValidity("");
      comboCmp.reportValidity();
    }
    this.validateForDate(comboCmp);
  }
  checkForDateTime(comboCmp,DATE_FIELD){
    let value;
    if(comboCmp.type=='date'){
      value=this.myDateFields[DATE_FIELD];
      if(value==undefined){
        comboCmp.setCustomValidity(DATE_TIME_ERR);
        comboCmp.reportValidity();
      }
    }else{
      value=this.myTimeFields[DATE_FIELD];
      if(value==undefined){
        comboCmp.setCustomValidity(DATE_TIME_ERR);
        comboCmp.reportValidity();
      }
    }
  }
  validateForDate(comboCmp) {
    if (comboCmp.type === "date") {
      let date = new Date();
      let inputDate = new Date(this.myDateFields[comboCmp.dataset.item]);
      if (date < inputDate) {
        if (comboCmp.dataset.item == ACTUAL_DEWAR_PKG_TXT && !this.packageDateTimeErr &&comboCmp.type=='date') {
          comboCmp.setCustomValidity(this.dewarErrmsg);
          comboCmp.reportValidity();
        }
        if (comboCmp.dataset.item == ACTUAL_PICKUP_DATE_TXT && !this.pickUpDateTimeErr && comboCmp.type=='date') {
          comboCmp.setCustomValidity(this.cryoErrMsg);
          comboCmp.reportValidity();
        }
      } else if (
        (!this.packageDateTimeErr &&
          comboCmp.dataset.item == ACTUAL_DEWAR_PKG_TXT) ||
        (!this.pickUpDateTimeErr &&
          !this.pickUpDateEarlier &&
          comboCmp.dataset.item == ACTUAL_PICKUP_DATE_TXT)
      ) {
        comboCmp.setCustomValidity("");
        comboCmp.reportValidity();
      }
    }
  }
   convertDate(date, time, field, dateTimeField, checkField) {
getUserTimeZoneDetails({
    timeZoneVal: this.timezoneKey
  })
    .then((result) => {
      let userTimeZone = result;
      
      if (
        userTimeZone !== undefined &&
        userTimeZone !== null &&
        userTimeZone !== ""
      ) {
        let fields = userTimeZone.split("T");
        let currentTime = fields[1];
        let currentDate = fields[0];
        let timeValue=currentTime.split(":");
		
	let firstName= this.userName.FirstName? this.userName.FirstName:'';
      this.fields[field] =
        firstName +
        " " +
        this.userName.LastName +
        " " +
        this.formatDate(currentDate) +
        " " +
        timeValue[0]+':'+timeValue[1]+
        " " +
        this.timeZone;
     this.getFormattedDateTimeVal(date, time, dateTimeField, checkField);
      }
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
      
    });
  }
  msToTime(currentDate) {
    let mins =currentDate.getMinutes();
    let hrs =currentDate.getHours();
    hrs = hrs < 10 ? "0" + hrs : hrs;
    mins = mins < 10 ? "0" + mins : mins;
    return hrs + ":" + mins;
  }
  handleClose() {
    this.showSuccess = false;
  }
  navigateToView() {
	  let self=this;
   this[NavigationMixin.Navigate]({
      type: 'standard__objectPage',
      attributes: {
          objectApiName: 'CCL_Shipment__c',
          actionName: 'list',
      },
      state: {
        filterName: self.listViewid
    }
  });
  }
  navigateToViewHome() {
    this[NavigationMixin.Navigate]({
      type: 'standard__recordPage',
            attributes: {
                recordId: this.orderId,
                objectApiName: 'CCL_Order__c',
                actionName: 'view'
            },
    });
  }
  connectedCallback() {
	   this.isLoaded=true;
      getAllPermissionSets()
        .then((result) => {
          let self = this;
          result.forEach(function (node) {
            if(node.PermissionSet.Name == 'CCL_Apheresis_Shipper') {
              self.isShipper =true;
              self.hasEditAccess = true;
            }
            if(node.PermissionSet.Name == 'CCL_Customer_Operations' || node.PermissionSet.Name == 'CCL_ADF_Viewer_Internal') {

              self.hasEditAccess = true;
            }
          });
      
          this.error = null;
        })
        .catch((error) => {
          
          this.error = error;
        });
  }
  renderedCallback() {
	   if(this.isLoaded){
      this.fetchValues();
      this.isLoaded=false;
     }
    this.getSCPFile();
    if (
      this.contentDocumentId !== undefined &&
      this.associatedADFStatus === "ADF Approved"
    ) {
      this.disablePrintPDF = false;
    }
	if(this.statusVal){
    this.disablePrintPDF = true;
    this.hasResult=true;
    this.isShipper=false;   
    const displayCheck = this.template.querySelector(".footerDiv");
    if(!displayCheck.classList.contains('displayNone')){
      displayCheck.classList.add('displayNone');
    }
   }
  }
  viewPdf() {
    this.SCPurl =
      "/sfc/servlet.shepherd/document/download/" + this.contentDocumentId;
    window.open(this.SCPurl);
  }

  getFormattedDateTimeVal(date, time, dateTimeField, checkField) {
    let currentDate = new Date();
    let dateTime = this.formatDateFormat(date.substr(0, 11)) + " " + time.substr(0,5)+':00.000';
    getFormattedDateTimeDetails({
      dateTimeVal: dateTime,
      timeZoneVal: this.timeZone
    })
      .then((result) => {
        this.formattedDateTime = result;
        this.fields[dateTimeField] = this.formattedDateTime;
        let resultDate = new Date(result);
        let nowDate = new Date(currentDate.toISOString());
        if (
          resultDate.getTime() > nowDate.getTime() &&
          nowDate.getDate() == resultDate.getDate() &&
          nowDate.getMonth() == resultDate.getMonth() &&
          nowDate.getYear() == resultDate.getYear()
        ) {
          this.validateTime(checkField);
        } else if( nowDate < resultDate){
          this.validateTime(checkField);
        }
        else {
          const allValid = [...this.template.querySelectorAll(".myInputs")].reduce(
            (validSoFar, inputCmp) => {
              inputCmp.reportValidity();
              return validSoFar && inputCmp.checkValidity();
            },
            true
          );
          this.count=allValid?this.count+1:this.count;
          if(this.count==this.validDates || allValid){
            this.saveData();
            this.count==0;
          }
          if(this.count>2){
            this.count=0
          }
        }
      })
      .catch((error) => {
        this.error = error;
        
      });
  }
  validateTime(field) {
    let self = this;
    this.template.querySelectorAll(".myInputs").forEach(function (node) {
      if (node.dataset.item == field) {
        if (node.dataset.item == ACTUAL_DEWAR_PKG_TXT) {
          node.setCustomValidity(self.dewarErrmsg);
          node.reportValidity();
        }
        if (node.dataset.item == ACTUAL_PICKUP_DATE_TXT) {
          node.setCustomValidity(self.cryoErrMsg);
          node.reportValidity();
        }
      }
    });
  }
  formatDateFormat(date) {
    if (date !== null && date !== "" && date !== undefined) {
      let d = new Date(date),
        month = "" + (d.getMonth() + 1),
        day = "" + d.getDate(),
        year = d.getFullYear();

      if (month.length < 2) month = "0" + month;
      if (day.length < 2) day = "0" + day;

      return [year, month, day].join("-");
    }

    return "";
  }
  saveData() {
    const allValid = [...this.template.querySelectorAll(".myInputs")].reduce(
      (validSoFar, inputCmp) => {
        inputCmp.reportValidity();
        return validSoFar && inputCmp.checkValidity();
      },
      true
    );
    if (allValid) {
      this.recordInput = { fields: this.fields };
      this.fields["Id"] = this.recordId;
	  delete  this.fields['CCL_Status__c'];
	  this.isLoading=true;
      saveDetails({
        bags: this.updatedbag,
        summaryLst: [],
        comments: null,
        adfId: null
      })
        .then((result) => {
          //updating the save json varible
          
          this.error = null;
        })
        .catch((error) => {
	     this.isLoading=false;
          
          this.error = error;
        });
      updateRecord(this.recordInput)
        .then(() => {
          
          this.showSuccess = true;
		   this.isLoading=false;
		  this.template.querySelector('.wrapperDiv').scrollIntoView({ block: 'start', behavior: 'smooth' });
          this.fetchValues();
        })
        .catch((error) => {
          this.showSuccess = false;
		   this.isLoading=false;
          
        });
    }
  }
    //2450
   getAddresses(){
    getAddress({
      accountIds: this.adressIds,
    })
      .then((result) => {
        this.addressList=result;
        
        this.error = null;
      })
      .catch((error) => {
        
        this.error = error;
      });
  }

  get chkbxbInp() {
    return !this.hasEditAccess;
  }
 
}
import {
    LightningElement,
    track,
    api,
    wire
} from "lwc";
//import getFileDetails from '@salesforce/apex/CCL_PRF_Controller.getUploadedFileDetails';

import getFileDetails from "@salesforce/apex/CCL_ADFController_Utility.getUploadedFileDetails";
import fetchAphShipmentRecords from '@salesforce/apex/CCL_PRF_Controller.fetchAphShipmentRecords';
import checkInternalPermission from '@salesforce/apex/CCL_PRF_Controller.checkInternalPermission';

import getcustomdoc from "@salesforce/apex/CCL_ADFController_Utility.getDocId";
import CCL_Document_Upload_Msg from "@salesforce/label/c.CCL_Document_Upload_Msg";
import deleteFile from "@salesforce/apex/CCL_ADFController_Utility.deleteFile";
import CCL_DeleteConfirmation_Title from "@salesforce/label/c.CCL_DeleteConfirmation_Title";
import CCL_DeleteConfirmation_Message from "@salesforce/label/c.CCL_DeleteConfirmation_Message";
import CCL_DeleteConfirmation_Ok from "@salesforce/label/c.CCL_DeleteConfirmation_Ok";
import CCL_DeleteConfirmation_Cancel from "@salesforce/label/c.CCL_DeleteConfirmation_Cancel";
import checkUserPermission from "@salesforce/apex/CCL_ADFController_Utility.checkUserPermission";
import updateOrderDocument from "@salesforce/apex/CCL_Utility.updateOrderDocument";
import CCL_Additional_Documentation from '@salesforce/label/c.CCL_Additional_Documentation';
import { refreshApex } from "@salesforce/apex";


export default class CCLAphAdditionalDocumentation extends LightningElement {

label={
  CCL_Additional_Documentation
};
    @api orderId;
    @api recordId;
    @track filesUploaded = [];
    @track showUploadedFiles = false;
    @api aphShipmentList;
    @api status;
    @api aphPickUpLocation;
    @api plannedAphPickupDate;
    @api actualAphPickupDate;
    @api actualAphReceiptDate;
    @api street;
    @api aphShipmentId;
    @track isFileUpload=false;
    @track isDialogVisible=false;

    @track adfDocId;
    @track originalMessage;
  @track displayMessage =
    "Click on the 'Open Confirmation' button to test the dialog.";
  @track fileUrl;
  @track idPosition;
  @track docVersionId;
  @track isAdfSubmitter = false;
  @track renderDelete = false;
  @track fileResultDeleted;
  @track patientConsentError=false;
  @track prfSubmitter="CCL_PRF_Submitter";
  @track isPrfSubmitter=false;
  @track prfApprover="CCL_PRF_Approver";
  @track prfViewer="CCL_PRF_Viewer";
  @track isPrfViewer=false;
  @track isPrfApprover=false;
  @track custOp="CCL_Customer_Operations";
  @track isCustOP=false;
  @track viewUpload=false;

    @track labels={
        CCL_DeleteConfirmation_Title,
        CCL_DeleteConfirmation_Message,
        CCL_DeleteConfirmation_Ok,
        CCL_DeleteConfirmation_Cancel,
        CCL_Document_Upload_Msg
    }


    @wire(fetchAphShipmentRecords, {
        orderId: "$orderId"
    })
    fetchAphShipmentRecords({
        error,
        data
    }) {
        if (data) {
            this.disableFileUpload();
            this.aphShipmentList = data;
            this.aphShipmentId = this.aphShipmentList[0].Id;
            this.status = this.aphShipmentList[0].CCL_Status__c;
            this.aphPickUpLocation = this.aphShipmentList[0].CCL_Pick_up_Location__r.ShippingAddress;
            this.plannedAphPickupDate = this.aphShipmentList[0].CCL_TEXT_Planned_Cryro_Aphsersis_Pick_Up__c;
            this.actualAphPickupDate = this.aphShipmentList[0].CCL_TEXT_Actual_Pick_up_Date__c;
            this.actualAphReceiptDate = this.aphShipmentList[0].CCL_Actual_Aph_Receipt_Date_Time_Text__c;

            if (this.aphPickUpLocation !== null) {
                this.street = this.aphPickUpLocation.street;
                this.city = this.aphPickUpLocation.city;
                this.state = this.aphPickUpLocation.state;
                this.postalCode = this.aphPickUpLocation.postalCode;
                this.country = this.aphPickUpLocation.country;

            }
        } else if (error) {
            this.error = error;
        }
    }

    /*@wire(getFileDetails, {
        recordId: "$adfDocId"
    })
    wiredFileListSteps({
        error,
        data
    }) {
        if (data) {
            this.filesUploaded = data;
            if (this.filesUploaded.length > 0) {
                this.showUploadedFiles = true;
           }
            this.error = null;
        } else if (error) {
            this.error = error;
        }
    }*/

    get acceptedFormats() {
        return [".docx", ".txt", ".xls", ".csv", ".ppt", ".jpg", ".jpeg", ".png",".pdf"];
      }

    connectedCallback(){
          checkUserPermission({
            permissionName: this.prfSubmitter
          })
            .then((result) => {
                this.isPrfSubmitter = result;
              this.error = null;
              checkUserPermission({
                permissionName: this.prfApprover
                })
                .then((result) => {
                    this.isPrfApprover = result;
                    this.error = null;
                    checkUserPermission({
                        permissionName: this.prfViewer
                        })
                        .then((result) => {
                        this.isPrfViewer = result;
                            this.error = null;
                            checkUserPermission({
                                permissionName: this.custOp
                                })
                                .then((result) => {
                                this.isCustOP = result;
                                    if(this.isPrfApprover||this.isPrfSubmitter||this.isPrfViewer|| this.isCustOP){
                                        this.viewUpload=true;
                                    }
                                    this.error = null;
                                })
                                .catch((error) => {
                                    this.error = error;
                                });
                        })
                        .catch((error) => {
                            this.error = error;
                        });
                })
                .catch((error) => {
                    this.error = error;
                });
            })
            .catch((error) => {
              this.error = error;
            });
    }


    @wire(getFileDetails, { recordId: "$adfDocId" })
  wiredFileListSteps({ error, data }) {
    if (data) {
        this.filesUploaded = data;
      if (this.filesUploaded.length > 0) {
        this.showUploadedFiles = true;
      }

      this.error = null;
    } else if (error) {
        this.error = error;
    }
  }
  handleDeleteFinished() {
    return refreshApex(this.fileResultDeleted);
  }
  handleUploadFinished(event) {
	   /*Logic to send document Id to apex in order to update backend field, which in turn
      triggers the email notification*/
      updateOrderDocument({docId:this.adfDocId})
      .then((result) => {

      })
      .catch((error) => {
      });
    return refreshApex(this.fileResult);
  }

  @wire(getFileDetails, { recordId: "$adfDocId" })
  imperativeFile(result) {
      this.fileResult = result;
    if (result.data) {
      this.filesUploaded = result.data;
      if (this.filesUploaded.length > 0) {
        this.showUploadedFiles = true;
      }
    }
  }
  //added as part of 355
  @wire(getFileDetails, { recordId: "$adfDocId" })
  imperativeDeleteFile(result) {
      this.fileResultDeleted = result;
    if (result.data) {
      this.filesUploaded = result.data;
      if (this.filesUploaded.length > 0) {
        this.showUploadedFiles = true;
      }
    }
  }
    //added as part of 2298
    @wire(getcustomdoc, { recordId: "$orderId" })
  wiredDocId({ error, data }) {
    if (data) {
      this.adfDocId = data[0].Id;
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }

    viewDocument(event) {
        this.isFileViewModalOpen = true;
        this.url = '/sfc/servlet.shepherd/document/download/' + event.target.name;
        window.open(this.url)
    }



    /**************Check if user is internal user, then disable fileUpload***********************/
    disableFileUpload() {
        checkInternalPermission().then(result => {
    if (result) {
     this.isFileUpload = result;
   } else if (result.error) {
   this.error = result.error;
    }
 }).catch(error => {
   this.error=error;
 })
}

//Changes as part of CGTU 355
removeFile(event) {
    if (event.target.name === "openConfirmation") {
        let jsonToDelete;
        this.docVersionId = event.target.alternativeText;
        for(let i=0; i<this.fileResult.data.length;i++){
            if(this.fileResult.data[i].ContentDocumentId===this.docVersionId){
                jsonToDelete=this.fileResult.data[i];
                break;
            }
        }
        if(JSON.stringify(jsonToDelete).includes("CCL_Document_Classification__c")){
            if(jsonToDelete.ContentDocument.LatestPublishedVersion.CCL_Document_Classification__c=="Patient Consent"){
                this.isDialogVisible = false;
                this.patientConsentError=true;
            }
            else{
                this.isDialogVisible = true;
            }
        }
        else{
            this.isDialogVisible = true;
        }
    }else if (event.target.name === "confirmModal") {
        //when user clicks outside of the dialog area, the event is dispatched with detail value  as 1
        if (event.detail !== 1) {
          if (event.detail.status === "confirm") {
            deleteFile({ contentDocumentId: this.docVersionId })
              .then((result) => {
                this.handleDeleteFinished();
                this.isDialogVisible = false;
                this.error = null;
              })
              .catch((error) => {
                this.error = error;
              });
            //Changes as part of 355 ends here
            this.isDialogVisible = false;
          } else if (event.detail.status === "cancel") {
            this.isDialogVisible = false;
          }
        }
        //hides the component
        this.isDialogVisible = false;
      }
    }

  handleOk(event){
    this.patientConsentError=false;
}


}
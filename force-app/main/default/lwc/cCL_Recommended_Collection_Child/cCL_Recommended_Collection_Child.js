import { LightningElement, api, track, wire } from "lwc";
import { getRecord,updateRecord } from "lightning/uiRecordApi";
import futureDateError from "@salesforce/label/c.CCL_Future_testDate_Error";
import futureTimeError from "@salesforce/label/c.CCL_Future_TestTime_Error";
import getPageTimeZoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getPageTimeZoneDetails";
import getFormattedDateTimeDetails from "@salesforce/apex/CCL_ADFController_Utility.getFormattedDateTimeDetails";
const SITE_LOCATION = "CCL_Site__c";
const TEXT_API_FIELD = "CCL_Date_Time_Text_API__c";
const FIELD_API_NAME = "CCL_Date_Time_field_API__c";
const CCL_TEXT_CBC_Differential_Date_Time__c="CCL_TEXT_CBC_Differential_Date_Time__c";
import getTimezoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getTimeZoneDetails";
import enterBothDateTime from "@salesforce/label/c.CCL_Enter_both_Date_and_Time";
//imported as part of 850
//364
import badTimeInput from "@salesforce/label/c.CCL_Enter_time_in_12_hour_format";
import reasonForModification from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Reason_For_Modification__c";
import CCL_Approval_Counter__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Approval_Counter__c";
import ID from "@salesforce/schema/CCL_Apheresis_Data_Form__c.Id";
import CCL_Reason_Modal_Header from "@salesforce/label/c.CCL_Reason_Modal_Header";
import CCL_Reason_Modal_Message from "@salesforce/label/c.CCL_Reason_Modal_Message";
import CCL_Reason_Modal_Helptext from "@salesforce/label/c.CCL_Reason_Modal_Helptext";
import CCL_Reason_Modal_Subheading from "@salesforce/label/c.CCL_Reason_Modal_Subheading";
import CCL_TimeZone from "@salesforce/label/c.CCL_TimeZone";
import CCL_Cancel_Button from "@salesforce/label/c.CCL_Cancel_Button";
import CCL_Date_Of_Testing from "@salesforce/label/c.CCL_Date_Of_Testing";
import CCL_Time from "@salesforce/label/c.CCL_Time";
import CCL_Modal_Close from "@salesforce/label/c.CCL_Modal_Close";
import CCL_Submit from "@salesforce/label/c.CCL_Submit";
import CCL_Utility_modal_Cancel from "@salesforce/label/c.CCL_Utility_modal_Cancel";
import CCL_Save_Changes from "@salesforce/label/c.CCL_Save_Changes";

export default class CCL_Recommended_Collection_Child extends LightningElement {
  @api inputValueJSON = [];
  @api collectionid;
  @api apiNames = [];
  @track hasDates=false;
   //changes for 364
  @track validDates=0;
  @track count=0;
  @track fields = {};
  @track myDateFields={};
  @track myTimeFields={}
  @track CCL_Date_of_Testing__c;
  @track CCL_Date_of_Testing_Flow_Cytometry__c;
  @track CCL_Date_of_Testing_Absolute_Counts__c;
  @api readOnlyfields=false;
  @api storedArr = [];
  @track dateVal;
  @api index;
  @api myFormatDates;
  @api isADFViewerInternal;
  @api myFormatTimes;
  @track selrec = [];
  @track recordInput;
  @track myValue;
  @api sectionArr = [];
  @track saveLaterValue;
  @track mydate=new Date();
  @api navigateFurther = false;
  @api futureDate = false;
  @api timeZoneDetails;
  @api allTimeZoneDetails;
  @track screennameVal="Recommended Collection Details";
  @track siteTimeZone;
  @api textFields=[];
  @api dateTimeFields=[];
  @api formattedDateTime;
  @api recordIdVal;
  @api siteTimeZoneVal;
  
  
 @api labels = {
      CCL_Reason_Modal_Header,
      CCL_Reason_Modal_Message,
      CCL_Reason_Modal_Helptext,
      CCL_Reason_Modal_Subheading,
      CCL_Save_Changes,
      CCL_Utility_modal_Cancel,
      CCL_Submit,
      CCL_Modal_Close,
      CCL_TimeZone
    };

  //added as part of 850
  @track isReasonModalOpen=false;
  @track ReasonForMod;
  @track reason;
  @track navigate;
  @track counter;
  @track orignalJson;
  @track values;
  @api commentfileflag=false;
  @api
  get commentorfilechange(){
    return this.myValue;
  }

  @wire(getRecord, { recordId: "$recordId", fields:[reasonForModification,CCL_Approval_Counter__c] })
  Reason({ error, data }) {
    if (error) {
      this.error = error;
     
    } else if (data) {
     
      this.reason = data.fields.CCL_Reason_For_Modification__c.value;
      this.counter=data.fields.CCL_Approval_Counter__c.value;
      if(this.counter===null){
        this.counter=0;
      }
     
    }
  };


  
  @api
  get saveclicked() {
    return this.myValue;
  }
  @api
  get myActualArr() {
    return "my value";
  }

  @api
  get timeZone() {
    return this.timeZoneDetails;
  }

  set recordId(recordId) {
   
    this.recordIdVal=recordId;
    this.getTimeZoneDetailsVal();
  }
		
	set commentorfilechange(flag){
     if(flag){
    this.commentfileflag=flag;}
  }	
		
  @api
  get recordId() {
    return this.recordIdVal;
  }

  set timeZone(timezoneVal) {
    this.timeZoneDetails=timezoneVal;
  }

  connectedCallback() {
    this.getTimeZoneScreenDetails();
  }

  getTimeZoneDetailsVal(){
    getTimezoneDetails({
      screenName:this.screennameVal, adfId: this.recordIdVal
    })
      .then((result) => {
        this.timeZoneDetails = result;
        this.siteTimeZoneVal = this.timeZoneDetails[CCL_TEXT_CBC_Differential_Date_Time__c];
       
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }

  getTimeZoneScreenDetails(){
    getPageTimeZoneDetails({screenName:this.screennameVal})
      .then((result) => {
        let self=this;
        this.allTimeZoneDetails = result;
        this.siteTimeZone='Time zone of '+this.allTimeZoneDetails[0][SITE_LOCATION];
        this.allTimeZoneDetails.forEach(function (node) {
          let newArr = {API_NAME: "",TEXT_API_NAME:""};
          newArr.API_NAME=node[FIELD_API_NAME];
          newArr.TEXT_API_NAME=node[TEXT_API_FIELD];
          self.textFields.push(newArr);
        });
      })
      .catch((error) => {
        this.error = error;
      });
  }

 //changes for 364
  getFormattedDateTimeVal(dateTimeVal,timeZoneVal,dateTimeField,field){
    let formattedVal='';
    let currentDate = new Date();
    getFormattedDateTimeDetails({dateTimeVal:dateTimeVal,timeZoneVal:timeZoneVal})
      .then((result) => {
        this.formattedDateTime = result;
        formattedVal=this.formattedDateTime;
        this.fields[dateTimeField] = this.formattedDateTime;
        this.recordInput = { fields: this.fields };
        let resultDate = new Date(result);
        let nowDate = new Date(currentDate.toISOString());
        if (
          resultDate.getTime() > nowDate.getTime() &&
          nowDate.getDate() == resultDate.getDate() &&
          nowDate.getMonth() == resultDate.getMonth() &&
          nowDate.getYear() == resultDate.getYear()
        ) {
          this.validateTime(field);
        } else if( nowDate < resultDate){
          this.validateTime(field);
        }else{
            this.count+=1;
        }
        if(this.count==this.validDates){
          this.updateRecordValues();
        }
        if(this.count>3){
          this.count=0;
        }
      })
      .catch((error) => {
        this.error = error;
        
      });
      return formattedVal;
  }
  validateTime(field) {
    this.template.querySelectorAll("lightning-input").forEach(function (node) {
      if (node.dataset.item == field && node.type=='time') {
          node.setCustomValidity(futureDateError);
          node.reportValidity();
      }
    });
    const selectEvent2 = new CustomEvent("saveclickedfalse", {});
    this.dispatchEvent(selectEvent2);
  }

  set myActualArr(prValue) {
   
    this.getTimeZoneDetailsVal();
    this.sectionArr = prValue;
    let myValues;
    let self = this;
   if (this.index != undefined) {
      let index = this.index + 1;
      if(this.selrec!=undefined){
      for (let [key, value] of Object.entries(this.selrec)) {
          if(value.Id==this.collectionid){
              myValues=value;
          }
      }
    }
	  this.values=myValues;
    }

    if (
      this.sectionArr.length > 0 &&
      myValues != undefined &&
      myValues != null
    ) {
      for (let i = 0; i < this.sectionArr.length; i++) {
        let newArr = { sectioname: "", myFields: [] };
        newArr.sectioname = this.sectionArr[i].sectioname;
        this.sectionArr[i].myFields.forEach(function (node) {
          let fieldArr = {
            Fieldlabel: "",
            fieldApiName: "",
            fieldtype: "",
            fieldorder: "",
            fieldValue: "",
            fieldhelptext: "",
            placeHolder: "",
            showStep: "",
            max:"",
            rangeOverflow:"",
            isDate:'',
            class:'',
            isTime:'',
            timeZone:'',
            stepMisMatch:"",
			badInput:"",
			min:"",
            rangeUnderflow:""
          };
          fieldArr.Fieldlabel = node.Fieldlabel;
          fieldArr.fieldApiName = node.fieldApiName;
          fieldArr.fieldtype = node.fieldtype;
          fieldArr.fieldorder = node.fieldorder;
          fieldArr.fieldhelptext = node.fieldhelptext;
          fieldArr.showStep = node.showStep;
          fieldArr.max = node.max;
          fieldArr.rangeOverflow = node.rangeOverflow;
          fieldArr.placeHolder = node.placeHolder;
          fieldArr.stepMisMatch = node.stepMisMatch;
		  fieldArr.min = node.min;
          fieldArr.rangeUnderflow = node.rangeUnderflow;
          fieldArr.fieldValue = myValues[node.fieldApiName];
          self.fields[fieldArr.fieldApiName] = myValues[node.fieldApiName];
          fieldArr.class='myClass';
          if(fieldArr.fieldtype==='Date' ||fieldArr.fieldtype=='date'){
            fieldArr.isDate=true;
            let dateVal='';
            if(fieldArr.fieldValue!==undefined && fieldArr.fieldValue!==null && fieldArr.fieldValue!==''){
              dateVal=fieldArr.fieldValue.substr(0,12);
            }
            self.myDateFields[node.fieldApiName]=self.formatDate(dateVal);
            fieldArr.fieldValue='';
            fieldArr.placeHolder='';
            fieldArr.class='myClass dateClass slds-size_1-of-2 slds-grid';
          }
          if(fieldArr.fieldtype=='Time' ||fieldArr.fieldtype=='time'){
            fieldArr.isTime=true;
			fieldArr.badInput=badTimeInput;
            let timeVal='';
            if(fieldArr.fieldValue!==undefined && fieldArr.fieldValue!==null && fieldArr.fieldValue!==''){
              if(fieldArr.fieldValue.substr(12,23)!==self.timeZoneDetails[node.fieldApiName]){
                timeVal=fieldArr.fieldValue.substr(12,23);
              }
            }
            self.myTimeFields[node.fieldApiName]=self.getFormattedTime(timeVal);
            fieldArr.fieldValue='';
            fieldArr.placeHolder='';
            if(self.timeZoneDetails!==undefined && self.timeZoneDetails[node.fieldApiName]!==undefined){
              fieldArr.timeZone=self.timeZoneDetails[node.fieldApiName];}
            fieldArr.class='slds-grid myClass timeClass slds-col slds-size_1-of-2';
          }
          if(self.readOnlyfields){
            fieldArr.placeHolder = "";
            if(fieldArr.fieldtype==='Time'){
              fieldArr.Fieldlabel = CCL_Time;
              let timeVal='';
              if(myValues[node.fieldApiName]!==undefined && myValues[node.fieldApiName]!==null && myValues[node.fieldApiName]!==''){
                timeVal=myValues[node.fieldApiName].substr(12,23);
              }
              fieldArr.fieldValue = self.getFormattedTime(timeVal);
              fieldArr.fieldtype="text";
              fieldArr.class='myClass';
            }else if(fieldArr.fieldtype==='Date' || fieldArr.fieldtype==='date'){       
              fieldArr.Fieldlabel = CCL_Date_Of_Testing;
              let dateVal='';
              if(myValues[node.fieldApiName]!==undefined && myValues[node.fieldApiName]!==null && myValues[node.fieldApiName]!==''){
                dateVal=myValues[node.fieldApiName].substr(0,12);
              }
              fieldArr.fieldValue=self.formatDate(dateVal);
              fieldArr.isDate=true;
              fieldArr.fieldtype="text";
              fieldArr.class='myClass';
            }
          }
          newArr.myFields.push(fieldArr);
        });
        this.storedArr.push(newArr);
      }
      this.fields["Id"] = this.collectionid;
      this.recordInput = { fields: this.fields };
    }
  }

  convertDate(date, time, field, dateTimeField) {
    let timeZoneVal='';
    let self=this;
    if(this.timeZoneDetails!==undefined){
    timeZoneVal=this.timeZoneDetails[field];
    }
	//changes for 364
    let formattedVal=this.getFormattedDateTimeVal(this.formatDateFormat(date) + " " +this.getFormattedTimeForTimeField(time),timeZoneVal,dateTimeField,field);
  }
  msToTime(s) {
    let ms = s % 1000;
    s = (s - ms) / 1000;
    let secs = s % 60;
    s = (s - secs) / 60;
    let mins = s % 60;
    let hrs = (s - mins) / 60;
    hrs = hrs < 10 ? "0" + hrs : hrs;
    mins = mins < 10 ? "0" + mins : mins;
    return hrs + ":" + mins + ":00.000";
  }

  set saveclicked(prvalue) {
    //changes for 364
     if (prvalue) {
      this.myValue = true;
      this.futureDateValidity();
     this.navigateFurther=this.checkIfError();
     if( this.navigateFurther){
      this.setDateTimeTextFields();
      this.recordInput = { fields: this.fields };
      }else if(!this.navigateFurther){
        const selectEvent2 = new CustomEvent("saveclickedfalse", {});
        this.dispatchEvent(selectEvent2);
      }
    }
  }

	compareJson(orignalJson,newJson){
    let oJson=orignalJson.fields;
    let nJson=newJson;
    if(oJson.CCL_TEXT_CBC_Differential_Date_Time__c!=nJson.CCL_TEXT_CBC_Differential_Date_Time__c||oJson.CCL_Hgb__c!=nJson.CCL_Hgb__c||
      oJson.CCL_PLT__c!=nJson.CCL_PLT__c||oJson.CCL_TEXT_Absolute_Counts_Date_Time__c!=nJson.CCL_TEXT_Absolute_Counts_Date_Time__c||
      oJson.CCL_White_Blood_Cell_Count__c!=nJson.CCL_White_Blood_Cell_Count__c||oJson.CCL_Absolute_Lymphocyte_Counts__c!=nJson.CCL_Absolute_Lymphocyte_Counts__c||
      oJson.CCL_TEXT_Flow_Cytometry_Date_Time__c!=nJson.CCL_TEXT_Flow_Cytometry_Date_Time__c||oJson.CCL_Absolute_CD3_Lymphocyte_Count__c
      !=nJson.CCL_Absolute_CD3_Lymphocyte_Count__c){
        return false;
    }
	return true;
  }

  updateRecordValues(){
	  //364
    if(this.counter>0){
          if (this.navigateFurther && !this.futureDate) {

            let updateReasonDetails = {
              fields: this.fields,
              index: this.index+1
            };
            
         
              const selectEvent = new CustomEvent('updatereasonevent', {
                  detail: updateReasonDetails
              });
              this.dispatchEvent(selectEvent);
      
        }
      }
      
    else if (this.recordInput != undefined && this.navigateFurther && !this.futureDate&&!this.isADFViewerInternal) {
      updateRecord(this.recordInput)
        .then(() => {
			this.fields['Id']=this.collectionid;
            const selectEvent = new CustomEvent('mycustomevent', {
                 detail: this.fields
            });
			let validationDetails = {
				navigateFurther: this.navigateFurther,
				Id: this.collectionid
			};
            this.dispatchEvent(selectEvent);
            const selectEvent1 = new CustomEvent("myvalidationevent", {
                detail: validationDetails
            });
            this.dispatchEvent(selectEvent1);
            })
	}else if(this.recordInput==undefined || this.isADFViewerInternal){
        this.fields['Id']=this.collectionid;
        const selectEvent = new CustomEvent('mycustomevent', {
            detail: this.fields
        });
        let validationDetails = {
        navigateFurther: this.navigateFurther,
        Id: this.collectionid
          };
          this.dispatchEvent(selectEvent);
          const selectEvent1 = new CustomEvent("myvalidationevent", {
            detail: validationDetails
          });
          this.dispatchEvent(selectEvent1);
        }
    
  }

  @api
  get savelaterclicked() {
    return this.saveLaterValue;
  }
  set savelaterclicked(prvalue) {
    if (prvalue) {
      this.saveLaterValue = true;
	  this.fields["Id"] = this.collectionid;
      const selectEvent = new CustomEvent("mycustomevent", {
        detail: this.fields
      });
      this.dispatchEvent(selectEvent);
    }
  }
  handleChange(event) {
    this.fields[event.target.dataset.item] = event.target.value;
    let key=event.target.dataset.item;
    
    let dateInputs=this.template.querySelectorAll('.dateClass');
    let timeInputs=this.template.querySelectorAll('.timeClass');
    
    if(key in this.myFormatDates && event.target.classList.contains('dateClass')){
      this.hasDates=true;
      
     this[key]=this.formatDate(event.target.value);
      this.myDateFields[key]=this.formatDate(event.target.value);
    }
    if(key in this.myFormatTimes && event.target.classList.contains('timeClass')){
      this.myTimeFields[key]=event.target.value;
    }
    dateInputs.forEach(function (node){
    node.value='';
    });
    timeInputs.forEach(function (node){
    //  node.value='';
      });
    let dateTime=this.myDateFields[key]+' '+this.myTimeFields[key];
    
    this.fields["Id"] = this.collectionid;
    if(event.target.dataset.item=='CCL_TEXT_CBC_Differential_Date_Time__c'){
      this.fields['CCL_TEXT_CBC_Differential_Date_Time__c']= this.myDateFields[key]+" "+this.myTimeFields[key]+' '+this.timeZoneDetails[key];
    }else if(event.target.dataset.item=='CCL_TEXT_Absolute_Counts_Date_Time__c'){
      this.fields['CCL_TEXT_Absolute_Counts_Date_Time__c']= this.myDateFields[key]+" "+this.myTimeFields[key]+' '+this.timeZoneDetails[key];
    } else if(event.target.dataset.item=='CCL_TEXT_Flow_Cytometry_Date_Time__c'){
      this.fields['CCL_TEXT_Flow_Cytometry_Date_Time__c']= this.myDateFields[key]+" "+this.myTimeFields[key]+' '+this.timeZoneDetails[key];
    }
    this.recordInput = { fields: this.fields };
  }

  handleBlur(event){
        let apiName = event.target.dataset.item;
        let comp2 = this.template.querySelectorAll(".timeClass");
        
        if (apiName in this.myFormatDates && event.target.classList.contains('timeClass')) {
          this.myTimeFields[apiName] = event.target.value;
          comp2.forEach(function (node) {
            node.value = "";
          });
        }
    
      }

  @api
  get storedValues() {
    return this.selrec;
  }
  set storedValues(prvalue) {
    
    this.getTimeZoneDetailsVal();
    this.selrec = prvalue;
    let myValues;
    let self = this;
    if (this.index != undefined) {
      let index = this.index + 1;
      myValues = this.selrec != undefined ? this.selrec[index] : null;
    }

    if (
      this.sectionArr.length > 0 &&
      myValues != undefined &&
      myValues != null
    ) {
      for (let i = 0; i < this.sectionArr.length; i++) {
        let newArr = { sectioname: "", myFields: [] };
        newArr.sectioname = this.sectionArr[i].sectioname;
        this.sectionArr[i].myFields.forEach(function (node) {
          let fieldArr = {
            Fieldlabel: "",
            fieldApiName: "",
            fieldtype: "",
            fieldorder: "",
            fieldValue: "",
            fieldhelptext: "",
            placeHolder: "",
            showStep: "",
            max:"",
            rangeOverflow:"",
            isDate:'',
            isTime:'',
            timeZone:'',
            stepMisMatch:"",
			badInput:"",
			min:"",
            rangeUnderflow:""
          };
          fieldArr.Fieldlabel = node.Fieldlabel;
          fieldArr.fieldApiName = node.fieldApiName;
          fieldArr.fieldtype = node.fieldtype;
          fieldArr.fieldorder = node.fieldorder;
          fieldArr.fieldhelptext = node.fieldhelptext;
          fieldArr.placeHolder = node.placeHolder;
          fieldArr.showStep = node.showStep;
          fieldArr.max = node.max;
          fieldArr.rangeOverflow = node.rangeOverflow;
		  fieldArr.stepMisMatch = node.stepMisMatch;
		  fieldArr.min = node.min;
          fieldArr.rangeUnderflow = node.rangeUnderflow;
          fieldArr.fieldValue = myValues[node.fieldApiName];
          fieldArr.class='myClass';
          self.fields[fieldArr.fieldApiName] = myValues[node.fieldApiName];
          if(fieldArr.fieldtype==='Date' ||fieldArr.fieldtype=='date'){
            fieldArr.isDate=true;
            let dateVal='';
            if(fieldArr.fieldValue!==undefined && fieldArr.fieldValue!==null && fieldArr.fieldValue!==''){
              dateVal=fieldArr.fieldValue.substr(0,12);
            }
            self.myDateFields[node.fieldApiName]=self.formatDate(dateVal);
            fieldArr.fieldValue='';
            fieldArr.placeHolder='';
            fieldArr.class='myClass dateClass';
          }
          if(fieldArr.fieldtype=='Time' ||fieldArr.fieldtype=='time'){
            fieldArr.isTime=true;
			fieldArr.badInput=badTimeInput;
            let timeVal='';
              if(fieldArr.fieldValue!==undefined && fieldArr.fieldValue.substr(12,23)!==self.timeZoneDetails[node.fieldApiName]){
                timeVal=fieldArr.fieldValue.substr(12,23);
              }
            self.myTimeFields[node.fieldApiName]=self.getFormattedTime(timeVal);
            fieldArr.fieldValue='';
            fieldArr.placeHolder='';
            if(self.timeZoneDetails!==undefined && self.timeZoneDetails[node.fieldApiName]!==undefined){
              fieldArr.timeZone=self.timeZoneDetails[node.fieldApiName];}
            fieldArr.class='myClass timeClass';
          }
          if(self.readOnlyfields){
            fieldArr.placeHolder = "";
            if(fieldArr.fieldtype==='Time'){
              fieldArr.Fieldlabel = CCL_Time;
              let timeVal='';
              if(myValues[node.fieldApiName]!==undefined && myValues[node.fieldApiName]!==null && myValues[node.fieldApiName]!==''){
                timeVal=myValues[node.fieldApiName].substr(12,23);
              }
              fieldArr.fieldValue = self.getFormattedTime(timeVal);
              fieldArr.fieldtype="text";
              fieldArr.class='myClass';
            }else if(fieldArr.fieldtype==='Date' || fieldArr.fieldtype==='date'){       
              fieldArr.Fieldlabel = CCL_Date_Of_Testing;
              let dateVal='';
              if(myValues[node.fieldApiName]!==undefined && myValues[node.fieldApiName]!==null && myValues[node.fieldApiName]!==''){
                dateVal=myValues[node.fieldApiName].substr(0,12);
              }
              fieldArr.fieldValue=self.formatDate(dateVal);
              fieldArr.isDate=true;
              fieldArr.fieldtype="text";
              fieldArr.class='myClass';
            }
          }
          newArr.myFields.push(fieldArr);
        });
        this.storedArr.push(newArr);
      }
      this.fields["Id"] = this.collectionid;
      this.recordInput = { fields: this.fields };
    }
  }

  setDateTimeTextFields(){
    //changes for 364
    let self=this;
    self.validDates=0;
    this.count=0
    this.allTimeZoneDetails.forEach(function (node) {
      if (self.myDateFields[node[TEXT_API_FIELD]] && self.myTimeFields[node[TEXT_API_FIELD]]) {
        self.validDates+=1;
        self.convertDate(
          self.myDateFields[node[TEXT_API_FIELD]],
          self.myTimeFields[node[TEXT_API_FIELD]],
          node[TEXT_API_FIELD],
          node[FIELD_API_NAME]
        );
      }
    });
    if(self.validDates==0){
      this.updateRecordValues();
    }
  }

  getFormattedTime(fieldValue){
    let timeval=fieldValue;
    if(timeval!==null && timeval!=='' && timeval!==undefined){
      let format;
      let hrs = timeval.substr(0, 2);
      let min = timeval.substr(3, 2);
      return hrs+':'+min;}
      return '';
}

getFormattedTimeForTimeField(fieldValue){
  
  let timeval=fieldValue;
  if(timeval!==null && timeval!=='' && timeval!==undefined){
    let format;
    let hrs = timeval.substr(0, 2);
    let min = timeval.substr(3, 2);
    return hrs+':'+min +":00.000";}
    return '';
}

formatDate(date) {
  if(date!==null && date!==''&& date!==undefined){
  let mydate = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    mydate=new Date(date);
  }
  let monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];
  let day = '' + mydate.getDate();
  if (day.length < 2)
  day = '0' + day;
  
  let monthIndex = mydate.getMonth();
  let monthName = monthNames[monthIndex];
  let year = mydate.getFullYear();
  return [ day, monthName,year].join(' ');
}
return '';
}

formatDateFormat(date) {
  if(date!==null && date!==''&& date!==undefined){
   let mydate = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    mydate=new Date(date);
  }
  let monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];

  let day = mydate.getDate();
  let monthIndex = mydate.getMonth();
  let year = mydate.getFullYear();
  return ` ${year}-${monthIndex+1}-${day}`;
}
return '';
}

  @api
  get summarypage() {
    return false;
  }
  set summarypage(prvalue) {
    
    if (prvalue) {
      
        this.readOnlyfields=true;
    }
  }
  @api
    futureDateValidity(){
    //364  changes
    let inputs=this.template.querySelectorAll('lightning-input');
    for (let i = 0; i < inputs.length; i++) {
      this.checkForDateTime(inputs[i]);
    if (inputs[i].type === "date" && inputs[i].checkValidity()) {
      let date = new Date();
      this.mydate = new Date(this.myDateFields[inputs[i].getAttribute('data-item')]);
      if (date < this.mydate&&(this.myTimeFields[inputs[i].dataset.item]!=undefined&&
        this.myTimeFields[inputs[i].dataset.item].length>0)) {
        inputs[i].setCustomValidity(futureDateError);
        inputs[i].reportValidity();
        this.futureDate=true;
      }
      else{
        inputs[i].setCustomValidity("");
        inputs[i].reportValidity();
        this.futureDate=false;
      }
      }
    else if(inputs[i].type === "time" && inputs[i].checkValidity()){
      let currentDateTime= new Date();
      let inputDateTime=new Date();
      let currentTime= currentDateTime.getTime();
      let time=this.myTimeFields[inputs[i].getAttribute('data-item')]!==undefined?this.myTimeFields[inputs[i].getAttribute('data-item')].split(":"):'';
      inputDateTime.setHours(time[0], time[1], 0);
      let inputTime=inputDateTime.getTime();
      if(inputTime>currentTime && this.mydate.getDate()===currentDateTime.getDate() && this.mydate.getMonth()===currentDateTime.getMonth()
      && this.mydate.getFullYear()===currentDateTime.getFullYear()){
        
        inputs[i].setCustomValidity(futureTimeError);
        inputs[i].reportValidity();
        this.navigateFurther = false;
      }
      else{
        inputs[i].setCustomValidity("");
        inputs[i].reportValidity();
        this.futureDate=false;
      }
    }
    }
  }
  //364 changes
  checkForDateTime(inputCmp){
    let key=inputCmp.dataset.item;
    let time=this.myTimeFields[key];
    let date=this.myDateFields[key];
    if(inputCmp.type=='date'|| inputCmp.type=='time'){
     if((time==null||time==undefined)&&(date!=undefined&& date.length>0)&&inputCmp.type=='time'){
        inputCmp.setCustomValidity(enterBothDateTime);
        inputCmp.reportValidity();
    this.navigateFurther = false;
      }
      else if(time!=null&&time.length<=0&&(date!=undefined&& date.length>0)&&inputCmp.type=='time'){
        inputCmp.setCustomValidity(enterBothDateTime);
        inputCmp.reportValidity();
    this.navigateFurther = false;
      }
     
      else if((time!=null&&time.length>0) && (date==undefined||date==null||date==' '||(date.length==0&&date!=undefined)) && inputCmp.type=='date'){
        inputCmp.setCustomValidity(enterBothDateTime);
        inputCmp.reportValidity();
      }else{
      inputCmp.setCustomValidity('');
      inputCmp.reportValidity();
    }
    }
  }
  checkIfError(){
    //change for 364
    const allValid = [...this.template.querySelectorAll("lightning-input")].reduce(
      (validSoFar, inputCmp) => {
        inputCmp.reportValidity();
        return validSoFar && inputCmp.checkValidity();
      },
      true
    );
    
    return allValid;
  
  }
  //changes as part of 850
closeReasonModal() {
  const selectEvent2 = new CustomEvent("saveclickedfalse", {});
  this.dispatchEvent(selectEvent2);
  this.commentfileflag=false;
  this.isReasonModalOpen=false;
}
handleReasonChange(event){
  this.ReasonForMod=event.target.value;
}
validateTextarea(){
  const textArea=this.template.querySelector('.Reason');
  let commentValue=textArea.value;
  if(commentValue==undefined){
    this.navigater=false;
    textArea.setCustomValidity('Complete this Field');
    textArea.reportValidity();
  }else{
    this.navigate=true;
    textArea.setCustomValidity('');
    textArea.reportValidity();
  }
}
onReasonSubmit(event){
  this.validateTextarea();
  if(this.navigate){
  this.updateReason();}
}
updateReason(){
  const fields = {};
  fields[ID.fieldApiName] = this.recordId;
  fields[reasonForModification.fieldApiName] =this.ReasonForMod;
  const recordInput = { fields };
   if(this.isADFViewerInternal){
    this.fields['Id']=this.collectionid;
    const selectEvent = new CustomEvent('mycustomevent', {
        detail: this.fields
    });
    let validationDetails = {
      navigateFurther: this.navigateFurther,
      Id: this.collectionid
    };
    this.dispatchEvent(selectEvent);
    const selectEvent1 = new CustomEvent("myvalidationevent", {
      detail: validationDetails
    });
    this.dispatchEvent(selectEvent1);
  this.isReasonModalOpen=false;
  }else{
    updateRecord(recordInput)
    .then(() => {
      if (this.recordInput != undefined && this.navigateFurther && !this.futureDate) {
        updateRecord(this.recordInput)
          .then(() => {
            
          })
          .catch((error) => {
            
          });
          this.fields['Id']=this.collectionid;
            const selectEvent = new CustomEvent('mycustomevent', {
                detail: this.fields
            });
            let validationDetails = {
              navigateFurther: this.navigateFurther,
              Id: this.collectionid
            };
            this.dispatchEvent(selectEvent);
            const selectEvent1 = new CustomEvent("myvalidationevent", {
              detail: validationDetails
            });
            this.dispatchEvent(selectEvent1);
          this.isReasonModalOpen=false;
      }
    }).catch((error) => {
      this.error = error
    });
  }
 
    }
  //changes for 850 end here
}
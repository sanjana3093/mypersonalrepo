import { LightningElement, api, wire, track } from 'lwc';
import fetchAphShipmentRecords from '@salesforce/apex/CCL_PRF_Controller.fetchAphShipmentRecords';
import fetchShipmentForOrderUpdate from '@salesforce/apex/CCL_PRF_Controller.fetchShipmentForOrderUpdate';
import { getObjectInfo, getPicklistValues } from "lightning/uiObjectInfoApi";
import CCL_ORDER_OBJ from "@salesforce/schema/CCL_Order__c";
import Cancel_Reason from '@salesforce/schema/CCL_Order__c.CCL_Treatment_Cancellation_Reason_Code__c';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import NOVARTIS_BATCH_ID from '@salesforce/schema/CCL_Order__c.CCL_Novartis_Batch_ID__c';
import ORDERSTATUS from '@salesforce/schema/CCL_Order__c.CCL_Treatment_Status__c';
import fetchADFDetails from '@salesforce/apex/CCL_PRF_Controller.fetchADFDetails';
import updateOrderForCancelReason from '@salesforce/apex/CCL_PRF_Controller.updateOrderForCancelReason';
import orderCannotCancelled from "@salesforce/label/c.CCL_Order_cannot_cancelled";
import orderAlreadyCancelled from "@salesforce/label/c.CCL_Order_already_cancelled";
import wantToCancelOrder from "@salesforce/label/c.CCL_Want_to_cancel_order";
import cancelNovartisBatchId from "@salesforce/label/c.CCL_Cancel_Novartis_BatchID";
import actionCannotBeUndone from "@salesforce/label/c.CCL_action_cannot_be_undone";
import doNotHaveOrderCancelPerm from "@salesforce/label/c.CCL_Donot_Have_Permission_Order_Cancellation";
import back from "@salesforce/label/c.CCL_Back";
import cancelOrder from "@salesforce/label/c.CCL_Cancel_Order";
import checkInternalPermission from "@salesforce/apex/CCL_PRF_Controller.checkInternalPermission";

const FIELDS = [NOVARTIS_BATCH_ID,ORDERSTATUS];

export default class CCL_OrderCancel extends LightningElement {

    doNotCancelOrder = false;
    @api canCancelOrder = false;
    @api recordId;
    @api aphShipmentId;
    @api aphShipmentStatus;
    @api aphShipmentList;
    @api shipmentdata;
    @track newArr;
    @api cancelReason
    @track status;
    orderData;
    @track novartisBatchId;
    @api cannotCancelOrder = false;
    @api oderToBeUpdated;
    @api loaded = false;
    @api apheresisList;
    @api adfId;
    @api order_status;
    @api cancelledOrder = false;
    @api userHasPermission;
	errorOnUpdate = false;
	
	label = { orderCannotCancelled, orderAlreadyCancelled, wantToCancelOrder, cancelNovartisBatchId,
         actionCannotBeUndone, doNotHaveOrderCancelPerm, back,cancelOrder };

    connectedCallback() {
        checkInternalPermission()
        .then(result => {
        const hasPermission = result;
            if (hasPermission) {
                this.userHasPermission = false;
            }
            else {
                this.userHasPermission = true;
            }
        })
        .catch(error => {
            this.error = error;
        });
    }

    // wire to get order object data
    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredRecord({ error, data }) {
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
        } else if (data) {
            this.orderData = data;
            this.novartisBatchId = this.orderData.fields.CCL_Novartis_Batch_ID__c.value;
            this.order_status = this.orderData.fields.CCL_Treatment_Status__c.value;
			this.getShipmentDetails();
            if  (this.order_status == 'Cancelled') {
                this.cancelledOrder = true;
            }
        }
    }

    //function to fetch the cancellation reason picklist values
	@wire(getObjectInfo, {
		objectApiName: CCL_ORDER_OBJ
	})
	objectInfo;

	@wire(getPicklistValues, {
		recordTypeId: "$objectInfo.data.defaultRecordTypeId",
		fieldApiName: Cancel_Reason
	})
	systemUsedPicklistValues(result) {
		if (result.data) {
			this.newArr = result.data.values;
		}
	}

    // Method for BACK button
    cannotCancel(event) {
		const closeModal = new CustomEvent("closemodal");
		this.dispatchEvent(closeModal);
    }

    
    // on change of cancellation reason picklist
    handleChange(event) {
        if (event.target.dataset.id === "cancelReasonId") {
            this.cancelReason = event.target.value;
        }
    }


    //Fetch Shipment Records
    getShipmentDetails() {
        fetchAphShipmentRecords({ orderId: this.recordId})
        .then(result => {
            this.aphShipmentList = result;
            this.aphShipmentId=this.aphShipmentList[0].Id;
            this.aphShipmentStatus = this.aphShipmentList[0].CCL_Status__c;
            if  (this.aphShipmentStatus == null || this.aphShipmentStatus == 'Apheresis Pick Up Planned') {
                this.aphShipmentStatus = 'Apheresis Pick Up Cancelled';
            }
         })
        .catch(error => {
            this.error = error;
        });

        // ADF details
        fetchADFDetails({ orderId: this.recordId})
        .then(result => {
            this.apheresisList = result;
            this.adfId=this.apheresisList[0].Id;
         })
        .catch(error => {
            this.error = error;
         });

        
        fetchShipmentForOrderUpdate({ orderId: this.recordId})
        .then(result => {
            if (!this.userHasPermission) {
                this.shipmentdata = result;
                this.status = this.shipmentdata[0].CCL_Status__c;
                if (this.status == 'Product Shipped' ||
                    this.status == 'Product Delivery Accepted' ||
                    this.status == 'Product Delivery Rejected') {
                    this.loaded = true;
                    // you cannot cancel the this order
                    this.cannotCancelOrder = true
                    this.loaded = false;
                }
                if (this.status == 'Product Shipment Planned' ||
                    this.status == 'Product Returned' ||
                    this.status == 'Shipment Cancelled' ||
                    this.status == null) {
                    this.loaded = true;
                    // you can cancel the this order
                    this.canCancelOrder = true;
                    this.loaded = false;
                } else {
                    this.cannotCancelOrder = true
                    this.loaded = false;
                }
            } else {
                this.userHasPermission = true;
                this.loaded = false;
            }
        })
        .catch(error => {
            this.error = error;
        });
    }

    CancelOrder(event) {
        // report validity
        this.loaded = true;
        let fieldVal = this.template.querySelector('.validValue');
        if(fieldVal.reportValidity()) {
            this.updateOrder();
        } else {
            this.loaded = false;
        }

    }

    updateOrder () {
        this.oderToBeUpdated=
        {
            "CCL_Treatment_Cancellation_Reason_Code__c":this.cancelReason,
            "Id": "",
            "CCL_Treatment_Status__c" :'Cancelled',
            "CCL_Status__c" : 'Order Cancelled', // ADF Status
            "adfId": this.adfId,
            "aphId": this.aphShipmentId,
            "aphStatus": this.aphShipmentStatus, // Aph status
        }
        this.oderToBeUpdated.Id = this.recordId;
        updateOrderForCancelReason({ result: this.oderToBeUpdated })
        .then((result) => {
        if(result==true)
        {
            const cancelThisOrder = new CustomEvent("cancelorder");
            this.dispatchEvent(cancelThisOrder);
        }
        })
        .catch((error) => {
        this.error = error.body.message;
        this.errorOnUpdate = error.body.message;
        this.loaded = false;
        });
    }
}
import { LightningElement,api } from 'lwc';

export default class CCL_TimeFormat extends LightningElement {
    @api fieldApiName;
    @api myTimeFields;
    @api inShipMent=false;
    @api inCollection=false;
    get display(){
        let rtnValue='';
        if(this.myTimeFields[this.fieldApiName]!=undefined&&this.myTimeFields[this.fieldApiName]!=''&&this.myTimeFields[this.fieldApiName]!=null){
                rtnValue=this.myTimeFields[this.fieldApiName].substr(0,5);
        }else{
            rtnValue='Please Enter Time' 
        }
        return rtnValue;
    }
}
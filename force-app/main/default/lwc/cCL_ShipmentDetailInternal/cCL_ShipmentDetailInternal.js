import { LightningElement, api, wire, track } from "lwc";
import geFieldDetails from "@salesforce/apex/CCL_ADFController_Utility.getShipmentDetails";
import getAccountDetails from "@salesforce/apex/CCL_ADF_Controller.getRecords";
import errMsg from "@salesforce/label/c.CCL_No_info_Available";
import pleaseSelect from "@salesforce/label/c.CCL_CryoBag_Conformation";
import successMsg from "@salesforce/label/c.CCL_Aph_Shiment_Details";
import getUserName from "@salesforce/apex/CCL_ADF_Controller.getUserName";
import { NavigationMixin, CurrentPageReference } from "lightning/navigation";
import getPickUpTimeZone from "@salesforce/apex/CCL_ADFController_Utility.getTimeZone";
import backtoshiplist from "@salesforce/label/c.CCL_Back_to_Shipment_List";
import adfShipmntHeader from "@salesforce/label/c.CCL_Apheresis_Shipment_Details";
import shipmnetInfoHeader from "@salesforce/label/c.CCL_Shipment_Information";
import viewOrder from "@salesforce/label/c.CCL_View_Order_Summary";
import cryoBagHeader from "@salesforce/label/c.CCL_CryoBag_Batch_Information";
import dearTrackingHeader from "@salesforce/label/c.CCL_Dewar_Tracking_Information";
import cryobagId from "@salesforce/label/c.CCL_CryoBag_ID";
import cryoBagNo from "@salesforce/label/c.CCL_CryoBag_No";
import checkUserPermission from "@salesforce/apex/CCL_ADFController_Utility.checkUserPermission";
import getFormattedDateTimeDetails from "@salesforce/apex/CCL_ADFController_Utility.getFormattedDateTimeDetails";
const ACTUAL_DEWAR__DATE = "CCL_Actual_Dewar_Packaging_Date__c";
const ACTUAL_DEWAR_TIME = "CCL_Actual_Dewar_Packaging_Time__c";
const ACTUAL_APH_PICKUP_DATE = "CCL_Actual_Cryo_Apheresis_Pick_up__c";
const ACTUAL_APH_PICKUP_TIME = "CCL_Actual_Apheresis_Pick_up_Time__c";
const ACTUAL_DEWAR_PKG_DT_TIME = "CCL_Actual_Dewar_Packaging_Date_Time__c";
const ACTUAL_DEWAR_PICKU_DT_TIME = "CCL_Actual_Aph_Pick_up_Date_Time__c";
const LAST_REPORTED_BY_SHPMNT = "CCL_Last_Reported_By_Shipment__c";
const LAST_REPORTED_BY = "CCL_Last_Reported_By_Packaging__c";
const ACTUAL_DEWAR_PKG_TXT = "CCL_TEXT_Actual_Dewar_Pkg_Date_Time__c";
const ACTUAL_PICKUP_DATE_TXT = "CCL_TEXT_Actual_Pick_up_Date__c";
const PLANNED_DWR_LABEL = "Planned Charged Dewar Arrival Date/Time";
export default class CCL_ShipmentDetailInternal extends NavigationMixin(
  LightningElement
) {
  @api recordId;
  @track label = {
    backtoshiplist,
    adfShipmntHeader,
    shipmnetInfoHeader,
    viewOrder,
    cryoBagHeader,
    dearTrackingHeader,
    cryobagId,
    cryoBagNo
  };
  dewarErrmsg = "Actual Dewar Packaging Date/Time cannot be in future";
  cryoErrMsg =
    "Actual Cryopreserved Apheresis Pick up Date/Time cannot be in future";
  pickupdateErr =
    "Actual Cryopreserved Apheresis Pick up Date/Time should be later than Actual Dewar Packaging Date/Time";
  @api apiNames = [];
  @track sobj;
  @track checkDate;
  @track updatedbag = [];
  @track customerOp = "CCL_Customer_Operations_User";
  @track inboundPermission = "CCL_Inbound_Logistics_Users";
  @track isCustomOpOrInbound = false;
  @track successMsg = successMsg;
  @track showSuccess = false;
  @track mainArr = [];
  @track pickupLocId;
  @track recordInput;
  @track timeZone;
  @track isShipper;
  @track consent = false;
  @track packageDateTimeErr = false;
  @track pickUpDateTimeErr = false;
  @track pickUpDateEarlier = false;
  @track hasResult = true;
  @track fieldValidations;
  @track userName;
  @track siteTimeZone = "Time zone of  Apheresis Pickup Location";
  //358
  @track myDateFields = {};
  @track myTimeFields = {};
  @track myDateObj = {
    CCL_TEXT_Actual_Dewar_Pkg_Date_Time__c: "",
    CCL_TEXT_Actual_Pick_up_Date__c: ""
  };
  @track errMsg = errMsg;
  @track EMPTY_STRING = "";
  @track firstValue = {};
  @track fields = {};
  @track aphIdLabel;
  @track orderId;
  @track pleaseSelect = pleaseSelect;
  @track status;
  @api bagList = [];
  @track dewarArr = [];
  @track hasFieldvalues;
  @track hasData;
  @track consentError = false;
  @track contentDocumentId;
  @track disablePrintPDF = true;
  @track SCPurl;
  @track disabled = true;
  @track associatedADFStatus;
  @track associatedADFId;
  @wire(CurrentPageReference) pageRef;
  @wire(geFieldDetails)
  wiredSteps({ error, data }) {
    if (data) {
      this.sobj = "CCL_Shipment__c";
      this.apiNames.push("CCL_CryoBag_Packing_Consent__c");
      this.apiNames.push("CCL_Order_Apheresis__c");
      this.apiNames.push(LAST_REPORTED_BY_SHPMNT);
      this.apiNames.push(LAST_REPORTED_BY);
      let self = this;
      data.forEach(function (node) {
        self.setFields(node, self);
      });
      data.forEach(function (node) {
        self.setDewarFields(node, self);
      });
      this.fetchValues(this.apiNames);

      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }
  @wire(getUserName)
  wiredStepUserName({ error, data }) {
    if (data) {
      this.userName = data;
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }
  @wire(getPickUpTimeZone, { recordId: "$pickupLocId" })
  wiredTime({ error, data }) {
    if (data) {
      this.timeZone = data[0].CCL_Time_Zone__r.Name;
      this.error = null;
    } else if (error) {
      
      this.error = error;
    }
  }
  fetchValues() {
    //358
    this.apiNames = [...new Set(this.apiNames)];
    getAccountDetails({
      sobj: this.sobj,
      cols: this.apiNames.toString(),
      recordId: this.recordId
    })
      .then((result) => {
        this.hasFieldvalues = result;
        this.orderId = result[0].CCL_Order_Apheresis__c;
        this.pickupLocId = result[0].CCL_Pick_up_Location__c;
        this.setValues();
        this.error = null;
      })
      .catch((error) => {
        
        this.error = error;
      });
  }
  setFields(mainArrr, self) {
    let address;
    let fieldArr = {
      Fieldlabel: "",
      fieldApiName: "",
      fieldorder: "",
      fieldValue: "",
      fieldType: "",
      fieldAddress: "",
      isEditable: "",
      class: "",
      isTime: "",
      isDate: "",
      innerClass: "",
      outerClass: ""
    };
    if (mainArrr.CCL_Order_of_field__c < 10) {
      fieldArr.Fieldlabel = mainArrr.MasterLabel;
      fieldArr.fieldType = mainArrr.CCL_Field_Type__c;
      fieldArr.fieldApiName = mainArrr.CCL_Field_API_Name__c;
      fieldArr.class =
        mainArrr.CCL_Field_Length__c == "Full"
          ? "slds-col slds-size_1-of-2 statusClass"
          : "slds-col slds-size_1-of-2 width40";

      fieldArr.isEditable = mainArrr.CCL_isEditable__c ? false : true;

      if (mainArrr.CCL_Field_Type__c == "Lookup") {
        fieldArr.fieldApiName =
          fieldArr.fieldApiName.substring(0, fieldArr.fieldApiName.length - 1) +
          "r.Name";
        if (mainArrr.CCL_HasAddress__c) {
          address =
            fieldArr.fieldApiName.substring(
              0,
              fieldArr.fieldApiName.length - 5
            ) + ".ShippingAddress";
          this.apiNames.push(address);
        }
      }
      //358
      fieldArr.outerClass = "labelIputs";
      fieldArr.innerClass = "labelClass myInputs";
      if (mainArrr.CCL_Field_Type__c == "time") {
        fieldArr.isTime = true;
        fieldArr.innerClass = "labelClass myInputs timeClass";
        fieldArr.class += " slds-grid width50 ";
      }
      if (mainArrr.CCL_Field_Type__c == "date") {
        fieldArr.isDate = true;
        fieldArr.outerClass = "labelIputs date-inputs";
        fieldArr.class += " slds-grid";
        fieldArr.innerClass = "labelClass myInputs dateClass";
      }
      fieldArr.Fieldlabel = this.setLabels(
        mainArrr.CCL_Field_Type__c,
        mainArrr.CCL_Field_API_Name__c,
        mainArrr.MasterLabel
      );
      this.apiNames.push(fieldArr.fieldApiName);
      fieldArr.fieldorder = mainArrr.CCL_Order_of_Field__c;
      self.mainArr.push(fieldArr);
    }
  }
  setLabels(type, apiName, label) {
    let myLabel = label;
    if (type == "date" && apiName == ACTUAL_DEWAR_PKG_TXT) {
      myLabel = "Actual Dewar Packaging Date";
    } else if (type == "date" && apiName == ACTUAL_PICKUP_DATE_TXT) {
      myLabel = "Actual Cryopreserved Apheresis Pick up";
    } else if (type == "time" && apiName == ACTUAL_PICKUP_DATE_TXT) {
      myLabel = "Time";
    } else if (type == "time" && apiName == ACTUAL_DEWAR_PKG_TXT) {
      myLabel = "Time";
    }
    return myLabel;
  }
   setDewarFields(mainArrr, self) {
    if (mainArrr.CCL_Order_of_field__c >= 10) {
      let fieldArr = {
        Fieldlabel: "",
        fieldApiName: "",
        fieldorder: "",
        fieldValue: "",
        fieldType: "",
        fieldAddress: "",
        isEditable: "",
        class: "",
        isTime: "",
        isUrl: "",
        isDate: "",
        innerClass: "",
        outerClass: "",
        iswaybill:''
      };
      fieldArr.Fieldlabel = mainArrr.MasterLabel;
      fieldArr.outerClass = "labelIputs";
      fieldArr.innerClass = "labelClass myInputs";
      fieldArr.fieldType = mainArrr.CCL_Field_Type__c;
      fieldArr.fieldApiName = mainArrr.CCL_Field_API_Name__c;
      fieldArr.class =
        mainArrr.CCL_Field_Length__c == "Full"
          ? "slds-col slds-size_1-of-1 statusClass"
          : "slds-col slds-size_1-of-2 width40";
      fieldArr.isEditable = mainArrr.CCL_isEditable__c ? false : true;
      if (mainArrr.CCL_Field_Type__c == "Lookup") {
        this.setForLookUp(fieldArr,mainArrr);
       }
      if (mainArrr.CCL_Field_Type__c == "time") {
        fieldArr.isTime = true;
        fieldArr.class += " slds-grid width50";
      }
      if (mainArrr.CCL_Field_Type__c == "url") {
        fieldArr.isUrl = true;
      }
      if (mainArrr.CCL_Field_API_Name__c == "CCL_Waybill_number__c") {
        fieldArr.iswaybill = true;
      }
      this.apiNames.push(fieldArr.fieldApiName);
      fieldArr.fieldorder = mainArrr.CCL_Order_of_Field__c;
      self.dewarArr.push(fieldArr);
    }
  }
  setForLookUp(fieldArr,mainArrr){
    let address;
    fieldArr.fieldApiName =
    fieldArr.fieldApiName.substring(0, fieldArr.fieldApiName.length - 1) +
    "r.Name";
  if (mainArrr.CCL_HasAddress__c) {
    address =
      fieldArr.fieldApiName.substring(
        0,
        fieldArr.fieldApiName.length - 5
      ) + ".ShippingAddress";
    this.apiNames.push(address);
  }
  }
  setValues() {
    let temp = this.hasFieldvalues[0];
    this.status = temp.CCL_Status__c;
    let self = this;
    this.dewarArr.forEach(function (node) {
      node.fieldValue =
        node.fieldType == "Lookup"
          ? temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)]
              .Name
          : temp[node.fieldApiName];
      node.fieldAddress =
        node.fieldType == "Lookup"
          ? temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)]
              .ShippingAddress
          : null;
    });
    this.mainArr.forEach(function (node) {
      node.fieldValue =
        node.fieldType == "Lookup"
          ? temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)]
              .Name
          : temp[node.fieldApiName];
      node.fieldAddress =
        node.fieldType == "Lookup"
          ? temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)]
              .ShippingAddress
          : null;
      if (node.fieldType != "Lookup") {
        self.fields[node.fieldApiName] = node.fieldValue;
      }

      if (node.fieldType == "time") {
        node.fieldValue = self.setTime(
          node.fieldApiName,
          temp[node.fieldApiName]
        );
      }
      if (node.fieldType == "date") {
        node.fieldValue = self.setDate(
          node.fieldApiName,
          temp[node.fieldApiName]
        );
      }
      if (node.Fieldlabel == PLANNED_DWR_LABEL) {
        node.fieldValue =
          node.fieldValue == "" || node.fieldValue == undefined
            ? "NA"
            : node.fieldValue;
      }
    });
  }
  //358
  setTime(apiName, value) {
    if (value != undefined) {
      this.myTimeFields[apiName] = value.substr(12, 18);
    }
    return "";
  }
  //358
  setDate(apiName, value) {
    if (value != undefined) {
      this.myDateFields[apiName] = value.substr(0, 11);
    }
    return "";
  }
  //358
  formatDate(date) {
    let mydate = new Date(date);
    let monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];

    let day = "" + mydate.getDate();
    if (day.length < 2) day = "0" + day;
    let monthIndex = mydate.getMonth();
    let monthName = monthNames[monthIndex];
    let year = mydate.getFullYear();
    return [day, monthName, year].join(" ");
  }

  navigateToViewHome() {
    this[NavigationMixin.Navigate]({
      type: "standard__recordPage",
      attributes: {
        recordId: this.orderId,
        objectApiName: "CCL_Order__c",
        actionName: "view"
      }
    });
  }
  connectedCallback() {
    this.checkPermission(this.customerOp);
    this.checkPermission(this.inboundPermission);
     
  }
  checkPermission(param) {
    checkUserPermission({
      permissionName: param
    })
      .then((result) => {
       if(result){
        this.isCustomOpOrInbound=true;
       }
        this.error = null;
      })
      .catch((error) => {
        
        this.error = error;
      });
    
  }
  getFormattedDateTimeVal(date, time, dateTimeField, checkField) {
    let currentDate = new Date();
    let dateTime =
      this.formatDateFormat(date.substr(0, 11)) +
      " " +
      time.substr(0, 5) +
      ":00.000";
    getFormattedDateTimeDetails({
      dateTimeVal: dateTime,
      timeZoneVal: this.timeZone
    })
      .then((result) => {
        this.formattedDateTime = result;
        this.fields[dateTimeField] = this.formattedDateTime;
        let resultDate = new Date(result);
        let nowDate = new Date(currentDate.toISOString());
        if (
          resultDate.getTime() > nowDate.getTime() &&
          nowDate.getDate() == resultDate.getDate() &&
          nowDate.getMonth() == resultDate.getMonth() &&
          nowDate.getYear() == resultDate.getYear()
        ) {
          this.validateTime(checkField);
        } else {
          this.saveData();
        }
      })
      .catch((error) => {
        this.error = error;
        
      });
  }
  formatDateFormat(date) {
    if (date !== null && date !== "" && date !== undefined) {
      let d = new Date(date),
        month = "" + (d.getMonth() + 1),
        day = "" + d.getDate(),
        year = d.getFullYear();

      if (month.length < 2) month = "0" + month;
      if (day.length < 2) day = "0" + day;

      return [year, month, day].join("-");
    }

    return "";
  }
}
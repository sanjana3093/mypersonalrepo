import {
    LightningElement,
    wire,
    track
} from 'lwc';
import getAllPatients from '@salesforce/apex/PSP_ContactController.getAllHCPs';
import {
    updateRecord
} from 'lightning/uiRecordApi';
import {
    refreshApex
} from '@salesforce/apex';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';


const COLS = [{
        label: "Name",
        fieldName: "nameURL",
        type: "url",
        typeAttributes: {
            label: {
                fieldName: "name"
            },
            tooltip: {
                fieldName: "name"
            },
            target: "__self"
        },
        sortable: true
    },
    {
        label: 'Professional ID',
        fieldName: 'professionalID',
        type: 'text'
    },
    {
        label: 'Speciality',
        fieldName: 'speciality',
        type: 'Picklist',
        sortable: true
    },
    {
        label: 'Status',
        fieldName: 'status',
        type: 'Picklist',
        sortable: true
    },
    {
        label: "Accounts Name",
        fieldName: "accountsNameURL",
        type: "url",
        typeAttributes: {
            label: {
                fieldName: "accountsName"
            },
            tooltip: {
                fieldName: "accountsName"
            },
            target: "__self"
        },
        sortable: true
    },
    {
        label: 'Phone',
        fieldName: 'phone',
        type: 'Phone',
        sortable: true
    }

];
export default class pSP_DisplayPatientList extends LightningElement {

    @track error;
    @track columns = COLS;
    @track draftValues = [];
    @track sortedBy;
    @track sortDirection = 'asc';
    @track showSpinner = true;

    @wire(getAllPatients)
    product({
        error,
        data
    })

    {
        if (data) {
            this.showSpinner = false;
            this.data = data;
        } else if (error) {
            this.error = error;
        }
    }
    //Method for sorting
    sortData(fieldName, sortDirection) {
        var data = JSON.parse(JSON.stringify(this.data));
        //function to return the value stored in the field
        var key = (a) => a[fieldName];
        var reverse = sortDirection === 'asc' ? 1 : -1;
        data.sort((a, b) => {
            let valueA = key(a) ? key(a).toLowerCase() : '';
            let valueB = key(b) ? key(b).toLowerCase() : '';
            return reverse * ((valueA > valueB) - (valueB > valueA));
        });
        //set sorted data to opportunities attribute
        this.data = data;
    }
    //this method is called when a column sort icon is clicked
    sortThisColumn(event) {
        this.sortedBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortedBy, this.sortDirection);
    }
}
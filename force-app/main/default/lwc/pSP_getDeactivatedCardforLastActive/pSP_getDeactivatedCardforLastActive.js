import {
    LightningElement,
    track,
    api
} from 'lwc';
import fetchRecords from "@salesforce/apex/PSP_custLookUpCntrl.getEnrolledPrograms";
export default class PSP_getDeactivatedCardforLastActive extends LightningElement {
    @api getAllCards = null;
    @api getAllEnrolless = null;
    @api showErrMsg = false;
    @api lastActiveCard;
    @api recordId;
    @track val;
    connectedCallback() {
        /* method to fetch all program enrollees details associated with patient/HCP  */
        fetchRecords({
                recordId: this.recordId,

            })
            .then(result => {
                this.getAllEnrolless = result;
                /* if program enrollee fetched is just onemptied, then directly assign the last active card*/
                if (this.getAllEnrolless.length === 1) {
                    this.lastActiveCard = this.getAllEnrolless[0].PSP_Card_Number__r.Name;
                } else if (this.getAllEnrolless.length > 1) {
                    this.getCardNumner(this.getAllEnrolless);
                }
                if (this.showErrMsg) {
                    this.getLatestDeactivatedCard(this.getAllEnrolless);
                } 
                this.error = null;
            })
            .catch(error => {
                this.error = error;
            });
    }
    /*this method verifies if all the program enrolless have same card or not */ 
    getCardNumner(enrolleeList) {
        let i = 0;
        let firstCardNumber;
        for (i = 0; i < enrolleeList.length; i++) {
            if (enrolleeList[i].PSP_Card_Number__c) {
                this.lastActiveCard = enrolleeList[i].PSP_Card_Number__r.Name;
                firstCardNumber = enrolleeList[i].PSP_Card_Number__c;
            }
        }

        for (i = 0; i < enrolleeList.length; i++) {
            if (enrolleeList[i].PSP_Card_Number__c) {
                if (firstCardNumber !== enrolleeList[i].PSP_Card_Number__c) {
                    this.showErrMsg = true;
                }
            }
        }
    }
    /* this method aasigns the latest deactivated card to lastActiveCard based on deactivation date*/ 
    getLatestDeactivatedCard(enrolleeList) {
        let i = 0;
        let maxDateObj;
        let tempDate;
        for (i = 0; i < enrolleeList.length; i++) {
            if (enrolleeList[i].PSP_Card_Number__r) {
                maxDateObj = enrolleeList[i].PSP_Card_Number__r.PSP_Deactivation_Date__c;
                this.lastActiveCard = enrolleeList[i].PSP_Card_Number__r.Name;
            }
        }
        for (i = 0; i < enrolleeList.length; i++) {
            if (enrolleeList[i].PSP_Card_Number__r) {
                tempDate = new Date(enrolleeList[i].PSP_Card_Number__r.PSP_Deactivation_Date__c);
                if (tempDate > maxDateObj) {
                    this.lastActiveCard = enrolleeList[i].PSP_Card_Number__r.Name;
                }
            }
        }
    }

}
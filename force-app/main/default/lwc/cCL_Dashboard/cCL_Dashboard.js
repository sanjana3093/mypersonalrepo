import { LightningElement,track,wire,api} from 'lwc';
import getHeader from "@salesforce/apex/CCL_ADFController_Utility.getOrderHeader";
import getAccountDetails from "@salesforce/apex/CCL_ADF_Controller.getRecords";
import getOrderList from "@salesforce/apex/CCL_ADFController_Utility.getAllOrders";
import checkphiPermission from "@salesforce/apex/CCL_PRF_Controller.checkPHIUser";
import fetchFinishedProductDetails from "@salesforce/apex/CCL_PRF_Controller.fetchFinishedProductDetails";
import HOSPITAL_PATIENT_ID from "@salesforce/label/c.CCL_Hospital_Patient_ID";
import COMMERCIAL from "@salesforce/label/c.CCL_Therapy_Type_Commercial";
import CLINICAL from "@salesforce/label/c.CCL_Therapy_Type_Clinical";
import PRINCIPAL_INVESTOGATOR from "@salesforce/label/c.CCL_Principal_Investigator";
import TRTMNT_PROTOCOL from "@salesforce/label/c.CCL_Treatment_Protocal_Subject_ID";
import PRESCRIBER from "@salesforce/label/c.CCL_Prescriber";
import {NavigationMixin } from "lightning/navigation";
import CCL_Org_Link from "@salesforce/label/c.CCL_Org_Link";
import orderCount from "@salesforce/apex/CCL_ADFController_Utility.getAllOrderCount";
import filterAcc from "@salesforce/apex/CCL_ADFController_Utility.fetchAccountsFrmFilter";
import gethospitalOptIn from "@salesforce/apex/CCL_ADFController_Utility.fetchHospitalOptIn";
import checkUserPermission from "@salesforce/apex/CCL_PRF_Controller.checkUserPermission";
import checkCustomerOperationsUser from "@salesforce/apex/CCL_PRF_Controller.checkCustOpsPermission";
import checkallowreplacementPermission from "@salesforce/apex/CCL_PRF_Controller.checkallowreplacementPermission";
import checkPRFApprover from "@salesforce/apex/CCL_PRF_Controller.checkPRFApprover";
import UserId from '@salesforce/user/Id';
const PRESCRIBER_API_NAME = "CCL_Prescriber__c";
const HOSPITALPATIENI_API_NAME = "CCL_Protocol_Subject_Hospital_Patient__c";
const PATIENT_DOB="CCL_Date_Of_Birth_Text__c";

import Your_Orders from "@salesforce/label/c.CCL_Your_Orders";
import quickFilters from "@salesforce/label/c.CCL_Quick_Filters";
import orderingHospital from "@salesforce/label/c.CCL_Ordering_Hospital_Placeholder";
import prescriberPlaceholder from "@salesforce/label/c.CCL_Prescriber_Principal_Placeholder";
import clearLink from "@salesforce/label/c.CCL_Clear_Link";
import showingOrders from "@salesforce/label/c.CCL_Showing_Orders_Pagination";
import of from "@salesforce/label/c.CCL_Of";
import previous from "@salesforce/label/c.CCL_Previous_Pagination";
import next from "@salesforce/label/c.CCL_Next_Pagination";
import sortOrder from "@salesforce/label/c.CCL_Sort_Order";
import sortOrderNote from "@salesforce/label/c.CCL_Order_Note_Text";
import Patient_Name from "@salesforce/label/c.CCL_Patient_Name";
import Patient_Initials from "@salesforce/label/c.CCL_Patient_Initials";
import replacementOrder from "@salesforce/label/c.CCL_ReplacementOrderCaps";
import submittedText from "@salesforce/label/c.CCL_Submitted";
import enterReplacementOrder from "@salesforce/label/c.CCL_Enter_Replacement_Order";
import enterReplacementOrderHelptext from "@salesforce/label/c.CCL_Enter_Replacement_Order_Helptext";
import verifyApproveOrder from "@salesforce/label/c.CCL_Verify_and_Approve_Order";
import Patient_ID from "@salesforce/label/c.CCL_Patient_Id";
import SHIPPED_UNDER_QUARANTINE from "@salesforce/label/c.CCL_SHIPPED_UNDER_QUARANTINE";
import FINAL_PRODUCT_RELEASED from "@salesforce/label/c.CCL_FINAL_PRODUCT_RELEASED";
import ORDER_CHEVRON from "@salesforce/label/c.CCL_Order_Chevron";
import APHERESIS_CHEVRON from "@salesforce/label/c.CCL_Apheresis_Chevron";
import MANUFACTURING_CHEVRON from "@salesforce/label/c.CCL_Manufacturing_Chevron";
import DELIVERY_CHEVRON from "@salesforce/label/c.CCL_Delivery_Chevron";

export default class CCL_Dashboard extends NavigationMixin(LightningElement) {
  label = {Your_Orders,
    quickFilters,
    orderingHospital,
    prescriberPlaceholder,
    clearLink,
    showingOrders,of,
    previous,next,
    sortOrder,
    sortOrderNote,
    replacementOrder,
    submittedText,
    enterReplacementOrder,
    enterReplacementOrderHelptext,
    Patient_ID,
    verifyApproveOrder,
    SHIPPED_UNDER_QUARANTINE,
    FINAL_PRODUCT_RELEASED
  };	
@track sobj;
@track mainArr = [];
@track finalArr=[];
@api apiNames = [];
@api recordId;
@track hasFieldvalues;
@api therapyType;
@track linkEnable = true;
@track name;
@track firstname;
@track lastname;
@track middlename;
@track suffix;
@track firstValue = {};
@track orderList;
@track wiredResult;
@track count=0;
@api phiPermission=false;
@api orderingTherapyAssociation;
@track hosptialOptedIn;
disableReplacement=false;
@api allowreppermission;
@track disableApproval=false;

 /*****************************************************************************
  * New variables to determine filter and pagination
  * **************************************************************************/
 @track offsetVal = 0;
   @track page = 1;
    perpage = 10;
    @track pages = [];
    set_size = 3;
  @track totalOrders = 0;
  @track lowerLimit = 0;
  @track upperLimit =0;

  /*****************************************************************************
  * New variables to apply lookup filter
  * **************************************************************************/
 @track accountId = '';
 @track filterVal;
 @track accList = [];
 @track sObjName;
 @track iconName;
 @track conList = [];
 @track selectedValAcc;
 @track selectedValCon;
 @track accId ='';
 @track conId = '';
 @track diabledCon =false;
 @track diabledAcc = false;


    connectedCallback(){
    }
    @wire(checkUserPermission)
    accountManagerPermission({ error, data }) {
      if (data) {
        this.disableReplacement = data;
      }
      else if (error) {
        this.error = error;}}
@wire(checkCustomerOperationsUser)
    checkCustomerOperations({error, data}) {
        if (data) {
          this.disableApproval = data;
        }
        else if (error) {
          this.error = error;
      }
    }

    @wire(getHeader)
    wiredSteps({ error, data }) {
      if (data) {
        this.sobj = "CCL_Order__c";
        this.wiredResult=data;
        this.getApiNames(data);
        this.fetchOrderListValues();
        this.error = null;
      } else if (error) {
        this.error = error;
      }
    }

    @wire(checkphiPermission)
    phiPermissioncheck({ error, data }) {
      if (data!=undefined) {
        this.phiPermission = data;
      }
      else if (error) {
        this.error = error;
      }
    }

    getApiNames(data) {
        let self = this;
        let apiname;
        data.forEach((element) => {
          apiname = element.CCL_Field_API_Name__c;
          if (element.CCL_Field_Type__c == "Lookup") {
			  if(element.CCL_Field_API_Name__c == 'CCL_Therapy__c'){
              apiname =
              element.CCL_Field_API_Name__c.substring(
                0,
                element.CCL_Field_API_Name__c.length - 1
              ) + "r.CCL_Therapy_Description__c";
            }else{
            apiname =
              element.CCL_Field_API_Name__c.substring(
                0,
                element.CCL_Field_API_Name__c.length - 1
              ) + "r.Name";
			}
          }
          self.apiNames.push(apiname);
        });
      }

      fetchOrderListValues() {
        getOrderList({offsetData: this.offsetVal,accId: this.accId,conId: this.conId})
          .then((result) => {
            let self = this;
            this.orderList = result;
            this.orderList.forEach(function (node) {
                self.setFields(node, self);
            });
            this.error = null;
          })
          .catch((error) => {
            this.error = error;
          });
      }

      fetchHospitalOptIn(therapyId,hospitalId,node) {
        gethospitalOptIn({therapyId: therapyId,hospitalId: hospitalId})
          .then((result) => {
          this.orderingTherapyAssociation = result;
            if(this.orderingTherapyAssociation[0]){
              this.hosptialOptedIn=this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Opt_In__c;
            }else{
              node.Fieldlabel='';
              node.fieldValue='';
            }
            if(this.hosptialOptedIn=='No' || this.hosptialOptedIn==undefined || node.fieldValue==='' || node.fieldValue==undefined){
              node.Fieldlabel='';
              node.fieldValue='';
            }
	  this.error = null;
          })
          .catch((error) => {
            this.error = error;
            
          });
      }

      fetchValues(orderRecordId) {
        this.apiNames;
        getAccountDetails({
          sobj: this.sobj,
          cols: this.apiNames.toString(),
          recordId: orderRecordId
        })
          .then((result) => {
            this.hasFieldvalues = result;
            this.setValues(orderRecordId);
            this.count=this.count+1;
            this.error = null;
          })
          .catch((error) => {
            this.error = error;
          });
      }

      setFields(mainArrr, self) {
		let currentloggedinuser=UserId;
        let self1=this;
          let newArr = { Id: "", myFields: [],
          nameLabel:"",patientName:"",dob:"",isReplacementOrder:"",orderDate:"",conditionalStatus:"",exceptionalStatus:"",additionalInfo:"",isReplacementAllowed:"",isApprovalEligible:"",prfOrderStatus:"",prfsubmitter:""};
          newArr.Id = mainArrr.Id;
          newArr.nameLabel=Patient_Name;
          if(mainArrr.CCL_Patient_Initials__c) {
            newArr.nameLabel=Patient_Initials;
          }
          newArr.patientName=mainArrr.CCL_Patient_Name_Text__c;
        self1.wiredResult.forEach(function (node) {
        let fieldArr = {
          Fieldlabel: "",
          fieldApiName: "",
          fieldorder: "",
          fieldValue: "",
          fieldType: "",
          isName:"",
          Name:"",
          secondaryTherapy:"",
          isDOB:"",
		      enterapproval:""
        };
        fieldArr.Fieldlabel = node.MasterLabel;
        fieldArr.fieldType = node.CCL_Field_Type__c;
        newArr.isReplacementOrder=mainArrr.CCL_Returning_Patient__c;
        newArr.orderDate=self.getOrderDateFormatted(mainArrr.CCL_Order_Submission_Date_Text__c);
        newArr.conditionalStatus=mainArrr.CCL_Conditional_Status__c;
        newArr.exceptionalStatus=mainArrr.CCL_Exception_Status__c;
        newArr.additionalInfo=mainArrr.CCL_Additional_Information__c;
        fieldArr.secondaryTherapy=mainArrr.CCL_Secondary_Therapy__c;
        newArr.isReplacementAllowed=mainArrr.CCL_Allow_Replacement_Order__c;
		newArr.isApprovalEligible=mainArrr.CCL_Order_Approval_Eligibility__c;
        newArr.prfsubmitter=(mainArrr.CCL_Latest_Order_Submitted_By__c!=currentloggedinuser )?true:false;
        newArr.prfOrderStatusSubmt=(mainArrr.CCL_PRF_Ordering_Status__c=='PRF_Submitted') ||
        (mainArrr.CCL_PRF_Ordering_Status__c=='PRF_Re-Submitted') ?true:false;
        if (
          node.CCL_Field_API_Name__c == PRESCRIBER_API_NAME && mainArrr.CCL_Therapy__c!=undefined &&
          mainArrr.CCL_Therapy__r.CCL_Type__c == COMMERCIAL
        ) {
          fieldArr.Fieldlabel = PRESCRIBER;
        }
        if (
          node.CCL_Field_API_Name__c == PRESCRIBER_API_NAME && mainArrr.CCL_Therapy__c!=undefined &&
          mainArrr.CCL_Therapy__r.CCL_Type__c == CLINICAL
        ) {
          fieldArr.Fieldlabel = PRINCIPAL_INVESTOGATOR;
        }
        if (
          node.CCL_Field_API_Name__c == HOSPITALPATIENI_API_NAME && mainArrr.CCL_Therapy__c!=undefined &&
          mainArrr.CCL_Therapy__r.CCL_Type__c == COMMERCIAL
        ) {
          fieldArr.Fieldlabel = HOSPITAL_PATIENT_ID;
        }
        if (
          node.CCL_Field_API_Name__c == HOSPITALPATIENI_API_NAME && mainArrr.CCL_Therapy__c!=undefined &&
          mainArrr.CCL_Therapy__r.CCL_Type__c == CLINICAL
        ) {
          fieldArr.Fieldlabel = TRTMNT_PROTOCOL;
        }
        if(node.CCL_Field_API_Name__c==PATIENT_DOB){
			 
          if ( !mainArrr.CCL_Day_of_Birth_COI__c){
            newArr.dob = 'dd' + ' ';
          }else {
            newArr.dob = mainArrr.CCL_Date_of_DOB__c.length < 2?'0'+mainArrr.CCL_Date_of_DOB__c+' ':mainArrr.CCL_Date_of_DOB__c+ ' '
          }
          if ( !mainArrr.CCL_Month_of_Birth_COI__c){
            newArr.dob = newArr.dob + 'mmm' + ' ';
          }else {
            newArr.dob = newArr.dob + mainArrr.CCL_Month_of_DOB__c + ' ';
          }
          if ( !mainArrr.CCL_Year_of_Birth_COI__c){
            newArr.dob = newArr.dob + 'yyyy';
          } else {
            newArr.dob = newArr.dob + mainArrr.CCL_Year_of_DOB__c ;
          }
		  fieldArr.isDOB=true;
        }
		if (
          newArr.patientName == '' || newArr.patientName == null || newArr.patientName == undefined
        ) {
          newArr.nameLabel=Patient_Name;
            newArr.patientName="--";
        }
        fieldArr.fieldApiName = node.CCL_Field_API_Name__c;
        if (node.CCL_Field_Type__c == "Lookup") {
			if(node.CCL_Field_API_Name__c=='CCL_Therapy__c'){
            fieldArr.fieldApiName =
            fieldArr.fieldApiName.substring(0, fieldArr.fieldApiName.length - 1) +
            "r.CCL_Therapy_Description__c";
          }else{
          fieldArr.fieldApiName =
            fieldArr.fieldApiName.substring(0, fieldArr.fieldApiName.length - 1) +
            "r.Name";
		  }
        }
        if(node.CCL_Field_API_Name__c=='CCL_Patient_Name_Text__c'){
          fieldArr.isName=true;
        }
        fieldArr.fieldorder = node.CCL_Order_of_Field__c;
        newArr.myFields.push(fieldArr);
      });
	   if(mainArrr.CCL_Patient_Id__c!=undefined){
          newArr["showPatientId"]=true;
          newArr["patientID"]=mainArrr.CCL_Patient_Id__c;
        }
        else{
          newArr["showPatientId"]=false;
        }
      let chevronJson=this.setChevronJson(mainArrr);
      newArr["chevronJson"]=chevronJson;
      let color=this.setChevronColor(mainArrr);
      newArr["ChevronColor"]=color;
      self.fetchFPBatchStatus(mainArrr,newArr,self);
      }
		
	fetchFPBatchStatus(mainArrr,newArr,self){
        fetchFinishedProductDetails({orderId: mainArrr.Id})
                      .then((result)=>{
                        
                        this.error=null;
                        if(JSON.stringify(result)!="[]"){
                          
                          for(let i=0;i<Object.keys(result).length;i++){
                            
                            if(result[i].CCL_Batch_Status__c!=undefined&&result[i].CCL_Batch_Status__c!='Rejected'){
                              
                              newArr["Ribbon"]=true;
                              
                              if(result[i].CCL_Batch_Status__c=='Quality'){
                                newArr["Quarentine"]=true;
                                break;
                              }
                              else if(i==0&&(result[i].CCL_Batch_Status__c=="Approved"||result[i].CCL_Batch_Status__c=="Conditionally Approved")){
                                
                                newArr["Quarentine"]=false;
                              }
                              else{
                                
                                newArr["Ribbon"]=false;
                              }
                            }
                            if(i==Object.keys(result).length-1&&newArr.Ribbon==undefined){
                              
                              newArr["Ribbon"]=false;
                            }
                          }
                        }
                        else{
                          newArr["Ribbon"]=false;
                        }
                        self.mainArr.push(newArr);
                        this.fetchValues(mainArrr.Id);
                      })
                      .catch((error) => {
                        
                        this.error = error;
                        return null;
                      });
      }

		
      setChevronJson(mainArr){
        return( [

        { label: ORDER_CHEVRON, status: mainArr.CCL_Order_Chevron__c!=undefined?mainArr.CCL_Order_Chevron__c:"",date: mainArr.CCL_Order_Chevron_Date__c!=undefined?
        mainArr.CCL_Order_Chevron_Date__c:'',value: '1' },
        { label: APHERESIS_CHEVRON,status: mainArr.CCL_Apheresis_Chevron__c!=undefined?mainArr.CCL_Apheresis_Chevron__c:"",date: mainArr.CCL_Apheresis_Chevron_Date__c
        !=undefined?mainArr.CCL_Apheresis_Chevron_Date__c:'', value: '2' },
        { label: MANUFACTURING_CHEVRON,status: mainArr.CCL_Manufacturing_Chevron__c!=undefined?mainArr.CCL_Manufacturing_Chevron__c:"",date: mainArr.CCL_Order_Chevron_Date__c
        !=undefined?mainArr.CCL_Manufacturing_Chevron_Date__c:'', value: '3' },
        { label: DELIVERY_CHEVRON,status: mainArr.CCL_Delivery_Chevron__c!=undefined?mainArr.CCL_Delivery_Chevron__c:"",date: mainArr.CCL_Delivery_Chevron_Date__c!=undefined?
        mainArr.CCL_Delivery_Chevron_Date__c:'', value: '4' },
        ]);
      }

      setChevronColor(mainArr){
  let colour={};
  if(mainArr.CCL_Order_Chevron__c!=undefined&&mainArr.CCL_Apheresis_Chevron__c!=undefined&&mainArr.CCL_Manufacturing_Chevron__c!=undefined&&
    mainArr.CCL_Delivery_Chevron__c!=undefined){
    if(mainArr.CCL_Order_Chevron__c==='Cancelled'){
      colour["currentStep"]=0;
      colour["completeStep"]=0;
      return colour;
    }
    else if(mainArr.CCL_Order_Chevron__c!="Confirmed"&&mainArr.CCL_Order_Chevron__c!="Cancelled"){
      colour["currentStep"]=1;
      colour["completeStep"]=0;
      return colour;
    }
    else if((mainArr.CCL_Apheresis_Chevron__c==="Planned Pick Up"||mainArr.CCL_Apheresis_Chevron__c==="Picked Up")
    &&mainArr.CCL_Manufacturing_Chevron__c=="Pending"&&mainArr.CCL_Delivery_Chevron__c==="Estimated Delivery"){
      colour["currentStep"]=2;
      colour["completeStep"]=1;
      return colour;
    }
    
    else if((mainArr.CCL_Apheresis_Chevron__c==="Received")
    &&(mainArr.CCL_Manufacturing_Chevron__c=="Pending"||
mainArr.CCL_Manufacturing_Chevron__c=="Terminated"||
mainArr.CCL_Manufacturing_Chevron__c=="Manufacturing Start On Hold")
    &&(mainArr.CCL_Delivery_Chevron__c==="Estimated Delivery"||mainArr.CCL_Delivery_Chevron__c==="Product Returned"||
    mainArr.CCL_Delivery_Chevron__c==="Product Delivery On Hold"||mainArr.CCL_Delivery_Chevron__c==="Cancelled")){
      colour["currentStep"]=0;
    colour["completeStep"]=2;
    return colour;
    }
    else if(mainArr.CCL_Apheresis_Chevron__c==="Apheresis Pick Up On Hold"){
      colour["currentStep"]=0;
    colour["completeStep"]=1;
    return colour;
    }
    else{
      colour=this.checkRest(mainArr);
      return colour;
    }
 }else{
      colour["currentStep"]=0;
        colour["completeStep"]=0;
        return colour;
    }
}
checkRest(mainArr){
let colour={};
 if((mainArr.CCL_Manufacturing_Chevron__c==="Manufacturing Started")
    &&mainArr.CCL_Delivery_Chevron__c==="Estimated Delivery"){
      colour["currentStep"]=3;
    colour["completeStep"]=2;
    return colour;
    }
    else if((mainArr.CCL_Manufacturing_Chevron__c==="QA testing started"||mainArr.CCL_Manufacturing_Chevron__c==="QA testing completed")
    &&(mainArr.CCL_Delivery_Chevron__c==="Estimated Delivery"||mainArr.CCL_Delivery_Chevron__c==="Shipped. Estimated Delivery")){
      colour["currentStep"]=4;
    colour["completeStep"]=3;
    return colour;
    }
    else if((mainArr.CCL_Manufacturing_Chevron__c==="QA testing started"||mainArr.CCL_Manufacturing_Chevron__c==="QA testing completed")
    &&(mainArr.CCL_Delivery_Chevron__c==="Product Returned"||
    mainArr.CCL_Delivery_Chevron__c==="Product Delivery On Hold"||mainArr.CCL_Delivery_Chevron__c==="Cancelled")){
      colour["currentStep"]=0;
    colour["completeStep"]=3;
    return colour;
    }
    else if(mainArr.CCL_Delivery_Chevron__c==="Delivered"){
      colour["currentStep"]=0;
    colour["completeStep"]=4;
    return colour;
    }
    else{
  colour["currentStep"]=4;
    colour["completeStep"]=2;
    return colour;
    }
}


      getOrderDateFormatted(dateText){
        let finalDate='';
        if(dateText!==undefined && dateText!==''){
        let fields=dateText.split(' ');
        let timeVal=fields[3].split(':');
        finalDate=fields[0]+' '+fields[1]+' '+fields[2]+' '+timeVal[0]+':'+timeVal[1]+' '+fields[4];
        }
      return finalDate;
      }


	openApproval(event){
          this.linkEnable = false;
          let id=event.target.name;
          this[NavigationMixin.GenerateUrl]({
            type: 'comm__namedPage',
            attributes: {
            pageName: "ccl-verification-approval",
            recordId: id,
            url: 'ccl-verification-approval?recordId='+id
            },
            state: {
            recordId: id
            }
        }).then(url => {
            window.open(url);
        });
      }

	@wire(checkPRFApprover)
	prfApproverPermission({ error, data }) {
      if (data) {
        this.disableApproval = data;

      }
      else if (error) {
        this.error = error;
      }
    }

/* Commenting as of now, we should use conctenated patient name text to display the value
      getPatientName(firstName,middleName,lastName,suffix){
        let patientName='';
        if(firstName!==undefined){
          patientName=firstName;
          if(middleName!=undefined){
            patientName=patientName+' '+middleName;
          }
          if(lastName!=undefined){
            patientName=patientName+' '+lastName;
          }
          if(suffix!=undefined){
            patientName=patientName+' '+suffix;
          }
        }
        return patientName;
      }
*/
      setValues(orderRecordId) {
        let self=this;
        if(this.mainArr.length>0){
          for (let i = 0; i < this.mainArr.length; i++) {
            this.mainArr[i].myFields.forEach(function (node) {
          if(self.mainArr[i].Id===orderRecordId) {
			  self.checkFields(node,self);
        }
          });
        }
      }
      }
checkFields(node,self){
	let temp = this.hasFieldvalues[0];
	if(node.fieldType == "Lookup"){
            let fieldNameVal=temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-5)];
            if(fieldNameVal==undefined){
            node.fieldValue ='';
            }else{
              node.fieldValue =temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)]
                    .Name
            }
			if(node.fieldApiName=='CCL_Therapy__r.CCL_Therapy_Description__c'){
      let fieldNameVal=temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-27)];
    
    if(fieldNameVal==undefined){
    node.fieldValue ='';
    }else{
      node.fieldValue =temp[node.fieldApiName.substring(0, node.fieldApiName.length - 27)]
            .CCL_Therapy_Description__c
    }
    }
                }else if(node.fieldApiName==PATIENT_DOB){
                  node.fieldValue=this.formatNVSDate(temp[node.fieldApiName]);
          }else{
            node.fieldValue = temp[node.fieldApiName];
          }
          if(node.fieldApiName=='CCL_Therapy__r.CCL_Therapy_Description__c' && node.secondaryTherapy!='' && node.secondaryTherapy!==undefined){
            node.fieldValue=node.fieldValue+' shipped as '+node.secondaryTherapy;
          }
	          if(node.fieldApiName=='CCL_Protocol_Subject_Hospital_Patient__c' && node.Fieldlabel==HOSPITAL_PATIENT_ID){
              this.fetchHospitalOptIn(temp["CCL_Therapy__r"].Id,temp["CCL_Ordering_Hospital__r"].Id,node);
              if(node.fieldValue==''){
                node.Fieldlabel='';
                node.fieldValue='';
              }
          }
}
      formatNVSDate(date) {
        if(date!==null && date!==''&& date!==undefined){
        let dateVal=date.split(' ');
        let day='';
        if(dateVal[0].length<2){
          day='0'+dateVal[0];
        }else{
          day=dateVal[0];
        }
        return [day, dateVal[1],dateVal[2]].join(' ');
      }
      return '';
      }
      pageClick(event){
        let id=event.target.name;
		this.linkEnable = false;
        this[NavigationMixin.GenerateUrl]({
            type: 'comm__namedPage',
            attributes: {
            pageName: "ccl-prf",
            recordId: id,
            url: 'ccl-prf?recordId='+id
            },
            state: {
            recordId: id
            }
        }).then(url => {
            window.open(url);
        });
        }
        openRecord(event){
          let id=event.currentTarget.dataset.id;
		  let urlID ='';
          if(this.linkEnable){
            urlID = CCL_Org_Link + id;
          }
          this.linkEnable = true;
          this[NavigationMixin.Navigate]({
              type: 'standard__webPage',
              attributes: {
              url: urlID
              }
          }).then(url => {
            window.open(url);
        });
          }


         /************************************************************************
     * Order pagination logic begins here
     * **********************************************************************/
    renderedCallback() {
      this.renderButtons();
  }
  renderButtons = () => {
      this.template.querySelectorAll('button').forEach((but) => {
          if(!but.disabled) {
              but.style.backgroundColor = this.page === parseInt(but.dataset.id, 10) ? 'dodgerblue' : 'white';
              but.style.color = this.page === parseInt(but.dataset.id, 10) ? 'white' : 'black';
          }
      });
  }
  get pagesList() {
      let mid = Math.floor(this.set_size / 2) + 1;
      if (this.page > mid) {
          return this.pages.slice(this.page - mid, this.page + mid - 1);
      }
      return this.pages.slice(0, this.set_size);
  }
  setPages = (data) => {
      let numberOfPages = Math.ceil(data / this.perpage);
      for (let index = 1; index <= numberOfPages; index++) {
          this.pages.push(index);
      }
  }
  get hasPrev() {
      return this.page > 1;
  }
  get hasNext() {
      return this.page < this.pages.length
  }
  onNext = () => {
      ++this.page;
  }
  onPrev = () => {
      --this.page;
  }
  onPageClick = (e) => {
      this.mainArr=[];
      this.page = parseInt(e.target.dataset.id, 10);
      this.offsetVal = parseInt(e.target.dataset.id, 10)*10;
      if(parseInt(e.target.dataset.id, 10)>1) {
        this.lowerLimit = ((this.page-1)*10)+1;
      } else {
        this.lowerLimit=1;
      }
      this.upperLimit = this.page*10;
      if(this.upperLimit>this.totalOrders) {
        this.upperLimit =this.totalOrders;
      }
     this.fetchOrderListValues();
  }

  @wire (orderCount,{accId :'$accId' , conId: '$conId'})
  wiredOrderCount(result) {
    if(result.data>=0) {
         /*******************************************************************
       * Newly added logic for pagintion
       * *****************************************************************/
     this.totalOrders = parseInt(result.data,10);
     if( this.totalOrders >=0) {
       this.setPages(this.totalOrders);
        if(this.totalOrders >= 10) {
            this.lowerLimit =1;
            this.upperLimit =10;
        } else if(this.totalOrders>0){
           this.lowerLimit =1;
            this.upperLimit =this.totalOrders;
        } else {
          this.lowerLimit =0;
            this.upperLimit =this.totalOrders;
        }
       }
       result = null;
    }
  }
/*********************************************************************************
 * Newly added logic for lookup filter
 * ******************************************************************************/
  handleHospChange(evt) {
    this.accList = [];
    this.conList = [];
       this.filterVal = evt.target.value;
       if(this.filterVal.length>2) {
       if(evt.target.id.includes('hospitalId')) {
        this.iconName ="standard:delegated_account";
        this.sObjName = 'Account';
       } else {
         this.iconName = "standard:contact";
         this.sObjName = 'Contact';
       }
       this.handleHospChange2();
  }
}
handleHospChange2() {
  filterAcc({ searchKeyword: this.filterVal, sObjectName : this.sObjName})
    .then(result => {
      if(result) {
      let parsedResponse = JSON.parse(result);
      let searchRecordList = parsedResponse[0];
      for ( let i=0; i < searchRecordList.length; i++ ) {
            let record = searchRecordList[i];
           let option = {Name: record['Name'].toString(),Id: record['Id'].toString()} ;
		   this.checkAccContact(option);
             }
      this.error = null;
    }
    })
    .catch(error => {
      this.error = error;
      this.accList = null;
    });
}
checkAccContact(option){
	if(this.sObjName === 'Account') {
        this.accList= [...this.accList,option];
           } else if(this.sObjName === 'Contact') {
            this.conList= [...this.conList,option];
           }
}
get shwHideLiAcc() {
  let dynamicClass;
  if (this.accList.length >0  && this.filterVal ) {
    dynamicClass =
      "slds-lookup__menu displayblock";
  } else {
    dynamicClass =
      "slds-lookup__menu";
  }
  return dynamicClass;
}
get shwHideLiCon() {
  let dynamicClass;
  if ( this.conList.length >0 && this.filterVal ) {
    dynamicClass =
      "slds-lookup__menu displayblock  ";
  } else {
    dynamicClass =
      "slds-lookup__menu";
  }
  return dynamicClass;
}

chooseThisItem(evt) {
  this.mainArr =[];
  if(this.sObjName === 'Account') {
    this.diabledAcc = true;
    this.selectedValAcc = evt.target.dataset.item;
    this.accId = evt.target.id.split('-')[0];
  } else if(this.sObjName === 'Contact') {
    this.diabledCon =true;
    this.selectedValCon = evt.target.dataset.item;
   this.conId = evt.target.id.split('-')[0]
  }
  this.accList = [];
    this.conList = [];
    this.fetchOrderListValues();
    this.setPages(this.totalOrders);
}

clearFilters() {
  this.accList =[];
  this.conList =[];
  this.accId ='';
  this.conId = '';
  this.offsetVal =0;
  this.selectedValCon =null;
  this.selectedValAcc=null;
  this.mainArr = [];
  this.fetchOrderListValues();
  this.diabledAcc = false;
  this.diabledCon = false;
}

	@wire(checkallowreplacementPermission)
    checkallowreplacementPermission({ error, data }) {
      if (data) {
        this.allowreppermission = data;
      }
      else if (error) {
        this.error = error;
      }
    }

 get enterreplacement() {
      return this.allowreppermission;
    }
}
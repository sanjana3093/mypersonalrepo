import { LightningElement,api,wire } from 'lwc';
import CCL_Request_New_Order from '@salesforce/label/c.CCL_Request_New_Order';
import checkprfviewerPermission from "@salesforce/apex/CCL_PRF_Controller.checkPRFViewer";
import checkprfapproverPermission from "@salesforce/apex/CCL_PRF_Controller.checkPRFApprover";
import checkprfsubmitterPermission from "@salesforce/apex/CCL_PRF_Controller.checkUserPermissionSet";

export default class CCL_Request_An_OrderForm_Button extends LightningElement {
    label={CCL_Request_New_Order};
  @api prfviewerpermission;
  @api prfapproverpermission;
  @api prfsubmitterpermission;
    page(){
        window.open('ccl-prf');
    }
	@wire(checkprfviewerPermission)
    prfPermissioncheck({ error, data }) {
      if (data) {
        this.prfviewerpermission = data;
      }
      else if (error) {
        this.error = error;
      }
    }
    @wire(checkprfapproverPermission)
    prfApproverPermissioncheck({ error, data }) {
      if (data) {
        this.prfapproverpermission = data;
      }
      else if (error) {
        this.error = error;
      }
    }
    @wire(checkprfsubmitterPermission)
    prfsubmitterPermissioncheck({ error, data }) {
      if (data) {
        this.prfsubmitterpermission = data;
      }
      else if (error) {
        this.error = error;
      }
    }
    get prfviewer(){
        return this.prfviewerpermission;
      }
    get prfapprover(){
        return this.prfapproverpermission && !this.prfsubmitterpermission;
    }
      

}
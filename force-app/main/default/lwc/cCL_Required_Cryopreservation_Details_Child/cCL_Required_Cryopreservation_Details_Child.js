import { LightningElement, api, track, wire } from "lwc";
import additionalComments from "@salesforce/label/c.CCL_Aph_Comments";
import cryobagInfo from "@salesforce/label/c.CCL_Cryobag_Information";
import noOfBags from "@salesforce/label/c.CCL_Number_of_Bags";
import cryobagNo from "@salesforce/label/c.CCL_CryoBag_No";
import cryoBagId from "@salesforce/label/c.CCL_CryoBag_ID";
import tnc from "@salesforce/label/c.CCL_Total_Nucleated_Cell_Count";
import cd3cellcount from "@salesforce/label/c.CCL_CD3_Cell_count";
import totalVolume from "@salesforce/label/c.CCL_Total_Volume_per_Bag_mL";
import addmaxBagsmsg from "@salesforce/label/c.CCL_Add_ten_bags_max_msg";
import addanotherbag from "@salesforce/label/c.CCL_Add_another_bag";
import subtotal from "@salesforce/label/c.CCL_Subtotal";
import wbcConcentration from "@salesforce/label/c.CCL_WBC_Concentration";
import cd3tnc from "@salesforce/label/c.CCL_CD3_TNC_X100_3";
import CCL_Field_Required from "@salesforce/label/c.CCL_Field_Required";
import enterBagId from "@salesforce/label/c.CCL_Enter_bag_ID";
import cd3threshhold from "@salesforce/label/c.CCL_Total_CD3_Cell_Count_Threshold";
import startDateGreater from "@salesforce/label/c.CCL_Start_Cryo_Greater_Collection";
// Added as part of CGTU-298
import tnc109 from "@salesforce/label/c.CCL_TNC_10_9";
import CCL_WBC_oncentration_label from "@salesforce/label/c.CCL_WBC_oncentration_label";
import CCL_CD3_suffix from "@salesforce/label/c.CCL_CD3_suffix";
//364
import enterBothDateTime from "@salesforce/label/c.CCL_Enter_both_Date_and_Time";
import badTimeInput from "@salesforce/label/c.CCL_Enter_time_in_12_hour_format";
//Added as part of 275
import dateDiff from "@salesforce/label/c.CCL_DateDiff24hr";
import dateDiffError from "@salesforce/label/c.CCL_DateDiffError";
import CCL_Future_Time_Error from "@salesforce/label/c.CCL_Future_Time_Error";

//Added as part of 273
import CCL_Cryo_Start_Date from "@salesforce/label/c.CCL_Cryo_Start_Date";
import CCL_Cryo_Start_Time from "@salesforce/label/c.CCL_Cryo_Start_Time";
import CCL_Cryo_Start_Date_Error from "@salesforce/label/c.CCL_Cryo_Start_Date_Error";
//Added as part of 290
import CCL_CryoBagId_Error from "@salesforce/label/c.CCL_CryoBagId_Error";
import { getObjectInfo, getPicklistValues } from "lightning/uiObjectInfoApi";
import Batch_Obj from "@salesforce/schema/CCL_Batch__c";
import TNC_Power from "@salesforce/schema/CCL_Batch__c.CCL_Power_Of_TNC__c";
import finishedBags from "@salesforce/label/c.CCL_Finished_Adding_Bags";
import CCL_TimeZone from "@salesforce/label/c.CCL_TimeZone";
import CCL_255_Char_Limit from "@salesforce/label/c.CCL_255_Char_Limit";
import CCL_CryoBag_ID from "@salesforce/label/c.CCL_CryoBag_ID";
import CCL_CryobagId_helptext from "@salesforce/label/c.CCL_CryobagId_helptext";
import CCL_Cell_Count from "@salesforce/label/c.CCL_Cell_Count";
import CCL_CD_Cell_Count from "@salesforce/label/c.CCL_CD_Cell_Count";
import CCL_Total_Volume_per_Bag_mL from "@salesforce/label/c.CCL_Total_Volume_per_Bag_mL";
import CCL_TNC_Err_Msg from "@salesforce/label/c.CCL_TNC_Err_Msg";
import CCL_Total_Vol_Err_Msg from "@salesforce/label/c.CCL_Total_Vol_Err_Msg";
import CCL_Action from "@salesforce/label/c.CCL_Action";
import getFormattedDateTimeDetails from "@salesforce/apex/CCL_ADFController_Utility.getFormattedDateTimeDetails";
import getPageTimeZoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getPageTimeZoneDetails";
const CCL_TEXT_Cryopreserve_Start_Date_Time__c="CCL_TEXT_Cryopreserve_Start_Date_Time__c";
const SITE_LOCATION = "CCL_Site__c";
export default class CCL_Required_Cryopreservation_Details_Child extends LightningElement {
  @api mybagvalue;
  @api selrec;
  @api collectionId;
  @api allBags;
  @track disabled = false;
  @track sectionArr;
  @track bagCount = 0;
  @track startDate;
  @track showFinishedButton;
  @track summaryValues;
  @track startTime;
   @api adfId;
  //Added as part of 273
  @track date_time;
  @track mydate;
  @track checkdate;
  @track newArr;
  @api adf = {
    CCL_GT_CD3_Cell_Count__c: "",
    CCL_GT_Nucleated_Cell_Count__c: "",
    CCL_Apheresis_Expiry_Date_Time__c: "",
    CCL_Collection_Cryopreservation_Comments__c: "",
	CCL_Power_Of_CD3__c: "9",
    CCL_Power_Of_TNC__c:"9"
  };
  @track commentChanged = true;
  @track bagList = [];
  @track EMPTY_STRING = "";
  @api navigateFurther = false;
  @api configArr;
  @api threshold;
  @api indexofcollection;
  @api numberOfCollection;
  @api wbcConcentrationFormula;
  @api cd3_tnc;
  // Added as part of CGTU-298, to store the subtotal of TNC
  @api subtotalNucleatedCellCount;
  @api cd3Total;
  @api powerOfThreshold;
  @api jsonData;
  @api storedOutput;
  @api jsonObj = {};
  @track isLoading = false;
  @track disabledbtn = false;
  @track index = 0;
  @api myFieldArr = [];
  @track inputVal;
  @track num = 0;
  @track den = 0;
  @track cd3 = 0;
  //changes for 469
  @track myTimeFields = {};
  @track myDateFields={};
  @api myTimeObj = {
  
    CCL_TEXT_Cryopreserve_Start_Date_Time__c:""
  };
  @api myDateObj = {
    CCL_TEXT_Cryopreserve_Start_Date_Time__c:""
  };
@api timeZoneDetails;
  //Added as part of 275
  @api latestComment = "";
  @api collectionEndDate;
  @api collectionEndTime;
  @api cryoStartDate;
  @api cryoStartTime;
  @api boolVisible = false;
  @api readOnlyfields=false;
  @track screennameVal="Required Cryopreservation Details";
  @track screen2nameVal="Required Collection Details";
  @api allTimeZoneDetails;
  @track siteTimeZone;
  @api allTimeZoneDetails2;
  @track siteTimeZone2;
  @track orignalBag;
  @track objectInfo;
  @track recordTypeId;
  //Ended here
  @api get disableBtn() {
    return "button disabled";
  }
  set disableBtn(prvalue) {
    if (prvalue) {
      this.disabled = true;
    } else {
      this.disabled = false;
    }
  }
  @track summary = {
    CCL_Start_of_Cryopreservation_Date__c: "",
    CCL_Start_of_Cryopreservation_Time__c: "",
    CCL_WBC_Concentration__c: "",
    CCL_ST_Nucleated_Cell_Count__c: "",
    CCL_Aph_Colln_Cryopreservation_Comments__c: "",
    CCL_ST_CD3_Cell_Count__c: "",
	CCL_CD3_TNC_X100_3__c: "",
    Id: this.collectionId,
   CCL_TEXT_Cryopreserve_Start_Date_Time__c:"",
    CCL_Start_of_Cryopreservation_Date_Time__c:"",
	 CCL_Power_Of_CD3__c:"9",
    CCL_Power_Of_TNC__c:"9",
	CCL_Power_Of_WBC__c:"8"
  };
  @track label = {
    cryobagInfo,
    noOfBags,
    cryobagNo,
    cryoBagId,
    tnc,
    cd3cellcount,
    totalVolume,
    addmaxBagsmsg,
    addanotherbag,
    subtotal,
    wbcConcentration,
    cd3tnc,
    enterBagId,
    cd3threshhold,
    dateDiff,
	CCL_Cryo_Start_Date, CCL_Cryo_Start_Time, CCL_Future_Time_Error,
    CCL_Cryo_Start_Date_Error,CCL_CryoBagId_Error,
  finishedBags,
  CCL_Total_Vol_Err_Msg,
  CCL_TNC_Err_Msg,
  CCL_Total_Volume_per_Bag_mL,
  CCL_CD_Cell_Count,
  CCL_Cell_Count,
  CCL_CryobagId_helptext,
  CCL_CryoBag_ID,
  CCL_255_Char_Limit,
  CCL_TimeZone,
  CCL_Action
  };
  @api cryoComments;
  @track comments = additionalComments;
  @api count = 1;
  @api get savedForlaterClicked() {
    return "save clicked";
  }
	@track prevClicked=false;
  @api
  get timeZone() {
    return this.timeZoneDetails;
  }
  set timeZone(timezoneVal) {
    this.timeZoneDetails=timezoneVal;
  }

  set savedForlaterClicked(prvalue) {
    if (prvalue) {
		this.prevClicked=true;
      this.adf.CCL_Collection_Cryopreservation_Comments__c = this.cryoComments;
      this.summary.Id=this.collectionId;
      if(this.summary.CCL_TEXT_Cryopreserve_Start_Date_Time__c!=undefined&&this.summary.CCL_TEXT_Cryopreserve_Start_Date_Time__c!=''){
        this.getFormattedDateTimeVal(this.summary.CCL_TEXT_Cryopreserve_Start_Date_Time__c,this.timeZoneDetails[CCL_TEXT_Cryopreserve_Start_Date_Time__c],false);
      }else{
        this.fireEventToParent(false);
      }
     
    
    }
  }
  @api get isNextClicked() {
    return "next is clicked";
  }
  set isNextClicked(prvalue) {
    if (prvalue) {
      this.checkdate = 1;
      this.validateFields();
      const selectEvent2 = new CustomEvent("saveclickedfalse", {});
      this.dispatchEvent(selectEvent2);
      this.getFormattedDateTimeVal(
        this.summary.CCL_TEXT_Cryopreserve_Start_Date_Time__c,
        this.timeZoneDetails[CCL_TEXT_Cryopreserve_Start_Date_Time__c],
        true
      );
    }
  }
  @api
  get myActualArr() {
    return this.selrec;
  }
  set myActualArr(prvalue) {
    this.configArr = prvalue;
  }
  @api
  get recordId() {
    return this.selrec;
  }
  set recordId(prvalue) {
    this.collectionId = prvalue;
    let self = this;
    this.configArr.forEach(function (node) {
      if (node.Id == prvalue) {
        self.sectionArr = node;
      }
    });
  }
  @api
  get collectionValues() {
   return this.selrec;
  }
  set collectionValues(cvalues) {
    let self = this;
    if(cvalues!=undefined){
      cvalues.forEach(function (node) {
        if (node.Id == self.collectionId) {
         self.inputVal = node;
        }
      });
    }
    
  
  }
  get options() {
    return [
      { label: "Yes", value: true },
      { label: "No", value: false }
    ];
  }
  @api bag = {
    CCL_Cryobag_ID__c: "",
    CCL_Nucleated_Cell_Count__c: "",
    CCL_Total_Volume_Per_Bag__c: "",
    CCL_CD3_Cell_Count__c: "",
    CCL_Power_Of_CD3__c: "",
    CCL_Power_Of_TNC__c: "",
    CCL_Apheresis_Data_Form__c: "",
    CCL_Order__c: "",
    CCL_Cryobag__c:"",
    CCL_Collection__c: this.collectionId,
    key: "",
	CCL_Cryobag_External_ID__c:''
  };
    @wire(getObjectInfo, { objectApiName: Batch_Obj })
      wiredObjectInfo({ error, data }) {
      if(data){
        this.objectInfo=data;
        const rtis =this.objectInfo.recordTypeInfos;
        this.recordTypeId= Object.keys(rtis).find(rti => rtis[rti].name === 'Cryobag');
      }
      else if (error){
        
      }
    }
  @wire(getPicklistValues, {
    recordTypeId: "$recordTypeId",
    fieldApiName: TNC_Power
  })
  systemUsedPicklistValues(result) {
    if (result.data) {
      this.newArr = result.data.values;
    }
  }
  setValues(myFieldArr, inputVal) {
    let fieldArr = {
      Fieldlabel: "",
      fieldApiName: "",
      fieldtype: "",
      fieldorder: "",
      fieldValue: "",
      fieldhelptext: "",
      isRadio: "",
      isEditable: "",
      Id: "",
	  isDate: "",
      class: "",
      isTime: "",
      timeZone:""
    };
    let self = this;
    myFieldArr.myFields.forEach(function (node) {
      fieldArr = {
        Fieldlabel: "",
        fieldApiName: "",
        fieldtype: "",
        fieldorder: "",
        fieldValue: "",
        fieldhelptext: "",
        isRadio: "",
        isEditable: "",
        Id: "",
        isDate: "",
        class: "",
        isTime: "",
        timeZone:"",
        readOnlyTime:"",
		badInput:""
      };
      fieldArr.fieldApiName = node.fieldApiName;
      fieldArr.fieldtype = node.fieldtype;
      fieldArr.Fieldlabel = node.Fieldlabel;
      fieldArr.isRadio = node.isRadio;
 	  fieldArr.class=node.class;
      if(self.readOnlyfields){
        fieldArr.isEditable=true;
      }else{
        fieldArr.isEditable=node.isEditable;
      }
      //changes for 469
      fieldArr.fieldLength =
        node.fieldLength == "Full"
          ? "slds-col slds-size_1-of-1 slds-grid"
          : "slds-col slds-size_1-of-2 slds-grid";
      if(node.fieldApiName && inputVal && inputVal[node.fieldApiName]){
        fieldArr.fieldValue = node.isEditable ? inputVal[node.fieldApiName]: "";
      }else{
        fieldArr.fieldValue="";
      }
      //changes for 469
      fieldArr.isTime=node.isTime;
      fieldArr.isDate=node.isDate;
      if(fieldArr.isTime){
		   //364
      fieldArr.badInput=badTimeInput;
      fieldArr.timeZone=self.timeZoneDetails[node.fieldApiName];
      }
      
      let readTimeZone='';
      if(node.fieldApiName=='CCL_TEXT_Aph_Collection_End_Date_Time__c' && node.fieldtype=='time'){ 
      readTimeZone=fieldArr.fieldValue!==undefined?fieldArr.fieldValue.substr(25,28):'';
      fieldArr.Fieldlabel='End of Apheresis Collection Time';
      fieldArr.readOnlyTime=true;
      }
      if(node.fieldApiName=='CCL_TEXT_Aph_Collection_End_Date_Time__c' && node.fieldtype=='date'){ 
        fieldArr.Fieldlabel='End of Apheresis Collection Date';
        }
      let value=inputVal?inputVal[node.fieldApiName]:"";
      fieldArr.fieldValue=self.getFormattedDateTimeValue(node.fieldtype,node.isEditable,value);
      fieldArr.fieldtype = node.isEditable ? "text" : node.fieldtype;
      if(node.fieldApiName=='CCL_TEXT_Cryopreserve_Start_Date_Time__c' && self.summaryValues!=undefined && node.fieldtype=='time'){
       
        fieldArr.fieldValue='';
        fieldArr.readOnlyTime=false;
        let textVal=self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c;
        if(textVal!==undefined && self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c){
          self.myTimeFields.CCL_TEXT_Cryopreserve_Start_Date_Time__c=self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c.substr(12,16);
        }       
        fieldArr.isTime = true;
        self.summary.CCL_TEXT_Cryopreserve_Start_Date_Time__c=self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c;
        if(self.readOnlyfields){
          let textVal=self.summaryValues!==undefined?self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c:'';
          if(textVal!==undefined && textVal!=='' && self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c){
            fieldArr.fieldValue = self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c.substr(12,5);
            fieldArr.fieldValue=fieldArr.fieldValue.substr(0,5);
            fieldArr.class='labelClass ';
          } 
		   fieldArr.fieldtype="text";
      }
     
       
    }
      if(node.fieldApiName=='CCL_TEXT_Aph_Collection_End_Date_Time__c' && node.fieldtype=='time'){     
        fieldArr.isTime = true;
        fieldArr.timeZone=readTimeZone;
      }
      
      if(node.fieldApiName=='CCL_TEXT_Cryopreserve_Start_Date_Time__c' && self.summaryValues!=undefined&& node.fieldtype=='date'){
        fieldArr.fieldValue='';
        let textVal=self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c;
        if(textVal!==undefined){
          self.myDateFields.CCL_TEXT_Cryopreserve_Start_Date_Time__c=self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c.substr(0,12);
        } 
        fieldArr.isDate = true;
        self.summary.CCL_TEXT_Cryopreserve_Start_Date_Time__c=self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c;
        if(self.readOnlyfields){
          let textVal=self.summaryValues!==undefined?self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c:'';
          if(textVal!==undefined && textVal!=='' && self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c){
            fieldArr.fieldValue = self.summaryValues.CCL_TEXT_Cryopreserve_Start_Date_Time__c.substr(0,12);
            fieldArr.class='labelClass ';
          } 
            fieldArr.fieldtype="text";
        }
      }
      
      self.myFieldArr.push(fieldArr);
    });
  }

  getFormattedDateTimeValue(fieldType,isEditable,fieldValue){
    let value=fieldValue;
    if (fieldType === "date" && fieldValue) {
      value = isEditable
        ? this.formatDate(fieldValue.substr(0,12))
        : "";
    }
    if (fieldType == "time" && fieldValue) {
      value = isEditable
        ? fieldValue.substr(12,16).substr(0,5)
        : "";
    }
    return value;
  }

    getFormattedTime(fieldValue){
          let timeval=fieldValue;
          if(timeval!==null && timeval!=='' && timeval!==undefined){
            let format;
            let hrs = timeval.substr(0, 2);
            let min = timeval.substr(3, 2);
            if(hrs>12){
              hrs=hrs-12;
              format=' PM'
            }else{
              format=' AM'
            }
            if(hrs==='00'){
              hrs=12;
            }
            return hrs+':'+min+format;}
            return fieldValue;
}


  @api get storedValues(){
    return 'values';
  }
  set storedValues(prvalue){
    this.storedOutput=prvalue;
    if(this.collectionId!=undefined&&prvalue!=undefined){
      this.setOldValues(prvalue);
    }
  }
  connectedCallback() {
    this.bagList.push(JSON.parse(JSON.stringify(this.bag)));
    let self=this;
    if(this.storedOutput!=undefined){
     this. storedOutput.forEach(function(node){
        if(node.Id==self.collectionId){
         self.bagList =JSON.parse(JSON.stringify(node.bags));
         self.summaryValues=node.summary;
        }
      });
	  this.bagCount=this.bagList.length;
      this.index=this.bagList.length-1;
	  this.matchBags();
      if(this.summaryValues!=undefined){		
		this.summary.CCL_ST_CD3_Cell_Count__c= this.summaryValues.CCL_ST_CD3_Cell_Count__c;
        this.summary.CCL_ST_Nucleated_Cell_Count__c= this.summaryValues.CCL_ST_Nucleated_Cell_Count__c;
        this.summary.CCL_WBC_Concentration__c= this.summaryValues.CCL_WBC_Concentration__c;
        this.summary.CCL_CD3_TNC_X100_3__c = this.summaryValues.CCL_CD3_TNC_X100_3__c;
        this.subtotalNucleatedCellCount=this.summaryValues.CCL_ST_Nucleated_Cell_Count__c+' '+tnc109;
        this.wbcConcentrationFormula=this.summaryValues.CCL_WBC_Concentration__c+' '+CCL_WBC_oncentration_label;
        this.cd3Total=this.summaryValues.CCL_ST_CD3_Cell_Count__c+' '+CCL_CD3_suffix;
        this.cd3_tnc =this.summaryValues.CCL_CD3_TNC_X100_3__c+" %";
      }
     
    }
    this.setValues(this.sectionArr, this.inputVal);
    if (this.jsonData != undefined || this.jsonData != null) {
      this.jsonObj = JSON.parse(this.jsonData);
      this.cryoComments = this.jsonObj["Collection and Cryopreservation Comments"];
    }
    this.getTimeZoneScreenDetails();
    this.getTimeZoneScreen2Details();
  }

    getTimeZoneScreenDetails(){
    getPageTimeZoneDetails({screenName:this.screennameVal})
      .then((result) => {
        this.allTimeZoneDetails = result;
        this.siteTimeZone='Time zone of '+this.allTimeZoneDetails[0][SITE_LOCATION];
        
      })
      .catch((error) => {
        this.error = error;
      });
  }
  
    getTimeZoneScreen2Details(){
    getPageTimeZoneDetails({screenName:this.screen2nameVal})
      .then((result) => {
        this.allTimeZoneDetails2 = result;
        this.siteTimeZone2='Time zone of '+this.allTimeZoneDetails2[0][SITE_LOCATION];
        
      })
      .catch((error) => {
        this.error = error;
      });
  }
  
  formatDate(date) {
  if(date!==null && date!==''&& date!==undefined){
  let mydate = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    mydate=new Date(date);
  }
    let monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];
    let day = '' + mydate.getDate();
    if (day.length < 2) 
    day = '0' + day;
    let monthIndex = mydate.getMonth();
    let monthName = monthNames[monthIndex];
    let year = mydate.getFullYear();
    return `${day} ${monthName} ${year}`;
}
return '';
}

matchBags(){
    
    let allBags=[];
    let self=this;
    
   
    
    if(this.allBags!=null&&this.allBags!=undefined){
      this.allBags=JSON.parse(this.allBags);
      this.allBags.forEach(function(node){
          if(node.CCL_Collection__c==self.collectionId){
            allBags.push(node);
          }
      });
    }
    if(allBags.length>0){
      this.bagList=allBags;
    }
    for(let i=0;i<this.bagList.length;i++){
      if(this.bagList[i]["attributes"]){
        delete this.bagList[i]["attributes"];
        this.bagList[i].key = i;
      }
    }
    
  }


  msToTime(s) {
    let ms = s % 1000;
    s = (s - ms) / 1000;
    let secs = s % 60;
    s = (s - secs) / 60;
    let mins = s % 60;
    let hrs = (s - mins) / 60;
    hrs = hrs < 10 ? "0" + hrs : hrs;
    mins = mins < 10 ? "0" + mins : mins;
    return hrs + ":" + mins;
  }
  handleCommentsChange(event) {
    this.cryoComments = event.target.value;
    this.summary.CCL_Aph_Colln_Cryopreservation_Comments__c =
      event.target.value;
    this.summary.Id = this.collectionId;
    const selectEvent = new CustomEvent("commentchange", {
      detail: event.target.value
    });
    this.dispatchEvent(selectEvent);
  }
  addBag() {
	  this.checkdate = 0;
    this.validateFields();
    const allValid = [...this.template.querySelectorAll(".myInput")].reduce(
      (validSoFar, inputCmp) => {
        inputCmp.reportValidity();
        return validSoFar && inputCmp.checkValidity();
      },
      true
    );
    if (allValid && this.navigateFurther) {
      this.template.querySelectorAll(".myInput").forEach(function (node) {
        node.classList.remove("marginBottom");
      });
      if (this.bagList.length < 10) {
        this.disabledbtn = false;
		this.index++;
		const i = this.index;
        this.count = this.index + 1;
        let temp = { count: this.count, index: this.indexofcollection };
        const selectEvent = new CustomEvent("mycountevent", {
          detail: temp
        });
        this.dispatchEvent(selectEvent);
        this.bag.key = i;
        let mybag=[];
        this.bagList.push(JSON.parse(JSON.stringify(this.bag)));
        this.bagCount = this.bagList.length - 1;
      } else {
        this.disabledbtn = true;
      }
    }
  }
  removeBag(event) {
    this.isLoading = true;
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.calculateWBC(
      this.bagList[key].CCL_Nucleated_Cell_Count__c,
      this.bagList[key].CCL_Power_Of_TNC__c,
      this.bagList[key].CCL_Total_Volume_Per_Bag__c,
      this.bagList[key].CCL_CD3_Cell_Count__c,
      this.bagList[key].CCL_Power_Of_CD3__c,
      false
    );
    if (this.bagList.length > 1) {
      this.bagList.splice(key, 1);
      this.index--;
      this.count = this.index + 1;
      this.isLoading = false;
      let temp = { count: this.count, index: this.indexofcollection };
      const selectEvent = new CustomEvent("mycountevent", {
        detail: temp
      });
      this.dispatchEvent(selectEvent);
      this.disabledbtn = false;
    } else if (this.bagList.length == 1) {
      this.bagList = [];
      this.index = 0;
      this.count = 1;
      this.isLoading = false;
    }
    this.bagCount = this.bagList.length;
  }
  get renderdelete() {
    return this.index !== 0 ? true : false;
  }

  handleIdChange(event) {
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.bagList[key].CCL_Cryobag__c=parseInt(key,10)+1;
    
    this.bagList[key].CCL_Cryobag_ID__c = event.target.value;
    this.bagList[key].CCL_Collection__c = this.collectionId;
	this.bagList[key].CCL_Cryobag_External_ID__c=this.adfId+this.collectionId+key;
  }

  handleTNCpowerChange(event) {
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.den = null;
    let self = this;
    this.num = null;
    this.cd3 = null;
    this.bagList[key].CCL_Power_Of_TNC__c = event.target.value;
    if (
      this.bagList[key].CCL_Nucleated_Cell_Count__c &&
      this.bagList[key].CCL_Power_Of_TNC__c &&
      this.bagList[key].CCL_Total_Volume_Per_Bag__c &&
      this.bagList[key].CCL_CD3_Cell_Count__c &&
      this.bagList[key].CCL_Power_Of_CD3__c
    ) {
      this.calculateWBCForBagList(key);
    }
  }
  handleTNCChange(event) {
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.den = null;
    let self = this;
    this.num = null;
    this.cd3 = null;
    this.bagList[key].CCL_Nucleated_Cell_Count__c = event.target.value;
    if (
      this.bagList[key].CCL_Power_Of_TNC__c &&
      this.bagList[key].CCL_Nucleated_Cell_Count__c &&
      this.bagList[key].CCL_Total_Volume_Per_Bag__c &&
      this.bagList[key].CCL_CD3_Cell_Count__c &&
      this.bagList[key].CCL_Power_Of_CD3__c
    ) {
      this.calculateWBCForBagList(key);
    }
  }
  handleCD3Change(event) {
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.den = null;
    let self = this;
    this.num = null;
    this.cd3 = null;

    this.bagList[key].CCL_CD3_Cell_Count__c = event.target.value;
    if (
      this.bagList[key].CCL_Power_Of_TNC__c &&
      this.bagList[key].CCL_Nucleated_Cell_Count__c &&
      this.bagList[key].CCL_Total_Volume_Per_Bag__c &&
      this.bagList[key].CCL_CD3_Cell_Count__c &&
      this.bagList[key].CCL_Power_Of_CD3__c
    ) {
      this.calculateWBCForBagList(key);
    }
  }
  handleCD3powerChange(event) {
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.den = null;
    let self = this;
    this.num = null;
    this.cd3 = null;
    this.bagList[key].CCL_Power_Of_CD3__c = event.target.value;
    if (
      this.bagList[key].CCL_Power_Of_TNC__c &&
      this.bagList[key].CCL_Nucleated_Cell_Count__c &&
      this.bagList[key].CCL_Total_Volume_Per_Bag__c &&
      this.bagList[key].CCL_CD3_Cell_Count__c &&
      this.bagList[key].CCL_Power_Of_CD3__c
    ) {
      this.calculateWBCForBagList(key);
    }
  }
  handleVolChange(event) {
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    this.den = null;
    let self = this;
    this.num = null;
    this.cd3 = null;
    this.bagList[key].CCL_Total_Volume_Per_Bag__c = event.target.value;
    if (
      this.bagList[key].CCL_Nucleated_Cell_Count__c &&
      this.bagList[key].CCL_Total_Volume_Per_Bag__c &&
      this.bagList[key].CCL_Power_Of_TNC__c &&
      this.bagList[key].CCL_CD3_Cell_Count__c &&
      this.bagList[key].CCL_Power_Of_CD3__c
    ) {
      this.calculateWBCForBagList(key);
    }
  }

  calculateWBCForBagList(key){
	   if(key==0){
      this.count =  1;
      let temp = { count: this.count, index: this.indexofcollection };
      const selectEvent = new CustomEvent("mycountevent", {
        detail: temp
      });
      this.dispatchEvent(selectEvent);
    }
    let self=this;
    this.bagList.forEach(function (node) {
      self.calculateWBC(
        node.CCL_Nucleated_Cell_Count__c,
        node.CCL_Power_Of_TNC__c,
        node.CCL_Total_Volume_Per_Bag__c,
        node.CCL_CD3_Cell_Count__c,
        node.CCL_Power_Of_CD3__c,
        true
      );
    });
  }

  // Added as part of CGTU-300 and CGTU-298
   calculateWBC(tnc, tncpower, vol, cdc, cdcpower, addRemove) {
    this.summary.Id = this.collectionId;
    let tncVal,cd3Val,cd3tncVal;
	this.bagCount=this.bagList.length;
    if (addRemove === true && tnc!=='' && tncpower!='' && vol!=='' && cdcpower!='' && cdc!=='' ) {
      this.num += parseFloat(tnc) * Math.pow(10, parseInt(tncpower, 10));
      this.den += parseInt(vol, 10);
      this.cd3 += parseFloat(cdc, 10) * Math.pow(10, parseInt(cdcpower, 10));
    } else if(addRemove === false && tnc!=='' && tncpower!='' && vol!=='' && cdcpower!='' && cdc!=='' ){
      this.num -= parseFloat(tnc) * Math.pow(10, parseInt(tncpower, 10));
      this.den -= parseInt(vol, 10);
      this.cd3 -= parseFloat(cdc, 10) * Math.pow(10, parseInt(cdcpower, 10));
    }
    if (this.num !== 0 && this.num !== null) {
      const calcVal = this.toFixedNoRounding((this.num / Math.pow(10, 9))).toString();
      tncVal=(this.num / Math.pow(10, 9));
      this.summary.CCL_ST_Nucleated_Cell_Count__c = calcVal;
      this.subtotalNucleatedCellCount = calcVal + " " + tnc109;
    }
    // Changes End
    if (this.den !== 0 && this.den !==null) {
      const calcVal = this.toFixedNoRounding((this.num / (this.den * Math.pow(10, 8)))).toString();
        
      this.wbcConcentrationFormula = calcVal + " " + CCL_WBC_oncentration_label;
      this.summary.CCL_WBC_Concentration__c = calcVal;
    }
    if (this.cd3 != 0 && this.cd3!==null) {;
     const caclcVal = this.toFixedNoRounding((this.cd3 / Math.pow(10, 9))).toString();
      cd3Val=(this.cd3 / Math.pow(10, 9));
      this.cd3Total = caclcVal + " " + CCL_CD3_suffix;
      this.summary.CCL_ST_CD3_Cell_Count__c = caclcVal;
    }
    if(this.num !== 0 && this.num !== null && this.cd3 != 0 && this.cd3!==null ){
      const calcval = this.toFixedNoRounding ((this.cd3 / this.num)*100).toString();
      this.cd3_tnc = calcval + " %";
      cd3tncVal=(this.cd3/(this.num));
      this.summary.CCL_CD3_TNC_X100_3__c = calcval;
    }
    let temp = { tnc: tncVal,cd3:cd3Val,cd3tnc:cd3tncVal, index: this.indexofcollection };
    const selectEvent = new CustomEvent("mycalcevent", {
      detail: temp
    });
    this.dispatchEvent(selectEvent);
   
  }
  checkValidation() {
    this.fieldValidations = this.template.querySelectorAll(".myInput");
    let invalid = false;
    let mynode;
     this.fieldValidations.forEach(function (node) {
      if (node.checkValidity()) {
        node.classList.add("marginBottom");
      }else{
        node.classList.remove("marginBottom");
      }
    });
   
    const allValid = [...this.template.querySelectorAll(".myInput")].reduce(
      (validSoFar, inputCmp) => {
        inputCmp.reportValidity();
        return validSoFar && inputCmp.checkValidity();
      },
      true
    );
    if (allValid) {
      this.template.querySelectorAll(".myInput").forEach(function (node) {
        node.classList.remove("marginBottom");
      });
    }
  }
  validateFields() {
    this.fieldValidations = this.template.querySelectorAll(".myInput");
	this.date_time= this.template.querySelectorAll(".date_time");
    for (let i = 0; i < this.fieldValidations.length; i++) {
		this.fieldValidations[i].setCustomValidity(this.EMPTY_STRING);
      this.fieldNullCheck(this.fieldValidations[i], CCL_Field_Required);
    }
	if(this.checkdate===1){
    for (let j = 0; j < this.date_time.length; j++) {
      if(this.date_time[j].label===CCL_Cryo_Start_Date || this.date_time[j].label===CCL_Cryo_Start_Time){	
        if(this.date_time[j].label===CCL_Cryo_Start_Date ){	
          this.mydate = this.date_time[j].value;	
        }
        this.date_time[j].setCustomValidity(this.EMPTY_STRING);
        this.fieldNullCheck(this.date_time[j], CCL_Field_Required);
      }
    }
  }
   this.checkValidation();
  }
 fieldNullCheck(comboCmp, errorMsg) {
    let value;
    if (comboCmp.type == "Date" || comboCmp.type == "date") {
      value = this.myDateFields[comboCmp.dataset.item];
    } else if (comboCmp.type == "Time" || comboCmp.type == "time") {
      value = this.myTimeFields[comboCmp.dataset.item];
    } else {
      value = comboCmp.value;
    }
    if (value === this.EMPTY_STRING || value === undefined || value == null) {
      comboCmp.setCustomValidity(errorMsg);
      comboCmp.reportValidity();

      this.navigateFurther = false;
    } else if (comboCmp.name == "cryobagId" && comboCmp.checkValidity()) {
     
      let letters = /^[0-9a-zA-Z]+$/;
      if (comboCmp.value.match(letters)) {
       
      } else {
        
        comboCmp.setCustomValidity(CCL_CryoBagId_Error);
        comboCmp.reportValidity();
        this.navigateFurther = false;
      }
    } else {
      comboCmp.classList.add("marginBottom");
      this.navigateFurther = true;
      comboCmp.setCustomValidity(this.EMPTY_STRING);
      comboCmp.reportValidity();
    }
    this.checkDateTime(comboCmp, value);
  }
  //364
  checkDateTime(comboCmp, value) {
	   let key=comboCmp.dataset.item;
	  if(this.myDateFields[key]!=undefined&&this.myTimeFields[key]==undefined &&comboCmp.type=='time'){
      comboCmp.setCustomValidity(enterBothDateTime);
      comboCmp.reportValidity();
    }
    else if(this.myDateFields[key]==undefined&&this.myTimeFields[key]!=undefined &&comboCmp.type=='date'){
      comboCmp.setCustomValidity(enterBothDateTime);
      comboCmp.reportValidity();
    }
    if (comboCmp.label == CCL_Cryo_Start_Date && comboCmp.checkValidity()) {
     
      let date = new Date();
      let mydate = new Date(this.myDateFields[comboCmp.dataset.item]);
      if (date < mydate) {
       
        comboCmp.setCustomValidity(CCL_Cryo_Start_Date_Error);
        comboCmp.reportValidity();

        this.navigateFurther = false;
      }

      this.checkForStartTimeGreter(comboCmp);
    } else if (
      comboCmp.label == CCL_Cryo_Start_Time &&
      comboCmp.checkValidity()
    ) {
      let currentDateTime = new Date();
      let inputDateTime = new Date();
      let currentTime = currentDateTime.getTime();
      let time = this.myTimeFields[comboCmp.dataset.item].split(":");
      inputDateTime.setHours(time[0], time[1], 0);
      let inputTime = inputDateTime.getTime();
      if (
        inputTime > currentTime &&
        new Date(this.mydate).getDate() >= currentDateTime.getDate() &&
        new Date(this.mydate).getMonth() >= currentDateTime.getMonth() &&
        new Date(this.mydate).getFullYear() >= currentDateTime.getFullYear()
      ) {
        comboCmp.setCustomValidity(CCL_Future_Time_Error);
        comboCmp.reportValidity();
      }
    }
  }

  checkForStartTimeGreter(comboCmp) {
    if (
      this.myTimeFields[comboCmp.dataset.item] != undefined &&
      this.myTimeFields[comboCmp.dataset.item].length > 0
    ) {
      let mydate = new Date(
        this.myDateFields[comboCmp.dataset.item] +
          " " +
          this.myTimeFields[comboCmp.dataset.item]
      );
      let collectionDate = new Date(
        this.inputVal.CCL_TEXT_Aph_Collection_End_Date_Time__c.substr(0, 17)
      );
      if (mydate < collectionDate && comboCmp.type == "date") {
        comboCmp.setCustomValidity(startDateGreater);
        comboCmp.reportValidity();
        this.navigateFurther = false;
      }
    } else {
      comboCmp.setCustomValidity("");
      comboCmp.reportValidity();
    }
  }
  onClickTNCPower(event){
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    const menu = this.template.querySelectorAll('.tncPowMenu');
    const menu2 = this.template.querySelectorAll('.cdcPowMenu');
  }
  onClickCDCPower(event){
    const selectedRow = event.currentTarget;
    const key = selectedRow.dataset.id;
    const menu = this.template.querySelectorAll('.cdcPowMenu');
    const menu2 = this.template.querySelectorAll('.tncPowMenu');
  }

  handleBlur(event){
    let apiName = event.target.dataset.item;
    let comp2 = this.template.querySelectorAll(".timeClass");
    
    if (apiName in this.myTimeObj && event.target.classList.contains('timeClass')) {
      this.myTimeFields[apiName] = event.target.value;
      comp2.forEach(function (node) {
        node.value = "";
      });
    }

  }

  handleChange(event) {
    let apiName = event.target.dataset.item;
    this.summary.Id = this.collectionId;
    let comp=this.template.querySelectorAll('.dateClass');
    let comp2=this.template.querySelectorAll('.timeClass');
    if (event.target.classList.contains('dateClass')) {
      this.startDate = event.target.value;
    }
    if (event.target.classList.contains('timeClass')) {
      this.startTime = event.target.value;
    }
	 if(this.startDate===undefined){
       this.startTime=this.myTimeFields[apiName]!=undefined?this.myTimeFields[apiName].substr(0,12):'';
    }
	if(this.startTime===undefined&&this.myTimeFields[apiName]!=undefined) {
      this.startTime=this.myTimeFields[apiName].substr(0,12);
    }
    //Added as part of 275
    this.collectionEndDate = new Date(
      this.inputVal.CCL_TEXT_Aph_Collection_End_Date_Time__c.substr(0,12)
    );
     this.collectionEndTime =this.inputVal.CCL_TEXT_Aph_Collection_End_Date_Time__c.substr(12,5);
    let hrs = this.collectionEndTime.substr(0, 2);
    let min = this.collectionEndTime.substr(3, 2);
    this.collectionEndDate.setHours(hrs, min);
      this.cryoStartDate = new Date( this.startDate);
      this.cryoStartTime = this.startTime;
    if(this.cryoStartDate && this.cryoStartTime){
      let hours = this.cryoStartTime.substr(0, 2);
      let minutes = this.cryoStartTime.substr(3, 2);
      this.cryoStartDate.setHours(hours, minutes);
      if (
        (this.cryoStartDate.getTime() - this.collectionEndDate.getTime()) /
          (1000 * 60 * 60) >
        "24"
      ) {
        this.boolVisible = true;
      } else {
        this.boolVisible = false;
      }
    }
    //changes for 469
    if(apiName in this.myDateObj && event.target.classList.contains('dateClass')){
      this.myDateFields[apiName]=this.formatDate(event.target.value);
      comp.forEach(function(node){
        node.value='';
      });
    }
    if(apiName in this.myTimeObj && event.target.classList.contains('timeClass')){
      this.myTimeFields[apiName]=event.target.value;
    }
   
    if(event.target.dataset.item=='CCL_TEXT_Cryopreserve_Start_Date_Time__c'){
      this.summary.CCL_TEXT_Cryopreserve_Start_Date_Time__c= this.myDateFields[apiName].substr(0,11)+" "+this.myTimeFields[apiName]+" "+'EST';
    }
   
  }
  @api
   validateCommentsChanges() {
    //364
    if (
      this.jsonObj["Collection and Cryopreservation Comments"] == undefined &&
      this.cryoComments == undefined && this.boolVisible
    ) {
      this.commentChanged = false;
      const CommentBox = this.template.querySelector(".commenntClass");
      CommentBox.setCustomValidity(dateDiffError);
      CommentBox.reportValidity();
    } else if (
      this.boolVisible &&
      (this.cryoComments.localeCompare(
        this.jsonObj["Collection and Cryopreservation Comments"]
      ) == 0 ||
        this.cryoComments == null ||
        this.cryoComments == "" ||
        this.cryoComments == undefined)
    ) {
     
      this.commentChanged = false;
      const CommentBox = this.template.querySelector(".commenntClass");
      CommentBox.setCustomValidity(dateDiffError);
      CommentBox.reportValidity();
    } else {
      this.commentChanged = true;
    }
  }
  setOldValues(storedArr){
    let self=this;
    let summary;
    storedArr.forEach(function(node){
      if(node.Id==self.collectionId){
       summary=node.summary;
      }
    });

  }

  @api
  get summarypage() {
    return false;
  }
  set summarypage(prvalue) {
    if (prvalue) {
        this.readOnlyfields=true;
    }
  }
   // as part of CGTU 467
  handleFinishBag(){
    const selectEvent = new CustomEvent("finishedaddbag", {
    });
    this.dispatchEvent(selectEvent);
  }
   renderedCallback(){
    if((this.numberOfCollection-1)==this.indexofcollection){
      this.showFinishedButton=true;
    }
  }
  //469
  //changes for 364
  getFormattedDateTimeVal(dateTimeVal, timeZoneVal, isNext) {
    let dateTime =
      this.formatDateFormat(dateTimeVal.substr(0, 13)) +
      " " +
      dateTimeVal.substr(12, 17);
    let currentDate = new Date();
    getFormattedDateTimeDetails({
      dateTimeVal: dateTime,
      timeZoneVal: timeZoneVal
    })
      .then((result) => {
        this.summary.CCL_Start_of_Cryopreservation_Date__c = result;
       
        let resultDate = new Date(result);
        //364
        let nowDate = new Date(currentDate.toISOString());
        if (
          resultDate.getTime() > nowDate.getTime() &&
          nowDate.getDate() == resultDate.getDate() &&
          nowDate.getMonth() == resultDate.getMonth() &&
          nowDate.getYear() == resultDate.getYear()
        ) {
          this.validateTime();
        } else if (nowDate < resultDate) {
          this.validateTime();
        }
        this.fireToParent();
      })
      .catch((error) => {
		  this.fireToParent();
        this.error = error;
        
      });
  }
  validateTime() {
    this.template.querySelectorAll("lightning-input").forEach(function (node) {
      if (node.type == "date") {
        node.setCustomValidity(CCL_Cryo_Start_Date_Error);
        node.reportValidity();
      }
    });
    this.navigateFurther = false;
  }
  fireToParent() {
    const allValid = [...this.template.querySelectorAll(".queryComp")].reduce(
      (validSoFar, inputCmp) => {
        
        inputCmp.reportValidity();
        return validSoFar && inputCmp.checkValidity();
      },
      true
    );
    this.navigateFurther =
      this.navigateFurther && allValid && this.commentChanged;
    this.adf.CCL_Collection_Cryopreservation_Comments__c = this.cryoComments;
    this.summary.Id = this.collectionId;
    //364
	 if(this.prevClicked){
      this.fireEventToParent(false);
    } else{
		this.fireEventToParent(true);
    }
  }
  formatDateFormat(date) {
   
    if(date!==null && date!==''&& date!==undefined){
		let d = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    d=new Date(date);
  }
  
      let month = '' + (d.getMonth() + 1),
       day = '' + d.getUTCDate(),
      year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;

  return [year, month, day].join('-');
  }

  return '';
  } 
  fireEventToParent(isNext){
    if(isNext){
         let bagDetails = {
        navigateFurther: this.navigateFurther,
        bags: this.bagList,
        summary: this.summary,
        adf: this.adf,
        Id: this.collectionId
      };
      const selectEvent2 = new CustomEvent("mysaveevent", {
        detail: bagDetails
      });
      this.dispatchEvent(selectEvent2);
      const selectEvent = new CustomEvent("myvalidationevent", {
        detail: bagDetails
      });
      this.dispatchEvent(selectEvent);
    }else{
      let bagDetails = {
		  commentChanged: this.commentChanged,
        bags: this.bagList,
        summary: this.summary,
        adf: this.adf,
        Id: this.collectionId
      };
      const selectEvent = new CustomEvent("mysaveevent", {
        detail: bagDetails
      });
      this.dispatchEvent(selectEvent);
    }
  }
   toFixedNoRounding(number){
  const reg = new RegExp("^-?\\d+(?:\\.\\d{0," + 2 + "})?", "g")
  const a = number.toString().match(reg)[0];
  const dot = a.indexOf(".");
  if (dot === -1) { // integer, insert decimal dot and pad up zeros
      return a + "." + "0".repeat(2);
  }
  const b = 2 - (a.length - dot) + 1;
  return b > 0 ? (a + "0".repeat(b)) : a;
}
}
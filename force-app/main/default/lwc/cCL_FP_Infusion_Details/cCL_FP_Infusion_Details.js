import { LightningElement, track, api, wire } from "lwc";
import backToInfusionList from "@salesforce/label/c.CCL_Back_to_Infusion_List";
import finishedProductInfusionHeader from '@salesforce/label/c.CCL_Finished_Product_Infusion_Header';
import doseInformation from "@salesforce/label/c.CCL_Dose_Information";
import finishedProductBatchNumber from "@salesforce/label/c.CCL_Finished_Product_Batch_Number";
import finishedProductBatchNumberTooltip from "@salesforce/label/c.CCL_FinishedProductBatchNumber_Tooltip";
import productAcceptanceStatus from "@salesforce/label/c.CCL_Product_Acceptance_Status";
import infusionReportingStatus from "@salesforce/label/c.CCL_Infusion_Reporting_Status";
import productInfusionCompleted from "@salesforce/label/c.CCL_Product_Infusion_Completed";
import productInfusionPending from "@salesforce/label/c.CCL_Product_Infusion_Pending";
import productInfusionInterrupted from "@salesforce/label/c.CCL_Product_Infusion_Interrupted";
import productInfusionCancelled from "@salesforce/label/c.CCL_Product_Infusion_Cancelled";
import numberOfBagsShippedLabel from "@salesforce/label/c.CCL_Number_of_Bags_Shipped";
import expDateOfShippedBagsLabel from "@salesforce/label/c.CCL_Expiration_Date_of_Shipped_Bags";
import reportInfusion from "@salesforce/label/c.CCL_Report_Infusion";
import numberOfBagsInfusedLabel from "@salesforce/label/c.CCL_Number_of_Bags_Infused";
import actualDateOfInfusionLabel from "@salesforce/label/c.CCL_Actual_Date_of_Infusion";
import infusionProcedureStatusLabel from "@salesforce/label/c.CCL_Infusion_Procedure_Status";
import cancelButtonLabel from "@salesforce/label/c.CCL_Cancel_Button";
import saveButtonLabel from "@salesforce/label/c.CCL_Save_Button";
import showToastMessageOnHoldLabel from "@salesforce/label/c.CCL_show_Toast_Message_On_Hold";
import showToastMessageOnHoldNoteLabel from "@salesforce/label/c.CCL_show_Toast_Message_On_Hold_Note";
import outcomeBasedAgreementTitle from "@salesforce/label/c.CCL_Outcome_Based_Agreement";
import obaDynamicMessageUS from "@salesforce/label/c.CCL_Dynamic_OBA_Message_US";
import obaDynamicMessageNonUS from "@salesforce/label/c.CCL_Dynamic_OBA_Message_Non_US";
import milestoneTitle from "@salesforce/label/c.CCL_Milestone";
import outcomeReportingDeadlineTitle from "@salesforce/label/c.CCL_Outcome_Reporting_Deadline";
import outcomeTitle from "@salesforce/label/c.CCL_Outcome";
import reportedByTitle from "@salesforce/label/c.CCL_Reported_By";
import saveOutcomeButton from "@salesforce/label/c.CCL_Save_Outcome_Button";
import submitLabel from "@salesforce/label/c.CCL_Submit";
import preStatusLabel from "@salesforce/label/c.CCL_PreStatus_Message";
import remissionLabel from "@salesforce/label/c.CCL_Remission";
import nonRemissionLabel from "@salesforce/label/c.CCL_NonRemission";
import nonRemissionTextLabel from "@salesforce/label/c.CCL_NonRemission_Text";
import confirmOutcome from "@salesforce/label/c.CCL_ConfirmOutcome";
import noConfirmOutcome from "@salesforce/label/c.CCL_NoOutcome";
import pleaseSelectLabel from "@salesforce/label/c.CCL_PleaseSelect";
import naLabel from "@salesforce/label/c.CCL_NA";
import saveInfusion from "@salesforce/label/c.CCL_SaveInfusion";
import dateEnteredByLabel from "@salesforce/label/c.CCL_DateEnteredBy";
import saveInfusionTextLabel from "@salesforce/label/c.CCL_SaveInfusionText";
import undoneMessageLabel from "@salesforce/label/c.CCL_UndoneMessage";
import infusionCompleteMessageLabel from "@salesforce/label/c.CCL_InfusionCompleteButton";
import fpInfusionSavedLabel from "@salesforce/label/c.CCL_FP_Infusion_Saved";
import actualInfusionNotFuture from "@salesforce/label/c.CCL_Actual_Infusion_Not_Future";
import infusedBagsCannotGrtrShippedBags from "@salesforce/label/c.CCL_Infused_Bags_Cannot_Greater_Bags_Shipped";
import error from "@salesforce/label/c.CCL_Error";
import fpDoseOutOfSpecification from "@salesforce/label/c.CCL_FP_Dose_Out_of_Specification";
import close from '@salesforce/label/c.CCL_Modal_Close';

import MILESTONE_OUTCOME_1 from '@salesforce/schema/CCL_Finished_Product__c.CCL_OBA_Outcome_1__c';
import MILESTONE_OUTCOME_2 from '@salesforce/schema/CCL_Finished_Product__c.CCL_OBA_Outcome_2__c';
import MILESTONE_OUTCOME_3 from '@salesforce/schema/CCL_Finished_Product__c.CCL_OBA_Outcome_3__c';
import MILESTONE_OUTCOME_4 from '@salesforce/schema/CCL_Finished_Product__c.CCL_OBA_Outcome_4__c';
import MILESTONE_OUTCOME_5 from '@salesforce/schema/CCL_Finished_Product__c.CCL_OBA_Outcome_5__c';
import {getRecord} from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id';
import { updateRecord } from "lightning/uiRecordApi";
import NAME_FIELD from '@salesforce/schema/User.Name';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import viewOrder from "@salesforce/label/c.CCL_View_Order_Summary";
import relatedDocumentsLabel from "@salesforce/label/c.CCL_Related_Documents";
import { NavigationMixin } from "lightning/navigation";
import FINISHEDPRODUCT_OBJECT from '@salesforce/schema/CCL_Finished_Product__c';
import INFUSION_PROCEDURE_STATUS from '@salesforce/schema/CCL_Finished_Product__c.CCL_Infusion_Procedure_status__c';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getFieldDetails from "@salesforce/apex/CCL_FinishedProduct_Controller.getFinishedProductData";
import utilityMethodToConvertDate from "@salesforce/apex/CCL_FinishedProduct_Controller.getFormattedDate";
import getCheckUserPermission from "@salesforce/apex/CCL_FinishedProduct_Controller.checkUserPermission";
import getCheckUserPermissionPrimaryInfusion from "@salesforce/apex/CCL_FinishedProduct_Controller.checkUserPermissionPrimaryInfusion";
import CCL_Mandatory_Field from "@salesforce/label/c.CCL_Mandatory_Field";
import unknownError from '@salesforce/label/c.CCL_Unknown_error';
import getFileDetails from "@salesforce/apex/CCL_FinishedProduct_Controller.getUploadedFileDetails";
import utilityMethodToConvertLastModifiedDate from "@salesforce/apex/CCL_FPShipmentController.getFormattedDateTime";
import getsUserNameMethod from "@salesforce/apex/CCL_FinishedProduct_Controller.getUserName";
import getsUserTimeZoneMethod from "@salesforce/apex/CCL_FPShipmentController.getsUserTimeZone";
import { refreshApex } from '@salesforce/apex';


const infusionCompleted = 'Completed';
const infusionCancelled = 'Cancelled';
const infusionPending = 'Pending';
const infusionInterrupted = 'Interrupted';
const productAccValue = 'Accepted';
const productRejValue = 'Rejected';
const unitedStatesValue = 'US';
const remission = '01';
const nonRemission = '02';


export default class cCL_FP_Infusion_Details extends NavigationMixin(LightningElement) {

    @api recordId;
    @api infusionrecorddetail;
    @track objUser = {};
    @track error;
    @track err;
    @track result;
    @track resultUser;
    @track showErrorMsg = false;
    @track selectedTime;
    @track loggedInUserName;
    @track loggedInUserNameButton;
    @track loggedInUserNameButton2;
    @track loggedInUserNameButton3;
    @track loggedInUserNameButton4;
    @track loggedInUserNameButton5;
    @track recordInput;
    @track returnVal;
    @track dateValue;
    @track selectedDate;
    @track actualDateValue;
    @track enabletoEditFields = false;
    @track captureUserAccess;
    @track captureUserAccessPrimaryinfusion;
    @track isLoaded = true;
    isPermLoaded;
    @track fields = {};
    @track showFinalScreenOba = false;
    @track showFinalScreen = false;
    @track showMainContent = true;
    @track showErrorOnScreen = false
    @track numberOfBagsInfused;
    @track EMPTY_STRING = "";
    @track infusionProcedureStatusValue;
    @track infusionCompletedFlag;
    @track milestoneOutcome1;
    @track milestoneOutcome2;
    @track milestoneOutcome3;
    @track milestoneOutcome4;
    @track milestoneOutcome5;
    @track milestoneOutcome1Flag;
    @track milestoneOutcome2Flag;
    @track milestoneOutcome3Flag;
    @track milestoneOutcome4Flag;
    @track milestoneOutcome5Flag;
    @track reprtedByOba;
    @track date1Difference;
    @track lastModifiedDateOba;
    @track actualDateOfInfusion;
    @track infusionReportingStatus;
    @track nowDate;
    @track infusionReportingStatusPrevious;
    @track filesUploaded = [];
    @track showUploadedFiles = false;
    @track disableRelatedDocumentsIR = false;
    @track captureUserAccessIR;
    @track lockRecord;
    @track milestone1Flag = false;
    @track milestone2Flag = false;
    @track milestone3Flag = false;
    @track milestone4Flag = false;
    @track milestone5Flag = false;
    @track milestone1FlagNoOutcome = false;
    @track milestone2FlagNoOutcome = false;
    @track milestone3FlagNoOutcome = false;
    @track milestone4FlagNoOutcome = false;
    @track milestone5FlagNoOutcome = false;
    @track userName;
    @track actualInfusionDateText;
    @track timeZoneValue;


    @track savedFields = false;
    @track disabletoEditFields = false;
    //@track lastModifiedTime;
    @track reloadSucessfull = false;
    @track dataEntered;
    @track completedInfusion = false;
    @track lastModifiedDateTimeInNovartisFormat;
    @track showToastMessageOnHold = false;
    @track showUSDynamicMessage = false;
    @track signatureFlag = false;
    @track shipmentStatusAcceptedRejectedFlag = false;
    @track lastModifiedByName;
    @track milestoneSCPDate1;
    @track milestoneSCPDate2;
    @track milestoneSCPDate3;
    @track milestoneSCPDate4;
    @track milestoneSCPDate5;
    @track isObaStatusSectionDisplayed;
    @track isReportInfusionSectionDisplayed;
    @track infusionSignatureName;
    @track infusionSignature;
    successMsg = fpInfusionSavedLabel;
    errorMsg = '';
    showSuccess = false;
    showError = false;
    wiredShipmentResult;

    @track label = {
        finishedProductInfusionHeader,
        backToInfusionList,
        viewOrder,
        relatedDocumentsLabel,
        doseInformation,
        finishedProductBatchNumber,
        productAcceptanceStatus,
        infusionReportingStatus,
        numberOfBagsShippedLabel,
        expDateOfShippedBagsLabel,
        reportInfusion,
        numberOfBagsInfusedLabel,
        actualDateOfInfusionLabel,
        infusionProcedureStatusLabel,
        cancelButtonLabel,
        saveButtonLabel,
        finishedProductBatchNumberTooltip,
        productInfusionCompleted,
        productInfusionPending,
        productInfusionInterrupted,
        productInfusionCancelled,
        showToastMessageOnHoldLabel,
        showToastMessageOnHoldNoteLabel,
        outcomeBasedAgreementTitle,
        obaDynamicMessageUS,
        obaDynamicMessageNonUS,
        milestoneTitle,
        outcomeReportingDeadlineTitle,
        outcomeTitle,
        reportedByTitle,
        saveOutcomeButton,
        submitLabel,
        preStatusLabel,
        remissionLabel,
        nonRemissionLabel,
        nonRemissionTextLabel,
        confirmOutcome,
        noConfirmOutcome,
        pleaseSelectLabel,
        naLabel,
        saveInfusion,
        dateEnteredByLabel,
        saveInfusionTextLabel,
        undoneMessageLabel,
        infusionCompleteMessageLabel,
        actualInfusionNotFuture,
        infusedBagsCannotGrtrShippedBags,
        error,
        fpDoseOutOfSpecification,
        close


    };


  @wire(getRecord, {
    recordId: USER_ID,
    fields: [NAME_FIELD]
}) wireuser({
    error,
    data
}) {
    if (error) {
       this.errorValue = error ;
    } else if (data) {
        this.loggedInUserName = data.fields.Name.value;
    }
}
  @wire(getFieldDetails, { recordId: '$recordId'})
  wiredShipmentDetails(res){
      this.wiredShipmentResult = res;
      let {data, error} = res;
      if(data){
          this.result= data;

          this.infusionSignature = this.result.CCL_Infusion_Details__c;
          this.actualInfusionDateText = this.result.CCL_Actual_Infusion_Date_Time_Text__c;

          getsUserTimeZoneMethod({
        })
          .then(result=>{
            this.timeZoneValue = (result).substr(4,6);
        })
        .catch(error=>{
            this.showErrorOnScreen=true;
            this.err = JSON.stringify(error);
        })

          getsUserNameMethod({
            userId: USER_ID
          })
            .then(result=>{
              this.infusionSignatureName = (result);

          })
          .catch(error=>{
              this.showErrorOnScreen=true;
              this.err = JSON.stringify(error);
          })


          getsUserNameMethod({
            userId: this.result.CCL_OBA_Outcome_1_Reported_By__c
          })
            .then(result=>{

              this.loggedInUserNameButton = (result);

          })
          .catch(error=>{

              this.showErrorOnScreen=true;
              this.err = JSON.stringify(error);
          })

            getsUserNameMethod({
                    userId: this.result.CCL_OBA_Outcome_2_Reported_By__c
                })
                .then(result => {

                    this.loggedInUserNameButton2 = (result);

                })
                .catch(error => {

                    this.showErrorOnScreen = true;
                    this.err = JSON.stringify(error);
                })


            getsUserNameMethod({
                    userId: this.result.CCL_OBA_Outcome_3_Reported_By__c
                })
                .then(result => {

                    this.loggedInUserNameButton3 = (result);

                })
                .catch(error => {

                    this.showErrorOnScreen = true;
                    this.err = JSON.stringify(error);
                })


            getsUserNameMethod({
                    userId: this.result.CCL_OBA_Outcome_4_Reported_By__c
                })
                .then(result => {

                    this.loggedInUserNameButton4 = (result);

                })
                .catch(error => {

                    this.showErrorOnScreen = true;
                    this.err = JSON.stringify(error);
                })


            getsUserNameMethod({
                    userId: this.result.CCL_OBA_Outcome_5_Reported_By__c
                })
                .then(result => {

                    this.loggedInUserNameButton5 = (result);

                })
                .catch(error => {

                    this.showErrorOnScreen = true;
                    this.err = JSON.stringify(error);
                })

            this.isReportInfusionSectionDisplayed = this.result.CCL_Shipment__r.CCL_Infusion_Data_Collection_Allowed__c || this.result.CCL_Shipment__r.CCL_Infusion_Date_Required__c;
            this.isObaStatusSectionDisplayed = this.result.CCL_Shipment__r.CCL_OBA__c;

            this.outcome1 = this.result.CCL_OBA_Outcome_1__c;
            this.outcome2 = this.result.CCL_OBA_Outcome_2__c;
            this.outcome3 = this.result.CCL_OBA_Outcome_3__c;
            this.outcome4 = this.result.CCL_OBA_Outcome_4__c;
            this.outcome5 = this.result.CCL_OBA_Outcome_5__c;

            // Lock all Rows if one row is in Non Remission
            if (this.outcome1 == nonRemission ||
                this.outcome2 == nonRemission ||
                this.outcome3 == nonRemission ||
                this.outcome4 == nonRemission ||
                this.outcome5 == nonRemission) {

                this.milestone2Flag = true;
                this.milestone3Flag = true;
                this.milestone4Flag = true;
                this.milestone5Flag = true;

            }


            var newDate = new Date();

            this.nowDate = newDate.toISOString().substr(0, 10).split('-');
            var mydate = new Date(this.nowDate);

      this.milestoneSCPDate1 = this.result.CCL_OBA_Milestone_1_Date__c;
      if(this.milestoneSCPDate1 != (null || undefined)){
      var milestoneSCPDate1parts =  this.milestoneSCPDate1.split('-');
      var mydate2 = new Date(milestoneSCPDate1parts[0], milestoneSCPDate1parts[1] - 1, milestoneSCPDate1parts[2]);
      this.date1Difference = mydate2 - mydate;
      }

            this.milestoneSCPDate2 = this.result.CCL_OBA_Milestone_2_Date__c;
            if (this.milestoneSCPDate2 != (null || undefined)) {
                var milestoneSCPDate2parts = this.milestoneSCPDate2.split('-');
                var mydate3 = new Date(milestoneSCPDate2parts[0], milestoneSCPDate2parts[1] - 1, milestoneSCPDate2parts[2]);
                this.date1Difference2 = mydate2 - mydate;
            }

            this.milestoneSCPDate3 = this.result.CCL_OBA_Milestone_3_Date__c;
            if (this.milestoneSCPDate3 != (null || undefined)) {
                var milestoneSCPDate3parts = this.milestoneSCPDate3.split('-');
                var mydate4 = new Date(milestoneSCPDate3parts[0], milestoneSCPDate3parts[1] - 1, milestoneSCPDate3parts[2]);
                this.date1Difference3 = mydate3 - mydate;
            }


            this.milestoneSCPDate4 = this.result.CCL_OBA_Milestone_4_Date__c;
            if (this.milestoneSCPDate4 != (null || undefined)) {
                var milestoneSCPDate4parts = this.milestoneSCPDate4.split('-');
                var mydate5 = new Date(milestoneSCPDate4parts[0], milestoneSCPDate4parts[1] - 1, milestoneSCPDate4parts[2]);
                this.date1Difference4 = mydate4 - mydate;
            }


            this.milestoneSCPDate5 = this.result.CCL_OBA_Milestone_5_Date__c;
            if (this.milestoneSCPDate5 != (null || undefined)) {
                var milestoneSCPDate5parts = this.milestoneSCPDate5.split('-');
                var mydate6 = new Date(milestoneSCPDate5parts[0], milestoneSCPDate5parts[1] - 1, milestoneSCPDate5parts[2]);
                this.date1Difference5 = mydate5 - mydate;
            }



            if (this.outcome1 != (null || undefined)) {
                this.milestone1Flag = true;

            } else if (this.date1Difference < 0 && (this.outcome1 != (null || undefined))) {
                this.milestone1Flag = true;
                this.milestone1FlagNoOutcome = false;

            } else if (this.date1Difference < 0 && (this.outcome1 == (null || undefined))) {
                this.milestone1Flag = true;
                this.milestone1FlagNoOutcome = true;

            }

            if (this.outcome2 != (null || undefined)) {
                this.milestone2Flag = true;

            } else if ((this.date1Difference2 > 0) || (this.date1Difference2 == 0)) {
                this.milestone2Flag = true;

            } else if ((this.outcome2 != (null || undefined)) && ((mydate3 - mydate) < 0)) {
                this.milestone2Flag = true;
                this.milestone2FlagNoOutcome = false;

            } else if ((this.outcome2 == (null || undefined)) && ((mydate3 - mydate) < 0) && (this.outcome1 != nonRemission)) {
                this.milestone2Flag = true;
                this.milestone2FlagNoOutcome = true;

            } else if ((this.outcome2 == (null || undefined)) && ((mydate3 - mydate) < 0) && (this.outcome1 != remission)) {
                this.milestone2Flag = true;
                this.milestone2FlagNoOutcome = false;

            }


            if (this.outcome3 != (null || undefined)) {
                this.milestone3Flag = true;

            } else if ((this.date1Difference3 > 0) || (this.date1Difference3 == 0)) {
                this.milestone3Flag = true;

            } else if ((this.outcome3 != (null || undefined)) && ((mydate4 - mydate) < 0)) {
                this.milestone3Flag = true;
                this.milestone3FlagNoOutcome = false;

            } else if ((this.outcome3 == (null || undefined)) && ((mydate4 - mydate) < 0) && ((this.outcome1 || this.outcome2) != nonRemission)) {
                this.milestone3Flag = true;
                this.milestone3FlagNoOutcome = true;

            } else if ((this.outcome3 == (null || undefined)) && ((mydate4 - mydate) < 0) && ((this.outcome1 || this.outcome2) != remission)) {
                this.milestone3Flag = true;
                this.milestone3FlagNoOutcome = false;

            }


            if (this.outcome4 != (null || undefined)) {
                this.milestone4Flag = true;

            } else if ((this.date1Difference4 > 0) || (this.date1Difference4 == 0)) {
                this.milestone4Flag = true;

            } else if ((this.outcome4 != (null || undefined)) && ((mydate5 - mydate) < 0)) {
                this.milestone4Flag = true;
                this.milestone4FlagNoOutcome = false;

            } else if ((this.outcome4 == (null || undefined)) && ((mydate5 - mydate) < 0) && ((this.outcome1 || this.outcome2 || this.outcome3) != nonRemission)) {
                this.milestone4Flag = true;
                this.milestone4FlagNoOutcome = true;

            } else if ((this.outcome4 == (null || undefined)) && ((mydate5 - mydate) < 0) && ((this.outcome1 || this.outcome2 || this.outcome3) != remission)) {
                this.milestone4Flag = true;
                this.milestone4FlagNoOutcome = false;

            }


            if (this.outcome5 != (null || undefined)) {
                this.milestone5Flag = true;

            } else if ((this.date1Difference5 > 0) || (this.date1Difference5 == 0)) {
                this.milestone5Flag = true;

            } else if ((this.outcome5 != (null || undefined)) && ((mydate6 - mydate) < 0)) {
                this.milestone5Flag = true;
                this.milestone5FlagNoOutcome = false;

            } else if ((this.outcome5 == (null || undefined)) && ((mydate6 - mydate) < 0) && ((this.outcome1 || this.outcome2 || this.outcome3 || this.outcome4) != nonRemission)) {
                this.milestone5Flag = true;
                this.milestone5FlagNoOutcome = true;

            } else if ((this.outcome5 == (null || undefined)) && ((mydate6 - mydate) < 0) && ((this.outcome1 || this.outcome2 || this.outcome3 || this.outcome4) != remission)) {
                this.milestone5Flag = true;
                this.milestone5FlagNoOutcome = false;

            }

            this.milestoneOutcome1 = this.result.CCL_OBA_Outcome_1__c;
            this.milestoneOutcome2 = this.result.CCL_OBA_Outcome_2__c;
            this.milestoneOutcome3 = this.result.CCL_OBA_Outcome_3__c;
            this.milestoneOutcome4 = this.result.CCL_OBA_Outcome_4__c;
            this.milestoneOutcome5 = this.result.CCL_OBA_Outcome_5__c;

          this.infusionProcedureStatusValue = this.result.CCL_Infusion_Procedure_status__c;
          if(this.result.CCL_Actual_Infusion_Date_Time__c != (null || undefined)){
            var dt =   new Date(this.result.CCL_Actual_Infusion_Date_Time__c);
            this.actualDateOfInfusion = dt.toISOString().slice(0,10);
            this.actualDateValue= this.actualDateOfInfusion;

          }

          this.numberOfBagsInfused = this.result.CCL_Number_of_bags_Infused__c;
          this.dataEntered = this.result.CCL_Infusion_Details__c;
          this.lastModifiedByName=this.result.LastModifiedBy.Name;
          if(this.result.CCL_Infusion_Procedure_status__c === infusionCompleted){
            this.infusionReportingStatus = productInfusionCompleted;
            this.infusionReportingStatusPrevious = productInfusionCompleted;
            this.completedInfusion = true;
          }
          if(this.result.CCL_Infusion_Procedure_status__c === infusionPending){
            this.infusionReportingStatus = productInfusionPending;
            this.infusionReportingStatusPrevious = productInfusionPending;
          }
          if(this.result.CCL_Infusion_Procedure_status__c === infusionInterrupted){
            this.infusionReportingStatus = productInfusionInterrupted;
            this.infusionReportingStatusPrevious = productInfusionInterrupted;
          }
          if(this.result.CCL_Infusion_Procedure_status__c === infusionCancelled){
            this.infusionReportingStatus = productInfusionCancelled;
            this.infusionReportingStatusPrevious = productInfusionCancelled;
          }
          if(this.result && this.result.CCL_Batch_Status__c == 'Conditionally Approved'){
               this.showToastMessageOnHold=true;
            }
            if (this.result.CCL_Shipment__r.CCL_Infusion_Center__r.ShippingCountryCode === unitedStatesValue) {
                this.showUSDynamicMessage = true;

            }

        } else if (error) {
            this.error = unknownError;
            if (Array.isArray(error.body)) {
                this.error = error.body.map(e => e.message).join(', ');
                this.errorMsg = this.error;
                this.showError = true;
            } else if (typeof error.body.message === 'string') {
                this.error = error.body.message;
                this.errorMsg = this.error;
                this.showError = true;
            }
            this.result = undefined;
        }
    }
    get productAcceptancestatus() {
        if (this.shipProductAcceptancestatus === productAccValue) {
            this.CCL_Product_Accepatnce_Status__c = 'Product Delivery Accepted';
            this.shipmentStatusAcceptedRejectedFlag = true;

        }
        if (this.shipProductAcceptancestatus === productRejValue) {
            this.CCL_Product_Accepatnce_Status__c = 'Product Delivery Rejected';
            this.shipmentStatusAcceptedRejectedFlag = true;
        }
        return this.result ? this.CCL_Product_Accepatnce_Status__c : '';
    }
    get productBatchNumber() {
        return this.result ? this.result.Name : '';
    }
    get numberOfBagsShipped() {
        return this.result ? this.result.CCL_Shipment__r.CCL_Number_of_Bags_Shipped__c : '';
    }


    get loggedInuserNameVlaueBy() {
        return this.result ? this.result.CCL_OBA_Outcome_1_Reported_By__c : '';

    }

    get reportedbyMilestone1() {
        return this.result ? this.result.CCL_OBA_Outcome_1_Reported_On__c : '';
    }
    get reportedbyMilestone2() {
        return this.result ? this.result.CCL_OBA_Outcome_2_Reported_On__c : '';
    }
    get reportedbyMilestone3() {
        return this.result ? this.result.CCL_OBA_Outcome_3_Reported_On__c : '';
    }
    get reportedbyMilestone4() {
        return this.result ? this.result.CCL_OBA_Outcome_4_Reported_On__c : '';
    }
    get reportedbyMilestone5() {
        return this.result ? this.result.CCL_OBA_Outcome_5_Reported_On__c : '';
    }
    get shipProductAcceptancestatus() {
        return this.result ? this.result.CCL_Shipment__r.CCL_Product_Acceptance_Status__c : '';
    }
    get lastModifiedDate() {
        return this.result ? this.result.LastModifiedDate : '';
    }
    get infusionDetails() {
        return this.result ? this.result.CCL_Infusion_Details__c : '';
    }
    get infusionShippingCountry() {
        return this.result ? this.result.CCL_Shipment__r.CCL_Infusion_Center__r.ShippingCountryCode : '';
    }

    get obaMilestoneDate1() {
        return this.result ? this.result.CCL_OBA_Milestone_1__c : '';
    }

    get obaMilestoneDate2() {
        return this.result ? this.result.CCL_OBA_Milestone_2__c : '';
    }

    get obaMilestoneDate3() {
        return this.result ? this.result.CCL_OBA_Milestone_3__c : '';
    }

    get obaMilestoneDate4() {
        return this.result ? this.result.CCL_OBA_Milestone_4__c : '';
    }

    get obaMilestoneDate5() {
        return this.result ? this.result.CCL_OBA_Milestone_5__c : '';
    }

    get expDateOfShippedBags() {
        return this.result ? this.result.CCL_Shipment__r.CCL_Expiry_Date_of_Shipped_Bags_Text__c : '';
    }
    get infusionTimeZone() {
        return this.result ? this.result.CCL_Infusion_to_Time_Zone__c : '';
    }
    get lastModifiedTime() {
        let t = this.result.LastModifiedDate;
        t = t.substr(11, 15);
        let t1 = t.substr(0, 5);
        return t1;

    }
    getNumberOfBagsInfused(event) {
        this.numberOfBagsInfused = event.target.value;
        this.result ? this.result.CCL_Number_of_bags_Infused__c : '';
    }

    onClick() {
        return refreshApex(this.wiredShipmentResult);
    }

    getInfusionProcedureStatus(event) {
        this.infusionProcedureStatusValue = event.target.value;

        if (this.infusionProcedureStatusValue == infusionCompleted) {
            this.infusionCompletedFlag = true;

        } else {
            this.infusionCompletedFlag = false;
        }

        this.result ? this.result.CCL_Infusion_Procedure_status__c : '';
    }
    @wire(getObjectInfo, { objectApiName: FINISHEDPRODUCT_OBJECT })
    fpInfo;
    @wire(getPicklistValues, {
        recordTypeId: '$fpInfo.data.defaultRecordTypeId', //pass id dynamically
        fieldApiName: INFUSION_PROCEDURE_STATUS
    })
    infusionProcedureStatus;


    getMilestoneOutcome1(event) {
        this.milestoneOutcome1 = event.target.value;

        if (this.milestoneOutcome1 == remission) {
            this.milestoneOutcome1Flag = true;

        } else {
            this.milestoneOutcome1Flag = false;

        }

        this.result ? this.result.CCL_OBA_Outcome_1__c : '';
    }

    @wire(getPicklistValues, {
        recordTypeId: '$fpInfo.data.defaultRecordTypeId',
        fieldApiName: MILESTONE_OUTCOME_1
    })
    milestoneOutcomeOne;


    getMilestoneOutcome2(event) {
        this.milestoneOutcome2 = event.target.value;

        if (this.milestoneOutcome2 == remission) {
            this.milestoneOutcome2Flag = true;

        } else {
            this.milestoneOutcome2Flag = false;

        }

        this.result ? this.result.CCL_OBA_Outcome_2__c : '';
    }
    @wire(getObjectInfo, { objectApiName: FINISHEDPRODUCT_OBJECT })
    fpInfoOutcome2;
   @wire(getPicklistValues,
      {
          recordTypeId: '$fpInfo.data.defaultRecordTypeId',
          fieldApiName: MILESTONE_OUTCOME_2
      }
   )
   milestoneOutcomeTwo;

    getMilestoneOutcome3(event) {
        this.milestoneOutcome3 = event.target.value;

        if (this.milestoneOutcome3 == remission) {
            this.milestoneOutcome3Flag = true;


        } else {
            this.milestoneOutcome3Flag = false;


        }

        this.result ? this.result.CCL_OBA_Outcome_3__c : '';
    }
    @wire(getObjectInfo, { objectApiName: FINISHEDPRODUCT_OBJECT })
    fpInfoOutcome3;
   @wire(getPicklistValues,
      {
          recordTypeId: '$fpInfo.data.defaultRecordTypeId',
          fieldApiName: MILESTONE_OUTCOME_3
      }
   )
   milestoneOutcomeThree;

    getMilestoneOutcome4(event) {
        this.milestoneOutcome4 = event.target.value;

        if (this.milestoneOutcome4 == remission) {
            this.milestoneOutcome4Flag = true;


        } else {
            this.milestoneOutcome4Flag = false;


        }


        this.result ? this.result.CCL_OBA_Outcome_4__c : '';
    }
    @wire(getObjectInfo, { objectApiName: FINISHEDPRODUCT_OBJECT })
    fpInfoOutcome4;
   @wire(getPicklistValues,
      {
          recordTypeId: '$fpInfo.data.defaultRecordTypeId',
          fieldApiName: MILESTONE_OUTCOME_4
      }
   )
   milestoneOutcomeFour;

    getMilestoneOutcome5(event) {
        this.milestoneOutcome5 = event.target.value;

        if (this.milestoneOutcome5 == remission) {
            this.milestoneOutcome5Flag = true;


        } else {
            this.milestoneOutcome5Flag = false;


        }


        this.result ? this.result.CCL_OBA_Outcome_5__c : '';
    }
    @wire(getObjectInfo, { objectApiName: FINISHEDPRODUCT_OBJECT })
    fpInfoOutcome5;
   @wire(getPicklistValues,
      {
          recordTypeId: '$fpInfo.data.defaultRecordTypeId',
          fieldApiName: MILESTONE_OUTCOME_5
      }
   )
   milestoneOutcomeFive;


  getActualDateOfInfusion(event){
      this.actualDateValue=event.target.value;

      var newDate = new Date();
      this.dateValue = newDate.toISOString();
          utilityMethodToConvertDate({
              inputDate: event.target.value
          })
          .then(result=>{
              this.selectedDate= result;

            })
            .catch(error => {
                this.showErrorOnScreen = true;
                this.err = JSON.stringify(error);
            })
    }

    validateFormAndProceedToOBACompletion1() {

        this.fieldValidations = this.template.querySelectorAll("lightning-combobox");

            if(this.isReportInfusionSectionDisplayed == false){
                this.fieldNullCheck(this.fieldValidations[0], CCL_Mandatory_Field);
            } else if((this.result.CCL_Infusion_Procedure_status__c === infusionCompleted) && (this.isReportInfusionSectionDisplayed == true) ){
              this.fieldNullCheck(this.fieldValidations[0], CCL_Mandatory_Field);
            } else {
              this.fieldNullCheck(this.fieldValidations[1], CCL_Mandatory_Field);
            }

             if (this.isvalid) {

            this.navigateFurtherOBA = true;
        } else {
            this.navigateFurtherOBA = false;
        }
        this.obaProcess();
    }

    validateFormAndProceedToOBACompletion2() {

        this.fieldValidations = this.template.querySelectorAll("lightning-combobox");

            if(this.isReportInfusionSectionDisplayed == false){
                this.fieldNullCheck(this.fieldValidations[1], CCL_Mandatory_Field);
            } else if((this.result.CCL_Infusion_Procedure_status__c === infusionCompleted) && (this.isReportInfusionSectionDisplayed == true) ){
              this.fieldNullCheck(this.fieldValidations[1], CCL_Mandatory_Field);
            } else {
              this.fieldNullCheck(this.fieldValidations[2], CCL_Mandatory_Field);

            }

             if (this.isvalid) {

            this.navigateFurtherOBA = true;
        } else {
            this.navigateFurtherOBA = false;
        }
        this.obaProcess();
    }

    validateFormAndProceedToOBACompletion3() {

        this.fieldValidations = this.template.querySelectorAll("lightning-combobox");

            if(this.isReportInfusionSectionDisplayed == false){

                this.fieldNullCheck(this.fieldValidations[2], CCL_Mandatory_Field);
            } else if((this.result.CCL_Infusion_Procedure_status__c === infusionCompleted) && (this.isReportInfusionSectionDisplayed == true) ){
              this.fieldNullCheck(this.fieldValidations[2], CCL_Mandatory_Field);
            }
            else {
              this.fieldNullCheck(this.fieldValidations[3], CCL_Mandatory_Field);

            }

             if (this.isvalid) {

            this.navigateFurtherOBA = true;
        } else {
            this.navigateFurtherOBA = false;
        }
        this.obaProcess();
    }

    validateFormAndProceedToOBACompletion4() {

        this.fieldValidations = this.template.querySelectorAll("lightning-combobox");


            if(this.isReportInfusionSectionDisplayed == false){

                this.fieldNullCheck(this.fieldValidations[3], CCL_Mandatory_Field);
            } else if((this.result.CCL_Infusion_Procedure_status__c === infusionCompleted) && (this.isReportInfusionSectionDisplayed == true) ){
              this.fieldNullCheck(this.fieldValidations[3], CCL_Mandatory_Field);
            }
            else {
              this.fieldNullCheck(this.fieldValidations[4], CCL_Mandatory_Field);

            }

             if (this.isvalid) {

            this.navigateFurtherOBA = true;
        } else {
            this.navigateFurtherOBA = false;
        }
        this.obaProcess();
    }

    validateFormAndProceedToOBACompletion5() {

        this.fieldValidations = this.template.querySelectorAll("lightning-combobox");

            if(this.isReportInfusionSectionDisplayed == false){

                this.fieldNullCheck(this.fieldValidations[4], CCL_Mandatory_Field);
            } else if((this.result.CCL_Infusion_Procedure_status__c === infusionCompleted) && (this.isReportInfusionSectionDisplayed == true) ){
              this.fieldNullCheck(this.fieldValidations[4], CCL_Mandatory_Field);
            }
            else {

              this.fieldNullCheck(this.fieldValidations[5], CCL_Mandatory_Field);

            }

             if (this.isvalid) {

            this.navigateFurtherOBA = true;
        } else {
            this.navigateFurtherOBA = false;
        }
        this.obaProcess();
    }

    obaProcess() {

        if (this.isvalid && this.navigateFurtherOBA) {
            this.convertLastModifiedDateToNovartisFormatOba();
            this.showFinalScreenOba = true;
        }
    }



    infusionRecProcess() {
        this.lastModTime = this.lastModifiedDate;
        this.isLoaded = false;
        this.fields["Id"] = this.recordId;
        this.fields["CCL_Infusion_Procedure_status__c"] = this.infusionProcedureStatusValue;
        this.convertLastModifiedDateToNovartisFormat();
        if (this.infusionProcedureStatusValue === infusionCompleted) {
            this.infusionReportingStatus = productInfusionCompleted;
        }
        if (this.infusionProcedureStatusValue === infusionPending) {
            this.infusionReportingStatus = productInfusionPending;
        }
        if (this.infusionProcedureStatusValue === infusionInterrupted) {
            this.infusionReportingStatus = productInfusionInterrupted;
        }
        if (this.infusionProcedureStatusValue === infusionCancelled) {
            this.infusionReportingStatus = productInfusionCancelled;
        }
        this.fields["CCL_Infusion_Reporting_status__c"] = this.infusionReportingStatus;
        this.fields["CCL_Number_of_bags_Infused__c"] = this.numberOfBagsInfused;
        this.fields["CCL_Actual_Infusion_Date_Time_Text__c"] = this.selectedDate;
        this.fields["CCL_Infusion_Details__c"] = this.infusionSignatureName + ' ' + this.lastModifiedDateTimeInNovartisFormat + ' ' + this.infusionTimeZone;
        this.infusionProcedureStatusValue = this.infusionProcedureStatusValue;
        this.actualDateOfInfusion = this.selectedDate;
        this.numberOfBagsInfused = this.numberOfBagsInfused;
        this.recordInput = { fields: this.fields };
        if (this.infusionProcedureStatusValue === infusionCompleted) {
            this.showFinalScreen = true;
            this.showMainContent = false;
        } else {
            this.showFinalScreen = false;
            updateRecord(this.recordInput)
                .then(result => {
                    this.showErrorOnScreen = false;
                    this.returnVal = result;
                    this.isLoaded = true;
                    this.showSuccessToast();
                    this.signatureFlag = true;
                    this.reloadSucessfull = true;
                    this.renderedCallback();
                    this.dataEntered = this.lastModifiedByName + ' ' + this.lastModifiedDateTimeInNovartisFormat + ' ' + this.infusionTimeZone;
                    this.onClick();

                })
                .catch(error => {
                    this.showFinalScreen = false;
                    this.showErrorOnScreen = true;
                    this.enabletoEditFields = true;
                   // this.savedFields = false;
                    this.signatureFlag = false;
                    this.isLoaded = true;
                    this.err = JSON.stringify(error);
                })
        }
    }


  sucessfulInfuion(){
    this.isLoaded=false;
    this.fields["Id"]=this.recordId;
    this.fields["CCL_Infusion_Procedure_status__c"]=this.infusionProcedureStatusValue;
    this.convertLastModifiedDateToNovartisFormat();
    if(this.infusionProcedureStatusValue === infusionCompleted){
      this.infusionReportingStatus = productInfusionCompleted;
    }
    this.fields["CCL_Infusion_Reporting_status__c"]=this.infusionReportingStatus;
    this.fields["CCL_Infusion_Procedure_status__c"]=this.infusionProcedureStatusValue;
    this.fields["CCL_Number_of_bags_Infused__c"]=this.numberOfBagsInfused;
    this.fields["CCL_Actual_Infusion_Date_Time_Text__c"]= this.selectedDate;
    this.fields["CCL_Actual_Infusion_Date_Time__c"]= this.actualDateValue+'T00:00:00' + this.timeZoneValue;

    this.completedInfusion = true;
    this.infusionProcedureStatusValue = this.infusionProcedureStatusValue;
    this.actualDateOfInfusion = this.selectedDate;
    this.numberOfBagsInfused = this.numberOfBagsInfused;
    this.dataEntered = this.loggedInUserName+' '+this.selectedDate+' '+this.lastModifiedTime+' '+this.infusionTimeZone;
    this.recordInput = { fields: this.fields };
    if(this.infusionProcedureStatusValue === infusionCompleted)
    {
   updateRecord(this.recordInput)
    .then(result=>{
       this.showErrorOnScreen=false;
        this.returnVal = result;
        this.showFinalScreen = false;
        this.showMainContent = true;
        this.showSuccessToast();
        this.isLoaded = true;
        this.savedFields = true;
        this.reloadSucessfull = true;
        this.renderedCallback();
        this.onClick();
        })
    .catch(error=>{
        this.showFinalScreen=false;
        this.showErrorOnScreen=true;
        this.isLoaded=true;
        this.err = JSON.stringify(error);
    })
  }
    else{
      this.err = JSON.stringify(error);
  }
}
validateFormAndProceedToInfusion(){
  this.fieldValidations = this.template.querySelectorAll("lightning-input");
          this.count = 0;
          for (let i = 0; i < this.fieldValidations.length; i++) {
              this.fieldNullCheck(this.fieldValidations[i], CCL_Mandatory_Field);
              if (this.fieldValidations[i].checkValidity()) {
                this.count = this.count + 1;
            }
        }
        if (this.count == this.fieldValidations.length && this.isvalid) {
            this.navigateFurther = true;
        } else {
            this.navigateFurther = false;
        }
        this.validateFormAndProceedToInfusion2();
    }

    validateFormAndProceedToInfusion2() {
        this.fieldValidations = this.template.querySelectorAll("lightning-combobox");
        this.count = 0;
        for (let i = 0; i < 1; i++) {
            this.fieldNullCheck(this.fieldValidations[i], CCL_Mandatory_Field);
            if (this.fieldValidations[i].checkValidity()) {
                this.count = this.count + 1;
            }
        }
        if (this.count == 1 && this.isvalid && this.navigateFurther) {
            this.navigateFurther = true;
        } else {
            this.navigateFurther = false;
        }


        if (this.isvalid && this.navigateFurther && this.actualDateValue > this.dateValue) {
            this.showErrorOnScreen = true;
            this.err = actualInfusionNotFuture;
        } else if (this.isvalid && this.navigateFurther && this.numberOfBagsInfused > this.numberOfBagsShipped) {
            this.showErrorOnScreen = true;
            this.err = infusedBagsCannotGrtrShippedBags;
        } else if (this.isvalid && this.navigateFurther) {
            this.showErrorOnScreen = false;
            this.infusionRecProcess();
        }
    }

    obaProcessFinalScreen() {

        //this.loggedInUserNameButton= this.lastModifiedByName;

        this.lastModTime = this.lastModifiedDate;
        this.isLoaded = false;
        this.fields["Id"] = this.recordId;
        // this.loggedInUserName = this.data.fields.Name.value;


        //  this.loggedInUserNameButton =  this.loggedInUserName;

        if (this.milestone1Flag == false) {

            this.fields["CCL_OBA_Outcome_1__c"] = this.milestoneOutcome1;
            this.reprtedByOba = this.lastModifiedDateOba + ' ' + this.infusionTimeZone;
            this.fields["CCL_OBA_Outcome_1_Reported_On__c"] = this.reprtedByOba;
            this.fields["CCL_OBA_Outcome_1_Reported_By__c"] = USER_ID;
            //  this.milestone1Flag = true;
            //    this.loggedInUserNameButton=result.LastModifiedBy.Name;

            //this.userName= NAME_FIELD;
            // this.loggedInUserNameButton =  this.resultUser.fields.Name.value;
            // this.loggedInUserNameButton =  data.fields.Name.value;



            this.fields["CCL_OBA_Outcome_1_Reported_On_Date_Time__c"] = new Date().toISOString();
            //'YYYY-MM-ddThh:mm:ss.mil+hh:mm'
            //"2020-11-19T23:25:17.00+00:00:";
            //"20-11-19T23:55:20.057Z
            //"2020-11-04T00:30:00.000+00:00"

        } else if (this.milestone2Flag == false) {


            this.fields["CCL_OBA_Outcome_2__c"] = this.milestoneOutcome2;
            this.reprtedByOba = this.lastModifiedDateOba + ' ' + this.infusionTimeZone;
            this.fields["CCL_OBA_Outcome_2_Reported_On__c"] = this.reprtedByOba;
            this.fields["CCL_OBA_Outcome_2_Reported_By__c"] = USER_ID;
            this.fields["CCL_OBA_Outcome_2_Reported_On_Date_Time__c"] = new Date().toISOString();


        } else if (this.milestone3Flag == false) {

            this.fields["CCL_OBA_Outcome_3__c"] = this.milestoneOutcome3;
            this.reprtedByOba = this.lastModifiedDateOba + ' ' + this.infusionTimeZone;
            this.fields["CCL_OBA_Outcome_3_Reported_On__c"] = this.reprtedByOba;
            this.fields["CCL_OBA_Outcome_3_Reported_By__c"] = USER_ID;
            this.fields["CCL_OBA_Outcome_3_Reported_On_Date_Time__c"] = new Date().toISOString();

        } else if (this.milestone4Flag == false) {


            this.fields["CCL_OBA_Outcome_4__c"] = this.milestoneOutcome4;
            this.reprtedByOba = this.lastModifiedDateOba + ' ' + this.infusionTimeZone;
            this.fields["CCL_OBA_Outcome_4_Reported_On__c"] = this.reprtedByOba;
            this.fields["CCL_OBA_Outcome_4_Reported_By__c"] = USER_ID;
            this.fields["CCL_OBA_Outcome_4_Reported_On_Date_Time__c"] = new Date().toISOString();

        } else if (this.milestone5Flag == false) {


            this.fields["CCL_OBA_Outcome_5__c"] = this.milestoneOutcome5;
            this.reprtedByOba = this.lastModifiedDateOba + ' ' + this.infusionTimeZone;
            this.fields["CCL_OBA_Outcome_5_Reported_On__c"] = this.reprtedByOba;
            this.fields["CCL_OBA_Outcome_5_Reported_By__c"] = USER_ID;
            this.fields["CCL_OBA_Outcome_5_Reported_On_Date_Time__c"] = new Date().toISOString();

        }

        this.recordInput = { fields: this.fields };

        updateRecord(this.recordInput)
            .then(result => {

                this.showErrorOnScreen = false;
                this.returnVal = result;

                this.isLoaded = true;
                this.showSuccessToast();
                this.signatureFlag = true;
                this.reloadSucessfull = true;
                this.renderedCallback();
                this.dataEntered = this.lastModifiedByName + ' ' + this.lastModifiedDateTimeInNovartisFormat + ' ' + this.infusionTimeZone;
                // this.loggedInUserNameButton=this.result.LastModifiedBy.Name;
                // this.lastModifiedUserNameValue();

                this.onClick();
                this.showFinalScreen = false;
                this.showFinalScreenOba = false;
                this.showMainContent = true;

            })
            .catch(error => {
                this.showFinalScreenOba = false;
                this.showErrorOnScreen = true;
                this.enabletoEditFields = true;
                //this.savedFields = false;
                this.signatureFlag = false;
                this.isLoaded = true;
                this.err = JSON.stringify(error);
            })

    }

    // lastModifiedUserNameValue(){
    //   //this.loggedInUserName = data.fields.Name.value;
    //   this.loggedInUserNameButton= this.loggedInUserName;

    // }


    redirectToOrderSummaryPage() {

        this[NavigationMixin.Navigate]({
                type: 'standard__webPage',
                attributes: {
                    url: '/ccl-order/' + this.result.CCL_Order__r.Id + '/' + this.result.CCL_Order__r.Name
                }
            },
            true
        );
    }

    fieldNullCheck(comboCmp, errorMsg) {
        const value = comboCmp.value;
        const flag = comboCmp.checked;
        if (((flag == undefined && (value === this.EMPTY_STRING || value == null)) || ((value == 'undefined' || value === this.EMPTY_STRING || value == null) && flag == false))) {
            comboCmp.setCustomValidity(errorMsg);
            comboCmp.reportValidity();
            this.navigateFurther = false;
            this.isvalid = false;
        } else {
            comboCmp.setCustomValidity(this.EMPTY_STRING);
            comboCmp.reportValidity();
            this.isvalid = true;
        }
    }
    reloadOnSave() {
        this.getFieldDetails();
        this.reloadSucessfull = true;
        this.getCustomPermission(true);
    }

    closeModal() {
        this.showFinalScreen = false;
        this.showFinalScreenOba = false;
        this.showMainContent = true;
        this.infusionReportingStatus = this.infusionReportingStatusPrevious;
        this.selectedDate = '';

    }

    closeModalOba() {
        this.showFinalScreen = false;
        this.showFinalScreenOba = false;
        this.showMainContent = true;

  if((this.milestoneSCPDate1 != (null || undefined)) && this.milestone1Flag == false){
    this.milestoneOutcome1='';
  } else if((this.milestoneSCPDate2 != (null || undefined)) && this.milestone2Flag == false){
    this.milestoneOutcome2='';
  } else if((this.milestoneSCPDate3 != (null || undefined))&& this.milestone3Flag == false){
    this.milestoneOutcome3='';
  } else if((this.milestoneSCPDate4 != (null || undefined)) && this.milestone4Flag == false){
    this.milestoneOutcome4='';
  } else if((this.milestoneSCPDate5 != (null || undefined)) && this.milestone5Flag == false){
    this.milestoneOutcome5='';
  }


}

    cancelInfusionOba() {
        this.showMainContent = false;
        this.showFinalScreen = false;
        this.showFinalScreenOba = false;
        this.navigateToView();
    }

    cancelInfusion() {
        this.showMainContent = false;
        this.showFinalScreen = false;
        this.showFinalScreenOba = false;
        this.navigateToView();
    }

    showInfoToast() {
        const evt = new ShowToastEvent({
            title: fpDoseOutOfSpecification,
            message: '',
            variant: 'info',
            mode: 'sticky'
        });
        this.dispatchEvent(evt);
    }

    showSuccessToast() {
        this.showSuccess = true;
    }


    handleClose() {
      this.showSuccess = false;
    }

    getCustomPermission(reloadSucessfull) {

        getCheckUserPermissionPrimaryInfusion({

        })
        .then((result) => {
            this.captureUserAccessPrimaryinfusion = result;
        })
        .catch((error) => {
            this.error = error;
        });

        getCheckUserPermission({

            })
            .then((result) => {
                this.captureUserAccess = result;
                if (this.captureUserAccess === false && this.completedInfusion === true) {

                    this.disabletoEditFields = true;
                    this.savedFields = true;
                }
                if (this.captureUserAccess === true && reloadSucessfull === false && this.completedInfusion === false && this.shipmentStatusAcceptedRejectedFlag === true) {
                    this.enabletoEditFields = true;
                    this.disabletoEditFields = false;
                    this.savedFields = false;
                }
                if (this.captureUserAccess === true && reloadSucessfull === true && this.completedInfusion === true) {
                    this.savedFields = true;
                    this.enabletoEditFields = false;
                    this.disabletoEditFields = false;
                }
                if (this.captureUserAccess === true && reloadSucessfull === true && this.completedInfusion === false && this.shipmentStatusAcceptedRejectedFlag === true) {
                    this.savedFields = false;
                    this.enabletoEditFields = true;
                    this.disabletoEditFields = false;
                }
                if (this.captureUserAccess === true && reloadSucessfull === false && this.completedInfusion === true) {
                    this.savedFields = true;
                    this.enabletoEditFields = false;
                    this.disabletoEditFields = false;
                }
                if (this.captureUserAccess === false) {
                    this.enabletoEditFields = false;
                    this.disabletoEditFields = true;
                }
                if (this.captureUserAccess === true && this.shipmentStatusAcceptedRejectedFlag === false) {
                    this.enabletoEditFields = false;
                    this.disabletoEditFields = true;
                }


                this.error = null;
            })
            .catch((error) => {
                this.error = error;
            });




    }

    navigateToView1() {
        // Navigate to a URL
        this[NavigationMixin.Navigate]({
                type: 'standard__webPage',
                attributes: {
                    // url: '/?tabset-8e13c=94d71'
                    url: '/?tabset-8e13c=94d71&tabset-feec3=55ddf'
                }
            },
            true // Replaces the current page in your browser history with the URL
        );
    }
    navigateToView() {
        this[NavigationMixin.Navigate]({
            type: "comm__namedPage",
            attributes: {
                pageName: "fp-infusion-list-view"
            }
        });
    }
    navigateToViewHome() {
        this[NavigationMixin.Navigate]({
            type: "comm__namedPage",
            attributes: {
                name: "Home"
            }
        });
    }
    renderedCallback() {
        // this.getCustomPermissionIR();
        if(this.isPermLoaded){
            this.getCustomPermission(this.reloadSucessfull);
            this.isPermLoaded=false;
        }
      
    }
    connectedCallback() {


this.isPermLoaded=true;
        getFieldDetails({ recordId: this.recordId })
            .then(result => {
                this.actualDateOfInfusion = result.CCL_Actual_Infusion_Date_Time_Text__c;
                this.selectedDate = result.CCL_Actual_Infusion_Date_Time_Text__c;
                this.lastModifiedByName = result.LastModifiedBy.Name;
                this.result = result;
                this.convertLastModifiedDateToNovartisFormat();
                //this.dataEntered=this.loggedInUserName + ' '+this.lastModifiedDateTimeInNovartisFormat +' '+this.infusionTimeZone;
            })
            .catch(error => {
                this.err = JSON.stringify(error);

            });
    }


    @wire(getFileDetails, { recordId: '$recordId' })
    wiredFileListSteps({ error, data }) {
        if (data) {
            this.filesUploaded = data;

            if (this.filesUploaded.length > 0) {
                this.showUploadedFiles = true;

            }
            this.error = null;
        } else if (error) {
            this.error = error;
        }
    }

  viewDocument(event) {
    this.isFileViewModalOpen=true;
    this.url='/sfc/servlet.shepherd/document/download/'+event.target.name;
    window.open(this.url)
          }
  convertLastModifiedDateToNovartisFormat(){
      utilityMethodToConvertLastModifiedDate({
        inputDate: new Date(),
        tmz: this.result.CCL_Infusion_to_Time_Zone__c
    })
    .then(result=>{
        this.lastModifiedDateTimeInNovartisFormat= result;
        this.dataEntered=this.lastModifiedByName + ' '+this.lastModifiedDateTimeInNovartisFormat +' '+this.infusionTimeZone;

       })
    .catch(error=>{
        this.showErrorOnScreen=true;
        this.err = JSON.stringify(error);
    })
    }


    convertLastModifiedDateToNovartisFormatOba() {
        utilityMethodToConvertLastModifiedDate({
                inputDate: new Date(),
                tmz: this.result.CCL_Infusion_to_Time_Zone__c
            })
            .then(results => {
                this.lastModifiedDateOba = results;

            })
            .catch(error => {
                this.showErrorOnScreen = true;
                this.err = JSON.stringify(error);
            })
    }
}
import { LightningElement ,api,track} from 'lwc';
import searchAvailable from "@salesforce/label/c.PSP_search_Cards_and_Coupons";
import searchtext from "@salesforce/label/c.PSP_search_available_card";
import errMsg from "@salesforce/label/c.PSP_err_msg_for_Cards";
export default class PSP_CouponLookUp extends LightningElement {
    @api selectedItem=null;
    @api couponName="";
    @api recordtype="Coupon";
    @api object="PSP_Card_and_Coupon__c";
    @api searchtext=searchtext
    @track label=searchAvailable;
    @track inputVal='';
    @track errMsg=errMsg;
    connectedCallback(){
        this.selectedItem = null;

    }
    getselectedDetails(event){
       // console.log('the event details are'+JSON.stringify(event.detail));
        this.selectedItem=event.detail.Id;
        this.couponName=event.detail.Name;
        console.log('vals are'+this.couponName+ this.selectedItem);
    }
    
     @api
     validate() {
         if (this.selectedItem !== undefined && this.selectedItem !== null ) {
             return {
                 isValid: true
             };
         } else {
             //If the component is invalid, return the isValid parameter as false and return an error message. 
             return {
                 isValid: false,
                 errorMessage: errMsg
             };
         }
     }
}
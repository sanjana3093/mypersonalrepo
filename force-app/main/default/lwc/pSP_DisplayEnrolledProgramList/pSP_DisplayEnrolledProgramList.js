import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';
import enrolledProgram from "@salesforce/label/c.PSP_Enrolled_Program_Associated";
import getEpRecord from "@salesforce/apex/PSP_custLookUpCntrl.getEnrolledPrograms";
export default class PSP_DisplayEnrolledProgramList extends LightningElement {
    @api enrolledprogramlist;
    @api enrolledprogramName;
    @api careProgramName;
    @api selectedEp;
    @api careProgramId;
    @track set;
    @api recordId;
    @track error;
    @track eplist = [];
    @api showEpList = false;
    @api visibleEp = false;
    @track enrolledProgramList = enrolledProgram;

    connectedCallback() {
        getEpRecord({
                recordId: this.recordId,

            })
            .then(result => {
                if (result.length > 1) {
                    this.eplist = result;
                    this.showEpList = true;
                } else {
                    this.enrolledprogramlist = result[0].Id;
                    this.enrolledprogramName = result[0].Name;
                    this.careProgramName = result[0].PSP_Program_Enrollee__c;
                    this.careProgramId = result[0].CareProgramId;

                }

                this.error = null;
            })
            .catch(error => {
                this.error = error;

            });

    }
    handleChange(event) {
        let index = event.target.dataset.item;

        if (event.target.checked) {
            this.enrolledprogramlist = this.eplist[index].Id;
            this.enrolledprogramName = this.eplist[index].Name;
            this.careProgramName = this.eplist[index].PSP_Program_Enrollee__c;
            this.careProgramId = this.eplist[index].CareProgramId;
            this.careProgramName = this.careProgramName.substring(43, this.careProgramName.length - 4);

        }

    }

    //validate function
    @api
    validate() {

        if (this.enrolledprogramlist !== undefined && this.enrolledprogramlist !== null) {
            return {
                isValid: true
            };
        } else {
            //If the component is invalid, return the isValid parameter as false and return an error message. 
            return {
                isValid: false,
                errorMessage: 'Please select an Enrolled Program.'
            };
        }
    }
}
import { LightningElement, track, api } from "lwc";

export default class PSP_ManageProgramCatalogue extends LightningElement {
  @track bShowModal = false;
  @track selectedTab;
  @api productval;
  @api serviceval;
  @api recordId;
  @track currentTabName;
  @api product = "Product";
  @api service = "Service";
  @track productList = [];
  @track isButtonDisabled = true;

  /* javaScipt functions start */
  openModal() {
    // to open modal window set 'bShowModal' tarck value as true
    this.bShowModal = true;
  }

  closeModal() {
    // to close modal window set 'bShowModal' tarck value as false
    this.bShowModal = false;
    this.isButtonDisabled = true;
  }

  tabselect(evt) {
    this.selectedTab = evt.target.label;
  }
  getTabname(evt) {
    this.currentTabName = evt.detail;
    console.log("in event" + this.currentTabName);
  }
  handleselected(evt) {
    this.isButtonDisabled = false;
    console.log("event fired" + JSON.stringify(evt.detail));
    const textVal = evt.detail;
    if (this.currentTabName === this.product) {
      console.log("hello in product val 1" + this.productval);
      this.productval = textVal;
      console.log("hello in product val" + JSON.stringify(this.productval));
      this.productList = this.productList.concat(textVal);
    } else {
      this.serviceval = textVal;
    }
    console.log("textVaal" + JSON.stringify(textVal));
  }
  /* javaScipt functions end */
}
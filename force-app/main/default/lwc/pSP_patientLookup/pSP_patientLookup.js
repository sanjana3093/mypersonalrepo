import { LightningElement,api,track } from 'lwc';


import confirmPatient from "@salesforce/label/c.PSP_Confirm_Patient";
import searchtext from "@salesforce/label/c.PSP_Search";
import errMsg from "@salesforce/label/c.PSP_err_msg_for_Cards";
export default class PSP_patientLookup extends LightningElement {
    @track confirmPatient=confirmPatient;
    @track object='Account';
    @track recordtype='Person Account';
    @track searchtext=searchtext;
    @api selectedItem=null;
    @api selectedName;
    connectedCallback(){
        console.log('name'+this.selectedName+ this.selectedItem);
       

    }
    getselectedDetails(event){
        // console.log('the event details are'+JSON.stringify(event.detail));
         this.selectedItem=event.detail.Id;
         this.selectedName=event.detail.Name;
        // console.log(this.productServicename+ this.selectedItem);
     }
     @api
     validate() {

        console.log('in prev'+this.selectedItem);
         if (this.selectedItem !== undefined && this.selectedItem !== null ) {
             return {
                 isValid: true
             };
         } else {
             //If the component is invalid, return the isValid parameter as false and return an error message. 
             return {
                 isValid: false,
                 errorMessage: errMsg
             };
         }
     }
}
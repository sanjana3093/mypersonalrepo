import { LightningElement ,api} from 'lwc';

export default class CCL_LastReportedBy extends LightningElement {
    @api hasFieldValues;
    @api fieldApiName;
    @api username;
    get display(){
        if(this.fieldApiName=='CCL_TEXT_Actual_Dewar_Pkg_Date_Time__c'){
            return this.hasFieldValues[0].CCL_Last_Reported_By_Packaging__c;
        }else{
            return this.hasFieldValues[0].CCL_Last_Reported_By_Shipment__c;
        }
    }
}
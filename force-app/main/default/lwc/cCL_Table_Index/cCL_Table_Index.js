import { LightningElement ,api} from 'lwc';

export default class CCL_Table_Index extends LightningElement {
    @api index;
    get position() {
        return this.index + 1;
    }
}
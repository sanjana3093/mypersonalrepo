import {
    LightningElement,
    api,
    track
} from 'lwc';
import fetchRecords from "@salesforce/apex/PSP_custLookUpCntrl.getCareProgramProviders";
import selectProvider from "@salesforce/label/c.PSP_Select_Provider";
export default class PSP_getProviders extends LightningElement {
    @api selectedProServiceId;
    @api selectedProviderId = null;
    @api rtnValue;
    @track selectProvider = selectProvider;
    connectedCallback() {
        // subscribe to searchKeyChange event

        fetchRecords({
                proServiceId: this.selectedProServiceId,

            })
            .then(result => {
                console.log('the results are'+ this.rtnValue+ this.selectedProviderId);
                this.rtnValue = result;
                this.selectedProviderId=null;
                if (result.length === 0) {
                    this.rtnValue = null;
                }

                this.error = null;
            })
            .catch(error => {
                this.error = error;
            });

    }
    handleChange(event) {
        let index = event.target.dataset.item;
        console.log('in handlechange'+this.selectedProviderId);
        if (event.target.checked) {
            this.selectedProviderId = this.rtnValue[index].Id;

        }
    }

}
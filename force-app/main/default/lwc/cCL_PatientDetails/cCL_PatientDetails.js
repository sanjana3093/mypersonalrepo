import { LightningElement, api, wire, track } from "lwc";
import { getObjectInfo, getPicklistValues } from "lightning/uiObjectInfoApi";
import {
  FlowNavigationNextEvent,
  FlowNavigationBackEvent,
  FlowAttributeChangeEvent
} from "lightning/flowSupport";
import { CurrentPageReference } from "lightning/navigation";
import CCL_Patient_Details from "@salesforce/label/c.CCL_Patient_Details";
import CCL_Patient_Details_Header from "@salesforce/label/c.CCL_Patient_Details_Header";
import CCL_Hospital_Patient_ID from "@salesforce/label/c.CCL_Hospital_Patient_ID";
import CCL_Treatment_Protocal_Subject_ID from "@salesforce/label/c.CCL_Treatment_Protocal_Subject_ID";
import CCL_Treatment_Protocal_Subject_ID_Info from "@salesforce/label/c.CCL_Treatment_Protocal_Subject_ID_Info";
import CCL_Protocol_ID from "@salesforce/label/c.CCL_Protocol_ID";
import CCL_4_Digit_Center_Number from "@salesforce/label/c.CCL_4_Digit_Center_Number";
import CCL_4_Digit_Center_Number_Helptext from "@salesforce/label/c.CCL_4_Digit_Center_Number_Helptext";
import CCL_3_Digit_Subject_Number from "@salesforce/label/c.CCL_3_Digit_Subject_Number";
import CCL_3_Digit_Subject_Number_Helptext from "@salesforce/label/c.CCL_3_Digit_Subject_Number_Helptext";
import CCL_SubjectNumber_Lengthvalidation from "@salesforce/label/c.CCL_SubjectNumber_Lengthvalidation";
import CCL_SubjectNumber_Patternvalidation from "@salesforce/label/c.CCL_SubjectNumber_Patternvalidation";
import CCL_Pattern_Validation_error_message from "@salesforce/label/c.CCL_Pattern_Validation_error_message";
import CCL_First_Name from "@salesforce/label/c.CCL_First_Name";
import CCL_Middle_Name from "@salesforce/label/c.CCL_Middle_Name";
import CCL_Last_Name from "@salesforce/label/c.CCL_Last_Name";
import CCL_Patient_Intials from "@salesforce/label/c.CCL_Patient_Intials";
import CCL_Patient_Initial_Validation_Message from "@salesforce/label/c.CCL_Patient_Initial_Validation_Message";
import CCL_Year_Mismatch_Error_Message from "@salesforce/label/c.CCL_Year_Mismatch_Error_Message";
import CCL_Year_Format_Error_Message from "@salesforce/label/c.CCL_Year_Format_Error_Message";
import CCL_Patient_Weight_Error_Message from "@salesforce/label/c.CCL_Patient_Weight_Error_Message";
import CCL_Suffix from "@salesforce/label/c.CCL_Suffix";
import CCL_Date_fof_Birth from "@salesforce/label/c.CCL_Date_fof_Birth";
import CCL_Patient_Weight_PRF from "@salesforce/label/c.CCL_Patient_Weight_PRF";
import CCL_Patient_consent from "@salesforce/label/c.CCL_Patient_consent";
import CCL_PleaseSelect from "@salesforce/label/c.CCL_PleaseSelect";
import CCL_Cancel_Button from "@salesforce/label/c.CCL_Cancel_Button";
import CCL_Yes_edit_details from "@salesforce/label/c.CCL_Yes_edit_details";
import CCL_Consent from "@salesforce/label/c.CCL_Consent";
import CCL_dd_Lwc from "@salesforce/label/c.CCL_dd_Lwc";
import CCL_mmm_Lwc from "@salesforce/label/c.CCL_mmm_Lwc";
import CCL_yyyy_Lwc from "@salesforce/label/c.CCL_yyyy_Lwc";
import Capacity_API_message from "@salesforce/label/c.Capacity_API_message";
import CCL_Capacity_API_Change_Message from "@salesforce/label/c.CCL_Capacity_API_Change_Message";
import CCL_Prescriber from "@salesforce/label/c.CCL_Prescriber";
import CCL_Principal_Investigator from "@salesforce/label/c.CCL_Principal_Investigator";
import fetchPrescribers from "@salesforce/apex/CCL_PRF_Controller.fetchPrescribers";
import CCL_Mandatory_Field from "@salesforce/label/c.CCL_Mandatory_Field";
import CCL_Age_Error from "@salesforce/label/c.CCL_Age_Error";
import CCL_DOB_Error from "@salesforce/label/c.CCL_DOB_Error";
import CCL_Yes from "@salesforce/label/c.CCL_Yes";
import CCL_No from "@salesforce/label/c.CCL_No";
import SUFFIX_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Suffix__c';
import CCL_ORDER_OBJ from "@salesforce/schema/CCL_Order__c";
import CCL_Patient_Weight_Threshold from "@salesforce/label/c.CCL_Patient_Weight_Threshold";
import CCL_Replacement_Order from "@salesforce/label/c.CCL_Replacement_Order";
import checkphiPermission from "@salesforce/apex/CCL_PRF_Controller.checkPHIUser";
import sendCallout from "@salesforce/apex/CCL_PRF_Controller.sendCallout";
import NotAvailable from "@salesforce/label/c.CCL_Not_Available";
import ToBeConfirmed from "@salesforce/label/c.CCL_To_be_confirmed";
import getCoiDetails from "@salesforce/apex/CCL_PRF_Controller.fetchCountryCOI";
import checkCustOpsPermission from "@salesforce/apex/CCL_PRF_Controller.checkCustOpsPermission";
import cCL_Patient_Consents from "@salesforce/label/c.cCL_Patient_Consents";
import CCL_Document_Upload_Msg from "@salesforce/label/c.CCL_Document_Upload_Msg";
import checkUserPermission from "@salesforce/apex/CCL_PRF_Controller.checkPRFSubmitterPermission";
import createOrderDocument from "@salesforce/apex/CCL_Utility.createOrderDocument";
import CCL_Document_Required_Validation_Message from "@salesforce/label/c.CCL_Document_Required_Validation_Message";
import deleteFile from "@salesforce/apex/CCL_ADFController_Utility.deleteFile";
import CCL_DeleteConfirmation_Title from "@salesforce/label/c.CCL_DeleteConfirmation_Title";
import CCL_DeleteConfirmation_Message from "@salesforce/label/c.CCL_DeleteConfirmation_Message";
import CCL_DeleteConfirmation_Ok from "@salesforce/label/c.CCL_DeleteConfirmation_Ok";
import CCL_DeleteConfirmation_Cancel from "@salesforce/label/c.CCL_DeleteConfirmation_Cancel";
import { refreshApex } from "@salesforce/apex";

export default class CCLPatientDetails extends LightningElement {
  label = {
    CCL_Treatment_Protocal_Subject_ID,
    CCL_Patient_Details,
    CCL_Hospital_Patient_ID,
    CCL_Patient_Details_Header,
    CCL_Protocol_ID,
    CCL_4_Digit_Center_Number,
    CCL_4_Digit_Center_Number_Helptext,
    CCL_3_Digit_Subject_Number,
    CCL_3_Digit_Subject_Number_Helptext,
    CCL_SubjectNumber_Patternvalidation,
    CCL_SubjectNumber_Lengthvalidation,
    CCL_Treatment_Protocal_Subject_ID_Info,
    CCL_First_Name,
    CCL_Middle_Name,
    CCL_Last_Name,
    CCL_Suffix,
    CCL_Patient_Intials,
    CCL_Date_fof_Birth,
    CCL_Patient_Weight_PRF,
    CCL_Patient_consent,
    CCL_Prescriber,
    CCL_Principal_Investigator,
    CCL_Mandatory_Field,
    CCL_Patient_Weight_Threshold,
    CCL_Replacement_Order,
    NotAvailable,
    ToBeConfirmed,
    CCL_Pattern_Validation_error_message,
    CCL_Patient_Initial_Validation_Message,
    CCL_Year_Mismatch_Error_Message,
    CCL_Year_Format_Error_Message,
    CCL_Patient_Weight_Error_Message,
    CCL_PleaseSelect,
    CCL_Cancel_Button,
    CCL_Yes_edit_details,
    CCL_Consent,
    CCL_Capacity_API_Change_Message,
    Capacity_API_message,
    CCL_Yes,
    CCL_No,
	  cCL_Patient_Consents,
    CCL_Document_Upload_Msg,CCL_Document_Required_Validation_Message,
    CCL_dd_Lwc,
    CCL_mmm_Lwc,
    CCL_yyyy_Lwc, CCL_DeleteConfirmation_Title,
    CCL_DeleteConfirmation_Message, CCL_DeleteConfirmation_Ok,
    CCL_DeleteConfirmation_Cancel
  };
  @track originalMessage;
  @track fileResultDeleted;
  @track prescriberList;
  @api recordLength;
  @api prescriberId;
  @api prescriberName;
  @api hospital; // to capture site sfdcid
  @api therapyId;
  @api therapy1; //  to capture therapy sfdc id
  @api hospitalPatientId; //to access in flow
  hospPatientIdOptIn;
  @api hospPatientIdOptInflow;
  @api hospPatientoptflow;
  @api TherapySFDCID;
  @api stepName = [];
  isClinical = false;
  isCommercial = false;
  @api therapy = "";
  therapytype = "";
  error;
  gotresult = false;
  message;
  isvalid = false;
  futurevalid = false;
  agevalid = false;
  @track dobvalid = false;
  @track dobvalidmonth = false;
  @track dobvalidyear = false;
  prescriberError = false;
  @wire(CurrentPageReference) pageRef;
  @api CCL_centernum;
  @api CCL_subjectnum;
  @api CCL_firstname;
  @api CCL_middleinitial;
  @api CCL_lastname;
  @api CCL_suffix;
  @api CCL_patientweight;
  @api CCL_patientconsent;
  @api screenName = "";
  @api CCL_inpdate;
  @api CCL_inpmonth;
  @api CCL_inpyear;
  @api hospPatientIdflow;
  @api maximumAge;
  @api minimumAge;
  @api patientCountry;
  @api captturedpatientCountry;
  @api countryTherapyAssId;
  @api CCL_patientInitials;
  @api dateofBirth;
  @api optedIn;
  @api researchWithBioSample;
  @api CCL_prescriberid_commercial;
  @api CCL_principalinvestigatorid;
  @api contactidforprescriber;
  @api count = 0;
  patientId;
  @track newArr;
  @api centernum = "";
  @api minimumWeight;
  @api maximumWeight;
  @api weightValidationError=false;
  @api monthName;
  @api isLoadFirst =false;
  subjectnum;
  firstname;
  middleinitial;
  lastname;
  suffix;
  patientweight;
  @api patientconsent;
  researchsample; //variables to track the date validation on the patient details screen
  contactid;
  @track date;
  @track month;
  @track year;
  datenull;
  dateerror;
  leaperror;
  nonleaperror;
  @api intdate;
  @api intmonth;
  @api intyear;
  futureerror;
  datestr;
  dateval;
  ageval; //Variables to calculate the age based on the date of birth entered in patient details screen
  tod;
  todyear;
  age;
  todmonth;
  toddate;
  phipermission=false;
  @api age_day;
  @api age_month;
  @api CCL_patientage;
  @api var1 = "slds-p-left_none slds-p-right_none";
  @api var2 = "customPadding";
  @api returnToReview;
  @api therapyType;
  @api showHideBackBtn = false;
  @track returnFromReview = false;
  @api fieldValidations;
  @track EMPTY_STRING = "";
  @api navigateFurther = false; //function to set the date entered in patient details screen
  @api allowreplacementordertrue;
  @api recordId;
  @api valueDate;
  // CGTU-890
  @api backBtn; // flow variable
  @track centerNoSubjectIdArray = []; //local storage
  openDialogSub = true;
  schedulerPrompt = false;
  datasetName='';
  capcaityFieldValue='';
  @api qualifiedSlotsFlow;
  @api collectionCenterId;
  @api aphPickupLocationId;
  @api infusionCenterId;
  @api shipToLocId;
  @api qualifiedSlots;
  @track responseJson;
  @api plantId;
  @api slotId;
  @api valueStreamField;
  @api manufacturingStartDate;
  @api productFinishedDate;
  @api availableSlot;
  @api availableDateArray = [];
  @api aphPickupDate;
  @api plannedPickupDateFlow;
  @api availableDateArrayFlow;
  @api showDynamicText;
  @api lastNameOptedIn;
   @api offSetDays;
 @track recievedData=false;
 @api coiDataChanged=false;
 @track showFileUpload= false;
  @track orderpatientConsent;
  @track checkPatientConsent= false;
  @track showUploadedFiles = false;
  @track consentMandatory = false;
  @track  fileUploadMandatoryErr= false;
  @track fileUploadedCheck = false;
  @track renderDelete = false;
  @track filesUploaded = [];
  @track orderId;
  @api userId;
  @track newPermissionName = "CCL_PRF_Submitter";
  @track docVersionId;
  @track isDialogVisible = false;
  @track isPRFSubmitter = false;
  @api fileUploadDoc;
  @api orderDocId;
  @track custOpsLogin = false;
  dateHandler(event) {
	  this.coiDataChanged = true;
    this.date = event.detail.value;
    this.intdate = parseInt(this.date,10);
	this.nonleaperror = "";
	 this.dateofBirth= this.date;
    this.futureerror ="";
  } //function to set the month entered in patient details screen
  monthHandler(event) {
	  this.coiDataChanged = true;
    this.month = event.detail.value;
    this.intmonth = parseInt(this.month,10);
	this.nonleaperror = "";
    this.futureerror ="";
  } //function to set the year entered in patient details screen
  yearHandler(event) {
	  this.coiDataChanged = true;
    this.year = event.detail.value;
    this.intyear = parseInt(this.year,10);
	this.nonleaperror = "";
    this.futureerror ="";
  }
  renderedCallback() {
    if (this.isLoadFirst) {
      const prescriber = this.template.querySelector(".prescribercls");
      if (this.therapyType == "Commercial" && this.prescriberList) {
        prescriber.value = this.CCL_prescriberid_commercial == undefined ? '' : this.CCL_prescriberid_commercial;
        this.isLoadFirst = false;
      }
      const investigator = this.template.querySelector('.investigatorCls');
      if (this.therapyType == "Clinical" && this.prescriberList) {
        investigator.value = this.CCL_prescriberid_commercial == undefined ? '' : this.CCL_prescriberid_commercial;
        this.isLoadFirst = false;
      }
    }
	 if(this.fileUploadDoc && JSON.parse(this.fileUploadDoc).length>0 ){
    this.fileUploadedCheck=true;
  }else{
    this.fileUploadedCheck=false;
  }
  if(this.consentMandatory && this.fileUploadedCheck && this.fileUploadMandatoryErr){
      this.fileUploadMandatoryErr = false;
	  const fileCmp = this.template.querySelector(".fileClass");
      if(fileCmp.classList.contains("fileUploadBorderRed")){
        fileCmp.classList.remove("fileUploadBorderRed");
      }
      this.isvalid=true;
      this.navigateFurther=true;
    }
  }
  //function to capture the values from the previous screens
  connectedCallback() {
	  if(!this.orderDocId){
      createOrderDocument({docId:this.orderDocId})
      .then((result)=>{
        if(result){
          this.orderDocId = result;
        }
      })
      .catch((error)=>{
      });
    }
    if(this.fileUploadDoc) {
      this.filesUploaded = JSON.parse(this.fileUploadDoc);
    }
	if (this.hospPatientoptflow == 'Yes') {
      this.hospPatientIdOptIn = true;
    } else {
      this.hospPatientIdOptIn = false;
    }
    this.displaydiv(this.therapyType);
      if (
      (this.CCL_inpyear && this.CCL_inpdate && this.CCL_inpmonth) ||
      this.CCL_prescriberid_commercial
    ) {
      this.age = this.CCL_patientage;

      if (this.CCL_patientconsent) {
        this.patientconsent = "true";
      } else if (this.CCL_patientconsent == false) {
        this.patientconsent = "false";
      }
    }

    if (this.CCL_inpdate) {
      const date = ("0" + this.CCL_inpdate).slice(-2);
      this.intdate = parseInt(date, 10);
      this.age_day = this.CCL_inpdate;
      this.date = date.toString();
	   this.dateofBirth=this.date;
    }
    if(this.CCL_inpyear){
      this.intyear = parseInt(this.CCL_inpyear, 10);
      this.year = this.CCL_inpyear;
    }

    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];
    if(this.CCL_inpmonth){
      const monthInt = monthNames.indexOf(this.CCL_inpmonth);

      const formattedMonth = ("0" + monthInt).slice(-2);
      this.intmonth = monthInt;
      this.age_month = formattedMonth;

      this.month = formattedMonth.toString();
    }
    if (
      this.returnToReview !== null &&
      (this.returnToReview === "PatientCommercial" ||
        this.returnToReview === "PatientClinical")
    ) {
      this.returnFromReview = true;
    }
    if (this.therapyType === "Commercial") {
      this.showHideBackBtn = true;
    } else if (this.therapyType === "Clinical") {
      this.showHideBackBtn = false;
    }
    if (this.researchWithBioSample === "Yes") {
      this.researchsample = true;
    } else if (this.researchWithBioSample === "No") {
      this.researchsample = false;
    }
    checkCustOpsPermission()
      .then(result => {
        this.custOpsLogin = result;
      })
      .catch(error => {
        this.error = error;
      });
      // Added by Payal Start
      checkUserPermission({
        permissionName: this.newPermissionName
      })
        .then((result) => {
          //result=true;
          this.isPRFSubmitter = result;
          this.renderDelete = result;
          this.error = null;
        })
        .catch((error) => {
          this.error = error;
        });
  }

  //function to fetch picklist values
  @wire(getObjectInfo, { objectApiName: CCL_ORDER_OBJ })
  objectInfo;

  @wire(getPicklistValues, {
    recordTypeId: "$objectInfo.data.defaultRecordTypeId",
    fieldApiName: SUFFIX_FIELD
  })
  systemUsedPicklistValues(result) {
    if (result.data) {
      this.newArr = result.data.values;
   }
  }

  @wire(checkphiPermission)
  phiPermissioncheck({ error, data }) {
    if (data) {
      this.phipermission = data;
    }
    else if (error) {
      this.error = error;
    }
  }
 @wire(getCoiDetails, { therapyId: "$countryTherapyAssId" })
  wiredCOIDetails({ error, data }) {
    if (data) {
     this.optedIn=data[0];
     this.recievedData=true;
	  this.orderpatientConsent = this.optedIn.CCL_Patient_Consent_Upload__c;
     if(this.orderpatientConsent ==='Yes'){
       this.checkPatientConsent = true;
       this.showUploadedFiles = true;
       if(this.optedIn.CCL_Patient_Consent_Mandatory__c == 'Yes'){
        this.consentMandatory = true;
       }
     }
    } else if (error) {
      this.error = error;
    }
  }
  get acceptedFormats() {
    return [".docx", ".txt", ".xls", ".csv", ".ppt", ".jpg", ".jpeg", ".png", ".pdf"];
  }
  handleUploadFinished(event) {
   this.showUploadedFiles = true;
   this.fileUploadedCheck=true;
   this.filesUploaded =[...this.filesUploaded,...event.detail.files];
   this.fileUploadDoc = JSON.stringify(this.filesUploaded);
  }
 removeFile(event) {
        if (event.target.name === "openConfirmation") {
          let jsonToDelete;
          this.docVersionId = event.target.alternativeText;
          for(let i=0; i<this.filesUploaded.length;i++){
              if(this.filesUploaded.documentId===this.docVersionId){
                  jsonToDelete=this.filesUploaded[i];
                  break;
              }
          }
          this.isDialogVisible = true;
      }else if (event.target.name === "confirmModal") {
          //when user clicks outside of the dialog area, the event is dispatched with detail value  as 1
          if (event.detail !== 1) {
            if (event.detail.status === "confirm") {
              deleteFile({ contentDocumentId: this.docVersionId })
                .then((result) => {
        let files = [];
        this.filesUploaded.forEach(ele => {
          if(ele.documentId != this.docVersionId) {
            files.push(ele);
          }
        });
        this.filesUploaded = files;
		this.fileUploadDoc = JSON.stringify(this.filesUploaded);
                  this.isDialogVisible = false;
                  this.error = null;
				   if(this.filesUploaded.length<1){
                    this.fileUploadedCheck=false;
                  }
                })
                .catch((error) => {
                  this.error = error;
                });
              //Changes as part of 355 ends here
              this.isDialogVisible = false;
            } else if (event.detail.status === "cancel") {
              this.isDialogVisible = false;
            }
          }
          //hides the component
          this.isDialogVisible = false;
        }
}
viewDocument(event) {
  this.url = "/sfc/servlet.shepherd/document/download/" + event.target.name;
  window.open(this.url);
}

handleDeleteFinished() {
  this.filesUploaded =[...this.filesUploaded,...event.detail.files];
}
  //function to validate the date of birth entered in patient details screen
  validateForm() {
    //code for 2076
    if (this.phifield) {
      if ( (this.optedIn.CCL_Day_of_Birth__c && this.optedIn.CCL_Month_of_Birth__c) &&
        (this.intmonth === 3 ||
          this.intmonth === 5 ||
          this.intmonth === 8 ||
          this.intmonth === 10) &&
        this.intdate === 31
      ) {
        this.dateerror = CCL_DOB_Error;
        this.dobvalid = true;
        this.ageval = "";
        this.dobvalidmonth = false;
        this.dobvalidyear = false;
      } else if (
        (this.optedIn.CCL_Day_of_Birth__c && this.optedIn.CCL_Month_of_Birth__c && this.optedIn.CCL_Year_of_Birth__c)&&
        this.intmonth === 1 &&
        this.intdate > 29 &&
        this.intyear % 4 === 0
      ) {
        this.leaperror = CCL_DOB_Error;
        this.dobvalidmonth = true;
        this.dobvalid = false;
        this.ageval = "";
        this.dobvalidyear = false;
      } else if (
        (this.optedIn.CCL_Day_of_Birth__c && this.optedIn.CCL_Month_of_Birth__c)&&
        this.intmonth === 1 &&
        this.intdate > 29

      ) {
        this.leaperror = CCL_DOB_Error;
        this.dobvalidmonth = true;
        this.dobvalid = false;
        this.ageval = "";
        this.dobvalidyear = false;
      }
      else if (
        (this.optedIn.CCL_Day_of_Birth__c && this.optedIn.CCL_Month_of_Birth__c && this.optedIn.CCL_Year_of_Birth__c)&&
        this.intmonth === 1 &&
        this.intdate > 28 &&
        this.intyear % 4 !== 0
      ) {
        this.nonleaperror = CCL_DOB_Error;
        this.dobvalidyear = true;
        this.ageval = "";
        this.dobvalid = false;
        this.dobvalidmonth = false;
      } else if(this.optedIn.CCL_Year_of_Birth__c && this.intyear> (new Date).getFullYear()){
        this.futureerror = CCL_DOB_Error;
        this.futurevalid = true;
      }
      else {
        this.dateerror = "";
        this.leaperror = "";
        this.nonleaperror = "";
        this.dobvalid = false;
        this.futureerror = "";
        this.futurevalid = false;
        this.dobvalidmonth = false;
        this.dobvalidyear = false;
        if(this.date && this.month&&this.year){
          this.calculateage();
        }

      }
    }
  } //function to calculate the age based on the date of birth entered in patient details screen
  calculateage() {
    this.tod = new Date(Date.now());
    this.todyear = this.tod.getFullYear();
    this.age = parseInt(this.todyear - this.year,10);
    this.todmonth = this.tod.getMonth();
    this.toddate = this.tod.getDate();
    this.age_month = this.todmonth - this.month;
    this.age_day = this.toddate - this.date;
    if (this.age_month < 0 || (this.age_month == 0 && this.age_day < 0)) {
      this.age = parseInt(this.age,10) - 1;
      this.CCL_patientage = this.age;
      this.validateage();
    } else {
      this.age = parseInt(this.todyear - this.year,10);
      this.CCL_patientage = this.age;
      this.validateage();
    }
  }
  //function to validate age of the patient at the time of order creation Vs. Therapy preferred limit
  validateage() {
    if (this.age > this.minimumAge && this.age < this.maximumAge) {
      this.ageval = "";
      this.agevalid = false;
      this.futuredate();
    } else if (isNaN(this.age)) {
      this.ageval = "";
      this.agevalid = true;
      this.futuredate();
    } else if (this.age <= this.minimumAge || this.age >= this.maximumAge) {
      this.ageval = CCL_Age_Error;
      this.agevalid = true;
      this.futuredate();
    }
  }
  //function to display error for date entered in future
   futuredate() {
    //changes for 2076
    if(this.intyear&&this.intmonth&&this.intdate){
      this.datestr = new Date(this.intyear, this.intmonth, this.intdate);
      if (this.datestr > this.tod) {
        this.futureerror = CCL_DOB_Error;
        this.futurevalid = true;
        this.ageval = "";
      } else {
        this.futureerror = "";
        this.futurevalid = false;
      }
    }

  }
  get options() {
    return [
      { label: CCL_Yes, value: "true" },
      { label: CCL_No, value: "false" }
    ];
  }
  get salutationoptions() {
    return [
      { label: "None", value: "" },
      { label: "Sr.", value: "Sr." },
      { label: "Jr.", value: "Jr." }
    ];
  }
  get monthoptions() {
    return [
      { label: "Jan", value: "00" },
      { label: "Feb", value: "01" },
      { label: "Mar", value: "02" },
      { label: "Apr", value: "03" },
      { label: "May", value: "04" },
      { label: "Jun", value: "05" },
      { label: "Jul", value: "06" },
      { label: "Aug", value: "07" },
      { label: "Sep", value: "08" },
      { label: "Oct", value: "09" },
      { label: "Nov", value: "10" },
      { label: "Dec", value: "11" }
    ];
  }
  get dateoptions() {
    return [
      { label: "01", value: "01" },
      { label: "02", value: "02" },
      { label: "03", value: "03" },
      { label: "04", value: "04" },
      { label: "05", value: "05" },
      { label: "06", value: "06" },
      { label: "07", value: "07" },
      { label: "08", value: "08" },
      { label: "09", value: "09" },
      { label: "10", value: "10" },
      { label: "11", value: "11" },
      { label: "12", value: "12" },
      { label: "13", value: "13" },
      { label: "14", value: "14" },
      { label: "15", value: "15" },
      { label: "16", value: "16" },
      { label: "17", value: "17" },
      { label: "18", value: "18" },
      { label: "19", value: "19" },
      { label: "20", value: "20" },
      { label: "21", value: "21" },
      { label: "22", value: "22" },
      { label: "23", value: "23" },
      { label: "24", value: "24" },
      { label: "25", value: "25" },
      { label: "26", value: "26" },
      { label: "27", value: "27" },
      { label: "28", value: "28" },
      { label: "29", value: "29" },
      { label: "30", value: "30" },
      { label: "31", value: "31" }
    ];
  }

  get phifield(){
    return this.phipermission;
  }


  // Onchange
  handleCapacityFieldChange(event) {
	this.datasetName = event.target.dataset.id;
	this.capcaityFieldValue = event.target.value;
    if (
      this.collectionCenterId &&
      this.aphPickupLocationId &&
      this.infusionCenterId &&
      this.shipToLocId
    ) {
    if (this.openDialogSub && this.datasetName == "subjectnumid") {
			event.srcElement.value = this.CCL_subjectnum; // to retain the old value on HTML during poopup display
			this.showPrompt();
        this.template.querySelector(".sbjnum").blur();
		} else if (event.srcElement.reportValidity()) {
		  this.updateCapacityField(this.datasetName, this.capcaityFieldValue);
			}
	  } else {
		  this.updateCapacityField(this.datasetName, this.capcaityFieldValue);
	  }
  }

  // set the updated value in backend
  updateCapacityField(datasetName, value) {
    if (datasetName === "subjectnumid") {
      this.CCL_subjectnum = value;
    }
  }

  resetModalFlag(event) {
    if (event.srcElement.checkValidity()) {
      if (this.datasetName == "subjectnumid") {
        this.openDialogSub = true; // to reatin the old value on HTML during poopup display
      }
    }
  }

  //function to handle the inputs/outputs to flow capture them in the review page
  handleChange(event) {
    this.value = event.detail.value;
    if (event.target.dataset.id === "PatientId") {
		this.coiDataChanged = true;
      this.hospPatientIdflow = event.target.value;
    }
	if (event.target.dataset.id === "firstnameid") {
		this.coiDataChanged = true;
      this.CCL_firstname = event.target.value;
    }
    if (event.target.dataset.id === "middleinitialid") {
		this.coiDataChanged = true;
      this.CCL_middleinitial = event.target.value;
    }
    if (event.target.dataset.id === "lastnameid") {
		this.coiDataChanged = true;
      this.CCL_lastname = event.target.value;
    }
    if (event.target.dataset.id === "suffixid") {
		this.coiDataChanged = true;
      this.CCL_suffix = event.target.value;
    }
	  // code for 2076
    if (event.target.dataset.id === "initials") {
		this.coiDataChanged = true;
      this.CCL_patientInitials = event.target.value;
    }
    if (event.target.dataset.id === "patientweightid") {
      let patientWeightTemp = event.target.value;
      let patientWeightTemp1 = patientWeightTemp.split(".");
      let patientWeightTemp2 = '';
      if (patientWeightTemp1[1] != '' && patientWeightTemp1.length > 1) {
        patientWeightTemp2 = '.'+patientWeightTemp1[1].substring(0, 1);
      }
      this.CCL_patientweight = patientWeightTemp1[0]+patientWeightTemp2;
	  this.weightValidationError = false;
      this.template
        .querySelector(".validateweight")
        .classList.remove("weightInput");
    }
    if (event.target.dataset.id === "patientconsentid") {
      this.patientconsent = event.target.value;
    }
  }
  //function to be called on next button on patient details screen page
  onNext() {
    this.screenName = "Patient_Detail_Next";
	this.fileUploadDoc = JSON.stringify(this.filesUploaded);
	// CGTU-890
	this.centerNoSubjectIdArray.push(this.CCL_centernum);
    this.centerNoSubjectIdArray.push(this.CCL_subjectnum);
	this.backBtn = this.centerNoSubjectIdArray.toString();
    this.valueDate = this.template.querySelector(".dateyear");

    this.handleValidation();
     if(this.valueDate && this.valueDate.checkValidity()) {
      this.validateForm();
    }
    this.handleCustomValidationWeight();
    this.CCL_patientconsent = this.patientconsent;
    this.CCL_inpdate = this.intdate;
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"];
	this.monthName = monthNames[this.intmonth];
    this.CCL_inpmonth = this.monthName;
    this.CCL_inpyear = this.intyear;
  if (this.CCL_prescriberid_commercial === undefined) {
      this.template.querySelector(".customError").classList.add("redBorder");
      this.prescriberError = true;
    }
  if (this.CCL_prescriberid_commercial != undefined) {
      this.prescriberError = false;
      this.template
        .querySelector(".customError")
        .classList.remove("redBorder");
    }
    if(this.consentMandatory && !this.fileUploadedCheck && !this.custOpsLogin){
      this.fileUploadMandatoryErr = true;
	  const fileCmp = this.template.querySelector(".fileClass");
      if(!fileCmp.classList.contains("fileUploadBorderRed")){
        fileCmp.classList.add("fileUploadBorderRed");
      }
      this.isvalid=false;
      this.navigateFurther=false;
    }
    if (
      this.navigateFurther &&
      this.isvalid &&
      !this.dobvalid &&
      !this.agevalid &&
      !this.futurevalid &&
      !this.dobvalidmonth &&
      !this.dobvalidyear
    ) {
      if (this.CCL_prescriberid_commercial != undefined) {
        this.prescriberError = false;
        this.template
          .querySelector(".customError")
          .classList.remove("redBorder");
        if (this.aphPickupLocationId && this.shipToLocId && this.plannedPickupDateFlow) {
          this.handleCallout();
        }
        else {
          const attributeChangeEvent = new FlowAttributeChangeEvent(
            "ScreenName",
            this.screenName
          );
          this.dispatchEvent(attributeChangeEvent);
          const navigateNextEvent = new FlowNavigationNextEvent();
          this.dispatchEvent(navigateNextEvent);
        }
      }
    }
  }
  onReview() {
    this.screenName = "Patient_Detail_Review";
	this.fileUploadDoc = JSON.stringify(this.filesUploaded);
    this.handleValidation();
    this.validateForm();
    this.handleCustomValidationWeight();
    this.CCL_patientconsent = this.patientconsent;
    this.CCL_inpdate = this.intdate;
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"]
    this.monthName = monthNames[this.intmonth];
    this.CCL_inpmonth = this.monthName;
    this.CCL_inpyear = this.intyear;
    if (this.CCL_prescriberid_commercial === undefined) {
      this.template.querySelector(".customError").classList.add("redBorder");
      this.prescriberError = true;
    }
    if (this.CCL_prescriberid_commercial != undefined) {
        this.prescriberError = false;
        this.template
          .querySelector(".customError")
          .classList.remove("redBorder");
    }
    if (
      this.navigateFurther &&
      this.isvalid &&
      !this.dobvalid &&
      !this.agevalid &&
      !this.futurevalid &&
      !this.dobvalidmonth &&
      !this.dobvalidyear
    ) {
      if (this.CCL_prescriberid_commercial != undefined) {
        this.prescriberError = false;
        this.template
          .querySelector(".customError")
          .classList.remove("redBorder");
        if (this.aphPickupLocationId && this.shipToLocId && this.plannedPickupDateFlow) {
          this.handleCallout();
        }
        else {
          const attributeChangeEvent = new FlowAttributeChangeEvent(
            "ScreenName",
            this.screenName
          );
          this.dispatchEvent(attributeChangeEvent);
          const navigateNextEvent = new FlowNavigationNextEvent();
          this.dispatchEvent(navigateNextEvent);
        }
      }
    }
  }
  onPrevious() {
	this.CCL_patientconsent = this.patientconsent;
	this.fileUploadDoc = JSON.stringify(this.filesUploaded);
    this.CCL_inpdate = this.intdate;
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"]
	this.monthName = monthNames[this.intmonth];
    this.CCL_inpmonth = this.monthName;
    this.CCL_inpyear = this.intyear;
    this.screenName = "Patient_Detail_Back";
    const attributeChangeEvent = new FlowAttributeChangeEvent(
      "ScreenName",
      this.screenName
    );
    this.dispatchEvent(attributeChangeEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }
  handleSuccess(event) {
    this.dispatchEvent(
      new ShowToastEvent({
        title: "Success",
        message: event.detail.apiName + " created.",
        variant: "success"
      })
    );
  }
  displaydiv(therapyname) {
    this.therapyType = therapyname;
    if (this.therapyType === "Commercial") {
      this.isCommercial = true;
    } else {
      this.isClinical = true;
    }
  }
  handleValidation() {
    let inputValid = false;
    let comboboxValid = false;
    this.fieldValidations = this.template.querySelectorAll("lightning-input");
    this.count = 0;
    for (let i = 0; i < this.fieldValidations.length; i++) {
      this.fieldNullCheck(this.fieldValidations[i], CCL_Mandatory_Field);
      if (this.fieldValidations[i].checkValidity()) {
        this.count = this.count + 1;
      }
    }
    if (this.count == this.fieldValidations.length) {
      this.navigateFurther = true;
      inputValid = true;
    } else {
      this.navigateFurther = false;
    }
    this.fieldValid = this.template.querySelectorAll("lightning-combobox");
    this.count = 0;
    for (let j = 0; j < this.fieldValid.length; j++) {
      this.fieldNullCheck(this.fieldValid[j], CCL_Mandatory_Field);
      if (this.fieldValid[j].checkValidity()) {
        this.count = this.count + 1;
      }
    }
    if (this.count == this.fieldValid.length && inputValid) {
      this.navigateFurther = true;
      comboboxValid = true;
      this.validateForm();
    } else {
      this.navigateFurther = false;
    }
    this.fieldVal = this.template.querySelectorAll("lightning-radio-group");
    this.count = 0;
    for (let j = 0; j < this.fieldVal.length; j++) {
      this.fieldNullCheck(this.fieldVal[j], CCL_Mandatory_Field);
      if (this.fieldVal[j].checkValidity()) {
        this.count = this.count + 1;
      }
    }
    if (this.count == this.fieldVal.length && inputValid && comboboxValid) {
      this.navigateFurther = true;
      this.validateForm();
    } else {
      this.navigateFurther = false;
    }
  }
  fieldNullCheck(comboCmp, errorMsg) {
    const value = comboCmp.value;
    const fieldName = comboCmp.name;
    if (
      (value === this.EMPTY_STRING || value == null) &&
      fieldName !== "Midlle Initial" &&
      fieldName !== "progress" &&
      comboCmp.disabled != true
    ) {
      comboCmp.setCustomValidity(errorMsg);
      comboCmp.reportValidity();
      this.navigateFurther = false;
      this.isvalid = false;
    } else {
      comboCmp.setCustomValidity(this.EMPTY_STRING);
      comboCmp.reportValidity();
      this.isvalid = true;
    }
  }
  handleMessage(myMessage) {
    this.message = myMessage;
  }
  @wire(fetchPrescribers, { hospitalId: "$hospital", therapyId: "$therapy1" })
  wiredSteps({ error, data }) {
    if (data) {
      this.prescriberList = data;
      this.isLoadFirst = true;
      this.error = null;
      if (this.prescriberList.length > 1) {
        this.recordLength = true;
      } else if (this.prescriberList.length == 1) {
        let self = this;
        this.prescriberList.forEach(function (node) {
          self.prescriberId = node.Name;
          self.CCL_prescriberid_commercial = self.prescriberId;
          self.contactid =node.Id;
          self.contactidforprescriber =self.contactid;
        });
      }
    } else if (error) {
      this.error = error;
      this.prescriberList = null;
    }
  }
  handlePrescriber(event) {
    let self = this;
    const selectedOption = event.target.value;
    if (this.selectedOption == "" || this.selectedOption == null) {
      this.CCL_prescriberid_commercial = undefined;
    }
    this.prescriberList.forEach(function (node) {
      if (node.Name == selectedOption) {
        self.CCL_prescriberid_commercial = node.Name;
        self.contactid =node.Id;
        self.contactidforprescriber =self.contactid;
      }
    });
  }
//Added as part of user story CGTU-1064-Validate patient's weight on PRF
  handleCustomValidationWeight() {
 if((this.CCL_patientweight !== undefined) && (this.minimumWeight !== undefined) && (this.maximumWeight !== undefined))
  {
   let value= this.template.querySelector(".validateweight");
   let weight=parseFloat(this.CCL_patientweight, 1);
   if ((value.checkValidity()) && ((weight < this.minimumWeight) || (weight > this.maximumWeight)))  {
         this.template.querySelector(".validateweight").classList.add("weightInput");
         this.weightValidationError=true;
         this.navigateFurther = false;
       }
   }
    else{
      this.weightValidationError = false;
      this.template
        .querySelector(".validateweight")
        .classList.remove("weightInput");
        }
  }

  showPrompt() {
    // to open modal set isModalOpen tarck value as true
    this.schedulerPrompt = true;
    this.template.querySelector(".btnClass").focus();
  }
  changeScheduler() {
    this.schedulerPrompt = false;

    if (this.datasetName == "subjectnumid") {
      this.openDialogSub = false;
    }
  }
  doNotChangeScheduler() {
    this.schedulerPrompt = false;
  }


  handleCallout() {

    sendCallout({
      therapyId:  this.therapy1,
      aphPickupLocation: this.aphPickupLocationId,
      shipToLoc: this.shipToLocId,
      subjectId: this.CCL_centernum + '_' + this.CCL_subjectnum
    })
      .then((result) => {
        if (result) {
          this.responseJson = JSON.parse(result);
          if (this.responseJson.Status == "OK") {
            this.qualifiedSlots = this.responseJson.QualifiedSlots;
            this.qualifiedSlotsFlow = JSON.stringify(this.qualifiedSlots);
            this.plantId = this.qualifiedSlots[0].Manufacturing_Plant;
            this.slotId = this.qualifiedSlots[0].SlotID;
            this.valueStreamField = this.qualifiedSlots[0].ValueStream;
            const aphStartDate = this.qualifiedSlots[0].Day_0_Date;
            let newStartDate = aphStartDate.split("-").reverse().join("-");
            this.manufacturingStartDate = this.formatDate(newStartDate);
            const finishDate = this.qualifiedSlots[0].APHPickup_Date;

            let newFinishedDate = finishDate.split('-').reverse().join('-');
            const formattedFinshedDate = new Date(newFinishedDate);

            let currentDate = new Date(
              formattedFinshedDate.getFullYear(),
              formattedFinshedDate.getMonth(),
              formattedFinshedDate.getDate() +
              parseInt(this.qualifiedSlots[0].ValueStream,10)
            );

            const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
            let endDate =
              currentDate.getFullYear() +
              "/" +
              month +
              "/" +
              currentDate.getDate();
            this.productFinishedDate = this.formatDate(endDate);
            this.availableDates = this.qualifiedSlots[0].APHPickup_Date.replace(
              /-|\//g,
              "/"
            );
            this.availableSlot = this.availableDates;
            let tempDateArray = [];
            this.qualifiedSlots.forEach(function (node) {
              tempDateArray.push(node.APHPickup_Date.replace(/-|\//g, "/"));
            });
            this.availableDateArray = tempDateArray;
            this.availableDateArrayFlow = tempDateArray.toString();
            const flowMonth = this.plannedPickupDateFlow.split(" ");
            const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            const monthIndex = monthNames.indexOf(flowMonth[1]) + 1;
            const formattedDate = ("0" + (flowMonth[0])).slice(-2);
            const formattedMonth = ("0" + (monthIndex)).slice(-2);
            this.aphPickupDate = formattedDate + '/' + formattedMonth + '/' + flowMonth[2];

            if (this.availableDateArray.includes(this.aphPickupDate)) {
              let self = this;
              this.showDynamicText = true;
              this.qualifiedSlots.forEach(function (node) {
                if (node.APHPickup_Date.replace(/-|\//g, "/") === self.aphPickupDate) {
                    self.plantId = node.Manufacturing_Plant;
                    self.slotId = node.SlotID;
                    self.valueStreamField = node.ValueStream;
                  let startDate = node.Day_0_Date;
                  let newStartDate = startDate.split('-').reverse().join('-');
                  const formattedStartDate = new Date(newStartDate);

                  // used to pass in JSON to insert in SF
                  self.manufacturingDate = formattedStartDate;
                  let formattedDate = newStartDate.replace(/-|\//g, "/")
                  self.manufacturingStartDate = self.formatDate(formattedDate);
                  const finishDate = node.APHPickup_Date;

                  let newFinishedDate = finishDate.split('-').reverse().join('-');
                  const formattedFinshedDate = new Date(newFinishedDate);

                  // used to pass in JSON to insert in SF
                  formattedFinshedDate.setDate(formattedFinshedDate.getDate() + parseInt(node.ValueStream,10));
                  self.FinishedDate = formattedFinshedDate;
                  const month = ("0" + (formattedFinshedDate.getMonth() + 1)).slice(-2);
                  let endDate = formattedFinshedDate.getFullYear() + '/' + month + '/' + formattedFinshedDate.getDate();
                  self.productFinishedDate = self.formatDate(endDate);

                }
              });
            }
            else {
              this.interfaceFailureMethod();
            }

          } else {
            this.availableSlot = null;
            this.availableDateArrayFlow = null;
            this.interfaceFailureMethod();
          }
        } else {
          this.availableSlot = null;
          this.availableDateArrayFlow = null;
          this.interfaceFailureMethod();
        }
        const attributeChangeEvent = new FlowAttributeChangeEvent(
          "ScreenName",
          this.screenName
        );
        this.dispatchEvent(attributeChangeEvent);
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
      })
      .catch((error) => {
        this.error = error;
      });
  }


  interfaceFailureMethod() {
    if (this.therapyType == 'Commercial' && !this.replacementOrder) {
      this.manufacturingStartDate = ToBeConfirmed;
      this.showDynamicText = false;
      this.productFinishedDate = ToBeConfirmed;
      this.missingResponse = true;
      this.availableDateArray = null;
      this.availableDates = null;
      this.selectedDate = null;
    } else if (this.therapyType == 'Clinical') {
      this.showDynamicText = false;
      this.manufacturingStartDate = NotAvailable;
      this.productFinishedDate = NotAvailable;
      this.missingResponse = true;
      this.availableDateArray = null;
      this.selectedDate = null;
      this.availableDates = null;
    }
  }

  formatDate(date) {
    let mydate = new Date(date);
    let monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];

    let day = mydate.getDate();

    let monthIndex = mydate.getMonth();
    let monthName = monthNames[monthIndex];
    let year = mydate.getFullYear();
    return `${day} ${monthName} ${year}`;
  }
}
import {
    LightningElement,
    api
    //track
} from 'lwc';
import fetchRecords from "@salesforce/apex/PSP_custLookUpCntrl.getDeactivatedCardList";
//import selectProvider from "@salesforce/label/c.PSP_Select_Provider";
export default class PSP_getProviders extends LightningElement {
    @api selectedAssignedToId;
    @api selectedCardId = null;
    @api rtnValue;
    //@track selectProvider = selectProvider;
    connectedCallback() {
        // subscribe to searchKeyChange event

        fetchRecords({
                assignedToID: this.selectedAssignedToId,

            })
            .then(result => {

                this.rtnValue = result;
                if (result.length === 0) {
                    this.rtnValue = null;
                }

                this.error = null;
            })
            .catch(error => {
                this.error = error;
            });

    }
    handleChange(event) {
        let index = event.target.dataset.item;

        if (event.target.checked) {
            this.selectedCardId = this.rtnValue[index].Id;

        }
    }

}
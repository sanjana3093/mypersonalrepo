import { LightningElement,api,wire } from 'lwc';
import fetchFinishedProductDetails from '@salesforce/apex/CCL_PRF_Controller.fetchFinishedProductDetails';

import CCL_Finished_Product_Details from '@salesforce/label/c.CCL_Finished_Product_Details';
import CCL_Dose from '@salesforce/label/c.CCL_Dose';
import CCL_Expiration_Date from '@salesforce/label/c.CCL_Expiration_Date';
import CCL_Product_Disposition_Status from '@salesforce/label/c.CCL_Product_Disposition_Status';
import CCL_Actual_Date_of_Infusion from '@salesforce/label/c.CCL_Actual_Date_of_Infusion';
import CCL_Number_of_Bags_Infused from '@salesforce/label/c.CCL_Number_of_Bags_Infused';
import CCL_Infusion_Status from '@salesforce/label/c.CCL_Infusion_Status';
export default class FinishedProductInfusionDetails extends LightningElement {
label={
CCL_Finished_Product_Details,
CCL_Dose,
CCL_Expiration_Date,
CCL_Product_Disposition_Status,
CCL_Actual_Date_of_Infusion,
CCL_Number_of_Bags_Infused,
CCL_Infusion_Status

};
    @api orderId;
    @api manufacturingList=[];

    @wire(fetchFinishedProductDetails, { orderId: "$orderId"})
    fetchFinishedProductDetails({ error, data }) {
     if (data) {
                let updatedLst=[];

        for(let i=0; i<data.length; i++){
                let manufacture =  {...data[i]};

                if(manufacture.CCL_Batch_Status__c == 'Conditionally Approved' || manufacture.CCL_Batch_Status__c=='Approved'){
                  manufacture.deposition= 'Final Product Released';
                }
                if(manufacture.CCL_Batch_Status__c== 'Quality'){
                  manufacture.deposition= 'Shipped Under Quarantine';
                }
                if(manufacture.CCL_Batch_Status__c== 'Rejected'){
                  manufacture.deposition= 'Final Product Rejected';
                }
                updatedLst.push(manufacture);
            }
                this.manufacturingList = updatedLst;

             } else if (error) {
                this.error = error;
              }
    }
}
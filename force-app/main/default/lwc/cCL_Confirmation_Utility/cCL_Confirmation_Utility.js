import { LightningElement, api,track } from 'lwc';
import { NavigationMixin } from "lightning/navigation";
import CCL_Return_to_Dashboard from "@salesforce/label/c.CCL_Return_to_Dashboard";


export default class CCL_Confirmation_Utility extends NavigationMixin(
    LightningElement
  ) {
    @api iconName;
    @api orderApprovedText;
    @api orderApprovedSubText;
    @api iconClass;

    @track labels={
        CCL_Return_to_Dashboard
    }

    goToNextCommercialScreen() {
        this[NavigationMixin.Navigate]({
            type: "comm__namedPage",
            attributes: {
                name: 'Home'
            }
        });
    }

}
import { LightningElement,api,wire } from 'lwc';
import fetchManufacturingDetails from '@salesforce/apex/CCL_PRF_Controller.fetchManufacturingDetails';
import CCL_Manufacturing_Details from "@salesforce/label/c.CCL_Manufacturing_Details";
import CCL_Manufacturing_Run from "@salesforce/label/c.CCL_Manufacturing_Run";
import CCl_Manufacturing_Status from "@salesforce/label/c.CCl_Manufacturing_Status";
import CCL_Manufacturing_Chevron from "@salesforce/label/c.CCL_Manufacturing_Chevron";
import CCL_Start_Date from "@salesforce/label/c.CCL_Start_Date";
import CCL_End_Date from "@salesforce/label/c.CCL_End_Date";
import CCL_QA_Testing from "@salesforce/label/c.CCL_QA_Testing";
import CCL_Completion_Date from "@salesforce/label/c.CCL_Completion_Date";
import CCL_Exception from "@salesforce/label/c.CCL_Exception";
export default class CCL_ManufacturingSummary extends LightningElement {
 label={
    CCL_Manufacturing_Details,
    CCL_Manufacturing_Run,
    CCl_Manufacturing_Status,
    CCL_Start_Date,
    CCL_End_Date,
    CCL_QA_Testing,
    CCL_Completion_Date,
    CCL_Exception
};
    @api orderId;
    @api manufacturingList=[];
    @api orderStatus='';

    @wire(fetchManufacturingDetails, { orderId: "$orderId"})
    fetchManufacturingDetails({ error, data }) {
      if (data) {
        this.manufacturingList = data;
     } else if (error) {
        this.error = error;
      }
}

   get displayWarning() {
    return (this.orderStatus == 'APH_OnHold' || this.orderStatus == 'MFG_OnHold');
  }

}
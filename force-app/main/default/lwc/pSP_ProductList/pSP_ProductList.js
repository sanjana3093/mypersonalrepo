import {
  LightningElement,
  wire,
  track,
  api
} from "lwc";
import getProductList from "@salesforce/apex/PSP_Search_Component_Controller.getProductList";
import getRecord from "@salesforce/apex/PSP_Search_Component_Controller.getProduct";
import createRecords from "@salesforce/apex/PSP_Search_Component_Controller.createCPPRecords";
import updateRecords from "@salesforce/apex/PSP_Search_Component_Controller.updateRecords";
import {
  getObjectInfo
} from 'lightning/uiObjectInfoApi';
import PRODUCT_OBJECT from '@salesforce/schema/Product2';
import {
  getPicklistValues
} from 'lightning/uiObjectInfoApi';
import PRODUCT_FRANCHISE from '@salesforce/schema/Product2.PSP_Franchise__c';
import PRODUCT_PORTFOLIO from '@salesforce/schema/Product2.PSP_Portfolio__c';
import {
  refreshApex
} from "@salesforce/apex";
import {
  ShowToastEvent
} from "lightning/platformShowToastEvent";
import successMessage from "@salesforce/label/c.PSP_Success_Message";
import successUpdMessage from "@salesforce/label/c.PSP_Success_Message_Update";
import successTitle from "@salesforce/label/c.PSP_Toast_Title_Success";
import errorVariant from "@salesforce/label/c.PSP_Error_Variant";
import errorUpdMessage from "@salesforce/label/c.PSP_Error_Message_Update";
import errorMessage from "@salesforce/label/c.PSP_Error_Message_Create";
import activeHeader from "@salesforce/label/c.PSP_Active";
import franchiseHeader from "@salesforce/label/c.PSP_Franchise";
import portfolioHeader from "@salesforce/label/c.PSP_Portfolio";
import defaultheader from "@salesforce/label/c.PSP_Default";
import productHeader from "@salesforce/label/c.PSP_Product_Name";
import cppheader from "@salesforce/label/c.PSP_CPP_Name";
import carePlanTemplate from "@salesforce/label/c.PSP_Care_Plan_Template";
import filter from "@salesforce/label/c.PSP_Filter";
import clear from "@salesforce/label/c.PSP_Clear";
import save from "@salesforce/label/c.PSP_Save";
import cancel from "@salesforce/label/c.PSP_Cancel";

const COLS = [

  {
    label: productHeader,
    fieldName: "nameURL",
    type: "url",
    typeAttributes: {
      label: {
        fieldName: "name"
      },
      tooltip: {
        fieldName: "name"
      },
      target: "__self"
    },
    sortable: true
  },
  {
    label: cppheader,
    fieldName: "cppNameUrl",
    type: "url",
    typeAttributes: {
      label: {
        fieldName: "cppName"
      },
      tooltip: {
        fieldName: "cppName"
      },
      target: "__self"
    },
    sortable: true
  },
  {
    label: activeHeader,
    editable: true,
    fieldName: "isActive",
    type: "boolean"

  },
  {
    label: defaultheader,
    fieldName: "defaultAct",
    type: "boolean",
    editable: true
  },
  {
    label: franchiseHeader,
    fieldName: "franchise",
    type: "text",
    sortable: true
  },
  {
    label: portfolioHeader,
    fieldName: "portfolio",
    type: "text",
    sortable: true
  },
  {
    label: carePlanTemplate,
    fieldName: "careProgProdId",
    type: "lookup",
    editable: true,
    typeAttributes: {

      label: {
        fieldName: "carePlanTemplate"
      }
    }

  }

];

export default class Datatable extends LightningElement {
  @track error;
  @track data = [];
  @track columns = COLS;
  @track draftValues = [];
  @api rowOffset;
  @track sortedBy;
  @track sortDirection = "asc";
  @track showSpinner = true;
  @api recordId;
  @api rows;
  @track backupdraft = [];
  @track selRec = [];
  @track proserviceVal;
  @track activeBox;
  @track careProgram;
  @track portfolio;
  @track ifArrayEmpty = false;
  @track franchise;
  @api resultdata = [];
  @track filterdata = [];
  @track careplandata = [];
  @track btnClass = "slds-hide";
  @track inlineEdit = false;
  @track finalArray = [];
  @track franchisepicklistVal;
  @track portfoliopicklistVal;
  @track active=activeHeader;
  @track filter=filter;
  @track clear=clear;
  @track save=save;
  @track cancel=cancel;
  @track productName=productHeader;
  wiredsObjectData;

  @wire(getObjectInfo, {
    objectApiName: PRODUCT_OBJECT
  })
  objectInfo;
  @wire(getPicklistValues, {
    recordTypeId: '$objectInfo.data.defaultRecordTypeId',
    fieldApiName: PRODUCT_FRANCHISE
  })
  IndustryPicklistValues(result) {
    this.franchisepicklistVal = result;
  }
  @wire(getPicklistValues, {
    recordTypeId: '$objectInfo.data.defaultRecordTypeId',
    fieldApiName: PRODUCT_PORTFOLIO
  })
  portfolioval(result) {
    this.portfoliopicklistVal = result;
  }

  @wire(getProductList, {
    recordId: "$recordId"
  })
  wiredProducts(result) {
    this.wiredsObjectData = result;
    if (result.data) {
      this.data = result.data;
      this.resultdata = result.data;
      this.showSpinner = false;
    } else if (result.error) {
      this.error = result.error;
    }

  }
  @wire(getRecord, {
    recordId: "$recordId"
  })
  programRec(result) {
    if (result.data) {
      this.careProgram = result.data;
    }
  }

  @api
  get productval() {
    return this.selRec;
  }
  set productval(prvalue) {
    if (prvalue !== undefined) {
      let saveAll = Array.isArray(prvalue);
      if (saveAll) {
        if (prvalue.length > 0) {
          this.selRec = prvalue;
          this.saveMethod(this.selRec);
        }
      } else {
        this.selRec.push(prvalue);
        this.saveMethod(this.selRec);
      }
    }
  }

  saveMethod(productList) {
    let draftValuesStr = JSON.stringify(productList);
    let prod = this.careProgram;
    createRecords({
        productRecs: draftValuesStr,
        careProgram: prod
      })
      .then(result => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: successTitle,
            message: successMessage,
            variant: successTitle
          })
        );
        const currentRecord = this.data;
        const newData = productList.concat(currentRecord);
        this.data = newData;
        this.selRec = [];
        return refreshApex(this.wiredsObjectData);
      })
      .catch(error => {
        // Handle error
        this.dispatchEvent(
          new ShowToastEvent({
            title: errorMessage,
            message: error.body.message,
            variant: errorVariant
          })
        );
        this.selRec = [];
      });
  }

  //Method for sorting
  sortData(fieldName, sortDirection) {
    let data = JSON.parse(JSON.stringify(this.data));
    //function to return the value stored in the field
    let key = a => a[fieldName];
    let reverse = sortDirection === "asc" ? 1 : -1;
    data.sort((a, b) => {
      let valueA = key(a) ? key(a).toLowerCase() : "";
      let valueB = key(b) ? key(b).toLowerCase() : "";
      return reverse * ((valueA > valueB) - (valueB > valueA));
    });
    //set sorted data to opportunities attribute
    this.data = data;
  }
  //this method is called when a column sort icon is clicked
  sortThisColumn(event) {
    this.sortedBy = event.detail.fieldName;
    this.sortDirection = event.detail.sortDirection;
    this.sortData(this.sortedBy, this.sortDirection);
  }
  handleSave(event) {
    let mapData = [];
    this.inlineEdit = false;
    let draftValuesStr = JSON.stringify(event.detail.draftValues);
    if (draftValuesStr === undefined || draftValuesStr === null) {
      draftValuesStr = JSON.stringify(this.careplandata);
    } else if (this.careplandata !== undefined && this.careplandata !== null) {
      draftValuesStr = this.mergeArray(event.detail.draftValues, this.careplandata);
    }
    for (let i = 0; i < this.data.length; i++) {
      mapData.push({
        value: this.data[i].proProductId,
        key: 'row-' + i
      });
    }
    draftValuesStr = this.mergeDraftVals(draftValuesStr, mapData);

    updateRecords({
        updateObjStr: draftValuesStr,
        mapfamToPro: mapData
      })
      .then(result => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: successTitle,
            message: successUpdMessage,
            variant: successTitle
          })
        );
        // Clear all draft values
        this.draftValues = [];

        // Display fresh data in the datatable
        return refreshApex(this.wiredsObjectData);
      })
      .catch(error => {
        // Handle error
        this.dispatchEvent(
          new ShowToastEvent({
            title: errorUpdMessage,
            message: error.body.message,
            variant: errorVariant
          })
        );
      });
  }
  handlechange(event) {
    if (event.target.value.length > 2) {
      this.proserviceVal = event.target.value.toLowerCase();
    }

  }
  handlechangeCheckbox(event) {
    this.activeBox = event.target.checked;
  }
  onselectFranchise(event) {
    this.franchise = event.target.value;

  }
  onselectPortfolio(event) {
    this.portfolio = event.target.value;
  }
  filterTable() {
    this.filterdata = [];
    let finalVar = []
    this.ifArrayEmpty=false;
    this.data = this.wiredsObjectData.data;
    console.log('the val'+JSON.stringify(this.data));
    if (this.proserviceVal !== null && this.proserviceVal !== undefined) {
      finalVar = this.filterByValue(productHeader, this.data);
    }

    if (this.portfolio !== undefined && this.portfolio != null) {
      if (finalVar.length > 0) {
        finalVar = this.filterByValue(portfolioHeader, finalVar);
      } else {
        finalVar = this.filterByValue(portfolioHeader, this.data);
      }
    }
    if (this.franchise !== undefined && this.franchise != null) {
      if (finalVar.length > 0) {
        finalVar = this.filterByValue(franchiseHeader, finalVar);
      } else {
        finalVar = this.filterByValue(franchiseHeader, this.data);
      }
    }
    if (this.activeBox !== undefined && this.activeBox != null) {
      if (finalVar.length > 0) {
        finalVar = this.filterByValue(activeHeader, finalVar);
      } else {
        finalVar = this.filterByValue(activeHeader, this.data);
      }
    }
    if (this.ifArrayEmpty) {
      this.data = []
      console.log('in 349');
    } else {
      this.data = finalVar;
    }

  }
  filterByValue(key, array) {
    let i = 0;
  
    console.log('the val336--'+JSON.stringify(array)+key);
    this.finalArray = [];
    for (i = 0; i < array.length; i++) {
      if (key === productHeader) {
        if (array[i].name.toLowerCase().includes(this.proserviceVal)) {
          this.finalArray.push(array[i]);
        }
      }

      if (key === franchiseHeader) {
        if (array[i].franchise === this.franchise) {
          this.finalArray.push(array[i]);
        }
      }
      if (key === portfolioHeader) {
        if (array[i].portfolio === this.portfolio) {
          this.finalArray.push(array[i]);
        }
      }

      if (key === activeHeader) {
        if (array[i].isActive) {
          this.finalArray.push(array[i]);
        }
      }
    }
    if (this.finalArray.length == 0) {
      this.ifArrayEmpty = true;
    }
    return this.finalArray;
  }
  clearFilter() {
    this.filterdata = [];
    this.portfolio = null;
    this.proserviceVal = null;
    this.activeBox = null;
    this.franchise = null;
    this.ifArrayEmpty = false;
    this.data = this.wiredsObjectData.data;
    return refreshApex(this.wiredsObjectData);
  }
  getDetails(event) {
    console.log('in selected event');
    if (this.inlineEdit) {
      this.btnClass = "slds-hide";
    } else {
      this.btnClass = "slds-show";
    }

    let draftstr = {
      'carePlanTemplate': '',
      'Id': ''
    };
    let eventVal = event.detail;
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].careProgProdId === eventVal.careProgId) {
        draftstr.carePlanTemplate = eventVal.Id;
        draftstr.Id = 'row-' + i;
      }
    }
    this.careplandata.push(draftstr);

  }
  get SaveStyle() {
    return this.btnClass;
  }
  hideSave() {
    this.btnClass = "slds-hide";
    this.data = this.wiredsObjectData.data;
    return refreshApex(this.wiredsObjectData);
  }
  saveChanges(event) {

    this.btnClass = "slds-hide";
    setTimeout(() => {
      this.handleSave(event);
    }, 2000);


  }
  handlechangeedit(event) {
    let draftValuesStr = JSON.stringify(event.detail.draftValues);
    this.backupdraft = draftValuesStr;
    this.inlineEdit = true;
    this.btnClass = "slds-hide";
  }
  handleCancel() {
    this.inlineEdit = false;
  }
  mergeArray(draft, careplan) {
    let i = 0,
      index = [],
      j = 0,
      finalVar;
    for (i = 0; i < draft.length; i++) {
      for (j = 0; j < careplan.length; j++) {
        if (draft[i].Id === careplan[j].Id) {
          draft[i].carePlanTemplate = careplan[j].carePlanTemplate;
          index.push(j);

        }
      }

    }
    for (j = 0; j < careplan.length; j++) {
      if (!index.includes(j)) {
        draft.push(careplan[j]);
      }
    }

    finalVar = draft;

    return JSON.stringify(finalVar);
  }
  mergeDraftVals(draft, map) {
    let i = 0,
      j = 0;
    draft = JSON.parse(draft);
    for (j = 0; j < map.length; j++) {
      for (i = 0; i < draft.length; i++) {
        if (draft[i].Id === map[j].key) {
          if (draft[i].defaultAct === null || draft[i].defaultAct === undefined) {

            draft[i].defaultAct = this.data[map[j].key.substr(4)].defaultAct;
          }
          if (draft[i].isActive === null || draft[i].isActive === undefined) {
            draft[i].isActive = this.data[map[j].key.substr(4)].isActive;
          }

          if (draft[i].carePlanTemplate === null || draft[i].carePlanTemplate === undefined) {
            draft[i].carePlanTemplate = this.data[map[j].key.substr(4)].tepmplateId;
          }
        }
      }
    }

    return JSON.stringify(draft);
  }

}
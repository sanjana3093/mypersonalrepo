import { LightningElement,api } from 'lwc';
import CCL_Finished_Product_Shipment_Header from "@salesforce/label/c.CCL_Finished_Product_Shipment_Header";
import CCL_Details from "@salesforce/label/c.CCL_Details";
export default class CCL_FPTable_Index extends LightningElement {
	    label= {CCL_Finished_Product_Shipment_Header,
        CCL_Details,
    };
    @api index;
    get position() {
        return this.index + 1;
    }
}
import { LightningElement, track, api, wire } from 'lwc';
import { FlowAttributeChangeEvent, FlowNavigationNextEvent } from 'lightning/flowSupport';
import getPickAndDeliveryDetails from "@salesforce/apex/CCL_PRF_Controller.fetchPickAndDeliveryDetails";
import submitPrfForm from "@salesforce/apex/CCL_PRF_Controller.submitPrfForm";
import {fireEvent} from 'c/cCL_Pubsub';
import {CurrentPageReference} from 'lightning/navigation';
import sendCallout from "@salesforce/apex/CCL_PRF_Controller.sendCallout";
import checkCustOpsPermission from "@salesforce/apex/CCL_PRF_Controller.checkCustOpsPermission";

// custom label
import ReviewOrderRequest from '@salesforce/label/c.CCL_Review_Order_Request';
import TreatmentInformation from '@salesforce/label/c.CCL_Treatment_Information';
import ApheresisPickupDelivery from '@salesforce/label/c.CCL_Apheresis_Pickup_Delivery';
import PaymentDetails from '@salesforce/label/c.CCL_Payment_Details';
import PatientDetails from '@salesforce/label/c.CCL_Patient_Details';
import OrderingHospital from '@salesforce/label/c.CCL_Ordering_Hospital';
import TherapyName from '@salesforce/label/c.CCL_Therapy_Name';
import HospitalPatientID from '@salesforce/label/c.CCL_Hospital_Patient_ID';
import TreatmentProtocalSubjectID from '@salesforce/label/c.CCL_Treatment_Protocal_Subject_ID';
import HospitalPurchaseOrderNumber from '@salesforce/label/c.CCL_Hospital_Purchase_Order_Number';
import VAPurchaseOrder from '@salesforce/label/c.CCL_VA_Purchase_Order';
import FirstName from '@salesforce/label/c.CCL_First_Name';
import LastName from '@salesforce/label/c.CCL_Last_Name';
import MiddleName from '@salesforce/label/c.CCL_Middle_Name';
import Suffix from '@salesforce/label/c.CCL_Suffix';
import DOB from '@salesforce/label/c.CCL_Date_fof_Birth';
import PatientWeight from '@salesforce/label/c.CCL_Patient_Weight_PRF';
import PrincipalInvestigator from '@salesforce/label/c.CCL_Principal_Investigator';
import Patientconsent from '@salesforce/label/c.CCL_Patient_consent';
import Prescriber from '@salesforce/label/c.CCL_Prescriber';
import ApheresisCollectionCenter from '@salesforce/label/c.CCL_Apheresis_Collection_Center';
import ApheresisPickupLocation from '@salesforce/label/c.CCL_Apheresis_Pickup_Location';
import InfusionCenter from '@salesforce/label/c.CCL_Infusion_Center';
import CCL_Edit from '@salesforce/label/c.CCL_Edit';
import CCL_Return_to_Review from '@salesforce/label/c.CCL_Return_to_Review';
import CCL_FOOTER_SUBMIT from '@salesforce/label/c.CCL_FOOTER_SUBMIT';
import CCL_Collection_Address from '@salesforce/label/c.CCL_Collection_Address';
import CCL_Pickup_Address from '@salesforce/label/c.CCL_Pickup_Address';
import InfusionAddress from '@salesforce/label/c.CCL_Infusion_Address';
import ShipToLocation from '@salesforce/label/c.CCL_Ship_To_Location';
import ShipToAddress from '@salesforce/label/c.CCL_Ship_To_Address';
import CryopreservedApheresisShipmentDate from '@salesforce/label/c.CCL_Cryopreserved_Apheresis_Shipment_Date';
import EstimatedCellmanufacturingStartDate from '@salesforce/label/c.CCL_Estimated_Cell_manufacturing_Start_Date';
import EstimatedFinishedProductDeliveryDate from '@salesforce/label/c.CCL_Estimated_Finished_Product_Delivery_Date';
import CommercialPhysicianAttestation from '@salesforce/label/c.CCL_Commercial_Physician_Attestation';
import CommercialAcknowledgement from '@salesforce/label/c.CCL_Commercial_Acknowledgement';
import CCL_Clinical_Popup_Review_Message from '@salesforce/label/c.CCL_Clinical_Popup_Review_Message';
import CCL_Scheduler_dates_unavailable_message from '@salesforce/label/c.CCL_Scheduler_dates_unavailable_message';
import CCL_Select_Alternate_Scheduler_Dates_Popup_Message from '@salesforce/label/c.CCL_Select_Alternate_Scheduler_Dates_Popup_Message';
import CCL_Modal_Close from '@salesforce/label/c.CCL_Modal_Close';
import CCL_Choose_Alternative_Date_Button from '@salesforce/label/c.CCL_Choose_Alternative_Date_Button';
import CCL_Accept_New_Dates_Button from '@salesforce/label/c.CCL_Accept_New_Dates_Button';
import CCL_Confirm_Patient_Eligibility from '@salesforce/label/c.CCL_Confirm_Patient_Eligibility';
import CCL_Patient_Consent_Clinical from '@salesforce/label/c.CCL_Patient_Consent_Clinical';
import CCL_PRF_Submission_Error from '@salesforce/label/c.CCLPRF_Submission_Error';
import ClinicalSupportReview from '@salesforce/label/c.CCL_Clinical_Support_Review';
import CCL_Replacement_Order from "@salesforce/label/c.CCL_Replacement_Order";
import checkphiPermission from "@salesforce/apex/CCL_PRF_Controller.checkPHIUser";
import NotAvailable from "@salesforce/label/c.CCL_Not_Available";
import ToBeConfirmed from "@salesforce/label/c.CCL_To_be_confirmed";
import patientCountryLabel from "@salesforce/label/c.CCL_PatientCountry";
import billingPartyLabel from "@salesforce/label/c.CCL_BillingParty";
import attestation from "@salesforce/label/c.CCL_Commercial_Physician_Attestation";
import CCL_Patient_Intials from "@salesforce/label/c.CCL_Patient_Intials";

import getCoiDetails from "@salesforce/apex/CCL_PRF_Controller.fetchCountryCOI";
import getAddress from "@salesforce/apex/CCL_Utility.fetchAddress";
import updateContentVersion from "@salesforce/apex/CCL_Utility.updateContentVersion";
import checkPRFApprover from "@salesforce/apex/CCL_PRF_Controller.checkPRFApprover";
import cCL_Patient_Consents from "@salesforce/label/c.cCL_Patient_Consents";
import getFileDetails from "@salesforce/apex/CCL_PRF_Controller.getUploadedFileDetails";
import getcustomdoc from "@salesforce/apex/CCL_ADFController_Utility.getDocId";
import createOrderDocument from "@salesforce/apex/CCL_Utility.createOrderDocument";
import getConsentFile from "@salesforce/apex/CCL_Utility.getConsentFiles";

export default class CCLPRFSummarySubmit extends LightningElement {

  @api therapy='';
  @api hospital='';
  @api purchaseOrder='';
  @api editScreenVar='';
  @api paymentCheckbox;
  @api ReturnFromReview;
  @api var1='slds-p-left_none slds-p-right_none';
  @api var2='customPadding';
  @api hospitalOptInFlow=false;
  @api veteransApplicableFlow=false;
  @api CCL_centernum;
  @api CCL_subjectnum;
  @api CCL_firstname;
  @api CCL_middleinitial;
  @api CCL_lastname;
  @api CCL_suffix;
  @api CCL_patientweight;
  @api CCL_inpdate;
  @api CCL_inpmonth;
  @api CCL_inpyear;
  @api countryTherapyAssId;
  @api CCL_patientInitials;
  @api dateofBirth;
  @track optedIn;
  @api showDynamicText;
  @api hospitalPatientId;
  @api CCL_patientconsent;
  @api CCL_patientCon;
  @api collectionCenter;
  @api infusionCenter;
  @api aphPickupLocation;
  @api shipToLoc;
  @api collectionCenterId;
  @api aphPickupLocationId;
  @api infusionCenterId;
  @api shipToLocId;
  @api therapyType='';
  @api PrescriberInvestigator;
  @api hospitalPurchaseOrderOptInFlow;
  @api veteransFlagFlow;
  @track isClinical=false;
  @track isCommercial=false;
  @track month;
  checkPatientConsent;
  @api commercialPhysicianAttestation;
  @track isCommercialModalOpen = false;
  @track isClinicalModalOpen = false;
  @api isCommercialChecked=false;
  @api isClinicalChecked = false;
  @track aphPickupAndDeliveryData =null;
  @track collCenterAdd=[];
  @track aphPickupLocAdd=[];
  @track infCenterAdd=[];
  @track shiToLocAdd=[];
  @track setRadioBtnYes;
  @track setRadioBtnNo;
  @api therapyFlow;
  @api errorModal=false;
  @api loaded = false;
  @api CCL_patientage;
  @api contactidforprescriber;
  @api OrderingSiteSFDCID;
  @api TherapySFDCID;
  @api plannedPickupDate;
  @api plannedPickupDateFlow;
  @api manufacturingStartDate;
  @api productFinishedDate;
  @api manufacturingStartDateFlow;
  @api productFinishedDateFlow;
  @api missingResponse;
  @api belowSchedulerText;
  @api orderData;
  @api therapyIdName;
  @api patientEligibilityText;
  @api previousButtonClick;
  @api replacementOrderDates=false;
  @api allowreplacementordertrue;
  @api PatientRecordId;
  @api SiteDiscrepancyText;
  @api sitetextavail;
  @api manufacturingStartDatesecond;
  @api productFinishedDatesecond;
  @track isModalOpen = false;
  @api productFinishedDatesecondFlow;
  @api manufacturingStartDatesecondFlow;
  @track responseJson;
  @api rescheduleReason;
  @api plantId;
  @api slotId;
  @api valueStreamField;
  @api qualifiedSlots;
  @api qualifiedSlotsFlow;
  @api availableSlot;
  @api availableDateArrayFlow;
  @track calendarField;
  @api aphPickupDate;
  @api approvalCheckboxFlow;
  @track radioOptions=[
    {'label': 'Yes', 'value': 'Yes'},
    {'label': 'No', 'value': 'No'}
    ];
  @api billingPartyVisible;
  @api capturePatientCountry;
  @api patientCountry;
  @api patientCountryResidenceValue;
  @api billingPartyValue;
  @api billingParty;
  @api hospitalPurchaseOrderText;
  @api splitSubject;
  @api approvalPageCounter = false;
  @api hospatIdEmailOpt;
  @api hospatId;
  @api hospatIdStr;
  hospatIdEmailOptIn=false;
  hospatIdIn=false;
  billingPartyOptIn = false;
  patientCountryOptIn = false;
  phipermission=false;
  showDynamicTextTemp=false;
  @track custOpsLogin;
  @api lastNameOptedIn;

	//CGTU 2204
	@api orderfirstnamecoi;
   @api ordermiddlenamecoi;
   @api orderlasttnamecoi;
   @api ordersuffixcoi;
   @api orderinitialscoi;
   @api orderdaycoi;
   @api ordermonthcoi;
   @api orderyearcoi;
   @api allCOIFalse=false;
   @api patId;
    @api offSetDays;
	@api addressList;
	 @api addressIds =[];
	 @api coiDataChanged;
@api orderid;
  @api orderDocId;
  @track fileIdList = [];
    @api fileUploadDoc;
    @track filesUploaded=[];
    @track prfApprover=false;
	@track filePresent=false;

  @wire(CurrentPageReference) pageRef;

  @wire(checkphiPermission)
  phiPermissioncheck({ error, data }) {
    if (data) {
      this.phipermission = data;
    }
    else if (error) {
      this.error = error;
    }
  }
//2076
  @wire(getCoiDetails, { therapyId: "$countryTherapyAssId" })
  wiredCOIDetails({ error, data }) {
    if (data) {
		this.optedIn=data[0];
		this.hasDob=this.optedIn.CCL_Day_of_Birth__c||this.optedIn.CCL_Month_of_Birth__c||this.optedIn.CCL_Year_of_Birth__c?true:false;
		if(!this.orderfirstnamecoi && !this.ordermiddlenamecoi && !this.orderlasttnamecoi && !this.ordersuffixcoi && !this.orderinitialscoi
         && !this.orderdaycoi && !this.ordermonthcoi && !this.orderyearcoi){
			this.allCOIFalse=true;
      }
      if(this.optedIn.CCL_Patient_Consent_Upload__c ==='Yes'){
        this.checkPatientConsent = true;
      }
    } else if (error) {
      this.error = error;
    }
  }
  label = { ReviewOrderRequest,
            CCL_Edit,
            CCL_Return_to_Review,
            CCL_FOOTER_SUBMIT,
            TreatmentInformation,
            ApheresisPickupDelivery,
            PaymentDetails,
            PatientDetails,
            OrderingHospital,
            TherapyName,
            HospitalPatientID,
            TreatmentProtocalSubjectID,
            HospitalPurchaseOrderNumber,
            VAPurchaseOrder,
            FirstName,
            LastName,
            MiddleName,
            Suffix,
            DOB,
            PatientWeight,
            PrincipalInvestigator,
            Patientconsent,
            Prescriber,
            ApheresisCollectionCenter,
            CCL_Collection_Address,
            ApheresisPickupLocation,
            CCL_Pickup_Address,
            InfusionCenter,
            InfusionAddress,
            ShipToLocation,
            ShipToAddress,
            CryopreservedApheresisShipmentDate,
            EstimatedCellmanufacturingStartDate,
            EstimatedFinishedProductDeliveryDate,
            CommercialPhysicianAttestation,
            CommercialAcknowledgement,
            CCL_Clinical_Popup_Review_Message,
            CCL_Modal_Close,
            CCL_Confirm_Patient_Eligibility,
            CCL_Patient_Consent_Clinical,
            CCL_Scheduler_dates_unavailable_message,
            CCL_Select_Alternate_Scheduler_Dates_Popup_Message,
            CCL_Choose_Alternative_Date_Button,
            CCL_Accept_New_Dates_Button,
            CCL_PRF_Submission_Error,
            ClinicalSupportReview,
            CCL_Replacement_Order,
            NotAvailable,
            ToBeConfirmed,
            patientCountryLabel,
            cCL_Patient_Consents,
            billingPartyLabel,
            attestation,
			CCL_Patient_Intials
          };

Dataorder()
{
  let countPatient = this.patientCountry;
  let billParty = this.billingParty;

  if(this.patientCountry == 'none' || !this.patientCountry) {
    countPatient = '';
  }
  if(this.billingParty == 'none' || !this.billingParty) {
    billParty = '';
  }

  this.orderData=
  {"CCL_First_Name__c":this.CCL_firstname,
    "CCL_Last_Name__c":this.CCL_lastname,
    "CCL_Middle_Name__c":this.CCL_middleinitial,
    "CCL_Suffix__c":this.CCL_suffix,
    "CCL_Ordering_Hospital__c":this.OrderingSiteSFDCID,
    "CCL_Therapy__c":this.TherapySFDCID,
    "CCL_Date_of_DOB__c":this.CCL_inpdate,
	"CCL_Patient_Initials__c":this.CCL_patientInitials,
	"CCL_Patient_Id__c":this.patId,
    "CCL_Month_of_DOB__c":this.CCL_inpmonth,
    "CCL_Year_of_DOB__c":this.CCL_inpyear,
    "CCL_Hospital_Patient_ID__c":this.hospitalPatientId,
    "CCL_Patient_Weight__c":this.CCL_patientweight,
    "CCL_Patient_Age_At_Order__c":this.CCL_patientage,
    "CCL_Treatment_Protocol_Subject_ID__c":this.therapyIdName+"_"+this.CCL_centernum+"_"+this.CCL_subjectnum,
    "CCL_Protocol_Center_Number__c":this.CCL_centernum,
    "CCL_Protocol_Subject_Number__c":this.CCL_subjectnum,
    "CCL_Consent_for_Additional_Research__c":this.CCL_patientCon,
    "CCL_Apheresis_Collection_Center__c":this.collectionCenterId,
   "CCL_Apheresis_Pickup_Location__c":this.aphPickupLocationId,
   "CCL_Infusion_Center__c":this.infusionCenterId,
   "CCL_PRF_Ordering_Status__c":"PRF_Submitted",
   "CCL_Ship_to_Location__c":this.shipToLocId,
   "CCL_Hospital_Purchase_Order_Number__c":this.hospitalPurchaseOrderOptInFlow,
   "CCL_Patient_Eligibility__c":this.isClinicalChecked,
   "CCL_Physician_Attestation__c":this.isCommercialChecked,
   "CCL_VA_Patient__c":this.paymentCheckbox,
   "CCL_Prescriber__c":this.contactidforprescriber,
   "CCL_Scheduler_Missing__c":this.missingResponse,
   "CCL_Planned_Apheresis_Pick_up_Date_Time__c":this.plannedPickupDate,
   "CCL_Estimated_FP_Delivery_Date__c": this.productFinishedDatesecond,
   "CCL_Estimated_Manufacturing_Start_Date__c":this.manufacturingStartDatesecond,
   "CCL_Plant__c":this.plantId,
   "CCL_Value_Stream__c":this.valueStreamField,
   "CCL_Manufacturing_Slot_ID__c":this.slotId,
    "CCL_Returning_Patient__c":this.allowreplacementordertrue,
    "CCL_Patient__c":this.PatientRecordId,
    "CCL_Physician_Attestation_Text__c":this.commercialPhysicianAttestation,
    "CCL_Patient_Eligibility_Text__c":this.patientEligibilityText,
    "TherapyType":this.therapyType,
    "CCL_Order_Approval_Eligibility__c": this.approvalCheckboxFlow,
    "CCL_Patient_Country_Residence__c":countPatient,
    "CCL_Billing_Party__c": billParty,
    "CCL_Day_of_Birth_COI__c":this.optedIn.CCL_Day_of_Birth__c,
    "CCL_FirstName_COI__c":this.optedIn.CCL_First_Name__c,
    "CCL_Initials_COI__c":this.optedIn.CCL_Initials__c,
    "CCL_LastName_COI__c":this.optedIn.CCL_Last_Name__c,
    "CCL_MiddleName_COI__c":this.optedIn.CCL_Middle_Initial__c,
    "CCL_Month_of_Birth_COI__c":this.optedIn.CCL_Month_of_Birth__c,
    "CCL_Secondary_COI_Patient_ID__c":this.optedIn.CCL_Secondary_COI_Patient_ID__c,
    "CCL_Sufix_COI__c":this.optedIn.CCL_Suffix__c,
    "CCL_Year_of_Birth_COI__c":this.optedIn.CCL_Year_of_Birth__c,
    "CCL_TECH_Hos_Patient_ID_Email_Opt_in__c": this.hospatIdEmailOptIn,
    "CCL_TECH_Hospital_Patient_ID_Opt_in__c": this.hospatIdIn,
	"coiDataChanged":this.coiDataChanged
  }
}

  connectedCallback() {
	  if(this.fileUploadDoc && JSON.parse(this.fileUploadDoc).length>0) {
		  this.filePresent=true;
      this.filesUploaded = JSON.parse(this.fileUploadDoc);
    for (let i=0; i< Object.keys(JSON.parse(this.fileUploadDoc)).length; i++){
      this.fileIdList[i] = this.filesUploaded[i].documentId;
    }
  } else{
    checkPRFApprover()
      .then(result => {
        this.prfApprover = result;
        if(this.prfApprover){
          getcustomdoc({recordId : this.orderid})
          .then(result =>{
      this.orderDocId = result[0].Id;
      getConsentFile({recordId:this.orderDocId})
      .then(result=>{
    this.filesUploaded = result;
	if (this.filesUploaded.length > 0) {
      this.filePresent=true;
    }
      }).catch (error=> {
        this.error = error;
      })
          }).catch(error => {
            this.error = error;
          })
        }
      })
      .catch(error => {
        this.error = error;
      });
  }
	  if(this.therapyType=="Commercial") {
    this.subjectId = null;
	} else if(this.therapyType=="Clinical") {
    this.subjectId = this.CCL_centernum+"_"+this.CCL_subjectnum;
	}
	if(!this.manufacturingStartDateFlow || !this.productFinishedDateFlow){
		this.replacementOrderDates=true;
	}
	this.therapyName = this.therapy;
	if(this.therapy != null){
	 this.showhideReviewSections()
	}
   this.month = this.CCL_inpmonth;
   if(this.CCL_patientCon) {
     this.setRadioBtnYes = 'Yes';
   }else if(this.CCL_patientCon == false) {
     this.setRadioBtnYes = 'No';
   }else {
     this.setRadioBtnYes = false;
   }
   if(this.billingPartyVisible=='Yes') {
       this.billingPartyOptIn=true;
   }
   if(this.hospatIdEmailOpt=='Yes') {
    this.hospatIdEmailOptIn=true;
    }

    if(this.hospatIdStr=='Yes') {
      this.hospatIdIn=true;
    }
   if(this.capturePatientCountry=='Yes') {
       this.patientCountryOptIn=true;
   }
   if (this.splitSubject) {
    this.splitSubjectId(this.splitSubject);
  }
  this.addressIds.push(this.collectionCenterId);
  this.addressIds.push(this.aphPickupLocationId);
  this.addressIds.push(this.infusionCenterId);
  this.addressIds.push(this.shipToLocId);
  if(this.addressIds.length>0) {
    this.getAddresses();
   }

   checkCustOpsPermission()
      .then(result => {
        this.custOpsLogin = result;
      })
      .catch(error => {
        this.error = error;
      });
}

handleCallout() {
 if ((this.therapyType == 'Commercial' && !this.allowreplacementordertrue) || this.therapyType == 'Clinical') {
  this.loaded = true;
  this.calendarField = true;
  sendCallout({
    therapyId: this.TherapySFDCID,
    aphPickupLocation: this.aphPickupLocationId,
    shipToLoc: this.shipToLocId,
    subjectId: this.subjectId
  })
    .then((result) => {
      if (result) {
        this.responseJson = JSON.parse(result);
        if (this.responseJson.Status == "OK") {
          this.loaded = false;
          this.qualifiedSlots = this.responseJson.QualifiedSlots;
          this.qualifiedSlotsFlow = JSON.stringify(this.qualifiedSlots);
          this.plantId = this.qualifiedSlots[0].Manufacturing_Plant;
          this.slotId = this.qualifiedSlots[0].SlotID;
          this.valueStreamField = this.qualifiedSlots[0].ValueStream;
          const aphStartDate = this.qualifiedSlots[0].Day_0_Date;
          let newStartDate = aphStartDate.split("-").reverse().join("-");
          this.manufacturingStartDatesecond = this.formatDate(newStartDate);
          this.manufacturingStartDatesecondFlow = this.manufacturingStartDatesecond;
          const finishDate = this.qualifiedSlots[0].APHPickup_Date;
          let newFinishedDate = finishDate.split('-').reverse().join('-');
          const formattedFinshedDate = new Date(newFinishedDate);
          let currentDate = new Date(
            formattedFinshedDate.getFullYear(),
            formattedFinshedDate.getMonth(),
            formattedFinshedDate.getDate() +
            parseInt(this.qualifiedSlots[0].ValueStream,10)
          );
          const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
          let endDate =
            currentDate.getFullYear() +
            "/" +
            month +
            "/" +
            currentDate.getDate();
          this.productFinishedDatesecond = this.formatDate(endDate);
          this.productFinishedDatesecondFlow = this.productFinishedDatesecond;
          this.availableDates = this.qualifiedSlots[0].APHPickup_Date.replace(
            /-|\//g,
            "/"
          );
          this.availableSlot = this.availableDates;
          let tempDateArray = [];
          this.qualifiedSlots.forEach(function (node) {
            tempDateArray.push(node.APHPickup_Date.replace(/-|\//g, "/"));
          });
          this.availableDateArray = tempDateArray;
          this.availableDateArrayFlow = tempDateArray.toString();
         const flowMonth = this.plannedPickupDateFlow.split(" ");
         const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
         const monthIndex = monthNames.indexOf(flowMonth[1]) + 1;
         const formattedDate = ("0" + (flowMonth[0])).slice(-2);
         const formattedMonth = ("0" + (monthIndex)).slice(-2);
         this.aphPickupDate = formattedDate + '/' + formattedMonth + '/' + flowMonth[2];
          if (this.availableDateArray.includes(this.aphPickupDate)) {
            this.missingResponse = false;
            this.showDynamicTextTemp = true;
            let self = this;
            this.qualifiedSlots.forEach(function (node) {
              if (node.APHPickup_Date.replace(/-|\//g, "/") === self.aphPickupDate) {
                  self.plantId = node.Manufacturing_Plant;
                  self.slotId = node.SlotID;
                  self.valueStreamField = node.ValueStream;
                let startDate = node.Day_0_Date;
                let newStartDate = startDate.split('-').reverse().join('-');
                const formattedStartDate = new Date(newStartDate);

                // used to pass in JSON to insert in SF
                self.manufacturingDate = formattedStartDate;
                let formattedDate = newStartDate.replace(/-|\//g, "/")
                self.manufacturingStartDatesecond = self.formatDate(formattedDate);

                // self.manufacturingStartDateFlow used in flow for review screen
                self.manufacturingStartDatesecondFlow = self.manufacturingStartDatesecond;
                const event = new Date(self.manufacturingStartDatesecond+'GMT');
                self.manufacturingStartDatesecond = event.toISOString();
                const finishDate = node.APHPickup_Date;

                let newFinishedDate = finishDate.split('-').reverse().join('-');
                const formattedFinshedDate = new Date(newFinishedDate);

                // used to pass in JSON to insert in SF

                formattedFinshedDate.setDate(formattedFinshedDate.getDate() + parseInt(node.ValueStream,10));
                self.FinishedDate = formattedFinshedDate;
                const month = ("0" + (formattedFinshedDate.getMonth() + 1)).slice(-2);
                let endDate = formattedFinshedDate.getFullYear() + '/' + month + '/' + formattedFinshedDate.getDate();
                self.productFinishedDatesecond = self.formatDate(endDate);

                // self.productFinishedDateFlow used in flow for review screen
                self.productFinishedDatesecondFlow = self.productFinishedDatesecond;
                const deliveryEvent = new Date(self.productFinishedDatesecond+'GMT');
                self.productFinishedDatesecond = deliveryEvent.toISOString();
              }
            });
          }
          else {
            this.loaded = false;
            this.interfaceFailure();
          }
		}
          else {
            this.loaded = false;
            this.availableSlot = null;
            this.availableDateArrayFlow = null;
            this.interfaceFailure();
          }
        }
        else {
          this.loaded = false;
          this.availableSlot = null;
          this.availableDateArrayFlow = null;
          this.interfaceFailure();
        }
      this.openModal();
    })
    .catch((error) => {
     this.error = error;
      this.loaded = false;
        this.availableSlot = null;
        this.availableDateArrayFlow = null;
      this.interfaceFailure();
      this.openModal();
    });
	} else {
    this.openModal();
  }

}

interfaceFailure(){
      if (this.therapyType == 'Commercial' && !this.allowreplacementordertrue) {
        this.manufacturingStartDate = '';
        this.showDynamicTextTemp = false;
        this.productFinishedDate = '';
        this.availableDateArray = null;
        this.availableDates= null;
        this.selectedDate = null;
        this.missingResponse = true;
        this.manufacturingStartDatesecondFlow = ToBeConfirmed;
        this.productFinishedDatesecondFlow = ToBeConfirmed;
        this.productFinishedDatesecond = undefined;
        this.manufacturingStartDatesecond = undefined;
        this.plantId = null;
        this.slotId = null;
        this.valueStreamField = undefined;
      } else if (this.therapyType == 'Clinical') {
        this.showDynamicTextTemp = false;
        this.manufacturingStartDate = '';
        this.productFinishedDate = '';
        this.availableDateArray = null;
        this.selectedDate = null;
        this.missingResponse = true;
        this.availableDates= null;
        this.manufacturingStartDatesecondFlow = NotAvailable;
        this.productFinishedDatesecondFlow = NotAvailable;
		this.productFinishedDatesecond = undefined;
        this.manufacturingStartDatesecond = undefined;
        this.plantId = null;
        this.slotId = null;
        this.valueStreamField = undefined;
    }
}

  // show review screen based on therapy type selected on screen 1.
 showhideReviewSections(){
 if(this.therapyType=="Commercial"){
   this.isCommercial=true;
  }else if(this.therapyType=="Clinical"){
    this.isClinical=true;
  }
 }
  editPatientDetails(){
    this.editScreenVar = 'Patient' + this.therapyType;
    const attributeChangeEvent = new FlowAttributeChangeEvent('editScreenVar', this.editScreenVar);
    this.dispatchEvent(attributeChangeEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }

  editPickupDetails(){
	this.isModalOpen = false;
    this.previousButtonClick=true;
    this.manufacturingStartDateFlow = this.manufacturingStartDatesecondFlow;
    this.productFinishedDateFlow = this.productFinishedDatesecondFlow;
    this.showDynamicText = this.showDynamicTextTemp;
    this.editScreenVar = 'Pickup' + this.therapyType;
    const attributeChangeEvent = new FlowAttributeChangeEvent('editScreenVar', this.editScreenVar);
    this.dispatchEvent(attributeChangeEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }

  editPaymentDetails(){
    this.previousButtonClick=true;
    this.editScreenVar = 'Payment' + this.therapyType;
    const attributeChangeEvent = new FlowAttributeChangeEvent('editScreenVar', this.editScreenVar);
    this.dispatchEvent(attributeChangeEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }

  onPrevious(){
    if(this.therapyType=='Commercial'){
      this.editScreenVar = 'Review_Back_'+ this.therapyType;
    }
    if(this.therapyType=='Clinical'){
      this.editScreenVar = 'Review_Back_'+ this.therapyType;
    }
    const attributeChangeEvent = new FlowAttributeChangeEvent('editScreenVar', this.editScreenVar);
    this.dispatchEvent(attributeChangeEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }
  handleUploadFinished(event) {
   this.filesUploaded =[...this.filesUploaded,...event.detail.files];
  }

viewDocument(event) {
  this.url = "/sfc/servlet.shepherd/document/download/" + event.target.name;
  window.open(this.url);
}

//This will be called on click of Submit button
onSubmit(){
  this.manufacturingStartDateFlow = this.manufacturingStartDatesecondFlow;
  this.productFinishedDateFlow = this.productFinishedDatesecondFlow;
  this.showDynamicText = this.showDynamicTextTemp;
  if(this.therapyType=='Commercial')
    {
	  this.isModalOpen = false;
      this.isCommercialModalOpen = true;
      this.isCommercialChecked=false;
    }
    if(this.therapyType=='Clinical')
    {
	  this.isModalOpen = false;
      this.isClinicalModalOpen = true;
      this.isClinicalChecked=false;
    }
  }
 goToNextCommercialScreen() {
   if(this.isCommercialChecked)
   {
       this.isCommercialModalOpen = false;
       this.loaded = true;
	   this.Dataorder();
	   submitPrfForm({ result: this.orderData })
       .then((result) => {
         if(result)
         {
			 createOrderDocument({docId : this.orderDocId})
          .then((result) => {
            if(result){
              updateContentVersion({contentDocIdList : this.fileIdList})
              .then((result) => {
              })
              .catch((error) =>{

              })
            }
          })
          .catch((error) =>{
          })
          this.loaded = false;
          fireEvent(this.pageRef, 'orderConfirmation', 'orderConfirmation');
          this.editScreenVar = 'orderConfirmation_'+ this.therapyType;
          const attributeChangeEvent = new FlowAttributeChangeEvent('editScreenVar', this.editScreenVar);
          this.dispatchEvent(attributeChangeEvent);
          const navigateNextEvent = new FlowNavigationNextEvent();
          this.dispatchEvent(navigateNextEvent);
         }
         })
       .catch((error) => {
         this.message = "";
         this.error = error;
         this.errorModal=true;
         this.loaded = false;
       });
    }
    else if(!this.isCommercialChecked)
    {
        this.isCommercialModalOpen = true;
    }
   }
   onSubmitError()
  {
	this.errorModal = false;
    this.Dataorder();
	this.loaded = true;
    submitPrfForm({ result: this.orderData })
    .then((result) => {
      if(result==true)
      {
      this.errorModal = false;
      this.loaded = false;
      fireEvent(this.pageRef, 'orderConfirmation', 'orderConfirmation');
      const navigateNextEvent = new FlowNavigationNextEvent();
      this.dispatchEvent(navigateNextEvent);
      }
     })
    .catch((error) => {
      this.message = "";
      this.error = error;
      this.errorModal = true;
      this.loaded = false;
     });

}

openModal() {
  this.loaded = false;
    if ((this.manufacturingStartDateFlow !==this.manufacturingStartDatesecondFlow) || (this.productFinishedDateFlow !==this.productFinishedDatesecondFlow ))
         {
          this.isModalOpen = true;
        }
    else{
          this.isModalOpen = false;
          if(this.therapyType=='Commercial')
          {
            this.isCommercialModalOpen = true;
            this.isCommercialChecked=false;
          }
          if(this.therapyType=='Clinical')
          {
            this.isClinicalModalOpen = true;
            this.isClinicalChecked=false;
          }
        }
      }

  returnToReview()
  {
    this.isCommercialModalOpen = false;
    this.isClinicalModalOpen = false;
   }
  onCommercialConsent(event) {
       this.isCommercialChecked=event.target.checked;
     }
   goToNextClinicalScreen() {
     if(this.isClinicalChecked==true)
      {
        this.isClinicalModalOpen = false;
        this.loaded = true;
        this.Dataorder();
		submitPrfForm({ result: this.orderData })
        .then((result) => {
        if(result)
          {
            this.loaded = false;
            fireEvent(this.pageRef, 'orderConfirmation', 'orderConfirmation');
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
			createOrderDocument({docId : this.orderDocId})
            .then((result) => {
              if(result){
                updateContentVersion({contentDocIdList : this.fileIdList})
                .then((result) => {
                })
                .catch((error) =>{

                })
              }
            })
            .catch((error) =>{
            })
           }
           })
          .catch((error) => {
            this.message = "";
            this.error = error;
            this.errorModal=true;
            this.loaded = false;
          });

          }
      else if(!this.isClinicalChecked)
      {
         this.isClinicalModalOpen = true;
      }
      }
  onClinicalConsent(event) {
    this.isClinicalChecked=event.target.checked;
     }
  closeModal() {
	   this.isModalOpen = false;
       this.isCommercialModalOpen = false;
       this.isClinicalModalOpen = false;
       this.errorModal = false;
	   this.loaded = false;
   }
  @wire(getPickAndDeliveryDetails,
{
  collectionCenterId: "$collectionCenterId",
  aphPickupLocationId: "$aphPickupLocationId",
  infusionCenterId: "$infusionCenterId",
  shipToLocId: "$shipToLocId"
})
AphPickupAndDelivery({ error, data }) {
if (data) {
  this.aphPickupAndDeliveryData = data;
  let self = this;
  this.aphPickupAndDeliveryData.forEach(function (node) {
    if (node.Id === self.collectionCenterId) {
      self.collCenterAdd = node.ShippingAddress;
      self.collectionCenter = node.Name;
    }
    if (node.Id === self.aphPickupLocationId) {
        self.aphPickupLocAdd = node.ShippingAddress;
        self.aphPickupLocation = node.Name;
      }
      if (node.Id === self.infusionCenterId) {
        self.infCenterAdd = node.ShippingAddress;
        self.infusionCenter = node.Name;
      }
      if (node.Id === self.shipToLocId) {
        self.shiToLocAdd = node.ShippingAddress;
        self.shipToLoc = node.Name;
      }
  });
}
else if (error) {
  this.error = error;
}
}

splitSubjectId(subjectId) {
  if (subjectId) {
    let strLst = subjectId.split("_");
    if (strLst.length > 2) {
      this.therapyIdName = strLst[0];
      this.CCL_centernum = strLst[1];
      this.CCL_subjectnum = strLst[2];
    }
  }
}

formatDate(date) {
  let mydate = new Date(date);
  let monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];

  let day = mydate.getDate();

  let monthIndex = mydate.getMonth();
  let monthName = monthNames[monthIndex];
  let year = mydate.getFullYear();
  return `${day} ${monthName} ${year}`;
}
getAddresses(){
  getAddress({
    accountIds: this.addressIds,
  })
    .then((result) => {
      this.addressList=result;
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

}
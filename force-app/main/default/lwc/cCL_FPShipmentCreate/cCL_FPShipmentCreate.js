import { LightningElement, track, api, wire } from "lwc";

import { getRecord } from 'lightning/uiRecordApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getSiteWrapper from "@salesforce/apex/CCL_PRF_Controller.getSiteWrapper";
import getShipToLocation from "@salesforce/apex/CCL_PRF_Controller.getShipToLocation";
import InfusionCenter from "@salesforce/label/c.CCL_Infusion_Center";
import InfusionAddress from "@salesforce/label/c.CCL_Infusion_Address";
import ShipToLocation from "@salesforce/label/c.CCL_Ship_To_Location";
import ShipToAddress from "@salesforce/label/c.CCL_Ship_To_Address";
import EstimateShipmentDateInPast from "@salesforce/label/c.CCL_Estimated_Shipment_Date_Cannot_Be_Past";
import CompleteThisField from "@salesforce/label/c.CCL_Complete_This_Field";
import EstimatedShipmentDateHelp from "@salesforce/label/c.CCL_Estimated_Shipment_Date_Help";
import SelectShipmentReason from "@salesforce/label/c.CCL_Select_Shipment_Reason";
import SelectShipToLocation from "@salesforce/label/c.CCL_Select_Ship_To_Location";
import SelectInfusionCenter from "@salesforce/label/c.CCL_Select_Infusion_Center";
import CreateFinishedProductShipment from "@salesforce/label/c.CCL_Create_Subsequent_FP_Shipment";
import Saving from "@salesforce/label/c.CCL_Saving";
import Submit from "@salesforce/label/c.CCL_Submit_Button";
import Cancel from "@salesforce/label/c.CCL_Cancel_Button";
import Success from "@salesforce/label/c.CCL_Success";
import FinishedProductCreated from "@salesforce/label/c.CCL_Finished_Product_Created";
import NoneAvailable from "@salesforce/label/c.CCL_None_Available";

import SHIPMENT_OBJECT from '@salesforce/schema/CCL_Shipment__c';
import SHIPMENT_REASON_FIELD from '@salesforce/schema/CCL_Shipment__c.CCL_Shipment_Reason__c';
import THERAPY_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Therapy__c';
import INFUSION_CENTRE_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Infusion_Center__c';
import SHIP_TO_LOCATION_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Ship_to_Location__c';
import ORDERING_HOSPITAL_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Ordering_Hospital__c';
import HOSP_PURCHASE_ORDER_NUM_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Hospital_Purchase_Order_Number__c';
import TIME_ZONE_NAME_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Plant__r.CCL_Time_Zone__r.Name';
import PLANT_ID_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Plant__c';
import TIMEZONE_ID_FIELD from '@salesforce/schema/Account.CCL_Time_Zone__c';
import TIMEZONE_NAME_FIELD from '@salesforce/schema/CCL_Time_Zone__c.Name';
import NOVARTIS_BATCH_ID_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Novartis_Batch_ID__c';
import DATE_OF_BIRTH_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Date_Of_Birth_Text__c';
import PROTOCOL_SUBJECT_HOSPITAL_PATIENT_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Protocol_Subject_Hospital_Patient__c';
import PATIENT_NAME_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Patient_Name_Text__c';
import ESTIMATE_FP_DELIVERY_DATE_TEXT_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Estimated_FP_Delivery_Date_Text__c';
import ESTIMATE_FP_DELIVERY_DATE from '@salesforce/schema/CCL_Order__c.CCL_Estimated_FP_Delivery_Date__c';

const FIELDS = [
  ESTIMATE_FP_DELIVERY_DATE,
    THERAPY_FIELD,
    INFUSION_CENTRE_FIELD,
    SHIP_TO_LOCATION_FIELD,
    ORDERING_HOSPITAL_FIELD,
    HOSP_PURCHASE_ORDER_NUM_FIELD,
    TIME_ZONE_NAME_FIELD,
    PLANT_ID_FIELD,
    NOVARTIS_BATCH_ID_FIELD,
    PATIENT_NAME_FIELD,
    DATE_OF_BIRTH_FIELD,
    PROTOCOL_SUBJECT_HOSPITAL_PATIENT_FIELD,
    ESTIMATE_FP_DELIVERY_DATE_TEXT_FIELD
  ];


export default class CCL_FPShipmentCreate extends LightningElement {

    @track infCenterData;
    @track shipToLocData = null;
    @track showErrorModal;
    @track showInfusionRequiredMessage;
    @track showShipToRequiredMessage;
    @track showShipmentReasonRequiredMessage;
    @track showShipmentDateInPastMessage;
    @track errorMessage;
    @track timeZone;
    @track fpShipmentRecordTypeId;
    @track shipmentReasonPicklistValues;
    @track plantId;
    @track timezoneId;
    @track timezoneName;
    @track estimatedFPShipDateLabel;
    @track patientName; 
    @track dateOfBirth;
    @track protocolSubjectHospitalPatient; 
    @track orderApheresis;
    @track estimateFPDeliveryDate;

    @api isMultipleInfusionCentres; //Boolean to inidicate multiple infusion centres. Used to determine whether to pre-select infusion centre picklist. Previously called isMultipleInfusionCentres
    @api isMultipleShipToRecords; //Boolean to inidicate multiple shipToLoc records. Used to determine whether to pre-select Ship to Location picklist. Previously called isMultipleShipToRecords
    // @api infusionCenter = null;
    @api infusionCenterId;
    @api shipToLocId;
    @api defaultShipToLocId;
    @api hasShipToLocAddress;
    @api shipToLocSiteAddress;
    @api hasInfAddress;
    @api infSiteAddress;
    @api therapy;
    @api hospital;
    @api recordId;
    @api record;
    @api shipToSelectDisabled;
    @api showSavingSpinner;
    @api hospitalPONumberFromOrder;
    @api shipmentReason;
    @api noInfusionCentresAvailable;
    @api noShipToLocsAvailable;
    @api orderNovartisBatchId;
    
    label = {
      InfusionCenter,
      InfusionAddress,
      ShipToLocation,
      ShipToAddress,
      EstimateShipmentDateInPast,
      CompleteThisField,
      EstimatedShipmentDateHelp,
      SelectShipmentReason,
      SelectShipToLocation,
      SelectInfusionCenter,
      CreateFinishedProductShipment,
      Saving,
      Submit,
      Cancel,
      Success,
      FinishedProductCreated,
      NoneAvailable
    };    

    // Initial query to get the Therapy and Ordering Hospital from the Order
    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    queryOrderData({ error, data }) {
      if (data && !this.record) {
        this.record = data;
        if (this.record.fields.CCL_Therapy__c != null) {
          this.therapy = this.record.fields.CCL_Therapy__c.value;
        }
        if (this.record.fields.CCL_Ordering_Hospital__c != null) {
          this.hospital = this.record.fields.CCL_Ordering_Hospital__c.value;
        }
        if (this.record.fields.CCL_Infusion_Center__c != null) {
          this.infusionCenterId = this.record.fields.CCL_Infusion_Center__c.value;
          // this.infusionCenter = '';
        }
        if (this.record.fields.CCL_Ship_to_Location__c != null) {
          this.defaultShipToLocId = this.shipToLocId = this.record.fields.CCL_Ship_to_Location__c.value;
        }
        if (this.record.fields.CCL_Hospital_Purchase_Order_Number__c != null) {
          this.hospitalPONumberFromOrder = this.record.fields.CCL_Hospital_Purchase_Order_Number__c.value;
        }
        if (this.record.fields.CCL_Plant__c != null && 
            this.record.fields.CCL_Plant__r.CCL_Time_Zone__c != null && 
            this.record.fields.CCL_Plant__r.CCL_Time_Zone__r.Name != null) {
              
          this.timeZone = this.record.fields.CCL_Plant__r.CCL_Time_Zone__r.Name.value;
        }
        if (this.record.fields.CCL_Plant__c != null) {
          this.plantId = this.record.fields.CCL_Plant__c.value;
        }
        if(this.record.fields.CCL_Novartis_Batch_ID__c != null){
          this.orderNovartisBatchId=this.record.fields.CCL_Novartis_Batch_ID__c;
        }
        if (this.record.fields.CCL_Patient_Name_Text__c != null) {
            this.patientName = this.record.fields.CCL_Patient_Name_Text__c.value;
        }
        if (this.record.fields.CCL_Date_Of_Birth_Text__c != null) {
            this.dateOfBirth = this.record.fields.CCL_Date_Of_Birth_Text__c.value;
        }
        if (this.record.fields.CCL_Protocol_Subject_Hospital_Patient__c != null) {
            this.protocolSubjectHospitalPatient = this.record.fields.CCL_Protocol_Subject_Hospital_Patient__c.value;
        }
        if (this.record.fields.CCL_Estimated_FP_Delivery_Date__c != null) {
          this.estimateFPDeliveryDate = this.record.fields.CCL_Estimated_FP_Delivery_Date__c.value;
      }
        this.shipToSelectDisabled = true;
        this.showSavingSpinner = false;
        this.showErrorModal = false;
        this.showInfusionRequiredMessage = false;
        this.showShipToRequiredMessage = false;
        this.showShipmentReasonRequiredMessage = false;
        this.showShipmentDateInPastMessage = false;
        this.hasInfAddress = false;
        this.hasShipToLocAddress = false;
        this.noInfusionCentresAvailable = false;
        this.noShipToLocsAvailable = false;
        this.shipmentReason = '';
        
        } else if (error) {
          
          
          this.errorMessage = error;
          this.showErrorModal = true;
        }
    }

    @wire(getRecord, { recordId: '$plantId', fields: [TIMEZONE_ID_FIELD] })
      queryTimezoneId({ error, data }) {
        if (data && !this.timezoneId) {
          if (data.fields.CCL_Time_Zone__c != null) {
            this.timezoneId = data.fields.CCL_Time_Zone__c.value;
          }
        }
    }

    @wire(getRecord, { recordId: '$timezoneId', fields: [TIMEZONE_NAME_FIELD] })
      queryTimezoneName({ error, data }) {
        if (data && !this.timezoneName) {
          if (data.fields.Name != null) {
            this.timezoneName = ' (' + data.fields.Name.value + ')';
          }
        }
    }    

    // Query to get the Infusion centres for the hospital and Therapy combination
    @wire(getSiteWrapper, { therapyId: "$therapy", hospital: "$hospital" })
    queryInfusionCentres({ error, data }) {
      if (data) {
        this.wrapper = data;
        this.wrapper = JSON.parse(this.wrapper);
        let self = this;
        this.wrapper.forEach(function (node) {
          self.infCenterData = node.infusionCenter;
        });

       

        if (this.infCenterData.length > 1) {
          this.isMultipleInfusionCentres = true;
          let tempInfCentreDataArray = [];
          let defaultInfusionCentreFound = false;
          // duplicateCheckArray checks for duplicate infusion centre assoications. Commenting out, because it is not necessary if master data correctly set up
          // i.e. correct Site and Therapy Associations
          // let duplicateCheckArray = [];
          
          

          this.infCenterData.forEach(function (node) {

            

            if (!defaultInfusionCentreFound && node.CCL_Site__c === self.infusionCenterId) {
              tempInfCentreDataArray.unshift(node);
              // duplicateCheckArray.push(node.CCL_Site__c);
              self.isMultipleInfusionCentres = false;
              defaultInfusionCentreFound = true;
              if (node.CCL_Site__r.ShippingAddress !== null) {
                self.hasInfAddress = true;
                self.infSiteAddress = node.CCL_Site__r.ShippingAddress;
              }
            } else {
              // if (!duplicateCheckArray.includes(node.CCL_Site__c)) {
                tempInfCentreDataArray.push(node);
                // duplicateCheckArray.push(node.CCL_Site__c);
              // }
            }
          });

          this.infCenterData = tempInfCentreDataArray;

          if (defaultInfusionCentreFound) {
            this.loadShipToLocations();
          }

        } else if (this.infCenterData.length > 0) {
          this.isMultipleInfusionCentres = false;
          this.hasInfAddress = true;
          if (this.infCenterData[0].CCL_Site__r.ShippingAddress !== null) {
            this.infSiteAddress = this.infCenterData[0].CCL_Site__r.ShippingAddress;
          }
          // this.infusionCenter = this.infCenterData[0].CCL_Site__r.Name;
          // this.infusionCenter = this.infCenterData[0].CCL_Site__c;
          this.infusionCenterId = this.infCenterData[0].CCL_Site__c;

          this.loadShipToLocations();
        } else {
          this.noInfusionCentresAvailable = true;
        }
      } else if (error) {
        
        this.errorMessage = error;
        this.showErrorModal = true;
      }
    }

    @wire(getObjectInfo, { objectApiName: SHIPMENT_OBJECT })
    queryShipmentRecordTypeId({ error, data }) {
      if (data) {
        let shipmentObjectInfo = data.recordTypeInfos;
        this.fpShipmentRecordTypeId = Object.keys(shipmentObjectInfo).find(rtid => shipmentObjectInfo[rtid].name === 'Finished Product');
        if (this.fpShipmentRecordTypeId == null || this.fpShipmentRecordTypeId == '') {
          this.fpShipmentRecordTypeId = shipmentObjectInfo.data.defaultRecordTypeId;
        }
        this.estimatedFPShipDateLabel = data.fields.CCL_Estimated_FP_Shipment_Date_Time__c.label;
      } else if (error) {
         
          this.errorMessage = error;
          this.showErrorModal = true;
      }
    }

    @wire(getPicklistValues, { recordTypeId: "$fpShipmentRecordTypeId", fieldApiName: SHIPMENT_REASON_FIELD })
    retrieveShipmentReasonPicklistValues({ error, data }) {
      if (data) {
        let tempPicklistValueArray = [];
        this.shipmentReasonPicklistValues = data.values;
        this.shipmentReasonPicklistValues.forEach(function (picklistValue) {
          if (picklistValue.value != "IN") {
            tempPicklistValueArray.push(picklistValue);
          }
        });
        this.shipmentReasonPicklistValues = tempPicklistValueArray;
      } else if (error) {
       
        this.errorMessage = error;
        this.showErrorModal = true;
      }
    }

    loadShipToLocations() {

      

      getShipToLocation({ therapyId: this.therapy, infusionCenter: this.infusionCenterId })
          .then(result => {
              let self = this;
              this.shipToLocData = result;

             
              
              if (this.shipToLocData != null && this.shipToLocData.length > 0) {
                this.noShipToLocsAvailable = false;
                this.shipToSelectDisabled = false;
                if (this.shipToLocData.length > 1) {
                  this.isMultipleShipToRecords = true;
                  let tempShipToLocDataArray = [];
                  this.shipToLocData.forEach(function (node) {
                    if (node.CCL_Site__c === self.defaultShipToLocId) {
                      tempShipToLocDataArray.unshift(node);
                      self.isMultipleShipToRecords = false;

                      if (node.CCL_Site__r.ShippingAddress !== null) {
                        self.hasShipToLocAddress = true;
                        self.shipToLocSiteAddress = node.CCL_Site__r.ShippingAddress;
                      }
                    } else {
                      tempShipToLocDataArray.push(node);
                    }
                  });

                  this.shipToLocData = tempShipToLocDataArray;
                } else {
                  this.isMultipleShipToRecords = false;
                  this.hasShipToLocAddress = true;
                  if (this.shipToLocData[0].CCL_Site__r.ShippingAddress !== null) {
                    this.shipToLocSiteAddress = this.shipToLocData[0].CCL_Site__r.ShippingAddress;
                  }
                  //this.shipToLoc = this.shipToLocData[0].CCL_Site__r.Name;
                  this.shipToLocId = this.shipToLocData[0].CCL_Site__c;
                }
              } else {
                this.hasShipToLocAddress = false;
                this.noShipToLocsAvailable = true;
                this.isMultipleShipToRecords = false;
                
                // if (this.infusionCenter != null) {
               
                if (this.infusionCenterId != null) {
                  this.shipToSelectDisabled = false;
                }
              }
          })
          .catch(error => {
             
              this.errorMessage = error;
              this.showErrorModal = true;
          });
    }


   	// Called from onChange
    selectionChangeHandler(event) {
        let selectedValue = event.target.value;

       

        let datasetName = event.target.dataset.item;
        let self = this;
        if (datasetName == 'infCenter') {
          // this.infusionCenter = selectedValue;
          this.infusionCenterId = selectedValue;
          this.shipToLocData = null;
          this.hasShipToLocAddress = false;
          // this.shipToLoc = null;
          // if (this.infusionCenter == '') {
          if (this.infusionCenterId == '') {
            this.shipToSelectDisabled = true;
            this.hasInfAddress = false;
            this.hasShipToLocAddress = false;
            this.shipToLocData = null;
            // this.infusionCenter = null;
            // this.shipToLoc = null;
            this.shipToLocId = null;
            this.infusionCenterId = null;
          } else {
            this.showInfusionRequiredMessage = false;
          }
          this.infCenterData.forEach(function (node) {
            // if (node.CCL_Site__r.Name === self.infusionCenter && node.CCL_Site__r.ShippingAddress !== null) {
            if (node.CCL_Site__c === self.infusionCenterId && node.CCL_Site__r.ShippingAddress !== null) {
              self.hasInfAddress = true;
              self.infSiteAddress = node.CCL_Site__r.ShippingAddress;
              self.infusionCenterId = node.CCL_Site__c;
            }
          });
          this.loadShipToLocations();
        } else if (datasetName == 'shipToLoc') {
          //this.shipToLoc = selectedValue;
          this.shipToLocId = selectedValue;
          //if (this.shipToLoc == '') {
          if (this.shipToLocId == '') {
            this.hasShipToLocAddress = false;
            // this.shipToLoc = null;
            this.shipToLocId = null;
          } else {
            this.showShipToRequiredMessage = false;
          }
          this.shipToLocData.forEach(function (node) {
            // if (node.CCL_Site__r.Name === self.shipToLoc && node.CCL_Site__r.ShippingAddress !== null) {
            if (node.CCL_Site__c === self.shipToLocId && node.CCL_Site__r.ShippingAddress !== null) {
              self.hasShipToLocAddress = true;
              self.shipToLocSiteAddress = node.CCL_Site__r.ShippingAddress;
              self.shipToLocId = node.CCL_Site__c;
            }
          });
        }
      }

      shipmentReasonChangeHander(event) {
        this.shipmentReason = event.target.value;
        if (this.shipmentReason != null && this.shipmentReason != '') {
          this.showShipmentReasonRequiredMessage = false;
        } 
      }

      submitShipmentCreate(event) {
        event.preventDefault();
        let estimatedShipmentDateTime = event.detail.fields.CCL_Estimated_FP_Shipment_Date_Time__c;
        let todayDateTime = new Date();
      
        let dd = todayDateTime.getDate();
        let mm = todayDateTime.getMonth() + 1; //January is 0!
        let yyyy = todayDateTime.getFullYear();
        if(dd < 10){
            dd = '0' + dd;
        } 
        if(mm < 10){
            mm = '0' + mm;
        }
        
        let todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        if (estimatedShipmentDateTime < todayFormattedDate) {
          this.showShipmentDateInPastMessage = true;
        } else {
          this.showShipmentDateInPastMessage = false;
        }
        if (this.infusionCenterId == null || this.infusionCenterId == '') {
          this.showInfusionRequiredMessage = true;
        } 
        if (this.shipToLocId == null || this.shipToLocId == '') {
          this.showShipToRequiredMessage = true;
        } 
        if (this.shipmentReason == null || this.shipmentReason == '') {
          this.showShipmentReasonRequiredMessage = true;
        } 
        if (this.showShipmentDateInPastMessage == false
          &&
          this.showInfusionRequiredMessage == false
          && 
          this.showShipToRequiredMessage == false
          && 
          this.showShipmentReasonRequiredMessage == false) {
          this.showSavingSpinner = true;
          const fields = event.detail.fields;
          fields.CCL_Order__c = this.recordId;
          fields.CCL_Infusion_Center__c = this.infusionCenterId;
          fields.CCL_Novartis_Batch_Id__c=this.orderNovartisBatchId;
          fields.CCL_Ship_to_Location__c = this.shipToLocId;
          fields.CCL_Shipment_Reason__c = this.shipmentReason;
          fields.CCL_Indication_Clinical_Trial__c = this.therapy;
          fields.CCL_Protocol_Subject_Hospital_Patient__c = this.protocolSubjectHospitalPatient; 
          fields.CCL_Patient_Name__c = this.patientName;
          fields.CCL_Date_of_Birth__c = this.dateOfBirth;
          fields.CCL_Estimated_FP_Delivery_Date_Time__c = this.estimateFPDeliveryDate;
          fields.CCL_Ordering_Hospital__c = this.hospital;
          fields.RecordTypeId = this.fpShipmentRecordTypeId;
          
          this.template.querySelector('lightning-record-edit-form').submit(fields);
        }
      }

      handleShipmentCreateSuccess(event) {
        
        this.showSavingSpinner = false;
        this.showSuccessNotification();
        const closeModal = new CustomEvent("closemodal");
        this.dispatchEvent(closeModal);
      }

      handleShipmentCreateError(event) {
        
        this.errorMessage = event.detail.detail;
        this.showErrorModal = true;
      }

      cancelShipmentCreate(event) {
        const closeModal = new CustomEvent("closemodal");
        this.dispatchEvent(closeModal);
      }

      showSuccessNotification() {
        const evt = new ShowToastEvent({
            title: Success,
            message: FinishedProductCreated,
            variant: 'success',
            mode: 'sticky',
        });
        this.dispatchEvent(evt);
      }      
}
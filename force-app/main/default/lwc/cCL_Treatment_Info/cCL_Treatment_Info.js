import {LightningElement,api,track,wire} from 'lwc';
import {
    FlowAttributeChangeEvent,
    FlowNavigationNextEvent
} from 'lightning/flowSupport';
import {fireEvent} from 'c/cCL_Pubsub';
import {
    CurrentPageReference
} from 'lightning/navigation';
import fetchOrderingHospital from '@salesforce/apex/CCL_PRF_Controller.fetchOrderingHospital';
import fetchTherapyAssociation from '@salesforce/apex/CCL_PRF_Controller.fetchTherapyAssociation';
import fetchShipmentRecords from '@salesforce/apex/CCL_PRF_Controller.fetchShipmentRecords';
import getpatientfromorder from '@salesforce/apex/CCL_PRF_Controller.getpatientfromorder';
import fetchTherapy from '@salesforce/apex/CCL_PRF_Controller.fetchTherapy';
import OrderingHospitals from "@salesforce/label/c.CCL_Ordering_Hospital";
import RequestNewOrder from "@salesforce/label/c.CCL_Request_New_Order";
import TreatmentInfoHeader from "@salesforce/label/c.CCL_TreatmentInfo_Header";
import TherapyName from "@salesforce/label/c.CCL_Therapy_Name";
import TherapyDescriptions from "@salesforce/label/c.CCL_Therapy_Description";
import EnterReplacementOrder from "@salesforce/label/c.CCL_Enter_a_Replacement_Order";
import CCL_Mandatory_Field from "@salesforce/label/c.CCL_Mandatory_Field";
import CCL_Select_an_ordering_hospital from "@salesforce/label/c.CCL_Select_an_ordering_hospital";
import CCL_Select_Indication_Clinical_trial from "@salesforce/label/c.CCL_Select_Indication_Clinical_trial";
import CCL_See_full from "@salesforce/label/c.CCL_See_full";
import CCL_Prescribing_Information from "@salesforce/label/c.CCL_Prescribing_Information";

export default class CCLTreatmentInfo extends LightningElement {
    @api value = "";
    @api hospital;
    @api therapy;
    @api therapyId;
    @api therapyType;
    @track noTherapy = false;
    @track therapySelected = false;
    @api orderingHospital = [];
    @api orderingTherapy = [];
    @api orderingTherapyAssociation = [];
    @api recordLength;
    @api therapyRecordLength;
    @wire(CurrentPageReference) pageRef;
    @track error;
    @api hospitalId;
    @track mapData = [];
    @api therapyDescription;
    @api hasTherapyDescription;
    @api hospitalPatientId;
    @api hospitalPurchaseOrder;
    @api infusionDataCollection;
    @api researchWithBioSample;
    @api veteranAffiarFlag;
    @api hospitalExternalId;
    @api therapyExternalId;
    @api therapyType1;
    @api therapyAssociationId;
	@api CCL_patientInitial;
    @api hospitalCountryCode;
    @api minimumAge;
    @api maximumAge;
    @api commercialPhysicianAttestation;
    @api minimumWeight;
    @api maximumWeight;
    @api therapyName;
    @api patientEligibilityText;
    @api hospitalPurchaseOrderText;
    @api hospitalPurchaseOrderNumber;
    @api recordId;
    @api allowreplacementordertrue;
    @api shipmentdata = [];
    @api patientdata = [];
    @api CCL_centernum;
    @api CCL_subjectnum;
    @api CCL_firstname;
    @api CCL_middleinitial;
    @api CCL_lastname;
    @api CCL_suffix;
    @api CCL_patientweight;
    @api CCL_inpdate;
    @api CCL_inpmonth;
    @api CCL_inpyear;
    @api hospitalPatientIdFlow;
    @api PatientRecordId;
    @api orderinghospError;
    @api therapyError;
    @api billingPartyVisible;
    @api capturePatientCountry;
    @api applicableBillingParties;
    @api patId;
    @api hospatIdEmailOpt;
    @api hospatId;
    @api hospatIdStr;

    label = {
        OrderingHospitals,
        RequestNewOrder,
        TreatmentInfoHeader,
        TherapyName,
        TherapyDescriptions,
        EnterReplacementOrder,
        CCL_Mandatory_Field,
        CCL_Select_an_ordering_hospital,
        CCL_Select_Indication_Clinical_trial,
        CCL_See_full,
        CCL_Prescribing_Information
    };
    @wire(fetchOrderingHospital)
    wiredSteps({
        error,
        data
    }) {
        if (data) {
            this.getOrderingHospital(data);
        } else if (error) {
            this.error = error;
            this.orderingHospital = null;
        }
    }

    getOrderingHospital(data) {
        this.orderingHospital = data;
            this.error = null;
            if (this.orderingHospital.length > 1) {
                this.recordLength = true;
            } else if (this.orderingHospital.length == 1) {
                let self = this;
                this.orderingHospital.forEach(function(node) {
                    self.hospitalId = node.Id;
                    self.hospitalName = node.Name;
                    self.hospital = node.Name;
                    self.hospitalCountryCode = node.ShippingCountryCode;
                });
                fetchTherapy({
                        orderingHospital: this.hospitalId
                    })
                    .then(result => {
                        this.orderingTherapy = result;
                        if (this.orderingTherapy.length > 1) {
                            this.therapyRecordLength = true;
                            this.therapySelected = false;
                        } else if (this.orderingTherapy.length == 1) {
                            this.therapySelected = true;
                            this.therapyDescription = this.orderingTherapy[0].CCL_Description__c;
                            this.therapyType = this.orderingTherapy[0].CCL_Type__c;
                            this.therapyExternalId = this.orderingTherapy[0].CCL_External_ID__c;
                            this.therapy = this.orderingTherapy[0].CCL_Therapy_Description__c;
                            this.minimumWeight = this.orderingTherapy[0].CCL_Minimum_Weight__c;
                            this.maximumWeight = this.orderingTherapy[0].CCL_Maximum_Weight__c;
                            this.therapyName = this.orderingTherapy[0].Name;
                            this.patientEligibilityText = this.orderingTherapy[0].CCL_Clinical_Patient_Eligibility_Text__c;
                            if ((this.therapyDescription !== undefined) && (this.therapyDescription.length > 0)) {
                                this.hasTherapyDescription = true;
                            }
                            this.orderingTherapy.forEach(function(node) {
                                self.therapyId = node.Id;
                                self.therapy = node.CCL_Therapy_Description__c;
                                self.therapyName = node.Name;
                            });

                            fetchTherapyAssociation({
                                    orderingHospital: this.hospitalId,
                                    therapy: this.therapyId,
                                    countryCode: this.hospitalCountryCode
                                })
                                .then(result => {
                                    this.orderingTherapyAssociation = result;
                                    this.hospitalPatientId = this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Opt_In__c;
                                    this.hospitalPurchaseOrder = this.orderingTherapyAssociation[0].CCL_Hosp_Purchase_Order_Opt_In__c;
                                    this.infusionDataCollection = this.orderingTherapyAssociation[0].CCL_Infusion_Data_Collection__c;
                                    this.researchWithBioSample = this.orderingTherapyAssociation[0].CCL_Research_with_Bio_Samples_Opt_In__c;
                                    this.minimumAge = this.orderingTherapyAssociation[0].CCL_Minimum_Age__c;
                                    this.maximumAge = this.orderingTherapyAssociation[0].CCL_Maximum_Age__c;
                                    this.commercialPhysicianAttestation = this.orderingTherapyAssociation[0].CCL_Commercial_Physician_Attestation__c;
                                    this.veteranAffiarFlag = this.orderingTherapyAssociation[0].CCL_Veteran_Affairs_Applicable__c;
                                    this.therapyAssociationId = this.orderingTherapyAssociation[0].Id;
                                    this.hospitalPurchaseOrderText = this.orderingTherapyAssociation[0].CCL_Payment_Section_Text__c;
                                    this.billingPartyVisible = this.orderingTherapyAssociation[0].CCL_Billing_Party_Visible__c;
                                    this.capturePatientCountry = this.orderingTherapyAssociation[0].CCL_Capture_Patient_Country_of_Residence__c;
                                    this.applicableBillingParties = this.orderingTherapyAssociation[0].CCL_Applicable_Billing_Parties__c;
                                    this.CCL_centernum = this.orderingTherapyAssociation[0].CCL_Treatment_Protocol_Center_Number__c;
                                    this.hospatIdEmailOpt=this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Email_Opt_In__c;
                                    this.hospatIdStr= this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Opt_In__c;
                                })
                                .catch(error => {
                                    this.error = error;
                                });
                        }
                    })
                    .catch(error => {
                        this.error = error;
                    });
            }
    }

    connectedCallback() {
        let testURL = window.location.href;
        let newURL = new URL(testURL).searchParams;
        this.recordId = newURL.get('recordId');
        if (this.recordId !== null) {
            this.allowreplacementordertrue = true;
            fetchShipmentRecords({
                    orderId: this.recordId
                })
                .then(result => {
                    this.shipmentdata = result;
					// Commented as a part of CGTU-2111
					//this.hospitalPurchaseOrderNumber = this.shipmentdata[0].CCL_Hospital_Purchase_Order_Number__c;
                })
                .catch(error => {
                    this.error = error;
                });
            getpatientfromorder({
                    recordId: this.recordId
                })
                .then(data => {
                    this.patientdata = data;
                    //this.CCL_centernum = this.patientdata[0].CCL_Protocol_Center_Number__c;
                    this.CCL_subjectnum = this.patientdata[0].CCL_Protocol_Subject_Number__c;
                    this.CCL_firstname = this.patientdata[0].CCL_Patient__r.FirstName;
                    this.CCL_middleinitial = this.patientdata[0].CCL_Patient__r.MiddleName;
                    this.CCL_lastname = this.patientdata[0].CCL_Patient__r.LastName;
                    this.CCL_suffix = this.patientdata[0].CCL_Patient__r.Suffix;
                    this.CCL_inpdate = this.patientdata[0].CCL_Patient__r.CCL_Date_of_DOB__pc;
                    this.CCL_inpmonth = this.patientdata[0].CCL_Patient__r.CCL_Month_of_DOB__pc;
                    this.CCL_inpyear = this.patientdata[0].CCL_Patient__r.CCL_Year_of_DOB__pc;
                    this.hospitalPatientIdFlow = this.patientdata[0].CCL_Patient__r.CCL_Hospital_Patient_ID__pc;
                    this.PatientRecordId = this.patientdata[0].CCL_Patient__c;
					this.CCL_patientInitial=this.patientdata[0].CCL_Patient__r.CCL_Patient_Initials__c;
					this.patId=this.patientdata[0].CCL_Patient_Id__c;
                });
            fetchTherapyAssociation({
                    orderingHospital: this.hospitalId,
                    therapy: this.therapyId,
                    countryCode: this.hospitalCountryCode
                })
                .then(result => {
                    this.orderingTherapyAssociation = result;
                    this.CCL_centernum = this.orderingTherapyAssociation[0].CCL_Treatment_Protocol_Center_Number__c;
                    this.hospatIdEmailOpt=this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Email_Opt_In__c;
                    this.hospatIdStr= this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Opt_In__c;

                })
                .catch(error => {
                    this.error = error;
                });
        }
    }

    handleOrderingHospital(event) {
        const selectedOption = event.target.value;
        let self = this;
        if (selectedOption == '') {
            self.hospitalId = "";
        } else {
            this.orderingHospital.forEach(function(node) {
                if (node.Name == selectedOption) {
                    self.hospitalId = node.Id;
                    self.hospital = node.Name;
                    self.hospitalCountryCode = node.ShippingCountryCode;
                }
            });
        }
        this.hasTherapyDescription = false;
        this.therapyRecordLength = false;
        this.orderingTherapy = "";
        this.therapySelected = false;
        if(this.hospitalId)
        {
            fetchTherapy({
                orderingHospital: this.hospitalId
            })
            .then(result => {
                this.orderingTherapy = result;
                if (this.orderingTherapy.length > 1) {
                    this.therapyRecordLength = true;
                    this.therapySelected = false;
                } else if (this.orderingTherapy.length == 1) {
                    this.therapyDescription = this.orderingTherapy[0].CCL_Description__c;
                    this.therapyType = this.orderingTherapy[0].CCL_Type__c;
                    this.therapyExternalId = this.orderingTherapy[0].CCL_External_ID__c;
                    this.therapy = this.orderingTherapy[0].CCL_Therapy_Description__c;
                    this.therapyName = this.orderingTherapy[0].Name;
                    this.therapyId = this.orderingTherapy[0].Id;
                    this.therapySelected = true;
                    this.minimumWeight = this.orderingTherapy[0].CCL_Minimum_Weight__c;
                    this.maximumWeight = this.orderingTherapy[0].CCL_Maximum_Weight__c;
                    this.patientEligibilityText = this.orderingTherapy[0].CCL_Clinical_Patient_Eligibility_Text__c;
                    if ((this.therapyDescription !== undefined) && (this.therapyDescription.length > 0)) {
                        this.hasTherapyDescription = true;
                    }
                    fetchTherapyAssociation({
                            orderingHospital: this.hospitalId,
                            therapy: this.therapyId,
                            countryCode: this.hospitalCountryCode
                        })
                        .then(result => {
                            this.orderingTherapyAssociation = result;
                            this.hospitalPatientId = this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Opt_In__c;
                            this.hospitalPurchaseOrder = this.orderingTherapyAssociation[0].CCL_Hosp_Purchase_Order_Opt_In__c;
                            this.infusionDataCollection = this.orderingTherapyAssociation[0].CCL_Infusion_Data_Collection__c;
                            this.researchWithBioSample = this.orderingTherapyAssociation[0].CCL_Research_with_Bio_Samples_Opt_In__c;
                            this.minimumAge = this.orderingTherapyAssociation[0].CCL_Minimum_Age__c;
                            this.maximumAge = this.orderingTherapyAssociation[0].CCL_Maximum_Age__c;
                            this.commercialPhysicianAttestation = this.orderingTherapyAssociation[0].CCL_Commercial_Physician_Attestation__c;
                            this.veteranAffiarFlag = this.orderingTherapyAssociation[0].CCL_Veteran_Affairs_Applicable__c;
                            this.therapyAssociationId = this.orderingTherapyAssociation[0].Id;
                            this.billingPartyVisible = this.orderingTherapyAssociation[0].CCL_Billing_Party_Visible__c;
                            this.capturePatientCountry = this.orderingTherapyAssociation[0].CCL_Capture_Patient_Country_of_Residence__c;
                            this.hospitalPurchaseOrderText = this.orderingTherapyAssociation[0].CCL_Payment_Section_Text__c;
                            this.applicableBillingParties = this.orderingTherapyAssociation[0].CCL_Applicable_Billing_Parties__c;
                            this.CCL_centernum = this.orderingTherapyAssociation[0].CCL_Treatment_Protocol_Center_Number__c;
                            this.hospatIdEmailOpt=this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Email_Opt_In__c;
                            this.hospatIdStr= this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Opt_In__c;


                        })
                        .catch(error => {
                            this.error = error;
                        });
                }
            })
            .catch(error => {
                this.error = error;
            });
        }

    }

    handleTherapy(event) {
        const selectedOption = event.target.value;
        let self = this;
        if (selectedOption == undefined) {
            this.therapySelected = false;
            this.therapyDescription = "";
            this.therapyId = "";
            this.therapy = "";
            this.hasTherapyDescription = false;
        } else {
            this.therapySelected = true;
            this.orderingTherapy.forEach(function(node) {
                if (node.CCL_Therapy_Description__c === selectedOption) {
                    self.therapyDescription = node.CCL_Description__c;
                    self.therapyId = node.Id;
                    self.therapyName = node.Name;
                    self.therapyType = node.CCL_Type__c;
                    self.therapyExternalId = node.CCL_External_ID__c;
                    self.therapy = node.CCL_Therapy_Description__c;
                    self.minimumWeight = node.CCL_Minimum_Weight__c;
                    self.maximumWeight = node.CCL_Maximum_Weight__c;
                    self.patientEligibilityText = node.CCL_Clinical_Patient_Eligibility_Text__c;
                    if ((self.therapyDescription !== undefined) && (self.therapyDescription.length > 0)) {
                        self.hasTherapyDescription = true;
                    }
                }
            });
        }
        fetchTherapyAssociation({
                orderingHospital: this.hospitalId,
                therapy: this.therapyId,
                countryCode: this.hospitalCountryCode
            })
            .then(result => {
                this.orderingTherapyAssociation = result;
                this.hospitalPatientId = this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Opt_In__c;
                this.hospitalPurchaseOrder = this.orderingTherapyAssociation[0].CCL_Hosp_Purchase_Order_Opt_In__c;
                this.infusionDataCollection = this.orderingTherapyAssociation[0].CCL_Infusion_Data_Collection__c;
                this.researchWithBioSample = this.orderingTherapyAssociation[0].CCL_Research_with_Bio_Samples_Opt_In__c;
                this.minimumAge = this.orderingTherapyAssociation[0].CCL_Minimum_Age__c;
                this.maximumAge = this.orderingTherapyAssociation[0].CCL_Maximum_Age__c;
                this.commercialPhysicianAttestation = this.orderingTherapyAssociation[0].CCL_Commercial_Physician_Attestation__c;
                this.veteranAffiarFlag = this.orderingTherapyAssociation[0].CCL_Veteran_Affairs_Applicable__c;
                this.billingPartyVisible = this.orderingTherapyAssociation[0].CCL_Billing_Party_Visible__c;
                this.capturePatientCountry = this.orderingTherapyAssociation[0].CCL_Capture_Patient_Country_of_Residence__c;
                this.therapyAssociationId = this.orderingTherapyAssociation[0].Id;
                this.hospitalPurchaseOrderText = this.orderingTherapyAssociation[0].CCL_Payment_Section_Text__c;
                this.applicableBillingParties = this.orderingTherapyAssociation[0].CCL_Applicable_Billing_Parties__c;
                this.CCL_centernum = this.orderingTherapyAssociation[0].CCL_Treatment_Protocol_Center_Number__c;
                this.hospatIdEmailOpt=this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Email_Opt_In__c;
                this.hospatIdStr= this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Opt_In__c;


            })
            .catch(error => {
                this.error = error;
            });
    }

    onBegin() {
        if (this.hospitalId === undefined || this.hospitalId ==='') {
            this.template.querySelector(".hospitalError").classList.add("redBorder");
            this.orderinghospError = true;
          }
        else if (this.hospitalId != undefined) {
            this.orderinghospError = false;
            this.template
              .querySelector(".hospitalError")
              .classList.remove("redBorder");
          }
       if (this.therapyId === undefined || this.therapyId ==='' || this.therapySelected===false) {
            this.template.querySelector(".therapyError").classList.add("redBorder");
            this.therapyError = true;
          }
       else if (this.therapyId != undefined) {
            this.therapyError = false;
            this.template
              .querySelector(".therapyError")
              .classList.remove("redBorder");
          }
        if (this.therapySelected === true) {
            fireEvent(this.pageRef, 'therapytype1', this.therapyType);
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
        }
    }
}

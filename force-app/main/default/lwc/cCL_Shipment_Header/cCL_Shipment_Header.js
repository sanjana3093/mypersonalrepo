import { LightningElement, track, api, wire } from "lwc";
import getHeader from "@salesforce/apex/CCL_ADFController_Utility.getShipmentHeader";
import getTherapyType from "@salesforce/apex/CCL_ADFController_Utility.getTherapyType";
import getAccountDetails from "@salesforce/apex/CCL_ADF_Controller.getRecords";
import COMMERCIAL from "@salesforce/label/c.CCL_Therapy_Type_Commercial";
import gethospitalOptIn from "@salesforce/apex/CCL_ADFController_Utility.fetchHospitalOptIn";
import CLINICAL from "@salesforce/label/c.CCL_Therapy_Type_Clinical";
import PRINCIPAL_INVESTOGATOR from "@salesforce/label/c.CCL_Principal_Investigator";
import TRTMNT_PROTOCOL from "@salesforce/label/c.CCL_Treatment_Protocal_Subject_ID";
import HOSPITAL_PATIENT_ID from "@salesforce/label/c.CCL_Hospital_Patient_ID";
import PRESCRIBER from "@salesforce/label/c.CCL_Prescriber";
import PATIENT_NAME from "@salesforce/label/c.CCL_Patient_Name";
import PATIENT_ID from "@salesforce/label/c.CCL_Patient_Id";
import PATIENT_INITIALS from  "@salesforce/label/c.CCL_patientInitials";
const HOSPITALPATIENI_API_NAME = "CCL_Protocol_Subject_Hospital_Patient__c";
const PRESCRIBER_API_NAME = "CCL_Prescriber__c";
const ORDER_PRESCRIBER_API_NAME = "CCL_Prescriber_Text__c";
const ORDER_PATIENT_API_NAME = "CCL_Patient_Name_Text__c";
import { registerListener } from "c/cCL_Pubsub";
import {  CurrentPageReference } from "lightning/navigation";
import getAddress from "@salesforce/apex/CCL_Utility.fetchAddress";
export default class CCL_Shipment_Header extends LightningElement {
  @track therapyType;
  @api apiNames = [];
  @api hosptialOptedIn;
  @track statusColr=false;
  @track name;
  @track firstname;
  @track lastname;
  @track middlename;
  @track suffix;
  @track sobj;
  @track therapyId;
  @track orderinHospital;
  @track mainArr = [];
  @track firstValue = {};
  @track novBatchId;
  @track receivedDate;
  @track onHold;
  @track stepData=[];
  @track isTherapy=false;
  @track cancelled;
  @api recordId;
  @api isLoaded=false;
  @track status;
   //2701 code changes
  @track showInitials=false;
  @track patientInitials;
  @track hasFieldvalues;
  @api objectType='Shipment';
  patientIdValue;
  patientIdlabel=PATIENT_ID;
 // 2450 code
 @track adressIds=[];
 @api addressList;
   @wire(CurrentPageReference) pageRef;
  @wire(getTherapyType, { recordId: "$recordId", objectType: "$objectType" })
  wiredData({ error, data }) {
    if (data) {

      if(this.objectType == 'Shipment') {
        this.therapyType = data[0].CCL_Indication_Clinical_Trial__r.CCL_Type__c;
        this.therapyId = data[0].CCL_Indication_Clinical_Trial__c;
		 //cache issues
		  if(this.stepData.length>0){
        this.checkFields();
      }
		 //Added as part of 2306.!Imp!
     if(JSON.stringify(data).includes("CCL_Order_Apheresis__r")){
     this.showInitials=data[0].CCL_Order_Apheresis__r.CCL_Initials_COI__c;
        this.patientInitials=data[0].CCL_Order_Apheresis__r.CCL_Patient_Initials__c;
        this.patientIdValue=data[0].CCL_Order_Apheresis__r.CCL_Patient_Id__c;}
        else{
          this.showInitials=data[0].CCL_Order__r.CCL_Initials_COI__c;
      this.patientInitials=data[0].CCL_Order__r.CCL_Patient_Initials__c;
        this.patientIdValue=data[0].CCL_Order__r.CCL_Patient_Id__c;
        }
      }
	else if(this.objectType == 'Order') {
        this.therapyType = data[0].CCL_Therapy__r.CCL_Type__c;
        this.therapyId = data[0].CCL_Therapy__c;
		this.showInitials=data[0].CCL_Initials_COI__c;
        this.patientInitials=data[0].CCL_Patient_Initials__c;
		 //cache issues
		  if(this.stepData.length>0){
        this.checkFields();
      }
      }
      this.orderinHospital = data[0].CCL_Ordering_Hospital__c;
	  this.novBatchId = data[0].CCL_Novartis_Batch_Id__c;
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }

  @wire(gethospitalOptIn, {
    therapyId: "$therapyId",
    hospitalId: "$orderinHospital"
  })
  wiredHospital({ error, data }) {
    if (data) {
      this.hosptialOptedIn = data[0].CCL_Hospital_Patient_ID_Opt_In__c;
    } else if (error) {
    }
  }
  @wire(getHeader, {
    sobj: "$sobj"
  })
  wiredSteps({ error, data }) {
    if (data) {
      this.getApiNames(data);
      let self = this;
      this.stepData=data;
      if(this.therapyType!=undefined){
        data.forEach(function (node) {
          self.setFields(node, self);
        });
        this.isTherapy=false;
      }

	    if(this.objectType=='Shipment') {
       this.apiNames.push("CCL_Status__c");
      }

	    if(this.objectType=='Order') {
       this.apiNames.push("CCL_Patient_Id__c");
       this.apiNames.push("CCL_Secondary_COI_Patient_ID__c");
      }
      if(this.recordId!=undefined){
        this.isLoaded=false;
        this.fetchValues();
     }
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }
  getApiNames(data) {
    let self = this;
    let apiname;
    data.forEach((element) => {
      apiname = element.CCL_Field_API_Name__c;
      if (element.CCL_Field_Type__c == "Lookup" && (element.CCL_Field_API_Name__c == "CCL_Therapy__c"||element.CCL_Field_API_Name__c == "CCL_Indication_Clinical_Trial__c") ) {
        apiname =
          element.CCL_Field_API_Name__c.substring(
            0,
            element.CCL_Field_API_Name__c.length - 1
          ) + "r.CCL_Therapy_Description__c";
      } else if (element.CCL_Field_Type__c == "Lookup") {
        apiname =
          element.CCL_Field_API_Name__c.substring(
            0,
            element.CCL_Field_API_Name__c.length - 1
          ) + "r.Name";
      }
      self.apiNames.push(apiname);
    });
  }
  fetchValues() {
    getAccountDetails({
      sobj: this.sobj,
      cols: this.apiNames.toString(),
      recordId: this.recordId
    })
      .then((result) => {
        this.hasFieldvalues = result;
        this.setValues();
		  //2450
        if(this.adressIds.length>0) {
          this.getAddresses();
         }
        this.error = null;

      })
      .catch((error) => {
        this.error = error;
      });
  }
  setFields(mainArrr, self) {
    let fieldArr = {
      Fieldlabel: "",
      fieldApiName: "",
      fieldorder: "",
      fieldValue: "",
      fieldType: "",
	   isSite:""
    };
    fieldArr.Fieldlabel = mainArrr.MasterLabel;
    fieldArr.fieldType = mainArrr.CCL_Field_Type__c;
    if (
      (mainArrr.CCL_Field_API_Name__c == PRESCRIBER_API_NAME || mainArrr.CCL_Field_API_Name__c == ORDER_PRESCRIBER_API_NAME) &&
      this.therapyType == COMMERCIAL
    ) {
      fieldArr.Fieldlabel = PRESCRIBER;
    }
    if (
      (mainArrr.CCL_Field_API_Name__c == PRESCRIBER_API_NAME || mainArrr.CCL_Field_API_Name__c == ORDER_PRESCRIBER_API_NAME) &&
      this.therapyType == CLINICAL
    ) {
      fieldArr.Fieldlabel = PRINCIPAL_INVESTOGATOR;
    }
    if (
      mainArrr.CCL_Field_API_Name__c == HOSPITALPATIENI_API_NAME &&
      this.therapyType == COMMERCIAL
    ) {
      fieldArr.Fieldlabel = HOSPITAL_PATIENT_ID;
    }
    if (
      mainArrr.CCL_Field_API_Name__c == HOSPITALPATIENI_API_NAME &&
      this.therapyType == CLINICAL
    ) {
      fieldArr.Fieldlabel = TRTMNT_PROTOCOL;
    }
    if (mainArrr.CCL_Field_API_Name__c == ORDER_PATIENT_API_NAME) {
      fieldArr.Fieldlabel = PATIENT_NAME;
    }

    fieldArr.fieldApiName = mainArrr.CCL_Field_API_Name__c;
    if (mainArrr.CCL_Field_Type__c == "Lookup") {
      fieldArr.fieldApiName =
        fieldArr.fieldApiName.substring(0, fieldArr.fieldApiName.length - 1) +
        "r.Name";
		 fieldArr.isSite=true;
    }
    if(fieldArr.fieldApiName=='CCL_Indication_Clinical_Trial__r.Name' || fieldArr.fieldApiName=='CCL_Therapy__r.Name'){
      fieldArr.isSite=false;
    }
    fieldArr.fieldorder = mainArrr.CCL_Order_of_Field__c;
    self.mainArr.push(fieldArr);
  }
  setValues() {
    let temp = this.hasFieldvalues[0];
    this.status = temp.CCL_Status__c;
    let self=this;
	if(this.status == 'Apheresis Pick Up Cancelled' || this.status =='Apheresis Pick Up On Hold'){
      this.statusColr=true;
    }
    if(this.therapyType == CLINICAL && temp.CCL_Secondary_COI_Patient_ID__c && temp.CCL_Patient_Id__c) {
      this.patientIdValue  = temp.CCL_Patient_Id__c;
    }
    this.checkForOptIn();
    this.mainArr.forEach(function (node, index) {
      if((node.fieldApiName == 'CCL_Indication_Clinical_Trial__r.Name'||node.fieldApiName == 'CCL_Therapy__r.Name') && node.fieldType == "Lookup") {
        node.fieldValue = temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)]
        .CCL_Therapy_Description__c
      } else {
      node.fieldValue =
        node.fieldType == "Lookup"
          ? temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)]
              .Name
          : temp[node.fieldApiName];
      }
	   //2450
  if(node.isSite){
    node.fieldValue =
    node.fieldType == "Lookup"
      ? temp[node.fieldApiName.substring(0, node.fieldApiName.length - 5)]
          .Id
      : temp[node.fieldApiName];
    self.adressIds.push(temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-6)+'c']);
  }
    });

    if(this.mainArr[0].fieldApiName=='CCL_Patient_Name__c' || this.mainArr[0].fieldApiName=='CCL_Patient_Name_Text__c'){
    this.firstValue = this.mainArr[0];

	if(this.firstValue.fieldValue){
		this.name = this.firstValue.fieldValue;
  }
  //2701
  if(this.showInitials){
    this.firstValue.Fieldlabel=PATIENT_INITIALS;
  }
  else{

    this.firstValue.Fieldlabel='Patient Name';
    }
	 if(this.firstValue.fieldValue.split(" ").length==4){
      this.firstname = this.firstValue.fieldValue.split(" ")[0];
      this.lastname = this.firstValue.fieldValue.split(" ")[2];
      this.middlename = this.firstValue.fieldValue.split(" ")[1];
      this.suffix = this.firstValue.fieldValue.split(" ")[3];
    }
	 else if(this.firstValue.fieldValue.split(" ").length==3){
      this.firstname = this.firstValue.fieldValue.split(" ")[0];
      this.lastname = this.firstValue.fieldValue.split(" ")[2];
      this.middlename = this.firstValue.fieldValue.split(" ")[1];
    }
    else{
      this.firstname = this.firstValue.fieldValue.split(" ")[0];
      this.lastname = this.firstValue.fieldValue.split(" ")[1];
    }
    this.mainArr.splice(0, 1);
  }
  }
  checkForOptIn() {
    let self = this;
	//2701
   if (this.therapyType == COMMERCIAL) {
      this.mainArr.forEach(function (node, index) {
        if (node.fieldApiName == HOSPITALPATIENI_API_NAME&& !self.hasFieldvalues[0].CCL_Protocol_Subject_Hospital_Patient__c) {
          self.mainArr.splice(index, 1);
        }
      });
    }
  }
connectedCallback(){
  if(this.objectType == 'Shipment') {
    this.sobj='CCL_Shipment__c';
  } else if(this.objectType == 'Order') {
    this.sobj='CCL_Order__c';
  }
   this.isLoaded=true;
   this.isTherapy=true;
    registerListener("statusChange", this.handleStatusChange,this);
  }
  handleStatusChange(status) {
   if(status ){
      this.status = status;

  }
}
renderedCallback() {
  this.checkFields();

	 if(this.isLoaded){
   this.fetchValues();
    this.isLoaded=false;
  }
  const displayCheck = this.template.querySelector(".statusColr");
    if(this.statusColr){
      displayCheck.classList.remove('statusColr');
    }
}
checkFields(){
  let self=this;
  if(this.isTherapy&&this.therapyType!=undefined){
    this.stepData.forEach(function (node) {
      self.setFields(node, self);
    });
    this.fetchValues();
    this.isTherapy=false;
    this.isLoaded=false;
  }
}
//2450
 getAddresses(){
  getAddress({
    accountIds: this.adressIds,
  })
    .then((result) => {
      this.addressList=result;
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

}
import {
    LightningElement,
    api,
    track
} from 'lwc';
import understand from "@salesforce/label/c.PSP_Consent_Giving_Text";
import errMsgSign from "@salesforce/label/c.PSP_ErrMsg_to_sign";
import successMsg from "@salesforce/label/c.PSP_Succcess_Msg_for_consent";
import digSign from "@salesforce/label/c.PSP_Digital_Signature";
import save from "@salesforce/label/c.PSP_Save";
import cancel from "@salesforce/label/c.PSP_Cancel";
import clearsignature from "@salesforce/label/c.PSP_Clear_Signature";
import SaveSign from "@salesforce/apex/PSP_SignatureHandler.saveSign";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
//declaration of variables for calculations
let isDownFlag,
    isDotFlag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0;

let x = "#0000A0"; //blue color
let y = 1.5; //weight of line width and dot.       

let canvasElement, ctx; //storing canvas context
let attachment; //holds attachment information after saving the sigture on canvas
let dataURL, convertedDataURI; //holds image data
export default class pSP_DigitalSignature extends LightningElement {
    @api authformTextId;
    @api contentDocId;
    @api authFormconsentId;
    @api recordName;
    @api consentCapturedSource;
    @api authFormname;
    @api consentGiverId;
    @api dateTimeVal;
    @api consentType;
    @track successMessage;
    @api errMsg;
    @track save = save;

    // @api touchStart;
    // @api touchEnd;
    // @api touchCancel;
    @track cancel = cancel;
    @track clrSign = clearsignature;
    @track consentText = understand;
    @track digSign = digSign;
    @track eventVal;
    @api signaturePad;
    @api
    get touchaction() {
        return this.eventVal;
    }
    set touchaction(retnVal) {
        if (retnVal === 'start') {
            this.handleMouseDown(this);
        } else if (retnVal === 'move') {
            this.handleMouseMove(this);
        } else if (retnVal === 'end') {
            this.handleMouseUp(this);
        } else if (retnVal === 'cancel') {
            this.handleMouseOut(this);
        }
    }
    constructor() {
        super();
        this.dateTimeVal = new Date();
        this.template.addEventListener('mousemove', this.handleMouseMove.bind(this));
        this.template.addEventListener('mousedown', this.handleMouseDown.bind(this));
        this.template.addEventListener('mouseup', this.handleMouseUp.bind(this));
        this.template.addEventListener('mouseout', this.handleMouseOut.bind(this));

    }
    renderedCallback() { // invoke the method when component rendered or loaded
        canvasElement = this.template.querySelector('canvas');
        ctx = canvasElement.getContext("2d");

    }
    //handler for mouse move operation
    handleMouseMove(event) {
        this.searchCoordinatesForEvent('move', event);
    }

    //handler for mouse down operation
    handleMouseDown(event) {
        this.searchCoordinatesForEvent('down', event);
    }

    //handler for mouse up operation
    handleMouseUp(event) {
        this.searchCoordinatesForEvent('up', event);
    }

    //handler for mouse out operation
    handleMouseOut(event) {
        this.searchCoordinatesForEvent('out', event);
    }
    //clear the signature from canvas
    handleClearClick() {
        ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);
    }
    searchCoordinatesForEvent(requestedEvent, event) {
        if (event.clientX) {
            event.preventDefault();
        }

        if (requestedEvent === 'down') {
            this.setupCoordinate(event);
            isDownFlag = true;
            isDotFlag = true;
            if (isDotFlag) {
                this.drawDot();
                isDotFlag = false;
            }
        }
        if (requestedEvent === 'up' || requestedEvent === "out") {
            isDownFlag = false;
        }
        if (requestedEvent === 'move') {
            if (isDownFlag) {
                this.setupCoordinate(event);
                this.redraw();
            }
        }
    }

    //This method is primary called from mouse down & move to setup cordinates.
    setupCoordinate(eventParam) {
        //get size of an element and its position relative to the viewport 
        //using getBoundingClientRect which returns left, top, right, bottom, x, y, width, height.
        const clientRect = canvasElement.getBoundingClientRect();
        prevX = currX;
        prevY = currY;
        if (eventParam.clientX == undefined) {
            currX = this.signaturePad.clientX - clientRect.left;
            currY = this.signaturePad.clientY - clientRect.top;
        } else {
            currX = eventParam.clientX - clientRect.left;
            currY = eventParam.clientY - clientRect.top;
        }

    }

    //For every mouse move based on the coordinates line to redrawn
    redraw() {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(currX, currY);
        ctx.strokeStyle = x; //sets the color, gradient and pattern of stroke
        ctx.lineWidth = y;
        ctx.closePath(); //create a path from current point to starting point
        ctx.stroke(); //draws the path
    }

    //this draws the dot
    drawDot() {
        ctx.beginPath();
        ctx.fillStyle = x; //blue color
        ctx.fillRect(currX, currY, y, y); //fill rectrangle with coordinates
        ctx.closePath();
    }

    SaveSignature() {
        dataURL = canvasElement.toDataURL("image/png");
        //convert that as base64 encoding
        let blank = document.createElement('canvas');
        blank.width = canvasElement.width;
        blank.height = canvasElement.height;
        convertedDataURI = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
        if (dataURL === blank.toDataURL("image/png")) {
            this.errMsg = errMsgSign;
        } else {
            SaveSign({
                    base64Data: convertedDataURI,
                    contentType: "image/png",
                    authFormconsentId: this.authFormconsentId,
                    objId: this.consentGiverId,
                    fileName: this.consentCapturedSource + '-' + this.authFormname
                })
                .then(result => {

                    const event = new ShowToastEvent({
                        title: 'Success!',
                        message: successMsg,
                        variant: 'success'
                    });
                    this.dispatchEvent(event);
                    console.log('1');
                    this.error = null;
                    const successEvent = new CustomEvent('saveclose', {
                        bubbles: true,

                    });
                    console.log('11');
                    setTimeout(() => {
                        console.log('2');
                        this.dispatchEvent(successEvent);
                    }, 2000);

                })
                .catch(error => {
                    this.error = error;

                });
        }


    }
    handleCancel() {
        this.errMsg = null;
        const cancelEvent = new CustomEvent('cancelmodal', {
            bubbles: true,
            composed: true
        });
        this.dispatchEvent(cancelEvent);
    }
}
import { LightningElement,track, api } from 'lwc';

export default class MyListItem extends LightningElement {
 @api item;
@track myclass;
    constructor () {
        super();
    }
    connectedCallback () {
         this._item =  JSON.parse(JSON.stringify (this.item));
         if(this.item.selected){
            this.myclass='slds-listbox__item ms-list-item slds-is-selected';
        }else{
            this.myclass='slds-listbox__item ms-list-item';
        }
         
    }
    @api
    get itemClass () {
        console.log('in 14'+this.item.selected);
    
        if(this.item.selected){
            this.myclass='slds-listbox__item ms-list-item slds-is-selected';
        }else{
            this.myclass='slds-listbox__item ms-list-item';
        }
      
        console.log('the class is'+this.myclass);
        return this.myclass;
    }
   
    onItemSelected (event) {
        console.log('in 17'+this.item.selected);
        const evt = new CustomEvent ('items', { detail : {'item' :this.item, 'selected' : !this.item.selected }});
        this.dispatchEvent (evt);
        //this.itemClass();
        console.log('the val---'+ Array.from (this.template.querySelectorAll ('.slds-listbox__item')));
       
        this.itemClass();
        event.stopPropagation ();
       
    }
}
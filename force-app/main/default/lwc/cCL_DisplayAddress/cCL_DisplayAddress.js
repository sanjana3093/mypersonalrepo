import { LightningElement,api } from 'lwc';

export default class CCL_DisplayAddress extends LightningElement {

    @api addressList;
    @api siteName;
get address(){
        let self=this;
        let completeAddress;
        if(this.addressList){
        this.addressList.forEach(function(node){
            if(node.CCL_Site__c==self.siteName){
                completeAddress=node.CCL_Complete_Address__c;
            }
        });
    }
        return completeAddress;
    }
}
import { LightningElement, api, wire,track } from 'lwc';
import { FlowAttributeChangeEvent ,FlowNavigationNextEvent} from 'lightning/flowSupport';
import {getRecord,updateRecord} from 'lightning/uiRecordApi';
import {fireEvent} from 'c/cCL_Pubsub';
import { CurrentPageReference } from "lightning/navigation";
import STATUS from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Status__c";
import CCL_Manufacturing_Started__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_ADF_Locked_For_Edit__c";
import CCL_PRF_Updated__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_PRF_Updated__c";
import CCL_ADF_Submitter_Permission from "@salesforce/label/c.CCL_ADF_Submitter_Permission";
import ID from "@salesforce/schema/CCL_Apheresis_Data_Form__c.Id";
import getAdfSummarPageDetails from "@salesforce/apex/CCL_ADFController_Utility.getADFSummaryPageInfo";


import checkUserPermission from "@salesforce/apex/CCL_ADFController_Utility.checkUserPermission";

export default class CCL_ParseJson extends LightningElement {
    @api jsonData;  //Fetches the JSON data comming from the flow.
    @api screenName;//sends the string value of flow screen back to the flow.
    @api recordId;
    @api status;
    @api manufacturingStarted=false;
    @track ApproverPermission='CCL_ADF_Approver_User';
    @track SubmitterPermission=CCL_ADF_Submitter_Permission;
    @track ViewerPermission='CCL_ADF_Viewer_User';
    @track newViewerPermission='CCL_ADF_Viewer';
    @track newPermissionName="CCL_ADF_Submitter";
    @track CustomerOpPermission='CCL_Customer_Operations_User';
    @track prfUpdated=false;
    @track isAdfApprover=false;
    @track isAdfSubmitter=false;
    @track isAdfViewer=false;
    @api isCustomerOp=false;
	   @api isADFViewerInternal=false;
    @track isAdfSubmitterUser=false;
    @track newApproverPermission="CCL_ADF_Approver";
    @track isAdfApproverUser=false;
    @track isAdfViewerUser=false;
    @track isCustomerOpUser=false;
    @track InboundLogistcsPermission='CCL_Inbound_Logistics_Users';
    @track newInboundLogistcsPermission='CCL_Inbound_Logistics';
  @track isInboundLogisctics=false;
  @track isInboundLogiscticsUser=false;
  @track NovartisAdminPermission='CCL_Novartis_Admin_Permission_Set';
  @track newNovartisAdminPermission='CCL_Novartis_Business_Admin';
  @track newCustomerOpPermission='CCL_Customer_Operations';
  @track isNovartisAdmin=false;
  @track isNovartisAdminUser=false;
  @track newOutboundLogisticPermission = "CCL_Outbound_Logistics";
  @track isOutboundLogistic = false;
  @track isOutboundLogisticUser = false;
  @track pageResult;
  @track adfViewerInternal="CCL_ADF_Viewer_Internal";

    @wire(CurrentPageReference) pageRef;

    @wire(getRecord, {
        recordId: "$recordId",
        fields: [STATUS,CCL_Manufacturing_Started__c,CCL_PRF_Updated__c,CCL_PRF_Updated__c]
    }) getADF({
        error,
        data
    }) {
        if (error) {
           this.error = error ;
        } else if (data) {
            this.status = data.fields.CCL_Status__c.value;
            this.manufacturingStarted=data.fields.CCL_ADF_Locked_For_Edit__c.value;
            this.prfUpdated=data.fields.CCL_PRF_Updated__c.value;
            this.checkLoggedUserPermission();
        }
    }
    checkLoggedUserPermission(){
		  //2260
      checkUserPermission({
        permissionName: this.adfViewerInternal
      })
        .then((result) => {
          
         this.isADFViewerInternal=result;
          this.error = null;
          const attributeChangeEvent = new FlowAttributeChangeEvent('isADFViewerInternal', this.isADFViewerInternal);
        this.dispatchEvent(attributeChangeEvent);
        })
        .catch((error) => {
          this.error = error;
        });

        checkUserPermission({
            permissionName: this.ApproverPermission
          })
            .then((result) => {
              this.isAdfApprover=result;
              checkUserPermission({
                permissionName: this.newApproverPermission
              })
                .then((result) => {
                  if(result){
                    this.isAdfApproverUser=result;
                    if(this.isAdfApproverUser){
                      this.isAdfApprover=result;
                    }
                  }
                })
                .catch((error) => {
                  this.error = error;
                });
              checkUserPermission({
                permissionName: this.SubmitterPermission
              })
                .then((result) => {
                  this.isAdfSubmitter=result;
                 checkUserPermission({
                  permissionName: this.newPermissionName
                })
                  .then((result) => {
                    if(result){
                      this.isAdfSubmitterUser=result;
                      if(this.isAdfSubmitterUser){
                        this.isAdfSubmitter=result;
                      }
                    }
                    this.error = null;
                  })
                  .catch((error) => {
                    this.error = error;
                  });
                  //adding check for viewer
                  checkUserPermission({
                    permissionName: this.ViewerPermission
                  })
                    .then((result) => {
                       this.isAdfViewer=result;
                       checkUserPermission({
                        permissionName: this.newViewerPermission
                      })
                        .then((result) => {
                          if(result){
                            this.isAdfViewerUser=result;
                            if(this.isAdfViewerUser){
                              this.isAdfViewer=result;
                            }
                          }
                          this.error = null;
                        })
                        .catch((error) => {
                          this.error = error;
                        });
                       //Checking for customerOpPermission
                      checkUserPermission({
                        permissionName: this.CustomerOpPermission
                      })
                        .then((result) => {
                           this.isCustomerOp=result;
                           checkUserPermission({
                            permissionName: this.newCustomerOpPermission
                          })
                            .then((result) => {
                              if(result){
                                this.isCustomerOpUser=result;
                                if(this.isCustomerOpUser){
                                  this.isCustomerOp=result;
                                  }
                              }
                              this.error = null;
                            })
                            .catch((error) => {
                              this.error = error;
                            });
                           checkUserPermission({
                            permissionName: this.InboundLogistcsPermission
                          })
                            .then((result) => {
                             this.isInboundLogisctics=result;
                             checkUserPermission({
                              permissionName: this.newInboundLogistcsPermission
                            })
                              .then((result) => {
                                if(result){
                                  this.isInboundLogiscticsUser=result;
                                  if(this.isInboundLogiscticsUser){
                                    this.isInboundLogisctics=result;
                                  }
                                }
                                this.error = null;
                              })
                              .catch((error) => {
                                this.error = error;
                              });
                              //adding permission check for outboundLogistics User
                            checkUserPermission({
                              permissionName: this.newOutboundLogisticPermission
                            })
                              .then((result) => {
                              this.isOutboundLogistic=result;
                              checkUserPermission({
                                permissionName: this.newOutboundLogisticPermission
                              })
                                .then((result) => {
                                  if(result){
                                    this.isOutboundLogisticUser=result;
                                    if(this.isOutboundLogisticUser){
                                      this.isOutboundLogistic=result;
                                    }
                                  }
                                  this.error = null;
                                })
                                .catch((error) => {
                                  this.error = error;
                                });
                             checkUserPermission({
                              permissionName: this.NovartisAdminPermission
                            })
                              .then((result) => {
                               this.isNovartisAdmin=result;
                               checkUserPermission({
                                permissionName: this.newNovartisAdminPermission
                              })
                                .then((result) => {
                                  if(result){
                                    this.isNovartisAdminUser=result;
                                    if(this.isNovartisAdminUser){
                                      this.isNovartisAdmin=result;
                                    }
                                  }
                                  this.error = null;
                                })
                                .catch((error) => {
                                  this.error = error;
                                });
                               
                               this.parseScreenName();
                                this.error = null;
                              })
							  .catch((error) => {
                                  this.error = error;
                                  
                                });
                                this.error = null;
                              })				  
                              .catch((error) => {
                                this.error = error;
                                
                              });
                              this.error = null;
                            })
                            .catch((error) => {
                              this.error = error;
                            });
                          this.error = null;
                        })
                        .catch((error) => {
                          this.error = error;
                        });
                      this.error = null;
                    })
                    .catch((error) => {
                      this.error = error;
                    });
                  //viewer check ends here
                  //this.parseScreenName();
                  this.error = null;
                })
                .catch((error) => {
                  this.error = error;
                });
              this.error = null;
            })
            .catch((error) => {
              this.error = error;
            });
			//added as part of 2295
			const attributeChangeEvent1 = new FlowAttributeChangeEvent('isCustomerOp', this.isCustomerOp);
            this.dispatchEvent(attributeChangeEvent1);
                                
    }

    parseScreenName(){
      getAdfSummarPageDetails({
        recordId: this.recordId
      })
        .then((result) => {
          if(result){
            this.pageResult = result;
            
            let submitterId='';
            if(this.pageResult[0].CCL_ADF_Submitted_By__r){
            submitterId=this.pageResult[0].CCL_ADF_Submitted_By__r.Id;
            }
            if((this.isAdfViewer && !this.isAdfSubmitter)||(this.isCustomerOp)||(this.isNovartisAdmin)||(this.isInboundLogisctics)||(this.isOutboundLogistic)||
            (this.status==="Order Cancelled"&&this.isAdfSubmitter)){
               this.screenName="6";
            }
            else if((this.jsonData==null||this.jsonData==undefined||this.jsonData==='{}' || this.pageResult[0].CCL_Land_on_Patient_Verification__c ||
             (this.prfUpdated && this.isAdfSubmitter))&&!this.isAdfApprover){
              this.screenName="0";
            }
            else{
                if((this.isAdfApprover||this.isADFViewerInternal) && !this.manufacturingStarted&&this.status==="ADF Pending Approval"){
                    this.screenName="7";
                    }
                else if(this.isAdfApprover && !this.isAdfSubmitter){
                  this.screenName="6";
                }
          else if(this.isAdfSubmitter&&this.status==="ADF Approved"){
                  this.screenName="6";
                } else if(this.isAdfSubmitter && this.isAdfApprover && submitterId=='' && (this.jsonData==null||this.jsonData==undefined||this.jsonData==='{}')){
                  this.screenName="0";
                }else if(this.isAdfSubmitter && this.isAdfApprover && submitterId=='' && this.jsonData){
                  let jsonObj=JSON.parse(this.jsonData);
                    this.screenName=jsonObj.latestScreen;
                }else if(this.isAdfSubmitter && this.isAdfApprover){
                  this.screenName="6";
                }
                else{
                    let jsonObj=JSON.parse(this.jsonData);
                    this.screenName=jsonObj.latestScreen;
                }
            }
            this.changeAttribute();
          }
          this.error = null;
        })
        .catch((error) => {
          this.error = error;
        });
    }

    changeAttribute(){
      const attributeChangeEvent = new FlowAttributeChangeEvent('screenName', this.screenName);
        this.dispatchEvent(attributeChangeEvent);
        if(this.prfUpdated){
            const fields = {};
            fields[ID.fieldApiName] = this.recordId;
            fields[CCL_PRF_Updated__c.fieldApiName] = false;
            const recordInput = { fields };
            updateRecord(recordInput)
                .then(() => {
                    if(this.isAdfApprover && !this.isAdfSubmitter){
                        fireEvent(this.pageRef, 'adfApprovedPage', 'adfApproved');
                    }
                    if(this.isADFViewerInternal&&this.status==="ADF Pending Approval"){
                      fireEvent(this.pageRef, 'adfApprovedPage', 'adfApproved');
                      fireEvent(this.pageRef, 'showSummaryLink', false);
                    }
                    const navigateNextEvent = new FlowNavigationNextEvent();
                    this.dispatchEvent(navigateNextEvent);
                })
                .catch(error => {
                  
                    this.error = error;
                    });
            }
        if(this.isAdfApprover && !this.isAdfSubmitter){
        fireEvent(this.pageRef, 'adfApprovedPage', 'adfApproved');}
        if(this.isADFViewerInternal&&this.status==="ADF Pending Approval"){
          
		  fireEvent(this.pageRef, 'adfApprovedPage', 'adfApproved');
        }
        const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
    }

}
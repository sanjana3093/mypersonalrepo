import { LightningElement, api, track } from "lwc";
import CCL_FOOTER_NEXT from '@salesforce/label/c.CCL_FOOTER_NEXT';
import CCL_FOOTER_BACK from '@salesforce/label/c.CCL_FOOTER_BACK';
import CCL_FOOTER_SUBMIT from '@salesforce/label/c.CCL_FOOTER_SUBMIT';
import CCL_FOOTER_REVIEWANDSUBMIT from '@salesforce/label/c.CCL_FOOTER_REVIEWANDSUBMIT';
import CCL_FOOTER_BEGIN_REQUEST from '@salesforce/label/c.CCL_FOOTER_BEGIN_REQUEST';
import CCL_Cancel_Changes from '@salesforce/label/c.CCL_Cancel_Changes';
import CCL_Save_Fr_Later from '@salesforce/label/c.CCL_Save_Fr_Later';
import CCL_Reject from '@salesforce/label/c.CCL_Reject_Button';
import CCL_Approve from '@salesforce/label/c.CCL_Approve_Button';
import CCL_Submit_Changes from '@salesforce/label/c.CCL_Submit_Changes';
export default class CCLFlowFooter extends LightningElement {
    @api showNext;
    @api showPrev;
    @api showSave;
    @api showReview;
    @api showSubmit;
    @api showBegin;
    @api showReject;
    @api showApprove;
    @api cssitem;
    @api cssitem1;
	@api showGrey;
    @api greyApprove;
    @api greyReject;
	@api showSaveChange;
    @api showCancelChange;
    @track cssVar;
    @track cssVar1;
    @api showSubmitDisable;
	@api showSaveGrey;
    label = {
      CCL_FOOTER_NEXT,
      CCL_FOOTER_BACK,
      CCL_FOOTER_SUBMIT,
      CCL_FOOTER_REVIEWANDSUBMIT,
      CCL_FOOTER_BEGIN_REQUEST,
      CCL_Cancel_Changes,
      CCL_Save_Fr_Later,
      CCL_Reject,
      CCL_Approve,
      CCL_Submit_Changes
    };
    connectedCallback(){
      this.cssVar = 'slds-p-horizontal_medium btnwidth slds-button slds-button_brand ' + this.cssitem;
      this.neutral = 'slds-p-horizontal_medium btnwidth slds-button slds-button_outline-brand ' + this.cssitem;
      this.neutralOutlined = 'slds-p-horizontal_medium btnwidth slds-button slds-button_outline-brand ' + this.cssitem1;
    }
  handleBeginRequest(event){
    this.dispatchEvent(new CustomEvent("begin"));
  }
  handlePrevious(event) {
    this.dispatchEvent(new CustomEvent("previous"));
  }
  handleSave() {
    this.dispatchEvent(new CustomEvent("save"));
  }
  handleNext() {
    this.dispatchEvent(new CustomEvent("next"));
  }

  handleReview(){
    this.dispatchEvent(new CustomEvent("review"));
  }
  handleSubmit(){
    this.dispatchEvent(new CustomEvent("submit"));
  }
  handleReject() {
    this.dispatchEvent(new CustomEvent("reject"));
  }
  handleApprove() {
    this.dispatchEvent(new CustomEvent("approve"));
  }
  saveChanges() {
    this.dispatchEvent(new CustomEvent("savechanges"));
  }
  cancelChanges() {
    this.dispatchEvent(new CustomEvent("cancelchanges"));
  }
}
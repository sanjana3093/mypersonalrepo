import { LightningElement, track, api,wire } from 'lwc';
import saveForLater from '@salesforce/apex/CCL_ADF_Controller.saveForLater';
import CCL_Aph_Comments from "@salesforce/label/c.CCL_Aph_Comments";
import {
  FlowNavigationNextEvent,
  FlowAttributeChangeEvent
} from "lightning/flowSupport";
import { NavigationMixin,CurrentPageReference} from "lightning/navigation";
import {refreshApex} from '@salesforce/apex';
import getRecommendedCryoDetailsList from "@salesforce/apex/CCL_ADF_Controller.getRecommendedCryoDetails";
import relatedDocuments from '@salesforce/label/c.CCL_Related_Documents';
import getFileDetails from "@salesforce/apex/CCL_ADFController_Utility.getUploadedFileDetails";
//2260
import { registerListener,fireEvent } from "c/cCL_Pubsub";
//Added as part of 355
import checkUserPermission from "@salesforce/apex/CCL_ADFController_Utility.checkUserPermission";
import deleteFile from "@salesforce/apex/CCL_ADFController_Utility.deleteFile";
import CCL_DeleteConfirmation_Title from "@salesforce/label/c.CCL_DeleteConfirmation_Title";
import CCL_DeleteConfirmation_Message from "@salesforce/label/c.CCL_DeleteConfirmation_Message";
import CCL_DeleteConfirmation_Ok from "@salesforce/label/c.CCL_DeleteConfirmation_Ok";
import CCL_DeleteConfirmation_Cancel from "@salesforce/label/c.CCL_DeleteConfirmation_Cancel";
import getTimezoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getTimeZoneDetails";

//imported as part of 850
import { getRecord,updateRecord } from "lightning/uiRecordApi";
import reasonForModification from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Reason_For_Modification__c";
import CCL_Approval_Counter__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Approval_Counter__c";
import ID from "@salesforce/schema/CCL_Apheresis_Data_Form__c.Id";
import CCL_Reason_Modal_Header from "@salesforce/label/c.CCL_Reason_Modal_Header";
import CCL_Reason_Modal_Message from "@salesforce/label/c.CCL_Reason_Modal_Message";
import CCL_Reason_Modal_Helptext from "@salesforce/label/c.CCL_Reason_Modal_Helptext";
import CCL_Reason_Modal_Subheading from "@salesforce/label/c.CCL_Reason_Modal_Subheading";
import CCL_ADF_Submitter_Permission from "@salesforce/label/c.CCL_ADF_Submitter_Permission";

import CCL_PRF_Resubmission_Approval from "@salesforce/label/c.CCL_PRF_Resubmission_Approval";
import getOrderApprovalInfo from "@salesforce/apex/CCL_ADFController_Utility.getOrderApprovalInfo";
import getcustomdoc from "@salesforce/apex/CCL_ADFController_Utility.getDocId";
import CCL_Document_Upload_Msg from "@salesforce/label/c.CCL_Document_Upload_Msg";
//tranlsation labels
import CCL_Rec_Comm_err_msg_6_digits from "@salesforce/label/c.CCL_Rec_Comm_err_msg_6_digits";
import CCL_Rec_Comm_err_msg_max_digits from "@salesforce/label/c.CCL_Rec_Comm_err_msg_max_digits";
import CCL_Rec_Comm_err_msg_3_digits from "@salesforce/label/c.CCL_Rec_Comm_err_msg_3_digits";
import CCL_Rec_Cryo_2_digits_1_digit_aftr from "@salesforce/label/c.CCL_Rec_Cryo_2_digits_1_digit_aftr";
import CCL_Rec_cryo_2_digits_err from "@salesforce/label/c.CCL_Rec_cryo_2_digits_err";
import CCL_Absolute_Counts from "@salesforce/label/c.CCL_Absolute_Counts";
import CCL_Percentages from "@salesforce/label/c.CCL_Percentages";
import CCL_Cell_Viability from "@salesforce/label/c.CCL_Cell_Viability";
import CCL_Complete_Blood_Count_CBC_with_Differential from "@salesforce/label/c.CCL_Complete_Blood_Count_CBC_with_Differential";

import CCL_Warning from "@salesforce/label/c.CCL_Warning";
import CCL_Recom_Cryo_Details from "@salesforce/label/c.CCL_Recom_Cryo_Details";
import CCL_Additional_Comments from "@salesforce/label/c.CCL_Additional_Comments";
import CCL_255_Char_Limit from "@salesforce/label/c.CCL_255_Char_Limit";
import CCL_Delete from "@salesforce/label/c.CCL_Delete";
import CCL3_Approved from "@salesforce/label/c.CCL3_Approved";
import CCL_Submit from "@salesforce/label/c.CCL_Submit";
import CCL_Back from "@salesforce/label/c.CCL_Back";
import CCL_Save_Changes from "@salesforce/label/c.CCL_Save_Changes";
import CCL_Modal_Close from "@salesforce/label/c.CCL_Modal_Close";
export default class CCL_Recommended_Cryopreservation_Details extends NavigationMixin(
    LightningElement
  ){
    @track commentValue;
    @track AccountName='abc';
    @track AccountNumber='123';
    @track myJSON='';
    @track seconCollection=[];
    @track activesectionName;
    @api recordId;
    @api flowScreen='Rec Cryo';
    @api jsonData;
    @api jsonObj={};// this will contain the entire json structure of all the pages if any data saved
    @api comments;
    @api tableData;
	@api isADFViewerInternal;
    @track multiple=true;
	      //2260
@wire(CurrentPageReference) pageRef;
    @track labels={
      CCL_Aph_Comments,
      relatedDocuments,
      CCL_DeleteConfirmation_Title,
      CCL_DeleteConfirmation_Message,
      CCL_DeleteConfirmation_Ok,
      CCL_DeleteConfirmation_Cancel,
	  CCL_Reason_Modal_Header,
      CCL_Reason_Modal_Message,
      CCL_Reason_Modal_Helptext,
	  CCL_PRF_Resubmission_Approval,
      CCL_Reason_Modal_Subheading,
	  CCL_Document_Upload_Msg,
CCL_Save_Changes,
    CCL_Back,
    CCL_Submit,
    CCL3_Approved,
    CCL_Delete,
    CCL_255_Char_Limit,
    CCL_Additional_Comments,
    CCL_Recom_Cryo_Details,
    CCL_Warning,
    CCL_Modal_Close
    };
	//translation 2593
  errorlabels={CCL_Rec_Comm_err_msg_6_digits,CCL_Rec_Comm_err_msg_max_digits,CCL_Rec_Comm_err_msg_3_digits,CCL_Rec_Cryo_2_digits_1_digit_aftr,CCL_Rec_cryo_2_digits_err};
 sectionlabels={CCL_Absolute_Counts,CCL_Percentages,CCL_Complete_Blood_Count_CBC_with_Differential,CCL_Cell_Viability};
	@track prfApprovalRequired;
    @track prfResubmission;
    @track prfStatus;
    @track prfWarningMessage= false;
    @track configArr;
    @api apiNames=[];
    @api myActualArr = [];
    @api configVal;
    @api saveClicked=false;
    @api collectionInput={};
    @track count=0;
    @api saveLaterClicked=false;
    @api previousClicked=false;
    @api screenName;
    @api storedValues;
    @track navigateFurther;
    @api validationInput={};
    @track filesUploaded=[];
    @track url;
    @track showUploadedFiles=false;
    wiredFile;

     //added as part of us 355
  @track isDialogVisible = false;
  @track originalMessage;
  @track displayMessage = 'Click on the \'Open Confirmation\' button to test the dialog.';
  @track fileUrl;
  @track idPosition;
  @track docVersionId;
  @track SubmitterPermission=CCL_ADF_Submitter_Permission;
  @track isAdfSubmitter=false;
  @track renderDelete=false;
  @track wiredDeleteFile;
  @track screennameVal="Recommended Cryopreservation Details";
  @api timeZone;
  @api myTimeObj = {
    CCL_TEXT_Cryopreserve_Mat_Date_Time__c:""
  };
  @api myDateObj = {
    CCL_TEXT_Cryopreserve_Mat_Date_Time__c:""
  };
  //ends here

	
  //added as part of 850
  @track isReasonModalOpen=false;
  @track ReasonForMod;
  @track reason;
  @track navigate;
  @track counter;
  @track orignalJson;
  @track orignalFileresult;
  @track commentOrFileChange=false;
  @track newPermissionName="CCL_ADF_Submitter";
  @track isAdfSubmitterUser=false;
  @track adfDocId;
  @api isCustomerOp;
  @track disableFileUpload=false;

  @wire(getRecord, { recordId: "$recordId", fields:[reasonForModification,CCL_Approval_Counter__c] })
  Reason({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
      this.reason = data.fields.CCL_Reason_For_Modification__c.value;
      this.counter=data.fields.CCL_Approval_Counter__c.value;
      if(this.counter===null){
        this.counter=0;
      }
    }
  };
  //changes for 850 end here
	
	@wire(getcustomdoc, { recordId: "$recordId" })
  wiredDocId({ error, data }) {
      if (data) {
        this.adfDocId = data[0].Id;
          this.error = null;
      } else if (error) {
        this.error = error;
      }
  }
  get acceptedFormats() {
                  return ['.doc','.txt','.pdf','.xls','.csv','.ppt','.jpg','.jpeg','.png'];
              }

  @wire(getTimezoneDetails, {screenName:"$screennameVal", adfId: "$recordId" })
wiredTimeZoneDetailsSteps({ error, data }) {
    if (data) {
        this.timeZone = data;
      this.error = null;
    } else if (error) {
        this.error = error;
    }
}

@wire(getOrderApprovalInfo, { recordId: "$recordId" })
  wiredAssocidatedOrderApprovalDetails({ error, data }) {
    if (data) {
      this.prfApprovalRequired =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_Order_Approval_Eligibility__c:this.prfApprovalRequired;
      this.prfResubmission =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Approval_Counter__c:this.prfResubmission;
      this.prfStatus =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Ordering_Status__c:this.prfStatus;
      if(this.prfApprovalRequired && this.prfResubmission >='1' && this.prfStatus!='PRF_Approved') {
        this.prfWarningMessage = true;
      }
    } else if (error) {
      this.error = error;
    }
  }
  renderedCallback() {
    if(this.isADFViewerInternal) {
      this.renderDelete =false;
    }
  }
  connectedCallback(){
	  if(this.isADFViewerInternal||this.isCustomerOp){
      this.disableFileUpload=true;
    }
    if(this.isADFViewerInternal) {
      this.renderDelete =false;
    }
    let val=JSON.parse(this.jsonData);
	  //2260
 registerListener("gotoSummary", this.gotoADFSummary,this);
	this.orignalJson=val;
         let mytable=val["2"];
         let storedValue = val["5"];
         if (storedValue) {
          if (storedValue.collections !== undefined) {
          storedValue = storedValue.collections;
            if (Object.keys(storedValue["1"]).length > 0) {
            this.loadOldval = true;
            this.storedValues = storedValue;
             }
          }
        }
        this.tableData = mytable[1];
        if(mytable[1][0]["CCL_DIN__c"]===null) {
        this.activesectionName='Collection '+'1'+' - Apheresis Material Testing Information' + " for " + " Apheresis ID : " + mytable[1][0]["CCL_Apheresis_ID__c"];
      }
        else {
          this.activesectionName = "Collection " + "1" +' - Apheresis Material Testing Information' + " for " + " DIN : " + mytable[1][0]["CCL_DIN__c"];
        }
    this.tableData.forEach(function(node,index) {
      if(mytable[1][0]["CCL_DIN__c"]===null) {
        node.attributes.url='Collection '+(index+1)+' - Apheresis Material Testing Information' + " for " + " Apheresis ID : " + mytable[1][index]["CCL_Apheresis_ID__c"];}
        else{
          node.attributes.url='Collection '+(index+1)+ ' - Apheresis Material Testing Information' + " for " + " DIN : " + mytable[1][index]["CCL_DIN__c"];
        }
    });
    if(this.jsonData!=undefined ||this.jsonData!=null ){
      this.jsonObj=JSON.parse(this.jsonData);
      this.commentValue = this.jsonObj["Collection and Cryopreservation Comments"];
    }
    //Changes as part of 355
    checkUserPermission({
      permissionName: this.SubmitterPermission
    })
      .then((result) => {
        result=true;
        this.isAdfSubmitter=result;
        this.renderDelete=result;
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
      checkUserPermission({
        permissionName: this.newPermissionName
      })
        .then((result) => {
          if(result){
            this.isAdfSubmitterUser=result;
            if(this.isAdfSubmitterUser){
              this.isAdfSubmitter=result;
              this.renderDelete=result;
            }
          }
          this.error = null;
        })
        .catch((error) => {
          this.error = error;
        });
    //Changes as part of 355 ends here
}


@wire(getFileDetails, { recordId: "$adfDocId" })
wiredFileListSteps({ error, data }) {
    if (data) {
        this.filesUploaded = data;
        if(this.filesUploaded.length>0){
          this.showUploadedFiles=true;
        }
        this.error = null;
    } else if (error) {
        this.error = error;
    }
}

@wire(getFileDetails, { recordId: "$adfDocId" })
imperativeFilWiring(result) {
    this.wiredFile = result;
   if(result.data) {
        this.filesUploaded = result.data;
        if(this.filesUploaded.length>0){
          this.showUploadedFiles=true;
        }
    }
}

viewDocument(event) {
  this.url='/sfc/servlet.shepherd/document/download/'+event.target.name;
  window.open(this.url)
        }

handleUploadFinished(event) {
	this.orignalFileresult=JSON.stringify(this.wiredFile);
  return refreshApex(this.wiredFile);
      }
//added as part of 355
@wire(getFileDetails, { recordId: "$adfDocId" })
imperativeFielDeletion(result) {
    this.wiredDeleteFile = result;
    if(result.data) {
        this.filesUploaded = result.data;
        if(this.filesUploaded.length>0){
          this.showUploadedFiles=true;
        }
    }
}

	saveClickedFalse(){
    this.commentOrFileChange=false;
    this.saveClicked=false;
    this.saveLaterClicked=false;
    this.previousClicked=false;
  }


handleDeleteFinished() {
	this.orignalFileresult=JSON.stringify(this.wiredFile);
  return refreshApex(this.wiredDeleteFile);
      }
//355 changes ends here


@wire(getRecommendedCryoDetailsList)
wiredSteps({ error, data }) {
  if (data) {
    this.sobj = data[0].CCL_Object_API_Name__c;
    this.configVal = data;
    let mySectioned;
    mySectioned = this.getSections(this.configVal);
    this.configArr= this.getFields(data, mySectioned);
    this.error = null;
    this.gotresult = true;
  } else if (error) {
    this.error = error;
  }
}
getSections(myArr) {
  let configArr = [];
  let sectionnames = [];
 //translation 2593
    let self=this;
    let myObj = { sectioname: "", myFields: [] };
    myArr.forEach(function (node) {
      sectionnames.push(self.sectionlabels[node.CCL_Section_Name__c]);
    });
  let uniq = [...new Set(sectionnames)];
  uniq.forEach(function (node) {
    myObj = { sectioname: "", myFields: [] };
    myObj.sectioname = node;
    configArr.push(myObj);
  });
  return configArr;
}
getFields(mainArrr, configArr) {
  let i, j;
  let self = this;
  for (i = 0; i < mainArrr.length; i++) {
    for (j = 0; j < configArr.length; j++) {
       //tranlsation 2593
        if (configArr[j].sectioname === self.sectionlabels[mainArrr[i].CCL_Section_Name__c]) {
          self.setFields(configArr, mainArrr, i, j);
        }
    }
  }
  configArr.forEach(function (node) {
    node.myFields = self.sortData("fieldorder", "asc", node.myFields);
  });
  this.myActualArr = configArr;
 }
sortData(fieldName, sortDirection, mydata) {
  let data = JSON.parse(JSON.stringify(mydata));
  let key = (a) => a[fieldName];
  let reverse = sortDirection === "asc" ? 1 : -1;
  data.sort((a, b) => {
    let valueA = key(a) ? key(a) : "";
    let valueB = key(b) ? key(b) : "";
    return reverse * ((valueA > valueB) - (valueB > valueA));
  });
   return data;
}
setFields(configArr,mainArrr,i,j){
  let fieldArr = {
    Fieldlabel: "",
    fieldApiName: "",
    fieldtype: "",
    fieldorder: "",
    fieldValue: "",
    fieldhelptext: "",
    placeHolder:"",
    showStep:"",
    isRadio: "",
    max:"",
    rangeOverflow:"",
	stepMisMatch:"",
	 isDate: "",
    class: "",
    isTime: "",
    timeZone:"",
	min:"",
    rangeUnderflow:"",
	step:""
  };
  fieldArr.Fieldlabel = mainArrr[i].MasterLabel;
  fieldArr.fieldApiName = mainArrr[i].CCL_Field_Api_Name__c;
  fieldArr.fieldtype = mainArrr[i].CCL_Field_Type__c;
  fieldArr.class = "myClass";
    if (fieldArr.fieldtype == "Date" || fieldArr.fieldtype == "date") {
      fieldArr.isDate = true;
      fieldArr.Fieldlabel = 'Date Of Testing';
      fieldArr.class = "myClass dateClass";
    }
    if (fieldArr.fieldtype == "Time" || fieldArr.fieldtype == "time") {
      fieldArr.isTime = true;
      fieldArr.Fieldlabel ='Time';
      fieldArr.class = "myClass timeClass";
      if(this.timeZone!==undefined && this.timeZone[fieldArr.fieldApiName]!==undefined){
        fieldArr.timeZone=this.timeZone[fieldArr.fieldApiName];}
    }
  fieldArr.fieldorder = mainArrr[i].CCL_Order_of_Field__c;
  fieldArr.fieldhelptext = mainArrr[i].CCL_Help_Text__c;
  fieldArr.placeHolder = mainArrr[i].CCL_Place_Holder__c;
  fieldArr.showStep = mainArrr[i].CCL_Show_Step__c;
  fieldArr.max=mainArrr[i].CCL_Max_Allowed__c;
  fieldArr.rangeOverflow =this.errorlabels[mainArrr[i].CCL_When_Range_overflow__c] ;
	fieldArr.stepMisMatch =this.errorlabels[mainArrr[i].CCL_When_Step_Mismatch__c] ;
	fieldArr.min = mainArrr[i].CCL_Min_Value__c;
	fieldArr.rangeUnderflow =this.errorlabels[mainArrr[i].CCL_When_Range_Underflow__c];
  if(fieldArr.fieldApiName==="CCL_Hematocrit__c"){
    fieldArr.step=0.1;
  }
  else if(fieldArr.fieldApiName==="CCL_White_Blood_Cell_Count_l_Cryo__c"){
    fieldArr.step=0.01;
  }
  fieldArr.isRadio =
  mainArrr[i].CCL_Field_Type__c === "Checkbox" ? true : false;
  this.apiNames.push(fieldArr.fieldApiName);
  configArr[j].myFields.push(fieldArr);
}
    handleChange(event){
     this.commentValue=event.target.value;
    }

    onSave(event){
      this.saveLaterClicked=true;
    }
    onPrevious(event){
        //2260
      if(this.isADFViewerInternal){
        const attributeChangeEvent = new FlowAttributeChangeEvent('screenName', '4');
        this.dispatchEvent(attributeChangeEvent);
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
      }else{
        this.saveLaterClicked = true;
        this.previousClicked = true;
      }
    }
    onNext(event){
		let commentCmp=this.template.querySelector('.comment');
        let comment=commentCmp.value;
        if(((this.orignalFileresult!==undefined?this.orignalFileresult!=JSON.stringify(this.wiredFileResult):false)
        ||(comment!=undefined?this.orignalJson["Collection and Cryopreservation Comments"]!==comment:false))){
          this.commentOrFileChange=true;
        }
		
      this.saveClicked=true;
     // this.goToNextScreen();
    }
    handleCustomEvent(event) {
      let match=false;
      if(this.count==0){
        this.count=this.count+1;
         this.collectionInput[this.count]=event.detail;
      }else{
        for(let i=0;i<=this.count;i++){
          for (const [key, value] of Object.entries(this.collectionInput)) {
            if(value.Id==event.detail.Id){
              this.collectionInput[key]=event.detail;
              match=true;
            }
          }
        }
      }
      if(!match &&this.collectionInput[1].Id!=event.detail.Id){
        this.count=this.count+1;
        this.collectionInput[this.count]=event.detail;
      }
    this.myJson={collections:this.collectionInput};
      if(this.count===this.tableData.length){
        let val=this.orignalJson;
        let mytable=val["5"];
        let compare=mytable!=undefined&&this.myJson!=undefined?this.compareJson(this.myJson,mytable,this.tableData.length):true;
        let commentCmp=this.template.querySelector('.comment');
        let comment=commentCmp.value;
        
		
		if((!compare||(this.orignalFileresult!==undefined?this.orignalFileresult!=JSON.stringify(this.wiredFileResult):false)
        ||(comment!=undefined?this.orignalJson["Collection and Cryopreservation Comments"]!==comment:false))
        &&this.counter>0&&(this.saveLaterClicked)){
          this.isReasonModalOpen=true;
        }
        else{
      this.saveAsJson();}
      }
    }

    compareJson(orignalJson,newJson,length){
      for(let i=1;i<=length;i++){
        let oJson=orignalJson.collections[i];
        let nJson=newJson.collections[i];
        let oldDateArr=oJson.CCL_TEXT_Cryopreserve_Mat_Date_Time__c?oJson.CCL_TEXT_Cryopreserve_Mat_Date_Time__c.split(" "):'';
        let newDateArr=nJson.CCL_TEXT_Cryopreserve_Mat_Date_Time__c?nJson.CCL_TEXT_Cryopreserve_Mat_Date_Time__c.split(" "):'';
        if(oJson.CCL_Hematocrit__c!=nJson.CCL_Hematocrit__c||oJson.CCL_Platelets_x10_3_L_Cryo__c!=nJson.CCL_Platelets_x10_3_L_Cryo__c||
          oJson.CCL_Neutrophils__c!=nJson.CCL_Neutrophils__c||oJson.Id!=nJson.Id||oJson.CCL_Others__c!=nJson.CCL_Others__c||
          oldDateArr[0]!=newDateArr[0]||oldDateArr[1]!=newDateArr[1]||oldDateArr[2]!=newDateArr[2]||
          oJson.CCL_Hemoglobin_g_dL_Cryo__c!=nJson.CCL_Hemoglobin_g_dL_Cryo__c||oJson.CCL_White_Blood_Cell_Count_l_Cryo__c!=nJson.CCL_White_Blood_Cell_Count_l_Cryo__c||
          oJson.CCL_Absolute_Lymphocyte_Count_l_Cryo__c!=nJson.CCL_Absolute_Lymphocyte_Count_l_Cryo__c||oJson.CCL_Monocytes__c!=nJson.CCL_Monocytes__c||
          oJson.CCL_Lymphocytes__c!=nJson.CCL_Lymphocytes__c||oJson.CCL_Blasts__c!=nJson.CCL_Blasts__c||oJson.CCL_Viability__c!=nJson.CCL_Viability__c){
          return false;
        }
      }
      return true;
    }

    handleValidation(event){
      let validCount=0;
      let isNotvalid=false;
      this.validationInput[event.detail.Id]=event.detail.navigateFurther;
      if (this.tableData.length===Object.keys(this.validationInput).length){
        for (let [key, value] of Object.entries(this.validationInput)) {
          if(value===true){
            validCount=validCount+1;
          }
          else{
            isNotvalid=true;
          }
      }
      }
      if(isNotvalid){
       this.validationInput={};
      }
      if(validCount===this.tableData.length){
        let val=this.orignalJson;
        let mytable=val["5"];
        
          this.goToNextScreen();
       
      }
      else{
        this.saveClicked=false;
        }
  }
    //Added as part 227
    saveAsJson() {
      let commentBox=this.template.querySelector("lightning-textarea");
      this.comments= commentBox.value;
      if (this.jsonData == undefined || this.jsonData == null) {
        this.jsonObj[this.flowScreen] = this.myJson;
        this.jsonObj["Collection and Cryopreservation Comments"] = this.comments;
        this.jsonObj["latestScreen"] = this.flowScreen;
      } else {
        if (this.flowScreen in this.jsonObj) {
          delete this.jsonObj[this.flowScreen];
          this.jsonObj[this.flowScreen] = this.myJson;
          this.jsonObj[
            "Collection and Cryopreservation Comments"
          ] = this.comments;
          this.jsonObj["latestScreen"] = this.flowScreen;
        } else {
          this.jsonObj[this.flowScreen] = this.myJson;
          this.jsonObj[
            "Collection and Cryopreservation Comments"
          ] = this.comments;
            this.jsonObj["latestScreen"] = this.flowScreen;
        }
      }
        const attributeChangeEvent = new FlowAttributeChangeEvent(
        "jsonData",
        JSON.stringify(this.jsonObj)
      );
      this.dispatchEvent(attributeChangeEvent);
      //2260 
      if(this.isADFViewerInternal){
        if(this.count===this.tableData.length && this.saveLaterClicked && !this.previousClicked){
          this.goToListScreen();
        }
        if(this.count===this.tableData.length && this.saveLaterClicked && this.previousClicked){
          const attributeChangeEvent = new FlowAttributeChangeEvent('screenName', '4');
          this.dispatchEvent(attributeChangeEvent);
          const navigateNextEvent = new FlowNavigationNextEvent();
          this.dispatchEvent(navigateNextEvent);
          }
    if(this.saveClicked){
            this.goToNextScreen();
          }
      }else{
        const attributeChangeEvent = new FlowAttributeChangeEvent(
          "jsonData",
          JSON.stringify(this.jsonObj)
        );
        this.dispatchEvent(attributeChangeEvent);
        saveForLater({
          myJSON: JSON.stringify(this.jsonObj),
          recordId: this.recordId
        })
          .then((result) => {
            //updating the save json varible
            this.error = null;
            if(this.count===this.tableData.length && this.saveLaterClicked && !this.previousClicked){
              this.goToListScreen();
            }
            if(this.count===this.tableData.length && this.saveLaterClicked && this.previousClicked){
              const attributeChangeEvent = new FlowAttributeChangeEvent('screenName', '4');
              this.dispatchEvent(attributeChangeEvent);
              const navigateNextEvent = new FlowNavigationNextEvent();
              this.dispatchEvent(navigateNextEvent);
              }
        if(this.saveClicked){
                this.goToNextScreen();
              }
          })
          .catch((error) => {
            this.error = error;
          });
      }
       
    }
    goToNextScreen() {
      //2260
    if (this.isADFViewerInternal) {
      const attributeChangeEvent = new FlowAttributeChangeEvent(
        "screenName",
        "6"
      );
      this.dispatchEvent(attributeChangeEvent);
      const navigateNextEvent = new FlowNavigationNextEvent();
      this.dispatchEvent(navigateNextEvent);
    } else {
      this.jsonObj["latestScreen"] = "6";
      const attributeChangeEvent = new FlowAttributeChangeEvent(
        "jsonData",
        JSON.stringify(this.jsonObj)
      );
      this.dispatchEvent(attributeChangeEvent);
      saveForLater({
        myJSON: JSON.stringify(this.jsonObj),
        recordId: this.recordId
      })
        .then((result) => {
          const attributeChangeEvent = new FlowAttributeChangeEvent(
            "screenName",
            "6"
          );
          this.dispatchEvent(attributeChangeEvent);
          const navigateNextEvent = new FlowNavigationNextEvent();
          this.dispatchEvent(navigateNextEvent);
        })
        .catch((error) => {
          this.error = error;
        });
    }
      }
      goToListScreen(){
        this[NavigationMixin.Navigate]({
      type: "comm__namedPage",
      attributes: {
        name: "Home"
      },
      state: {
        "tabset-8e13c": "4d03f"
      }
    });
        }
		
	//changes as part of 850
  closeModal() {
	  this.count=0;
    this.saveLaterClicked=false;
    this.previousClicked=false;
    this.saveClicked=false;
    this.isReasonModalOpen=false;
  }
  handleReasonChange(event){
    this.ReasonForMod=event.target.value;
  }
  validateTextarea(){
    const textArea=this.template.querySelector('.Reason');
    let commentValue=textArea.value;
    if(commentValue==undefined){
      this.navigater=false;
      textArea.setCustomValidity('Complete this Field');
      textArea.reportValidity();
    }else{
      this.navigate=true;
      textArea.setCustomValidity('');
      textArea.reportValidity();
    }
  }
  onReasonSubmit(event){
    this.validateTextarea();
    if(this.navigate){
    this.updateReason();}
  }
   updateReason(){
    const fields = {};
    fields[ID.fieldApiName] = this.recordId;
    fields[reasonForModification.fieldApiName] =this.ReasonForMod;
    const recordInput = { fields };
   //2260
    if (this.isADFViewerInternal) {
      this.saveAsJson();
    } else {
      updateRecord(recordInput)
        .then(() => {
          if (this.saveClicked) {
            this.updateCollectionValues();
          } else {
            this.saveAsJson();
          }
        })
        .catch((error) => {
          this.error = error;
         });
    }
      }
    //changes for 850 end here
	
	updateCollectionValues(){
    for(let i=1; i<=this.tableData.length;i++){
      let fields=this.myJson.collections[i];
      let recordInput={fields};
      updateRecord(recordInput)
      .then(()=>{
        if(i==this.tableData.length){
          this.saveAsJson();
        }
      }).catch((error) => {
        this.error = error;
      });
    }
  }
  
   handleUpdateReason(event){
    this.count = this.count + 1;
	this.collectionInput[event.detail.index] = event.detail.fields;
	this.myJson = { collections: this.collectionInput };
	if(this.count===this.tableData.length){
	let val=this.orignalJson;
	let mytable=val["5"];
	let compare;
	if(mytable!=undefined && mytable.collections[this.tableData.length]!=undefined){
    compare=this.myJson!=undefined?this.compareJson(this.myJson,mytable,this.tableData.length):true;
	}else{
		compare=true;
	}
	if((!compare||(this.orignalFileresult!==undefined?this.orignalFileresult!=JSON.stringify(this.wiredFileResult):false)
	||(this.comments!=undefined?this.orignalJson["Collection and Cryopreservation Comments"]!==this.comments:false))
	&&this.counter>0){
		this.isReasonModalOpen=true;
	}
	else{
		this.saveAsJson();}
	}
	}



  //Changes as part of CGTU 355
  removeFile(event){
    if(event.target.name === 'openConfirmation'){
      this.docVersionId=event.target.alternativeText;
      this.isDialogVisible = true;
    }else if(event.target.name === 'confirmModal'){
      //when user clicks outside of the dialog area, the event is dispatched with detail value  as 1
      if(event.detail !== 1){
           if(event.detail.status === 'confirm') {
              deleteFile({contentDocumentId:this.docVersionId}).then((result) => {
              this.handleDeleteFinished();
              this.isDialogVisible = false;
              this.error = null;
            })
            .catch((error) => {
              this.error = error;
            });
          //Changes as part of 355 ends here
              this.isDialogVisible = false;
          }else if(event.detail.status === 'cancel'){
            this.isDialogVisible = false;
          }
      }
      //hides the component
      this.isDialogVisible = false;
  }
  }
  //Changes as part of CGTU 355 end here
   //2260
  gotoADFSummary() {
    const screenNextEvent = new FlowAttributeChangeEvent("screenName", "6");
    this.dispatchEvent(screenNextEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }
}

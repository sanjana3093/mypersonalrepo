import { LightningElement ,track} from 'lwc';


import copyWright from "@salesforce/label/c.CCL_Copyright_text";
import allrightsReserved from "@salesforce/label/c.CCL_All_Rights_Reserved";
export default class CCL_Community_Footer extends LightningElement {
    @track copyWright=copyWright;
    @track allrightsReserved=allrightsReserved;
    @track currentYear=new Date().getFullYear();
    @track privacyPolicy='https://www.salesforce.com/company/privacy/';
    @track termsofUse='https://www.salesforce.com/content/dam/web/en_us/www/documents/legal/salesforce_MSA.pdf';
}
import { LightningElement, wire, track } from 'lwc';
import getContactList from '@salesforce/apex/ContactController.getContactList';
import FAMILY_NAME_FIELD__c from '@salesforce/schema/Product2.PSP_Family__r.Name';
import { updateRecord } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


const COLS = [
    {label: 'Product Name', fieldName: 'Name', type: 'text',sortable:true},
    {label: 'Active', fieldName: 'IsActive', type: 'boolean',editable:true},
    {label: 'Franchise', fieldName: 'PSP_Franchise__c',type: 'text',sortable:true},
    {label: 'Portfolio', fieldName: 'PSP_Portfolio__c',type: 'text',sortable:true},
    {label: 'Product Family', fieldName: [FAMILY_NAME_FIELD__c],type: 'text',sortable:true}

];
export default class Datatable extends LightningElement {

    @track error;
    @track columns = COLS;
    @track draftValues = [];
    @track sortedBy;
    @track sortDirection = 'asc';
    @track showSpinner=true;

    @wire(getContactList)
    product({
        error,
        data
    })
    
    {if (data) {
        this.showSpinner=false;
        this.data = data;
     }else if (error) {
        this.error = error;
    }
    }
    //Method for sorting
    sortData(fieldName, sortDirection){
        var data = JSON.parse(JSON.stringify(this.data));
        //function to return the value stored in the field
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        data.sort((a,b) => {
            let valueA = key(a) ? key(a).toLowerCase() : '';
            let valueB = key(b) ? key(b).toLowerCase() : '';
            return reverse * ((valueA > valueB) - (valueB > valueA));
        });
        //set sorted data to opportunities attribute
        this.data = data;
    }
    //this method is called when a column sort icon is clicked
    sortThisColumn(event){
            this.sortedBy = event.detail.fieldName;
            this.sortDirection = event.detail.sortDirection;
            this.sortData(this.sortedBy,this.sortDirection);       
        }
        handleSave(event) {
            const recordInputs =  event.detail.draftValues.slice().map(draft => {
                const fields = Object.assign({}, draft);
                return { fields };
            });
        
            const promises = recordInputs.map(recordInput => updateRecord(recordInput));
            Promise.all(promises).then(product => {
                console.log('product:-  ',product,this.data);
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Products updated',
                        variant: 'success'
                    })
                );
                 // Clear all draft values
                 this.draftValues = [];
    
                // Display fresh data in the datatable
                 return refreshApex(this.data);
            
                
                 
            }).catch(error => {
                // Handle error
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error updating record',
                        message: error.body.message,
                        variant: 'error'
                    }));
            });

        }
        
  
}
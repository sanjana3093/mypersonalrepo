import {
    LightningElement,
    api,
    track
} from 'lwc';
import errMsg from "@salesforce/label/c.PSP_Handed_out_Err_Msg";
import handedoutBy from "@salesforce/label/c.PSP_Handed_out_By";
import searchtext from "@salesforce/label/c.PSP_Search_Contacts";

export default class PSP_HandedOutByLookup extends LightningElement {

    @api recordtype = "HCP";
    @api object = "Contact";
    @track inputVal = '';
    @track rtnValue;
    @api selectedHandedOutById = null;
    @api handedoutName;
    @api searchtext = searchtext;
    @api retInputVal;
    @track handedoutBy=handedoutBy;
    @track errMsg=errMsg;
    @track inputHandedOutById;

    getselectedDetails(event) {
        this.selectedHandedOutById = event.detail.Id;
        this.handedoutName = event.detail.Name;
    }
    checkForInput(event) {
        this.inputVal = event.detail;
    }

    @api
    validate() {
        console.log('the vals are'+this.selectedHandedOutById+this.inputVal.length+this.inputVal);
        if (this.selectedHandedOutById === null && (this.inputVal === null || this.inputVal === '' || this.inputVal.length == 0)) {
            return {
                isValid: true
            };
        } else if (this.selectedHandedOutById === null && this.inputVal.length > 0) {
            //If the component is invalid, return the isValid parameter as false and return an error message. 
            return {
                isValid: false,
                errorMessage: errMsg
            };
        } 
    }
}
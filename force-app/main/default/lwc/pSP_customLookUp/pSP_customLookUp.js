import { LightningElement, track, api } from 'lwc';
import fetchRecords from "@salesforce/apex/PSP_custLookUpCntrl.fetchLookupValues";
export default class PSP_customLookUp extends LightningElement {
  @api greeting = "";
  @track products = [];
  @track index = 0;
  @track dynamicClass;
  @track proservicedetail = [];
  @api recordId;
  @track iconName = "standard:product";
  @api tabproductservice;
  @api selectedItems = [];
  @track nonselected = true;
  @api selectedItem;
  @api productServicename="";
  @api recordtype='';
  @api object='';
  @api searchtext = '';
  @api tableclass;
  @api cppid;
  @api disableSelection=false;
  @api fieldvalue;
  @track count=0;
  @track showSpinner = true;
    @api
    get UiClass() {
      
      if (this.products.length > 0 && this.greeting && this.nonselected) {
        this.dynamicClass =
          "slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-lookup__menu displayblock "+this.tableclass;
      } else {
        this.dynamicClass =
          "slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-lookup__menu";
      }
      return this.dynamicClass;
    }
    get lookupInput() {
      return 'lookupinput '+this.tableclass;
    }
    changeHandler(event) {
        this.greeting = event.target.value;
       
        if (this.greeting.length >= 2) {
          this.nonselected = true;
          this.showSpinner=true;
          const evt2 = new CustomEvent("typedinput", {
            // detail contains only primitives
            detail: this.greeting,
            composed: true,
           bubbles: true
          });
         
          this.dispatchEvent(evt2);
          if(this.recordtype===null || this.recordtype===' '){
            this.recordtype='none';
          }
          setTimeout(() => {   
          fetchRecords({
            recordType: this.recordtype,
            searchKeyword: this.greeting,
            objectName:this.object
            
          })
         
            .then(result => {
              this.products = result;
              this.error = null;
              console.log('callback got 1 with timeout');
              this.count=this.count+1;
              console.log('count8'+this.count);
              this.showSpinner = false;
              return this.promiseFunc();
            })
            .catch(error => {
              this.error = error;
              this.products = null;
              console.log('callback got');
              this.showSpinner = false;
            });
          }, 500);
        } else {
          this.nonselected = false;
        }
        console.log('this list is'+JSON.stringify(this.products));
      }
      promiseFunc() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve('foo');
            }, 3000);
        });
    }
      handleClickProducts(event){
          let i=0;
          let selectedVars;
          let tempVal={'careProgId':this.cppid};
        this.selectedItem = event.target.dataset.item;
        if(event.target.tagName==='LIGHTNING-ICON'){
          this.selectedItem = event.target.parentNode.dataset.item;
        }

        for(i=0;i<this.products.length;i++){
            if(this.products[i].Id== this.selectedItem){
                this.greeting=this.products[i].Name;
                this.disableSelection=true;
                console.log('in line 82'+this.products[i]);
                this.productServicename=this.products[i].Name;
                selectedVars=this.products[i];
            }
        }

        this.nonselected = false;
        console.log('the selectedVars--==-'+JSON.stringify(selectedVars));
        if(this.cppid){
          //selectedVars=null;
        
          //selectedVars.careProgId=this.cppid;
          selectedVars= Object.assign(tempVal, selectedVars);
          
        }
        const evt = new CustomEvent("selecteditemid", {
            // detail contains only primitives
            detail: selectedVars,
            composed: true,
           bubbles: true
          });
         
          this.dispatchEvent(evt);
      }
      clearAll(){
        this.selectedItem=[];
        this.greeting=null;
        this.disableSelection=false;
        const evt = new CustomEvent("selecteditemid", {
          // detail contains only primitives
          detail: this.selectedItem,
          composed: true,
         bubbles: true
        });
       
        this.dispatchEvent(evt);
      }
      @api
      validate() {
          if (this.selectedItem !== undefined && this.selectedItem !== null) {
              return {
                  isValid: true
              };
          } else if(this.selectedItem===null && this.greeting!==null) {
              //If the component is invalid, return the isValid parameter as false and return an error message. 
              return {
                  isValid: false,
                  errorMessage: 'Please select a valid option.'
              };
          }
      }
      handleRowActions(event){
        console.log('in sub rowww'+event.detail.row);
    }
}
import {
    LightningElement,
    api,
    wire,
    track
} from 'lwc';
import fetchRecords from "@salesforce/apex/PSP_custLookUpCntrl.getEnrolledPrescriptions";
import errMsg from "@salesforce/label/c.PSP_No_Prescription_Found";
import enrolledList from "@salesforce/label/c.PSP_Enrolled_Prescription_Associated";
import enrolledListForProduct from "@salesforce/label/c.PSP_Select_Product_Remove";
import presQty from "@salesforce/label/c.PSP_Prescription_Qunatity";
import presFreq from "@salesforce/label/c.PSP_Prescription_Frequency";
import units from "@salesforce/label/c.PSP_Units";
import cprovider from "@salesforce/label/c.PSP_Care_Program_Provider";
import enrolleProgram from "@salesforce/label/c.PSP_Enrolled_Program";

export default class PSP_GetPrescriptionList extends LightningElement {
    @api recordId;
    @api recordType;
    @api enrolledprescriptionlist;
    @api enrolledprescriptionName;
    @api productServiceId;
    @api enrolledprogramlist;
    @api showEpCmp = false;
    @track resultData = [];
    @track units=units;
    @track enrolleProgram=enrolleProgram
    @track cprovider=cprovider;
    @track presQty=presQty;
    @track presFreq=presFreq;
    @api selectedprecription;
    @api selectedprecriptionIdList = [];
    @api selectedprecriptionNameList = [];
    @track prescription;
    @track enrolledProgramProduct=enrolledListForProduct;
    @api selectedPrescriptionName;
    @track count = 0;
    @api gotResults = false;
    @track enrolledListservice = enrolledList;
    @track errMsg = errMsg;
    @track rtnValue;
    @track product = false;
    @api enrolledProgramName;
    get productService(){
        return this.recordType==='Product'? true:false;
    }
    connectedCallback() {
        // subscribe to searchKeyChange event
        console.log('the enrolle Id' + this.enrolledprogramlist);
        if (this.recordType === 'Product') {
            this.product = true;
        }
        fetchRecords({
                recordId: this.recordId,
                enroleeId: this.enrolledprogramlist,
                recordType: this.recordType

            })
            .then(result => {
                this.resultData = result;
                this.rtnValue = result;
                this.gotResults = false;
                this.selectedprecriptionIdList=[];
                this.selectedprecriptionNameList = [];
                console.log('the results are'+JSON.stringify(result));
                if (result.length > 1) {
                    this.gotResults = true;
                    this.enrolledprescriptionlist = result;
                    //this.enrolledProgramName=result[0].CareProgramEnrollee.Name;
                } else if (result.length === 1) {
                    this.gotResults = true;
                    this.prescription = result[0];
                    this.selectedprecription = result[0].Id;
                    this.selectedprecriptionIdList[this.count] = result[0].Id;
                    this.selectedprecriptionNameList[this.count] = result[0].Name;
                    //this.enrolledProgramName=result[0].CareProgramEnrollee.Name;
                }

                this.error = null;
            })
            .catch(error => {
                this.error = error;
                this.enrolledprescriptionlist = null;
            });

    }
    handleChange(event) {
        let index = event.target.dataset.item;
        let i = 0;
console.log('index'+index+event.target.checked+ this.selectedprecriptionIdList[this.count]+this.enrolledprescriptionlist[index].Id);
        if (event.target.checked) {

            this.selectedprecriptionIdList[this.count] = this.enrolledprescriptionlist[index].Id;
            this.selectedprecriptionNameList[this.count] = this.enrolledprescriptionlist[index].Name;
            this.count = this.count + 1;
        } else {

            for (i = 0; i < this.selectedprecriptionIdList.length; i++) {
                if (this.selectedprecriptionIdList[i] === this.enrolledprescriptionlist[index].Id) {

                    this.selectedprecriptionIdList.splice(i, 1);
                    this.selectedprecriptionNameList.splice(i, 1);

                    this.count = this.count - 1;
                }
            }
        }

    }
    //validate function
    @api
    validate() {
        if (this.selectedprecriptionIdList !== undefined && this.selectedprecriptionIdList !== null && this.selectedprecriptionIdList.length) {
            return {
                isValid: true
            };
        } else if (this.resultData.length) {
            //If the component is invalid, return the isValid parameter as false and return an error message. 
            return {
                isValid: false,
                errorMessage: 'Please select an Enrolled Prescrition.'
            };
        }
    }

}
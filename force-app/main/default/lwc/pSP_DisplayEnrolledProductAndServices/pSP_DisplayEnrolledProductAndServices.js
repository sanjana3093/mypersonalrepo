import { LightningElement, track, api, wire } from 'lwc';
import header from '@salesforce/label/c.PSP_Enrolled_Prescriptions_Enrollment';
import getRecords from "@salesforce/apex/PSP_CreateEnrolledPrescAndServices.createEnrolledPrescAndServices";
export default class PSP_DisplayEnrolledProductAndServices extends LightningElement {
@api enrolleeId;
@api careProgramId;
@api patientName;
@api physicianFlow = false;
@track eplist = [];
@track showSpinner = true;
label = {
  header,
};
connectedCallback() {
getRecords ({ enrolleeId: this.enrolleeId ,careProgramId: this.careProgramId,patientName: this.patientName})
      .then(result => {
        this.eplist = result;
        this.error = null;
        this.showSpinner = false;
      })
      .catch(error => {
        this.error = error;
        this.eplist = null;
        this.showSpinner = false;
      });
    }
}
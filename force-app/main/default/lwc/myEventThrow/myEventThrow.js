import { LightningElement,api,wire } from 'lwc';
import {  fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
export default class MyEventThrow extends LightningElement {
@api eventName;
@wire(CurrentPageReference) pageRef;
connectedCallback() {
    fireEvent(this.pageRef,'screenName' , this.eventName);
}

}
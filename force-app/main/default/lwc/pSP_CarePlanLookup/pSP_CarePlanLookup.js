import { LightningElement,api } from 'lwc';

export default class pSP_CarePlanLookup extends LightningElement {
    @api selectedItem;
    @api productServicename="";
  
    getselectedDetails(event){
        this.selectedItem=event.detail.Id;
        this.productServicename=event.detail.Name;
    }
}
import { LightningElement,api,track } from 'lwc';
import fetchRecords from "@salesforce/apex/PSP_custLookUpCntrl.getHandedOutByName";
import activation_Code from "@salesforce/label/c.PSP_Activation_Code";
import valid_Untill from "@salesforce/label/c.PSP_Valid_Untill";
import provider_label from "@salesforce/label/c.PSP_Provider";
import available_Tests from "@salesforce/label/c.PSP_Available_Tests";
import assigned_To from "@salesforce/label/c.PSP_Assigned_To";
import activation_Date from "@salesforce/label/c.PSP_Activation_Date";PSP_Handed_out_By
import handed_Out_By from "@salesforce/label/c.PSP_Handed_out_By";
import errMsg from "@salesforce/label/c.PSP_Handed_out_Err_Msg";
import getpicklist from "@salesforce/apex/PSP_custLookUpCntrl.getPickListValuesIntoList";
export default class PSP_Display_Coupon_Info extends LightningElement {
    @api assignedTo;
    @api couponNumber;
    @api provider;
    @api activationCode;
    @api activationDate;
    @api validUntil;
    @api couponId;
    @api availableTests;
    @api selectedHandedOutById=null;
    @api recordtype="HCP";
    @api object="Contact";
    @track inputVal='';
    @track rtnValue;
    @api handedoutName;
    @api searchtext="Search a HCP";
    @api retInputVal;
    @track inputHandedOutById;
    @track activation_Code = activation_Code;
    @track valid_Untill = valid_Untill;
    @track available_Tests = available_Tests;
    @track provider_label = provider_label;
    @track assigned_To = assigned_To;
    @track activation_Date = activation_Date;
    @track handed_Out_By = handed_Out_By;
    @track errMsg=errMsg;
    getselectedDetails(event){
         this.selectedHandedOutById=event.detail.Id;
     }
     checkForInput(event){
        this.inputVal=event.detail;
     }
     connectedCallback(){
        this.inputHandedOutById = this.selectedHandedOutById;
        fetchRecords({
            handedOutById: this.selectedHandedOutById,

        })
        .then(result => {
            this.rtnValue = result;
            if (result.length === 0) {
                this.rtnValue = null;
            }else{
                if(this.rtnValue !== null || this.rtnValue === ''){
                    this.retInputVal=this.rtnValue
                }else{
                   this.searchtext="Search a HCP"
                }
            }

            this.error = null;
        })
        .catch(error => {
            this.error = error;
        });
        getpicklist({
            couponId: this.couponId,
      
        })
        .then(result => {
          console.log('all vals are'+JSON.stringify(result));
           this.availableTests=result;
           this.availableTests=result.join(',');
           console.log('the avaiable test are'+this.availableTests);
            this.error = null;
        })
        .catch(error => {
            this.error = error;
      
        });
        
     }
    @api
    validate() {
        console.log('in iffff');
        console.log('this.inputHandedOutById: '+this.inputHandedOutById);
        console.log('this.selectedHandedOutById: '+this.selectedHandedOutById);
        console.log('this.inputVal: '+this.inputVal);
        console.log('this.retInputVal: '+this.retInputVal);
        console.log('this.inputVal len: '+this.inputVal.length);
        if (this.selectedHandedOutById===null &&  (this.inputVal===null || this.inputVal==='' )) {
            return {
                isValid: true
            };
        } else if(this.selectedHandedOutById===null && this.inputVal!==null)
        {
            console.log('in if 1');
            //If the component is invalid, return the isValid parameter as false and return an error message. 
            return {
                isValid: false,
                errorMessage: errMsg
            };
        }
        else if(this.inputHandedOutById === this.selectedHandedOutById && this.inputVal !== this.retInputVal 
            && (this.inputVal !== null && this.inputVal !== undefined && this.inputVal!=='' && this.inputVal.length>0)){
           
            console.log('this.inputHandedOutById: '+this.inputHandedOutById);
            console.log('this.selectedHandedOutById: '+this.selectedHandedOutById);
            console.log('this.inputVal: '+this.inputVal);
            console.log('this.retInputVal: '+this.retInputVal);
           // console.log('isemp: '+this.inputVal.isEmpty());
            //If the component is invalid, return the isValid parameter as false and return an error message. 
            return {
                isValid: false,
                errorMessage: errMsg
            };
        }
    }
}
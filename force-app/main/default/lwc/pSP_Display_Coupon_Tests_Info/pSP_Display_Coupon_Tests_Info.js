import { LightningElement,api ,wire, track} from 'lwc';
import {
    getObjectInfo
  } from 'lightning/uiObjectInfoApi';
  import PRODUCT_OBJECT from '@salesforce/schema/PSP_Coupon_Test__c';
 
  import getpicklist from "@salesforce/apex/PSP_custLookUpCntrl.getPickListValuesIntoList";
  import {
    getPicklistValues
  } from 'lightning/uiObjectInfoApi';
  import TEST_REQUESTED from '@salesforce/schema/PSP_Coupon_Test__c.PSP_Test_Requested__c';
  import testRequestedVal from "@salesforce/label/c.PSP_Test_Requested";
  import activeStatus from "@salesforce/label/c.PSP_Activated";
export default class PSP_Display_Coupon_Tests_Info extends LightningElement {
    @api couponNumber;
    @api couponStatus;
    @api provider;
    @api activationCode;
    @api validUntil;
    @api couponId;
    @api availableTests=[];
    @api availableTestsMulti=[];
    @api testRequested=null;
    @track selectedLabel;
    @api testRequestedValues;
    @api recordtype="Business";
    @api object="Account";
    @api searchtext="Search Test Provider";
    @api testProvider;
    @track testRequestedVal=testRequestedVal;
    @track activeStatus=activeStatus;
    @api selectedTestProviderItem;
    @api selectedTestProviderName="";
    @track inputVal='';
    @wire(getObjectInfo, {
        objectApiName: PRODUCT_OBJECT
      })
      objectInfo;
      @wire(getPicklistValues, {
        recordTypeId: '$objectInfo.data.defaultRecordTypeId',
        fieldApiName: TEST_REQUESTED
      })
      IndustryPicklistValues(result) {
        this.testRequestedValues = result;
        
      
      }
      onselectservice(event){
        this.testRequested = event.target.value;
        let tempVal=this.testRequestedValues.data.values;
        let i;
        for(i=0;i<tempVal.length;i++){
          if(this.testRequested===tempVal[i].value){
            this.selectedLabel=tempVal[i].label;
          }
        }
     
      }
      getselectedDetails(event){
        // console.log('the event details are'+JSON.stringify(event.detail));
         this.testProvider=event.detail.Id;
         this.selectedTestProviderName=event.detail.Name;
        // console.log(this.productServicename+ this.selectedItem);
     }
     checkForInput(event){
      this.inputVal=event.detail;
   }

  //  @wire(getpicklist, {
  //  couponId:this.couponId
  // })
  // getAllVals(result) {
  // console.log('all vals are'+JSON.stringify(result));
  // this.availableTests=result.data;
  // }
   connectedCallback() {
     getpicklist({
      couponId: this.couponId,

  })
  .then(result => {
     this.availableTests=result;

      this.error = null;
  })
  .catch(error => {
      this.error = error;

  });
}
    @api
    validate() {
        let validVar=true;
        console.log('the vals'+JSON.stringify(this.availableTests)+'--'+this.selectedLabel+this.testRequestedVal);
        console.log(this.availableTests.includes(this.selectedLabel));
        if(JSON.stringify(this.availableTests).includes(this.selectedLabel)){
            console.log('true');
        }else{
            console.log('in else for test req');
            validVar=false;
        }
        console.log('this.testprov: '+this.testProvider);
      //   if ((this.testProvider===null || this.testProvider===undefined) &&  (this.inputVal===null || this.inputVal==='' || this.inputVal.lenghth===0)) {
      //     console.log('if 1');
      //     console.log('this.testprov: '+this.testProvider);
      //     console.log('this.inp: '+this.inputVal);
      //     return {
      //         isValid: true
      //     };
      // } else if((this.testProvider===null || this.testProvider===undefined) && this.inputVal!==null)
      // {
      //   console.log('if 2');
      //   console.log('this.testprov: '+this.testProvider);
      //     console.log('this.inp: '+this.inputVal);
      //     //If the component is invalid, return the isValid parameter as false and return an error message. 
      //     return {
      //         isValid: false,
      //         errorMessage: 'Please select correct Test Provider.'
      //     };
      // }
        if (validVar) {
            // return {
            //     isValid: true
            // };
            if ((this.testProvider===null || this.testProvider===undefined) &&  (this.inputVal===null || this.inputVal==='' || this.inputVal.lenghth===0)) {
            
              return {
                  isValid: true
              };
          } else if((this.testProvider===null || this.testProvider===undefined) && this.inputVal!==null)
          {
            console.log('if 2');
            console.log('this.testprov: '+this.testProvider);
              console.log('this.inp: '+this.inputVal);
              //If the component is invalid, return the isValid parameter as false and return an error message. 
              return {
                  isValid: false,
                  errorMessage: 'Please select correct Test Provider.'
              };
          }
        } else if(!validVar) {
            //If the component is invalid, return the isValid parameter as false and return an error message. 
            return {
                isValid: false,
                errorMessage: 'Please select Test Requested from Available Test'
            };
       }



         if ((this.testProvider===null || this.testProvider===undefined) &&  (this.inputVal===null || this.inputVal==='' || this.inputVal.lenghth===0)) {
          console.log('if 1');
          console.log('this.testprov: '+this.testProvider);
          console.log('this.inp: '+this.inputVal);
          return {
              isValid: true
          };
      } else if((this.testProvider===null || this.testProvider===undefined) && this.inputVal!==null)
      {
        console.log('if 2');
        console.log('this.testprov: '+this.testProvider);
          console.log('this.inp: '+this.inputVal);
          //If the component is invalid, return the isValid parameter as false and return an error message. 
          return {
              isValid: false,
              errorMessage: 'Please select correct Test Provider.'
          };
      }
       
    }
}
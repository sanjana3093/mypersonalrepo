import { LightningElement, api, track, wire} from "lwc";
import { getRecord, getFieldValue,updateRecord } from "lightning/uiRecordApi";
// Schema Definition for Fields used on this page
import Collection_Center_FIELD from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__r.Name";
import Collection_Center_FIELD_shippingStreet from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__r.ShippingStreet";
import Collection_Center_FIELD_shippingCity from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__r.ShippingCity";
import Collection_Center_FIELD_shippingState from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__r.ShippingState";
import Collection_Center_FIELD_shippingPostalCode from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__r.ShippingPostalCode";
import Collection_Center_FIELD_shippingCountry from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__r.ShippingCountry";
import Apheresis_Pickup_Location_FIELD from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Pickup_location__r.Name";
import Pickup_shippingStreet from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Pickup_location__r.ShippingStreet";
import Pickup_shippingCity from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Pickup_location__r.ShippingCity";
import Pickup_shippingState from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Pickup_location__r.ShippingState";
import Pickup_shippingPostalCode from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Pickup_location__r.ShippingPostalCode";
import Pickup_shippingCountry from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Pickup_location__r.ShippingCountry";
import Other_FIELD from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Other__c";
import Collection_Cryopreservation_Comments_FIELD from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Collection_Cryopreservation_Comments__c";
import CCL_Apheresis_Collection_Center__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Apheresis_Collection_Center__c";
import CCL_Label_Compliant__c from "@salesforce/schema/Account.CCL_Label_Compliant__c";
import CCL_DIN__c from "@salesforce/schema/CCL_Summary__c.CCL_DIN__c";
import CCL_Apheresis_ID__c from "@salesforce/schema/CCL_Summary__c.CCL_Apheresis_ID__c";
import CCL_SEC__c from "@salesforce/schema/CCL_Summary__c.CCL_SEC__c";
import approvedBy from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_ADF_Approved_By__c";
import approvedDate from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_ADF_Approved_Date_Time__c";
import approvedDateText from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_TEXT_ADF_Approved_Date_Time__c";
import submittedBy from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_ADF_Submitted_By__c";
import submittedDate from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_ADF_Submitted_Date_Time__c";
import submittedDateText from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_TEXT_ADF_Submitted_Date_Time__c";
import apheresisStatus from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Status__c";
import ID from "@salesforce/schema/CCL_Apheresis_Data_Form__c.Id";
import approvalCounter from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Approval_Counter__c";
import rejectedBy from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_ADF_Rejected_By__c";
import rejectedDate from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_ADF_Rejected_Date_Time__c";
import rejectionReason from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Rejection_Reason__c";
import rejectedDateText from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_TEXT_ADF_Rejected_Date_Time__c";
// Labels added for this Page
import CCL_other from "@salesforce/label/c.CCL_Other";
import CCL_Aph_Comments from "@salesforce/label/c.CCL_Aph_Comments";
import CCL_Collection from "@salesforce/label/c.CCL_Collection_label";
import CCL_Aph_Id from "@salesforce/label/c.CCL_Apheresis_ID_DIN_SEC";
import CCL_Vol from "@salesforce/label/c.CCL_Volume_of_blood";
import CCL_coll_Date from "@salesforce/label/c.CCL_End_of_Apheresis_Coll_Date";
import CCL_coll_Time from "@salesforce/label/c.CCL_End_of_Apheresis_Coll_Time";
import aphDetail from "@salesforce/label/c.CCL_Apheresis_Material_Cryobag_Collection";
import collection from "@salesforce/label/c.CCL_Collection";
import excesSection from "@salesforce/label/c.CCL_Excess_reatined";
import excesLabel from "@salesforce/label/c.CCL_Excess_Aph_material_retained_at_site";
import grandTotal from "@salesforce/label/c.CCL_Grand_totals_across_all_bags_and_collection_days";
import tnc from "@salesforce/label/c.CCL_Total_Nucleated_Cell_Count";
import cd3threshhold from "@salesforce/label/c.CCL_Total_CD3_Cell_Count_Threshold";
import wbcConcentration from "@salesforce/label/c.CCL_WBC_Concentration";
import aphdatetime from "@salesforce/label/c.CCL_Apheresis_Expiry_Date_Time";
import tnc109 from "@salesforce/label/c.CCL_TNC_10_9";
import CCL_CD3_suffix from "@salesforce/label/c.CCL_CD3_suffix";
import CCL_coll_Center from "@salesforce/label/c.CCL_Apheresis_Collection_Center";
import CCL_pickup_Center from "@salesforce/label/c.CCL_Apheresis_Pickup_Location";
import collectionName from '@salesforce/label/c.CCL_Collection_Blood_Testing';
import CCL_aph_sys from "@salesforce/label/c.CCL_Apheresis_System_Used";
import CCL_Collection_Details_Complete from '@salesforce/label/c.CCL_Collection_Details_Complete';
import CCL_Modal_Close from '@salesforce/label/c.CCL_Modal_Close';
import CCL_Summary_Submission_Header from '@salesforce/label/c.CCL_Summary_Submission_Header';
import CCL_Summary_Approval_Header from '@salesforce/label/c.CCL_Summary_Approval_Header';
import CCL_Manufacturing_Started_Label from '@salesforce/label/c.CCL_Manufacturing_Started_Label';
import CCL_Registered_Trademark_Symbol from '@salesforce/label/c.CCL_Registered_Trademark_Symbol';
import relatedDocuments from "@salesforce/label/c.CCL_Related_Documents";
import rejectionReasons from "@salesforce/label/c.CCL_Rejection_Reason_Summary";
import CCL_Rejected_By from "@salesforce/label/c.CCL_Rejected_By";
import CCL_Reason_for_Rejection from "@salesforce/label/c.CCL_Reason_for_Rejection";
import CCL_To_Be_Confirmed_Label from "@salesforce/label/c.CCL_To_Be_Confirmed_Label";
import CCL_ADF_Submitter_Permission from "@salesforce/label/c.CCL_ADF_Submitter_Permission";
import cd3tnc from "@salesforce/label/c.CCL_CD3_TNC_X100_3";
import CCL_APH_onHold from "@salesforce/label/c.CCL_APH_onHold";
import CCL_Warning from "@salesforce/label/c.CCL_Warning";
import CCL3_Approved from "@salesforce/label/c.CCL3_Approved";
import CCL_Print_ADF from "@salesforce/label/c.CCL_Print_ADF";
import CCL_ADF_Summ_View from "@salesforce/label/c.CCL_ADF_Summ_View";
import CCL_PRF_Summary from "@salesforce/label/c.CCL_PRF_Summary";
import CCL_show_Toast_Message_On_Hold_Note from "@salesforce/label/c.CCL_show_Toast_Message_On_Hold_Note";
import CCL_Infect_Disease_Tstng from "@salesforce/label/c.CCL_Infect_Disease_Tstng";
import CCL_Edit from "@salesforce/label/c.CCL_Edit";
import CCL_Aph_Collections from "@salesforce/label/c.CCL_Req_Collection_Detls";
import CCL_TimeZone_Of_Aph_Pickup from "@salesforce/label/c.CCL_TimeZone_Of_Aph_Pickup";
import CCL_Action from "@salesforce/label/c.CCL_Action";
import CCL_Recomended_Collection_Details from "@salesforce/label/c.CCL_Recomended_Collection_Details";
import CCL_Required_Cryopreservation_Details from "@salesforce/label/c.CCL_Required_Cryopreservation_Details";
import CCL_Threshold from "@salesforce/label/c.CCL_Threshold";
import CCL_Recom_Cryo_Details from "@salesforce/label/c.CCL_Recom_Cryo_Details";
import CCL_Additional_Comments from "@salesforce/label/c.CCL_Additional_Comments";
import CCL_ADF_Submssn_Apprvl from "@salesforce/label/c.CCL_ADF_Submssn_Apprvl";
import CCL_Collection_Detl_DateTime from "@salesforce/label/c.CCL_Collection_Detl_DateTime";
import CCL_ADF_Submitted_By from "@salesforce/label/c.CCL_ADF_Submitted_By";
import CCL_ADF_Submttd_DateTime from "@salesforce/label/c.CCL_ADF_Submttd_DateTime";
import CCL_ADF_ApprovedBy from "@salesforce/label/c.CCL_ADF_ApprovedBy";
import CCL_ADF_Apprvd_DateTime from "@salesforce/label/c.CCL_ADF_Apprvd_DateTime";
import CCl_ApprveADF from "@salesforce/label/c.CCl_ApprveADF";
import CCL_Approver_Checkbox_Label from "@salesforce/label/c.CCL_Approver_Checkbox_Label";
import CCL_Utility_modal_Cancel from "@salesforce/label/c.CCL_Utility_modal_Cancel";
import CCL_Rejct_ADF from "@salesforce/label/c.CCL_Rejct_ADF";
import CCL_ADF_Reject_Reason from "@salesforce/label/c.CCL_ADF_Reject_Reason";
import CCL_Submit_Approval from "@salesforce/label/c.CCL_Submit_Approval";
import CCL_TimeZone from "@salesforce/label/c.CCL_TimeZone";
import CCL_Delete from "@salesforce/label/c.CCL_Delete";
import CCL_Approve_Label from "@salesforce/label/c.CCL_Approve_Label";
import CCL_Reject_Button from "@salesforce/label/c.CCL_Reject_Button";
// Apex functions used to fetch details for this Page
import checkUserPermission from "@salesforce/apex/CCL_ADFController_Utility.checkUserPermission";
import getDisease from "@salesforce/apex/CCL_ADFController_Utility.getInfectousDiseases";
import getFileDetails from "@salesforce/apex/CCL_ADFController_Utility.getUploadedFileDetails";
import getRecommendedCollectionList from "@salesforce/apex/CCL_ADF_Controller.getRecommendedCollectionDetails";
import getRecommendedCryoDetailsList from "@salesforce/apex/CCL_ADF_Controller.getRecommendedCryoDetails";
import getDetails from "@salesforce/apex/CCL_ADF_Controller.getRequiredCryo";
import getCryoDetails from "@salesforce/apex/CCL_ADF_Controller.getRequiredCryoDetails";
import getUserTimeFormatted from "@salesforce/apex/CCL_ADF_Controller.getUserTimeInFormat";
import getAdfSummarPageDetails from "@salesforce/apex/CCL_ADFController_Utility.getADFSummaryPageInfo";
import getAdfSummaryObjDetails from "@salesforce/apex/CCL_ADFController_Utility.getSummaryObjectsInfo";
import getUserName from "@salesforce/apex/CCL_ADF_Controller.getUserName";
import getUserTimeZoneDetails from "@salesforce/apex/CCL_ADF_Controller.getUserTimeZoneDetails";
import {fireEvent} from 'c/cCL_Pubsub';
import thresholdErrmsg from "@salesforce/label/c.CCL_Threshold_err_msg";
import submitterErrorMsg from "@salesforce/label/c.CCL_Submitter_Error";
import getADFFileDetails from "@salesforce/apex/CCL_ADFController_Utility.getADFFileDetails";
import getAccountDetails from "@salesforce/apex/CCL_ADF_Controller.getRecords";
import getPRFList from "@salesforce/apex/CCL_ADF_Controller.getPRF";
import {CurrentPageReference,NavigationMixin} from "lightning/navigation";
import {refreshApex} from '@salesforce/apex';
import {
  FlowNavigationNextEvent,
  FlowAttributeChangeEvent
} from "lightning/flowSupport";

//Added as part of 355
//import checkUserPermission from "@salesforce/apex/CCL_ADFController_Utility.checkUserPermission";
import deleteFile from "@salesforce/apex/CCL_ADFController_Utility.deleteFile";
import CCL_DeleteConfirmation_Title from "@salesforce/label/c.CCL_DeleteConfirmation_Title";
import CCL_DeleteConfirmation_Message from "@salesforce/label/c.CCL_DeleteConfirmation_Message";
import CCL_DeleteConfirmation_Ok from "@salesforce/label/c.CCL_DeleteConfirmation_Ok";
import CCL_DeleteConfirmation_Cancel from "@salesforce/label/c.CCL_DeleteConfirmation_Cancel";
import getTimezoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getTimeZoneDetails";
import getTimezoneIDDetails from "@salesforce/apex/CCL_ADFController_Utility.getTimeZoneIDDetails";
import replacement_FIELD  from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Returning_Patient__c";
import CCL_Principal_Investigator from "@salesforce/label/c.CCL_Principal_Investigator"
import CCL_Prescriber from "@salesforce/label/c.CCL_Prescriber"
import CCL_Order_Summary_Replacement from "@salesforce/label/c.CCL_Order_Summary_Replacement"
import CCL_Therapy_Type__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Therapy_Type__c";
import getFormattedDateTimeDetails from "@salesforce/apex/CCL_ADFController_Utility.getFormattedDateTimeDetails";
import getShipmentList from "@salesforce/apex/CCL_ADFController_Utility.getShipmentInfo";

import CCL_ADF_OrderCancelled_Warning from "@salesforce/label/c.CCL_ADF_OrderCancelled_Warning";
import CCL_PRF_Resubmission_Approval from "@salesforce/label/c.CCL_PRF_Resubmission_Approval";
import getOrderHardPeg from "@salesforce/apex/CCL_ADFController_Utility.getADFAssociatedOrderHardPeg";
import getAddress from "@salesforce/apex/CCL_Utility.fetchAddress";

import getcustomdoc from "@salesforce/apex/CCL_ADFController_Utility.getDocId";
import CCL_Document_Upload_Msg from "@salesforce/label/c.CCL_Document_Upload_Msg";
//2593 tranlsation changes
import CCL_Rec_Comm_err_msg from "@salesforce/label/c.CCL_Rec_Comm_err_msg";
import CCL_Rec_Comm_err_msg_3_digits from "@salesforce/label/c.CCL_Rec_Comm_err_msg_3_digits";
import CCL_Rec_Comm_err_msg_6_digits from "@salesforce/label/c.CCL_Rec_Comm_err_msg_6_digits";
import CCL_Rec_Comm_err_msg_max_digits from "@salesforce/label/c.CCL_Rec_Comm_err_msg_max_digits";
import CCL_Flow_Cytometry from "@salesforce/label/c.CCL_Flow_Cytometry";
import CCL_Absolute_Counts from "@salesforce/label/c.CCL_Absolute_Counts";
import CCL_Complete_Blood_Count_CBC_with_Differential from "@salesforce/label/c.CCL_Complete_Blood_Count_CBC_with_Differential";
import CCL_Rec_Cryo_2_digits_1_digit_aftr from "@salesforce/label/c.CCL_Rec_Cryo_2_digits_1_digit_aftr";
import CCL_Rec_cryo_2_digits_err from "@salesforce/label/c.CCL_Rec_cryo_2_digits_err";
import CCL_Percentages from "@salesforce/label/c.CCL_Percentages";
import CCL_Cell_Viability from "@salesforce/label/c.CCL_Cell_Viability";
import CCL_Site_and_Scheduling_Information from "@salesforce/label/c.CCL_Site_and_Scheduling_Information";

import CCL_Infectious_disease_question5 from "@salesforce/label/c.CCL_Infectious_disease_question5";
import CCL_Infectious_disease_question4 from "@salesforce/label/c.CCL_Infectious_disease_question4";
import CCL_Infectious_disease_question3 from "@salesforce/label/c.CCL_Infectious_disease_question3";
import CCL_Infectious_disease_question2 from "@salesforce/label/c.CCL_Infectious_disease_question2";
import CCL_Infectious_disease_question1 from "@salesforce/label/c.CCL_Infectious_disease_question1";
const fields = [
  Collection_Center_FIELD,
  Collection_Center_FIELD_shippingStreet,
  Collection_Center_FIELD_shippingCity,
  Collection_Center_FIELD_shippingState,
  Collection_Center_FIELD_shippingPostalCode,
  Collection_Center_FIELD_shippingCountry,
  Pickup_shippingStreet,
  Pickup_shippingCity,
  Pickup_shippingPostalCode,
  Apheresis_Pickup_Location_FIELD,
  Pickup_shippingState,
  Pickup_shippingCountry,
  Other_FIELD,
  Collection_Cryopreservation_Comments_FIELD,
  CCL_Apheresis_Collection_Center__c,
  approvalCounter
];
const END_OF_APHERESIS_COLLECTION_TIME = "CCL_TEXT_Aph_Collection_End_Date_Time__c";
const CCL_TEXT_Cryopreserve_Start_Date_Time__c = "CCL_TEXT_Cryopreserve_Start_Date_Time__c";
export default class CCL_Summary_Submit extends NavigationMixin(LightningElement) {
  @track labels={
    CCL_coll_Center,
    CCL_pickup_Center,
    CCL_aph_sys,
    CCL_other,
    CCL_Aph_Comments,
    CCL_Collection,
    CCL_Aph_Id,
    CCL_Vol,
    CCL_coll_Date,
    CCL_coll_Time,
    collection,
    aphDetail,
    excesSection,
    excesLabel,
    grandTotal,
    tnc,
    cd3threshhold,
    wbcConcentration,
    aphdatetime,
    CCL_Collection_Details_Complete,
    CCL_Modal_Close,
    CCL_Summary_Submission_Header,
    CCL_Summary_Approval_Header,
    thresholdErrmsg,
    submitterErrorMsg,
	rejectionReasons,
    CCL_Rejected_By,
    CCL_Reason_for_Rejection,
    relatedDocuments,
	CCL_DeleteConfirmation_Title,
      CCL_DeleteConfirmation_Message,
      CCL_DeleteConfirmation_Ok,
      CCL_DeleteConfirmation_Cancel,
    CCL_ADF_OrderCancelled_Warning,
    CCL_PRF_Resubmission_Approval,
	  cd3tnc,
	  CCL_Document_Upload_Msg,
    CCL_Submit_Approval,
    CCL_ADF_Reject_Reason,
    CCL_Rejct_ADF,
    CCL_Utility_modal_Cancel,
    CCL_Approver_Checkbox_Label,
    CCl_ApprveADF,
    CCL_ADF_Apprvd_DateTime,
    CCL_ADF_ApprovedBy,
    CCL_ADF_Submttd_DateTime,
    CCL_ADF_Submitted_By,
    CCL_Collection_Detl_DateTime, 
   CCL_ADF_Submssn_Apprvl,
   CCL_Additional_Comments, 
   CCL_Recom_Cryo_Details, 
   CCL_Threshold, 
   CCL_Required_Cryopreservation_Details, 
   CCL_Recomended_Collection_Details,
   CCL_Action, 
   CCL_TimeZone_Of_Aph_Pickup,
   CCL_Aph_Collections, 
   CCL_Edit, 
   CCL_Infect_Disease_Tstng, 
   CCL_show_Toast_Message_On_Hold_Note, 
   CCL_PRF_Summary, 
   CCL_ADF_Summ_View, 
   CCL_Print_ADF,
   CCL3_Approved,
   CCL_Warning,
   CCL_APH_onHold,
   CCL_TimeZone,
   CCL_Delete,
   CCL_Approve_Label,
   CCL_Reject_Button
  };
  ///translation changes
  sectionLabels={CCL_Flow_Cytometry,CCL_Absolute_Counts,CCL_Complete_Blood_Count_CBC_with_Differential,CCL_Cell_Viability,CCL_Percentages,CCL_Site_and_Scheduling_Information};
  errLabels={CCL_Rec_Comm_err_msg,CCL_Rec_Comm_err_msg_3_digits,CCL_Rec_Comm_err_msg_6_digits,CCL_Rec_Comm_err_msg_max_digits,CCL_Rec_Cryo_2_digits_1_digit_aftr,CCL_Rec_cryo_2_digits_err,
    };
	  questionLabels = {
    CCL_Infectious_disease_question1,
    CCL_Infectious_disease_question2,
    CCL_Infectious_disease_question3,
    CCL_Infectious_disease_question4,
    CCL_Infectious_disease_question5
  };
  @track systemUsed;
  @track commentValue;
  @api therapyName;
  @track showApprovalLabel=false;
  @track otherValue;
  @track collectionList = [];
  @api tableData;
  @api tableData1;
  @api tableData2;
  @track sobj;
   @track configuredArr=[];
  @track rejectionflag= false;
  @track sobj1;
  @track sobj2;
  @track sobj3;
  @api configVal;
  @api configVal1;
  @api configVal2;
  @track displayThresholdWarning=false;
  @track displaySubmitterError=false;
  @api warningMsg
  @track configArr;
  @track configArr1=[];
  @track configArr2;
  @api apiNames=[];
  @api apiNames1=[];
  @api apiNames2=[];
  @api apiNames3=[];
  @api isADFViewerInternal;
  @api jsonData;
  @api myActualArr = [];
  @api myActualArr1 = [];
  @api myActualArr2 = [];
  @api myActualArr3 = [];
  @api myActualArr4 = [];
  @api storedValues;
  @api storedValues1;
  @api storedValues2;
  @api summarypage=false;
  @track hasFieldvalues;
  @track hasFieldvalues1;
  @track childArr;
  @track userTimeZone;
  @api recordId;
  @api screenName;
  @track idList = [];
  @track myFieldsArr = [];
  @track filesUploaded=[];
  @api threshold;
  @api powerOfThreshold;
  @track adfRecord;
  @api jsonObj={};
  @api isLoaded = false;
  @track checkboxFields = {};
  @track hasPermission=false;
  @track rtnValue;
  @track adfStatus;  
  @track orderStatus;
  @track approvalEligiblity;
  @track adfRejectedBy;
  @track adfRejectedDate;
  @track adfRejectedReason;
  @track editEnabled=false;
  @track showSubmit=false;
  @track showPRFReapprovalWarning=false;
  @track showgreyedSubmit= false;
  @track showGreyedRejectApprove= false;
  @track showSubmissionLabel=false;
  @track permissionName=CCL_ADF_Submitter_Permission;
  @track newPermissionName="CCL_ADF_Submitter";
  @track grandTotalTNC;
  @track grandTotalCD3;
  @track grandTotalCD3TNC;
  @api timeFormat='EEEE dd MMMMM yyyy\' \'HH:mm z';
  @api completedTimeZoneFormatted;
  @api timeZoneFormatted;
  @api submittedTimeZoneFormatted;
  @api approvedTimeZoneFormatted;
  @api rejectedTimeZoneFormatted;
  @api completedUserTimeZone;
  @api submittedUserTimeZone;
  @api approvedUserTimeZone;
  @api rejectedUserTimeZone;
  @api completedUser;
   //addd as part of CGTU 843
  @api powerOfCD3Threshold;
  @api currentUser;
  @api adfSubmittedBy;
  @api cd3threshold;
  @api cd3tncthreshold;
  @track sameUser=false;
  @track changetoPendingSubmission=false;
  @api submittedUser;
  @api approvedUser;
  @api rejectedUser;
  @api apheresisIdLabel;
  @track accountId;
  @track ApheresisId=CCL_Apheresis_ID__c;
  @track din=CCL_DIN__c;
  @track sec=CCL_SEC__c;
  @api isApheresisId=false;
  @api isDin=false;
  @api isSec=false;
  @api idField;
  @api summaryPage;
  @api manufactureStarted=false;
  @api userTimeZoneFormatted;
  @track isAdfApprover=false;
  @track ApproverPermission='CCL_ADF_Approver_User';
  @track newApproverPermission="CCL_ADF_Approver";
  @track viewerPermission='CCL_ADF_Viewer_User';
  @track newViewerPermission='CCL_ADF_Viewer';
  @track isAdfViewer=false;
  @track showViewerLabel=false;
  @track approveRejectEnabled=false;
  @track PRFapprovalCounter;							
  @track isApprovalModalOpen = false;
  @track isRejectModalOpen = false;
  @track isSubmitModalOpen=false;
  @api userName;
  @track rejectReason;
  @wire(CurrentPageReference) pageRef;
  @wire(getRecord, { recordId: "$recordId", fields })
  adf;
  @track subId;
  @api excessRetained;
  @api aphShelfLife;
  @api aphShelfLifeUnit;
  @api shelfLifeUnit;
  @api expiryDate;
  @api collectionDates = [];
  @track navigateFurther=false;
  @track summaryObj;
  @track displayManufacturingInfo=false;
  @track manufacturingMessage;
  @track loggedInUserTimeZone;
  @track contentDocumentId;
  @track disablePrintPDF=true;
  @track url;
  @track SCPurl;
  @track showUploadedFiles=false;
  @track showFileUpload=false;
  @track configVal3;
    //added as part of us 355
    @track isDialogVisible = false;
    @track originalMessage;
    @track displayMessage = 'Click on the \'Open Confirmation\' button to test the dialog.';
    @track fileUrl;
    @track idPosition;
    @track docVersionId;
    @track SubmitterPermission=CCL_ADF_Submitter_Permission;
    @track isAdfSubmitter=false;
    @track renderDelete=false;
    @track fileResultDeleted;
    @api timeZone;
    @api timeZoneScreen3;
    @api timeZoneScreen2;
    @api timeZoneScreen4;
    @api timeZoneVal;
    @api timeZoneScreen6;
    @api timeZoneVal4;
    //ends here
	 //changes as part of 868
  @track CustomerOpPermission='CCL_Customer_Operations_User';
  @track newCustomerOpPermission='CCL_Customer_Operations';
  @track isCustomerOp=false;
  @track isCustomerOpUser=false;
  @track InboundLogistcsPermission='CCL_Inbound_Logistics_Users';
  @track newInboundLogistcsPermission='CCL_Inbound_Logistics';
  @track isInboundLogisctics=false;
  @track isInboundLogiscticsUser=false;
  @track NovartisAdminPermission='CCL_Novartis_Admin_Permission_Set';
  @track newNovartisAdminPermission='CCL_Novartis_Business_Admin';
  @track isNovartisAdmin=false;
  @track isNovartisAdminUser=false;
  @track newOutboundLogisticPermission = "CCL_Outbound_Logistics";
  @track isOutboundLogistic = false;
  @track isOutboundLogisticUser = false;
  @track adfViewerInternal="CCL_ADF_Viewer_Internal";
  //Changes for 868 end here
  
	@api isClinical=false;
	@track replaceOrder;
	@track noteReplaceOrder;
	@track countReplace=0;
  @track userDateText;
  @track displayWarningMessage=false;
  @track shipmentDetails;
  @track approveRejectForCancelled=false;
  @track orderHardPeg;
  @track hardPegVal= false;
  @api timeZoneIDVal;
  @track isAdfSubmitterUser=false;
  @track isAdfApproverUser=false;
  @track isAdfViewerUser=false;
  @track adfDocId;
  @track orderID;
  @track disableFileUpload=false;
  @track showUploadButton=false;
	// 2450 code
@track adressIds=[];
@api addressList;
@api siteIds=[];
collectionId;
pickupId;
  get optionsLabel() {
    return [
      { label: "Yes", value: 'Yes' },
      { label: "No", value: 'No' }
    ];
  }
  //2450
@wire(getRecord, { recordId: "$recordId", fields })
wiredRecordCollData({ error, data }) {
  if (error) {
  } else if (data) {
    this.collectionId=data.fields.CCL_Apheresis_Collection_Center__c.value;
    this.pickupId=data.fields.CCL_Apheresis_Pickup_location__r.value.id;
    
     //2450
  }
}

get acceptedFormats() {
  return ['.docx','.txt','.xls','.csv','.ppt','.jpg','.jpeg','.png','.pdf'];
}


    connectedCallback() {
  fireEvent(this.pageRef, 'showSummaryLink', false);
    this.isLoaded = true;
    let self=this;
    this.summarypage=true;
    if(this.jsonData!==undefined){
    let val = JSON.parse(this.jsonData);
    let mytable = val["2"];
    let collectionData = JSON.parse(this.jsonData);
    let finalCollectiondata = collectionData["2"];
    let collectionData1 = JSON.parse(this.jsonData);
    let finalCollectiondata1 = collectionData1["2"];
    this.setScreen4LoadValues();
    this.setScreen20LoadValues();
	if(mytable!==undefined){
    this.tableData = mytable[1];
    this.setFormattedTimeValues();
    this.getTimeZoneScreen5Details("Recommended Cryopreservation Details");
    this.getTimeZoneScreen3Details("Recommended Collection Details");
    this.getTimeZoneScreen4Details("Required Cryopreservation Details");
    this.getTimeZoneScreen2Details("Required Collection Details");
    this.getTimeZoneScreen6Details("Summary details");
    this.getTimeZoneIDScreenDetails("Summary details");
    this.systemUsed = mytable[0][0].CCL_Apheresis_System_Used__c;
    if(this.systemUsed!=='Other'){
      this.systemUsed=this.systemUsed+' '+CCL_Registered_Trademark_Symbol;
    }
    this.otherValue = mytable[0][0].CCL_Other__c;
    let storedValue = val["3"];
    if (storedValue) {
      if (storedValue.collections !== undefined) {
      storedValue = storedValue.collections;
      if (Object.keys(storedValue["1"]).length > 0) {
        this.storedValues = storedValue;
      }
      }
    }
    let storedValueRecCryo = val["5"];
    if (storedValueRecCryo) {
      if (storedValueRecCryo.collections !== undefined) {
        storedValueRecCryo = storedValueRecCryo.collections;
      if (Object.keys(storedValueRecCryo["1"]).length > 0) {
        this.storedValues2 = storedValueRecCryo;
      }
      }
    }
    this.tableData.forEach(function (node, index) {
      if(finalCollectiondata[1][index]["CCL_DIN__c"]===null) {
      node.attributes.url = "Collection " + (index + 1) +' - '+  collectionName + " for " + " Apheresis ID: " + finalCollectiondata[1][index]["CCL_Apheresis_ID__c"];}
      else{
        node.attributes.url = "Collection " + (index + 1) +' - '+  collectionName + " for " + " DIN: " + finalCollectiondata[1][index]["CCL_DIN__c"];
      }
    });
    this.tableData1=finalCollectiondata[1];
    this.tableData1.forEach(function(node,index){
      if(finalCollectiondata[1][0]["CCL_DIN__c"]===null) {
        node.attributes.url='Collection '+(index+1)+'- Apheresis Material Testing Information' + " for " + " Apheresis ID: " + finalCollectiondata[1][index]["CCL_Apheresis_ID__c"];}
        else{
          node.attributes.url='Collection '+(index+1)+'- Apheresis Material Testing Information' + " for " + " DIN: " + finalCollectiondata[1][index]["CCL_DIN__c"];
        }
    });
    this.tableData2=finalCollectiondata1[1];
    this.tableData2.forEach(function(node,index){
      
      if(finalCollectiondata[1][0]["CCL_DIN__c"]===null) {
        node.attributes.url=collection +' '+ (index + 1) + aphDetail + " for " + " Apheresis ID: " + finalCollectiondata[1][index]["CCL_Apheresis_ID__c"];}
        else{
          node.attributes.url=collection +' '+ (index + 1) + aphDetail + " for " + " DIN: " + finalCollectiondata[1][index]["CCL_DIN__c"];
        }
        self.collectionDates.push(node.CCL_End_of_Apheresis_Collection_Date__c);
    });}
    }
  checkUserPermission({
    permissionName: this.ApproverPermission
  })
    .then((result) => {
      this.isAdfApprover=result;
      this.error = null;
      checkUserPermission({
        permissionName: this.newApproverPermission
      })
        .then((result) => {
          if(result){
            this.isAdfApproverUser=result;
            if(this.isAdfApproverUser){
              this.isAdfApprover=result;
            }
          }
          checkUserPermission({
            permissionName: this.InboundLogistcsPermission
          })
            .then((result) => {
             this.isInboundLogisctics=result;
              this.error = null;
              checkUserPermission({
                permissionName: this.adfViewerInternal
              })
                .then((result) => {
                 this.isADFViewerInternal=result;
                 if(this.isADFViewerInternal){
                  const button=this.template.querySelector('.reject');
				  if(button) {
                    button.disabled=true;
				  }
                 }
                 this.error = null;
                  
              checkUserPermission({
                permissionName: this.newInboundLogistcsPermission
              })
                .then((result) => {
                  if(result){
                    this.isInboundLogiscticsUser=result;
                    if(this.isInboundLogiscticsUser){
                      this.isInboundLogisctics=result;
                    }
                  }
                  this.error = null;
                  checkUserPermission({
                    permissionName: this.newOutboundLogisticPermission
                  })
                    .then((result) => {
                      if (result) {
                        this.isOutboundLogisticUser = result;
                        if (this.isOutboundLogisticUser) {
                          this.isOutboundLogistic = result;
                        }
                      }
                      this.error = null;
                      checkUserPermission({
                        permissionName: this.NovartisAdminPermission
                      })
                        .then((result) => {
                         this.isNovartisAdmin=result;
                         this.error = null;
                          checkUserPermission({
                            permissionName: this.newNovartisAdminPermission
                          })
                            .then((result) => {
                              if(result){
                                this.isNovartisAdminUser=result;
                                if(this.isNovartisAdminUser){
                                  this.isNovartisAdmin=result;
                                }
                              }
                              this.error = null;
                              checkUserPermission({
                                permissionName: this.permissionName
                              })
                                .then((result) => {
                                  this.hasPermission=result;
                                  this.error = null;
                                  checkUserPermission({
                                    permissionName: this.newPermissionName
                                  })
                                    .then((result) => {
                                      this.isAdfSubmitterUser=result;
                                         if (this.isAdfSubmitterUser || this.isADFViewerInternal) {
                                      this.hasPermission = true;
                                      }
                                      this.error = null;
                                      checkUserPermission({
                                        permissionName: this.viewerPermission
                                      })
                                        .then((result) => {
                                          this.isAdfViewer=result;
                                          this.error = null;
                                          checkUserPermission({
                                            permissionName: this.newViewerPermission
                                          })
                                            .then((result) => {
                                              if(result){
                                                this.isAdfViewerUser=result;
                                                if(this.isAdfViewerUser){
                                                  this.isAdfViewer=result;
                                                }
                                              }
                                              this.error = null;
                                              checkUserPermission({
                                                permissionName: this.newPermissionName
                                              })
                                                .then((result) => {
                                                  //result=true;
                                                  this.isAdfSubmitter=result;
                                                  this.error = null;
                                                  checkUserPermission({
                                                    permissionName: this.CustomerOpPermission
                                                  })
                                                    .then((result) => {
                                                     this.isCustomerOp=result;
                                                     //this.isCustomerOp=true;
                                                      this.error = null;
                                                    checkUserPermission({
                                                      permissionName: this.newCustomerOpPermission
                                                    })
                                                      .then((result) => {
                                                        if(result){
                                                          this.isCustomerOpUser=result;
                                                          if(this.isCustomerOpUser){
                                                            this.isCustomerOp=result;
                                                          }
                                                        }
                                                        this.error = null;
                                                          //added as part of 2295
                                                          
                                                          if(this.isCustomerOp||this.isADFViewerInternal||this.isAdfViewer||this.isAdfApprover||this.isAdfSubmitter){
                                                            this.showUploadButton=true;
                                                            if(this.isCustomerOp||this.isADFViewerInternal||this.isAdfViewer){
                                                              this.disableFileUpload=true;}
                                                              if(this.isAdfApprover||this.isAdfSubmitter){
                                                                this.renderDelete=true;}
                                                          }
                                                      })
                                                      .catch((error) => {
                                                        this.error = error;
                                                      });
                                                    })
                                                    .catch((error) => {
                                                      this.error = error;
                                                    });
                                                })
                                                .catch((error) => {
                                                  this.error = error;
                                                });
                                            })
                                            .catch((error) => {
                                              this.error = error;
                                            });
                                        })
                                        .catch((error) => {
                                          this.error = error;
                                        });
                                    })
                                    .catch((error) => {
                                      this.error = error;
                                    });
                                })
                                .catch((error) => {
                                  this.error = error;
                                });
                        
                            })
                            .catch((error) => {
                              this.error = error;
                            });
                        })
                        .catch((error) => {
                          this.error = error;
                        });
                    })
                    .catch((error) => {
                      this.error = error;
                    });
                })
                .catch((error) => {
                  this.error = error;
                });
              
                })
                .catch((error) => {
                  this.error = error;
                });
            })
            .catch((error) => {
              this.error = error;
            });
        })
        .catch((error) => {
          this.error = error;
        });
    })
    .catch((error) => {
      this.error = error;
    });


if(this.gotresult){
              this.getPRFValues();
            }
    this.getRecommendedCryoDetailsListValues();
    this.getRecommendedCollectionListValues();
    this.getSummaryPageDetails();
	
	//Changes as part of 355
     
    //Changes as part of 355 ends here
	//changes as part of 868
	
			
      
      
	//changes as part of 868 ends here
}
    
getShipmentDetails(){
  getShipmentList({recordId: this.recordId})
    .then((result) => {
      this.shipmentDetails = result;
      if(this.shipmentDetails[0].CCL_Status__c==='Apheresis Pick Up On Hold'){
        this.displayWarningMessage=true;
      }
        this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

@wire(getOrderHardPeg, { recordId: "$recordId" })
wiredAssocidatedOrderDetails({ error, data }) {
  if (data) {
    this.orderHardPeg =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_Hard_Peg__c:this.orderHardPeg;
    this.orderID=data[0].CCL_Order__r.Id;
    this.getSummaryObjDetails();
    if(data[0].CCL_Order__r.CCL_Hard_Peg__c == true) {
      this.hardPegVal = true;
    }
    this.error = null;
  } else if (error) {
    this.error = error;
  }
}

getTimeZoneScreen5Details(screenNameVal){
  getTimezoneDetails({screenName: screenNameVal, adfId: this.recordId})
    .then((result) => {
      this.timeZone = result;
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

getTimeZoneScreen2Details(screenNameVal){
  getTimezoneDetails({screenName: screenNameVal, adfId: this.recordId})
    .then((result) => {
      this.timeZoneScreen2 = result;
      this.timeZoneVal=this.timeZoneScreen2[END_OF_APHERESIS_COLLECTION_TIME];
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

getTimeZoneScreen4Details(screenNameVal){
  getTimezoneDetails({screenName: screenNameVal, adfId: this.recordId})
    .then((result) => {
      this.timeZoneScreen4 = result;
      this.timeZoneVal4=this.timeZoneScreen4[CCL_TEXT_Cryopreserve_Start_Date_Time__c];
      if (this.aphShelfLife != undefined && this.shelfLifeUnit) {
        this.calExpiryDate(
          this.collectionDates,
          this.aphShelfLife,
          this.shelfLifeUnit
        );
      }
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

getTimeZoneScreen3Details(screenNameVal){
  getTimezoneDetails({screenName: screenNameVal, adfId: this.recordId})
    .then((result) => {
      this.timeZoneScreen3 = result;
        this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

getTimeZoneScreen6Details(screenNameVal){
  getTimezoneDetails({screenName: screenNameVal, adfId: this.recordId})
    .then((result) => {
      this.timeZoneScreen6 = result;
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

@wire(getRecord, {
 recordId: "$recordId",
  fields: [CCL_Therapy_Type__c,replacement_FIELD]
})
wiredRecord({ error, data }) {
  if (error) {
    this.error = error;
  } else if (data) {
    const Data = data;
    const val = Data.fields.CCL_Therapy_Type__c.value;
     const val_Replace=Data.fields.CCL_Returning_Patient__c.value;
    if (val==='Clinical'){
      this.isClinical=true;
    }
    if(val_Replace){
      this.replaceOrder = true;
        this.noteReplaceOrder = CCL_Order_Summary_Replacement;
    }
  }
}

@wire(getPRFList, {therapyName:'$therapyName'})
wiredSteps({ error, data }) {
  if (data) {
	  if(data!=undefined && data[0]!==undefined){
    this.sobj3 = data[0].CCL_Object_API_Name__c;
    this.configVal3 = data;
    let mySectioned;
    let flagValue="0";
    mySectioned = this.getSections(this.configVal3);
    this.myActualArr3= this.getFields(data, mySectioned,flagValue);
    this.getPRFValues();
    this.error = null;
    this.gotresult = true;
  }} else if (error) {
    this.error = error;
  }
}

getPRFValues() {
  let self = this;
  getAccountDetails({
    sobj: this.sobj3,
    cols: this.apiNames3.toString(),
    recordId: this.recordId
  })
    .then((result) => {
      if(result!==undefined && result!=null){
      this.hasFieldvalues1 = result;
      this.myActualArr3.forEach(function (node) {
        node.myFields = self.setPRFValues(node.myFields);
      });
      //2450
       if(this.adressIds.length>0) {
        this.getAddresses();
       }
      this.myActualArr4=this.myActualArr3;
      if (result.length === 0) {
        this.hasFieldvalues1 = null;
      }

      this.error = null;
    }
    })
    .catch((error) => {
      this.error = error;
    });
}

setPRFValues(myArr) {
  let temp = this.hasFieldvalues1[0];
  let self=this;
  //2450
  let tbcLabel=false;
  myArr.forEach(function (node) {
    let fieldNameVal=temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-5)];
    if(fieldNameVal==undefined){
      node.fieldValue ='';
    }else{
    //2450
      node.fieldValue =node.fieldtype=='Lookup'?temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-5)].Id: temp[node.fieldApiName];
      if(node.fieldtype=='Lookup'&& !node.isSite){
        node.fieldValue=temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-5)].Name;
      }
    node.fieldAddress=node.fieldtype=='Lookup'?temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-5)].ShippingAddress: null;
	}
    if(node.fieldtype==='Date'){
      node.fieldValue=self.formatDateForExpiry(temp[node.fieldApiName]);
    }
    if((node.fieldApiName==='CCL_Manufacturing_Plant__r.Name' && !self.hardPegVal) || (node.fieldApiName==='CCL_Manufacturing_Plant__r.Name' && node.fieldValue=='')){
      node.fieldAddress='';
	  tbcLabel=true;
      node.isSite=false;
      node.fieldValue=CCL_To_Be_Confirmed_Label;
    }
    if(node.fieldtype==='Text'){
      node.fieldValue=temp[node.fieldApiName];
      node.fieldValue=self.setDateAndTimeFields(node.fieldValue);
    }
	if(node.isSite&&((node.fieldApiName=='CCL_Manufacturing_Plant__r.Name' &&!tbcLabel)|| node.fieldApiName!='CCL_Manufacturing_Plant__r.Name')){
     self.siteIds.push(temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-5)].Id);
      self.adressIds.push(temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-6)+'c']);
    }
  });
  return myArr;
 
}

setDateAndTimeFields(nodeValue){
  let fieldValue=nodeValue;
  let dateValue='';
  let finalVal='';
  if(fieldValue!==undefined && fieldValue!==''){
    let fields=fieldValue.split(' ');
    dateValue=fields[0]+' '+fields[1]+' '+fields[2];
    if(fields[3]==='00:00'){
      finalVal=dateValue;
    }else{
      finalVal=nodeValue;
    }
  }
  return finalVal;
  }

@wire(getFileDetails, { recordId: "$adfDocId" })
 wiredFileListSteps({ error, data }) {
     if (data) {
       this.filesUploaded = data;
         if(this.filesUploaded.length>0){
           this.showUploadedFiles=true;
         }
         this.error = null;
     } else if (error) {
         this.error = error;
     }
 }


 @wire(getcustomdoc, { recordId: "$recordId" })
 wiredDocId({ error, data }) {
     if (data) {
       this.adfDocId = data[0].Id;
         this.error = null;
     } else if (error) {
       this.error = error;
     }
 }


 @wire(getFileDetails, { recordId: "$adfDocId" })
imperativeFile(result) {
    this.fileResult = result;
    if(result.data) {
        this.filesUploaded = result.data;
        if(this.filesUploaded.length>0){
          this.showUploadedFiles=true;
        }
    }
}

handleUploadFinished(event) {
  return refreshApex(this.fileResult);
      }
//added as part of 355
@wire(getFileDetails, { recordId: "$adfDocId" })
imperativeDeleteFile(result) {
    this.fileResultDeleted = result;
    if(result.data) {
        this.filesUploaded = result.data;
        if(this.filesUploaded.length>0){
          this.showUploadedFiles=true;
        }
    }
}
handleDeleteFinished() {
  return refreshApex(this.fileResultDeleted);
      }
      //ended as part of 355

calExpiryDate(dates, shelfLife, unit) {
  let earliest = dates.reduce(function (pre, cur) {
    return Date.parse(pre) > Date.parse(cur) ? cur : pre;
  });
  let earliestTime;
  let self = this;
  this.tableData.forEach(function (node) {
    if (earliest == node.CCL_End_of_Apheresis_Collection_Date__c && node.CCL_End_of_Apheresis_Collection_Date__c) {
      earliestTime = node.CCL_TEXT_Aph_Collection_End_Date_Time__c.substr(12,6);
    }
  });
  earliest = new Date(earliest);
  if (unit == "D") {
    earliest.setDate(earliest.getDate() + shelfLife);
  }
  if (unit == "W") {
    earliest.setDate(earliest.getDate() + shelfLife * 7);
  }
  if (unit == "M") {
    earliest.setMonth(earliest.getMonth() + shelfLife);
  }
  if (unit == "Y") {
    earliest.setFullYear(earliest.getFullYear() + shelfLife);
  }
  earliest = this.formatDate(earliest);
  let expiryDate = earliest + " " + earliestTime;
  this.expiryDate = earliest + " "+ earliestTime.substr(0,5)+' '+this.timeZoneScreen4['CCL_TEXT_Cryopreserve_Start_Date_Time__c'];
}

getSummaryPageDetails(){
  getAdfSummarPageDetails({
    recordId: this.recordId
  })
    .then((result) => {
      let firstName='';
      let lastName='';
      let submitterId='';
      let sameUser=false;
      let self=this;
      this.summaryPage = result;
      this.adfStatus=this.summaryPage[0].CCL_Status__c;
      this.orderStatus= this.summaryPage[0].CCL_Order__r.CCL_PRF_Ordering_Status__c;
      this.approvalEligiblity= this.summaryPage[0].CCL_Order__r.CCL_Order_Approval_Eligibility__c;
	  this.PRFapprovalCounter= this.summaryPage[0].CCL_Order__r.CCL_PRF_Approval_Counter__c;		 
	  if(this.summaryPage[0].CCL_ADF_Rejected_By__r!==undefined){
	  this.adfRejectedBy=this.summaryPage[0].CCL_ADF_Rejected_By__r.Name;
      this.adfRejectedDate=this.summaryPage[0].CCL_ADF_Rejected_Date_Time__c;
      this.adfRejectedReason=this.summaryPage[0].CCL_Rejection_Reason__c;
	  }
      this.manufactureStarted=this.summaryPage[0].CCL_ADF_Locked_For_Edit__c;
      if(this.summaryPage[0].CCL_ADF_Submitted_By__r!==undefined){
      submitterId=this.summaryPage[0].CCL_ADF_Submitted_By__r.Id;
	  this.subId = submitterId;
      firstName=this.summaryPage[0].CCL_ADF_Submitted_By__r.FirstName;
      lastName=this.summaryPage[0].CCL_ADF_Submitted_By__r.LastName;
      this.submittedUser=self.getUserName(firstName,lastName);
      }
      if(this.summaryPage[0].CCL_ADF_Approved_By__r!==undefined){
      firstName=this.summaryPage[0].CCL_ADF_Approved_By__r.FirstName;
      lastName=this.summaryPage[0].CCL_ADF_Approved_By__r.LastName;
      this.approvedUser=self.getUserName(firstName,lastName);
      }
      if(this.summaryPage[0].CCL_Collection_Details_Completed_By__r!==undefined){
      firstName=this.summaryPage[0].CCL_Collection_Details_Completed_By__r.FirstName;
      lastName=this.summaryPage[0].CCL_Collection_Details_Completed_By__r.LastName;
      this.completedUser=self.getUserName(firstName,lastName);
      }
	  if(this.summaryPage[0].CCL_ADF_Rejected_By__r!==undefined){
        firstName=this.summaryPage[0].CCL_ADF_Rejected_By__r.FirstName;
        lastName=this.summaryPage[0].CCL_ADF_Rejected_By__r.LastName;
        this.rejectedUser=self.getUserName(firstName,lastName);
      }
		if(this.approvalEligiblity  && this.PRFapprovalCounter >='1' && this.orderStatus!='PRF_Approved') {
        this.showPRFReapprovalWarning = true;
      }
      if(submitterId===this.userName.Id){
        sameUser=true;
		this.sameUser=true;
      }
      if (this.hasPermission || (this.isAdfApprover||this.isADFViewerInternal)||this.isCustomerOp||this.isAdfViewer||this.isNovartisAdmin) {
          this.showFileUpload = true;
        }
      if(this.sameUser && this.hasPermission && this.adfStatus =='ADF Pending Approval' && !this.manufactureStarted){
        this.editEnabled=true;
        this.changetoPendingSubmission=true;
      }
      else if(this.hasPermission && (this.adfStatus ==='ADF Rejected'||this.adfStatus ==='ADF Approved') && !this.manufactureStarted){
        this.editEnabled=true;
        this.changetoPendingSubmission=true;
      }
      let submitterStatusCheck=false;
      if(this.adfStatus==='ADF Pending Submission' || this.adfStatus==='ADF Rejected'){
        submitterStatusCheck=true;
      }
      let anotherSubmitterStatusCheck=false;
      if(this.adfStatus==='ADF Approved' || this.adfStatus==='ADF Rejected'){
        anotherSubmitterStatusCheck=true;
      }
      // Note to add permission check
      this.setLabelButtonVisibility(submitterStatusCheck,sameUser,submitterId,anotherSubmitterStatusCheck);
      this.completedUserTimeZone=this.summaryPage[0].CCL_TEXT_Collection_Details_Date_Time__c	;
      this.submittedUserTimeZone=this.summaryPage[0].CCL_TEXT_ADF_Submitted_Date_Time__c;
      this.approvedUserTimeZone=this.summaryPage[0].CCL_TEXT_ADF_Approved_Date_Time__c;
      this.rejectedUserTimeZone=this.summaryPage[0].CCL_TEXT_ADF_Rejected_Date_Time__c;
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

setLabelButtonVisibility(submitterStatusCheck,sameUser,submitterId,anotherSubmitterStatusCheck){
  if(this.adfStatus!=='Order Cancelled' && !this.manufactureStarted && this.hasPermission && submitterStatusCheck &&  (sameUser || submitterId === "" || this.isADFViewerInternal)){
    this.editEnabled=true;
    //this.showSubmit=true;
    this.showSubmissionLabel=true;
	
	if(this.approvalEligiblity== true){
      if(this.orderStatus=== 'PRF_Approved' && this.PRFapprovalCounter >='1'){
        this.showSubmit= true;        
      }else if(this.orderStatus!= 'PRF_Approved' && this.PRFapprovalCounter >='1'){
        this.showPRFReapprovalWarning= true;        
        this.showgreyedSubmit= true;
        this.showSubmit= false;  
      }else if(this.PRFapprovalCounter== null ){
        this.showSubmit= true; 
	  }
    }else{
      this.showSubmit=true;
    } 
	fireEvent(this.pageRef, 'adfApprovedPage', 'submitter');
  }else if(this.adfStatus!=='Order Cancelled' && !this.manufactureStarted && this.hasPermission && anotherSubmitterStatusCheck){
    this.editEnabled=true;
    //this.showSubmit=true;
    this.showSubmissionLabel=true;
	
	if(this.approvalEligiblity== true){
      if(this.orderStatus=== 'PRF_Approved' && this.PRFapprovalCounter >='1'){
        this.showSubmit= true;        
      }else if(this.orderStatus!= 'PRF_Approved' && this.PRFapprovalCounter >='1'){
        this.showPRFReapprovalWarning= true;        
        this.showgreyedSubmit= true;
        this.showSubmit= false; 
      }else if(this.PRFapprovalCounter== null ){
        this.showSubmit= true; 
      }
    }else{
      this.showSubmit=true;
    } 
	fireEvent(this.pageRef, 'adfApprovedPage', 'submitter');
  }else if(this.adfStatus==='ADF Pending Approval' && (this.isAdfApprover||this.isADFViewerInternal) && !this.manufactureStarted && !sameUser){
    fireEvent(this.pageRef, 'adfApprovedPage', 'adfApproved');
    //this.approveRejectEnabled=true;
	
	 if(this.approvalEligiblity== true){
      if(this.orderStatus=== 'PRF_Approved' && this.PRFapprovalCounter >='1'){
        this.approveRejectEnabled= true;        
      }else if(this.orderStatus!= 'PRF_Approved' && this.PRFapprovalCounter >='1'){
        this.showPRFReapprovalWarning= true;        
        this.showGreyedRejectApprove= true;
        this.approveRejectEnabled= false;    
      }
    }else{
      this.approveRejectEnabled=true;
    } 
  }else if(this.adfStatus!=='Order Cancelled' && !(this.isAdfApprover||this.isADFViewerInternal) && !this.manufactureStarted && this.hasPermission && !sameUser){
    this.showSubmissionLabel=true;
	fireEvent(this.pageRef, 'adfApprovedPage', 'submitter');
  }else if(this.isAdfViewer&&this.adfStatus!=='Order Cancelled'){
    this.showViewerLabel=true;
    fireEvent(this.pageRef, 'adfApprovedPage', 'adfApproved');
  }else if((this.isCustomerOp||this.isNovartisAdmin||this.isInboundLogisctics|| this.isOutboundLogistic)&&this.adfStatus!=='Order Cancelled'){
    this.showViewerLabel=true;
    fireEvent(this.pageRef, 'adfApprovedPage', 'adfApproved');
    fireEvent(this.pageRef, 'setRecordId', this.recordId);
  }else if(this.adfStatus==='Order Cancelled'&&(this.isAdfApprover||this.isADFViewerInternal||this.isAdfSubmitter||this.isAdfViewer)){
    this.displayCancelledWarningMessage=true;
   
    this.showSubmit=false;
    if(this.isAdfSubmitter){
      this.showSubmissionLabel=true;
      fireEvent(this.pageRef, 'adfApprovedPage', 'submitter');
    }
    else if(this.isAdfApprover||(this.isADFViewerInternal&&this.adfStatus==='ADF Pending Approval')){
		this.approveRejectForCancelled=true;
      if(!this.manufactureStarted && !sameUser){
	fireEvent(this.pageRef, 'adfApprovedPage', 'adfApproved');}
	}
      else if(this.isAdfViewer){
        this.showViewerLabel=true;
		fireEvent(this.pageRef, 'adfApprovedPage', 'adfApproved');
      }
  }
  else{
    this.showSubmissionLabel=true;
	if(this.isAdfSubmitter){
      fireEvent(this.pageRef, 'adfApprovedPage', 'submitter');
    }
    if(this.isAdfViewer || (this.isAdfApprover||(this.isADFViewerInternal&&this.adfStatus==='ADF Pending Approval'))){
    fireEvent(this.pageRef, 'adfApprovedPage', 'adfApproved');
    }
  }
  if((this.isAdfApprover||(this.isADFViewerInternal&&this.adfStatus==='ADF Pending Approval')) && !this.isAdfSubmitter && this.adfStatus!=='Order Cancelled'){
    this.showApprovalLabel=true;
    this.showSubmissionLabel=false;
  }
	if((this.adfStatus==='ADF Pending Approval' || this.adfStatus==='ADF Approved')   && this.hasPermission){
    this.showSubmit=false;
  }else if(this.isAdfApprove && this.isAdfSubmitter && submitterId===''){
    this.showSubmissionLabel=true;
  }
}

getUserName(firstName,lastName){
let userName;
if(firstName!==undefined){
userName=firstName;
if(lastName!==undefined){
 return userName+' '+lastName;
}
}else if(lastName!==undefined){
  userName=lastName;
}
return userName;
}

setScreen4LoadValues(){
  let collectionData = JSON.parse(this.jsonData);
  let cryoData=collectionData["4"];
  if(cryoData!=undefined){
    this.storedValues1=cryoData.croyos;
    this.grandTotalTNC=this.storedValues1[0].adf.CCL_GT_Nucleated_Cell_Count__c+' '+tnc109;
    this.grandTotalCD3=this.storedValues1[0].adf.CCL_GT_CD3_Cell_Count__c+' '+CCL_CD3_suffix;
    this.grandTotalCD3TNC=this.storedValues1[0].adf.CCL_CD3_TNC_X100_3__c+' '+'%';
    this.excessRetained = this.storedValues1[0].adf.CCL_Excess_Aph_Material_At_Site__c?'Yes':'No';
  }
}

setScreen20LoadValues(){
  if (this.jsonData != undefined || this.jsonData != null) {
    this.jsonObj = JSON.parse(this.jsonData);
    let myValue = this.jsonObj["20"];
    if (myValue != undefined) {
      this.checkboxFields = myValue;
    }
    this.commentValue = this.jsonObj[
    "Collection and Cryopreservation Comments"
    ];
}
}

setFormattedTimeValues(){
  let self=this;
  let val = JSON.parse(this.jsonData);
  let mytable = val["2"];
  this.collectionList=mytable[1];
  this.collectionList.forEach(function(node,index){
    let dateTimeVal=node["CCL_TEXT_Aph_Collection_End_Date_Time__c"];
    if(dateTimeVal!==undefined && dateTimeVal!==null && dateTimeVal!==''){
      let fields=dateTimeVal.split(' ');
      let timeVal=fields[3];
      let dateVal=fields[0]+' '+fields[1]+' '+fields[2];
      let timeValue=timeVal.split(":");
      node.CCL_End_of_Apheresis_Collection_Date__c=self.formatDate(dateVal);
      node.CCL_End_of_Apheresis_Collection_Time__c=timeValue[0]+':'+timeValue[1];
    }
});
}


formatDateForExpiry(date) {
  if(date!==null && date!==''&& date!==undefined){
   let mydate = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    mydate=new Date(date);
  }
  let monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];

  let day = mydate.getDate();
  let monthIndex = mydate.getMonth();
  let monthName = monthNames[monthIndex];
  let year = mydate.getFullYear();
  return `${day} ${monthName} ${year}`;
}
return '';
}

formatDate(date) {
   let mydate = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    mydate=new Date(date);
  }
  let monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];

  let day = mydate.getDate();
  let monthIndex = mydate.getMonth();
  let monthName = monthNames[monthIndex];
  let year = mydate.getFullYear();
  return `${day} ${monthName} ${year}`;
}

renderedCallback() {
  if (this.checkboxFields != undefined) {
    let self = this;
    this.template.querySelectorAll(".myInputs").forEach(function (node) {
      node.checked = self.checkboxFields[node.dataset.item];
    });
  }
  let self = this;
  self.getShipmentDetails();
  if (this.isLoaded) {
    if(this.configVal1 && this.myActualArr2.length==0){
	   this.configVal1.forEach(function (node) {
    
      if(node.CCL_Order_of_Field__c==0 && node.CCL_Field_Api_Name__c==self.idField){
        self.setFields3(node);}
        else if(node.CCL_Order_of_Field__c!=0){
          self.setFields3(node);
        }
    
  });}
    this.getSections3();
    this.configArr1.forEach(function (node) {
      self.setById(node);
    });
    this.getValues();
    this.isLoaded = false;
  }
  if(this.warningMsg!=undefined && this.warningMsg.length>0){
    this.displayThresholdWarning=true;
  }
  if(this.userName!==undefined && this.userName.Id == this.subId && this.adfStatus == 'ADF Pending Approval' && (this.isAdfApprover||this.isADFViewerInternal)){
    this.displaySubmitterError=true;
  }
 if(this.contentDocumentId!==undefined && (this.adfStatus ==='ADF Approved'||this.adfStatus ==='Order Cancelled')){
     this.disablePrintPDF=false; 
}
  if(this.adfStatus ==='ADF Rejected'){
    this.rejectionflag=true;	
  }
  if(this.gotresult){
    this.getPRFValues();
    this.gotresult=false;
  }
  if(this.orderID && this.adfStatus){
  this.getSummaryObjDetails();
  }
  const checkReplace = this.template.querySelector('.repalceOrderStyle');
    this.countReplace = this.countReplace + 1;
    if(this.countReplace !=2 && checkReplace){
      checkReplace.classList.remove('repalceOrderDisplay');
    }
}

getSummaryObjDetails(){
  getAdfSummaryObjDetails({
    recordId: this.orderID
  })
    .then((result) => {
      this.summaryObj = result;
      let manufacturingDateval=[];
      let manufacturingDateTimeVal=[];
      let earliestDate;
      let earliestTime;
      if(this.summaryObj!==undefined){
        this.summaryObj.forEach(function (node) {
          if (node.CCL_Manufacturing_Start_Date_Time__c!==undefined && node.CCL_Manufacturing_Start_Date_Time__c!=='') {
            let datVal=node.CCL_Manufacturing_Start_Date_Time__c.split('T');
            manufacturingDateval.push(datVal[0]);
            manufacturingDateTimeVal.push(node.CCL_Manufacturing_Start_Date_Time__c);
          }
        });
        earliestDate=manufacturingDateval.reduce(function (pre, cur) {
          return Date.parse(pre) > Date.parse(cur) ? cur : pre;
        });
        earliestTime=manufacturingDateTimeVal.reduce(function (pre, cur) {
          return Date.parse(pre) > Date.parse(cur) ? cur : pre;
        });
      }
      let timeAhead=this.validateTime(earliestTime,earliestDate);
      if(this.manufactureStarted && earliestDate!==undefined && earliestDate!=='' && this.adfStatus === 'ADF Approved' && timeAhead){
      this.displayManufacturingInfo=true;
      let formattedDate=this.formatDateForManufacturing(earliestDate);
      this.manufacturingMessage=CCL_Manufacturing_Started_Label+' '+formattedDate;
      }
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

validateTime(earliestTime,earliestDate){
  let earlistDateVal=new Date(earliestDate);
  let currentDateTime= new Date(this.userTimeZone);
  let timeAhead=false;
  let currentdatVal=this.userTimeZone.split('T');
  let currentTime=currentdatVal[1].split(":");
  currentDateTime.setHours(currentTime[0], currentTime[1], 0);
  let currentTimeVal=currentDateTime.getTime();
  let userTime = new Date(earliestTime).toLocaleString("en-US", {timeZone: this.loggedInUserTimeZone});
  let formattedTime=new Date(userTime);
    if(earlistDateVal.getDate()<=currentDateTime.getDate() && earlistDateVal.getMonth()<=currentDateTime.getMonth()
    && earlistDateVal.getFullYear()<=currentDateTime.getFullYear()){
        let inpTime=formattedTime.getTime();
        if(inpTime<currentTimeVal){
          timeAhead=true;
         }
  }
  return timeAhead;
}

formatDateForManufacturing(date) {
  let mydate = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    mydate=new Date(date);
  }
  let monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];

  let day = mydate.getDate();
  let monthIndex = mydate.getMonth();
  let monthName = monthNames[monthIndex];
  let year = mydate.getFullYear();
  return `${day} ${monthName} ${year}`;
}


@wire(getRecord, { recordId: "$recordId", fields })
  getAdf({
    error,
    data
}) {
    if (error) {
       this.error = error ;
    } else if (data) {
        this.accountId=data;
    }
}
@wire(getUserName)
  wiredStepApprovedUserName({ error, data }) {
          if (data) {
            this.userName = data;
            this.loggedInUserTimeZone=this.userName.TimeZoneSidKey;
            this.error = null;
          } else if (error) {
            this.error = error;
          }
        }

        getUserTimeZone(timeZoneVal){
          getUserTimeZoneDetails({timeZoneVal:timeZoneVal})
          .then((result) => {
            this.userTimeZone = result;
            this.getDateTextVal();
          })
          .catch((error) => {
            this.error = error;
          });
        }

        getDateTextVal(){
          if(this.userTimeZone!==undefined && this.userTimeZone!==null && this.userTimeZone!==''){
            let fields=this.userTimeZone.split('T');
            let timeVal=fields[1];
            let dateVal=fields[0];
            let timeValue=timeVal.split(":");
            let textVal=this.formatDate(dateVal)+' '+timeValue[0]+':'+timeValue[1]+' '+this.timeZoneScreen6[approvedDateText.fieldApiName];
            this.userDateText=textVal;
          }
        }

        @wire(getUserTimeFormatted,{dateTimeVal: '$userTimeZone',format: '$timeFormat'})
        wiredStepApprovedUserTimeFormatted({ error, data }) {
          if (data) {
            this.timeZoneFormatted = data;
            this.error = null;
          } else if (error) {
            this.error = error;
          }
        }

@wire(getRecord,{ recordId: "$accountId.fields.CCL_Apheresis_Collection_Center__c.value", fields:[CCL_Label_Compliant__c]})
   WiredIdType({
    error,
    data
}) {
    if (error) {
      this.error = error ;
    } else if (data) {
      if(data.fields.CCL_Label_Compliant__c.value==='ISBT'){
        this.apheresisIdLabel='DIN';
        this.isDin=true;
      }
      else{
        this.apheresisIdLabel='Apheresis Id';
        this.isApheresisId=true;
      }
    }
}

@wire(getUserTimeFormatted,{dateTimeVal: '$completedUserTimeZone',format: '$timeFormat'})
wiredStepCompletedUserTime({ error, data }) {
  if (data) {
    this.completedTimeZoneFormatted = data;
    this.error = null;
  } else if (error) {
    this.error = error;
  }
}

@wire(getUserTimeFormatted,{dateTimeVal: '$submittedUserTimeZone',format: '$timeFormat'})
wiredStepSubmittedUserTime({ error, data }) {
  if (data) {
    this.submittedTimeZoneFormatted = data;
    this.error = null;
  } else if (error) {
    this.error = error;
  }
}

@wire(getUserTimeFormatted,{dateTimeVal: '$approvedUserTimeZone',format: '$timeFormat'})
wiredStepApprovedUserTime({ error, data }) {
  if (data) {
    this.approvedTimeZoneFormatted = data;
    this.error = null;
  } else if (error) {
    this.error = error;
  }
}
@wire(getUserTimeFormatted,{dateTimeVal: '$rejectedUserTimeZone',format: '$timeFormat'})
wiredStepRejectedUserTime({ error, data }) {
  if (data) {
    this.rejectedTimeZoneFormatted = data;
    this.error = null;
  } else if (error) {
    this.error = error;
  }
}
@wire(getDetails)
wiredRequiredCryoSteps({ error, data }) {
  if (data) {
	 if(data!=undefined && data[0]!==undefined){
    this.sobj1 = data[0].CCL_Object_API_Name__c;
    this.configVal1 = data;
    let self = this;
    this.configVal1.forEach(function (node) {
     if(self.idField!=undefined){
      if(node.CCL_Order_of_Field__c==0 && node.CCL_Field_Api_Name__c==self.idField){
      self.setFields3(node);}
      else if(node.CCL_Order_of_Field__c!=0){
        self.setFields3(node);
      }
    }
    });
    if (this.tableData2 != undefined) {
      this.getSections3();
      this.configArr1.forEach(function (node) {
        self.setById(node);
      });
      this.isLoaded = false;
      this.getValues();
    }
	 this.error = null;}
  } else if (error) {
    this.error = error;
  }
}

getSections3() {
  let self = this;
  let myObj = { Id: "", myFields: [] };
  if(this.tableData2){
  this.tableData2.forEach(function (node) {
    myObj = { Id: "", myFields: [] };
    myObj.Id = node.Id;
    self.configArr1.push(myObj);
    self.idList.push(node.Id);
  });
}
}

setFields3(mainArrr) {
  let fieldArr = {
    Fieldlabel: "",
    fieldApiName: "",
    fieldtype: "",
    fieldorder: "",
    fieldValue: "",
    fieldhelptext: "",
    isRadio: "",
    isEditable: "",
    fieldLength: "",
    isDate: "",
    class: "",
    isTime: ""
  };
  fieldArr.Fieldlabel = mainArrr.MasterLabel;
  fieldArr.fieldApiName = mainArrr.CCL_Field_Api_Name__c;
  fieldArr.fieldtype = mainArrr.CCL_Field_Type__c;
  fieldArr.fieldorder = mainArrr.CCL_Order_of_Field__c;
  fieldArr.fieldhelptext = mainArrr.CCL_Help_Text__c;
  fieldArr.fieldLength = mainArrr.CCL_Field_Length__c;
  fieldArr.isRadio = mainArrr.CCL_Field_Type__c === "Checkbox" ? true : false;
  fieldArr.isEditable = mainArrr.CCL_IsEditable__c ? false : true;
  if(fieldArr.fieldtype=='date' && fieldArr.fieldApiName==CCL_TEXT_Cryopreserve_Start_Date_Time__c){
    fieldArr.Fieldlabel='Start of Cryopreservation Date';
    fieldArr.class = "labelClass dateClass queryComp date_time";
    fieldArr.isDate = true;
  }
  if(fieldArr.fieldtype=='time' && fieldArr.fieldApiName==CCL_TEXT_Cryopreserve_Start_Date_Time__c){
    fieldArr.Fieldlabel='Start of Cryopreservation Time';
    fieldArr.class = "labelClass timeClass queryComp date_time";
    fieldArr.isTime = true;
  }
  this.apiNames2.push(fieldArr.fieldApiName);
  this.myActualArr2.push(fieldArr);
}
setById(myArr) {
  this.myActualArr2.forEach(function (node) {
    myArr.myFields.push(node);
  });
}

getValues() {

  this.apiNames2 = [...new Set(this.apiNames2)];
  getCryoDetails({
    sobj: this.sobj1,
    cols: this.apiNames2.toString(),
    idList: this.idList
  })
    .then((result) => {
      let self=this;
      self.hasFieldvalues = result;
      this.hasFieldvalues.forEach(function (node) {});
	   //2260
        if(this.isADFViewerInternal){
          this.hasFieldvalues=this.tableData;
        }
      self.childArr = this.configArr1;
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

@wire(getDisease, { therapyName: '$therapyName' })
 wiredDiseaseSteps({ error, data }) {
    if (data) {
      this.myFieldsArr = data;
      let self = this;
    
      this.myFieldsArr.forEach(function (node) {
        let temp={CCL_Field_API_Name__c:"",CCL_Display_Label__c:"",CCL_Order_of_field__c:'',Id:""};
          temp.CCL_Field_API_Name__c=node.CCL_Field_API_Name__c,
          temp.Id=node.Id;
          temp.CCL_Order_of_field__c=node.CCL_Order_of_field__c;
          temp.CCL_Display_Label__c=self.questionLabels[node.CCL_Display_Label__c];
          self.configuredArr.push(temp);
        
      });
        this.error = null;
    } else if (error) {
        this.error = error;
    }
}

@wire(getADFFileDetails, { recordId: "$recordId" })
wiredADFFileS({ error, data }) {
    if (data) {
      this.contentDocumentId =data.length>0? data[0].ContentDocumentId:this.contentDocumentId;
       this.error = null;
    } else if (error) {
        this.error = error;
    }
}

getRecommendedCryoDetailsListValues(){
  getRecommendedCryoDetailsList({
  })
    .then((result) => {
		if(result!=undefined && result[0]!==undefined){
      this.sobj2 = result[0].CCL_Object_API_Name__c;
      this.configVal2 = result;
      let mySectioned;
      let flagValue="2";
      mySectioned = this.getSections(this.configVal2);
      this.myActualArr1= this.getFields(result, mySectioned,flagValue);
      this.error = null;
      this.gotresult = true;
    }})
    .catch((error) => {
      this.error = error;
    });
}
  getRecommendedCollectionListValues(){
    getRecommendedCollectionList({
    })
      .then((result) => {
		  if(result!=undefined && result[0]!==undefined){
        this.sobj = result[0].CCL_Object_API_Name__c;
      this.configVal = result;
      let mySectioned;
      let flagValue="1";
      mySectioned = this.getSections(this.configVal);
      this.myActualArr= this.getFields(result, mySectioned,flagValue);
      this.error = null;
      this.gotresult = true;
      }})
      .catch((error) => {
        this.error = error;
      });
  }

  getSections(myArr) {
    let configArr = [];
    let sectionnames = [];
	 let myObj = { sectioname: "", myFields: [] };
   //tranlsation 2593
    let self=this;
    myArr.forEach(function (node) {
      sectionnames.push(self.sectionLabels[node.CCL_Section_Name__c]);
    });
    let uniq = [...new Set(sectionnames)];
    uniq.forEach(function (node) {
      myObj = { sectioname: "", myFields: [] };
      myObj.sectioname = node;
      configArr.push(myObj);
    });
    return configArr;
  }
  getFields(mainArrr, configArr,flagCheck) {
    let i, j;
    let self = this;
    for (i = 0; i < mainArrr.length; i++) {
      for (j = 0; j < configArr.length; j++) {
		  //translation changes
         if (configArr[j].sectioname ===self.sectionLabels[mainArrr[i].CCL_Section_Name__c]) {
           if(flagCheck!=='0'){	
          self.setFields(configArr,mainArrr,i,j,flagCheck);	
          }else{	
            self.setPRFFields(configArr,mainArrr,i,j);	
          }
        }
      }
    }
    if(flagCheck==='0'){
      this.apiNames3.push('CCL_Therapy_Type__c');
    }
    configArr.forEach(function (node) {
      node.myFields = self.sortData("fieldorder", "asc", node.myFields);
    });
    return configArr;
  }
  
    setPRFFields(configArr,mainArrr,i,j){	
    let address;	
    let fieldArr = {	
      Fieldlabel: "",	
      fieldApiName: "",	
      fieldtype: "",	
      fieldorder: "",	
      fieldValue: "",	
      fieldhelptext: "",	
      isRadio: "",	
      fieldAddress:'',
       showWarning:''	,
	      isSite:''
    };	
    fieldArr.Fieldlabel = mainArrr[i].MasterLabel;	
	if (
      mainArrr[i].CCL_Field_Api_Name__c == 'CCL_Prescriber__c' &&
     this.isClinical
    ) {
      fieldArr.Fieldlabel = CCL_Principal_Investigator;
    } else if (mainArrr[i].CCL_Field_Api_Name__c == 'CCL_Prescriber__c' &&
    !this.isClinical) {
      fieldArr.Fieldlabel = CCL_Prescriber;
    }
    fieldArr.fieldApiName = mainArrr[i].CCL_Field_Api_Name__c;	
    if(mainArrr[i].CCL_Field_Type__c=='Lookup'){	
      fieldArr.fieldApiName= fieldArr.fieldApiName.substring(0,  (fieldArr.fieldApiName).length - 1)+'r.Name';	
	    fieldArr.isSite=true;
      if(mainArrr[i].CCL_HasAddress__c){	
        address=fieldArr.fieldApiName.substring(0,  (fieldArr.fieldApiName).length - 5)+'.ShippingAddress';	
        this.apiNames3.push(address);	
      }	
    }	
    if(mainArrr[i].CCL_Field_Api_Name__c==="CCL_TEXT_Planned_Cryo_Apheresis_Pick_up__c"){
      fieldArr.showWarning=true;
    }
	 //2450
    if(mainArrr[i].CCL_Field_Api_Name__c == 'CCL_Prescriber__c'){
      fieldArr.isSite=false;
    }
    fieldArr.fieldtype = mainArrr[i].CCL_Field_Type__c;	
    fieldArr.fieldorder = mainArrr[i].CCL_Order_of_Field__c;	
    fieldArr.fieldhelptext = mainArrr[i].CCL_Help_Text__c;	
    fieldArr.isRadio =	
      mainArrr[i].CCL_Field_Type__c === "Checkbox" ? true : false;	
    this.apiNames3.push( fieldArr.fieldApiName);	
    configArr[j].myFields.push(fieldArr);	
  }
  
  sortData(fieldName, sortDirection, mydata) {
    let data = JSON.parse(JSON.stringify(mydata));
    let key = (a) => a[fieldName];
    let reverse = sortDirection === "asc" ? 1 : -1;
    data.sort((a, b) => {
      let valueA = key(a) ? key(a) : "";
      let valueB = key(b) ? key(b) : "";
      return reverse * ((valueA > valueB) - (valueB > valueA));
    });
     return data;
  }
  setFields(configArr,mainArrr,i,j,flagCheck){
    let address;
    let fieldArr = {
      Fieldlabel: "",
      fieldApiName: "",
      fieldtype: "",
      fieldorder: "",
      fieldValue: "",
      fieldhelptext: "",
      placeHolder:"",
      showStep:"",
      isRadio: "",
      fieldAddress:"",
      isDate: "",
      class: "",
      isTime: ""
    };
    fieldArr.Fieldlabel = mainArrr[i].MasterLabel;
    fieldArr.fieldApiName = mainArrr[i].CCL_Field_Api_Name__c;
    if(mainArrr[i].CCL_Field_Type__c=='Lookup'){
      fieldArr.fieldApiName= fieldArr.fieldApiName.substring(0,  (fieldArr.fieldApiName).length - 1)+'r.Name';
      if(mainArrr[i].CCL_HasAddress__c){
        address=fieldArr.fieldApiName.substring(0,  (fieldArr.fieldApiName).length - 5)+'.ShippingAddress';
        this.apiNames.push(address);
      }
    }
    fieldArr.fieldtype = mainArrr[i].CCL_Field_Type__c;
    fieldArr.fieldorder = mainArrr[i].CCL_Order_of_Field__c;
    fieldArr.fieldhelptext = mainArrr[i].CCL_Help_Text__c;
    fieldArr.placeHolder = mainArrr[i].CCL_Place_Holder__c;
    fieldArr.showStep = mainArrr[i].CCL_Show_Step__c;
    fieldArr.isRadio =
    mainArrr[i].CCL_Field_Type__c === "Checkbox" ? true : false;
    this.apiNames.push(fieldArr.fieldApiName);
    if(this.storedValues==undefined && flagCheck==='1'){
      fieldArr.placeHolder = "";
      if(fieldArr.fieldtype==='Time'){
        fieldArr.Fieldlabel = 'Time'
        fieldArr.fieldtype="text";
      }else if(fieldArr.fieldtype==='Date'){
        fieldArr.Fieldlabel = 'Date of Testing'
        fieldArr.fieldtype="text";
      }
    } else if(this.storedValues2==undefined && flagCheck==='2'){
        fieldArr.placeHolder = "";
        if(fieldArr.fieldtype==='Time'){
          fieldArr.isTime = true;
          fieldArr.Fieldlabel ='Time';
          fieldArr.class = "labelClass timeClass";
          fieldArr.fieldtype="text";
        }else if(fieldArr.fieldtype==='Date'){
          fieldArr.isDate = true;
          fieldArr.Fieldlabel = 'Date of Testing';
          fieldArr.class = "labelClass dateClass";
          fieldArr.fieldtype="text";
        }
      }
    configArr[j].myFields.push(fieldArr);
  }

  get collectionCenter() {
    return getFieldValue(this.adf.data, Collection_Center_FIELD);
  }
  get collectionCenterStreet() {
    return getFieldValue(this.adf.data, Collection_Center_FIELD_shippingStreet);
  }
  get collectionCenterAddress() {
    const shipCity = getFieldValue(
      this.adf.data,
      Collection_Center_FIELD_shippingCity
    );
    const shipState = getFieldValue(
      this.adf.data,
      Collection_Center_FIELD_shippingState
    );
    const shipPostalCode = getFieldValue(
      this.adf.data,
      Collection_Center_FIELD_shippingPostalCode
    );
    let address = "";
    if (shipCity !== null) {
      address = shipCity;
    }
    if (shipState !== null) {
      address = address + ", " + shipState;
    }
    if (shipPostalCode !== null) {
      address = address + " " + shipPostalCode;
    }
    return address;
  }
  get collectionCenterCountry() {
    return getFieldValue(
      this.adf.data,
      Collection_Center_FIELD_shippingCountry
    );
  }
  get pickupLocation() {
    return getFieldValue(this.adf.data, Apheresis_Pickup_Location_FIELD);
  }
  get pickupStreet() {
    return getFieldValue(this.adf.data, Pickup_shippingStreet);
  }
  get pickupAddress() {
    const shipCity = getFieldValue(this.adf.data, Pickup_shippingCity);
    const shipState = getFieldValue(this.adf.data, Pickup_shippingState);
    const shipPostalCode = getFieldValue(
      this.adf.data,
      Pickup_shippingPostalCode
    );
    let address = "";
    if (shipCity !== null) {
      address = shipCity;
    }
    if (shipState !== null) {
      address = address + ", " + shipState;
    }
    if (shipPostalCode !== null) {
      address = address + " " + shipPostalCode;
    }
    return address;
  }
  get pickupCountry() {
    return getFieldValue(this.adf.data, Pickup_shippingCountry);
  }

  goToScreen(event){
    const screenVal=event.target.name;
    const fields = {};
    fields[ID.fieldApiName] = this.recordId;
    fields[apheresisStatus.fieldApiName]='ADF Pending Submission';
    const recordInput = { fields };
  fireEvent(this.pageRef, 'adfApprovedPage', 'noDisplay');
  //2260
    if(this.isADFViewerInternal){
      fireEvent(this.pageRef, 'showSummaryLink', true);
    }
  if(this.changetoPendingSubmission&& !this.isADFViewerInternal){
    updateRecord(recordInput)
    .then(() => {
      fireEvent(this.pageRef, 'setAdfStatus','ADF Pending Submission');
      const attributeChangeEvent = new FlowAttributeChangeEvent('screenName',screenVal);
      this.dispatchEvent(attributeChangeEvent);
      const navigateNextEvent = new FlowNavigationNextEvent();
      this.dispatchEvent(navigateNextEvent);
      })
      .catch(error => {
         this.error = error;
      });
  }else{
    const attributeChangeEvent = new FlowAttributeChangeEvent('screenName',screenVal);
    this.dispatchEvent(attributeChangeEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }
}

onPrevious(){
            const attributeChangeEvent = new FlowAttributeChangeEvent('screenName', '5');
            this.dispatchEvent(attributeChangeEvent);
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
}
onApprove() {
    this.isApprovalModalOpen = true;
}
onReject(){
  this.isRejectModalOpen=true;
}
onSubmit(){
  this.isSubmitModalOpen=true;
}
closeModal() {
  this.isApprovalModalOpen = false;
  this.isRejectModalOpen=false;
  this.isSubmitModalOpen=false;
}
onPageApproval(){
  this.assignApprovedFieldValue();
}

onPageSubmit(){
  this.assignSubmittedFieldValue();
}


validateCheckbox(){
  const checkBox=this.template.querySelector('.checkBox');
  const button=this.template.querySelector('.approve');
  if(button) {
   if(!checkBox.checked||this.isADFViewerInternal){
    button.disabled=true;
  }else{
    button.disabled=false;
  }
}
}

validateCheckboxSubmit(){
  const checkBox=this.template.querySelector('.checkBoxSubmit');
  const button=this.template.querySelector('.submit');
  if(button) {
  if(!checkBox.checked){
    button.disabled=true;
  }else{
    button.disabled=false;
  }
}
}

getTimeZoneIDScreenDetails(screenNameVal){
  getTimezoneIDDetails({screenName: screenNameVal, adfId: this.recordId})
    .then((result) => {
      this.timeZoneIDVal = result;
      this.getUserTimeZone(this.timeZoneIDVal[approvedDateText.fieldApiName]);
        this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

assignSubmittedFieldValue(){

  if(this.userTimeZone!==undefined && this.userTimeZone!==null && this.userTimeZone!==''){
    let fields=this.userTimeZone.split('T');
    let timeVal=fields[1];
    let dateVal=fields[0];
    let timeValue=timeVal.split(":");
    let textVal=this.formatDate(dateVal)+' '+timeValue[0]+':'+timeValue[1]+' '+this.timeZoneScreen6[submittedDateText.fieldApiName];
    this.getFormattedSubmittedDateTimeVal(dateVal+' '+timeValue[0]+':'+timeValue[1]+':000',this.timeZoneScreen6[submittedDateText.fieldApiName],textVal);
   
  }
      }

  getFormattedSubmittedDateTimeVal(dateTimeVal,timeZoneVal,textVal){
        let formattedVal='';
        const fields = {};
        fields[ID.fieldApiName] = this.recordId;
        fields[submittedBy.fieldApiName] = this.userName.Id;
        fields[submittedDateText.fieldApiName]=textVal;
        getFormattedDateTimeDetails({dateTimeVal:dateTimeVal,timeZoneVal:timeZoneVal})
          .then((result) => {
            this.formattedDateTime = result;
            fields[submittedDate.fieldApiName] =this.formattedDateTime;
            
            fields[apheresisStatus.fieldApiName]='ADF Pending Approval';
            const recordInput = { fields };
           
            updateRecord(recordInput)
                .then(() => {
                  this.isSubmitModalOpen = false;
                  const attributeChangeEvent = new FlowAttributeChangeEvent('screenName', '8');
                  this.dispatchEvent(attributeChangeEvent);
                  const navigateNextEvent = new FlowNavigationNextEvent();
                  this.dispatchEvent(navigateNextEvent);
                  })
                  .catch(error => {
                    this.error = error;
                  });
          })
          .catch((error) => {
            this.error = error;
          });
          return formattedVal;
      }     
   

assignApprovedFieldValue(){
  if(this.userTimeZone!==undefined && this.userTimeZone!==null && this.userTimeZone!==''){
    let fields=this.userTimeZone.split('T');
    let timeVal=fields[1];
    let dateVal=fields[0];
    let timeValue=timeVal.split(":");
    let textVal=this.formatDate(dateVal)+' '+timeValue[0]+':'+timeValue[1]+' '+this.timeZoneScreen6[approvedDateText.fieldApiName];
    this.getFormattedApprovedDateTimeVal(dateVal+' '+timeValue[0]+':'+timeValue[1]+':000',this.timeZoneScreen6[approvedDateText.fieldApiName],textVal);
  }
      }

      getFormattedApprovedDateTimeVal(dateTimeVal,timeZoneVal,textVal){
        let formattedVal='';
        const fields = {};
        fields[ID.fieldApiName] = this.recordId;
        fields[approvedBy.fieldApiName] = this.userName.Id;
        fields[approvedDateText.fieldApiName]=textVal;
        let approvalCounterVal=getFieldValue(this.adf.data, approvalCounter);
        if(approvalCounterVal!==null){
          approvalCounterVal=approvalCounterVal+1;
          fields[approvalCounter.fieldApiName]=approvalCounterVal;
        }else{
          fields[approvalCounter.fieldApiName]=1;
        }
        getFormattedDateTimeDetails({dateTimeVal:dateTimeVal,timeZoneVal:timeZoneVal})
          .then((result) => {
            this.formattedDateTime = result;
            fields[approvedDate.fieldApiName] =this.formattedDateTime;
            fields[apheresisStatus.fieldApiName]='ADF Approved';
            const recordInput = { fields };
            updateRecord(recordInput)
            .then(() => {
              this.isApprovalModalOpen = false;
              fireEvent(this.pageRef, 'adfApprovedPage', 'adfApproved');
              const attributeChangeEvent = new FlowAttributeChangeEvent('screenName', '8');
              this.dispatchEvent(attributeChangeEvent);
              const navigateNextEvent = new FlowNavigationNextEvent();
              this.dispatchEvent(navigateNextEvent);
              })
              .catch(error => {
                this.error = error;
              });
          })
          .catch((error) => {
            this.error = error;
          });
          return formattedVal;
      }         

      handleCommentsChange(event) {
        this.rejectReason = event.target.value;
      }

      onPageReject(){
        this.navigateFurther=true;
        this.validateTextarea();
        const textArea=this.template.querySelector('.rejectionComment');
        if(this.navigateFurther){
        this.assignRejectedFieldValue();
        }
      }

      validateTextarea(){
        const textArea=this.template.querySelector('.rejectionComment');
        let commentValue=textArea.value;
        if(commentValue==undefined){
          this.navigateFurther=false;
          textArea.setCustomValidity('Complete this Field');
          textArea.reportValidity();
        }else{
          this.navigateFurther=true;
          textArea.setCustomValidity('');
          textArea.reportValidity();
        }
      }



  assignRejectedFieldValue(){
  if(this.userTimeZone!==undefined && this.userTimeZone!==null && this.userTimeZone!==''){
    let fields=this.userTimeZone.split('T');
    let timeVal=fields[1];
    let dateVal=fields[0];
    let timeValue=timeVal.split(":");
    let textVal=this.formatDate(dateVal)+' '+timeValue[0]+':'+timeValue[1]+' '+this.timeZoneScreen6[rejectedDateText.fieldApiName];
    this.getFormattedRejectedDateTimeVal(dateVal+' '+timeValue[0]+':'+timeValue[1]+':000',this.timeZoneScreen6[rejectedDateText.fieldApiName],textVal);
  }
}

getFormattedRejectedDateTimeVal(dateTimeVal,timeZoneVal,textVal){
  let formattedVal='';
  const fields = {};
  fields[ID.fieldApiName] = this.recordId;
  fields[rejectedBy.fieldApiName] = this.userName.Id;
  fields[rejectionReason.fieldApiName] = this.rejectReason;
  fields[rejectedDateText.fieldApiName]=textVal;
  getFormattedDateTimeDetails({dateTimeVal:dateTimeVal,timeZoneVal:timeZoneVal})
    .then((result) => {
      this.formattedDateTime = result;
      fields[rejectedDate.fieldApiName] =this.formattedDateTime;
      fields[apheresisStatus.fieldApiName]='ADF Rejected';
      const recordInput = { fields };
      updateRecord(recordInput)
          .then(() => {
            this.isRejectModalOpen = false;
            const attributeChangeEvent = new FlowAttributeChangeEvent('screenName', '8');
            this.dispatchEvent(attributeChangeEvent);
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
            })
            .catch(error => {
              this.error = error;
            });
    })
    .catch((error) => {
      this.error = error;
    });
    return formattedVal;
}     


            viewPdf() {
this.SCPurl='/sfc/servlet.shepherd/document/download/'+this.contentDocumentId;
window.open(this.SCPurl);
          }

          viewDocument(event) {
            this.url='/sfc/servlet.shepherd/document/download/'+event.target.name;
            window.open(this.url)
                  }
				  
	
  //Changes as part of CGTU 355
  removeFile(event){
    if(event.target.name === 'openConfirmation'){
      this.docVersionId=event.target.alternativeText;
      this.isDialogVisible = true;
    }else if(event.target.name === 'confirmModal'){
      //when user clicks outside of the dialog area, the event is dispatched with detail value  as 1
      if(event.detail !== 1){
           if(event.detail.status === 'confirm') {
              deleteFile({contentDocumentId:this.docVersionId}).then((result) => {
              this.handleDeleteFinished();
              this.isDialogVisible = false;
              this.error = null;
            })
            .catch((error) => {
              this.error = error;
            });
          //Changes as part of 355 ends here
              this.isDialogVisible = false;
          }else if(event.detail.status === 'cancel'){
              this.isDialogVisible = false;
          }
      }
      //hides the component
      this.isDialogVisible = false;
  }
  }
  
  //Changes as part of CGTU 355 end here
    //2450
   getAddresses(){
    getAddress({
      accountIds: this.adressIds,
    })
      .then((result) => {
        this.addressList=result;
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }
}
import {
    LightningElement,
    api,
    track
} from 'lwc';
import fetchRecords from "@salesforce/apex/PSP_custLookUpCntrl.getCarePrograms";
import header from '@salesforce/label/c.PSP_Select_Care_Program';
import selectMsg from "@salesforce/label/c.PSP_Please_Select";
import errMsg from "@salesforce/label/c.PSP_No_CPP_Er_Msg";
export default class PSP_getCarePrograms extends LightningElement {
    @api type2;
    @api type1;
    @api selectedProId;
    @api selectedProName;
    @api productServiceName;
    @api rtnValue;
    @track selectMsg = selectMsg;
    @track errMsg = errMsg;
    @track header = header;
    connectedCallback() {

        fetchRecords({
               // careprogramId: this.careProgram,

               programType: this.type1,
               programType1: this.type2

            })
            .then(result => {

                this.rtnValue = result;
                if (result.length === 0) {
                    this.rtnValue = null;
                }

                this.error = null;
            })
            .catch(error => {
                this.error = error;
                this.enrolledprescriptionlist = null;
            });

    }
    handleChange(event) {
        let index = event.target.dataset.item;

        if (event.target.checked) {
            this.selectedProId = this.rtnValue[index].Id;
            this.selectedProName = this.rtnValue[index].Name

        }
    }

    //validate function
    @api
    validate() {

        if (this.selectedProId !== undefined && this.selectedProName !== null) {
            return {
                isValid: true
            };
        } else {
            //If the component is invalid, return the isValid parameter as false and return an error message. 
            return {
                isValid: false,
                errorMessage: header
            };
        }
    }
}
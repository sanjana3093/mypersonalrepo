import { LightningElement } from 'lwc';
import CCL_Document_Warning from "@salesforce/label/c.CCL_Document_Warning";

export default class CCL_StaticTextForDocumentationDetailPage extends LightningElement {
    label = {
        CCL_Document_Warning,  
    };

}
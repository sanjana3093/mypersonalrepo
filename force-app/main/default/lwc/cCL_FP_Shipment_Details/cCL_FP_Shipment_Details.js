import { LightningElement, track, api, wire } from "lwc";
import productAcceptanceLabel from '@salesforce/label/c.CCL_Product_Acceptance_Label';
import finishedProductShipmentHeader from '@salesforce/label/c.CCL_Finished_Product_Shipment_Header';
import hospitalPurchaseOrderNumberLabel from '@salesforce/label/c.CCL_Hospital_Purchase_Order_Number';
import wayBillNumberLabel from '@salesforce/label/c.CCL_Waybill_number';
import dewarTrackingLinkOneLabel from '@salesforce/label/c.CCL_Dewar_Tracking_Link_1';
import dewarTrackingLinkTwoLabel from '@salesforce/label/c.CCL_Dewar_Tracking_Link_2';
import statusLabel from '@salesforce/label/c.CCL_Status';
import shipToLocationLabel from '@salesforce/label/c.CCL_Ship_To_Location';
import shipmentReasonLabel from '@salesforce/label/c.CCL_Shipment_Reason';
import infusionCenterLabel from '@salesforce/label/c.CCL_Infusion_Center';
import productRejectedByLabel from '@salesforce/label/c.CCL_Product_Rejected_by';
import productAcceptancePendingLabel from '@salesforce/label/c.CCL_Product_Acceptance_Pending';
import productAcceptedByLabel from '@salesforce/label/c.CCL_Product_Accepted_by';
import actualProductShipmentDateTimeLabel from '@salesforce/label/c.CCL_Actual_Product_Shipment_Date_Time';
import estimatedProductDeliveryDateTimeLabel from '@salesforce/label/c.CCL_Estimated_Product_Delivery_Date_Time';
import secondaryTherapyLabel from '@salesforce/label/c.CCL_Secondary_Therapy';
import backToShipList from "@salesforce/label/c.CCL_Back_to_Shipment_List";
import rejectShipNoticeLabel from "@salesforce/label/c.CCL_Finished_product_shipment_has_been_rejected";
import viewOrder from "@salesforce/label/c.CCL_View_Order_Summary";
import relatedDocumentsLabel from "@salesforce/label/c.CCL_Related_Documents";
import estimatedProductShipmentDateTimeLabel from "@salesforce/label/c.CCL_Estimated_Product_Shipment_Date_Time";
import { NavigationMixin } from "lightning/navigation";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getFieldDetails from "@salesforce/apex/CCL_FPShipmentController.getShipmentData";
import getFieldDetailsCall from "@salesforce/apex/CCL_FPShipmentController.getShipmentData";
import actualProductDeliveryDateTimeLabel from '@salesforce/label/c.CCL_Actual_Product_Delivery_Date_Time';
import finishedProductBatchInformationLabel from '@salesforce/label/c.CCL_Finished_Product_Batch_Information';
import shippingNumber from '@salesforce/label/c.CCL_Shipping_Number';
import expiryDateOfShippedBagsLabel from '@salesforce/label/c.CCL_expiryDateOfShippedBags';
import numberOfDosesShippedLabel from '@salesforce/label/c.CCL_numberOfDosesShipped';
import numberOfBagsShippedLabel from '@salesforce/label/c.CCL_numberOfBagsShipped';
import numberOfBagsReceivedLabel from '@salesforce/label/c.CCL_numberOfBagsReceived';
import productRejectionNotesLabel from '@salesforce/label/c.CCL_Product_Rejection_Notes';
import productRejectionReasonLabel from '@salesforce/label/c.CCL_Product_Rejection_Reason';
import onHoldMessageLabel from '@salesforce/label/c.CCL_On_Hold_Message';
import shipmentCancellationReason from '@salesforce/label/c.CCL_Shipment_Cancellation_Reason';
import errorArrayavailable from '@salesforce/label/c.CCL_Error_Array_available';
import errorStringvalue from '@salesforce/label/c.CCL_Error_String_value';
import shipmentRejected from '@salesforce/label/c.CCL_Shipment_Rejected';
import shipmentHasBeenRejected from '@salesforce/label/c.CCL_Shipment_has_been_Rejected';
import unknownError from '@salesforce/label/c.CCL_Unknown_error';

import getFileDetails from "@salesforce/apex/CCL_FPShipmentController.getUploadedFileDetails";
import getCheckUserPermission from "@salesforce/apex/CCL_FPShipmentController.checkUserPermission";
import getAddress from "@salesforce/apex/CCL_Utility.fetchAddress";


const productAccValue = 'Accepted';
const productRejValue = 'Rejected';
const onHoldStatusValue = "\"Product Delivery On Hold\"";
const productAcceptanceNull = undefined;
const checkCustomPermission = 'CCL_Primary_FP_Shipment_Flow';

export default class CCL_FP_Shipment_Details extends NavigationMixin(LightningElement) {

  @api recordId;
  @track error;
  @track result;
  @track filesUploaded=[];
  @track showUploadedFiles=false;
  @api holdStatus;
  @track shipmentRecords;
  @track shipmentRecordStatusValue;
  @track disableAcceptRejectButtons = false;
  @track captureUserAccess;
  @track disableRelatedDocumentsIR = false;
  @track captureUserAccessIR;
  @track disableRelatedDocumentsAM = false;
  @track captureUserAccessAM;
  @api addressList;
  @api shipmentAddressList;
  @track addressArray=[];

  @track label = {
    productAcceptanceLabel,
    finishedProductShipmentHeader,
    backToShipList,
    viewOrder,
    dewarTrackingLinkTwoLabel,
    dewarTrackingLinkOneLabel,
    hospitalPurchaseOrderNumberLabel,
    wayBillNumberLabel,
    statusLabel,
    shipToLocationLabel,
    shipmentReasonLabel,
    infusionCenterLabel,
    actualProductShipmentDateTimeLabel,
    estimatedProductDeliveryDateTimeLabel,
    productRejectedByLabel,
    productAcceptancePendingLabel,
    productAcceptedByLabel,
    relatedDocumentsLabel,
    estimatedProductShipmentDateTimeLabel,
    secondaryTherapyLabel,
    actualProductDeliveryDateTimeLabel,
    finishedProductBatchInformationLabel,
    expiryDateOfShippedBagsLabel,
    shipmentCancellationReason,
    numberOfDosesShippedLabel,
    numberOfBagsShippedLabel,
    numberOfBagsReceivedLabel,
    productRejectionNotesLabel,
    productRejectionReasonLabel,
    rejectShipNoticeLabel,
    shippingNumber,
    onHoldMessageLabel
  };

  @wire(getFieldDetails, { recordId: '$recordId'})
  wiredShipmentDetails({data, error}){
      
      if(data){
          this.result= data;

          if(this.result.CCL_Infusion_Center__c || this.result.CCL_Ship_to_Location__c ){
            if(this.result.CCL_Infusion_Center__c){
             this.addressArray.push(this.result.CCL_Infusion_Center__c);
            }
            if(this.result.CCL_Ship_to_Location__c){
              this.addressArray.push(this.result.CCL_Ship_to_Location__c);
            }
            this.getAddresses();
          }


      }
      else if (error) {
        this.error = unknownError;
        if (Array.isArray(error.body)) {
          this.error = error.body.map(e => e.message).join(', ');
          this.dispatchEvent(
            new ShowToastEvent({
              title: errorArrayavailable,
              message: error.body.message,
              variant: 'error',
            }),
          );
        } else if (typeof error.body.message === 'string') {
          this.error = error.body.message;
          this.dispatchEvent(
            new ShowToastEvent({
              title: errorStringvalue,
              message: error.body.message,
              variant: 'error',
            }),
          );
        }
        this.result = null;
      }
  }


  get fPAcceptanceStatusValue() {
    if (this.result.CCL_Product_Acceptance_Status__c === productAccValue) {
      this.productAcceptanceStatusText = this.label.productAcceptedByLabel;
        }
    if (this.result.CCL_Product_Acceptance_Status__c === productRejValue ) {
       this.productAcceptanceStatusText = this.label.productRejectedByLabel;
    }
    if (this.result.CCL_Product_Acceptance_Status__c === productAcceptanceNull ) {
      this.productAcceptanceStatusText = this.label.productAcceptancePendingLabel;
   }
    return this.result ? this.productAcceptanceStatusText : '' ;
  }

  get status() {
    return this.result ? this.result.CCL_Status__c : '';
  }
  get shipmentReason() {
    return this.result ? this.result.CCL_Shipment_Reason__c : '';
  }
  get infusionCenter() {
    return this.result ? this.result.CCL_Infusion_Center_Formula__c : '';
  }
  get shipToLocation() {
    return this.result ? this.result.CCL_Ship_to_Location_Formula__c : '';
  }
  get hospitalPurchaseOrderNumber() {
    return this.result ? this.result.CCL_Hospital_Purchase_Order_Number__c : '';
  }
  get wayBillNumber() {
    return this.result ? this.result.CCL_Waybill_number__c : '';
  }
  get dewarTrackLinkOne() {
    return this.result ? this.result.CCL_Dewar_Tracking_link_1__c : '';
  }
  get dewarTrackLinkTwo() {
    return this.result ? this.result.CCL_Dewar_Tracking_link_2__c : '';
  }
  get actualProductShipDateTime() {
    return this.result ? this.result.CCL_Actual_Shipment_Date_Time__c : '';
  }
  get estimatedProductDeliveryDateTime() {
    return this.result ? this.result.CCL_Estimated_FP_Shipment_Date_Time__c : '';
  }
  get productAccRejUserTime() {
    if(this.result.CCL_Product_Acceptance_Status__c === productAccValue ){
      this.productAcceptanceUserAndDateTimeTimezonestamp = this.result.CCL_Acceptance_Rejection_Details__c;
    }
    if(this.result.CCL_Product_Acceptance_Status__c === productRejValue ){
      this.productAcceptanceUserAndDateTimeTimezonestamp = this.result.CCL_Acceptance_Rejection_Details__c;
    }
  return this.result ? this.productAcceptanceUserAndDateTimeTimezonestamp : '';
}

get infusionShippingStreet() {
  return this.result ? this.result.CCL_Infusion_Center__r.ShippingStreet : '';
}

get infusionShippingCity() {
  return this.result ? this.result.CCL_Infusion_Center__r.ShippingCity : '';
}
get infusionShippingState() {
  return this.result ? this.result.CCL_Infusion_Center__r.ShippingState : '';
}
get infusionShippingPostalCode() {
  return this.result ? this.result.CCL_Infusion_Center__r.ShippingPostalCode : '';
}
get infusionShippingCountry() {
  return this.result ? this.result.CCL_Infusion_Center__r.ShippingCountry : '';
}

get shipToLocationShippingStreet() {
  return this.result ? this.result.CCL_Ship_to_Location__r.ShippingStreet : '';
}

get shipToLocationShippingCity() {
  return this.result ? this.result.CCL_Ship_to_Location__r.ShippingCity : '';
}
get shipToLocationShippingState() {
  return this.result ? this.result.CCL_Ship_to_Location__r.ShippingState : '';
}
get shipToLocationShippingPostalCode() {
  return this.result ? this.result.CCL_Ship_to_Location__r.ShippingPostalCode : '';
}
get shipToLocationShippingCountry() {
  return this.result ? this.result.CCL_Ship_to_Location__r.ShippingCountry : '';
}

get estimatedFPDeliveryDateTime() {
  return this.result ? this.result.CCL_Estimated_FP_Delivery_Date_Time_Text__c : '';
}
get estimatedFPShipmentDateTime() {
  return this.result ? this.result.CCL_Estimated_FP_Shipment_Date_Time_Text__c : '';
}

get shipmentLocationTimeZone() {
  return this.result ? this.result.CCL_Ship_to_Time_Zone__c : '';
}

get plantLocationTimeZone() {
  return this.result.CCL_Actual_Shipment_Date_Time_Text__c ? this.result.CCL_Manufacturing_Time_Zone__c : '';
}

get actualShipmentDateTime() {
  return this.result ? this.result.CCL_Actual_Shipment_Date_Time_Text__c : '';
}

get secondaryTherapy() {
  return this.result ? this.result.CCL_Secondary_Therapy__c : '';
}
get actualProductDeliveryDateTime() {
  return this.result ? this.result.CCL_Actual_FP_Delivery_Date_Time_Text__c : '';
}

get shipmentCancellationReason() {
  return this.result ? this.result.CCL_Shipment_Cancellation_Reason__c : '';
}

get numberOfBagsReceived() {
  return this.result ? this.result.CCL_Number_of_Bags_Received__c : '';
}

get numberOfDosesShipped() {
  return this.result ? this.result.CCL_Number_of_Doses_Shipped__c : '';
}

get numberOfBagsShipped() {
  return this.result ? this.result.CCL_Number_of_Bags_Shipped__c : '';
}
get expiryDateOfShippedBags() {
  return this.result ? this.result.CCL_Expiry_Date_of_Shipped_Bags_Text__c : '';
}

get productRejectionReason() {
  return this.result ? this.result.CCL_Product_Rejection_Reason__c : '';
}

get productRejectionNotes() {
  return this.result ? this.result.CCL_Product_Rejection_Notes__c  : '';
}

get orderId() {
  return this.result ? this.result.CCL_Order__r.Id : '';
}

get orderName() {
  return this.result ? this.result.CCL_Order__r.Name  : '';
}


redirectToOrderSummaryPage() {

  this[NavigationMixin.Navigate]({
      type: 'standard__webPage',
      attributes: {
          url: '/ccl-order/' + this.result.CCL_Order__r.Id +'/' + this.result.CCL_Order__r.Name
      }
  },
  true
);
}

showRejectedShipmentToast() {
  if(this.returnVal !== undefined) {
      this.dispatchEvent(
          new ShowToastEvent({
          title: shipmentRejected,
          message: shipmentHasBeenRejected,
          variant: 'error',
          mode: 'dismissable',
          }),
      );
  }
}

navigateToView() {

  this[NavigationMixin.Navigate]({
    type: 'standard__webPage',
    attributes: {
        url: '/?tabset-feec3=b498a'
    }
  },
    true
  );
}

navigateToViewHome() {
  this[NavigationMixin.Navigate]({
    type: "comm__namedPage",
    attributes: {
      name: "Home"
    }
  });
}

  connectedCallback()
    {
      

  getFieldDetailsCall({recordId:this.recordId})
        .then(result => {
          
            this.shipmentRecordStatusValue = JSON.stringify(result.CCL_Status__c);
            

            this.error=undefined;

                if(this.shipmentRecordStatusValue!=null)
                {
                  

                  if(this.shipmentRecordStatusValue==onHoldStatusValue)

                  {
                    this.holdStatus=true;
                    

                  }
                  else{
                    this.holdStatus=false;
                    

                  }
               }

        })
        .catch(error => {
          
            this.error = error;
            this.shipmentRecords=undefined;
            this.shipmentRecordStatusValue=null;
        });
        
}

  @wire(getFileDetails, { recordId: '$recordId' })
  wiredFileListSteps({ error, data }) {
      if (data) {
          this.filesUploaded = data;
          

          if(this.filesUploaded.length>0){
            this.showUploadedFiles=true;
            

          }
          this.error = null;
      } else if (error) {
          this.error = error;
      }
  }

viewDocument(event) {
  this.isFileViewModalOpen=true;
  this.url='/sfc/servlet.shepherd/document/download/'+event.target.name;
  window.open(this.url)
        }


getCustomPermission() {
  getCheckUserPermission({
    permissionName: checkCustomPermission
  })
    .then((result) => {
      
      this.captureUserAccess = result;
      if(this.captureUserAccess === true) {
          this.disableAcceptRejectButtons = true;
      }else {
          this.disableAcceptRejectButtons = false;
      }
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}

goToListScreen(){
  this[NavigationMixin.Navigate]({
    type: "comm__namedPage",
    attributes: {
      pageName: "finished-product-list-view"
    }
  });
  }



renderedCallback() {
  this.getCustomPermission();

}
getAddresses(){
  getAddress({
    accountIds: this.addressArray,
  })
    .then((result) => {
      this.addressList=result;
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}


}

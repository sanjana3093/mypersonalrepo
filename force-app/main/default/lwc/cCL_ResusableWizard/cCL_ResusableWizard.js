import { LightningElement, track, wire, api } from "lwc";
import { registerListener, unregisterAllListeners } from "c/cCL_Pubsub";
import { CurrentPageReference } from "lightning/navigation";

export default class CCL_ResusableWizard extends LightningElement {
  @track message;
  @track UiClass;
  @track liClass = "slds-wizard__item slds-wizard__marker";
  @api stepName=['screen 1','screen 2','screen 3'];
  @wire(CurrentPageReference) pageRef;
  connectedCallback() {
    registerListener("screenName", this.handleMessage, this);
  }
  handleMessage(myMessage) {
    this.message = myMessage;
    let self = this;
    var children = this.querySelectorAll(".slds-wizard__marker");
    var bar = this.querySelectorAll(".divider");
    Array.from(this.template.querySelectorAll(".slds-wizard__marker")).forEach(
      function (node) {
        if (node.dataset.item < myMessage) {
          node.classList.add("slds-is-active");
        } else {
          node.classList.remove("slds-is-active");
        }
        if (node.dataset.item === myMessage) {
          node.classList.add("isCurrent");
        } else {
          node.classList.remove("isCurrent");
        }
      }
    );
    Array.from(this.template.querySelectorAll(".slds-wizard__label")).forEach(
      function (node) {
        if (node.dataset.item === myMessage) {
          node.classList.add("slds-is-current");
        } else {
          node.classList.remove("slds-is-current");
        }
        if (node.dataset.item < myMessage) {
          node.classList.remove("wizard-text-align");
        } else {
          node.classList.add("wizard-text-align");
        }
      }
    );
    Array.from(this.template.querySelectorAll(".myIcon")).forEach(function (
      node
    ) {
      if (node.dataset.item < myMessage) {
        node.classList.remove("mynonvisible");
      } else {
        node.classList.add("mynonvisible");
      }
    });
    Array.from(this.template.querySelectorAll(".divider")).forEach(function (
      node
    ) {
      if (node.dataset.item < myMessage) {
        node.classList.add("blue");
      } else {
        node.classList.remove("blue");
      }

      if (node.dataset.item == self.stepName.length - 1) {
        node.classList.add("slds-hide");
      }
    });

  }

  disconnectCallback() {
    unregisterAllListeners(this);
  }

}
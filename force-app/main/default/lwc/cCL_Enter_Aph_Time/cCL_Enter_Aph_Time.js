import { LightningElement,api,track,wire } from 'lwc';
import getAccountDetails from "@salesforce/apex/CCL_ADF_Controller.getRecords";
import getPickUpTimeZone from "@salesforce/apex/CCL_ADFController_Utility.getTimeZone";
import getFormattedDateTimeDetails from "@salesforce/apex/CCL_ADFController_Utility.getFormattedDateTimeDetails";
import { updateRecord } from "lightning/uiRecordApi";
import editAphTimeMsg from "@salesforce/label/c.CCL_APh_Edit_Threshold";
import editAphTimeAccess from "@salesforce/label/c.CCL_Planned_Aph_Edit_Access";
import siteTimeZone from "@salesforce/label/c.CCL_Time_Zone_Help_Text";
import checkUserPermission from "@salesforce/apex/CCL_ADFController_Utility.checkUserPermission";

export default class CCL_Enter_Aph_Time extends LightningElement {
    @api recordId;
    @api apiNames = [];
    @track sobj;
    @track hasFieldvalues;
    @track orderId;
    @track pickupLocId;
    @track timeZone;
    @track isModelOpen=false;
    @track myTimeFields = {};
    @track plannedApheresisDate;
    @track plannedApheresistext;
    @track shipmentAphText;
    @track plannedApheresisTime;
    @track orderFields = {};
    @track orderInput;
    @track formattedApheresisTime;
    @track editPlannedApheresis;
    @track isInboundUser;
    @track inboundPermission = "CCL_Inbound_Logistics";
    @track label = {
        editAphTimeMsg,
        editAphTimeAccess,
        siteTimeZone
      };


    connectedCallback() {
        this.checkPermission(this.inboundPermission);
    }

    checkPermission(param) {
      checkUserPermission({
        permissionName: param
      })
        .then((result) => {
         if(result){
          this.isInboundUser=true;
          this.fetchShipmentDetails();
         }
         else{
          this.isInboundUser=false;
        
         }
          this.error = null;
        })
        .catch((error) => {
          
          this.error = error;
        });
    }
    fetchShipmentDetails(){
        this.sobj = "CCL_Shipment__c";
        this.apiNames.push("CCL_Order_Apheresis__c");
        this.apiNames.push("CCL_Planned_Apheresis_Pickup_Date__c");
        this.apiNames.push("CCL_Status__c");
        this.apiNames.push("CCL_Pick_up_Location__c");
		this.apiNames.push("CCL_TEXT_Planned_Cryro_Aphsersis_Pick_Up__c");
        this.apiNames = [...new Set(this.apiNames)];
        getAccountDetails({
            sobj: this.sobj,
            cols: this.apiNames.toString(),
            recordId: this.recordId
          })
            .then((result) => {
              this.hasFieldvalues = result;
              this.orderId = result[0].CCL_Order_Apheresis__c;
              this.pickupLocId = result[0].CCL_Pick_up_Location__c;
              this.setModal();
              this.error = null;
            })
            .catch((error) => {
              
              this.error = error;
            });
    }
    @wire(getPickUpTimeZone, { recordId: "$pickupLocId" })
    wiredTime({ error, data }) {
      if (data) {
        this.timeZone = data[0].CCL_Time_Zone__r.Name;
        this.error = null;
      } else if (error) {
        
        this.error = error;
      }
    }
    setModal() {
        this.isModelOpen=true;
        const status=this.hasFieldvalues[0].CCL_Status__c;
		const result1=this.hasFieldvalues[0].CCL_TEXT_Planned_Cryro_Aphsersis_Pick_Up__c;
		 
        if(status==='Apheresis Pick Up Planned'){
            this.editPlannedApheresis=true;
        }
        else{
          this.editPlannedApheresis=false;
        }
        const date=result1.substring(0, 12);;
        this.plannedApheresisDate=this.formatDate(date);
       this.myTimeFields['time']=result1.substring(12, 18);
        this.plannedApheresisTime=result1.substring(12, 18);
      }
      handleModelChange(event) {
        if ( event.target.type == "time") {
          this.myTimeFields['time'] = event.target.value;
          this.plannedApheresisTime=event.target.value;
          this.plannedApheresistext=this.plannedApheresisDate+" "+this.myTimeFields['time'] +' '+this.timeZone;
          this.shipmentAphText=this.plannedApheresisDate+" "+this.myTimeFields['time'].substr(0,5) +' '+this.timeZone;
		  
        }
      }
      handleModelBlur(event){
        let comp2 = this.template.querySelectorAll(".timeClass");
        if (event.target.type == "time") {
          this.myTimeFields['time'] = event.target.value;
          comp2.forEach(function (node) {
            node.value = "";
          });
        }
      }
      saveApheresis(){
        this.getFormattedDateTimeValue(this.plannedApheresisDate,this.plannedApheresisTime,'CCL_Planned_Apheresis_Pick_up_Date_Time__c');
        this.isModelOpen=false;
        const saveQA = new CustomEvent('save');
        this.dispatchEvent(saveQA);
          }
          getFormattedDateTimeValue(date, time, dateTimeField) {
            let dateTime = this.formatDateFormat(date.substr(0, 11)) + " " + time;
            getFormattedDateTimeDetails({
              dateTimeVal: dateTime,
              timeZoneVal: this.timeZone
            })
              .then((result) => {
                this.formattedApheresisTime = result;
                
               this.orderFields[dateTimeField] = this.formattedApheresisTime;
               this.updateOrder();
              })
              .catch((error) => {
                this.error = error;
                
              });
          }
          updateOrder(){
            this.orderFields['Id']=this.orderId;
            let recordInput,shipmentFields={};
            this.orderFields['CCL_Planned_Cryopreserved_Apheresis_Text__c']=this.plannedApheresistext;
            this.orderInput={ fields: this.orderFields };
            
            shipmentFields['Id']=this.recordId;
            shipmentFields['CCL_Planned_Apheresis_Pickup_Date__c']= this.formattedApheresisTime;
            shipmentFields['CCL_TEXT_Planned_Cryro_Aphsersis_Pick_Up__c']=this.shipmentAphText;
            recordInput={ fields: shipmentFields };
            updateRecord(recordInput)
            .then(() => {
              
            })
            .catch((error) => {
              
            });
            updateRecord(this.orderInput)
            .then(() => {
              
            })
            .catch((error) => {
              
            });
            eval("$A.get('e.force:refreshView').fire();");
          }
          formatDateFormat(date) {
            if (date !== null && date !== "" && date !== undefined) {
              let d = new Date(date),
                month = "" + (d.getMonth() + 1),
                day = "" + d.getDate(),
                year = d.getFullYear();

              if (month.length < 2) month = "0" + month;
              if (day.length < 2) day = "0" + day;

              return [year, month, day].join("-");
            }

            return "";
          }

      formatDate(date) {
        let mydate = new Date(date);
        let monthNames = [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec"
        ];

        let day = "" + mydate.getDate();
        if (day.length < 2) day = "0" + day;
        let monthIndex = mydate.getMonth();
        let monthName = monthNames[monthIndex];
        let year = mydate.getFullYear();
        return [day, monthName, year].join(" ");
      }
      closeModal() {
        this.isModelOpen=false;
        const closeQA = new CustomEvent('close');
        this.dispatchEvent(closeQA);
    }
      }
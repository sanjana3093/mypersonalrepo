import { LightningElement, track, wire, api } from "lwc";
import { getRecord } from "lightning/uiRecordApi";
import getAccountDetails from "@salesforce/apex/CCL_ADF_Controller.getRecords";
import getPRFList from "@salesforce/apex/CCL_ADF_Controller.getPRF";
import prfSummary from "@salesforce/label/c.CCL_PRF_Summary";
import CCL_Therapy_Type__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Therapy_Type__c";
import replacement_FIELD  from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Returning_Patient__c";
import CCL_Principal_Investigator from "@salesforce/label/c.CCL_Principal_Investigator"
import CCL_Prescriber from "@salesforce/label/c.CCL_Prescriber"
import CCL_Order_Summary_Replacement from "@salesforce/label/c.CCL_Order_Summary_Replacement"
import getShipmentList from "@salesforce/apex/CCL_ADFController_Utility.getShipmentInfo";
import getOrderHardPeg from "@salesforce/apex/CCL_ADFController_Utility.getADFAssociatedOrderHardPeg";
import getOrderApprovalInfo from "@salesforce/apex/CCL_ADFController_Utility.getOrderApprovalInfo";
import CCL_To_Be_Confirmed_Label from "@salesforce/label/c.CCL_To_Be_Confirmed_Label";
import CCL_PRF_Resubmission_Approval from "@salesforce/label/c.CCL_PRF_Resubmission_Approval";
import CCL_Warning from "@salesforce/label/c.CCL_Warning";
import CCL_APH_onHold from "@salesforce/label/c.CCL_APH_onHold";
import CCL_Note from "@salesforce/label/c.CCL_show_Toast_Message_On_Hold_Note";
import getAddress from "@salesforce/apex/CCL_Utility.fetchAddress";
import {
  FlowNavigationNextEvent,
  FlowAttributeChangeEvent
} from "lightning/flowSupport";
//translation changes
import CCL_Site_and_Scheduling_Information from "@salesforce/label/c.CCL_Site_and_Scheduling_Information";
/*import registerListener from pubsub module*/
import { registerListener } from "c/cCL_Pubsub";
import { CurrentPageReference, NavigationMixin } from "lightning/navigation";
export default class CCLPRFSummary extends LightningElement {
  @track accountSourceval;
  @track selectedVal;
  @track showMessage = false;
  @api recordId;
  //tranlsation changes
  sectionLabels={CCL_Site_and_Scheduling_Information};
  @api therapyName;
  @api isClinical=false;
  @track prfSummary = prfSummary;
  @track sobj;
  @track configArr;
  @track isReview;
  @track apiNames = [];
  @track myActualArr = [];
  @track hasFieldvalues;
  @track replaceOrder;
  @api configVal;
  @track noteReplaceOrder;
  @track countReplace=0;
  @api screenName;
  @track displayWarningMessage=false;
  @track shipmentDetails;
  @track orderHardPeg;
  @track associatedOrderId;
  @track hardPegVal= false;
	 @track prfApprovalRequired;
  @track prfResubmission;
  @track prfStatus;
  @track prfWarningMessage= false;
  @track labels = {
    CCL_PRF_Resubmission_Approval,
    CCL_Warning,
    CCL_APH_onHold,
    CCL_Note
};
// 2450 code
  @track adressIds=[];
  @api addressList;
  @api siteIds=[];
  /*Added to support pubsub module*/
@wire(CurrentPageReference) pageRef;
  get options() {
    return [
      { label: "Yes", value: true },
      { label: "No", value: false }
    ];
  }
  @wire(getRecord, {
    recordId: "$recordId",
   fields: [CCL_Therapy_Type__c,replacement_FIELD]
  })
  wiredRecord({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
      const Data = data;
      const val = Data.fields.CCL_Therapy_Type__c.value;
      const val_Replace=Data.fields.CCL_Returning_Patient__c.value;
      if (val==='Clinical'){
        this.isClinical=true;
      }
      if(val_Replace){
        this.replaceOrder = true;
          this.noteReplaceOrder = CCL_Order_Summary_Replacement;
      }
    }
  }

  @wire(getOrderHardPeg, { recordId: "$recordId" })
  wiredAssocidatedOrderDetails({ error, data }) {
    if (data) {
      this.orderHardPeg =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_Hard_Peg__c:this.orderHardPeg;
      //this.orderHardPeg
      this.associatedOrderId =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.Id:this.associatedOrderId;
      if(data[0].CCL_Order__r.CCL_Hard_Peg__c == true) {
        this.hardPegVal = true;
      }

      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }

@wire(getOrderApprovalInfo, { recordId: "$recordId" })
  wiredAssocidatedOrderApprovalDetails({ error, data }) {
    if (data) {
      this.prfApprovalRequired =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_Order_Approval_Eligibility__c:this.prfApprovalRequired;
      this.prfResubmission =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Approval_Counter__c:this.prfResubmission;
      this.prfStatus =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Ordering_Status__c:this.prfStatus;

      if(this.prfApprovalRequired && this.prfResubmission >='1' && this.prfStatus!='PRF_Approved') {
        this.prfWarningMessage = true;
      }
    } else if (error) {
      this.error = error;
    }
  }
  @wire(getPRFList, {therapyName:'$therapyName'})
  wiredSteps({ error, data }) {
    if (data) {
      this.sobj = data[0].CCL_Object_API_Name__c;
      this.configVal = data;
      let mySectioned;
      mySectioned = this.getSections(this.configVal);
      this.configArr= this.getFields(data, mySectioned);
      this.error = null;
      this.gotresult = true;
    } else if (error) {
      this.error = error;
    }
  }
  connectedCallback() {
    if(this.gotresult){
      this.getValues();
    }
    window.addEventListener(
      "beforeunload",
      this.beforeUnloadHandler.bind(this)
    );
    registerListener("gotoSummary", this.gotoADFSummary, this);
  }

  getShipmentDetails(){
    getShipmentList({recordId: this.recordId})
      .then((result) => {
        this.shipmentDetails = result;
        if(this.shipmentDetails[0].CCL_Status__c==='Apheresis Pick Up On Hold'){
          this.displayWarningMessage=true;
        }
          this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }

  renderedCallback(){
    const checkReplace = this.template.querySelector('.repalceOrderStyle');
    this.countReplace = this.countReplace + 1;
    if(this.countReplace !=2 && checkReplace!=null){
      checkReplace.classList.remove('repalceOrderDisplay');
    }
    this.getShipmentDetails();
  }

  beforeUnloadHandler(evt) {
    if (typeof evt == "undefined") {
      evt = window.event;
    }
    if (evt) {
      evt.returnValue = "Are you sure you want to leave?";
    }
    return "Are you sure you want to leave?";
  }
  handleSelected(event) {
    this.selectedVal = event.target.value;
    if (
      this.selectedVal != null &&
      this.selectedVal != undefined &&
      this.selectedVal != ""
    ) {
      this.showMessage = true;
    } else {
      this.showMessage = false;
    }
  }
  getSections(myArr) {
    let configArr = [];
    let sectionnames = [];
   //tranlsation changes
    let self=this;
    let myObj = { sectioname: "", myFields: [] };
    myArr.forEach(function (node) {
      sectionnames.push(self.sectionLabels[node.CCL_Section_Name__c]);
    });
    let uniq = [...new Set(sectionnames)];
    uniq.forEach(function (node) {
      myObj = { sectioname: "", myFields: [] };
      myObj.sectioname = node;
      configArr.push(myObj);
    });
    return configArr;
  }
  getFields(mainArrr, configArr) {
    let i, j;
    let self = this;
    for (i = 0; i < mainArrr.length; i++) {
      for (j = 0; j < configArr.length; j++) {
        if (configArr[j].sectioname ===  self.sectionLabels[mainArrr[i].CCL_Section_Name__c]) {
          self. setFields(configArr,mainArrr,i,j);
        }
      }
    }
    this.apiNames.push('CCL_Therapy_Type__c');
    configArr.forEach(function (node) {
      node.myFields = self.sortData("fieldorder", "asc", node.myFields);
    });
    this.myActualArr = configArr;
    this.getValues();
  }

  setFields(configArr,mainArrr,i,j){
    let address;
    let fieldArr = {
      Fieldlabel: "",
      fieldApiName: "",
      fieldtype: "",
      fieldorder: "",
      fieldValue: "",
      fieldhelptext: "",
      isRadio: "",
      fieldAddress:'',
      showWarning:'',
	   isSite:''
    };
    fieldArr.Fieldlabel = mainArrr[i].MasterLabel;
	if (
      mainArrr[i].CCL_Field_Api_Name__c == 'CCL_Prescriber__c' &&
      this.isClinical
    ) {
      fieldArr.Fieldlabel = CCL_Principal_Investigator;
    } else if (mainArrr[i].CCL_Field_Api_Name__c == 'CCL_Prescriber__c' &&
    !this.isClinical) {
      fieldArr.Fieldlabel = CCL_Prescriber;
    }
    fieldArr.fieldApiName = mainArrr[i].CCL_Field_Api_Name__c;
    if(mainArrr[i].CCL_Field_Type__c=='Lookup'){
      fieldArr.fieldApiName= fieldArr.fieldApiName.substring(0,  (fieldArr.fieldApiName).length - 1)+'r.Name';
		//2450 code
	   fieldArr.isSite=true;
      if(mainArrr[i].CCL_HasAddress__c){
        address=fieldArr.fieldApiName.substring(0,  (fieldArr.fieldApiName).length - 5)+'.ShippingAddress';
        this.apiNames.push(address);
      }
    }
    if(mainArrr[i].CCL_Field_Api_Name__c==="CCL_TEXT_Planned_Cryo_Apheresis_Pick_up__c"){
      fieldArr.showWarning=true;
    }
	 if(mainArrr[i].CCL_Field_Api_Name__c == 'CCL_Prescriber__c'){
      fieldArr.isSite=false;
    }
    fieldArr.fieldtype = mainArrr[i].CCL_Field_Type__c;
    fieldArr.fieldorder = mainArrr[i].CCL_Order_of_Field__c;
    fieldArr.fieldhelptext = mainArrr[i].CCL_Help_Text__c;
    fieldArr.isRadio =
      mainArrr[i].CCL_Field_Type__c === "Checkbox" ? true : false;
    this.apiNames.push( fieldArr.fieldApiName);
    configArr[j].myFields.push(fieldArr);
  }
  sortData(fieldName, sortDirection, mydata) {
    let data = JSON.parse(JSON.stringify(mydata));
    let key = (a) => a[fieldName];
    let reverse = sortDirection === "asc" ? 1 : -1;
    data.sort((a, b) => {
      let valueA = key(a) ? key(a) : "";
      let valueB = key(b) ? key(b) : "";
      return reverse * ((valueA > valueB) - (valueB > valueA));
    });

    return data;
  }
  getValues() {
    let self = this;
    getAccountDetails({
      sobj: this.sobj,
      cols: this.apiNames.toString(),
      recordId: this.recordId
    })
      .then((result) => {
        this.hasFieldvalues = result;
        this.isClinical=this.hasFieldvalues[0].CCL_Therapy_Type__c==='Clinical'?true:false;
        this.myActualArr.forEach(function (node) {
          node.myFields = self.setValues(node.myFields);
        });
		  //2450
        if(this.adressIds.length>0) {
         this.getAddresses();
        }
        if (result.length === 0) {
          this.hasFieldvalues = null;
        }

        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }
  setValues(myArr) {
    let temp = this.hasFieldvalues[0];
    let self=this;
	 let tbcLabel=false;
    myArr.forEach(function (node) {
      let fieldNameVal=temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-5)];
      if(fieldNameVal==undefined){
      node.fieldValue ='';
      }else{
         //2450
        node.fieldValue =node.fieldtype=='Lookup'?temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-5)].Id: temp[node.fieldApiName];
        if(node.fieldtype=='Lookup'&& !node.isSite){
          node.fieldValue=temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-5)].Name;
        }
        node.fieldAddress=node.fieldtype=='Lookup'?temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-5)].ShippingAddress: null;
      }
      if(node.fieldtype==='Date'){
        node.fieldValue=self.formatDate(temp[node.fieldApiName]);
      }
      if((node.fieldApiName==='CCL_Manufacturing_Plant__r.Name' && !self.hardPegVal) || (node.fieldApiName==='CCL_Manufacturing_Plant__r.Name' && node.fieldValue=='')){
        node.fieldAddress='';
		 //2450
        tbcLabel=true;
        node.isSite=false;
        node.fieldValue=CCL_To_Be_Confirmed_Label;
      }
      if(node.fieldtype==='Text'){
        node.fieldValue=temp[node.fieldApiName];
        node.fieldValue=self.setDateAndTimeFields(node.fieldValue);
      }
	   //2450
      if(node.isSite&&((node.fieldApiName=='CCL_Manufacturing_Plant__r.Name' &&!tbcLabel)|| node.fieldApiName!='CCL_Manufacturing_Plant__r.Name')){
       self.siteIds.push(temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-5)].Id);
        self.adressIds.push(temp[node.fieldApiName.substring(0,(node.fieldApiName.length)-6)+'c']);
      }
    });
    return myArr;
  }

  setDateAndTimeFields(nodeValue){
  let fieldValue=nodeValue;
  let dateValue='';
  let finalVal='';
  if(fieldValue!==undefined && fieldValue!==''){
    let fields=fieldValue.split(' ');
    dateValue=fields[0]+' '+fields[1]+' '+fields[2];
    if(fields[3]==='00:00'){
      finalVal=dateValue;
    }else{
      finalVal=nodeValue;
    }
  }
  return finalVal;
  }

  onNext() {
    //validation
    const screenNextEvent = new FlowAttributeChangeEvent('screenName','20');
    this.dispatchEvent(screenNextEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }
  formatDate(date) {
    let mydate = new Date(date);
    let monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];

    let day = mydate.getDate();
    let monthIndex = mydate.getMonth();
    let monthName = monthNames[monthIndex];
    let year = mydate.getFullYear();
    return `${day} ${monthName} ${year}`;
  }
  onPrevious() {
    // previous code here
    const screenBackEvent = new FlowAttributeChangeEvent('screenName','0');
    this.dispatchEvent(screenBackEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }
  //2450
  getAddresses(){
    getAddress({
      accountIds: this.adressIds,
    })
      .then((result) => {
        this.addressList=result;
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }
  gotoADFSummary() {
    const screenNextEvent = new FlowAttributeChangeEvent("screenName", "6");
    this.dispatchEvent(screenNextEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }
}
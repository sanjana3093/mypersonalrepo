import { LightningElement,track, wire, api } from 'lwc';
import {getRecord} from 'lightning/uiRecordApi';
import {
    FlowNavigationNextEvent
  } from "lightning/flowSupport";

import CCL_ApproverMessage from "@salesforce/label/c.CCL_ApproverMessage";
import CCL_ApproverDinMessage from "@salesforce/label/c.CCL_ApproverDinMessage";
import CCL_ApproverSecMessage from "@salesforce/label/c.CCL_ApproverSecMessage";
import CCL_ApproverButtonLabel from "@salesforce/label/c.CCL_ApproverButtonLabel";
import CCL_ApproverError from "@salesforce/label/c.CCL_ApproverError";
import CCL_ApproverDinError from "@salesforce/label/c.CCL_ApproverDinError";
import CCL_ApproverSecError from "@salesforce/label/c.CCL_ApproverSecError";

import USER_ID from '@salesforce/user/Id';
import NAME_FIELD from '@salesforce/schema/User.Name';


import getUserTimeZoneDetails from "@salesforce/apex/CCL_ADF_Controller.getUserTimeZoneDetails";
import getTimezoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getTimeZoneDetails";
const CCL_TEXT_ADF_Approved_Date_Time__c = "CCL_TEXT_ADF_Approved_Date_Time__c";

//2260
import {CurrentPageReference } from "lightning/navigation";
//2260
import {registerListener} from 'c/cCL_Pubsub';

export default class CCL_Summary_And_Approval extends LightningElement {
  @api recordId;
   @api jsonData;
    @track error;
    @track name;
    @track dateTime;
    @track message;
    @track labels={
        CCL_ApproverButtonLabel
    };
    @track myInput;//received from the html
    @track apheresisId;//received from the record
    @track collectionList = [];
    @track index = 0;
    @track prevVal = [];
    @track flag=false;
    @track idType;
    @track errorMessage;

    @api timeFormat='EEEE dd MMMMM yyyy\' \'HH:mm z';
    @api timeZoneFormatted;
    @api userTimeZone;
    @api screenName="Approval Screen";
    @api allTimeZoneDetails;
    @api userTimeZoneVal;
    @api userDateText;

    @wire(CurrentPageReference) pageRef;


    @wire(getRecord, {
        recordId: USER_ID,
        fields: [NAME_FIELD]
    }) wireuser({
        error,
        data
    }) {
        if (error) {
           this.error = error ;
        } else if (data) {
            this.name = data.fields.Name.value;
        }
    }

    getTimeZoneScreenDetails(){
      getTimezoneDetails({screenName: this.screenName, adfId: this.recordId})
        .then((result) => {
          this.allTimeZoneDetails = result;
          if(this.allTimeZoneDetails){
          this.userTimeZoneVal=this.allTimeZoneDetails[CCL_TEXT_ADF_Approved_Date_Time__c];
          }
          this.getUserTimeZone(this.userTimeZoneVal);
            this.error = null;
        })
        .catch((error) => {
          this.error = error;
        });
    }

    getUserTimeZone(timeZoneVal){
      getUserTimeZoneDetails({timeZoneVal:timeZoneVal})
      .then((result) => {
        this.userTimeZone = result;
        this.getDateTextVal();
      })
      .catch((error) => {
        this.error = error;
      });
    }

    getDateTextVal(){
      if(this.userTimeZone!==undefined && this.userTimeZone!==null && this.userTimeZone!==''){
        let fields=this.userTimeZone.split('T');
        let timeVal=fields[1];
        let dateVal=fields[0];
        let timeValue=timeVal.split(":");
        let textVal=this.formatDate(dateVal)+' '+timeValue[0]+':'+timeValue[1]+' '+this.userTimeZoneVal;
        this.userDateText=textVal;
      }
    }

    formatDate(date) {
      let mydate = new Date(date);
      let monthNames = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
      ];

      let day = mydate.getDate();
      let monthIndex = mydate.getMonth();
      let monthName = monthNames[monthIndex];
      let year = mydate.getFullYear();
      return `${day} ${monthName} ${year}`;
    }

    connectedCallback() {
      registerListener("gotoSummary", this.gotoADFSummary,this);
        let val = JSON.parse(this.jsonData);
        let mytable = val["2"];
        if (mytable) {
          this.collectionList = mytable[1];
          this.index = this.collectionList.length - 1;
          for (let i = 0; i <= this.collectionList.length - 1; ++i) {
            if (this.collectionList[i]["attributes"]) {
                this.prevVal[i]={};
                this.prevVal[i].CCL_DIN__c = this.collectionList[i].CCL_DIN__c;
                this.prevVal[i].CCL_Apheresis_ID__c = this.collectionList[i].CCL_Apheresis_ID__c;
                this.prevVal[i].CCL_SEC__c = this.collectionList[i].CCL_SEC__c;
            }
        }
        }
        if(this.prevVal[0].CCL_DIN__c!=null){
            this.message=CCL_ApproverDinMessage;
            this.idType="CCL_DIN__c";
            this.errorMessage=CCL_ApproverDinError;
        }else if(this.prevVal[0].CCL_SEC__c!=null){
            this.message=CCL_ApproverSecMessage;
            this.idType="CCL_SEC__c";
            this.errorMessage=CCL_ApproverSecError;
        }else{
            this.message=CCL_ApproverMessage;
            this.idType="CCL_Apheresis_ID__c";
            this.errorMessage=CCL_ApproverError;
        }
        this.getTimeZoneScreenDetails();
    }
    handleEnter(event){
        const inputcmp=this.template.querySelector("lightning-input");
        this.myInput=inputcmp.value;
        for(let i=0;i<=this.prevVal.length-1;++i){
            if(this.prevVal[i][this.idType]===this.myInput&&(this.myInput!=null||this.myInput!=undefined)){
                this.navigateNext();
                this.flag=true;
            }
        }
        if(!this.flag){
        inputcmp.setCustomValidity(this.errorMessage);
        inputcmp.reportValidity();}
    }
    navigateNext(){
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
      }

      //2260
   gotoADFSummary(){
    const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
}
}
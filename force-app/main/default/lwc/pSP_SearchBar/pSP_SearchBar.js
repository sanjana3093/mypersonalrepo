import { LightningElement, track, api } from "lwc";
import fetchRecords from "@salesforce/apex/PSP_Search_Component_Controller.fetchRecords";
import searchtext from "@salesforce/label/c.PSP_Search_Product_Service";
import portfolioHeader from "@salesforce/label/c.PSP_Portfolio";
import productHeader from "@salesforce/label/c.PSP_Product_Name";
import serviceName from "@salesforce/label/c.PSP_Service_Name";
import serviceType from "@salesforce/label/c.PSP_Service_Type";
import proSku from "@salesforce/label/c.PSP_SKU";
import saveAll from "@salesforce/label/c.PSP_save_all";
export default class PSP_SearchBar extends LightningElement {
  @track greeting = "";
  @track products = [];
  @track index = 0;
  @track dynamicClass;
  @track proservicedetail = [];
  @api recordId;
  @track iconName = "standard:product";
  @api tabproductservice;
  @api selectedItems = [];
  @track nonselected = true;
  @track selectedItem;
  @track searchtext = searchtext;
  @track servicename=serviceName;
  @track portfolio=portfolioHeader;
  @track productName=productHeader;
  @track sku=proSku;
  @track serviceType=serviceType;
  @track saveAll=saveAll;
  
  @api
  get UiClass() {
    if (this.products.length > 0 && this.greeting && this.nonselected) {
      this.dynamicClass =
        "slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-lookup__menu displayblock";
    } else {
      this.dynamicClass =
        "slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-lookup__menu";
    }
    return this.dynamicClass;
  }
@api
  get ifIsProduct(){
    let rtnVal;
    if(this.tabproductservice==='Product'){
      rtnVal= true;
    }else{
      rtnVal= false;
    }
    return rtnVal;
  }
  changeHandler(event) {
    this.greeting = event.target.value;
    console.log('val'+this.greeting);
    if (this.greeting && this.greeting.length >= 2) {
      this.nonselected = true;
      fetchRecords({
        searchKeyword: this.greeting,
        objParam: this.tabproductservice,
        recordId: this.recordId
      })
        .then(result => {
          this.products = result;
          this.error = null;
        })
        .catch(error => {
          this.error = error;
          this.products = null;
        });
    } else {
      this.nonselected = false;
    }
  }
  handleClickProducts(event) {
    const evt = new CustomEvent("tabname", {
      // detail contains only primitives
      detail: this.tabproductservice
    });
    this.dispatchEvent(evt);
    this.selectedItem = event.target.dataset.item;
    this.nonselected = false;
    let i, j;
    this.greeting = null;
    let selectedBefore = false;
    if (
      this.proservicedetail === undefined ||
      this.proservicedetail === " " ||
      this.proservicedetail === null ||
      this.proservicedetail.length <= 0
    ) {
      this.index = 0;
    }
    for (i = 0; i < this.products.length; i++) {
      if (this.products[i].prodId === this.selectedItem) {
        if (
          this.proservicedetail === undefined ||
          this.proservicedetail === " " ||
          this.proservicedetail === null ||
          this.proservicedetail.length <= 0
        ) {
          this.proservicedetail[0] = this.products[i];
          this.index = this.index + 1;
        } else {
          for (j = 0; j < this.proservicedetail.length; j++) {
            if (this.proservicedetail[j].prodId === this.selectedItem) {
              selectedBefore = true;
            }
          }
          if (!selectedBefore) {
            this.proservicedetail[this.index] = this.products[i];
            this.proservicedetail[this.index].iconName = this.iconName;
            this.index = this.index + 1;
          }
        }
      }
    }
  }

  addClickedItem(event) {
    this.greeting = null;
    this.products = [];
    let index = event.target.dataset.item;
    this.selectedItems = this.proservicedetail[index];
    const evt = new CustomEvent("addproduct", {
      // detail contains only primitives
      detail: this.selectedItems
    });
    this.dispatchEvent(evt);
    this.proservicedetail.splice(index, 1);
    //setting the index value as per removing elements from array
    if (
      this.proservicedetail !== undefined &&
      this.proservicedetail.lenghth > 0 &&
      this.proservicedetail !== " " &&
      this.proservicedetail !== null
    ) {
      this.index = this.proservicedetail.length - 1;
    }
  }
  //method to make dropdown disappear on outside click
  bluredOut(event) {
    this.nonselected = false;
  }
  saveAllRec() {
    //save the whole list
    this.proservicedetail.currentTab = this.tabproductservice; //'Product';
    this.selectedItems = this.proservicedetail;
    const evt = new CustomEvent("addproduct", {
      // detail contains only primitives
      detail: this.selectedItems
    });
    this.dispatchEvent(evt);
    //setting the list as empty after firing an event
    this.proservicedetail = [];
  }
}
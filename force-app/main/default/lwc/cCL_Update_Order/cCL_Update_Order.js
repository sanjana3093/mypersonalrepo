import { LightningElement, api, wire, track } from "lwc";
import { getObjectInfo, getPicklistValues, getPicklistValuesByRecordType } from "lightning/uiObjectInfoApi";
import { CurrentPageReference } from "lightning/navigation";
import Id from '@salesforce/user/Id';
import CCL_OrderingHospital from '@salesforce/label/c.CCL_Ordering_Hospital';
import CCL_TherapyName from '@salesforce/label/c.CCL_Therapy_Name';
import CCL_Patient_Details from "@salesforce/label/c.CCL_Patient_Details";
import CCL_Patient_Details_Header from "@salesforce/label/c.CCL_Patient_Details_Header";
import CCL_Hospital_Patient_ID from "@salesforce/label/c.CCL_Hospital_Patient_ID";
import CCL_Treatment_Protocal_Subject_ID from "@salesforce/label/c.CCL_Treatment_Protocal_Subject_ID";
import CCL_Treatment_Protocal_Subject_ID_Info from "@salesforce/label/c.CCL_Treatment_Protocal_Subject_ID_Info";
import CCL_Protocol_ID from "@salesforce/label/c.CCL_Protocol_ID";
import CCL_4_Digit_Center_Number from "@salesforce/label/c.CCL_4_Digit_Center_Number";
import CCL_4_Digit_Center_Number_Helptext from "@salesforce/label/c.CCL_4_Digit_Center_Number_Helptext";
import CCL_3_Digit_Subject_Number from "@salesforce/label/c.CCL_3_Digit_Subject_Number";
import CCL_3_Digit_Subject_Number_Helptext from "@salesforce/label/c.CCL_3_Digit_Subject_Number_Helptext";
import CCL_Please_fill_Subject_Number from "@salesforce/label/c.CCL_Please_fill_Subject_Number";
import CCL_SubjectNumber_Lengthvalidation from "@salesforce/label/c.CCL_SubjectNumber_Lengthvalidation";
import CCL_Please_enter_numbers_only from "@salesforce/label/c.CCL_Please_enter_numbers_only";
import CCL_Hospital_Patient_ID_Validation from "@salesforce/label/c.CCL_Hospital_Patient_ID_Validation";
import CCL_Pattern_Validation_error_message from "@salesforce/label/c.CCL_Pattern_Validation_error_message";
import CCL_None from "@salesforce/label/c.CCL_None";
import CCL_None_Label from "@salesforce/label/c.CCL_None_Label";
import CCL_Yes from "@salesforce/label/c.CCL_Yes";
import CCL_No from "@salesforce/label/c.CCL_No";
import CCL_Date_Validation_Message from "@salesforce/label/c.CCL_Date_Validation_Message";
import CCL_Date_Placeholder from "@salesforce/label/c.CCL_Date_Placeholder";
import CCL_Month_Validation_Message from "@salesforce/label/c.CCL_Month_Validation_Message";
import CCL_Month_Placeholder from "@salesforce/label/c.CCL_Month_Placeholder";
import CCL_Year_Placeholder from "@salesforce/label/c.CCL_Year_Placeholder";
import CCL_Year_Mismatch_Error_Message from "@salesforce/label/c.CCL_Year_Mismatch_Error_Message";
import CCL_Year_Format_Error_Message from "@salesforce/label/c.CCL_Year_Format_Error_Message";
import CCL_Year_Missing_Message from "@salesforce/label/c.CCL_Year_Missing_Message";
import CCL_Patient_Weight_Error_Message from "@salesforce/label/c.CCL_Patient_Weight_Error_Message";
import CCL_Weight_Unit_In_kg_Placeholder from "@salesforce/label/c.CCL_Weight_Unit_In_kg_Placeholder";
import CCL_Please_Select from "@salesforce/label/c.CCL_Please_Select";
import CCL_dd_Lwc from "@salesforce/label/c.CCL_dd_Lwc";
import CCL_mmm_Lwc from "@salesforce/label/c.CCL_mmm_Lwc";
import CCL_yyyy_Lwc from "@salesforce/label/c.CCL_yyyy_Lwc";
import CCL_Optional_Placeholder_Lwc from "@salesforce/label/c.CCL_Optional_Placeholder_Lwc";
import CCL_PleaseSelect from "@salesforce/label/c.CCL_PleaseSelect";
import CCL_Loading from "@salesforce/label/c.CCL_Loading";
import CCL_First_Name from "@salesforce/label/c.CCL_First_Name";
import CCL_Middle_Name from "@salesforce/label/c.CCL_Middle_Name";
import CCL_Last_Name from "@salesforce/label/c.CCL_Last_Name";
import CCL_Suffix from "@salesforce/label/c.CCL_Suffix";
import CCL_Date_fof_Birth from "@salesforce/label/c.CCL_Date_fof_Birth";
import CCL_Patient_Weight_PRF from "@salesforce/label/c.CCL_Patient_Weight_PRF";
import CCL_Patient_consent from "@salesforce/label/c.CCL_Patient_consent";
import CCL_Prescriber from "@salesforce/label/c.CCL_Prescriber";
import CCL_Principal_Investigator from "@salesforce/label/c.CCL_Principal_Investigator";
import CCL_Order_Details from "@salesforce/label/c.CCL_Order_Details";
import CCL_Treatment_Information from '@salesforce/label/c.CCL_Treatment_Information';
import fetchPrescribers from "@salesforce/apex/CCL_PRF_Controller.fetchPrescribers";
import checkCustOpsPermission from "@salesforce/apex/CCL_PRF_Controller.checkCustOpsPermission";
import CCL_Mandatory_Field from "@salesforce/label/c.CCL_Mandatory_Field";
import CCL_Age_Error from "@salesforce/label/c.CCL_Age_Error";
import CCL_DOB_Error from "@salesforce/label/c.CCL_DOB_Error";
import SUFFIX_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Suffix__c';
import PATIENT_COUNTRY_RESIDENCE_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Patient_Country_Residence__c';
import BILLING_PARTY_FIELD from '@salesforce/schema/CCL_Order__c.CCL_Billing_Party__c';
import CCL_ORDER_OBJ from "@salesforce/schema/CCL_Order__c";
import CCL_Patient_Weight_Threshold from "@salesforce/label/c.CCL_Patient_Weight_Threshold";
import CCL_Hospital_Purchase_Order_Number from '@salesforce/label/c.CCL_Hospital_Purchase_Order_Number';
import CCL_PR_Submission_Text from '@salesforce/label/c.CCL_PR_Submission_Text';
import CCL_Payment_Details from '@salesforce/label/c.CCL_Payment_Details';
import CCL_VA_Purchase_Order from '@salesforce/label/c.CCL_VA_Purchase_Order';
import CCL_Purchase_Order_Error from '@salesforce/label/c.CCL_Purchase_Order_Error';
import CCL_Billing_Party_Label from '@salesforce/label/c.CCL_BillingParty';
import CCL_Country_Of_Residence_Label from '@salesforce/label/c.CCL_PatientCountry';

import editOrderDetails from '@salesforce/apex/CCL_PRF_Controller.editOrderDetails';
import getAphDataFormStatus from '@salesforce/apex/CCL_PRF_Controller.getAphDataFormStatus';
import fetchShipmentForOrderUpdate from '@salesforce/apex/CCL_PRF_Controller.fetchShipmentForOrderUpdate';
import fetchTherapyAssociation from '@salesforce/apex/CCL_PRF_Controller.fetchTherapyAssociation';
import updatePrfForm from '@salesforce/apex/CCL_PRF_Controller.updatePrfForm';
import fetchAphShipmentRecords from '@salesforce/apex/CCL_PRF_Controller.fetchAphShipmentRecords';

import CCL_Patient_Initials from '@salesforce/label/c.CCL_Patient_Initials';
import CCL_Patient_Initials_Err from '@salesforce/label/c.CCL_Patient_Initials_Error';

import getcustomdoc from "@salesforce/apex/CCL_ADFController_Utility.getDocId";
import getFileDetails from "@salesforce/apex/CCL_Utility.getConsentFiles";
import cCL_Patient_Consents from "@salesforce/label/c.cCL_Patient_Consents";

export default class CCLUpdateOrder extends LightningElement {
	label = {
		CCL_OrderingHospital,
		CCL_TherapyName,
		CCL_Treatment_Protocal_Subject_ID,
		CCL_Patient_Details,
		CCL_Hospital_Patient_ID,
		CCL_Patient_Details_Header,
		CCL_Protocol_ID,
    CCL_4_Digit_Center_Number,
    CCL_4_Digit_Center_Number_Helptext,
    CCL_3_Digit_Subject_Number,
    CCL_3_Digit_Subject_Number_Helptext,
		CCL_Treatment_Protocal_Subject_ID_Info,
		CCL_First_Name,
		CCL_Middle_Name,
		CCL_Last_Name,
		CCL_Suffix,
		CCL_Date_fof_Birth,
		CCL_Patient_Weight_PRF,
		CCL_Patient_consent,
		CCL_Prescriber,
		CCL_Principal_Investigator,
		CCL_Mandatory_Field,
		CCL_Patient_Weight_Threshold,
		CCL_Hospital_Purchase_Order_Number,
		CCL_PR_Submission_Text,
		CCL_Payment_Details,
		CCL_VA_Purchase_Order,
    CCL_Purchase_Order_Error,
    CCL_Order_Details,
    CCL_Treatment_Information,
    CCL_Patient_Initials,
    CCL_Billing_Party_Label,
    CCL_Country_Of_Residence_Label,
    CCL_Please_fill_Subject_Number,
    CCL_SubjectNumber_Lengthvalidation,
    CCL_Please_enter_numbers_only,
    CCL_Hospital_Patient_ID_Validation,
    CCL_Pattern_Validation_error_message,
    CCL_None,
    CCL_Date_Validation_Message,
    CCL_Date_Placeholder,
    CCL_Month_Validation_Message,
    CCL_Month_Placeholder,
    CCL_Year_Placeholder,
    CCL_Year_Mismatch_Error_Message,
    CCL_Year_Format_Error_Message,
    CCL_Year_Missing_Message,
    CCL_Patient_Weight_Error_Message,
    CCL_Weight_Unit_In_kg_Placeholder,
    CCL_Please_Select,
    CCL_dd_Lwc,
    CCL_mmm_Lwc,
    CCL_yyyy_Lwc,
    CCL_Optional_Placeholder_Lwc,
    CCL_PleaseSelect,
    CCL_Loading,
    CCL_None_Label,
    CCL_Yes,
    CCL_No, cCL_Patient_Consents
	};
	@api prescriberId;
	@api prescriberName;
	@api hospital; // to capture site sfdcid
	@api therapyId;
	@api hospitalPatientId; //to access in flow
	@api hospPatientoptflow;
	/*@api CCL_centernum;
	@api CCL_subjectnum; */
	@api CCL_firstname;
	@api CCL_middleinitial;
	@api CCL_lastname;
  @api CCL_suffix;
  @api CCL_Patient_Initials;
	@api CCL_patientweight;
	@api CCL_patientconsent;
	@api CCL_inpdate;
	@api CCL_inpmonth;
  @api CCL_inpyear;
	@api maximumAge;
	@api minimumAge;
	@api researchWithBioSample;
	@api CCL_prescriberid_commercial;
	@api contactidforprescriber;
  @api count = 0;
  @api centernum = "";
	@api minimumWeight;
	@api maximumWeight;
	@api weightValidationError = false;
	@api monthName;
  @api isLoadFirst = false;
  @api therapy = "";
  @api patientconsent;
  @api date;
	@api month;
  @api year;
  @api intdate;
	@api intmonth;
  @api intyear;
  @api age_day;
	@api age_month;
	@api CCL_patientage;
  @api var1 = "slds-p-left_none slds-p-right_none";
	@api var2 = "customPadding";
	@api returnToReview;
	@api therapyType;
  @api showHideBackBtn = false;
  @api navigateFurther = false; //function to set the date entered in patient details screen
  @api orderIdToBeUpdate; // Order Id passing from Order detail page
  @api hospitalPurchaseOrderOptInFlow;  // Need to query for Salesforce
  @api veteransFlagFlow; // Need to query for Salesforce
  @api shipmentdata;
  @api orderUpdateDetails=[];
  @api adfStatus=[];
  @api initialAdfApproved;
  @api purchaseorder;
  @api checkb;
  @api countryOfResidenceUserValue;
  @api billingPartyUserValue;
  @api countryOfResidenceUserValueLable;
  @api billingPartyUserValueLable;
  @api hospitalPurchaseOrderText;
  @api isClinical = false;
  @api isCommercial = false;
  @api showSaveChangesBtn = false;
  @api showCancelChangesBtn = false;
  @api showCancelThisOrderBtn = false;
  @api hospitalId;
  @api fieldValidations;
  @api showReadOnly;
  @api isEditablePatient = false;
  @api isReadOnlyPatient = false;
  @api isEditablePayment = false;
  @api isReadOnlyPayment = false;
  @api status;
  @api shipmentStatus;
  @api PrescriberInvestigator;
  @api patientDetail;
  @api paymentDetailAndOtherFields;
  @api orderingTherapyAssociation;
  @api siteTherapyAssociation;
  @api veteransApplicableFlow;
  @api hospitalOptInFlow;
  @api country;
  @api showValidation = false;
  @api finalDate;
  @api finalMonth;
  @api patientSubmitId;
  @api setRadioBtnYes;
  @api cancelOrder;
  @api loaded = false;
  @api fpshipmentId;
  @api apheresisDataFormId;
  @api aphShipmentId;
  @api aphShipmentList;
  @api researchsample; //variables to track the date validation on the patient details screen
  @api valueDate;
  @api protocolNumber;
  @api centerNumber;
  @api orderCenterNumber;
  @api subjectNumber;
  @api hasPrescriber;
  @api hpoReadonly;
  @api billingPartyValue;
  @api billingPartyFlag;
  @api countryOfResidenceValue;
  @api countryOfResidenceFlag;
  @api countryOfResidencePicklistValues;
  @api billingPartyPicklistValues;
  @api variableToDetermineResubmissionFlag=false;
  @api PatientAgeAtOrder;
  @api applicableBillingParties;
  @api flagToCheckIfPrfOrderingStatusWasApprovedOrRejected=false;
  @api prfOrderingStatusInitialValue;
  @api prfOrderingStatusToBeUpdated;

  @api custOpsLogin =false;

  @track dobvalid = false;
  @track dobvalidmonth = false;
  @track dobvalidyear = false;
  @track prescriberList = null;
  @track newArr;
  @track EMPTY_STRING = "";
  @track radioOptions=[
    {'label': this.label.CCL_Yes, 'value': 'Yes'},
    {'label': this.label.CCL_No, 'value': 'No'}
    ];
@track orderDocId;
@track filesUploaded = [];
  patientId;
	subjectnum;
	contactid;
  datenull;
	dateerror;
	leaperror;
	nonleaperror;
	futureerror;
	datestr;
	dateval;
	ageval; //Variables to calculate the age based on the date of birth entered in patient details screen
	tod;
	todyear;
	age;
	todmonth;
  toddate;
  hospPatientIdOptIn = true;
	therapytype = "";
	error;
	gotresult = false;
	message;
	isvalid = false;
	futurevalid = false;
	agevalid = false;
  prescriberError = false;
  errorOnUpdate;
  captureChangeReason = false;
  submitButtonClicked = false;
  changeReasonForDataUpdate='';
  billingPartyPicklist;
 @track showSuffix =false; @track showFirstName = false;  @track showMidName =false; @track showLastName=false;@track showInitials = false;
  @track showDyOfBirth = false; @track showMnthOfBrth = false; @track showYrOfBrth = false; @track showCOIPatientId =false;@track showDOBLabel =false;
	@wire(CurrentPageReference) pageRef;

  dateHandler(event) {
		this.date = event.detail.value;
		this.intdate = parseInt(this.date, 10);
        this.captureChangeReason = true;
	}

	//function to set the month entered in patient details screen
	monthHandler(event) {
    this.month = event.detail.value;
    this.intmonth = parseInt(this.month, 10);
    this.captureChangeReason = true;
  }

  //function to set the year entered in patient details screen
  yearHandler(event) {
		this.year = event.detail.value;
    this.intyear = parseInt(this.year, 10);
    this.nonleaperror = '';
    this.futureerror ="";
    this.captureChangeReason = true;
	}

	renderedCallback() {
    //this.displaydiv('Render @@@@@'+ this.therapyType);
     if(this.isLoadFirst)
     {
      if(this.checkb && this.veteransApplicableFlow && this.isEditablePayment){
        this.template.querySelector('.chk').checked=true;

      }
      this.isLoadFirst=false;
     }
	 if(this.hasPrescriber){
      const prescriber = this.template.querySelector(".prescribercls");
      if ((this.prescriberName != null) && (this.therapyType=='Commercial') && prescriber != null) {
        prescriber.value=this.prescriberName;
      }
      const investigator = this.template.querySelector(".investigatorCls");

      if ((this.prescriberName != null) && (this.therapyType=='Clinical') && investigator != null) {

        investigator.value=this.prescriberName;
        
      }
      if(this.billingPartyFlag && this.billingPartyPicklist && !this.billingPartyPicklistValues) {
        this.initializeBillingPartyValues();
    }
    }
 }

  //function to capture the values from the previous screens
	connectedCallback() {
		 getcustomdoc({recordId : this.orderIdToBeUpdate})
          .then(result =>{
      this.orderDocId = result[0].Id;
      getFileDetails({recordId:this.orderDocId})
      .then(result=>{
    this.filesUploaded = result;
    if (this.filesUploaded.length > 0) {
    }
    
      }).catch (error=> {
      })
          }).catch(error => {
            this.error = error;
          })
    this.showCancelChangesBtn = true;
    this.showCancelThisOrderBtn = true;
    this.showSaveChangesBtn = true;
    this.variableToDetermineResubmissionFlag=false;
    this.flagToCheckIfPrfOrderingStatusWasApprovedOrRejected=false;
    this.getOrderData();
    checkCustOpsPermission()
      .then(result => {
        this.custOpsLogin = result;
        if (this.custOpsLogin) {
          this.showSaveChangesBtn = false;
        }
      })
      .catch(error => {
        this.error = error;
      });
  }

  getOrderData()
  {
    editOrderDetails({ orderId: this.orderIdToBeUpdate})
    .then(result => {
      this.orderUpdateDetails = result;
      this.hospital = this.orderUpdateDetails[0].CCL_Ordering_Hospital__r.Name;
      this.therapy = this.orderUpdateDetails[0].CCL_Therapy__r.CCL_Therapy_Description__c;
      this.country = this.orderUpdateDetails[0].CCL_Ordering_Hospital__r.ShippingCountryCode;
      this.hospitalId = this.orderUpdateDetails[0].CCL_Ordering_Hospital__c;
      this.therapyId = this.orderUpdateDetails[0].CCL_Therapy__c;
      this.CCL_firstname= this.orderUpdateDetails[0].CCL_Patient__r.FirstName;
      this.CCL_middleinitial= this.orderUpdateDetails[0].CCL_Patient__r.MiddleName;
      this.CCL_lastname= this.orderUpdateDetails[0].CCL_Patient__r.LastName;
      this.minimumWeight=this.orderUpdateDetails[0].CCL_Therapy__r.CCL_Minimum_Weight__c;
      this.maximumWeight=this.orderUpdateDetails[0].CCL_Therapy__r.CCL_Maximum_Weight__c;
      this.patientconsent=this.orderUpdateDetails[0].CCL_Consent_for_Additional_Research__c;
      this.patientSubmitId=this.orderUpdateDetails[0].CCL_Patient__c;
      this.CCL_patientweight= this.orderUpdateDetails[0].CCL_Patient_Weight__c.toString();
      this.therapyType=this.orderUpdateDetails[0].CCL_Therapy__r.CCL_Type__c;
      this.hospitalPatientId=this.orderUpdateDetails[0].CCL_Patient__r.CCL_Hospital_Patient_ID__pc;
      this.checkb = this.orderUpdateDetails[0].CCL_VA_Patient__c;
      this.CCL_suffix = this.orderUpdateDetails[0].CCL_Patient__r.Suffix;
      this.prescriberId = this.orderUpdateDetails[0].CCL_Prescriber__c;
      this.prescriberName = this.orderUpdateDetails[0].CCL_Prescriber__r.Name;
      this.protocolNumber = this.orderUpdateDetails[0].CCL_Therapy__r.Name;
      this.orderCenterNumber = this.orderUpdateDetails[0].CCL_Protocol_Center_Number__c;
      this.subjectNumber = this.orderUpdateDetails[0].CCL_Protocol_Subject_Number__c;
      this.changeReasonForDataUpdate = this.orderUpdateDetails[0].CCL_Latest_Change_Reason__c;
      this.initialAdfApproved = this.orderUpdateDetails[0].CCL_Initial_ADF_Approved__c;
      this.countryOfResidenceUserValue=this.orderUpdateDetails[0].CCL_Patient_Country_Residence__c;
      this.billingPartyUserValue=this.orderUpdateDetails[0].CCL_Billing_Party__c;
      this.PatientAgeAtOrder=this.orderUpdateDetails[0].CCL_Patient_Age_At_Order__c;
      this.prfOrderingStatusInitialValue=this.orderUpdateDetails[0].CCL_PRF_Ordering_Status__c;

      this.getTreatmentCenterNumber();
      // Setting Date
      let initialDate = this.orderUpdateDetails[0].CCL_Patient__r.CCL_Date_of_DOB__pc;
      this.CCL_inpdate = initialDate;
      const formatedDate = initialDate ? ("0" + (initialDate)).slice(-2) : initialDate;
      this.date = formatedDate;
      this.intdate = formatedDate; // for future date
      this.age_day = this.date;

      // Setting Year
      this.year= this.orderUpdateDetails[0].CCL_Patient__r.CCL_Year_of_DOB__pc;
      this.intyear = this.year ? parseInt(this.year, 10) : this.year; // for future date

      // Setting Month
      const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
      this.CCL_inpmonth = this.orderUpdateDetails[0].CCL_Patient__r.CCL_Month_of_DOB__pc;
      let monthIndex = monthNames.indexOf(this.orderUpdateDetails[0].CCL_Patient__r.CCL_Month_of_DOB__pc);
	  
	  let formattedMonth;
      if(monthIndex==0)
      {
        formattedMonth ='00';
      }
      else
      {
        formattedMonth = monthIndex ? ("0" + (monthIndex)).slice(-2) : monthIndex;
      }
      // to set html part on load
      this.month = formattedMonth;
      this.intmonth = monthIndex; // for future date
      this.age= this.CCL_patientage;
      this.age_month = formattedMonth;


      /*Logic to show COI fields dynamiclly based on the setting at time of order creation (4.2)  */
      this.CCL_Patient_Initials = this.orderUpdateDetails[0].CCL_Patient_Initials__c;
      this.showFirstName = this.orderUpdateDetails[0].CCL_FirstName_COI__c;
      this.showMidName =this.orderUpdateDetails[0].CCL_MiddleName_COI__c	;
      this.showLastName = this.orderUpdateDetails[0].CCL_LastName_COI__c;
      this.showSuffix = this.orderUpdateDetails[0].CCL_Sufix_COI__c;
      this.showInitials = this.orderUpdateDetails[0].CCL_Initials_COI__c;
      this.showDyOfBirth  =this.orderUpdateDetails[0].CCL_Day_of_Birth_COI__c;
      this.showMnthOfBrth = this.orderUpdateDetails[0].CCL_Month_of_Birth_COI__c;
     this.showYrOfBrth = this.orderUpdateDetails[0].CCL_Year_of_Birth_COI__c;
      if(this.showDyOfBirth || this.showMnthOfBrth || this.showYrOfBrth) {
        this.showDOBLabel =true;
      }

      this.getShipmentDetails();
      this.initializeCountryBillPartyLabel();
 })
    .catch(error => {
      this.error = error;
    });
  }

//Fetch Shipment Records
getShipmentDetails()
{

  fetchAphShipmentRecords({ orderId: this.orderIdToBeUpdate})
  .then(result => {
    this.aphShipmentList = result;
    this.aphShipmentId=this.aphShipmentList[0].Id;
   })
  .catch(error => {
    this.error = error;
  });

  fetchShipmentForOrderUpdate({ orderId: this.orderIdToBeUpdate})
  .then(result => {
    this.shipmentdata = result;
    this.fpshipmentId=this.shipmentdata[0].Id;
    this.ShipmentReason = this.shipmentdata[0].CCL_Shipment_Reason__c;
    this.shipmentStatus = this.shipmentdata[0].CCL_Status__c;
    if (this.ShipmentReason === 'AD' || this.ShipmentReason ==undefined) {
     this.isReadOnlyPayment = true;
     this.isEditablePayment = false;
     //  this.purchaseorder = '';
     if (this.shipmentdata[0].CCL_Hospital_Purchase_Order_Number__c !== undefined) {
      this.purchaseorder =  this.shipmentdata[0].CCL_Hospital_Purchase_Order_Number__c;
    } else if (this.shipmentdata[0].CCL_Hospital_Purchase_Order_Number__c == undefined) {
      this.purchaseorder = '';
    }

     }
     else if((this.ShipmentReason == 'RE' || this.ShipmentReason == 'IN' ) && (this.shipmentStatus !== 'Product Shipment Planned'&& this.shipmentStatus !== undefined))
     {
      this.isReadOnlyPayment = true;
      this.isEditablePayment = false;
      this.hpoReadonly = true;
      if (this.shipmentdata[0].CCL_Hospital_Purchase_Order_Number__c !== undefined) {
        this.purchaseorder =  this.shipmentdata[0].CCL_Hospital_Purchase_Order_Number__c;
      } else if (this.shipmentdata[0].CCL_Hospital_Purchase_Order_Number__c == undefined) {
        this.purchaseorder = '';
      }
     }
     else if ((this.ShipmentReason == 'RE' || this.ShipmentReason == 'IN') && (this.shipmentStatus == 'Product Shipment Planned'|| this.shipmentStatus == undefined)) {
      this.isReadOnlyPayment = false;
        this.isEditablePayment = true;
        if (this.shipmentdata[0].CCL_Hospital_Purchase_Order_Number__c !== undefined) {
            this.purchaseorder =  this.shipmentdata[0].CCL_Hospital_Purchase_Order_Number__c;
          } else if (this.shipmentdata[0].CCL_Hospital_Purchase_Order_Number__c == undefined) {
            this.purchaseorder = '';
          }
     }
     this.getADFDetails();
    })
  .catch(error => {
    this.error = error;
    this.isEditablePayment = false;
    this.isReadOnlyPayment = true;
  });


}

getADFDetails()
{
  //Method to fetch adf status for threshold
  getAphDataFormStatus({ orderId: this.orderIdToBeUpdate})
  .then(result => {
    this.adfStatus = result;
    this.status = this.adfStatus[0].CCL_Status__c;
    this.apheresisDataFormId = this.adfStatus[0].Id;

    if(!(this.initialAdfApproved) && (
      this.status == 'Awaiting ADF' ||
      this.status == 'ADF Pending Submission' ||
      this.status == 'ADF Pending Approval' ||
      this.status == 'ADF Rejected' ||
      this.status == null)) {
      
      this.isEditablePatient = true;
      this.isReadOnlyPatient = false;
    
    } else {
      this.isEditablePatient = false;
      this.isReadOnlyPatient = true;
    }
    this.displaydiv(this.therapyType);
    this.getTherapyAssociation();
  })
  .catch(error => {
    this.error = error;
  });
}  

//fetch treatment center number directly from MD
getTreatmentCenterNumber() {fetchTherapyAssociation({ orderingHospital: this.hospitalId, therapy: this.therapyId,countryCode: this.country})
.then(result => {
  this.siteTherapyAssociation = result;
  this.centerNumber = this.siteTherapyAssociation[0].CCL_Treatment_Protocol_Center_Number__c;
  if(this.orderCenterNumber != this.centerNumber ) {
      this.captureChangeReason = true;
  }
})
.catch(error => {
  this.error = error;
});

}

//fetch therapy association

getTherapyAssociation()
{
	fetchTherapyAssociation({ orderingHospital: this.hospitalId, therapy: this.therapyId,countryCode: this.country})
    .then(result => {
      this.orderingTherapyAssociation = result;
      this.hospPatientoptflow =  this.orderingTherapyAssociation[0].CCL_Hospital_Patient_ID_Opt_In__c;
      this.hospitalPurchaseOrderOptInFlow =  this.orderingTherapyAssociation[0].CCL_Hosp_Purchase_Order_Opt_In__c;
      this.researchWithBioSample =  this.orderingTherapyAssociation[0].CCL_Research_with_Bio_Samples_Opt_In__c;
      this.minimumAge =  this.orderingTherapyAssociation[0].CCL_Minimum_Age__c;
      this.maximumAge =  this.orderingTherapyAssociation[0].CCL_Maximum_Age__c;
      this.veteransFlagFlow =  this.orderingTherapyAssociation[0].CCL_Veteran_Affairs_Applicable__c;
      this.countryOfResidenceValue=this.orderingTherapyAssociation[0].CCL_Capture_Patient_Country_of_Residence__c;
      this.billingPartyValue=this.orderingTherapyAssociation[0].CCL_Billing_Party_Visible__c;
      this.hospitalPurchaseOrderText = this.orderingTherapyAssociation[0].CCL_Payment_Section_Text__c;
      this.applicableBillingParties = this.orderingTherapyAssociation[0].CCL_Applicable_Billing_Parties__c;
      // Added for Payment screen
      if(this.hospitalPurchaseOrderOptInFlow=='Yes')
      {
        this.hospitalOptInFlow=true;
      }
      else if(this.hospitalPurchaseOrderOptInFlow=='No')
      {
        this.hospitalOptInFlow=false;
      }
      if(this.veteransFlagFlow=='Yes')
      {
        this.veteransApplicableFlow=true;
      }
      else if(this.veteransFlagFlow=='No')
      {
        this.veteransApplicableFlow=false;
      }
      //Changes made as part of addition of 2 new payment section fields:Billing Party & Country_Of_Residence.

      if(this.countryOfResidenceValue=='Yes')
      {
        this.countryOfResidenceFlag=true;
      }
      else if(this.countryOfResidenceValue=='No')
      {
        this.countryOfResidenceFlag=false;
      }
      if(this.billingPartyValue=='Yes')
      {
        this.billingPartyFlag=true;
      }
      else if(this.billingPartyValue=='No')
      {
        this.billingPartyFlag=false;
      }
      //ENDS: Changes made as part of addition of 2 new payment section fields:Billing Party & Country_Of_Residence.

      if (this.researchWithBioSample === "Yes") {
        this.researchsample = true;
      } else if (this.researchWithBioSample === "No") {
        this.researchsample = false;
      }
      if (this.patientconsent == true) {
        this.patientconsent = 'true';
        this.setRadioBtnYes = 'Yes';
      } else if (this.patientconsent == false){
        this.patientconsent = 'false';
        this.setRadioBtnYes = 'No';
      }
      this.isLoadFirst=true;
  })
    .catch(error => {
      this.error = error;
    });

}


	//function to fetch picklist values
	@wire(getObjectInfo, {
		objectApiName: CCL_ORDER_OBJ
	})
	objectInfo;

	@wire(getPicklistValues, {
		recordTypeId: "$objectInfo.data.defaultRecordTypeId",
		fieldApiName: SUFFIX_FIELD
	})
	systemUsedPicklistValues(result) {
		if (result.data) {
			this.newArr = [{label:this.label.CCL_None, value:''}];
			this.newArr = [...this.newArr,...result.data.values];
		}
  }
  @wire(getPicklistValuesByRecordType, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', objectApiName: CCL_ORDER_OBJ })
    picklistValuesList({data}) {
        if(data) {
            this.countryOfResidencePicklistValues = data.picklistFieldValues[PATIENT_COUNTRY_RESIDENCE_FIELD.fieldApiName].values;
            this.countryOfResidencePicklistValues = [{label:this.label.CCL_None_Label, value:'none'}, ...this.countryOfResidencePicklistValues];
            this.billingPartyPicklist = data.picklistFieldValues[BILLING_PARTY_FIELD.fieldApiName].values;
            this.initializeCountryBillPartyLabel();

        }
    }
/*Introduced in CT4.2 if Year is not selected as COI field */
noYearValidation() {
  this.errorOnUpdate = false;
if ((this.intmonth == 3 ||
this.intmonth == 5 ||
this.intmonth == 8 ||
this.intmonth == 10) &&
this.intdate == 31 && this.showMnthOfBrth && this.showDyOfBirth && !this.showYrOfBrth
) {
this.dateerror = CCL_DOB_Error;
this.dobvalid = true;
this.ageval = "";
this.dobvalidmonth = false;
}else if(this.intmonth === 1 && this.intdate>29 && this.showMnthOfBrth && this.showDyOfBirth && !this.showYrOfBrt) {
this.dateerror = CCL_DOB_Error;
this.dobvalid = true;
this.ageval = "";
this.dobvalidmonth = false;
} else {
this.dateerror = "";
this.leaperror = "";
this.nonleaperror = "";
this.dobvalid = false;
this.dobvalidmonth = false;
this.dobvalidyear = false;
}
}
	//function to validate the date of birth entered in patient details screen
	validateForm() {
    this.errorOnUpdate = false;
    if ((this.intmonth == 3 ||
		this.intmonth == 5 ||
		this.intmonth == 8 ||
		this.intmonth == 10) &&
		this.intdate == 31 && this.showMnthOfBrth && this.showDyOfBirth
		) {
      this.dateerror = CCL_DOB_Error;
			this.dobvalid = true;
			this.ageval = "";
			this.dobvalidmonth = false;
			this.dobvalidyear = false;
		} else if (
			this.intmonth === 1 &&
			this.intdate > 29 &&
			this.intyear % 4 === 0 &&  this.showMnthOfBrth && this.showDyOfBirth && this.showYrOfBrth
		) {
      this.leaperror = CCL_DOB_Error;
      this.dobvalidmonth = true;
			this.dobvalid = false;
			this.ageval = "";
			this.dobvalidyear = false;
		} else if (
			this.intmonth === 1 &&
			this.intdate > 28 &&
			this.intyear % 4 !== 0 &&  this.showMnthOfBrth && this.showDyOfBirth && this.showYrOfBrth
		) {
      this.nonleaperror = CCL_DOB_Error;
      this.dobvalidyear = true;
			this.ageval = "";
			this.dobvalid = false;
			this.dobvalidmonth = false;
		} else {
			this.dateerror = "";
			this.leaperror = "";
			this.nonleaperror = "";
			this.dobvalid = false;
			this.dobvalidmonth = false;
      this.dobvalidyear = false;
      this.calculateage();
		}
	} //function to calculate the age based on the date of birth entered in patient details screen

	calculateage() {
		this.tod = new Date(Date.now());
		this.todyear = this.tod.getFullYear();
		this.age = parseInt(this.todyear - this.year, 10);
		this.todmonth = this.tod.getMonth();
		this.toddate = this.tod.getDate();
		this.age_month = this.todmonth - this.month;
		this.age_day = this.toddate - this.date;
		if (this.age_month < 0 || (this.age_month == 0 && this.age_day < 0)) {
			this.age = parseInt(this.age, 10) - 1;
			this.CCL_patientage = this.age;
			this.validateage();
		} else {
			this.age = parseInt(this.todyear - this.year, 10);
			this.CCL_patientage = this.age;
			this.validateage();
		}
	}

	//function to validate age of the patient at the time of order creation Vs. Therapy preferred limit
	validateage() {
    if(this.valueDate && this.valueDate.checkValidity()) {
      if (this.age > this.minimumAge && this.age < this.maximumAge) {
        this.ageval = "";
        this.agevalid = false;
        this.futuredate();
      } else if (isNaN(this.age)) {
        this.ageval = "";
        this.agevalid = true;
        this.futuredate();
      } else if (this.age < this.minimumAge || this.age > this.maximumAge) {
        this.ageval = CCL_Age_Error;
        this.agevalid = true;
        this.futuredate();
      }
    }
	}

	//function to display error for date entered in future
	futuredate() {
    this.datestr = new Date(this.intyear, this.intmonth, this.intdate);
    //let value = this.template.querySelector(".dateyear");
    if (this.datestr > this.tod) {
        this.futureerror = CCL_DOB_Error;
        this.futurevalid = true;
        this.ageval = "";
      } else {
        this.futureerror = "";
        this.futurevalid = false;
      }
    }
  get options() {
    return [
      { label: this.label.CCL_Yes, value: "true" },
      { label: this.label.CCL_No, value: "false" }
    ];
  }
  get salutationoptions() {
    return [
      { label: "None", value: "" },
      { label: "Sr.", value: "Sr." },
      { label: "Jr.", value: "Jr." }
    ];
  }
  get monthoptions() {
    return [
      { label: "Jan", value: "00" },
      { label: "Feb", value: "01" },
      { label: "Mar", value: "02" },
      { label: "Apr", value: "03" },
      { label: "May", value: "04" },
      { label: "Jun", value: "05" },
      { label: "Jul", value: "06" },
      { label: "Aug", value: "07" },
      { label: "Sep", value: "08" },
      { label: "Oct", value: "09" },
      { label: "Nov", value: "10" },
      { label: "Dec", value: "11" }
    ];
  }

  get dateoptions() {
    return [
      { label: "01", value: "01" },
      { label: "02", value: "02" },
      { label: "03", value: "03" },
      { label: "04", value: "04" },
      { label: "05", value: "05" },
      { label: "06", value: "06" },
      { label: "07", value: "07" },
      { label: "08", value: "08" },
      { label: "09", value: "09" },
      { label: "10", value: "10" },
      { label: "11", value: "11" },
      { label: "12", value: "12" },
      { label: "13", value: "13" },
      { label: "14", value: "14" },
      { label: "15", value: "15" },
      { label: "16", value: "16" },
      { label: "17", value: "17" },
      { label: "18", value: "18" },
      { label: "19", value: "19" },
      { label: "20", value: "20" },
      { label: "21", value: "21" },
      { label: "22", value: "22" },
      { label: "23", value: "23" },
      { label: "24", value: "24" },
      { label: "25", value: "25" },
      { label: "26", value: "26" },
      { label: "27", value: "27" },
      { label: "28", value: "28" },
      { label: "29", value: "29" },
      { label: "30", value: "30" },
      { label: "31", value: "31" }
    ];
  }

	//function to handle the inputs/outputs to flow capture them in the review page
	handleChange(event) {
		this.value = event.detail.value;
		if (event.target.dataset.id === "PatientId") {
			this.hospitalPatientId = event.target.value;
            this.captureChangeReason = true;
		}
    /*if (event.target.dataset.id === "centernumField") {
			this.CCL_centernum = event.target.value;
		}
		if (event.target.dataset.id === "subjectnumid") {
			this.CCL_subjectnum = event.target.value;
		}*/
		if (event.target.dataset.id === "firstnameid") {
			this.CCL_firstname = event.target.value;
            this.captureChangeReason = true;
		}
		if (event.target.dataset.id === "middleinitialid") {
			this.CCL_middleinitial = event.target.value;
            this.captureChangeReason = true;
		}
		if (event.target.dataset.id === "lastnameid") {
			this.CCL_lastname = event.target.value;
            this.captureChangeReason = true;
		}
		if (event.target.dataset.id === "suffixid") {
			this.CCL_suffix = event.target.value;
            this.captureChangeReason = true;
    }
    if (event.target.dataset.id === "patientinit") {
			this.CCL_Patient_Initials = event.target.value;
            this.captureChangeReason = true;
		}
		if (event.target.dataset.id === "patientweightid") {
      this.CCL_patientweight = event.target.value;
      this.weightValidationError = false;
			this.template
				.querySelector(".validateweight")
				.classList.remove("weightInput");
            this.captureChangeReason = true;
		}
		if (event.target.dataset.id === "patientconsentid") {
			this.patientconsent = event.target.value;
            this.captureChangeReason = true;
    }
    // if (event.target.dataset.id === "centerNumberId") {
		// 	this.centerNumber = event.target.value;
    //         this.captureChangeReason = true;
		// }
		if (event.target.dataset.id === "subjectNumberId") {
			this.subjectNumber = event.target.value;
            this.captureChangeReason = true;
		}
  }

  onChangePaymentDetails(event){
    if(event.target.type== 'text'){
      this[event.target.name]=event.target.value;
      this.purchaseorder = event.target.value;
    }
    if (event.target.type == 'checkbox') {
      this.checkb = event.target.checked;
    }
  }
  onChangeCountryOfResidenceValue(event)
  {
    this.countryOfResidenceUserValue=event.detail.value;

  }
  onChangeBillingPartyValue(event)
  {
    this.billingPartyUserValue=event.detail.value;

  }
  initializeCountryBillPartyLabel() {
    if(this.countryOfResidencePicklistValues && this.countryOfResidenceUserValue && !this.countryOfResidenceUserValueLable) {
      this.countryOfResidencePicklistValues.forEach(ele => {
        if(ele.value == this.countryOfResidenceUserValue){
            this.countryOfResidenceUserValueLable = ele.label;
        }
    });
    }
    const billingPartyList = this.isReadOnlyPayment?this.billingPartyPicklist:this.billingPartyPicklistValues;
    if(billingPartyList && this.billingPartyUserValue && !this.billingPartyUserValueLable) {
      billingPartyList.forEach(ele => {
        if(ele.value == this.billingPartyUserValue){
            this.billingPartyUserValueLable = ele.label;
        }
    });
    }
  }
  initializeBillingPartyValues() {
    this.billingPartyPicklistValues = [];

    if(this.applicableBillingParties) {
        let billingPartyArray = this.applicableBillingParties.split(';');

        if(billingPartyArray.length) {
            this.billingPartyPicklistValues.push({label:this.label.CCL_None_Label, value:'none'});

            this.billingPartyPicklist.forEach(ele => {
                if(billingPartyArray.includes(ele.value)) {
                    this.billingPartyPicklistValues.push(ele);
                }
            })
            this.initializeCountryBillPartyLabel();
        }
    }
  }

	onUpdateOrder(event) {
    this.submitButtonClicked = false;
    if(!this.purchaseorder && !this.checkb){
      this.showValidation = false;
    }
    else if(this.purchaseorder && this.checkb){
      this.showValidation = true;
    }
    else if(this.purchaseorder || this.checkb){
      this.showValidation = false;
    }
    if(this.isEditablePatient) {  // All condition are for patient only
      this.valueDate = this.template.querySelector(".dateyear");
      this.handleValidation();
      if(this.valueDate && this.valueDate.checkValidity()) {
        this.validateForm();
      } else if(!this.valueDate) {
        this.noYearValidation();
      }
      this.handleCustomValidationWeight();
    }

    if (
      this.navigateFurther &&
      this.isvalid &&
      !this.dobvalid &&
      !this.agevalid &&
      !this.futurevalid &&
      !this.dobvalidmonth &&
      !this.dobvalidyear &&
      !this.showValidation
    ) {
      this.submitButtonClicked = true;
    } else if (!this.showValidation && this.therapyType === "Commercial" && this.isEditablePayment && this.isReadOnlyPatient) {
      this.submitButtonClicked = true;
    }
	this.checkforchangereason();
  }

  cancelThisOrder(event) {
    this.cancelOrder = false
    const closePopup = new CustomEvent("closepopup", {
      detail: this.cancelOrder
    });
    this.dispatchEvent(closePopup);
  }

  displaydiv(therapyname) {
    this.therapyType = therapyname;
		if (this.therapyType === "Commercial") {
      this.isCommercial = true;
      if (!this.isEditablePatient && !this.isEditablePayment) {
        this.showSaveChangesBtn = false;
      } else if (this.isEditablePatient || this.isEditablePayment) {
        this.showSaveChangesBtn = true;
      }
		} else if (this.therapyType === "Clinical"){
      this.isClinical = true;
      if (!this.isEditablePatient) {
        this.showSaveChangesBtn = false;
      } else if (this.isEditablePatient) {
        this.showSaveChangesBtn = true;
      }
    }
    if (this.custOpsLogin) {
      this.showSaveChangesBtn = false;
    }
	}

	handleValidation() {
    let inputValid = false;
    this.fieldValidations = this.template.querySelectorAll("lightning-input");
		this.count = 0;
		for (let i = 0; i < this.fieldValidations.length; i++) {
			this.fieldNullCheck(this.fieldValidations[i], CCL_Mandatory_Field);
			if (this.fieldValidations[i].checkValidity()) {
				this.count = this.count + 1;
      }
      /*Added Patient Initial Validation as part of 2077*/
      if(this.fieldValidations[i].name === 'Initial' && this.fieldValidations[i].value.length>0 && this.fieldValidations[i].value.length <2) {
        this.fieldValidations[i].setCustomValidity(CCL_Patient_Initials_Err);
        this.fieldValidations[i].reportValidity();
        this.count = this.count - 1;
      }
		}
		if (this.count == this.fieldValidations.length) {
			this.navigateFurther = true;
			inputValid = true;
		} else {
			this.navigateFurther = false;
		}

		this.fieldValid = this.template.querySelectorAll("lightning-combobox");
		this.count = 0;
		for (let j = 0; j < this.fieldValid.length; j++) {
			this.fieldNullCheck(this.fieldValid[j], CCL_Mandatory_Field);
			if (this.fieldValid[j].checkValidity()) {
				this.count = this.count + 1;
			}
		}

		if (this.count == this.fieldValid.length && inputValid) {
			this.navigateFurther = true;
			this.validateForm();
		} else {
			this.navigateFurther = false;
		}

		this.fieldVal = this.template.querySelectorAll("lightning-radio-group");
		this.count = 0;
		for (let j = 0; j < this.fieldVal.length; j++) {
			this.fieldNullCheck(this.fieldVal[j], CCL_Mandatory_Field);
			if (this.fieldVal[j].checkValidity()) {
				this.count = this.count + 1;
			}
		}

		if (this.count == this.fieldVal.length && inputValid) {
			this.navigateFurther = true;
			this.validateForm();
		} else {
			this.navigateFurther = false;
		}
	}

	fieldNullCheck(comboCmp, errorMsg) {
		const value = comboCmp.value;
		const fieldName = comboCmp.name;
		if (
			(value === this.EMPTY_STRING || value == null) &&
			fieldName !== "Midlle Initial" &&
			fieldName !== "progress" && fieldName !== "vetaranCheckbox" && fieldName !== "hospitalPurchaseOrder" && fieldName !== "CountryOfResidence" && fieldName !== "BillingParty"
		) {
			comboCmp.setCustomValidity(errorMsg);
			comboCmp.reportValidity();
			this.navigateFurther = false;
			this.isvalid = false;
		} else {
			comboCmp.setCustomValidity(this.EMPTY_STRING);
			comboCmp.reportValidity();
			this.isvalid = true;
		}
	}

	handleMessage(myMessage) {
		this.message = myMessage;
	}

  @wire(fetchPrescribers, {
		hospitalId: "$hospitalId",
		therapyId: "$therapyId"
	})
	wiredSteps({
		error,
		data
	}) {
		if (data) {
			this.prescriberList = data;
			this.error = null;
      let self = this;
      this.prescriberList.forEach(function (node) {
          if (node.Id == self.prescriberId) {
          self.prescriberName = node.Name;
		  self.hasPrescriber = true;
        }
      });
    } else if (error) {
			this.error = error;
			this.prescriberList = null;
		}
	}

	handlePrescriber(event) {
		let self = this;
		const selectedOption = event.target.value;
		if (this.selectedOption == "" || this.selectedOption == null) {
			this.CCL_prescriberid_commercial = undefined;
		}

		this.prescriberList.forEach(function (node) {
			if (node.Name == selectedOption) {
				self.CCL_prescriberid_commercial = node.Name;
        self.contactid = node.Id;
        self.prescriberId = node.Id;
        self.contactidforprescriber = self.contactid;
        self.prescriberName = node.Name;
        self.captureChangeReason = true;
			}
		});
	}
	//Added as part of user story CGTU-1064-Validate patient's weight on PRF
	handleCustomValidationWeight() {
    if ((this.CCL_patientweight !== undefined) && (this.minimumWeight !== undefined) && (this.maximumWeight !== undefined)) {
      let value = this.template.querySelector(".validateweight");
      let weight = parseFloat(this.CCL_patientweight, 1);
      if ((value.checkValidity()) && ((weight < this.minimumWeight) || (weight > this.maximumWeight))) {
				this.template.querySelector(".validateweight").classList.add("weightInput");
				this.weightValidationError = true;
				this.navigateFurther = false;
			}
		} else {
			this.weightValidationError = false;
			this.template
				.querySelector(".validateweight")
				.classList.remove("weightInput");
		}
  }
  //Method to check for resubmission process

  checkForResubmissionAndPopulateTheResubmissionVariable()
  {
  
    let resubmitFlag=false;
    this.variableToDetermineResubmissionFlag=false;
    

    const oldOrderMap = new Map(Object.entries(this.orderUpdateDetails[0]));
 
    for(const [key, value] of Object.entries(this.patientDetail)) {
      let oldValue = oldOrderMap.get(key);
      if(key=='CCL_Patient_Weight__c' || key =='CCL_Consent_for_Additional_Research__c')
        {
          oldValue = oldOrderMap.get(key).toString();
        }
        if(oldValue !=value && value)
        {
          resubmitFlag=true;
          break;
        }
    }

    
    this.variableToDetermineResubmissionFlag=resubmitFlag;
    if(resubmitFlag==false && (this.prfOrderingStatusInitialValue=='PRF_Rejected' || this.prfOrderingStatusInitialValue=='PRF_Approved') )
    {
      this.prfOrderingStatusToBeUpdated=this.prfOrderingStatusInitialValue;
    }
    else
    {
      this.prfOrderingStatusToBeUpdated="PRF_Submitted";
    }
    



  }

  updatePatientDetails()
  {
    let intmonth1 = this.month ? parseInt(this.month, 10) : this.month;
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    this.finalMonth = monthNames[intmonth1];

    this.finalDate = this.date ? parseInt(this.date, 10) : this.date;

    this.patientDetail=
    {
      "CCL_First_Name__c":this.CCL_firstname,
      "CCL_Last_Name__c":this.CCL_lastname,
      "CCL_Middle_Name__c":this.CCL_middleinitial,
      "CCL_Suffix__c":this.CCL_suffix,
      "CCL_Patient_Initials__c":this.CCL_Patient_Initials,
      "CCL_Ordering_Hospital__c":this.hospitalId,
      "CCL_Therapy__c":this.therapyId,
      "CCL_Date_of_DOB__c":this.finalDate,
      "CCL_Month_of_DOB__c":this.finalMonth,
      "CCL_Year_of_DOB__c":this.year,
      "CCL_Hospital_Patient_ID__c":this.hospitalPatientId,
      "CCL_Patient_Weight__c":this.CCL_patientweight,
      "CCL_Patient_Age_At_Order__c":this.CCL_patientage,
      "CCL_Consent_for_Additional_Research__c":this.patientconsent,
      "CCL_Prescriber__c":this.prescriberId,
      "CCL_Prescriber_Text__c":this.prescriberName,
      "CCL_Patient__c" :this.patientSubmitId,
      "CCL_Protocol_Center_Number__c": this.centerNumber,
      "CCL_Protocol_Subject_Number__c": this.subjectNumber,
      "CCL_Latest_Change_Reason__c": this.changeReasonForDataUpdate

}
   this.checkForResubmissionAndPopulateTheResubmissionVariable();
   
   let countPatient = this.countryOfResidenceUserValue;
   let billParty = this.billingPartyUserValue;

  if(this.countryOfResidenceUserValue == 'none' || !this.countryOfResidenceUserValue) {
    countPatient = '';
  }
  if(this.billingPartyUserValue == 'none' || !this.billingPartyUserValue) {
    billParty = '';
  }
   this.paymentDetailAndOtherFields={
    "CCL_PRF_Ordering_Status__c":this.prfOrderingStatusToBeUpdated,
    "CCL_Hospital_Purchase_Order_Number__c":this.purchaseorder,
    "CCL_VA_Patient__c":this.checkb,
    "CCL_Billing_Party__c":billParty,
    "CCL_Patient_Country_Residence__c":countPatient,
    "Id": "",
    "FPShipmentId" :this.fpshipmentId,
    "AphShipmentId" :this.aphShipmentId,
    "CCL_Treatment_Protocol_Subject_ID__c": this.protocolNumber + "_" + this.centerNumber + "_" + this.subjectNumber,
    "TherapyType":this.therapyType,
    "ResubmissionFlag":this.variableToDetermineResubmissionFlag,
    "lastNameOptedIn" : this.showLastName
    

   }
   this.paymentDetailAndOtherFields.Id = this.orderIdToBeUpdate;
   let patientPaymentAndOtherDetails={...this.patientDetail, ...this.paymentDetailAndOtherFields};
   updatePrfForm({ result: patientPaymentAndOtherDetails })
    .then((result) => {
    if(result==true)
    {
      this.cancelOrder = false;
      const success = new CustomEvent("success");
      this.dispatchEvent(success);
      this.loaded = false;
    }
    })
    .catch((error) => {
      this.message = "";
      this.error = error;
      this.errorOnUpdate = error.body? error.body.message ?  error.body.message :  error.body.pageErrors[0].message : error;
      this.loaded = false;
    });
  }
  
  get showChangeReasonPrompt() {
    return (this.captureChangeReason && this.submitButtonClicked);
  }

  checkforchangereason() {
    if(this.showChangeReasonPrompt) {
      const showconfirmation = new CustomEvent("showconfirmation");
      this.dispatchEvent(showconfirmation);
    }  else if(this.submitButtonClicked) {
      this.updatePatientDetails();
      this.loaded = true;
    }
  }
  
  @api submitChangesWithReason(reason) {
    this.changeReasonForDataUpdate = reason;
    this.updatePatientDetails();
    this.loaded = true;
  }
  viewDocument(event) {
    this.url = "/sfc/servlet.shepherd/document/download/" + event.target.name;
    window.open(this.url);
  }
}

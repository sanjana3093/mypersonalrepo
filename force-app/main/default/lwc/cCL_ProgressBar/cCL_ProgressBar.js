import { LightningElement,api} from 'lwc';

export default class CCL_ProgressBar extends LightningElement {
    @api steps = [
        { label: 'ORDER ', status:'Approved',date:'21 may 2020 17:23 EST',value: '1' },
        { label: 'APHERESIS',status:'Approved',date:'21 may 2020 17:23 EST', value: '2' },
        { label: 'MANUFACTURING',status:'Approved',date:'21 may 2020 17:23 EST', value: '3' },
        { label: 'DELIVERY',status:'Approved',date:'21 may 2020 17:23 EST', value: '4' },
    ];
    @api currStep='4';
    @api completedStep='3'

    @api
    get chevronjson(){
        return this.steps;
    }
    set chevronjson(json){
        this.steps=json;
    }

    @api
    get chevroncolour(){
        return this.currStep;
    }
    set chevroncolour(color){
        this.currStep=color.currentStep;
        this.completedStep=color.completeStep;
    }
    renderedCallback(){
        let self=this;
        let myStep=this.template.querySelectorAll(".slds-path__item");
        myStep.forEach(element => {
            if(element.dataset.item==self.currStep){
                element.classList.add('slds-is-active');
                element.classList.add('slds-is-current');
                element.classList.remove('slds-is-incomplete');
            }
            if(element.dataset.item<=self.completedStep){
                element.classList.add('slds-is-active');
                element.classList.add('slds-is-complete');
                element.classList.remove('slds-is-incomplete');
            }
        });
    }
}
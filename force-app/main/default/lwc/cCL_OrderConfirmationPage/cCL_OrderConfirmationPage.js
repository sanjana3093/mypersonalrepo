import { LightningElement } from 'lwc';
import { CurrentPageReference,NavigationMixin } from "lightning/navigation";
import { FlowAttributeChangeEvent, FlowNavigationNextEvent,  FlowNavigationBackEvent } from 'lightning/flowSupport';
import CCL_Order_Submitted from '@salesforce/label/c.CCL_Order_Submitted';
import CCL_Order_Submission_Message from '@salesforce/label/c.CCL_Order_Submission_Message';
import CCL_Thank_you from '@salesforce/label/c.CCL_Thank_you';
import CCL_Return_to_Dashboard from '@salesforce/label/c.CCL_Return_to_Dashboard';

export default class CCL_OrderConfirmationPage extends NavigationMixin(
    LightningElement
  ){

    label = {CCL_Order_Submitted,
            CCL_Order_Submission_Message,
            CCL_Thank_you,
            CCL_Return_to_Dashboard
            };


    goToNextCommercialScreen() {
      this[NavigationMixin.Navigate]({
        type: "comm__namedPage",
        attributes: {
            name: 'Home'
        }
      });
        }
}
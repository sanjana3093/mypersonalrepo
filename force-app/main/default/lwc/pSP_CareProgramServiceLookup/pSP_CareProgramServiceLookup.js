import { LightningElement,api,track } from 'lwc';

export default class PSP_CareProgramServiceLookup extends LightningElement {
    @api selectedItem;
    @api productServicename="";
    @api recordtype="Service";
    @api object="CareProgramEnrolleeProduct";
    @api searchtext="Search a Care program Product";
  
    getselectedDetails(event){
       // console.log('the event details are'+JSON.stringify(event.detail));
        this.selectedItem=event.detail.Id;
        this.productServicename=event.detail.Product.Name;
       // console.log(this.productServicename+ this.selectedItem);
    }
      //validate function
      @api
      validate() {
          if (this.selectedItem !== undefined && this.selectedItem !== null) {
              return {
                  isValid: true
              };
          } else {
              //If the component is invalid, return the isValid parameter as false and return an error message. 
              return {
                  isValid: false,
                  errorMessage: 'Please select a Care Program Product.'
              };
          }
      }

}
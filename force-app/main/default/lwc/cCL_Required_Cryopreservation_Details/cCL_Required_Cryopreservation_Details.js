import { LightningElement, api, track, wire } from "lwc";
import getDetails from "@salesforce/apex/CCL_ADF_Controller.getRequiredCryo";
import getCryoDetails from "@salesforce/apex/CCL_ADF_Controller.getRequiredCryoDetails";
import excesSection from "@salesforce/label/c.CCL_Excess_reatined";
import grandTotal from "@salesforce/label/c.CCL_Grand_totals_across_all_bags_and_collection_days";
import excesLabel from "@salesforce/label/c.CCL_Excess_Aph_material_retained_at_site";
import aphdatetime from "@salesforce/label/c.CCL_Apheresis_Expiry_Date_Time";
import wbcConcentration from "@salesforce/label/c.CCL_WBC_Concentration";
import tnc from "@salesforce/label/c.CCL_Total_Nucleated_Cell_Count";
import cd3threshhold from "@salesforce/label/c.CCL_Total_CD3_Cell_Count_Threshold";
import collection from "@salesforce/label/c.CCL_Collection";
import { updateRecord, getRecord } from "lightning/uiRecordApi";
import saveDetails from "@salesforce/apex/CCL_ADF_Controller.saveCryo";
import saveForLater from "@salesforce/apex/CCL_ADF_Controller.saveForLater";
import aphDetail from "@salesforce/label/c.CCL_Apheresis_Material_Cryobag_Collection";
import tnc109 from "@salesforce/label/c.CCL_TNC_10_9";
import CCL_WBC_oncentration_label from "@salesforce/label/c.CCL_WBC_oncentration_label";
import CCL_CD3_suffix from "@salesforce/label/c.CCL_CD3_suffix";
import bagErrMsg from "@salesforce/label/c.CCL_Enter_one_bag_err_msg";
import cd3tnc from "@salesforce/label/c.CCL_CD3_TNC_X100_3";
import CCL_PRF_Resubmission_Approval from "@salesforce/label/c.CCL_PRF_Resubmission_Approval";
import getOrderApprovalInfo from "@salesforce/apex/CCL_ADFController_Utility.getOrderApprovalInfo";  

//imported as part of 850
import reasonForModification from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Reason_For_Modification__c";
import CCL_Approval_Counter__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Approval_Counter__c";
import ID from "@salesforce/schema/CCL_Apheresis_Data_Form__c.Id";
import CCL_Reason_Modal_Header from "@salesforce/label/c.CCL_Reason_Modal_Header";
import CCL_Reason_Modal_Message from "@salesforce/label/c.CCL_Reason_Modal_Message";
import CCL_Reason_Modal_Helptext from "@salesforce/label/c.CCL_Reason_Modal_Helptext";
import CCL_Reason_Modal_Subheading from "@salesforce/label/c.CCL_Reason_Modal_Subheading";
import CCL_Warning from "@salesforce/label/c.CCL_Warning";
import CCL_Threshold from "@salesforce/label/c.CCL_Threshold";
import CCL3_Approved from "@salesforce/label/c.CCL3_Approved";
import CCL_Modal_Close from "@salesforce/label/c.CCL_Modal_Close";
import 	CCL_Back from "@salesforce/label/c.CCL_Back";
import CCL_Save_Changes from "@salesforce/label/c.CCL_Save_Changes";
import CCL_Loading from "@salesforce/label/c.CCL_Loading";
import CCL_Submit from "@salesforce/label/c.CCL_Submit";

import CCL_Cryobag_Create_Err from "@salesforce/label/c.CCL_Cryobag_Create_Err";

import getTimezoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getTimeZoneDetails";
import {
  FlowNavigationNextEvent,
  FlowNavigationBackEvent,
  FlowAttributeChangeEvent
} from "lightning/flowSupport";
//2260
import { registerListener,fireEvent } from "c/cCL_Pubsub";
import { NavigationMixin,CurrentPageReference} from "lightning/navigation";
import CCL_Number_of_Bags from "@salesforce/label/c.CCL_Number_of_Bags";
import reqCryoHeader from "@salesforce/label/c.CCL_Required_Cryopreservation_Details";
import thresholdErrmsg from "@salesforce/label/c.CCL_Threshold_err_msg";
const DATE_TIME_TEXT='CCL_TEXT_Cryopreserve_Start_Date_Time__c';
const SITE_LOCATION = "CCL_Site__c";
export default class CCL_Required_Cryopreservation_Details extends NavigationMixin(
  LightningElement
) {
	showBagFailureMsg;
  @api tableData;
  @track multiple = true;
  @track sobj;
    @api allBags;
  @track recordInput;
  @api aphShelfLife;
  @api aphShelfLifeUnit;
  @api idField;
  @api warningMsg;
  @api shelfLifeUnit;
  @api excessRetained;
  @track grandTotalTNC;
  @track grandTotalCD3;
  @track bagErrMsg=bagErrMsg;
  @track grandTotalCD3TNC;
  @track grandTotalBag = [];
   @track allSummaryList=[];
  @api disableBtn;
  @track displayThresholdWarning=false;
@track timeZone={};
  @track showErr=false;
  @api expiryDate;
  @track goNextScreen = true;
  @track activeSectionName;
  @track totalNucleatedCount;
  @api adfData = {
    CCL_GT_CD3_Cell_Count__c: "",
    CCL_GT_Nucleated_Cell_Count__c: "",
    CCL_CD3_TNC_X100_3__c: "",
    CCL_Apheresis_Expiry_Date_Time__c: "",
    CCL_Excess_Aph_Material_At_Site__c: "",
    CCL_Warning_Message__c:'',
    CCL_TEXT_Apheresis_Expiry_Date_Time__c:'',
    Id: this.recordId,
	CCL_Power_Of_CD3__c: "9",
    CCL_Power_Of_TNC__c:"9"
  };
  //364
  @track isLoading=false;
  @api storedValues;
  @api savedForlaterClicked = false;
  @api flowScreen;
  @api screenName;
  @api myJSON = {};
  @api collectionDates = [];
  @api allCollection = [];
  @api jsonObj = {};
  @api bagCount = [];
  @api configArr = [];
  @api cryobags = [];
  @api cryoBagIdlist = [];
  @track childArr;
  @api isNextClicked = false;
  @track isReview;
  @api navigateFurther = false;
  @api threshold;
  @api cd3threshold;
  @api powerOfThreshold;
  @api powerOfCD3Threshold;
  @api cd3tncthreshold;
  @api recordId;
  @track tnc;
  @track cd3;
  @track count = 0;
  @api isLoaded = false;
  @api jsonData;
  @track previousClicked = false;
  @track match = false;
  @track apiNames = [];
  @track summaryList = [];
  @track myActualArr = [];
  @track hasFieldvalues;
  @api cryoComments;
  @api sectionedArray = [];
  @track idList = [];
  @api configVal;
  @api orderId;
  @track screennameVal="Required Cryopreservation Details";
  @track prfApprovalRequired;
  @track prfResubmission;
  @track prfStatus;
  @track prfWarningMessage= false;	  
  @track label = {
    excesSection,
    excesLabel,
    grandTotal,
    aphdatetime,
    wbcConcentration,
    tnc,
    cd3threshhold,
    collection,
    aphDetail,
    cd3tnc
    ,reqCryoHeader,
    thresholdErrmsg,
	 CCL_Reason_Modal_Header,
    CCL_Reason_Modal_Helptext,
    CCL_Reason_Modal_Message,
    CCL_PRF_Resubmission_Approval,
    CCL_Reason_Modal_Subheading,
    CCL_Save_Changes,
    CCL_Back,
    CCL_Modal_Close,
    CCL3_Approved,
    CCL_Threshold,
    CCL_Warning,
    CCL_Loading,
    CCL_Submit,
	CCL_Cryobag_Create_Err
  };
    //2260
@wire(CurrentPageReference) pageRef;
  //2260
  @api isADFViewerInternal;
  //added as part of 850
  @track isReasonModalOpen = false;
  @track ReasonForMod;
  @track reason;
  @track navigate;
  @track counter;
  @track uniq;
  @track orignalJson;
  @wire(getRecord, {
    recordId: "$recordId",
    fields: [reasonForModification, CCL_Approval_Counter__c]
  })
  Reason({ error, data }) {
    if (error) {
      this.error = error;
    
    } else if (data) {
      this.reason = data.fields.CCL_Reason_For_Modification__c.value;
      this.counter = data.fields.CCL_Approval_Counter__c.value;
      if (this.counter === null) {
        this.counter = 0;
      }
    }
  }
  //changes for 850 end here
  
  get options() {
    return [
      { label: "Yes", value: 'Yes' },
      { label: "No", value: 'No' }
    ];
  }
  @wire(getDetails)
  wiredSteps({ error, data }) {
    if (data) {
      this.sobj = data[0].CCL_Object_API_Name__c;
      this.configVal = data;
      let self = this;
      this.configVal.forEach(function (node) {
         if(self.idField!=undefined){
          if (
            node.CCL_Order_of_Field__c == 0 &&
            node.CCL_Field_Api_Name__c == self.idField
          ) {
            self.setFields(node);
          } else if (node.CCL_Order_of_Field__c != 0) {
            self.setFields(node);
          }
        }
      });
      //2260
      if (this.tableData != undefined&&((this.isADFViewerInternal &&typeof(this.tableData)=='object')||!this.isADFViewerInternal)) {
        this.getSections();
        this.configArr.forEach(function (node) {
          self.setById(node);
        });
        this.isLoaded = false;
        this.getValues();
      }
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }
  @wire(getOrderApprovalInfo, { recordId: "$recordId" })
  wiredAssocidatedOrderApprovalDetails({ error, data }) {
    if (data) {
      this.prfApprovalRequired =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_Order_Approval_Eligibility__c:this.prfApprovalRequired;
      this.prfResubmission =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Approval_Counter__c:this.prfResubmission;
      this.prfStatus =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Ordering_Status__c:this.prfStatus;
      if(this.prfApprovalRequired && this.prfResubmission >='1' && this.prfStatus!='PRF_Approved') {
        this.prfWarningMessage = true;
      }
    } else if (error) {
      this.error = error;
    }
  }
  connectedCallback() {
    this.isLoaded = true;
	//2260
 registerListener("gotoSummary", this.gotoADFSummary,this);
    this.getTimeZoneDetailsVal();
    let self = this;
    let collectionData = JSON.parse(this.jsonData);
    this.jsonObj = JSON.parse(this.jsonData);
	this.orignalJson=JSON.parse(this.jsonData);
    let finalCollectiondata = collectionData["2"];
    let cryoData = collectionData["4"];
    if (cryoData != undefined) {
      this.storedValues = cryoData.croyos;
	  this.allBags = cryoData.allBags;
      this.grandTotalTNC =this.storedValues[0].adf.CCL_GT_Nucleated_Cell_Count__c +" " +tnc109;
      this.grandTotalCD3 =this.storedValues[0].adf.CCL_GT_CD3_Cell_Count__c +" " +CCL_CD3_suffix;
      this.grandTotalCD3TNC =this.storedValues[0].adf.CCL_CD3_TNC_X100_3__c + " " + "%";
      this.adfData.CCL_GT_Nucleated_Cell_Count__c = this.storedValues[0].adf.CCL_GT_Nucleated_Cell_Count__c;
      this.adfData.CCL_GT_CD3_Cell_Count__c = this.storedValues[0].adf.CCL_GT_CD3_Cell_Count__c;
      this.adfData.CCL_CD3_TNC_X100_3__c = this.storedValues[0].adf.CCL_CD3_TNC_X100_3__c;
      this.adfData.Id = this.recordId;
      this.adfData.CCL_Excess_Aph_Material_At_Site__c = this.storedValues[0].adf.CCL_Excess_Aph_Material_At_Site__c;
      this.excessRetained = this.storedValues[0].adf.CCL_Excess_Aph_Material_At_Site__c?'Yes':'No';
      if(this.storedValues[0].adf.CCL_GT_Nucleated_Cell_Count__c<this.threshold|| this.storedValues[0].adf.CCL_GT_CD3_Cell_Count__c<this.cd3threshhold
	  || this.storedValues[0].adf.CCL_CD3_TNC_X100_3__c < this.cd3tncthreshold){
        this.displayThresholdWarning=true;
        this.adfData.CCL_Warning_Message__c=thresholdErrmsg;
        const attributeChangeEvent = new FlowAttributeChangeEvent(
          "warningMsg",
          thresholdErrmsg
        );
        this.dispatchEvent(attributeChangeEvent);
      }
	  else{
        this.displayThresholdWarning=false;
        this.adfData.CCL_Warning_Message__c="";
        const attributeChangeEvent1 = new FlowAttributeChangeEvent("warningMsg",null);
        this.dispatchEvent(attributeChangeEvent1);
      }
    }
    this.cryoComments = this.jsonObj[
      "Collection and Cryopreservation Comments"
    ];
    this.tableData = finalCollectiondata[1];
    if(finalCollectiondata[1][0]["CCL_DIN__c"]===null){
    this.activeSectionName =
      this.label.collection + " " + "1" +" "+ this.label.aphDetail + " for " + " Apheresis ID : " + finalCollectiondata[1][0]["CCL_Apheresis_ID__c"];}
      else{
        this.activeSectionName=this.label.collection + " " + "1" +" "+ this.label.aphDetail + " for "  + " DIN : " + finalCollectiondata[1][0]["CCL_DIN__c"];
      }
    this.tableData.forEach(function (node, index) {
      if(finalCollectiondata[1][index]["CCL_DIN__c"]===null){
      node.attributes.url =
        self.label.collection + " " + (index + 1) +" "+ self.label.aphDetail + " for " + " Apheresis ID : " + finalCollectiondata[1][index]["CCL_Apheresis_ID__c"];}
        else{
          node.attributes.url =
        self.label.collection + " " + (index + 1) +" "+ self.label.aphDetail + " for " + " DIN : " + finalCollectiondata[1][index]["CCL_DIN__c"];
      }
      self.collectionDates.push(node.CCL_End_of_Apheresis_Collection_Date__c);
    });
	this.checkThreshHold();
  }

  getTimeZoneDetailsVal(){
    
    getTimezoneDetails({
      screenName:this.screennameVal, adfId: this.recordId
    })
      .then((result) => {
        this.timeZone = result;
        if (this.aphShelfLife != undefined && this.shelfLifeUnit) {
          this.calExpiryDate(
            this.collectionDates,
            this.aphShelfLife,
            this.shelfLifeUnit
          );
        }
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }

  renderedCallback() {
    let self = this;
    if (this.isLoaded) {
	  if(this.myActualArr.length==0) {	
		this.configVal.forEach(function (node) {
        if (
          node.CCL_Order_of_Field__c == 0 &&
          node.CCL_Field_Api_Name__c == self.idField
        ) {
          self.setFields(node);
        } else if (node.CCL_Order_of_Field__c != 0) {
          self.setFields(node);
        }
      });
	  }
      this.getSections();
      this.configArr.forEach(function (node) {
        self.setById(node);
      });
      this.getValues();
      this.isLoaded = false;
    }
  }
  getSections() {
    let self = this;
    let myObj = { Id: "", myFields: [] };
	if(typeof(this.tableData)=='string'){
      this.tableData=JSON.parse(this.tableData);
    }
    this.tableData.forEach(function (node) {
      myObj = { Id: "", myFields: [] };
      myObj.Id = node.Id;
      self.configArr.push(myObj);
      self.idList.push(node.Id);
    });
  }
  setFields(mainArrr) {
    let fieldArr = {
      Fieldlabel: "",
      fieldApiName: "",
      fieldtype: "",
      fieldorder: "",
      fieldValue: "",
      fieldhelptext: "",
      isRadio: "",
      isEditable: "",
      fieldLength: "",
	  isDate: "",
      class: "",
      isTime: "",
      timeZone:""
    };
    fieldArr.Fieldlabel = mainArrr.MasterLabel;
    fieldArr.fieldApiName = mainArrr.CCL_Field_Api_Name__c;
    fieldArr.fieldtype = mainArrr.CCL_Field_Type__c;
    fieldArr.class='labelClass queryComp date_time'
    //changes for 469
    if(fieldArr.fieldtype=='date' && fieldArr.fieldApiName==DATE_TIME_TEXT){
      fieldArr.Fieldlabel='Start of Cryopreservation Date';
      fieldArr.class = "labelClass dateClass queryComp  date_time";
      fieldArr.isDate = true;
    }
    if(fieldArr.fieldtype=='time' && fieldArr.fieldApiName==DATE_TIME_TEXT){
      fieldArr.Fieldlabel='Start of Cryopreservation Time';
      fieldArr.class = "labelClass timeClass queryComp  date_time";
      fieldArr.isTime = true;
    }
    fieldArr.fieldorder = mainArrr.CCL_Order_of_Field__c;
    fieldArr.fieldhelptext = mainArrr.CCL_Help_Text__c;
    fieldArr.fieldLength = mainArrr.CCL_Field_Length__c;
    fieldArr.isRadio = mainArrr.CCL_Field_Type__c === "Checkbox" ? true : false;
    fieldArr.isEditable = mainArrr.CCL_IsEditable__c ? false : true;
    this.apiNames.push(fieldArr.fieldApiName);
    this.myActualArr.push(fieldArr);
  }
  setById(myArr) {
    this.myActualArr.forEach(function (node) {
      myArr.myFields.push(node);
    });
  }
  getValues() {
    this.apiNames.push('CCL_TEXT_Aph_Collection_End_Date_Time__c');
   this.apiNames = [...new Set(this.apiNames)];
    getCryoDetails({
      sobj: this.sobj,
      cols: this.apiNames.toString(),
      idList: this.idList
    })
      .then((result) => {
        this.hasFieldvalues = result;
        this.hasFieldvalues.forEach(function (node) {});
		if(this.isADFViewerInternal){
          this.hasFieldvalues=this.tableData;
        }
        this.childArr = this.configArr;
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }

  onNext() {
    this.idList = [];
    let self = this;
    this.tableData.forEach(function (node) {
      self.idList.push(node.Id);
    });
    this.template
      .querySelector("c-c-c-l_-required_-cryopreservation_-details_-child")
      .validateCommentsChanges();
    this.isNextClicked = true;
    this.goNextScreen = false;
  }
  onPrevious() {
    this.idList = [];
    let self = this;
    this.tableData.forEach(function (node) {
      self.idList.push(node.Id);
    });
	//2260 removing validation of comment
	 const attributeChangeEvent = new FlowAttributeChangeEvent(
    "screenName",
    "3"
  );
    this.dispatchEvent(attributeChangeEvent);
    this.savedForlaterClicked = true;
    this.goNextScreen = false;
    this.previousClicked = true;
  }
  handleSaveLater(event) {
    let myArr = event.detail.bags;
    let match=false;
    let self = this;
    let collection = {
      bags: [],
      summary: {},
      Id: event.detail.summary.Id,
      adf: this.adfData
    };
    myArr.forEach(function (node) {
      collection.bags.push(node);
    });
    collection.summary = event.detail.summary;
    if(this.allCollection.length==0){
      this.allCollection.push(collection);
    }
   else{
    this.allCollection.forEach(function(node,index){
      if(node.Id==event.detail.summary.Id){
      self.allCollection[index]=collection;
      match=true;
      }
    });
   } 
    if (!match && this.allCollection[0].Id !== event.detail.summary.Id) {
      this.allCollection.push(collection);
    }
    if (this.allCollection.length == this.idList.length) {
      this.myJSON = { croyos: this.allCollection };
      this.jsonObj[this.flowScreen] = this.myJSON;
      this.jsonObj[
        "Collection and Cryopreservation Comments"
      ] = this.cryoComments;
      this.jsonObj["latestScreen"] = this.flowScreen;
     
     //changed as per 850
    
      let val = JSON.parse(this.jsonData);
      let mytable = val["4"];
       if (
        !(mytable!=undefined?this.compareJson(mytable,this.myJSON):true) &&
        this.counter > 0 && (this.savedForlaterClicked || this.previousClicked)
        &&event.detail.commentChanged
      ) {
        this.isReasonModalOpen = true;
    }
    else if ((this.savedForlaterClicked || this.previousClicked)&&event.detail.commentChanged) {
      this.saveJSON();
    }
    else if(!event.detail.commentChanged){
      this.savedForlaterClicked=false;
      
    }
  }
}

compareJson(oldjson,newjson){
  if(oldjson.croyos.length!=newjson.croyos.length){
    return false;
  }
  else{
    for(let i=0;i<=oldjson.croyos.length-1;i++) {
      let oldcoll=oldjson.croyos[i];
      let oldCollId=oldcoll.Id;
      for(let j=0;j<=newjson.croyos.length-1;j++){
        let newColl=newjson.croyos[j];
        let newCollId=newColl.Id;
        if(oldCollId===newCollId){
          if(!this.compareCollection(oldcoll,newColl)){
            return false;
          }
        }
      }
    }
    return true;
  }
}

compareCollection(oldcoll,newcoll){
  let oldCollSumm=oldcoll.summary;
  let newCollSumm=newcoll.summary;
  if(oldCollSumm.CCL_Start_of_Cryopreservation_Date__c!=newCollSumm.CCL_Start_of_Cryopreservation_Date__c||
    oldCollSumm.CCL_Start_of_Cryopreservation_Time__c!=newCollSumm.CCL_Start_of_Cryopreservation_Time__c
    ){
      return false;
    }
  else if(oldcoll.adf.CCL_Excess_Aph_Material_At_Site__c!=newcoll.adf.CCL_Excess_Aph_Material_At_Site__c){
    return false;
  }
  else if(oldcoll.bags.length!=newcoll.bags.length){
    return false;
  }
  else{
    for(let i=0;i<=oldcoll.bags.length-1;i++){
      let oldbag=oldcoll.bags[i];
      let newbag=newcoll.bags[i];
      if(oldbag.CCL_Cryobag_ID__c!=newbag.CCL_Cryobag_ID__c||
        oldbag.CCL_Nucleated_Cell_Count__c!=newbag.CCL_Nucleated_Cell_Count__c||
        oldbag.CCL_Power_Of_TNC__c!=newbag.CCL_Power_Of_TNC__c||
        oldbag.CCL_CD3_Cell_Count__c!=newbag.CCL_CD3_Cell_Count__c||
        oldbag.CCL_CDC_Power__c!=newbag.CCL_CDC_Power__c||
        oldbag.CCL_Total_Volume_Per_Bag__c!=newbag.CCL_Total_Volume_Per_Bag__c
        ){
          return false;
        }
    }
  }
  return true;
}






  handleValidation(event) {
    let self = this;
    let myArr = event.detail.bags;
    let showErr = false;
    this.allSummaryList.push(event.detail.summary);
    this.allSummaryList.forEach(function (node) {
      if (node.CCL_WBC_Concentration__c == "") {
        showErr = true;
      }
    });
	 let match=false;
    if (event.detail.navigateFurther) {
      //startement here
      if(this.summaryList.length==0){
        this.summaryList.push(event.detail.summary);
      }else{
        this.summaryList.forEach(function(node,index){
            if(node.Id==event.detail.summary.Id){
              match=true;
              self.summaryList[index]=event.detail.summary
            }
        });
        if(!match){
          this.summaryList.push(event.detail.summary);
          match=false;
        }
       this.summaryList.forEach(function (node) {
          if (node.CCL_WBC_Concentration__c == "") {
            showErr = true;
          }
        });
      }
 
      myArr.forEach(function (node) {
        node.CCL_Apheresis_Data_Form__c = self.recordId;
        node.CCL_Order__c = self.orderId;
        self.checkduplicate(self.cryobags, node, self);
      });
    } else if (event.detail.summary.CCL_WBC_Concentration__c == "") {
      showErr = true;
    }
    this.uniq = [...new Set(this.cryoBagIdlist)];
    //changed as per 850
    if (this.uniq.length == this.idList.length) {
      let val = this.orignalJson;
      let mytable = val["4"];
      if (
        (!(mytable!=undefined?this.compareJson(mytable,this.myJSON):true)||this.orignalJson["Collection and Cryopreservation Comments"]!==this.cryoComments) &&
        this.counter > 0
      ) {
        this.isReasonModalOpen = true;
      } else {
        this.RecordUpdate();
      }
    } else if (showErr) {
      this.showErr = true;
    } else {
      this.showErr = false;
    }
    this.isNextClicked = false;
  }
  RecordUpdate() {
    const attributeChangeEvent = new FlowAttributeChangeEvent(
      "jsonData",
      JSON.stringify(this.jsonObj)
    );
    this.dispatchEvent(attributeChangeEvent);
    
        this.adfData.CCL_Excess_Aph_Material_At_Site__c = this.adfData
          .CCL_Excess_Aph_Material_At_Site__c
          ? true
          : false;
        this.recordInput = { fields: this.adfData };
     this.isLoading=true;
        if (!this.isADFViewerInternal) {
      updateRecord(this.recordInput)
        .then(() => {})
        .catch((error) => {
        });

      saveDetails({
        bags: this.cryobags,
        summaryLst: this.summaryList,
        comments: this.cryoComments,
        adfId: this.recordId
      })
        .then((result) => {
          //updating the save json varible
          this.allBags = result;
          this.saveLater(result);
          const attributeChangeEvent = new FlowAttributeChangeEvent(
            "screenName",
            "5"
          );
          this.dispatchEvent(attributeChangeEvent);
          const navigateNextEvent = new FlowNavigationNextEvent();
          this.dispatchEvent(navigateNextEvent);
          this.isLoading = false;
          this.error = null;
        })
        .catch((error) => {
          console.log("Its Failed to update the bags" + JSON.stringify(error));
         this.showBagFailureMsg=true;
         this.isLoading = false;
        });
    } else {
      const attributeChangeEvent = new FlowAttributeChangeEvent(
        "screenName",
        "5"
      );
      this.dispatchEvent(attributeChangeEvent);
      const navigateNextEvent = new FlowNavigationNextEvent();
      this.dispatchEvent(navigateNextEvent);
    }
     
  }
  saveLater(allBags) {
    this.myJSON = { croyos: this.allCollection, allBags: allBags };
    this.jsonObj[this.flowScreen] = this.myJSON;
    this.jsonObj[
      "Collection and Cryopreservation Comments"
    ] = this.cryoComments;
    this.jsonObj["latestScreen"] = this.flowScreen;
    this.saveJSON();
  }
  checkduplicate(cryobags, bagList, self) {
    let match=false;
    if (cryobags.length == 0) {
      self.cryobags.push(bagList);
      self.cryoBagIdlist.push(bagList.CCL_Collection__c);
    } else {
      cryobags.forEach(function (node, index) {
        if (
          node.CCL_Collection__c == bagList.CCL_Collection__c &&
          node.key == bagList.key
        ) {
          match = true;
          self.cryobags[index] = bagList;
          self.cryoBagIdlist.push(bagList.CCL_Collection__c);
        }
      });
      if (!match) {
        match = false;
        self.cryobags.push(bagList);
        self.cryoBagIdlist.push(bagList.CCL_Collection__c);
      }
    }
  }
  handlecommentChange(event) {
    this.cryoComments = event.detail;
  }
  onSave(event) {
    this.idList = [];
    let self = this;
    this.tableData.forEach(function (node) {
      self.idList.push(node.Id);
    });
    this.savedForlaterClicked = true;
  }
  handleTotalCount(event) {
    let match = false;
    let self = this;
    let count = 0;
    if (this.bagCount.length == 0) {
      this.bagCount.push(event.detail);
      this.count = event.detail.count;
    } else {
      this.bagCount.forEach(function (node) {
        if (node.index == event.detail.index) {
          node.count = event.detail.count;
          self.count = event.detail.count;
          match = true;
        } else {
          self.count += event.detail.count;
        }
      });
    }
    if (!match && this.bagCount[0].index !== event.detail.index) {
      this.bagCount.push(event.detail);
    }
    this.bagCount.forEach(function (node) {
      count += node.count;
    });
    if (count >= 10) {
      this.disableBtn = true;
    } else {
      this.disableBtn = false;
    }
  }
  saveJSON() {
    if (this.jsonData == undefined || this.jsonData == null) {
      this.jsonObj[this.flowScreen] = this.myJSON;
      this.jsonObj[
        "Collection and Cryopreservation Comments"
      ] = this.cryoComments;
      this.jsonObj["latestScreen"] = this.flowScreen;
    } else {
      if (this.flowScreen in this.jsonObj) {
        delete this.jsonObj[this.flowScreen];
        this.jsonObj[this.flowScreen] = this.myJSON;
        this.jsonObj[
          "Collection and Cryopreservation Comments"
        ] = this.cryoComments;
        this.jsonObj["latestScreen"] = this.flowScreen;
      } else {
        this.jsonObj[this.flowScreen] = this.myJSON;
        this.jsonObj[
          "Collection and Cryopreservation Comments"
        ] = this.cryoComments;
        this.jsonObj["latestScreen"] = this.flowScreen;
      }
    }
    const attributeChangeEvent = new FlowAttributeChangeEvent(
      "jsonData",
      JSON.stringify(this.jsonObj)
    );
    this.dispatchEvent(attributeChangeEvent);
   if (!this.isADFViewerInternal) {
      this.dispatchEvent(attributeChangeEvent);
      saveForLater({
        myJSON: JSON.stringify(this.jsonObj),
        recordId: this.recordId
      })
        .then((result) => {
          //updating the save json varible
          this.error = null;
          if (!this.isNextClicked && this.goNextScreen) {
            this.goToListScreen();
          }
          if (this.previousClicked) {
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
          }
        })
        .catch((error) => {
          this.error = error;
        });
    } else {
      if (!this.isNextClicked && this.goNextScreen) {
        this.goToListScreen();
      }
      if (this.previousClicked) {
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
      }
    }
  }
  goToListScreen() {
     this[NavigationMixin.Navigate]({
      type: "comm__namedPage",
      attributes: {
        name: "Home"
      },
      state: {
        "tabset-8e13c": "4d03f"
      }
    });
  }
  handleGrandTotal(event) {
    let self = this;
    let mytnc = 0,
      mycd3 = 0,
      mywbc = 0;
    let match = false;
    if (this.grandTotalBag.length == 0) {
      this.grandTotalBag.push(event.detail);
    } else {
      this.grandTotalBag.forEach(function (node) {
        if (node.index == event.detail.index) {
          match = true;
          node.tnc = event.detail.tnc;
          node.cd3 = event.detail.cd3;
          node.cd3tnc = event.detail.cd3tnc;
        }
      });
    }
    if (!match && this.grandTotalBag[0].index !== event.detail.index) {
      this.grandTotalBag.push(event.detail);
    }
    this.grandTotalBag.forEach(function (node) {
      mytnc += parseFloat(node.tnc);
      mycd3 += parseFloat(node.cd3);
      mywbc += parseFloat(node.cd3tnc);
    });
    this.grandTotalTNC = this.toFixedNoRounding(mytnc) + " " + tnc109;
    this.grandTotalCD3 = this.toFixedNoRounding( mycd3 )+ " " + CCL_CD3_suffix;
    this.grandTotalCD3TNC = this.toFixedNoRounding( (mycd3 / mytnc)*100) + " " + "%";
    this.adfData.CCL_GT_Nucleated_Cell_Count__c =  this.toFixedNoRounding(mytnc);
    this.adfData.CCL_GT_CD3_Cell_Count__c = this.toFixedNoRounding( mycd3);
    this.adfData.CCL_CD3_TNC_X100_3__c =  this.toFixedNoRounding((mycd3 / mytnc)*100);
    this.adfData.Id = this.recordId;
  }
 /* code for 466*/
  handleChangeRadio(event) {
    this.excessRetained = event.target.value;
    if(event.target.value=='Yes'){
      this.adfData.CCL_Excess_Aph_Material_At_Site__c = true;
    }else{
      this.adfData.CCL_Excess_Aph_Material_At_Site__c = false;
    }

  }
   /* code for cgtu 467*/
   handleThresholdErr() {
	     this.isLoading = true;
    setTimeout(() => {
      this.isLoading = false;
  }, 2000);
    //change for power
    if (this.storedValues != undefined) {
      if (
        this.storedValues[0].adf.CCL_GT_Nucleated_Cell_Count__c <
          this.tnc ||
        this.storedValues[0].adf.CCL_GT_CD3_Cell_Count__c < this.cd3 ||
        this.storedValues[0].adf.CCL_CD3_TNC_X100_3__c < this.cd3tncthreshold
      ) {
        this.displayThresholdWarning = true;
        this.adfData.CCL_Warning_Message__c = thresholdErrmsg;
        const attributeChangeEvent = new FlowAttributeChangeEvent(
          "warningMsg",
          thresholdErrmsg
        );
        this.dispatchEvent(attributeChangeEvent);
      }
    }
    if (
      this.adfData.CCL_GT_Nucleated_Cell_Count__c <
        this.tnc ||
      this.adfData.CCL_GT_CD3_Cell_Count__c < this.cd3 ||
      this.adfData.CCL_CD3_TNC_X100_3__c < this.cd3tncthreshold
    ) {
      this.displayThresholdWarning = true;
      this.adfData.CCL_Warning_Message__c = thresholdErrmsg;
      const attributeChangeEvent = new FlowAttributeChangeEvent(
        "warningMsg",
        thresholdErrmsg
      );
      this.dispatchEvent(attributeChangeEvent);
    } else {
      this.displayThresholdWarning = false;
      this.adfData.CCL_Warning_Message__c = "";
      const attributeChangeEvent1 = new FlowAttributeChangeEvent(
        "warningMsg",
        null
      );
      this.dispatchEvent(attributeChangeEvent1);
    }
  }
  calExpiryDate(dates, shelfLife, unit) {
    //changes for 469
    let earliest = dates.reduce(function (pre, cur) {
      return Date.parse(pre) > Date.parse(cur) ? cur : pre;
    });
    let earliestTime;
    let self = this;
    this.tableData.forEach(function (node) {
      if (earliest == node.CCL_End_of_Apheresis_Collection_Date__c && node.CCL_End_of_Apheresis_Collection_Date__c) {
        earliestTime = node.CCL_TEXT_Aph_Collection_End_Date_Time__c.substr(12,6);
      }
    });
    earliest = new Date(earliest);
    if (unit == "D") {
      earliest.setDate(earliest.getDate() + shelfLife);
    }
    if (unit == "W") {
      earliest.setDate(earliest.getDate() + shelfLife * 7);
    }
    if (unit == "M") {
      earliest.setMonth(earliest.getMonth() + shelfLife);
    }
    if (unit == "Y") {
      earliest.setFullYear(earliest.getFullYear() + shelfLife);
    }
    earliest = this.formatDate(earliest);
    let expiryDate = earliest + " " + earliestTime;
    this.adfData.CCL_Apheresis_Expiry_Date_Time__c=(new Date(expiryDate)).toISOString();
    this.expiryDate = earliest + " "+ earliestTime.substr(0,5)+' '+this.timeZone['CCL_TEXT_Cryopreserve_Start_Date_Time__c'];
    this.adfData.CCL_TEXT_Apheresis_Expiry_Date_Time__c= this.expiryDate;
  }

  formatDate(date) {
    let mydate = new Date(date);
    let monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];

    let day = mydate.getDate();
    let monthIndex = mydate.getMonth();
    let monthName = monthNames[monthIndex];
    let year = mydate.getFullYear();
    return `${day} ${monthName} ${year}`;
  }
  
  //changes as part of 850
  closeModal() {
    this.isReasonModalOpen = false;
    this.isNextClicked=false;
    this.previousClicked=false;
    this.savedForlaterClicked=false;
  }
  handleReasonChange(event) {
    this.ReasonForMod = event.target.value;
  }
  validateTextarea() {
    const textArea = this.template.querySelector(".Reason");
    let commentValue = textArea.value;
    if (commentValue == undefined) {
      this.navigater = false;
      textArea.setCustomValidity("Complete this Field");
      textArea.reportValidity();
    } else {
      this.navigate = true;
      textArea.setCustomValidity("");
      textArea.reportValidity();
    }
  }
  onReasonSubmit(event) {
    this.validateTextarea();
    if (this.navigate) {
      this.updateReason();
    }
  }
  updateReason() {
    const fields = {};
    fields[ID.fieldApiName] = this.recordId;
    fields[reasonForModification.fieldApiName] = this.ReasonForMod;
    const recordInput = { fields };
    //2260
    if (!this.isADFViewerInternal) {
      updateRecord(recordInput)
        .then(() => {
          if (
            this.uniq.length == this.idList.length &&
            !this.savedForlaterClicked &&
            !this.previousClicked
          ) {
            this.RecordUpdate();
          } else {
            this.saveJSON();
          }
        })
        .catch((error) => {
          this.error = error;
        });
    } else {
      if (
        this.uniq.length == this.idList.length &&
        !this.savedForlaterClicked &&
        !this.previousClicked
      ) {
        this.RecordUpdate();
      } else {
        this.saveJSON();
      }
    }
  }
  //changes for 850 end here
   //364
  setAllBoolean(){
    this.isNextClicked=false;
    this.savedForlaterClicked = false;
    this.previousClicked = false;
  }
  checkThreshHold(){
    
    let threshhold,cd3;
    if(this.powerOfThreshold<9){
      threshhold=this.threshold/Math.pow(10, 9-(this.powerOfThreshold));
    }else if(this.powerOfThreshold>9){
      threshhold=this.threshold*Math.pow(10, (this.powerOfThreshold)-9)
    }
    if(this.powerOfCD3Threshold<9){
      cd3=this.cd3threshold/Math.pow(10, 9-(this.powerOfCD3Threshold));
    }else if(this.powerOfCD3Threshold>9){
      cd3=this.cd3threshold*Math.pow(10, (this.powerOfCD3Threshold)-9)
    }
    this.tnc=threshhold!=undefined?this.toFixedNoRounding(threshhold):this.threshold;
    this.cd3=cd3!=undefined?this.toFixedNoRounding(cd3):this.cd3threshold;
   
  }
  toFixedNoRounding(number){
    const reg = new RegExp("^-?\\d+(?:\\.\\d{0," + 2 + "})?", "g")
    const a = number.toString().match(reg)[0];
    const dot = a.indexOf(".");
    if (dot === -1) { // integer, insert decimal dot and pad up zeros
        return a + "." + "0".repeat(2);
    }
    const b = 2 - (a.length - dot) + 1;
    return b > 0 ? (a + "0".repeat(b)) : a;
  }
   //2260
  gotoADFSummary() {
    const screenNextEvent = new FlowAttributeChangeEvent("screenName", "6");
    this.dispatchEvent(screenNextEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
    this.dispatchEvent(navigateNextEvent);
  }
}
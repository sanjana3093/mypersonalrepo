import { LightningElement, wire,track,api } from 'lwc';
import getServiceList from '@salesforce/apex/PSP_Search_Component_Controller.getServiceList';
import { updateRecord} from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const columns = [
     {label: 'Name', fieldName: 'Name', type: 'text',sortable:true},
     {label: 'Active', fieldName: 'IsActive', type: 'boolean',editable:true},
     {label: 'Default', fieldName: 'IsActive', type: 'boolean',editable:true},
     {label: 'Service Family',fieldName: 'NameURL',type: 'url', typeAttributes: {
         label: {
              fieldName: 'FamilyName'
         },
         target: '_blank'
      },sortable:true},
     {label: 'Service Type', fieldName: 'PSP_Service_Type__c', type: 'picklist',sortable:true,editable:true},
     {label: 'Care Plan Template', fieldName: 'TemplateURL', type: 'url',
     typeAttributes: {
        label: {
             fieldName: 'CarePlanTemplate'
        },
        target: '_blank'
     },sortable:true,editable:true}
];

export default class pspActiveServiceList extends LightningElement {
    
    @track draftValues = [];
    @track error;
    @track data;
    @track columns = columns;
    @track loadMoreStatus;
    @api totalNumberOfRows;
    @track sortedBy;
    @track sortDirection = 'asc';
    @track showSpinner=true;
    @api recordId;
    @track refreshdata;
    @wire(getServiceList, { recordId: '$recordId'})
     services({
         error,
         data
     }) {
         if (data) {
            console.log('Data found:- '+JSON.stringify(data));
             this.refreshdata=data;
            this.showSpinner=false;
             this.data = data;
         } else if (error) {
             this.error = error;
             console.log('skms:- ',error);
         }
        }


        //Method for sorting
        sortData(fieldName, sortDirection){
            var data = JSON.parse(JSON.stringify(this.data));
            //function to return the value stored in the field
            var key =(a) => a[fieldName]; 
            var reverse = sortDirection === 'asc' ? 1: -1;
            data.sort((a,b) => {
                let valueA = key(a) ? key(a).toLowerCase() : '';
                let valueB = key(b) ? key(b).toLowerCase() : '';
                return reverse * ((valueA > valueB) - (valueB > valueA));
            });
            //set sorted data to opportunities attribute
            this.data = data;
        }
//this method is called when a column sort icon is clicked
sortThisColumn(event){
            this.sortedBy = event.detail.fieldName;
            this.sortDirection = event.detail.sortDirection;
            this.sortData(this.sortedBy,this.sortDirection);       
        }

        handleSave(event) {
            const recordInputs =  event.detail.draftValues.slice().map(draft => {
                const fields = Object.assign({}, draft);
                return { fields };
            });
        
            const promises = recordInputs.map(recordInput => updateRecord(recordInput));
            Promise.all(promises).then(() => {
            this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Services updated',
                        variant: 'success'
                    })
                );
                 // Clear all draft values
                 this.draftValues = [];
                 // Display fresh data in the datatable
                 return refreshApex(this.refreshdata);
                 
            }).catch(error => {
                // Handle error
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error updating record',
                        message: error.body.message,
                        variant: 'error'
                    }));
            });

        }
        
  
}
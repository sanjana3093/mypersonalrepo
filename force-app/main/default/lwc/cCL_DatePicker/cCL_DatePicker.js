import { LightningElement, track, api } from "lwc";
import CCL_Select_Date from "@salesforce/label/c.CCL_Select_Date";
const d = new Date();

export default class CCLDatePicker extends LightningElement {
  @api offSetDays;
  today = new Date();
  @track start_date;
  @track end_date;
  @api availableDates;
  @api therapyType;
  @api plannedPickupDateFlow;
  @api currentSelectedDate;
  @api responseRecieved= false;

  @track range;
  @track selectedDate = "";
  @track currentMonth;
  @api CalendarField;
  @track _initializationCompleted = false;
  @track Months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  @track pickerClass =
    "slds-datepicker slds-dropdown slds-dropdown_left slds-hide";
  @track currMonth = d.getMonth();
  @track currYear = d.getFullYear();
  @track currDay = d.getDate();
  @track currentDate = new Date(this.currYear, this.currMonth, this.currDay );
  @track todayDate = new Date(this.currYear, this.currMonth, this.currDay );
  @track endDate = new Date(this.currYear, this.currMonth, this.currDay +182 );
  @track endDateReschdule = new Date(this.currYear, this.currMonth, this.currDay +182 );
  @track dates = [];
  @track calendarPlaceHolder;
  label ={CCL_Select_Date};
  
  constructor() {
    super();
  }

  @api get availableSlot(){
    return this.availableSlot;
  }
  set availableSlot(value){
    if(value) {
      this.availableDates = value;
    }
  }

  @api get getCalendarField(){
    return 'the value';
  }
  set getCalendarField(prvalue){
    if(prvalue){
      this.CalendarField=true;
      this.template.querySelector(".customdatepicker").disabled = false;
    }
  }

  @api get datePickerError(){
    return 'the value';
  }
  set datePickerError(prvalue){
    if(prvalue){
      this.template.querySelector(".customdatepicker").classList.add("redBorder");
    }
  }
  renderedCallback() {
    if (this.isLoadedFirst) {
      this.selectedDate = this.plannedPickupDateFlow;
      this.template.querySelector(".customdatepicker").disabled = false;
      this.CalendarField = true;
      if(this.availableDates){
      this.availableSlots();
      }
      this.isLoadedFirst = false;
    }
    let self = this;
    if (!this._initializationCompleted) {
      this.template
        .querySelector(".slds-input")
        .addEventListener("click", function (event) {
          self.onDropDownClick(event.target);
          event.stopPropagation();
        });

      this.template.addEventListener("click", function (event) {
        event.stopPropagation();
      });
      document.addEventListener("click", function (event) {
        self.closeAllDropDown();
        let tempDate;
        if (self.currentSelectedDate){
          tempDate = new Date(self.currentSelectedDate.split('/').reverse().join('/'));
        } else if(self.plannedPickupDateFlow) {
          const flowMonth = self.plannedPickupDateFlow.split(" ");
          const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
          const monthIndex = monthNames.indexOf(flowMonth[1])+1;
          const formattedDate = ("0" + (flowMonth[0])).slice(-2);
          const formattedMonth = ("0" + (monthIndex)).slice(-2);
          const finalDate = flowMonth[2] + '/' + formattedMonth + '/' + formattedDate;
          tempDate = new Date(finalDate);
        }
        if(tempDate){
        self.formCalendar(tempDate.getFullYear(), tempDate.getMonth());
        self.currMonth = tempDate.getMonth();
        self.currYear = tempDate.getFullYear();
      }
      });
      this._initializationCompleted = true;
    }
  }
  connectedCallback() {
    if(this.plannedPickupDateFlow){
      this.isLoadedFirst = true;
    }
    this.formCalendar(this.currYear, this.currMonth);
  }
  closeAllDropDown() {
    Array.from(this.template.querySelectorAll(".ms-picklist-dropdown")).forEach(
      function (node) {
        node.classList.remove("slds-is-open");
      }
    );
  }
  onDropDownClick(dropDownDiv) {
    if(this.CalendarField){
      let classList = Array.from(
        this.template.querySelectorAll(".ms-picklist-dropdown")
      );
      if (!classList.includes("slds-is-open")) {
        this.closeAllDropDown();
        Array.from(
          this.template.querySelectorAll(".ms-picklist-dropdown")
        ).forEach(function (node) {
          node.classList.add("slds-is-open");
        });
      } else {
        this.closeAllDropDown();
      }
      
      this.currMonth = d.getMonth();
      this.currYear = d.getFullYear();
      this.currDay = d.getDate();
      this.formCalendar(this.currYear, this.currMonth);
       if(this.offSetDays !== undefined) {
        this.currentDate =  new Date(this.currYear, this.currMonth, this.currDay +this.offSetDays );
        this.todayDate = new Date(this.currYear, this.currMonth, this.currDay );
       this.endDate = new Date(this.currYear, this.currMonth, this.currDay  + this.offSetDays  + 183 ); 
       }
      this.availableSlots();
    }
  }

  diff = (sdate, edate) => {
    let diffTime = Math.abs(
      new Date(edate).getTime() - new Date(sdate).getTime()
    );
    return Math.ceil(diffTime / (1000 * 60 * 60 * 24));
  };

  valid_date = (sdate, edate) => {
    return new Date(edate) >= new Date(sdate);
  };

  prevMonth() {
    if (this.currMonth == 0) {
      this.currMonth = 11;
      this.currYear = this.currYear - 1;
    } else {
      this.currMonth = this.currMonth - 1;
    }
    this.formCalendar(this.currYear, this.currMonth);
    // Calling because to retain the available slots when coming back
    this.availableSlots();
  }
  nextMonth() {
    if (this.currMonth == 11) {
      this.currMonth = 0;
      this.currYear = this.currYear + 1;
    } else {
      this.currMonth = this.currMonth + 1;
    }
    this.formCalendar(this.currYear, this.currMonth);
    // Calling because to retain the available slots when coming back
    this.availableSlots();
  }
  formCalendar(y, m) {
    this.dates = [];
    let className;
    // First day of the week in the selected month
    let firstDayOfMonth = new Date(y, m, 1).getDay();
    // Last day of the selected month
    let lastDateOfMonth = new Date(y, m + 1, 0).getDate();
    // Last day of the previous month
    let lastDayOfLastMonth =
      m == 0 ? new Date(y - 1, 11, 0).getDate() : new Date(y, m, 0).getDate();

    this.currentMonth = this.Months[m] + " " + y;
    // Write the header of the days of the week

    // Write the days
    let i = 1;
    do {
      let dow = new Date(y, m, i).getDay();
      let mon;
      if (m < 9) {
        mon = "0" + (m + 1);
      } else {
        mon = m + 1;
      }
      // If Sunday, start new row
      if (dow == 0) {
      }
      else if (i == 1) {
        let k = lastDayOfLastMonth - firstDayOfMonth + 1;
        for (let j = 0; j < firstDayOfMonth; j++) {
          className = "not-current";
          this.dates.push({
            className,
            formatted:
              k > 9 ? k + "/" + mon + "/" + y : "0" + k + "/" + mon + "/" + y,
            text: k
          });
          k++;
        }
      }
      // Write the current day in the loop
      let chk = new Date();
      let chkY = chk.getFullYear();
      let chkM = chk.getMonth();
      if (
        chkY == this.currYear &&
        chkM == this.currMonth &&
        i == this.currDay
      ) {
        this.dates.push({
          className,
          formatted:
            i > 9 ? i + "/" + mon + "/" + y : "0" + i + "/" + mon + "/" + y,
          text: i
        });
      } else {
        className = "normal";
        this.dates.push({
          className,
          formatted:
            i > 9 ? i + "/" + mon + "/" + y : "0" + i + "/" + mon + "/" + y,
          text: i
        });
      }
      // If Saturday, closes the row

      // If not Saturday, but last day of the selected month
      // it will write the next few days from the next month
      if (i == lastDateOfMonth) {
        let k = 1;
        for (dow; dow < 6; dow++) {
          className = "not-current";
          this.dates.push({
            className,
            formatted:
              k > 9 ? k + "/" + mon + "/" + y : "0" + k + "/" + mon + "/" + y,
            text: k
          });
          k++;
        }
      }

      i++;
    } while (i <= lastDateOfMonth);
  }
  setSelected(e) {
    if (e.target.dataset.date != "not-current") {
      // this.currentSelectedDate is used for pickupdelivery comp
        this.currentSelectedDate = e.target.dataset.item;
        if(this.currentSelectedDate) {
          this.template.querySelector(".customdatepicker").classList.remove("redBorder");
        }
        let dateSeleted = e.target.dataset.item;
        let formattedDate = dateSeleted.split('/').reverse().join('/');
        this.selectedDate = this.formatDate(formattedDate);
        const formattedStartDate = formattedDate.replace(/\//g, '-');  
        const parsedFormattedDate =new Date(formattedStartDate);
        this.closeAllDropDown();

        let finalDates = {plannedDateReviewScreen:this.currentSelectedDate, plannedDateJSON:parsedFormattedDate};


        const selectedEvent = new CustomEvent("passselecteddate", {
            detail: finalDates
        });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
      }
  }
  availableSlots() {  
      let mydate = this.availableDates;
      let self = this;
      this.dates.forEach(function (item) {
        let newStartDate = item.formatted.split('/').reverse().join('/');
        const formattedates=new Date(newStartDate);
        if ((formattedates <=  self.currentDate || formattedates >=self.endDate) && !self.responseRecieved) {
          item.className = "not-current";
        } else if ((formattedates < self.todayDate || formattedates >=self.endDateReschdule) && self.responseRecieved) {
          item.className = "not-current";
        }
        else if (item.formatted == mydate && item.className == "normal" ) {
           item.className = "today";
        }
      });
  }

  formatDate(date) {
    let mydate = new Date(date);
    let monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];

    let day = mydate.getDate();
    let monthIndex = mydate.getMonth();
    let monthName = monthNames[monthIndex];
    let year = mydate.getFullYear();
    return `${day} ${monthName} ${year}`;
  }
}
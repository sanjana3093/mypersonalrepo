import { LightningElement,api,track,wire } from 'lwc';
import { updateRecord,getRecord } from 'lightning/uiRecordApi';
import CCL_Recomended_Cryo_Testing_Error_Msg from "@salesforce/label/c.CCL_Recomended_Cryo_Testing_Error_Msg";
import futureTimeError from "@salesforce/label/c.CCL_Future_TestTime_Error";
import getPageTimeZoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getPageTimeZoneDetails";
import getFormattedDateTimeDetails from "@salesforce/apex/CCL_ADFController_Utility.getFormattedDateTimeDetails";
import getTimezoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getTimeZoneDetails";
import enterBothDateTime from "@salesforce/label/c.CCL_Enter_both_Date_and_Time";
//imported as part of 850
import reasonForModification from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Reason_For_Modification__c";
import CCL_Approval_Counter__c from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Approval_Counter__c";
import ID from "@salesforce/schema/CCL_Apheresis_Data_Form__c.Id";
import CCL_Reason_Modal_Header from "@salesforce/label/c.CCL_Reason_Modal_Header";
import CCL_Reason_Modal_Message from "@salesforce/label/c.CCL_Reason_Modal_Message";
import CCL_Reason_Modal_Helptext from "@salesforce/label/c.CCL_Reason_Modal_Helptext";
import CCL_Reason_Modal_Subheading from "@salesforce/label/c.CCL_Reason_Modal_Subheading";
import CCL_TimeZone from "@salesforce/label/c.CCL_TimeZone";
import CCL_Modal_Close from "@salesforce/label/c.CCL_Modal_Close";
import CCL3_Approved from "@salesforce/label/c.CCL3_Approved";
import CCL_Utility_modal_Cancel from "@salesforce/label/c.CCL_Utility_modal_Cancel";
import CCL_Save_Changes from "@salesforce/label/c.CCL_Save_Changes";
import CCL_Submit from "@salesforce/label/c.CCL_Submit";
import CCL_Cancel_Button from "@salesforce/label/c.CCL_Cancel_Button";
//364
import badTimeInput from "@salesforce/label/c.CCL_Enter_time_in_12_hour_format";
const SITE_LOCATION = "CCL_Site__c";
const TEXT_API_FIELD = "CCL_Date_Time_Text_API__c";
const FIELD_API_NAME = "CCL_Date_Time_field_API__c";
const CCL_TEXT_Cryopreserve_Mat_Date_Time__c="CCL_TEXT_Cryopreserve_Mat_Date_Time__c";
export default class CCL_Recommended_Cryopreservation_Child extends LightningElement {
    @api apiNames = [];
    @api collectionid;
    @track recordInput;
    @track fields={};
    @track myValue;
    @track saveLaterValue;
    @track formattedDateTime;
    @api readOnlyfields=false;
    @api storedArr = [];
    @api index;
    @track selrec = [];
    @api sectionArr = [];
    @api navigateFurther = false;
	@track mydate=new Date();
    @api timeZoneDetails;
    @track myDateFields={};
    @track myTimeFields={}
	 //2260
  @api isADFViewerInternal;
    @api myFormatDates;
    @api myFormatTimes;
    @api allTimeZoneDetails;
    @track screennameVal="Recommended Cryopreservation Details";
    @track siteTimeZone;
    @api recordIdVal;
    @api siteTimeZoneVal;
	
	//added as part of 850
  @track isReasonModalOpen=false;
  @track ReasonForMod;
  @track reason;
  @track navigate;
  @track counter;
  @track orignalJson;
  @api values;
   @track labels={
      CCL_Reason_Modal_Header,
      CCL_Reason_Modal_Message,
      CCL_Reason_Modal_Helptext,
      CCL_Reason_Modal_Subheading,
      CCL_Save_Changes,
      CCL_Utility_modal_Cancel,
      CCL3_Approved,
      CCL_Modal_Close,
      CCL_TimeZone,
      CCL_Submit,
      CCL_Cancel_Button
    };
	@api commentfileflag=false;
  @api
  get commentorfilechange(){
    return this.myValue;
  }
	set commentorfilechange(flag){
    if(flag){
    this.commentfileflag=flag;}
  }
  @wire(getRecord, { recordId: "$recordId", fields:[reasonForModification,CCL_Approval_Counter__c] })
  Reason({ error, data }) {
    if (error) {
      this.error = error;
    } else if (data) {
     this.reason = data.fields.CCL_Reason_For_Modification__c.value;
      this.counter=data.fields.CCL_Approval_Counter__c.value;
      if(this.counter===null){
        this.counter=0;
      }
    }
  };
	
	
    @api
    get saveclicked(){
        return this.myValue;
    }
    @api
    get timeZone() {
      return "my value";
    }
  
    set timeZone(timezoneVal) {
      this.timeZoneDetails=timezoneVal;
    }

    set recordId(recordId) {
      this.recordIdVal=recordId;
      this.getTimeZoneDetailsVal();
    }
  
    @api
    get recordId() {
      return this.recordIdVal;
    }
	
	
	
    getTimeZoneDetailsVal(){
      getTimezoneDetails({
        screenName:this.screennameVal, adfId: this.recordIdVal
      })
        .then((result) => {
          this.timeZoneDetails = result;
          this.siteTimeZoneVal = this.timeZoneDetails[CCL_TEXT_Cryopreserve_Mat_Date_Time__c];
          this.error = null;
        })
        .catch((error) => {
          this.error = error;
        });
    }
  

    @wire(getPageTimeZoneDetails, {screenName:"$screennameVal"})
    wiredTimezoneSteps({ error, data }) {
        if (data) {
            this.allTimeZoneDetails = data;
            this.siteTimeZone='Time zone of '+this.allTimeZoneDetails[0][SITE_LOCATION];
            this.error = null;
        } else if (error) {
            this.error = error;
        }
    }

     getFormattedDateTimeVal(dateTimeVal, timeZoneVal, dateTimeField, field) {
    //364 changes
    let formattedVal = "";
    let currentDate = new Date();
    getFormattedDateTimeDetails({
      dateTimeVal: dateTimeVal,
      timeZoneVal: timeZoneVal
    })
      .then((result) => {
        this.formattedDateTime = result;
        formattedVal = this.formattedDateTime;
        this.fields[dateTimeField] = this.formattedDateTime;
        this.recordInput = { fields: this.fields };
        let resultDate = new Date(result);
        let nowDate = new Date(currentDate.toISOString());
        if (
          resultDate.getTime() > nowDate.getTime() &&
          nowDate.getDate() == resultDate.getDate() &&
          nowDate.getMonth() == resultDate.getMonth() &&
          nowDate.getYear() == resultDate.getYear()
        ) {
          this.validateTimeErr(field);
        } else if (nowDate < resultDate) {
          this.validateTimeErr(field);
        } else {
          this.updateRecordValues();
        }
      })
      .catch((error) => {
        this.error = error;
      });
    return formattedVal;
  }
  validateTimeErr(field) {
    //364 changes
    this.template.querySelectorAll("lightning-input").forEach(function (node) {
      if (node.dataset.item == field && node.type == "time") {
        node.setCustomValidity(futureTimeError);
        node.reportValidity();
      }
    });
    const selectEvent2 = new CustomEvent("saveclickedfalse", {});
    this.dispatchEvent(selectEvent2);
  }
   
    @api
    get myActualArr() {
      return "my value";
    }
    set myActualArr(prValue) {
      this.sectionArr = prValue;
      this.getTimeZoneDetailsVal();
      let myValues;
      let self = this;
      if (this.index != undefined) {
        let index = this.index + 1;
        myValues = this.selrec != undefined ? this.selrec[index] : null;
		this.values=myValues;
      }
	    //2260
  if(this.selrec!=undefined){
        for (const [key, value] of Object.entries(this.selrec)) {
          if (value.Id == this.collectionid) {
          myValues=value;
           
          }
        }
      }
      if (
        this.sectionArr.length > 0 &&
        myValues != undefined &&
        myValues != null
      ) {
        for (let i = 0; i < this.sectionArr.length; i++) {
          let newArr = { sectioname: "", myFields: [] };
          newArr.sectioname = this.sectionArr[i].sectioname;
          this.sectionArr[i].myFields.forEach(function (node) {
            let fieldArr = {
              Fieldlabel: "",
              fieldApiName: "",
              fieldtype: "",
              fieldorder: "",
              fieldValue: "",
              fieldhelptext: "",
              placeHolder: "",
              showStep: "",
              isRadio: "",
              max:"",
              rangeOverflow:"",
			  stepMisMatch:"",
			  badInput:"",
  		  isDate:'',
            class:'',
            isTime:'',
            timeZone:'',
			min:"",
            rangeUnderflow:"",
			step:""
            };
            fieldArr.Fieldlabel = node.Fieldlabel;
            fieldArr.fieldApiName = node.fieldApiName;
            fieldArr.fieldtype = node.fieldtype;
            fieldArr.fieldorder = node.fieldorder;
            fieldArr.fieldhelptext = node.fieldhelptext;
            fieldArr.placeHolder = node.placeHolder;
            fieldArr.showStep = node.showStep;
            fieldArr.max = node.max;
            fieldArr.rangeOverflow = node.rangeOverflow;
            fieldArr.stepMisMatch = node.stepMisMatch;
			fieldArr.min = node.min;
            fieldArr.rangeUnderflow = node.rangeUnderflow;
            fieldArr.isRadio = node.isRadio;
			fieldArr.step = node.step;
            fieldArr.fieldValue = myValues[node.fieldApiName];
            self.fields[fieldArr.fieldApiName] = myValues[node.fieldApiName];
            fieldArr.class='labelClass';
          if(fieldArr.fieldtype==='Date' ||fieldArr.fieldtype=='date'){
            fieldArr.placeHolder = "";
            fieldArr.isDate=true;    
            fieldArr.Fieldlabel = 'Date Of Testing';    
            let dateVal='';
            if(fieldArr.fieldValue!=='' && fieldArr.fieldValue!==undefined){
              dateVal=fieldArr.fieldValue.substr(0,12);
            }
            self.myDateFields[node.fieldApiName]=self.formatDate(dateVal);
            fieldArr.class='labelClass dateClass slds-size_1-of-2 slds-grid';
            fieldArr.fieldValue='';
          }
          if(fieldArr.fieldtype=='Time' ||fieldArr.fieldtype=='time'){
            fieldArr.placeHolder = "";
            fieldArr.isTime=true;
            fieldArr.Fieldlabel = 'Time';
            fieldArr.badInput=badTimeInput;
            let timeVal='';
            if(fieldArr.fieldValue!==undefined && fieldArr.fieldValue!==''){
              timeVal=fieldArr.fieldValue.substr(12,23);
            }
            self.myTimeFields[node.fieldApiName]=self.getFormattedTime(timeVal);
            if(self.timeZoneDetails!==undefined && self.timeZoneDetails[node.fieldApiName]!==undefined){
              fieldArr.timeZone=self.timeZoneDetails[node.fieldApiName];}
            fieldArr.class='slds-grid labelClass timeClass slds-col slds-size_1-of-2';
            fieldArr.fieldValue='';
          }
            if(self.readOnlyfields){
              fieldArr.placeHolder = "";
              if(fieldArr.fieldtype==='Time'){
                let timeVal='';
              if(myValues[node.fieldApiName]!==undefined && myValues[node.fieldApiName]!==null && myValues[node.fieldApiName]!==''){
                timeVal=myValues[node.fieldApiName].substr(12,23);
              }
                fieldArr.fieldValue = self.getFormattedTime(timeVal);
                fieldArr.fieldtype="text";
                fieldArr.class='labelClass';
              }else if(fieldArr.fieldtype==='Date'){
                let dateVal='';
              if(myValues[node.fieldApiName]!==undefined && myValues[node.fieldApiName]!==null && myValues[node.fieldApiName]!==''){
                dateVal=myValues[node.fieldApiName].substr(0,12);
              }
                fieldArr.fieldValue=self.formatDate(dateVal);
                fieldArr.fieldtype="text";
                fieldArr.class='labelClass';
              }
            }
            newArr.myFields.push(fieldArr);
          });
          this.storedArr.push(newArr);
        }
        this.fields["Id"] = this.collectionid;
        this.recordInput = { fields: this.fields };
        
      }
    }
     set saveclicked(prvalue){
     if (prvalue) {
      this.myValue = true;
      this.validateFields();
      //364
      this.navigateFurther = this.checkIfError();
      if (this.navigateFurther) {
        this.setDateTimeTextFields();
        this.recordInput = { fields: this.fields };
      } else {
        const selectEvent2 = new CustomEvent("saveclickedfalse", {});
        this.dispatchEvent(selectEvent2);
      }
    }
    }

    compareJson(orignalJson,newJson){
        let oJson=orignalJson.fields;
        let nJson=newJson;
       let oldDateArr=oJson.CCL_TEXT_Cryopreserve_Mat_Date_Time__c?oJson.CCL_TEXT_Cryopreserve_Mat_Date_Time__c.split(" "):'';
        let newDateArr=nJson.CCL_TEXT_Cryopreserve_Mat_Date_Time__c?nJson.CCL_TEXT_Cryopreserve_Mat_Date_Time__c.split(" "):'';

          
        if(oJson.CCL_Hematocrit__c===nJson.CCL_Hematocrit__c&&oJson.CCL_Platelets_x10_3_L_Cryo__c===nJson.CCL_Platelets_x10_3_L_Cryo__c&&
          oJson.CCL_Neutrophils__c===nJson.CCL_Neutrophils__c&&oJson.CCL_Others__c===nJson.CCL_Others__c&&
          oldDateArr[0]==newDateArr[0]&&oldDateArr[1]==newDateArr[1]&&oldDateArr[2]==newDateArr[2]&&
          oJson.CCL_Hemoglobin_g_dL_Cryo__c===nJson.CCL_Hemoglobin_g_dL_Cryo__c&&oJson.CCL_White_Blood_Cell_Count_l_Cryo__c===nJson.CCL_White_Blood_Cell_Count_l_Cryo__c&&
          oJson.CCL_Absolute_Lymphocyte_Count_l_Cryo__c===nJson.CCL_Absolute_Lymphocyte_Count_l_Cryo__c&&oJson.CCL_Monocytes__c===nJson.CCL_Monocytes__c&&
          oJson.CCL_Lymphocytes__c===nJson.CCL_Lymphocytes__c&&oJson.CCL_Blasts__c===nJson.CCL_Blasts__c&&oJson.CCL_Viability__c===nJson.CCL_Viability__c){
          
            return true;
        }else{
           return false;
        }
      }

    updateRecordValues(){
      if(this.counter>0){
          if (this.navigateFurther && !this.futureDate) {
            
          this.fields["Id"] = this.collectionid;
            let updateReasonDetails = {
              fields: this.fields,
              index: this.index+1
            };
          const selectEvent = new CustomEvent('updatereasonevent', {
                  detail: updateReasonDetails
              });
              this.dispatchEvent(selectEvent);
        }
      }else if (this.recordInput != undefined && this.navigateFurther && !this.futureDate && !this.isADFViewerInternal) {
            this.fields['Id']=this.collectionid;
        updateRecord(this.recordInput)
          .then(() => {
              const selectEvent = new CustomEvent('mycustomevent', {
                  detail: this.fields
              });
              let validationDetails = {
                navigateFurther: this.navigateFurther,
                Id: this.collectionid
              };
              this.dispatchEvent(selectEvent);
              const selectEvent1 = new CustomEvent("myvalidationevent", {
                detail: validationDetails
              });
              this.dispatchEvent(selectEvent1);
              })
          .catch((error) => {
            });
      }
      else if(this.recordInput==undefined|| this.isADFViewerInternal){
        this.fields['Id']=this.collectionid;
        const selectEvent = new CustomEvent('mycustomevent', {
            detail: this.fields
        });
        let validationDetails = {
          navigateFurther: this.navigateFurther,
          Id: this.collectionid
        };
        this.dispatchEvent(selectEvent);
        const selectEvent1 = new CustomEvent("myvalidationevent", {
          detail: validationDetails
        });
        this.dispatchEvent(selectEvent1);
      }
      }

    formatDateFormat(date) {
      if(date!==null && date!==''&& date!==undefined){
       let d = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    d=new Date(date);
  }
         let month = '' + (d.getMonth() + 1),
         day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
    }

    return '';
    } 
    

    setDateTimeTextFields(){
      let self=this;
	   let validDates=0;
      this.allTimeZoneDetails.forEach(function (node) {
        if (self.myDateFields[node[TEXT_API_FIELD]] && self.myTimeFields[node[TEXT_API_FIELD]]) {
			 validDates+=1;
          self.convertDate(
            self.myDateFields[node[TEXT_API_FIELD]],
            self.myTimeFields[node[TEXT_API_FIELD]],
            node[TEXT_API_FIELD],
            node[FIELD_API_NAME]
          );
        }
      });
	   if(!validDates){
        this.updateRecordValues();
      }
    }

    convertDate(date, time, field, dateTimeField) {
      let timeZoneVal='';
      let self=this;
      if(this.timeZoneDetails!==undefined){
      timeZoneVal=this.timeZoneDetails[field];
      }
      this.fields[field] =
      this.formatDate(date) + " " + time.substr(0, 5) + " " + timeZoneVal;
      let formattedVal=this.getFormattedDateTimeVal(this.formatDateFormat(date) + " " +this.getFormattedTimeForTimeField(time),timeZoneVal,dateTimeField,field);
    }

    getFormattedTimeForTimeField(fieldValue){
      let timeval=fieldValue;
      if(timeval!==null && timeval!=='' && timeval!=undefined){
        let format;
        let hrs = timeval.substr(0, 2);
        let min = timeval.substr(3, 2);
        return hrs+':'+min +":00.000";}
        return '';
    }

    @api
    get savelaterclicked(){
        return this.saveLaterValue;
    }
    set savelaterclicked(prvalue){
        if(prvalue){
            this.saveLaterValue=true;
			this.fields["Id"] = this.collectionid;
            const selectEvent = new CustomEvent('mycustomevent', {
                detail: this.fields
            });
            this.dispatchEvent(selectEvent);
        }
    }
    get options() {
        return [
          { label: "Manual", value: "Manual" },
          { label: "Automatic", value: "Automatic"}
        ];
      }

      handleBlur(event){
        let apiName = event.target.dataset.item;
        let comp2 = this.template.querySelectorAll(".timeClass");
        if (apiName in this.myFormatDates && event.target.classList.contains('timeClass')) {
          this.myTimeFields[apiName] = event.target.value;
          comp2.forEach(function (node) {
            node.value = "";
          });
        }
    
      }

      handleChange(event){
    this.fields[event.target.dataset.item]=event.target.value;
    let key=event.target.dataset.item;
    let comp=this.template.querySelectorAll('.dateClass');
    let comp2=this.template.querySelectorAll('.timeClass');
    if(key in this.myFormatDates && event.target.classList.contains('dateClass')){
      this.hasDates=true;
     this[key]=this.formatDate(event.target.value);
      this.myDateFields[key]=this.formatDate(event.target.value);
      comp.forEach(function(node){
        node.value='';
      })
      
    }
    if(key in this.myFormatTimes  && event.target.classList.contains('timeClass')){
      this.myTimeFields[key]=event.target.value;
      comp2.forEach(function(node){
       // node.value='';
      })
    }
   
        this.fields['Id']=this.collectionid;
        if(event.target.dataset.item=='CCL_TEXT_Cryopreserve_Mat_Date_Time__c'){
          this.fields['CCL_TEXT_Cryopreserve_Mat_Date_Time__c']= this.myDateFields[key]+" "+this.myTimeFields[key]+" "+this.timeZoneDetails[key];
        }
       
        this.recordInput = { fields:this.fields};

    }
    handleChangeRadio(event){
      this.fields['CCL_Differential_Performed__c']=event.target.value;
      this.recordInput = { fields:this.fields};

  }
  @api
  get storedValues() {
    return this.selrec;
  }
  set storedValues(prvalue) {
    this.selrec = prvalue;
    this.getTimeZoneDetailsVal();
    let myValues;
    let self = this;
    if (this.index != undefined) {
      let index = this.index + 1;
      myValues = this.selrec != undefined ? this.selrec[index] : null;
    }
	 //2260
  if(this.selrec!=undefined){
        for (const [key, value] of Object.entries(this.selrec)) {
          if (value.Id == this.collectionid) {
           myValues=value;
           
          }
        }
      }
    if (
      this.sectionArr.length > 0 &&
      myValues != undefined &&
      myValues != null
    ) {
      for (let i = 0; i < this.sectionArr.length; i++) {
        let newArr = { sectioname: "", myFields: [] };
        newArr.sectioname = this.sectionArr[i].sectioname;
        this.sectionArr[i].myFields.forEach(function (node) {
          let fieldArr = {
            Fieldlabel: "",
            fieldApiName: "",
            fieldtype: "",
            fieldorder: "",
            fieldValue: "",
            fieldhelptext: "",
            placeHolder: "",
            showStep: "",
            isRadio: "",
            max:"",
            rangeOverflow:"",
			stepMisMatch:"",
			isDate:'',
            class:'',
            isTime:'',
            timeZone:'',
			badInput:'',
			min:"",
            rangeUnderflow:"",
			step:""
          };
          fieldArr.Fieldlabel = node.Fieldlabel;
          fieldArr.fieldApiName = node.fieldApiName;
          fieldArr.fieldtype = node.fieldtype;
          fieldArr.fieldorder = node.fieldorder;
          fieldArr.fieldhelptext = node.fieldhelptext;
          fieldArr.placeHolder = node.placeHolder;
          fieldArr.showStep = node.showStep;
          fieldArr.max = node.max;
          fieldArr.rangeOverflow = node.rangeOverflow;
          fieldArr.stepMisMatch = node.stepMisMatch;
		  fieldArr.min = node.min;
          fieldArr.rangeUnderflow = node.rangeUnderflow;
          fieldArr.isRadio = node.isRadio;
		  fieldArr.step = node.step;
          fieldArr.fieldValue = myValues[node.fieldApiName];
          self.fields[fieldArr.fieldApiName] = myValues[node.fieldApiName];
          fieldArr.class='labelClass';
          if(fieldArr.fieldtype==='Date' ||fieldArr.fieldtype=='date'){
            fieldArr.placeHolder = "";
            fieldArr.isDate=true;
            fieldArr.Fieldlabel = 'Date Of Testing';
            let dateVal='';
            if(fieldArr.fieldValue!=='' && fieldArr.fieldValue!==undefined){
              dateVal=fieldArr.fieldValue.substr(0,12);
            }
            self.myDateFields[node.fieldApiName]=self.formatDate(dateVal);
            fieldArr.class='labelClass dateClass slds-size_1-of-2 slds-grid';
            fieldArr.fieldValue='';
          }
          if(fieldArr.fieldtype=='Time' ||fieldArr.fieldtype=='time'){
            fieldArr.placeHolder = "";
            fieldArr.isTime=true;
			fieldArr.badInput=badTimeInput;
            fieldArr.Fieldlabel = 'Time';
            let timeVal='';
            if(fieldArr.fieldValue!==undefined && fieldArr.fieldValue!==''){
              timeVal=fieldArr.fieldValue.substr(12,23);
            }
            self.myTimeFields[node.fieldApiName]=self.getFormattedTime(timeVal);
            if(self.timeZoneDetails!==undefined && self.timeZoneDetails[node.fieldApiName]!==undefined){
              fieldArr.timeZone=self.timeZoneDetails[node.fieldApiName];}
            fieldArr.class='slds-grid labelClass timeClass slds-col slds-size_1-of-2';
            fieldArr.fieldValue='';
          }
          if(self.readOnlyfields){
            fieldArr.placeHolder = "";
            if(fieldArr.fieldtype==='Time'){
              let timeVal='';
              if(myValues[node.fieldApiName]!==undefined && myValues[node.fieldApiName]!==null && myValues[node.fieldApiName]!==''){
                timeVal=myValues[node.fieldApiName].substr(12,23);
              }
              fieldArr.fieldValue = self.getFormattedTime(timeVal);
              fieldArr.fieldtype="text";
            }else if(fieldArr.fieldtype==='Date'){
              let dateVal='';
              if(myValues[node.fieldApiName]!==undefined && myValues[node.fieldApiName]!==null && myValues[node.fieldApiName]!==''){
                dateVal=myValues[node.fieldApiName].substr(0,12);
              }
              fieldArr.fieldValue=self.formatDate(dateVal);
              fieldArr.fieldtype="text";
            }
          }
          newArr.myFields.push(fieldArr);
        });
        this.storedArr.push(newArr);
      }
      this.fields["Id"] = this.collectionid;
      this.recordInput = { fields: this.fields };
    }
  }

  getFormattedDateTimeValue(fieldType,fieldValue){
    let value;
    let self=this;
    if(fieldType==='Time'){
      value = self.getFormattedTime(fieldValue);
    }else if(fieldType==='Date'){
      value=self.formatDate(fieldValue);
    }
    return value;
  }

  getFormattedTime(fieldValue){
    let timeval=fieldValue;
    if(timeval!=null && timeval!=='' && timeval){
      let format;
      let hrs = timeval.substr(0, 2);
      let min = timeval.substr(3, 2);
      if(hrs>12){
        hrs=hrs-12;
        format=' PM'
      }else{
        format=' AM'
      }
      if(hrs==='00'){
        hrs=12;
      }
      return hrs+':'+min;}
      return '';
}
formatDate(date) {
  if(date!==null && date!==''&& date!==undefined){
   let mydate = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    mydate=new Date(date);
  }
  let monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];
  let day = '' + mydate.getDate();
  if (day.length < 2) 
  day = '0' + day;
  let monthIndex = mydate.getMonth();
  let monthName = monthNames[monthIndex];
  let year = mydate.getFullYear();
  return [ day,monthName,year].join(' ');
}
return '';
}
  @api
  get summarypage() {
    return false;
  }
  set summarypage(prvalue) {
    if (prvalue) {
        this.readOnlyfields=true;
    }
  }
  checkIfError(){
    let areAllValid=true;
    let inputs=this.template.querySelectorAll('lightning-input');
    inputs.forEach(input=>{
      if(!input.checkValidity()){
      input.reportValidity();
      areAllValid=false;
      }
    });
    return areAllValid;
  }
   validateFields() {
    let inputs = this.template.querySelectorAll("lightning-input");
    for (let i = 0; i < inputs.length; i++) {
      this.checkForDateTime(inputs[i]);
      if (inputs[i].type == "date" && inputs[i].checkValidity()) {
        let date = new Date();
        this.mydate = new Date(this.myDateFields[inputs[i].dataset.item]);
        if (date < this.mydate) {
          inputs[i].setCustomValidity(CCL_Recomended_Cryo_Testing_Error_Msg);
          inputs[i].reportValidity();
        } else {
          inputs[i].setCustomValidity("");
          inputs[i].reportValidity();
        }
      }
      this.validateTime(inputs[i]);
    }
  }
  //364 changes
  checkForDateTime(inputCmp) {
    let key = inputCmp.dataset.item;
    if (inputCmp.type == "date" || inputCmp.type == "time") {
      if (
        (this.myTimeFields[key] == undefined || (this.myTimeFields[key] != undefined &&this.myTimeFields[key].length==0) ) &&
        this.myDateFields[key] != undefined && this.myDateFields[key].length>0&&
        inputCmp.type == "time"
      ) {
        inputCmp.setCustomValidity(enterBothDateTime);
        inputCmp.reportValidity();
        this.navigateFurther = false;
      } else if (
        this.myTimeFields[key] != undefined &&
        this.myDateFields[key] == undefined &&
        inputCmp.type == "date"
      ) {
        inputCmp.setCustomValidity(enterBothDateTime);
        inputCmp.reportValidity();
      } else {
        inputCmp.setCustomValidity("");
        inputCmp.reportValidity();
      }
    }
  }
 validateTime(input){
   let value=this.myTimeFields[input.dataset.item];
  if(input.label==="Time" && value!==undefined && value!==null){
    let currentDateTime= new Date();
    let inputDateTime=new Date();
    let currentTime= currentDateTime.getTime();
    let time=value.split(":");
    inputDateTime.setHours(time[0], time[1], 0);
    let inputTime=inputDateTime.getTime();
     if(inputTime>currentTime && this.mydate.getDate()===currentDateTime.getDate() && this.mydate.getMonth()===currentDateTime.getMonth()
    && this.mydate.getFullYear()===currentDateTime.getFullYear()){
      input.setCustomValidity(futureTimeError);
      input.reportValidity();
    }
    else{
      input.setCustomValidity('');
      input.reportValidity();
    }
   }
 }
 
  //changes as part of 850
 closeModal() {
  const selectEvent2 = new CustomEvent("saveclickedfalse", {});
  this.dispatchEvent(selectEvent2);
  this.commentfileflag=false;
  this.isReasonModalOpen=false;
}
handleReasonChange(event){
  this.ReasonForMod=event.target.value;
}
validateTextarea(){
  const textArea=this.template.querySelector('.Reason');
  let commentValue=textArea.value;
  if(commentValue==undefined){
    this.navigater=false;
    textArea.setCustomValidity('Complete this Field');
    textArea.reportValidity();
  }else{
    this.navigate=true;
    textArea.setCustomValidity('');
    textArea.reportValidity();
  }
}
onReasonSubmit(event){
  this.validateTextarea();
  if(this.navigate){
  this.updateReason();}
}
updateReason(){
  const fields = {};
  fields[ID.fieldApiName] = this.recordId;
  fields[reasonForModification.fieldApiName] =this.ReasonForMod;
  const recordInput = { fields };
  if(this.isADFViewerInternal){
    this.fields['Id']=this.collectionid;
              const selectEvent = new CustomEvent('mycustomevent', {
                  detail: this.fields
              });
              let validationDetails = {
                navigateFurther: this.navigateFurther,
                Id: this.collectionid
              };
              this.dispatchEvent(selectEvent);
              const selectEvent1 = new CustomEvent("myvalidationevent", {
                detail: validationDetails
              });
              this.dispatchEvent(selectEvent1);
            this.isReasonModalOpen=false;
  }else{
  updateRecord(recordInput)
      .then(() => {
        if (this.recordInput != undefined && this.navigateFurther && !this.futureDate) {
          updateRecord(this.recordInput)
            .then(() => {
            })
            .catch((error) => {
            });
            this.fields['Id']=this.collectionid;
              const selectEvent = new CustomEvent('mycustomevent', {
                  detail: this.fields
              });
              let validationDetails = {
                navigateFurther: this.navigateFurther,
                Id: this.collectionid
              };
              this.dispatchEvent(selectEvent);
              const selectEvent1 = new CustomEvent("myvalidationevent", {
                detail: validationDetails
              });
              this.dispatchEvent(selectEvent1);
            this.isReasonModalOpen=false;
        }
      }).catch((error) => {
        this.error = error;
      });
  }
    }
  //changes for 850 end here
 
}
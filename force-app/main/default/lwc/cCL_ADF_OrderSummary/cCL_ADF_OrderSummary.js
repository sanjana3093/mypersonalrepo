import { LightningElement,api,wire } from 'lwc';
import getAphDataFormStatus from '@salesforce/apex/CCL_PRF_Controller.getAphDataFormStatus';
import getADFFileDetails from '@salesforce/apex/CCL_PRF_Controller.getADFFileDetails';
import CCL_Collection_Center from "@salesforce/label/c.CCL_Collection_Center";
import CCL_ADF_Submitter from "@salesforce/label/c.CCL_ADF_Submitter";
import CCL_ADF_Submission_Date from "@salesforce/label/c.CCL_ADF_Submission_Date";
import CCL_ADF_Approver from "@salesforce/label/c.CCL_ADF_Approver";
import CCL_ADF_Approval_Date from "@salesforce/label/c.CCL_ADF_Approval_Date";
import CCL_ApheresisDetails_Summary from "@salesforce/label/c.CCL_ApheresisDetails_Summary";
import CCL_Apheresis_Expiry_Date from "@salesforce/label/c.CCL_Apheresis_Expiry_Date";
import getAddress from "@salesforce/apex/CCL_Utility.fetchAddress";
import CCL_View_ADF from "@salesforce/label/c.CCL_View_ADF";
export default class CCLADFOrderSummary extends LightningElement {
label = {CCL_Collection_Center,CCL_ADF_Submitter,CCL_ADF_Submission_Date,CCL_ADF_Approver,CCL_ADF_Approval_Date,
  CCL_ApheresisDetails_Summary,CCL_Apheresis_Expiry_Date,CCL_View_ADF};
@api apheresisId;
@api adfStatus;
@api apheresisDataFormId;
@api SCPurl;
@api contentDocumentId;
@api disablePrintPDF;
@api adfSubmitter;
@api adfApprover;
@api status;
@api showViewADF;
@api addressList;

    @wire(getAphDataFormStatus, { orderId: "$apheresisId"})
    getAphDataFormStatus({ error, data }) {
      if (data) {
        this.adfStatus = data;
        if(this.adfStatus[0].CCL_ADF_Submitted_By__c!==undefined)
        {
          this.adfSubmitter = this.adfStatus[0].CCL_ADF_Submitted_By__r.Name;
        }
        if(this.adfStatus[0].CCL_ADF_Approved_By__c!==undefined)
        {
          this.adfApprover = this.adfStatus[0].CCL_ADF_Approved_By__r.Name;
        }
        this.status = this.adfStatus[0].CCL_Status__c;
        this.apheresisDataFormId = this.adfStatus[0].Id;
        if(this.adfStatus[0].CCL_Apheresis_Collection_Center__c){
          this.getAddresses();
        }
		this.getSCPFile() ;
      } else if (error) {
        this.error = error;
      }
    }

    getSCPFile() {
        getADFFileDetails({
         recordId: this.apheresisDataFormId
        })
          .then((result) => {
           this.contentDocumentId =
              result.length > 0
                ? result[0].ContentDocumentId
                : this.contentDocumentId;
            this.error = null;
            if(this.contentDocumentId!==undefined)
            {
              this.showViewADF = true;
            }
            else{
              this.showViewADF = false;
            }
         })
          .catch((error) => {
           this.error = error;
           });
      }
     viewPdf() {
     if(this.contentDocumentId!==undefined)
     {
      this.SCPurl =
      "/sfc/servlet.shepherd/document/download/" + this.contentDocumentId;
      window.open(this.SCPurl);
     }
 }
 getAddresses(){
  getAddress({
    accountIds: this.adfStatus[0].CCL_Apheresis_Collection_Center__c,
  })
    .then((result) => {
      this.addressList=result;
      this.error = null;
    })
    .catch((error) => {
      this.error = error;
    });
}
 }
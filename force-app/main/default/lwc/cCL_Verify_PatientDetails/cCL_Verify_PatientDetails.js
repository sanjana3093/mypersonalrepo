import { LightningElement,track, wire, api } from 'lwc';

import {
  FlowNavigationNextEvent,
  FlowAttributeChangeEvent
} from "lightning/flowSupport";

import {getRecord,updateRecord} from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id';
import NAME_FIELD from '@salesforce/schema/User.Name';

import CCL_VerifyPatient_header from "@salesforce/label/c.CCL_VerifyPatient_header";
import CCL_VerifyPatient_checkbox from "@salesforce/label/c.CCL_VerifyPatient_checkbox";
import CCL_Field_Required from "@salesforce/label/c.CCL_Field_Required";

import COMPLETED_BY from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Patient_Identity_Confirmed_By__c";
import COMPLETED_ON from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Patient_Identity_Confirmed_Date_Time__c";
import COMPLETED_ON_DATE from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_TEXT_Patient_Id_Confirmed_Date_Time__c";
import ID from "@salesforce/schema/CCL_Apheresis_Data_Form__c.Id";
//2260
import {CurrentPageReference } from "lightning/navigation";
//2260
import {registerListener} from 'c/cCL_Pubsub';

import getUserTimeZoneDetails from "@salesforce/apex/CCL_ADF_Controller.getUserTimeZoneDetails";
import getUserTimeFormatted from "@salesforce/apex/CCL_ADF_Controller.getUserTimeInFormat";

import saveForLater from "@salesforce/apex/CCL_ADF_Controller.saveForLater";
import getTimezoneDetails from "@salesforce/apex/CCL_ADFController_Utility.getTimeZoneDetails";
import getTimezoneIDDetails from "@salesforce/apex/CCL_ADFController_Utility.getTimeZoneIDDetails";
import getFormattedDateTimeDetails from "@salesforce/apex/CCL_ADFController_Utility.getFormattedDateTimeDetails";
import CCL_PRF_Resubmission_Approval from "@salesforce/label/c.CCL_PRF_Resubmission_Approval";
import CCL_VerifyPatient from "@salesforce/label/c.CCL_Verify";
import CCL_Warning from "@salesforce/label/c.CCL_Warning";
import CCL_GOTO_ADFSummary from "@salesforce/label/c.CCL_Go_to_Summary";
import getOrderApprovalInfo from "@salesforce/apex/CCL_ADFController_Utility.getOrderApprovalInfo";
const CCL_TEXT_Patient_Id_Confirmed_Date_Time__c = "CCL_TEXT_Patient_Id_Confirmed_Date_Time__c";
export default class CCL_Verify_PatientDetails extends LightningElement {
  @api recordId;
  @track error;
  @track name;
  @track dateTime;
  @track latestScreeninJSON;
  @track goToSummary;
  @track fieldValidity;  
  @track prfApprovalRequired;
  @track prfResubmission;
  @track prfStatus;
  @track prfWarningMessage= false;
  @api screenName;
  @api flowScreen;
  @api timeZoneVal;
  @api timeZoneFinal;
  @track labels={
    CCL_VerifyPatient_header,
    CCL_PRF_Resubmission_Approval,
    CCL_VerifyPatient_checkbox,
    CCL_VerifyPatient,
    CCL_Warning,
    CCL_GOTO_ADFSummary
  };
  @wire(CurrentPageReference) pageRef;
  //2260
  @api isADFViewerInternal;	
  @api jsonObj = {}; // this will contain the entire json structure of all the pages if any data saved
  @api jsonData; // the json data in string received from flow

  @api timeFormat='EEEE dd MMMMM yyyy\' \'HH:mm z';
  @api timeZoneFormatted;
  @api userTimeZone;
  @api userDateText;
  @api timeZoneIDVal;

  @wire(getUserTimeFormatted,{dateTimeVal: '$userTimeZone',format: '$timeFormat'})
  wiredStepUserTimeFormatted({ error, data }) {
    if (data) {
      this.dateTime = data;
      this.error = null;
    } else if (error) {
      this.error = error;
    }
  }
@wire(getOrderApprovalInfo, { recordId: "$recordId" })
  wiredAssocidatedOrderApprovalDetails({ error, data }) {
    if (data) {
      this.prfApprovalRequired =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_Order_Approval_Eligibility__c:this.prfApprovalRequired;
      this.prfResubmission =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Approval_Counter__c:this.prfResubmission;
      this.prfStatus =data[0].CCL_Order__r!==undefined?data[0].CCL_Order__r.CCL_PRF_Ordering_Status__c:this.prfStatus;  
          
      if(this.prfApprovalRequired && this.prfResubmission >='1' && this.prfStatus!='PRF_Approved') {
        this.prfWarningMessage = true;
      }
    } else if (error) {
      this.error = error;
    }
  }

  getUserTimeZone(timeZoneVal){
    getUserTimeZoneDetails({timeZoneVal:timeZoneVal})
    .then((result) => {
      this.userTimeZone = result;
      this.getDateTextVal();
    })
    .catch((error) => {
      this.error = error;
    });
  }
  getDateTextVal(){
    if(this.userTimeZone!==undefined && this.userTimeZone!==null && this.userTimeZone!==''){
      let fields=this.userTimeZone.split('T');
      let timeVal=fields[1];
      let dateVal=fields[0];
      let timeValue=timeVal.split(":");
      let textVal=this.formatDate(dateVal)+' '+timeValue[0]+':'+timeValue[1]+' '+this.timeZoneVal[COMPLETED_ON_DATE.fieldApiName];
      this.userDateText=textVal;
    }
  }

  assignPatientFieldValue(){
    if(this.userTimeZone!==undefined && this.userTimeZone!==null && this.userTimeZone!==''){
      let fields=this.userTimeZone.split('T');
      let timeVal=fields[1];
      let dateVal=fields[0];
      let timeValue=timeVal.split(":");
      let textVal=this.formatDate(dateVal)+' '+timeValue[0]+':'+timeValue[1]+' '+this.timeZoneVal[COMPLETED_ON_DATE.fieldApiName];
      this.getFormattedPatientDateTimeVal(dateVal+' '+timeValue[0]+':'+timeValue[1]+':000',this.timeZoneVal[COMPLETED_ON_DATE.fieldApiName],textVal);
      }
        }

        formatDate(date) {
          let mydate = new Date(date);
          let monthNames = [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
          ];
          let day = mydate.getDate();
          let monthIndex = mydate.getMonth();
          let monthName = monthNames[monthIndex];
          let year = mydate.getFullYear();
          return `${day} ${monthName} ${year}`;
        }

        getFormattedPatientDateTimeVal(dateTimeVal,timeZoneVal,textVal){
          let formattedVal='';
          const fields = {};
          fields[ID.fieldApiName] = this.recordId;
          fields[COMPLETED_BY.fieldApiName] = USER_ID;
          fields[COMPLETED_ON_DATE.fieldApiName] = textVal;
          getFormattedDateTimeDetails({dateTimeVal:dateTimeVal,timeZoneVal:timeZoneVal})
            .then((result) => {
              this.formattedDateTime = result;
              fields[COMPLETED_ON.fieldApiName] =this.formattedDateTime;
              const recordInput = { fields };
              updateRecord(recordInput)
                  .then(() => {
                         //code to save data in json
              if(!this.goToSummary){
                if (this.jsonData == undefined || this.jsonData == null) {
                  this.jsonObj["latestScreen"] = "1";
                }
                else{
                  this.jsonObj = JSON.parse(this.jsonData);
                  this.jsonObj["latestScreen"] = "1";
                }
              }
                const attributeChangeEvent1 = new FlowAttributeChangeEvent("jsonData",JSON.stringify(this.jsonObj));
                this.dispatchEvent(attributeChangeEvent1);
                saveForLater({
                  myJSON: JSON.stringify(this.jsonObj),
                  recordId: this.recordId
                  })
                  .then((result) => {
                    if(this.goToSummary){
                      //this.adfSummary();
                    } else{
                    this.navigateNext();
                    }
                    this.error = null;
                  })
                  .catch((error) => {
                    this.error = error;
                  });
                  //code to save data in json ends here
                    })
                    .catch(error => {
                      this.error = error;
                    });
            })
            .catch((error) => {
              this.error = error;
            });
            return formattedVal;
        }

    connectedCallback() {
		    //  2260 
    registerListener("gotoSummary", this.gotoADFSummary,this);
        window.addEventListener('beforeunload', this.beforeUnloadHandler.bind(this));
        if (this.jsonData != undefined || this.jsonData != null) {
          this.jsonObj = JSON.parse(this.jsonData);
          if(this.jsonObj["latestScreen"]=='6'){
            this.latestScreeninJSON =true;
          }
        }
        this.getTimeZoneScreenDetails("Verify Patient Details");
        this.getTimeZoneIDScreenDetails("Verify Patient Details");
        }

      getTimeZoneScreenDetails(screenNameVal){
        getTimezoneDetails({screenName: screenNameVal, adfId: this.recordId})
          .then((result) => {
            this.timeZoneVal = result;
            if(this.timeZoneVal){
            this.timeZoneFinal=this.timeZoneVal[CCL_TEXT_Patient_Id_Confirmed_Date_Time__c];
            }
              this.error = null;
          })
          .catch((error) => {
            this.error = error;
          });
      }

      getTimeZoneIDScreenDetails(screenNameVal){
        getTimezoneIDDetails({screenName: screenNameVal, adfId: this.recordId})
          .then((result) => {
            this.timeZoneIDVal = result;
            this.getUserTimeZone(this.timeZoneIDVal[CCL_TEXT_Patient_Id_Confirmed_Date_Time__c]);
              this.error = null;
          })
          .catch((error) => {
            this.error = error;
          });
      }

      beforeUnloadHandler(evt) {
        if (typeof evt == 'undefined') {
            evt = window.event;
            }
        if (evt){
           evt.returnValue = 'Are you sure you want to leave?';
           }
        return 'Are you sure you want to leave?';
      }


      @wire(getRecord, {
          recordId: USER_ID,
          fields: [NAME_FIELD]
      }) wireuser({
          error,
          data
      }) {
          if (error) {
             this.error = error ;
          } else if (data) {
              this.name = data.fields.Name.value;
          }
      }
      onNext(event){
        //calling method to assign values to the fields. on result navigation to next
        this.validateCheckbox();

      }
      validateCheckbox(){
        const checkBox=this.template.querySelector('.checkBox');
        const mylabel=this.template.querySelector('.myVerifyCheckbox');
        if(!checkBox.checked){
          this.fieldValidity = false;
          checkBox.setCustomValidity(CCL_Field_Required);
        checkBox.reportValidity();
        mylabel.classList.add('marginLeft');
        }
        else{
       //2260
          if(this.isADFViewerInternal){
            if(!this.goToSummary){
				//next button pressed
				this.navigateNext();}
            else{
				//got to adf summary button pressed
				this.gotoADFSummary();
            }
          }else{
            this.assignPatientFieldValue();
            this.fieldValidity = true;
          }
      }
	  }

      navigateNext(){
		  const attributeChangeEvent = new FlowAttributeChangeEvent(
          "screenName",
          "1"
        );
        this.dispatchEvent(attributeChangeEvent);
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
		}
      adfSummary(){
        this.goToSummary = true;
        this.validateCheckbox();
        if(this.fieldValidity){
        const attributeChangeEvent = new FlowAttributeChangeEvent(
          "screenName",
          "6"
        );
        this.dispatchEvent(attributeChangeEvent);
      const navigateNextEvent = new FlowNavigationNextEvent();
      this.dispatchEvent(navigateNextEvent);
        }
      }
	   //2260
   gotoADFSummary(){
    const screenNextEvent = new FlowAttributeChangeEvent("screenName", "6");
    this.dispatchEvent(screenNextEvent);
    const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
  }
}
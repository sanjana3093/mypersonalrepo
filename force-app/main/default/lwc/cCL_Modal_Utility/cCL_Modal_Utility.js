import { LightningElement, track, api } from 'lwc';
import CCL_Reject_Button from "@salesforce/label/c.CCL_Reject_Button";
import CCL_Approve_Button from "@salesforce/label/c.CCL_Approve_Button";
import CCL_Utility_modal_Cancel from "@salesforce/label/c.CCL_Utility_modal_Cancel";


export default class CCL_Modal_Utility extends LightningElement {
    @api isModalOpen;
    @api isApproveBtnDisabled;
    @api isRejectBtnDisabled;
    @api checkBoxVal;
    @api showValidationMsg;
    @api showRejectBtn;
    @api showApproveBtn;

    @track labels={
        CCL_Reject_Button,
        CCL_Approve_Button,
        CCL_Utility_modal_Cancel
    }
   
    connectedCallback() {}
    
    closeModal() {
        let closeEve = new CustomEvent('close');
        this.dispatchEvent(closeEve);
    }

    approveOrder(event) {
        if (this.showValidationMsg) {
            let showValidation = new CustomEvent("showvalidation");
            this.dispatchEvent(showValidation);
        } else {
            let approveEve = new CustomEvent('orderapproved');
            this.dispatchEvent(approveEve);
        }
    }

    rejectOrder(event) {
        if (this.showValidationMsg) {
            let showValidation = new CustomEvent("showvalidation");
            this.dispatchEvent(showValidation);
        } else {
            let rejectEve = new CustomEvent('orderreject');
            this.dispatchEvent(rejectEve);
        }
    }
   
}
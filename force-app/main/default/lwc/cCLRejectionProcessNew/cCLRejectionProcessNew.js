import { LightningElement,track,api,wire } from 'lwc';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import REASON_FOR_REJECTION_FIELD from '@salesforce/schema/CCL_Shipment__c.CCL_Product_Rejection_Reason__c';
import SHIPMENT_OBJECT from '@salesforce/schema/CCL_Shipment__c';
import { updateRecord } from "lightning/uiRecordApi";
import utilityMethodToConvertDate from "@salesforce/apex/CCL_FPShipmentController.getFormattedDate";
import {getRecord} from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id';
import NAME_FIELD from '@salesforce/schema/User.Name';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from "lightning/navigation";

import getFieldDetails from "@salesforce/apex/CCL_FPShipmentController.getShipmentData";
import utilityMethodToConvertLastModifiedDate from "@salesforce/apex/CCL_FPShipmentController.getFormattedDateTime";
import getsUserTimeZoneMethod from "@salesforce/apex/CCL_FPShipmentController.getsUserTimeZone";
import getCheckUserPermission from "@salesforce/apex/CCL_FPShipmentController.checkUserPermission";

import CCL_Mandatory_Field from "@salesforce/label/c.CCL_Mandatory_Field";
import rejectProductDeliveryLabel from "@salesforce/label/c.CCL_Reject_Product_Delivery";
import actualProductDeliveryDateLabel from "@salesforce/label/c.CCL_Actual_Product_Delivery_Date";
import actualProductDeliveryTimeLabel from "@salesforce/label/c.CCL_Actual_Product_Delivery_Time";
import timeZoneLabel from "@salesforce/label/c.CCL_Time_Zone";
import numberOfDosesShippedLabel from '@salesforce/label/c.CCL_numberOfDosesShipped';
import numberOfBagsShippedLabel from '@salesforce/label/c.CCL_Number_of_Bags_Shipped';
import expiryDateOfShippedBagsLabel from '@salesforce/label/c.CCL_expiryDateOfShippedBags';
import productDeliveryRejectedLabel from '@salesforce/label/c.CCL_Product_Delivery_Rejected';
import rejectDeliveryMessageLabel from '@salesforce/label/c.CCL_Reject_Delivery_Message';
import immediateAssistanceLabel from '@salesforce/label/c.CCL_Immediate_Assistance';
import NUMBER_OF_BAGS_RECEIVED_LABEL from '@salesforce/label/c.CCL_numberOfBagsReceived';
import FP_SHIPMENT_REJECTION_ATTESTATION_LABEL from '@salesforce/label/c.CCL_FP_SHIPMENT_REJECTION_ATTESTATION_LABEL';
import PLEASE_ENTER_TIME_LABEL from '@salesforce/label/c.CCL_please_enter_time_warning';
import REJECT_DELIVERY_BUTTON_LABEL from '@salesforce/label/c.CCL_Reject_Delivery_button';
import BACK_LABEL from '@salesforce/label/c.CCL_Back';
import PLEASE_SELECT_DATE_LABEL from '@salesforce/label/c.CCL_Please_select_date_warning';
import BACK_TO_SHIPMENT_LABEL from '@salesforce/label/c.CCL_Back_to_Shipment';
import LOADING_LABEL from '@salesforce/label/c.CCL_loading_alternative_option';
import CLOSE_LABEL from '@salesforce/label/c.CCL_close_alt_text';
import ERROR_LABEL from '@salesforce/label/c.CCL_form_element_help_error';
import PRODUCT_REJECTION_NOTES_LABEL from '@salesforce/label/c.CCL_Product_Rejection_Notes';
import REASON_FOR_REJECTION_LABEL from '@salesforce/label/c.CCL_Reason_for_Rejection';
import PLEASE_SELECT_LABEL from '@salesforce/label/c.CCL_PleaseSelect';
import errorArrayAvailable from '@salesforce/label/c.CCL_Error_Array_available';
import actualProductDeliveryDateNotFuture from '@salesforce/label/c.CCL_Actual_Product_Delivery_Date_Not_Future';
import unknownError from '@salesforce/label/c.CCL_Unknown_error';

const checkCustomPermissionFpReceiver = 'CCL_FP_Receiver';
export default class CCLRejectionProcessNew extends NavigationMixin(LightningElement) {

    @api shipmentrecorddetail;
    @api recordId;
    @api partofacceptanceprocess;
    @api partofrejectionprocess;
    @api capturePrimaryFPShipmentAccess;
    @api isContentLoaded = false;
    @track finishedProductRecordTypeId;
    @track returnVal;
    @track err;
    @track fields = {};
    @track recordInput;
    @track selectedDate;
    @track selectedTime;
    @track actualDateValue;
    @track numberOfBagsReceived;
    @track reasonForRejection;
    @track productionRejectionNotes;
    @track rejectConfirmationCheckBox;
    @track closeCurrentModal='close';
    @track loggedInUserName;
    @track errorValue;
    @track showFinalScreen=false;
    @track showErrorOnScreen=false;
    @track isLoaded=true;
    @track EMPTY_STRING = "";
    @track siteTimeZone='Ship-to-Location Time Zone';
    @track lastModifiedDate;
    @track lastModifiedDateTimeInNovartisFormat
    @track dataEntered;
    @track timeZoneValue;
    @track selectedTargetValue;

    @track label = {
      rejectProductDeliveryLabel,
      actualProductDeliveryDateLabel,
      actualProductDeliveryTimeLabel,
      timeZoneLabel,
      numberOfDosesShippedLabel,
      numberOfBagsShippedLabel,
      expiryDateOfShippedBagsLabel,
      productDeliveryRejectedLabel,
      rejectDeliveryMessageLabel,
      immediateAssistanceLabel,
      NUMBER_OF_BAGS_RECEIVED_LABEL,
      FP_SHIPMENT_REJECTION_ATTESTATION_LABEL,
      PLEASE_ENTER_TIME_LABEL,
      REJECT_DELIVERY_BUTTON_LABEL,
      BACK_LABEL,
      PLEASE_SELECT_DATE_LABEL,
      BACK_TO_SHIPMENT_LABEL,
      LOADING_LABEL,
      CLOSE_LABEL,
      ERROR_LABEL,
      PRODUCT_REJECTION_NOTES_LABEL,
      REASON_FOR_REJECTION_LABEL,
      PLEASE_SELECT_LABEL
    };

  @wire(getCheckUserPermission,{permissionName: checkCustomPermissionFpReceiver})
    wiredUserPermission({error,data}) {
      if(error) {
        this.error = error;
      } else if (data) {
        this.capturePrimaryFPShipmentAccess = data;
      }
    }

  @wire(getFieldDetails, { recordId: '$recordId'})
  wiredShipmentDetails({data, error}){
    if(data){
        this.result= data;
        utilityMethodToConvertLastModifiedDate({
          inputDate: new Date(),
          tmz: this.result.CCL_Ship_to_Time_Zone__c
      })
      .then(results=>{
          this.lastModifiedDate = results;
      })
      .catch(error=>{
          this.showErrorOnScreen=true;
          this.err = JSON.stringify(error);
      })
    }
    else if (error) {
   
      this.error = unknownError;
      if (Array.isArray(error.body)) {
        this.error = error.body.map(e => e.message).join(', ');
        this.dispatchEvent(
          new ShowToastEvent({
            title: errorArrayAvailable,
            message: error.body.message,
            variant: 'error',
          }),
        );
      } 
      this.result = 'undefined';
    }
}
    closeModal(){
        const custEvent = new CustomEvent(
                                    'callpasstoparent', {
                                        detail: this.closeCurrentModal
                                    });
                                this.dispatchEvent(custEvent);
        this.closeRejectProcesForm = false;
    }
    
    // showErrorToast() {
    //     const evt = new ShowToastEvent({
    //         title: 'Error',
    //         message: this.err,
    //         variant: 'error',
    //         mode: 'sticky'
    //     });
    //     this.dispatchEvent(evt);
    // }

    @wire(getRecord, {
        recordId: USER_ID,
        fields: [NAME_FIELD]
    }) wireuser({
        error,
        data
    }) {
        if (error) {
           this.errorValue = error ;
        } else if (data) {

            this.loggedInUserName = data.fields.Name.value;
        }
    }


    @wire(getObjectInfo, { objectApiName: SHIPMENT_OBJECT })
    shipmentInfo;

    @wire(getPicklistValues,
        {
            recordTypeId: '$shipmentInfo.data.defaultRecordTypeId', //pass id dynamically
            fieldApiName: REASON_FOR_REJECTION_FIELD
        }
    )
    reasonForRejectionPicklist;

    getDeliveryDate(event){
        this.showErrorOnScreen=false;
        this.actualDateValue=event.target.value;
        var newDate = new Date()
        this.dateValue = newDate.toISOString()

            utilityMethodToConvertDate({
                inputDate: event.target.value
            })
            .then(result=>{
                this.selectedDate= result;


            })
            .catch(error=>{
                this.showErrorOnScreen=true;
                this.err = JSON.stringify(error);
            })
    }
    getDeliveryTime(event){
        this.showErrorOnScreen=false;
        this.selectedTime=(event.target.value).substr(0,5);
        this.selectedTargetValue = event.target.value;
     
        
      getsUserTimeZoneMethod({
      })
        .then(result=>{
          this.timeZoneValue = (result).substr(4,6);
          this.fields["CCL_Actual_FP_Delivery_Date_Time__c"] = this.actualDateValue + 'T' + this.selectedTargetValue + this.timeZoneValue;


      })
      .catch(error=>{
          this.showErrorOnScreen=true;
          this.err = JSON.stringify(error);
      })
    }
    getNumberOfBagsReceived(event){
        this.showErrorOnScreen=false;
        this.numberOfBagsReceived=event.target.value;
        this.fields["CCL_Number_of_Bags_Received__c"]= this.numberOfBagsReceived;
    }
    getReasonForRejection(event){
        this.showErrorOnScreen=false;
        this.reasonForRejection=event.target.value;
        this.fields["CCL_Product_Rejection_Reason__c"]= this.reasonForRejection;
    }
    getProductRejectionNotes(event){
        this.showErrorOnScreen=false;
        this.productionRejectionNotes=event.target.value;
        this.fields["CCL_Product_Rejection_Notes__c"]= this.productionRejectionNotes;
    }
    getProductRejectionConfirmation(event){
        this.showErrorOnScreen=false;
        this.rejectConfirmationCheckBox= event.target.checked;
        this.fields["CCL_Product_Rejection_Confirmation__c"]= this.rejectConfirmationCheckBox;
        this.fields["CCL_Status__c"]='Product Delivery Rejected';
    }
    shipmentRecProcess() {
        this.isLoaded=false;
        this.fields["Id"]=this.recordId;
        this.fields["CCL_Product_Acceptance_Status__c"]='Rejected';
        this.fields["CCL_Acceptance_Rejection_Details__c"]=this.loggedInUserName+' '+this.lastModifiedDate+ '  '+this.shipmentrecorddetail.CCL_Ship_to_Time_Zone__c;
        this.recordInput = { fields: this.fields };
        updateRecord(this.recordInput)
        .then(result=>{
            this.showErrorOnScreen=false;
            this.returnVal = result;
            this.isLoaded=true;
            this.showFinalScreen=true;
        })
        .catch(error=>{
            this.showFinalScreen=false;
            this.showErrorOnScreen=true;
            this.isLoaded=true;
            this.err = JSON.stringify(error);
        })
    }

    validateFormAndProceedToRejection(){
                this.fieldValidations = this.template.querySelectorAll("lightning-input");
                this.count = 0;
                for (let i = 0; i < this.fieldValidations.length; i++) {
                    this.fieldNullCheck(this.fieldValidations[i], CCL_Mandatory_Field);
                    if (this.fieldValidations[i].checkValidity()) {
                      this.count = this.count + 1;
                    }
                  }
                  if (this.count == this.fieldValidations.length && this.isvalid) {
                    this.navigateFurther = true;
                  } else {
                    this.navigateFurther = false;
                  }

                this.fieldValidations=this.template.querySelectorAll("lightning-combobox");
                this.count = 0;
                for (let i = 0; i < this.fieldValidations.length; i++) {
                    this.fieldNullCheck(this.fieldValidations[i], CCL_Mandatory_Field);
                    if (this.fieldValidations[i].checkValidity()) {
                      this.count = this.count + 1;
                    }
                  }
                  if (this.count == this.fieldValidations.length && this.isvalid && this.navigateFurther) {
                    this.navigateFurther = true;
                  } else {
                    this.navigateFurther = false;
                  }

                this.fieldValidations=this.template.querySelectorAll("lightning-textarea");
                this.count = 0;
                for (let i = 0; i < this.fieldValidations.length; i++) {
                    this.fieldNullCheck(this.fieldValidations[i], CCL_Mandatory_Field);
                    if (this.fieldValidations[i].checkValidity()) {
                      this.count = this.count + 1;
                    }
                  }

                  if (this.count == this.fieldValidations.length && this.isvalid && this.navigateFurther) {
                    this.navigateFurther = true;
                  } else {
                    this.navigateFurther = false;
                  }

        if(this.isvalid && this.navigateFurther && this.actualDateValue>this.dateValue){
            this.showErrorOnScreen=true;
            this.err=actualProductDeliveryDateNotFuture;
        }
         else if(this.isvalid && this.navigateFurther){
            this.showErrorOnScreen=false;
            this.shipmentRecProcess();
            this.isContentLoaded = !this.isContentLoaded;
            this.getDeliveryTime();
        }
    }

    redirectToShipmentList() {
     
      this[NavigationMixin.Navigate]({
          type: 'standard__webPage',
          attributes: {
              url: '/?tabset-feec3=b498a'
          }
      },
      true // Replaces the current page in your browser history with the URL
    );
  }
    fieldNullCheck(comboCmp, errorMsg) {
        const value=comboCmp.value;
        const flag = comboCmp.checked;
        const fieldName = comboCmp.name;
        errorMsg='Required.';
        if (    (flag==undefined && (value==undefined || value === this.EMPTY_STRING || value == null))     ||    
                  (flag==false  && (value==undefined || value === this.EMPTY_STRING || value == null))  ) {
          
          comboCmp.setCustomValidity(errorMsg);
          comboCmp.reportValidity();
          this.navigateFurther = false;
          this.isvalid = false;
        } else {
          comboCmp.setCustomValidity(this.EMPTY_STRING);
          comboCmp.reportValidity();
          this.isvalid = true;
        }
      }

    goToListScreen(){
      this[NavigationMixin.Navigate]({
        type: "comm__namedPage",
        attributes: {
          pageName: "finished-product-list-view"
        }
      });
    }
    onClick() {
      window.location.reload(true);
    }
}
import LightningDatatable from 'lightning/datatable';
import customCareplan from './PSP_CallLookUp.html';
export default class pSP_SubLightningDatatable extends LightningDatatable  {
    
    static customTypes = {
        lookup: {
            template: customCareplan,
            typeAttributes: ['label'],
           
        }
    };
    
   
}
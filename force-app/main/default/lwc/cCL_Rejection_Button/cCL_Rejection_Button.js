import { LightningElement, track, api, wire } from "lwc";
import getFPShipmentRec from '@salesforce/apex/CCL_FPShipmentController.getFinishedProductShipmentRec';
import rejectDeliveryButtonLabel from '@salesforce/label/c.CCL_Reject_Delivery_Button';
import acceptDeliveryButtonLabel from '@salesforce/label/c.CCL_Accept_Delivery_Button';

export default class CCL_Rejection_Button extends LightningElement {
    // Flexipage provides recordId and objectApiName
    @track showRejectButton = false;
    @api recordId;
    @track finishedProductShipmentRec;
    @track showRejectContent;
    @track showAcceptContent;

    label={
        rejectDeliveryButtonLabel,
        acceptDeliveryButtonLabel
    };

    openRejectModal() {
        // to open modal set isModalOpen tarck value as true
        this.showRejectContent = true;
        this.showAcceptContent = false;
    }
    openAcceptModal(){
        this.showAcceptContent = true;
        this.showRejectContent = false;

    }
    closeModal(){
        this.showAcceptContent = false;
        this.showRejectContent = false;
    }

    @wire(getFPShipmentRec, {
        shipmentFinishedProductRecId : '$recordId'
    })
    currentFPShipmentRec({error,data}){
        if(data){
            this.finishedProductShipmentRec = data;
            if(this.finishedProductShipmentRec && this.finishedProductShipmentRec.CCL_Status__c == 'Product Shipped'){
                this.showRejectButton = true;
            }
        }
        if(error){
        }
    }


}
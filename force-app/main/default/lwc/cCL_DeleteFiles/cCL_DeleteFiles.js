import { LightningElement ,api,wire} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import deleteAllFiles from "@salesforce/apex/CCL_ADFController_Utility.deleteFile";
import getDocumentDetails from "@salesforce/apex/CCL_ADFController_Utility.getDocumentDetails";
import CCL_Delete_File from "@salesforce/label/c.CCL_Delete_File";
import CCL_Success from "@salesforce/label/c.CCL_Success";
import CCL_Error_deleting_record from "@salesforce/label/c.CCL_Error_deleting_record";
import CCL_Record_deleted from "@salesforce/label/c.CCL_Record_deleted";
import CCL_DeleteConfirmation_Title from "@salesforce/label/c.CCL_DeleteConfirmation_Title";
import CCL_DeleteConfirmation_Message from "@salesforce/label/c.CCL_Delete_Confirm_Text";
import CCL_DeleteConfirmation_Ok from "@salesforce/label/c.CCL_Delete";
import CCL_DeleteConfirmation_Cancel from "@salesforce/label/c.CCL_DeleteConfirmation_Cancel";
export default class CCL_DeleteFiles extends NavigationMixin(LightningElement) {
   
   documentId;
   showDeleteButton=false;
  isDialogVisible=false;
  label = {
  
    CCL_DeleteConfirmation_Title,
    CCL_DeleteConfirmation_Message,
    CCL_DeleteConfirmation_Ok,
    CCL_DeleteConfirmation_Cancel,CCL_Delete_File
  };
@api recordId;

@wire(getDocumentDetails, { recordId: "$recordId" })
wiredSteps({ error, data }) {
  if (data) {
   
   this.documentId=data[0].Id;
   if(data[0].CCL_Order__c!=null&&data[0].CCL_Order__c!=undefined){
       this.showDeleteButton=true;
   }
    this.error = null;
  } else if (error) {
    
    this.error = error;
  }
}
handleClick(){
    this.isDialogVisible=false;  
}
onClick(){
    this.isDialogVisible=true;
}
deleteFile(){
        deleteAllFiles({
            contentDocumentId: this.recordId
          })
            .then((result) => {
            

            this.dispatchEvent(
                new ShowToastEvent({
                    title: CCL_Success,
                    message: CCL_Record_deleted,
                    variant: 'success'
                })
            );

            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: this.documentId,
                    objectApiName: 'CCL_Document__c',
                    actionName: 'view'
                }
            });
                
               
            })
            .catch(error => {
                
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: CCL_Error_deleting_record,
                        message: error.body.message,
                        variant: 'error'
                    })
                );
            });
    }
}
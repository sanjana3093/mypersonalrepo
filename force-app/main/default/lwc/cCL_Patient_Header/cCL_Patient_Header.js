import { LightningElement, api, track,wire } from "lwc";
import getADFHeaderDetails from "@salesforce/apex/CCL_ADF_Controller.getADFHeadeInfo";
import { CurrentPageReference } from "lightning/navigation";
import { registerListener,fireEvent } from "c/cCL_Pubsub";
import patientName from "@salesforce/label/c.CCL_Patient_Name";
import treatmentProtoocol from "@salesforce/label/c.CCL_Treatment_Protocol_Sub_Id";
import hospPatId from "@salesforce/label/c.CCL_Hospital_Patient_ID_label";
import therapy from "@salesforce/label/c.CCL_Indication_Clinical_Trial";
import weight from "@salesforce/label/c.CCL_Patient_Weight";
import batchId from "@salesforce/label/c.CCL_Novartis_Batch_ID";
import birthDate from "@salesforce/label/c.CCL_Date_of_Birth";
import checkUserPermission from "@salesforce/apex/CCL_ADFController_Utility.checkUserPermission";
export default class CCL_Patient_Header extends LightningElement{
  @api recordId;
  @track wiredsObjectData;
  @api rtnValue;
  @track patientName=patientName;
  @track patInitials='Patient Initials';
  @track treatmentProtoocol;
  @track therapylabel=therapy;
  @track weight=weight;
  @track novartisbatchId=batchId;
  @track birthDate=birthDate;
  @track name;
  @track firstname;
  @track lastname;
  @track subjectId;
  @track therapy;
  @track batchId;
  @track dob;
  @track patientWeight;
  @track status;
  @track middlename;
  @track suffix;
  @track patID;
  @track displayHospPatId=false;
  @track displayPatId=false;
  @wire(CurrentPageReference) pageRef;
   //2260
  adfVieweInternal='CCL_ADF_Viewer_Internal';
  isADFViewerInternal=false;
  connectedCallback() {
    registerListener("setAdfStatus", this.setAdfStatus, this);
    this.getPermission();
    getADFHeaderDetails({
      recordId: this.recordId
    })
      .then((result) => {
        this.rtnValue = result;
		if(this.rtnValue[0].CCL_Date_Of_Birth_Text__c){
			this.dob =this.rtnValue[0].CCL_Date_Of_Birth_Text__c;
        }
		if(this.rtnValue[0].CCL_Patient_Name__c){
			this.name = this.rtnValue[0].CCL_Patient_Name__c;
		}
		 if(this.rtnValue[0].CCL_Patient_Id__c ){
      this.displayPatId=true;
      this.patID = this.rtnValue[0].CCL_Patient_Id__c;
		}
		if(this.rtnValue[0].CCL_Patient_Name__c){
        if(this.rtnValue[0].CCL_Patient_Name__c.split(" ").length==4){
          this.firstname = this.rtnValue[0].CCL_Patient_Name__c.split(" ")[0];
          this.lastname = this.rtnValue[0].CCL_Patient_Name__c.split(" ")[2];
          this.middlename = this.rtnValue[0].CCL_Patient_Name__c.split(" ")[1];
          this.suffix = this.rtnValue[0].CCL_Patient_Name__c.split(" ")[3];
        }
        else if(this.rtnValue[0].CCL_Patient_Name__c.split(" ").length==3){
          this.firstname = this.rtnValue[0].CCL_Patient_Name__c.split(" ")[0];
          this.lastname = this.rtnValue[0].CCL_Patient_Name__c.split(" ")[2];
          this.middlename = this.rtnValue[0].CCL_Patient_Name__c.split(" ")[1];
        }
        else{
          this.firstname = this.rtnValue[0].CCL_Patient_Name__c.split(" ")[0];
          this.lastname = this.rtnValue[0].CCL_Patient_Name__c.split(" ")[1];
        }
		}
		if(this.rtnValue[0].CCL_Order__r.CCL_Initials_COI__c  ){
          this.patientName=this.patInitials;

       }
		 if(this.rtnValue[0].CCL_Therapy__r.CCL_Type__c == 'Clinical'){
            this.treatmentProtoocol=treatmentProtoocol;
          } else {
            this.treatmentProtoocol=hospPatId;
          }
          if(this.rtnValue[0].CCL_Treatment_Protocol_SubID_HospitalID__c){
            this.displayHospPatId=true;
            this.subjectId=this.rtnValue[0].CCL_Treatment_Protocol_SubID_HospitalID__c;
          }
        this.therapy=this.rtnValue[0].CCL_Therapy_Formula__c;
        this.batchId=this.rtnValue[0].CCL_Novartis_Batch_Id__c;
        this.patientWeight=this.rtnValue[0].CCL_Patient_Weight__c;
        this.status=this.rtnValue[0].CCL_Status__c;
        if (result.length === 0) {
          this.rtnValue = null;
        }

        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
	  registerListener("statusChange", this.handleStatusChange,this);
  }
  handleStatusChange(status){
    this.status = status;
  }

  setAdfStatus(adfStatus) {
    this.status=adfStatus;
  }

  formatDate(date) {
    let mydate = new Date(date+'T00:00:00');
  if(new Date(date+'T00:00:00')=='Invalid Date'){
    mydate=new Date(date);
  }
    let monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];

     let day = "" + mydate.getDate();
    if (day.length < 2) {
      day = "0" + day;
    }
    let monthIndex = mydate.getMonth();
    let monthName = monthNames[monthIndex];
    let year = mydate.getFullYear();
    return `${day} ${monthName} ${year}`;
  }
  renderedCallback(){
    if(this.status == 'ADF Rejected'){
      const displayCheck = this.template.querySelector(".statusColr");
      displayCheck.classList.remove('statusColr');
    }
  }
  //2260
  getPermission(){
    checkUserPermission({
      permissionName: this.adfVieweInternal
    })
      .then((result) => {
       this.isADFViewerInternal=result;
       if(result){
        fireEvent(this.pageRef, 'showSummaryLink', true);
       }

        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }
}
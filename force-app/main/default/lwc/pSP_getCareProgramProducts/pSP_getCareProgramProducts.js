import {
    LightningElement,
    api,
    track
} from 'lwc';
import fetchRecords from "@salesforce/apex/PSP_custLookUpCntrl.getCareProgramProducts";
import selectMsg from "@salesforce/label/c.PSP_Please_Select";
import errMsg from "@salesforce/label/c.PSP_No_CPP_Er_Msg";
import product from "@salesforce/label/c.PSP_Product";
import service from "@salesforce/label/c.PSP_Service";
export default class PSP_getCareProgramProducts extends LightningElement {
    @api careProgram;
    @api recordType;
    @api selectedProServiceId;
    @api selectedProServiceName;
    @api productServiceName;
    @api rtnValue;
    @track selectMsg = selectMsg;
    @track errMsg = errMsg;
    @track ifProduct=false;
    @track product=product;
    @track service=service;
    connectedCallback() {

        fetchRecords({
                careprogramId: this.careProgram,

                recordType: this.recordType

            })
            .then(result => {

                this.rtnValue = result;
                if (result.length === 0) {
                    this.rtnValue = null;
                }

                this.error = null;
            })
            .catch(error => {
                this.error = error;
                this.enrolledprescriptionlist = null;
            });
           
            if(this.recordType==='Product'){
                this.ifProduct=true;
            }

    }
    handleChange(event) {
        let index = event.target.dataset.item;

        if (event.target.checked) {
            this.selectedProServiceId = this.rtnValue[index].Id;
            this.selectedProServiceName = this.rtnValue[index].Name;
            this.productServiceName = this.rtnValue[index].Product.Name

        }
    }

    //validate function
    @api
    validate() {

        if (this.selectedProServiceId !== undefined && this.selectedProServiceId !== null) {
            return {
                isValid: true
            };
        } else {
            //If the component is invalid, return the isValid parameter as false and return an error message. 
            return {
                isValid: false,
                errorMessage: 'Please select a Care Program Product.'
            };
        }
    }
}
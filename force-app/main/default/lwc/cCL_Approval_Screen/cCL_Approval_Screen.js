import { LightningElement,api,wire } from 'lwc';
import { NavigationMixin,CurrentPageReference} from "lightning/navigation";
import { getRecord} from "lightning/uiRecordApi";
import {fireEvent} from 'c/cCL_Pubsub';
import apheresisStatus from "@salesforce/schema/CCL_Apheresis_Data_Form__c.CCL_Status__c";
import CCL_ApprovedADF_Message from "@salesforce/label/c.CCL_ApprovedADF_Message";
import CCL_SubmittedADF_Message from "@salesforce/label/c.CCL_SubmittedADF_Message";
import CCL_RejectedADF_Message from "@salesforce/label/c.CCL_RejectedADF_Message";
import CCL_ApprovedADF_Header from "@salesforce/label/c.CCL_ApprovedADF_Header";
import CCL_SubmittedADF_Header from "@salesforce/label/c.CCL_SubmittedADF_Header";
import CCL_RejectedADF_Header from "@salesforce/label/c.CCL_RejectedADF_Header";
import CCL_Back_to_patient_List from "@salesforce/label/c.CCL_Back_to_patient_List";
const fields = [
  apheresisStatus
];
export default class CCL_Approval_Screen extends NavigationMixin(LightningElement) {
  label = {
    CCL_ApprovedADF_Message,
    CCL_SubmittedADF_Message,
    CCL_RejectedADF_Message,
    CCL_ApprovedADF_Header,
    CCL_SubmittedADF_Header,
    CCL_RejectedADF_Header,
    CCL_Back_to_patient_List
  };
  @api recordId;
  @api adfStatus;
  @api isAdfApproved=false;
  @api isAdfRejected=false;
  @api isAdfSubmitted=false;
  @wire(CurrentPageReference) pageRef;
  @wire(getRecord, {
    recordId: "$recordId", fields
}) wireADFData({
    error,
    data
}) {
    if (error) {
       this.error = error ;
    } else if (data) {
        this.adfStatus = data.fields.CCL_Status__c.value;
        fireEvent(this.pageRef, 'setAdfStatus', this.adfStatus);
        if(this.adfStatus==='ADF Approved'){
          this.isAdfApproved=true;
        } else if(this.adfStatus==='ADF Rejected'){
          this.isAdfRejected=true;
        } else if(this.adfStatus==='ADF Pending Approval'){
          this.isAdfSubmitted=true;
        }
    }
}

    goToListScreen(){
        this[NavigationMixin.Navigate]({
          type: "comm__namedPage",
          attributes: {
            pageName: "adf-patient-list"
          }
        });
        }
}
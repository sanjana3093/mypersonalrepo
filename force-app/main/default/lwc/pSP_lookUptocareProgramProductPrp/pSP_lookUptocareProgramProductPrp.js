import { LightningElement, track, api } from 'lwc';
import fetchRecords from "@salesforce/apex/PSP_Search_Component_Controller.fetchRecords";
export default class PSP_lookUptocareProgramProductPrp extends LightningElement {
  @track greeting = "";
  @track products = [];
  @api productServicename="";
  @track index = 0;
  @track dynamicClass;
  @track proservicedetail = [];
  @api recordId;
  @track iconName = "standard:product";
  @api tabproductservice;
  @api selectedItems = [];
  @track nonselected = true;
  @api selectedItem;
  @track searchtext = 'Search a Care Program Product';
    @api
    get UiClass() {
      if (this.products.length > 0 && this.greeting && this.nonselected) {
        this.dynamicClass =
          "slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-lookup__menu displayblock";
      } else {
        this.dynamicClass =
          "slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-lookup__menu";
      }
      return this.dynamicClass;
    }
    changeHandler(event) {
        this.greeting = event.target.value;
        console.log('check 1');
        if (this.greeting.length >= 2) {
          this.nonselected = true;
          fetchRecords({
            recordType: 'Product',
            searchKeyword: this.greeting
            
          })
            .then(result => {
              this.products = result;
              this.error = null;
            })
            .catch(error => {
              this.error = error;
              this.products = null;
            });
        } else {
          this.nonselected = false;
        }
        console.log('this list is'+JSON.stringify(this.products));
      }
      handleClickProducts(event){
          let i=0;
        this.selectedItem = event.target.dataset.item;
        console.log('val is'+ this.selectedItem);
        for(i=0;i<this.products.length;i++){
            if(this.products[i].Id== this.selectedItem){
                this.greeting=this.products[i].Name;
                this.productServicename=this.products[i].Name;
            }
        }
        //this.greeting =  this.selectedItem;
        console.log('val is'+ this.selectedItem+ this.greeting);
        this.nonselected = false;
      }
}
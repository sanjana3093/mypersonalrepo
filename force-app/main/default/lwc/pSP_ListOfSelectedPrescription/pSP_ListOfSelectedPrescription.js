import { LightningElement,api,track } from 'lwc';
import selectStatus from "@salesforce/label/c.PSP_select_Status";
export default class PSP_ListOfSelectedPrescription extends LightningElement {
    @api selectedprecriptionIdList=[];
    @api selectedprecriptionNameList=[];
    @track selecPrescriptionStatus=selectStatus;
}
import {
    LightningElement,
    api,
    wire
} from 'lwc';
import getEpRecord from "@salesforce/apex/PSP_custLookUpCntrl.getEnrolledPrograms";
export default class PSP_GetEplist extends LightningElement {
    @api recordId;
    @api enrolledprogramlist;
    @api enrolledprogramName;
    @api careProgramName;
    @api showEpCmp = false;
    @api careProgramId;

    connectedCallback() {

        getEpRecord({
                recordId: this.recordId,

            })
            .then(result => {
                if (result.length > 1) {
                    this.showEpCmp = true;
                } else if (result.length === 1) {
                    this.showEpCmp = false;
                    this.enrolledprogramlist = result[0].Id;
                    this.enrolledprogramName = result[0].Name;
                    this.careProgramName = result[0].PSP_Program_Enrollee__c;
                    this.careProgramId = result[0].CareProgramId;
                    this.careProgramName = this.careProgramName.substring(43, this.careProgramName.length - 4);
                }


                this.error = null;
            })
            .catch(error => {
                this.error = error;

            });

    }
}
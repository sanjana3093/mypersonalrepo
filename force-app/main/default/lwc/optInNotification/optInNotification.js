import { LightningElement, track, api, wire } from "lwc";
import saveVal from "@salesforce/apex/saveContact.savePicklist";
export default class OptInNotification extends LightningElement {
  @track notificatinList;
  @api label = ""; //Name of the dropDown
  @api maxselected = 2; //Max selected item display
  @api options; // List of items to display
  @api showfilterinput = false; //show filterbutton
  @api showrefreshbutton = false; //show the refresh button
  @api showclearbutton = false; //show the clear button
  @api comboplaceholder = "Select a value";
    @track yourSelectedValues='';
   
  @track _initializationCompleted = false;
  @track _selectedItems = "Select a value";
  @track _filterValue;
  
  constructor () {
    super();
    this._filterValue = '';
    //this.showfilterinput = true;
    //this.showrefreshbutton = true;
    //this.showclearbutton = true;
}
renderedCallback () {
    let self = this;
    if (!this._initializationCompleted) {
        this.template.querySelector ('.ms-input').addEventListener ('click', function (event) {
            console.log ('multipicklist clicked');
            self.onDropDownClick(event.target);
            event.stopPropagation ();
        });
        this.template.addEventListener ('click', function (event) {
            console.log ('multipicklist-1 clicked');
            event.stopPropagation ();
        });
        document.addEventListener ('click', function (event) {
            console.log ('document clicked');
            self.closeAllDropDown();
        });
        this._initializationCompleted = true;
        this.setPickListName ();
    }
}
handleItemSelected (event) {
    let self = this;
    this._mOptions.forEach (function (eachItem) {
        if (eachItem.key == event.detail.item.key) {
            console.log('the vals are in 47'+event.detail.item.key+event.detail.selected);
            eachItem.selected = event.detail.selected;
            return;
        }
    });
    this.setPickListName ();
    this.onItemSelected ();
}
closeAllDropDown () {
    Array.from (this.template.querySelectorAll ('.ms-picklist-dropdown')).forEach (function (node) {
         node.classList.remove('slds-is-open');
    });

   
    let arr=this.getSelectedItems ();
    console.log('the arr'+JSON.stringify(arr));
    if(arr.length>0){
        this.yourSelectedValues = '';
        let self = this;
        arr.forEach(function (eachItem) {
            console.log(eachItem.value);
            self.yourSelectedValues += eachItem.value + "; ";
          });
        
        console.log("the values are" + this.yourSelectedValues);
        saveVal({
          picklistVal: this.yourSelectedValues,
          contactId: "0032800000i3paqAAA"
        })
          .then((result) => {
            console.log("success");
          })
          .catch((error) => {
            this.error = error;
          });
    }

    
}

onDropDownClick (dropDownDiv) {
    let classList = Array.from (this.template.querySelectorAll ('.ms-picklist-dropdown'));
    if(!classList.includes("slds-is-open")){
        this.closeAllDropDown();
        Array.from (this.template.querySelectorAll ('.ms-picklist-dropdown')).forEach (function (node) {
            node.classList.add('slds-is-open');
        });
    } else {
        this.closeAllDropDown();
    }
}
onClearClick (event) {
    this._filterValue = '';
    this.updateListItems ('');
}
connectedCallback () {  
    console.log('the optionsa re'+JSON.stringify(this.options));
    this.initArray (this);
}
initArray (context) {
    context._mOptions = new Array ();  
    console.log('the optionsa re'+JSON.stringify(context.options));
    context.options.forEach (function (eachItem) {
        context._mOptions.push (JSON.parse (JSON.stringify(eachItem)));
    });
    console.log('the items are'+JSON.stringify(this._mOptions));
}
updateListItems (inputText) {
    Array.from (this.template.querySelectorAll('c-pick-list-item')).forEach (function (node) {
        if(!inputText){
            node.style.display = "block";
        } else if(node.item.value.toString().toLowerCase().indexOf(inputText.toString().trim().toLowerCase()) != -1){
            node.style.display = "block";
        } else{
            node.style.display = "none";
        }
    });
    this.setPickListName ();
}
setPickListName () {
    let selecedItems = this.getSelectedItems ();
    let selections = '' ;
    if (selecedItems.length < 1) {
        selections = this.comboplaceholder;
    } else if (selecedItems.length > this.maxselected) {
        selections = selecedItems.length + ' Options Selected';
    } else {
        selecedItems.forEach (option => {
            selections += option.value+',';
        });
    }
    this._selectedItems = selections;
}
@api
getSelectedItems () {
    let resArray = new Array ();
    this._mOptions.forEach (function (eachItem) {
        if (eachItem.selected) {
            resArray.push (eachItem);
        }
    });
    return resArray;
}

onItemSelected () {
    const evt = new CustomEvent ('itemselected', { detail : this.getSelectedItems ()});
    this.dispatchEvent (evt);
}
}

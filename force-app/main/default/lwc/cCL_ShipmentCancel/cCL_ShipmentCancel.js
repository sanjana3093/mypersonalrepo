
import { LightningElement, api, wire, track } from 'lwc';
import { getObjectInfo, getPicklistValues } from "lightning/uiObjectInfoApi";
import CCL_SHIPMENT_OBJ from "@salesforce/schema/CCL_Shipment__c";
import Cancel_Reason from '@salesforce/schema/CCL_Shipment__c.CCL_Shipment_Cancellation_Reason__c';
import { getRecord } from 'lightning/uiRecordApi';
import fetchFPShipmentRecord from '@salesforce/apex/CCL_FPShipmentController.fetchFPShipmentRecord';
import SHIPMENTSTATUS from '@salesforce/schema/CCL_Shipment__c.CCL_Status__c';
import updateShipmentForCancelReason from '@salesforce/apex/CCL_FPShipmentController.shipmentToBeUpdated';
import checkPermission from '@salesforce/apex/CCL_FPShipmentController.checkUserPermission';

import cannotCancelShipmentLabel from '@salesforce/label/c.CCL_Shipment_cannot_be_cancelled_text';
import CCL_Attempting_to_can_NVS_batch_IdLabel from '@salesforce/label/c.CCL_Attempting_to_can_NVS_batch_Id';
import CCL_Attempting_to_can_NVS_batch_IdLabel2 from '@salesforce/label/c.CCL_Attempting_to_can_NVS_batch_Id_2';
import CCL_action_cannot_be_undoneLabel from '@salesforce/label/c.CCL_action_cannot_be_undone';
import CCL_you_do_not_have_permissionLabel from '@salesforce/label/c.CCL_you_do_not_have_permission';
import CCL_BackLabel from '@salesforce/label/c.CCL_Back';
import CCL_LoadingLabel from '@salesforce/label/c.CCL_Loading';
import CCL_Cancel_Shipment_btnLabel from '@salesforce/label/c.CCL_Cancel_Shipment_btn';
import CCL_please_select_cancellation_reason_picklistLabel from '@salesforce/label/c.CCL_please_select_cancellation_reason_picklist';
import CCL_are_you_sure_you_want_to_cancel_ShipmentLabel from '@salesforce/label/c.CCL_are_you_sure_you_want_to_cancel_Shipment';

const FIELDS= [SHIPMENTSTATUS];
const outboundLogisticPermission = "CCL_Outbound_Logistics";

export default class CCL_ShipmentCancel extends LightningElement {

    @api recordId;
    @api fpShipmentId;
    @api fpShipmentList;
    @api fpshipmentData;
    @track newArr;
    @track shipmentData;
    @api cancelReason
    @track status;
    @api shipmentStatus;
    @track novartisBatchId;
	 @track shipmentIdname;

    @api shipmentToBeUpdated;
    @api loaded = false;
    @api canCancelShipment = false;
    @api cannotCancelShipment = false;
    @api hasPermissionToCancel =false;
    errorOnUpdate = false;

    label = {
        cannotCancelShipmentLabel,
        CCL_Attempting_to_can_NVS_batch_IdLabel,
        CCL_Attempting_to_can_NVS_batch_IdLabel2,
        CCL_action_cannot_be_undoneLabel,
        CCL_you_do_not_have_permissionLabel,
        CCL_BackLabel,
        CCL_LoadingLabel,
        CCL_Cancel_Shipment_btnLabel,
        CCL_please_select_cancellation_reason_picklistLabel,
        CCL_are_you_sure_you_want_to_cancel_ShipmentLabel
    };

    connectedCallback() {
        this.getPermission();
    }

    getPermission() {
        checkPermission({
            permissionName: outboundLogisticPermission
          }).then((result) => {
              const checkPermissionUser = result;
              this.error = null;
              if(checkPermissionUser) {
                this.hasPermissionToCancel = true;
              } else {
                this.hasPermissionToCancel = false;
              }
            })
            .catch((error) => {
              this.error = error;
            });
    }

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS})
    wiredRecord({ error, data }) {
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
        } else if (data) {
            this.shipmentData = data;
            this.shipmentStatus =  this.shipmentData.fields.CCL_Status__c.value;

            if(this.shipmentStatus === 'Shipment Cancelled') {
                this.loaded = true;
                this.cannotCancelShipment = true;
                this.loaded = false;
            }else {
                this.getShipmentDetails();
            }
        }
    }
	@wire(getObjectInfo, {
		objectApiName: CCL_SHIPMENT_OBJ
	})
	objectInfo;

	@wire(getPicklistValues, {
		recordTypeId: "$objectInfo.data.defaultRecordTypeId",
		fieldApiName: Cancel_Reason
	})
	systemUsedPicklistValues(result) {
		if (result.data) {
			this.newArr = result.data.values;
		}
	}
    cannotCancel(event) {
        const closeModal = new CustomEvent("closemodal");
        this.dispatchEvent(closeModal);

    }
    handleChange(event) {
        if (event.target.dataset.id === "cancelReasonId") {
            this.cancelReason = event.target.value;
        }
    }
    getShipmentDetails() {
        this.loaded = true;
        fetchFPShipmentRecord({shipmentId: this.recordId})
        .then(result => {
            this.fpshipmentData = result;
            this.fpShipmentId=this.fpshipmentData[0].Id;
            this.status = this.fpshipmentData[0].CCL_Status__c;
        if (
            this.status == 'Product Delivery Accepted' ||
            this.status == 'Product Delivery Rejected' ||
            this.status == 'Product Delivery On Hold') {
            this.loaded = true;
            this.cannotCancelShipment = true
            this.loaded = false;
            }
        if (this.status == 'Product Shipment Planned' ||
            this.status == 'Product Returned' ||
            this.status == 'Product Shipped' ||
            this.status == null) {
            this.loaded = true;
            if(this.fpshipmentData[0].CCL_Order__r.CCL_Novartis_Batch_ID__c != undefined  || this.fpshipmentData[0].CCL_Order__r.CCL_Novartis_Batch_ID__c != null ) {
                this.novartisBatchId = this.fpshipmentData[0].CCL_Order__r.CCL_Novartis_Batch_ID__c;
            } else {
                this.novartisBatchId = "";
            }
			/*variable to show shipment id on cancel reason*/
            if(this.fpshipmentData[0].Name != null) {
                this.shipmentIdname = this.fpshipmentData[0].Name;
            }
            this.canCancelShipment = true;
            this.loaded = false;
            }

        })
        .catch(error => {
            this.loaded = false;
            this.error = error;
        });
    }

    CancelShipment(event) {
        // report validity
        this.loaded = true;
        let fieldVal = this.template.querySelector('.validValue');
        if(fieldVal.reportValidity()) {
            this.updateShipment();
        } else {
            this.loaded = false;
        }
    }

    updateShipment () {
        this.shipmentToBeUpdated=
        {
            "CCL_Shipment_Cancellation_Reason__c":this.cancelReason,
            "Id": "",
            "CCL_Status__c" : 'Shipment Cancelled',

        }
        this.shipmentToBeUpdated.Id = this.recordId;
        updateShipmentForCancelReason({ shipmentData: this.shipmentToBeUpdated })
        .then((result) => {
        if(result==true)
        {
            const cancelThisShipment = new CustomEvent("cancelshipment");
            this.dispatchEvent(cancelThisShipment);
        }
        })
        .catch((error) => {
            this.error = error.body.message;
            this.errorOnUpdate = error.body.message;
        this.loaded = false;
        });
    }
}
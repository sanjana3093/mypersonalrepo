import { LightningElement,api,track } from 'lwc';
import prescriberHeading from "@salesforce/label/c.PSP_Prescriber";
import searchText from "@salesforce/label/c.PSP_Search_a_Prescriber";
import errmsg from "@salesforce/label/c.PSP_Prescriotion_Qty_Err_Msg"; 
export default class PSP_PrescriberLookUp extends LightningElement {
    @api selectedItem=null;
    @api productServicename="";
    @api recordtype="HCP";
    @api object="Contact";
    @track prescriberHeading=prescriberHeading;
    @api searchtext=searchText;
    @api prescriber=null;
    @track errmsg=errmsg;
    getselectedDetails(event){
       // console.log('the event details are'+JSON.stringify(event.detail));
        this.selectedItem=event.detail.Id;
        this.productServicename=event.detail.Name;
       // console.log(this.productServicename+ this.selectedItem);
    }
    connectedCallback() {
        this.selectedItem=null;
    }
    @api
    validate() {
console.log('in prescriber'+this.selectedItem);
        if (this.prescriber !==0) {
            return {
                isValid: true
            };
        } else {
            //If the component is invalid, return the isValid parameter as false and return an error message. 
            return {
                isValid: false,
                errorMessage: errmsg
            };
        }
    }
  
}
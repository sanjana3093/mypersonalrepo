import { LightningElement,api } from 'lwc';

export default class CCL_DisplaySiteName extends LightningElement {

    @api fieldItem;
    @api addressList;
    @api siteId;
    get siteName(){
        let self=this;
        let sitename;
        let siteId=this.fieldItem?this.fieldItem.fieldValue:this.siteId;
        if(this.addressList){
        this.addressList.forEach(element => {
            if(element.CCL_Site__c==siteId){
                sitename=element.CCL_Site_Name__c;
            }
        })
    }
            return sitename;
    }
}
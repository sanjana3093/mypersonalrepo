import { LightningElement,api,wire,track} from 'lwc';
import fetchFPShipmentSummary from '@salesforce/apex/CCL_PRF_Controller.fetchFPShipmentSummary';
import getFileDetails from '@salesforce/apex/CCL_PRF_Controller.getUploadedFileDetails';
import CCL_Finished_Product_Shipment_Detailss from '@salesforce/label/c.CCL_Finished_Product_Shipment_Detailss';
import CCL_Shipment_Number from '@salesforce/label/c.CCL_Shipment_Number';
import CCL_Status from '@salesforce/label/c.CCL_Status';
import CCL_Ship_To_Locations from '@salesforce/label/c.CCL_Ship_To_Locations';
import CCL_Actual_Product from '@salesforce/label/c.CCL_Actual_Product';
import CCL_Shipment_Date_Time from '@salesforce/label/c.CCL_Shipment_Date_Time';
import CCL_Estimated_Product_Delivery from '@salesforce/label/c.CCL_Estimated_Product_Delivery';
import CCL_Date_Time from '@salesforce/label/c.CCL_Date_Time';
import CCL_Actual_Product_Delivery from '@salesforce/label/c.CCL_Actual_Product_Delivery';
import CCL_Number_of_Doses_Shipped from '@salesforce/label/c.CCL_Number_of_Doses_Shipped';
import CCL_Number_of_Bags_Shipped from '@salesforce/label/c.CCL_Number_of_Bags_Shipped';
import CCL_numberOfBagsReceived from '@salesforce/label/c.CCL_numberOfBagsReceived';
import CCL_Shipment_Reason from '@salesforce/label/c.CCL_Shipment_Reason';
import CCL_Expiration_Date_of_Shipped_Bags from '@salesforce/label/c.CCL_Expiration_Date_of_Shipped_Bags';
import CCL_Related_Documents from '@salesforce/label/c.CCL_Related_Documents';
import CCL_Hospital_Purchase_Order_Number from '@salesforce/label/c.CCL_Hospital_Purchase_Order_Number';

import getAddress from "@salesforce/apex/CCL_Utility.fetchAddress";
export default class CCLFPShipmentSummary extends LightningElement {
	  label={
      CCL_Finished_Product_Shipment_Detailss,
      CCL_Shipment_Number,
      CCL_Status,
      CCL_Ship_To_Locations,
      CCL_Actual_Product,
      CCL_Shipment_Date_Time,
      CCL_Estimated_Product_Delivery,
      CCL_Date_Time,
      CCL_Actual_Product_Delivery,
      CCL_Number_of_Doses_Shipped,
      CCL_Number_of_Bags_Shipped,
      CCL_numberOfBagsReceived,
      CCL_Shipment_Reason,
      CCL_Expiration_Date_of_Shipped_Bags,
      CCL_Hospital_Purchase_Order_Number,
      CCL_Related_Documents

  };
    @api orderId;
    @api FPShipmentList=[];
    @api street;
    @api city;
    @api state;
    @api postalCode;
    @api country;
    @api shipToLocation;
    @api index;
    @api productAcceptedBy=[];
    @api productRejectedBy=[];
    @api isProductAccepted=false;
    @api isProductRejected=false;
    @api shipmentStatus=[];
    @api getStatus= [];
    @track filesUploaded=[];
    @track files=[];
    @track error;
    @api fileTitle;
    @api ContentDocumentId;
    @api fileExtension;
    @api isDewar1;
    @api isDewar2;
    @api dewarUrl;
    @api orderStatus='';
	@track shipLoc;
@api addressList;

    @wire(fetchFPShipmentSummary, { orderId: "$orderId"})
    fetchFPShipmentSummary({ error, data }) {
      if (data) {
        this.FPShipmentList = data;
        let self = this;
         this.FPShipmentList.forEach(function(node){
         getFileDetails({ recordId: node.Id})
          .then(result => {
			  if(node.CCL_Ship_to_Location__c){
              self.shipLoc = node.CCL_Ship_to_Location__c;
              self.getAddresses();
			  }
            self.filesUploaded = result;
            self.files=[];
           self.filesUploaded.forEach(function(node1)
            {
             self.files=self.files.concat({
                "LinkedEntityId"  : node1.LinkedEntityId,
                "ContentDocumentId" : node1.ContentDocumentId,
                "Title" : node1.ContentDocument.Title,
                "FileExtension" : node1.ContentDocument.FileExtension
               });
            });
         self.shipmentStatus=node.CCL_Status__c;
          if((self.shipmentStatus!==undefined) && (self.shipmentStatus==='Product Delivery Accepted'))
            {
                self.productAcceptedBy=node.CCL_Acceptance_Rejection_Details__c;
                if(self.productAcceptedBy!==undefined)
                  {
                    self.productAcceptedBy=self.productAcceptedBy.substr(0,self.productAcceptedBy.indexOf(' '));
                 }
                  self.isProductAccepted=true;
                   self.isProductRejected=false;
                   if(node.CCL_Dewar_Tracking_link_1__c!==undefined)
                   {
                    self.isDewar1=true;
                   }
                   if(node.CCL_Dewar_Tracking_link_2__c!==undefined)
                   {
                    self.isDewar2=true;
                   }
				           self.getStatus=self.getStatus.concat({
                   "Id"  : node.Id,
                   "CCL_Status__c" : node.CCL_Status__c,
                   "CCL_Actual_Shipment_Date_Time_Text__c" : node.CCL_Actual_Shipment_Date_Time_Text__c,
                   "CCL_Estimated_FP_Delivery_Date_Time_Text__c" : node.CCL_Estimated_FP_Delivery_Date_Time_Text__c,
                   "CCL_Actual_FP_Delivery_Date_Time_Text__c" : node.CCL_Actual_FP_Delivery_Date_Time_Text__c,
                   "CCL_Waybill_number__c" : node.CCL_Waybill_number__c,
                   "CCL_Number_of_Doses_Shipped__c" : node.CCL_Number_of_Doses_Shipped__c,
                   "CCL_Hospital_Purchase_Order_Number__c" : node.CCL_Hospital_Purchase_Order_Number__c,
                   "CCL_Dewar_Tracking_link_1__c" : node.CCL_Dewar_Tracking_link_1__c,
                   "CCL_Dewar_Tracking_link_2__c" : node.CCL_Dewar_Tracking_link_2__c,
                   "CCL_Number_of_Bags_Shipped__c" : node.CCL_Number_of_Bags_Shipped__c,
                   "CCL_Number_of_Bags_Received__c" : node.CCL_Number_of_Bags_Received__c,
                   "CCL_Shipment_Reason__c" : node.CCL_Shipment_Reason__c,
                   "CCL_Expiry_Date_of_Shipped_Bags_Text__c" : node.CCL_Expiry_Date_of_Shipped_Bags_Text__c,
                   "CCL_Product_Rejection_Reason__c" : node.CCL_Product_Rejection_Reason__c,
                   "CCL_Product_Rejection_Notes__c" : node.CCL_Product_Rejection_Notes__c,
                   "Name" : node.Name,
                   "productAcceptedBy" :  self.productAcceptedBy,
                   "isProductAccepted": self.isProductAccepted,
                   "isProductRejected":self.isProductRejected,
                   "isDewar1":self.isDewar1,
                   "isDewar2":self.isDewar2,
                   'files': self.files
                  });
               }
            else if((self.shipmentStatus!==undefined) && (self.shipmentStatus==='Product Delivery Rejected'))
            {
              if(node.CCL_Dewar_Tracking_link_1__c!==undefined)
                   {
                    self.isDewar1=true;
                   }
                   if(node.CCL_Dewar_Tracking_link_2__c!==undefined)
                   {
                    self.isDewar2=true;
                   }
				    self.productRejectedBy=node.CCL_Acceptance_Rejection_Details__c;
             if(self.productRejectedBy!==undefined)
              {
                self.productRejectedBy=self.productRejectedBy.substr(0,self.productRejectedBy.indexOf(' '));
              }
             self.isProductRejected=true;
              self.isProductAccepted=false;
              self.getStatus=self.getStatus.concat({
              "Id"  : node.Id,
              "CCL_Status__c" : node.CCL_Status__c,
              "CCL_Actual_Shipment_Date_Time_Text__c" : node.CCL_Actual_Shipment_Date_Time_Text__c,
              "CCL_Estimated_FP_Delivery_Date_Time_Text__c" : node.CCL_Estimated_FP_Delivery_Date_Time_Text__c,
              "CCL_Actual_FP_Delivery_Date_Time_Text__c" : node.CCL_Actual_FP_Delivery_Date_Time_Text__c,
              "CCL_Waybill_number__c" : node.CCL_Waybill_number__c,
              "CCL_Number_of_Doses_Shipped__c" : node.CCL_Number_of_Doses_Shipped__c,
              "CCL_Hospital_Purchase_Order_Number__c" : node.CCL_Hospital_Purchase_Order_Number__c,
              "CCL_Dewar_Tracking_link_1__c" : node.CCL_Dewar_Tracking_link_1__c,
              "CCL_Dewar_Tracking_link_2__c" : node.CCL_Dewar_Tracking_link_2__c,
              "CCL_Number_of_Bags_Shipped__c" : node.CCL_Number_of_Bags_Shipped__c,
              "CCL_Number_of_Bags_Received__c" : node.CCL_Number_of_Bags_Received__c,
              "CCL_Shipment_Reason__c" : node.CCL_Shipment_Reason__c,
              "CCL_Expiry_Date_of_Shipped_Bags_Text__c" : node.CCL_Expiry_Date_of_Shipped_Bags_Text__c,
              "CCL_Product_Rejection_Reason__c" : node.CCL_Product_Rejection_Reason__c,
              "CCL_Product_Rejection_Notes__c" : node.CCL_Product_Rejection_Notes__c,
              "Name" : node.Name,
              "CCL_Acceptance_Rejection_Details__c" : node.CCL_Acceptance_Rejection_Details__c,
             "isProductAccepted": self.isProductAccepted,
             'files': self.files,
             "isDewar1":self.isDewar1,
             "isDewar2":self.isDewar2,
			 "isProductRejected":self.isProductRejected});
     }
            else{
              if(node.CCL_Dewar_Tracking_link_1__c!==undefined)
                   {
                    self.isDewar1=true;
                   }
                   if(node.CCL_Dewar_Tracking_link_2__c!==undefined)
                   {
                    self.isDewar2=true;
                   }
              self.getStatus=self.getStatus.concat({
                "Id"  : node.Id,
                "CCL_Status__c" : node.CCL_Status__c,
                "CCL_Actual_Shipment_Date_Time_Text__c" : node.CCL_Actual_Shipment_Date_Time_Text__c,
                "CCL_Estimated_FP_Delivery_Date_Time_Text__c" : node.CCL_Estimated_FP_Delivery_Date_Time_Text__c,
                "CCL_Actual_FP_Delivery_Date_Time_Text__c" : node.CCL_Actual_FP_Delivery_Date_Time_Text__c,
                "CCL_Waybill_number__c" : node.CCL_Waybill_number__c,
                "CCL_Number_of_Doses_Shipped__c" : node.CCL_Number_of_Doses_Shipped__c,
                "CCL_Hospital_Purchase_Order_Number__c" : node.CCL_Hospital_Purchase_Order_Number__c,
                "CCL_Dewar_Tracking_link_1__c" : node.CCL_Dewar_Tracking_link_1__c,
                "CCL_Dewar_Tracking_link_2__c" : node.CCL_Dewar_Tracking_link_2__c,
                "CCL_Number_of_Bags_Shipped__c" : node.CCL_Number_of_Bags_Shipped__c,
                "CCL_Number_of_Bags_Received__c" : node.CCL_Number_of_Bags_Received__c,
                "CCL_Shipment_Reason__c" : node.CCL_Shipment_Reason__c,
                "CCL_Expiry_Date_of_Shipped_Bags_Text__c" : node.CCL_Expiry_Date_of_Shipped_Bags_Text__c,
                "CCL_Product_Rejection_Reason__c" : node.CCL_Product_Rejection_Reason__c,
                "CCL_Product_Rejection_Notes__c" : node.CCL_Product_Rejection_Notes__c,
                "Name" : node.Name,
                "CCL_Acceptance_Rejection_Details__c" : node.CCL_Acceptance_Rejection_Details__c,
                'files': self.files,
               "isProductAccepted": false,
               "isDewar1":self.isDewar1,
               "isDewar2":self.isDewar2,
			   "isProductRejected": false});
            }
         })
        .catch(error => {
            self.error = error;
        });

        });
   } else if (error) {
        this.error = error;
      }
    }

viewDocument(event) {
   this.url='/sfc/servlet.shepherd/document/download/'+event.target.name;
    window.open(this.url);
          }

 viewDewar(event) {
            this.dewarUrl=event.target.name;
            window.open(this.dewarUrl);
   }

   get displayWarning() {
    return (this.orderStatus == 'APH_OnHold' || this.orderStatus == 'MFG_OnHold' || this.orderStatus == 'SHP_OnHold');
  }
 get getStatusList()
  {
    return this.getStatus.sort(function(a, b){return ('' + a.Name).localeCompare(b.Name)});
  }
  getAddresses(){
    getAddress({
      accountIds: this.shipLoc
    })
      .then((result) => {
        this.addressList=result;
        this.error = null;
      })
      .catch((error) => {
        this.error = error;
      });
  }
}
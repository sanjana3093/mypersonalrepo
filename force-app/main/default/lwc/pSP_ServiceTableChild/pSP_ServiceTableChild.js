import { LightningElement, api, track } from "lwc";
import getCarePlanTemplate from "@salesforce/apex/PSP_Search_Component_Controller.getCarePlanTemplate";
import { NavigationMixin } from 'lightning/navigation';
export default class PSP_ServiceTableChild extends NavigationMixin(LightningElement) {
  @api rec = {};
  @api indexvar;
  @api servicedata;
  @track tempRec = this.rec;
  @track serviceMap = [];
  @track filterVal = "";
  @track chooseThisTempExecuting = false;
  @track nonselected = false;
  @track cppList = [];
  @track isDisabled = false;
  @track selectedItem;
  @track inputChanged = false;
  @track recordPageUrl;
  /****************************************
   * Set the row number for a row in table
   *******************************************/
  get indexNumber() {
    return this.indexvar + 1;
  }

  /***************************************************************
   * Set the selected care plan template name in lookup field
   ***************************************************************/
  get filterValOnLoad() {
    if (this.rec && this.filterVal === "" && !this.inputChanged) {
      return this.rec.carePlanTemplate;
    }
    return this.filterVal;
  }

  /***********************************************************************
   * Disable the lookup for records whose service type is not Careplan
   **********************************************************************/
  get inputDisbled() {
    // if (this.rec.serviceType === "Careplan") {
    //   this.isDisabled = false;
    // } else {
    //   this.isDisabled = true;
    // }
    return this.isDisabled;
  }

  /****************************************************
   * Show or hide the searched Care Plan Template
   ****************************************************/
  get UiClass() {
    console.log("skms:- class"+this.rec.nameURL);
    let dynamicClass;
    if (this.cppList.length > 0 && this.filterVal && this.nonselected) {
      dynamicClass =
        "slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-lookup__menu displayblock dropdown";
    } else {
      dynamicClass =
        "slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-lookup__menu";
    }
    return dynamicClass;
  }

  /*********************************************************
   * track the serach field and set the filter criteria
   *********************************************************/
  handleOnChange(event) {
    this.inputChanged = true;
    this.filterVal = event.target.value;
    this.nonselected = true;
    this.hideLi = true;
    // console.log("hello 123"+this.recordId);
    getCarePlanTemplate({ searchKeyword: this.filterVal, productId: "someId" })
      .then(result => {
        this.cppList = result;
        this.error = null;
      })
      .catch(error => {
        this.error = error;
        this.cppList = null;
      });
  }
  /*********************************************************************
   * fire the event to set new care plan template in Parent component
   *********************************************************************/
  chooseThisTemplate(event) {
    this.chooseThisTempExecuting = true;
    this.selectedItem = event.target.dataset.item;
    this.filterVal = this.selectedItem;
    let evtParam = {
      cppId: event.currentTarget.id.split("-")[0],
      productId: this.rec.prodId,
      source: "lookup",
      careProgProdId: this.rec.careProgProdId
    };
    const selectedEvent = new CustomEvent("selected", { detail: evtParam });
    this.dispatchEvent(selectedEvent);
    this.nonselected = false;
    this.chooseThisTempExecuting = false;
  }

  /*******************************************
   * Handle when to hide drowpdown
   ********************************************/
  removeClass(event) {
    if (this.chooseThisTempExecuting === false) {
      this.nonselected = false;
    }
  }
  /***************************************************
   * Handle Active column changes in table
   ***************************************************/
  handleCPPChecked(event) {
    let evtParam = {
      actChecked: event.target.checked,
      productId: this.rec.prodId,
      source: "cppToggle",
      careProgProdId: this.rec.careProgProdId
    };
    const selectedEvent = new CustomEvent("selected", { detail: evtParam });
    this.dispatchEvent(selectedEvent);
  }

  /***************************************************
   * Handle Default column changes in table
   ***************************************************/
  handleServiceToggle(event) {
    let evtParam = {
      defChecked: event.target.checked,
      productId: this.rec.prodId,
      source: "serviceToggle",
      careProgProdId: this.rec.careProgProdId
    };
    const selectedEvent = new CustomEvent("selected", { detail: evtParam });
    this.dispatchEvent(selectedEvent);
  }
  navigatePage(){
    console.log('in nvaigate'+this.rec.prodId);
    this[NavigationMixin.Navigate]({
      type: 'standard__recordPage',
      attributes: {
          recordId: this.rec.prodId,
          objectApiName: 'Product2',
          actionName: 'view',
      },
 
  });
}
navigatePageCpp(){
  this[NavigationMixin.Navigate]({
    type: 'standard__recordPage',
    attributes: {
        recordId: this.rec.careProgProdId,
        objectApiName: 'CareProgramProduct',
        actionName: 'view',
    },

});
}
  connectedCallback() {
    // window.addEventListener("click", function(event) {
    //   if (
    //     event.target.tagName === "C-P-S-P_-MANAGE-PROGRAM-CATALOGUE" ||
    //     event.target.tagName === "BODY"
    //   ) {
    //     this.nonselected = true;
    //     this.console.log("skms:--   this.nonselected1" + this.nonselected);
    //     console.log("tag name:-  " + event.target.tagName);
    //     this.template.nonselected = false;
    //     this.console.log("skms:--   this.nonselected2" + this.nonselected);
    //   }
    // });
  }
 
  
}
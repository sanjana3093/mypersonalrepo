import { LightningElement,track,wire } from 'lwc';
import { registerListener, unregisterAllListeners} from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
export default class MyWizardtest extends LightningElement {
    @track message;
    @track UiClass;
    @track liClass;
    @wire(CurrentPageReference) pageRef;
    connectedCallback() {
        registerListener('screenName', this.handleMessage, this);
    }
   
    handleMessage(myMessage) {
        this.message = myMessage;
        console.log('the event is'+this.message);
        if(this.message=='screen 1'){
           this.UiClass='slds-wizard__progress-bar width25';
           this.liClass='slds-wizard__item ';
        }
        if(this.message=='screen 2'){
            this.UiClass='slds-wizard__progress-bar width50';
            this.liClass='slds-wizard__item slds-is-active';
        }
        //Add your code here
    }
   
    disconnectCallback() {
        unregisterAllListeners(this);
    }
}